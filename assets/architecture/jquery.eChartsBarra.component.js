﻿// ┌───────────────────────────────────┐ \\
// │ jQuery eChartsBarra Component 1.0.0 │ \\
// ├───────────────────────────────────┤ \\
// │         Michel Oliveira           │ \\
// │      Prime Team Tecnologia        │ \\
// ├───────────────────────────────────┤ \\
// │             Martins               │ \\
// └───────────────────────────────────┘ \\
function ComponenteeChartsBarra() {
    //
    this.modelo = {
        /** se os dados serão formatados em real */
        formatarEmReal: false,
        /** URL de POST de onde os dados serão buscados */
        ajaxPostURL: '',
        /** ID HTML do container do controle (preferencialmente uma DIV) */
        idContainerControle: '',
        /** ID do controle que será gerado */
        idControle: '',
        idsEnviarRequisicaoAjax: [],
        /** Array com os IDs dos elementos que serão enviados na requisição */
        get addidsEnviarRequisicaoAjax() {
            var itmPush = this.idsEnviarRequisicaoAjax;
            //
            var pushObj = {
                /** Nome (key) do JSON que será enviado */
                nomeJsonEnviar: '',
                /** Tipo do JSON que será enviado (Padrao tipoTexto) OPÇÕES: tipoTexto,
                                                                             tipoNumerico,
                                                                             tipoCheckBox,
                                                                             tipoParametro,
                                                                             tipoName,
                                                                             tipoFuncao */
                tipoJsonEnviar: 'tipoTexto',
                /** ID do elemento em que será extraído o valor utilizando o método val() do elemento jQuery */
                idJsonEnviar: ''
            };
            //
            itmPush.push(pushObj);
            return pushObj;
        },
        /** Texto que será mostrado no gráfico */
        texto: '',
        /** Subtexto que será mostrado no gráfico */
        subtexto: '',
        legendas: [],
        /** Legendas que serão mostradas no gráfico */
        get addlegendas() {
            var itmPush = this.legendas;
            //
            var pushObj = {
                /** Nome da legenda */
                nomeLegenda: ''
            };
            //
            itmPush.push(pushObj);
            return pushObj;
        },
        /** Array vindo da requisição ajax que será mostrado */
        categoryArray: '',
        series: [],
        /** Series que será mostrada no gráfico */
        get addseries() {
            var itmPush = this.series;
            //
            var pushObj = {
                /** Nome da Series */
                seriesName: '',
                /** Array que viria da requisição ajax */
                seriesDataArray: '',
                /** Stack da series */
                seriesStack: '',
                /** Se irá mostrar o label */
                mostrarLabel: false,
                /** Posição do label */
                posicaoLabel: '',
                /** Formatter do label */
                formatterLabel: ''
            };
            //
            itmPush.push(pushObj);
            return pushObj;
        }
    }

    this.inicializar = function (configuracao) {
        var modelo = this.modelo;
        //
        if ($("#" + modelo.idControle).length) {
            $("#" + modelo.idControle).remove();
        }
        var template = '<div id="' + modelo.idControle + '" style="height:640px;"></div>';
        //
        $('#' + modelo.idContainerControle).append(template);
        $('#' + modelo.idContainerControle).contents().unwrap();
        //
        var obj;
        var ctrl = echarts.init(document.getElementById(modelo.idControle));
        $.post(modelo.ajaxPostURL, parametrosAjaxData()).done(function (data) {
            var categoryArray = Function("data", "return data." + modelo.categoryArray + ";")(data);
            //
            if (data == null || categoryArray == null || categoryArray.length == 0) {
                ctrl.clear();
                return;
            }
            $(window).resize(function () {
                ctrl.resize();
            });
            ctrl.setOption({
                title: {
                    text: modelo.texto,
                    subtext: modelo.subtexto,
                    x: 'center'
                },
                tooltip: {
                    trigger: 'axis',
                    formatter: function () {
                        if (modelo.formatarEmReal) {
                            return function (params) {
                                var ret = "";
                                $(params).each(function (idx, val) {
                                    if (ret == "") {
                                        ret = params[0].name + '<br/>'
                                    }
                                    ret += val.seriesName +
                                        ': R$ ' +
                                        convertStringParaDecimalFormatado(val.value, 2, true, 'en') +
                                        '<br/>'
                                });
                                return ret;
                            }
                        } else return null;
                    }()
                },
                legend: {
                    data: modelo.legendas,
                    top: '60'
                },
                toolbox: {
                    show: true,
                    feature: {
                        /*magicType: {
                            show: true,
                            type: ['line', 'bar', 'stack'],
                            title: {
                                line: 'Gráfico de Linha',
                                bar: 'Gráfico de Barra',
                                stack: 'Gráfico de Pilha',
                            }
                        },*/
                        restore: { show: true, title: 'Resetar', },
                        saveAsImage: { show: true, title: 'Salvar' },
                    }
                },
                calculable: true,
                xAxis: [
                    {
                        type: 'category',
                        data: categoryArray
                    }
                ],
                yAxis: [
                    {
                        show: true,
                        type: 'value',
                        splitArea: {
                            show: true
                        },
                        axisLabel: function () {
                            if (modelo.formatarEmReal) {
                                return {
                                    formatter: function (value) {
                                        return 'R$ ' + convertStringParaDecimalFormatado(value, 2, true, 'en');
                                    }
                                }
                            } else return null;
                        }()
                    }
                ],
                grid: {
                    x: 150,
                    y: 150
                },
                series:
                    function () {
                        var arr = [];
                        modelo.series.forEach(function (ser, idx) {
                            var itmPush = {
                                name: ser.seriesName,
                                type: 'bar',
                                data: Function("data", "return data." + ser.seriesDataArray + ";")(data)
                            }
                            if (ser.seriesStack !== '') {
                                itmPush.stack = ser.seriesStack;
                            }
                            if (ser.mostrarLabel) {
                                itmPush.itemStyle = {
                                    normal: {
                                        label: {
                                            show: true,
                                            position: ser.posicaoLabel,
                                            formatter: ser.formatterLabel !== '' ? ser.formatterLabel : null
                                        }
                                    }
                                };
                            }
                            arr.push(itmPush);
                        });
                        return arr;
                    }()
            });
        });
        function parametrosAjaxData() {
            var obj;
            var arr = [];
            modelo.idsEnviarRequisicaoAjax.forEach(function (itm, idx) {
                //
                if (itm.tipoJsonEnviar == 'tipoNumerico' ||
                    itm.tipoJsonEnviar == 'tipoTexto') {
                    arr[itm.nomeJsonEnviar] =
                        isNullOrEmpty((obj = $('#' + itm.idJsonEnviar).val()))
                        ? (itm.tipoJsonEnviar == 'tipoNumerico' ? "-1" : "")
                        : obj;
                } else if (itm.tipoJsonEnviar == 'tipoCheckBox') {
                    arr[itm.nomeJsonEnviar] = (($('#' + itm.idJsonEnviar).is(':checked')) ? "1" : "0");
                } else if (itm.tipoJsonEnviar == 'tipoName') {
                    arr[itm.nomeJsonEnviar] = ($("[name='" + itm.idJsonEnviar + "']").val());
                } else if (itm.tipoJsonEnviar == 'tipoFuncao') {
                    arr[itm.nomeJsonEnviar] = itm.idJsonEnviar();
                }
            });
            return $.extend(true, {}, arr);
        }
    }
}
