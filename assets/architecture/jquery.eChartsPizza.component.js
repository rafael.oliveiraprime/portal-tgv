﻿// ┌───────────────────────────────────┐ \\
// │ jQuery eChartsPizza Component 1.0.0 │ \\
// ├───────────────────────────────────┤ \\
// │         Michel Oliveira           │ \\
// │      Prime Team Tecnologia        │ \\
// ├───────────────────────────────────┤ \\
// │             Martins               │ \\
// └───────────────────────────────────┘ \\
function ComponenteeChartsPizza() {
    //
    this.modelo = {
        /** URL de POST de onde os dados serão buscados */
        ajaxPostURL: '',
        /** ID HTML do container do controle (preferencialmente uma DIV) */
        idContainerControle: '',
        /** ID do controle que será gerado */
        idControle: '',
        idsEnviarRequisicaoAjax: [],
        /** Array com os IDs dos elementos que serão enviados na requisição */
        get addidsEnviarRequisicaoAjax() {
            var itmPush = this.idsEnviarRequisicaoAjax;
            //
            var pushObj = {
                /** Nome (key) do JSON que será enviado */
                nomeJsonEnviar: '',
                /** Tipo do JSON que será enviado (Padrao tipoTexto) OPÇÕES: tipoTexto,
                                                                             tipoNumerico,
                                                                             tipoCheckBox,
                                                                             tipoParametro,
                                                                             tipoName,
                                                                             tipoFuncao */
                tipoJsonEnviar: '',
                /** ID do elemento em que será extraído o valor utilizando o método val() do elemento jQuery */
                idJsonEnviar: ''
            };
            //
            itmPush.push(pushObj);
            return pushObj;
        },
        /** Texto que será mostrado no gráfico */
        texto: '',
        /** Subtexto que será mostrado no gráfico */
        subtexto: '',
        legendas: [],
        /** Legendas que serão mostradas no gráfico */
        get addlegendas() {
            var itmPush = this.legendas;
            //
            var pushObj = {
                /** Nome da legenda */
                nomeLegenda: ''
            };
            //
            itmPush.push(pushObj);
            return pushObj;
        },
        /** Array que viria da requisição ajax */
        seriesDataArray: ''
    }

    this.inicializar = function (configuracao) {
        var modelo = this.modelo;
        if ($("#" + modelo.idControle).length) {
            $("#" + modelo.idControle).remove();
        }
        //
        var template = '<div id="' + modelo.idControle + '" style="height:500px;"></div>';
        //
        $('#' + modelo.idContainerControle).append(template);
        $('#' + modelo.idContainerControle).contents().unwrap();
        //
        var obj;
        var ctrl = echarts.init(document.getElementById(modelo.idControle));
        $.post(modelo.ajaxPostURL, parametrosAjaxData()).done(function (data) {
            var seriesDataArray = Function("data", "return data." + modelo.seriesDataArray + ";")(data);
            //
            console.log(seriesDataArray);
            if (data == null || seriesDataArray == null || seriesDataArray.length == 0) {
                ctrl.clear();
                return;
            } else {
                var allZero = true;
                seriesDataArray.forEach(function(itm, idx) {
                    if (itm.value != '0') {
                        allZero = false;
                        return;
                    }
                });
                if (allZero) {
                    ctrl.clear();
                    return;
                }
            }
            $(window).resize(function () {
                ctrl.resize();
            });
            ctrl.setOption({
                title: {
                    text: modelo.texto,
                    subtext: modelo.subtexto,
                    x: 'center',
                },
                tooltip: {
                    trigger: 'item',
                    formatter: function (params, ticket, callback) {
                        var ret = "";
                        $(params).each(function (idx, val) {
                            ret += val.name +
                                ': R$ ' +
                                convertStringParaDecimalFormatado(val.value, 2, true, 'en') +
                                '<br/>'
                        });
                        return ret;
                    }
                },
                legend: {
                    data: modelo.legendas,
                    top: '60',
                },
                toolbox: {
                    show: true,
                    feature: {
                        restore: { show: true, title: 'Resetar' },
                        saveAsImage: { show: true, title: 'Salvar' },
                    }
                },
                calculable: true,
                series: [
                    {
                        name: 'Visão Geral',
                        type: 'pie',
                        radius: '55%',
                        center: ['50%', '60%'],
                        data: seriesDataArray,
                    }
                ]
            });
        });

        function parametrosAjaxData() {
            var obj;
            var arr = [];
            modelo.idsEnviarRequisicaoAjax.forEach(function (itm, idx) {
                //
                if (itm.tipoJsonEnviar == 'tipoNumerico' ||
                    itm.tipoJsonEnviar == 'tipoTexto') {
                    arr[itm.nomeJsonEnviar] =
                        isNullOrEmpty((obj = $('#' + itm.idJsonEnviar).val())) ? (itm.tipoJsonEnviar == 'tipoNumerico' ? "-1" : "") : obj;
                } else if (itm.tipoJsonEnviar == 'tipoCheckBox') {
                    arr[itm.nomeJsonEnviar] = (($('#' + itm.idJsonEnviar).is(':checked')) ? "1" : "0");
                } else if (itm.tipoJsonEnviar == 'tipoName') {
                    arr[itm.nomeJsonEnviar] = ($("[name='" + itm.idJsonEnviar + "']").val());
                } else if (itm.tipoJsonEnviar == 'tipoFuncao') {
                    arr[itm.nomeJsonEnviar] = itm.idJsonEnviar();
                }
            });
            return $.extend(true, {}, arr);
        }
    }
}