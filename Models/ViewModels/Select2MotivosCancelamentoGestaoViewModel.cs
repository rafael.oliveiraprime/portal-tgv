﻿public class Select2MotivosCancelamentoGestaoViewModel
{
    public string idControleInput { get; set; }

    public string idControleSelect2 { get; set; }

    public string clasColInput { get; set; }

    public string clasColSelect { get; set; }

    public string margintopSel { get; set; }

    public string margintopInput { get; set; }

    public bool viewCode { get; set; }

    public bool tipoPesquisa { get; set; }

    public bool miniTitle { get; set; }

    public bool ativo { get; set; }

    public int tipoMotivo { get; set; }

}