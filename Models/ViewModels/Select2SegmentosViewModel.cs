﻿public class Select2SegmentosViewModel
{
    public string idControleInput { get; set; }

    public string idControleSelect2 { get; set; }

    public string clasColInput { get; set; }

    public string clasColSelect { get; set; }

    public bool viewCode { get; set; }

    public bool tipoPesquisa { get; set; }

    public bool ativo { get; set; }
}