﻿public class Select2CondicaoViewModel
{
    public string idControleInput { get; set; }

    public string idControleSelect2 { get; set; }

    public string clasColInput { get; set; }

    public string clasColSelect { get; set; }

    public bool viewCode { get; set; }

    public bool tipoPesquisa { get; set; }
}
