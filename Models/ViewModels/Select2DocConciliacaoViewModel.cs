﻿public class Select2DocConciliacaoViewModel
{
    public string idControleInput { get; set; }

    public string idControleSelect2 { get; set; }

    public string clasColInput { get; set; }

    public string clasColSelect { get; set; }

    public string margintopSel { get; set; }

    public bool viewCode { get; set; }

    public bool tipoPesquisa { get; set; }

    public string clasCor { get; set; }

    public bool verTitulo { get; set; }

    public bool ativo { get; set; }

    public bool multiplo { get; set; }

}
