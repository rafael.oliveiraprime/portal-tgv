﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImportaEstruturaApiModel
{
    public class ImportaEstruturaApiModel
    {
        public Int64 CODCLI { get; set; }

        public string NUMCGCCLI { get; set; }

        public string NOMCLI { get; set; }

        public int CODFILEMP { get; set; }

    }
}