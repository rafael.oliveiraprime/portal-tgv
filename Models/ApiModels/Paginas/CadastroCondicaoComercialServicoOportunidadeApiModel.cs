﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CadastroCondicaoComercialServicoOportunidadeApiModel
{
    public class CadastroCondicaoComercialServicoOportunidadeApiModel
    {
        public int CODCNDCMC { get; set; }

        public string DESCNDCMC { get; set; }

        public decimal VLRDSC { get; set; }

        public decimal PERDSC { get; set; }

        public int CODPRDCES { get; set; }

        public int CODOPTVND { get; set; }

        public int CODSVCTGV { get; set; }

        public int INDSTA { get; set; }

        public List<long> LISTCODCNDCMC { get; set; }        

        public string STATTUSDESC { get; set; }
    }

    public class obterCadastroCondicaoComercialServicoOportunidadeSelect {

        public List<long> LISTCODCNDCMC { get; set; }

    }

    public class obterDadosClienteServico
    {
        public int CODCLI { get; set; }
        public List<long> LISTCODSVCTGV { get; set; }
        
    }

    public class CadastroCondicaoComercialServicoOportunidadeNovoApiModel
    {
        public int CODOPTVND { get; set; }

        public int CODSVC { get; set; }

        public int CODCNDCMC { get; set; }

        public int INDSTA { get; set; }

        public string VLRDSC { get; set; }

        public string PERDSC { get; set; }

        public string DESDSC { get; set; }

        public int INDTIPVGR { get; set; }

        public string DATINIVLD { get; set; }

        public string DATFIMVLD { get; set; }

        public int NUMMESINIVLD { get; set; }

        public int NUMMESFIMVLD { get; set; }

        public string STATTUSDESC { get; set; }
     
        public List<long> LISTVERIFVIGENCIA { get; set; }

        public int CODPRDCES { get; set; }

        public int CODCLI { get; set; }

    }
}