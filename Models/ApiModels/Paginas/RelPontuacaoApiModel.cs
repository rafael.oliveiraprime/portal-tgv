﻿
using System;

namespace RelPontuacaoApiModel
{
    public class obterPontuacaoApiModel
    {
        public int CODCLI { get; set; }
        public string NUMCGCCLI { get; set; }
        public int CODSVCTGV { get; set; }
        public int STATUS { get; set; }
        public string DATAINICIO { get; set; }
        public string DATAFIM { get; set; }
        public string LISTCODSVCTGV { get; set; }
        public string LISTSTATUS { get; set; }
        public string LISTCONSUL { get; set; }
    }

    public class ObterUnicaApiModel
    {
        public string MESANO { get; set; }
    }

}