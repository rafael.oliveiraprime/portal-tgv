﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AprovacaoApiModel
{
    public class CadastroOportunidadeApiModel
    {       
        public int CODOPTVND { get; set; }

        public int CODCLI { get; set; }

        public int INDSTAOPTVND { get; set; }

        public string DESOBSDSC { get; set; }

        public int CODFNCAPV { get; set; }

        public string DATHRAAPV { get; set; }

        public string DATHRARPV { get; set; }

        public  string DESMTVRPV { get; set; }

        public string DESCRIPT { get; set; }

        public int CDFNC { get; set; }

    }
}