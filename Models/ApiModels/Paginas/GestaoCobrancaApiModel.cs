﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestaoCobrancaApiModel
{
    public class GestaoCobrancaApiModel
    {
        public int ANOMESREF { get; set; }
        public int INDFATSVC { get; set; }
        public int CODCLI { get; set; }
        public int NUMTITCOBFAT { get; set; }
        public int CODFILEMP { get; set; }
        public string DATVNCTIT { get; set; }
        public string DATIPRNOTFSCSVC { get; set; }
        public int NUMPCLTITNOTFSCSVC { get; set; }
        public int INDDBTAUTPGT { get; set; }
        public decimal VLRPCLTIT { get; set; }
        public int CODSTATIT { get; set; }
        public int CODRPN { get; set; }
        public int NUMSEQARQPCS { get; set; }
        public string DATPGTTIT { get; set; }
        public decimal VLRSLDTIT { get; set; }
        public int NUMNOTFSCSVC { get; set; }
        public string NUMCGCCLI { get; set; }
        public int STATUS { get; set; }
        public int CODGRPEMPCLI { get; set; }
        public string ANOMESREFP { get; set; }
        public string NOMCLI { get; set; }
        public Int64 NUMLOTPRENOTFSCSVC { get; set; }

    }

    public class GestaoCobrancaTranfResponsavelApiModel
    {
        public Int32 CODRESPONSAVEL { get; set; }
        public List<long> LISTCODCLI { get; set; }
    }

    
    public class EnviarBoletoApiModel
    {
        public int NUMNOTFSCSVC { get; set; }
        public int CODFILEMP { get; set; }
        public int CODEMP { get; set; }
        public string DATIPRNOTFSCSVC { get; set; }
        public string EMAIL { get; set; }
    }

    public class AlteraStatusTitulosApiModel
    {
        public Int32 CODCLI { get; set; }
        public int CODSTATIT { get; set; }
        public List<long> LISTTITULOS { get; set; }
        public string DESOBS { get; set; }
        public string DATPGTTIT { get; set; }

    }

}