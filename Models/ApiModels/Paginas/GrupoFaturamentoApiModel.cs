﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GrupoFaturamentoApiModel
{

    public class GrupoFaturamentoApiModel
    {
        public int CODGRPEMPCLI { get; set; }

        public string NOMGRPEMPCLI { get; set; }

        public int INDFATGRP { get; set; }

        public int CODCLIGRPFAT { get; set; }

        public string CNPJ { get; set; }

        public string RAZAOSOCIAL { get; set; }

        public Int64 CODFNC { get; set; }

        public List<long> LISTCODGRPEMPCLI { get; set; }
    }

    public class GrupoFaturamentoContaCorrenteApiModel
    {
        public int CODGRPEMPCLI { get; set; }

        public int INDDBTAUTPGT { get; set; }

        public int CODBCO { get; set; }

        public int CODAGEBCO { get; set; }

        public string NUMDIGVRFAGE { get; set; }

        public string NUMCNTCRRCTT { get; set; }

        public string NUMDIGVRFCNTCRR { get; set; }

    }

}