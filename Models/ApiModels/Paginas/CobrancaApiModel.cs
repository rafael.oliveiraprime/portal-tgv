﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CobrancaApiModel
{
    public class CobrancaApiModel
    {
        public int ANOMESREF { get; set; }
        public int INDFATSVC { get; set; }
        public int CODCLI { get; set; }
        public int NUMTITCOBFAT { get; set; }
        public int CODFILEMP { get; set; }
        public string DATVNCTIT { get; set; }
        public string DATIPRNOTFSCSVC { get; set; }
        public int NUMPCLTITNOTFSCSVC { get; set; }
        public int INDDBTAUTPGT { get; set; }
        public decimal VLRPCLTIT { get; set; }
        public int CODSTATIT { get; set; }
        public int CODRPN { get; set; }
        public int NUMSEQARQPCS { get; set; }
        public string DATPGTTIT { get; set; }
        public decimal VLRSLDTIT { get; set; }
        public int NUMNOTFSCSVC { get; set; }
    }




}