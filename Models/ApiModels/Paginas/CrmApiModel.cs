﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrmApiModel
{
    public class obterCrmApiModel
    {
        public int CODCLI { get; set; }

        public int CODGRPEMPCLI { get; set; }

        public int CLITGV { get; set; }

        public int CODSTATUS { get; set; }

        public string NUMCGCCLI { get; set; } 
        
        public string NOMCLI { get; set; }      

        public int TIPOBUSCA { get; set; }
    }

    public class obterCrmNegociacao
    {
        public int CODCLI { get; set; }

        public string NUMCGCCLI { get; set; }

        public int TIPOBUSCA { get; set; }
    }

    public class obterEstruturaMartins
    {
        public int CODCLI { get; set; }
    }

    public class obterEstruturaTribanco
    {
        public int CODCLI { get; set; }
    }

    public class obterHistoricoSdm
    {
        public int CODCLI { get; set; }
    }

    public class obterDadosBasicos
    {
        public int CODCLI { get; set; }

        public string TIPENDCLI { get; set; }
    }

    public class obterGrupoEconomico
    {
        public int CODCLI { get; set; }
    }

    public class obterContatosTelevendas
    {
        public int CODCLI { get; set; }
    }

    public class obterCestaProdutoCliente
    {
        public int CODCLI { get; set; }

        public string DATUM { get; set; }
        public string DATDOIS { get; set; }
        public string DATTRES { get; set; }
        public string DATQUATRO { get; set; }
        public string DATCINCO { get; set; }
        public string DATSEIS { get; set; }
        public string DATSETE { get; set; }
        public string DATOITO { get; set; }
        public string DATNOVE { get; set; }
        public string DATDEZ { get; set; }
        public string DATONZE { get; set; }
        public string DATDOZE { get; set; }
        public string DATTREZE { get; set; }
    }

    public class obterGridsEstaticas
    {
        public int CODGRPEMPCLI { get; set; }
        public int CODCLI { get; set; }
    }

}