﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CadastroAnotacaoApiModel
{

    public class CadastroAnotacaoApiModel
    {
        public long CODCLI { get; set; }

        public int NUMSEQOBS { get; set; }

        public string TIPOBSCLI { get; set; }

        public int CODMTVCTO { get; set; }

        public string DESMTVCTO { get; set; }

        public string NOMFNC { get; set; }

        public string DESOBS { get; set; }

        public List<long> LISTDESOBS { get; set; }

        public int NUMNIVGARNGC { get; set; }

        public string DATAGDCTO { get; set; }

        public string HORAGDCTO { get; set; }

        public string HORAGD { get; set; }

        public string DATAGD { get; set; }

        public int CODFNCCAD { get; set; }

        public string DATCAD { get; set; }

        public int CODOPTVND { get; set; }

        public string DATCADFIL { get; set; }

        public string DATAGDFIL { get; set; }

        public int CODCONFIL { get; set; }

        public int TIPANTFIL { get; set; }

        public int MTVANTFIL { get; set; }

        public List<long> LISTTIPOBSCLI { get; set; }
    }
}