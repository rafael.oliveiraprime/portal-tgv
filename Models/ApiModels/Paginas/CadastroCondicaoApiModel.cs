﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CadastroCondicaoApiModel
{

    public class CadastroCondicaoApiModel
    {
        public int CODCNDCMC { get; set; }

        public string DESCNDCMC { get; set; }

        public string VLRDSCPER { get; set; }

        public int TIPDSCPER { get; set; }
        
        public string DATCAD { get; set; }

        public int CODFNCCAD { get; set; }

        public string DATALT { get; set; }

        public int CODFNCALT { get; set; }

        public string DATDST { get; set; }

        public int CODFNCDST { get; set; }

        public int PRDATI { get; set; }

        public int ATIVADO { get; set; }

        public int DESATIVADO { get; set; }

        public List<long> LISTCODCNDCMC { get; set; }
    }

    public class inserirCadastroCondicao
    {
        public string DESCNDCMC { get; set; }     

        public string VLRDSCPER { get; set; }

        public int TIPDSCPER { get; set; }

        public int CODPRDCES { get; set; }   

        public int CODGRPEMPCLI { get; set; }   

        public int CODSVCTGV { get; set; }   

        public int INDTIPVGR { get; set; }   

        public string DATINIVLD { get; set; }   

        public string DATFIMVLD { get; set; }  
        
        public int NUMMESINIVLD { get; set; }   

        public int NUMMESFIMVLD { get; set; } 
        
        public string DESOBSCNDCMC { get; set; }
        
        public string VLRPRDCES { get; set; }   
        
        public string DATCAD { get; set; }

        public int CODFNCCAD { get; set; }                 
    }

    public class alterarCadastroCondicao
    {
        public int CODCNDCMC { get; set; }

        public string DESCNDCMC { get; set; }

        public string VLRDSCPER { get; set; }

        public int TIPDSCPER { get; set; }

        public int CODPRDCES { get; set; }

        public int CODGRPEMPCLI { get; set; }

        public int CODSVCTGV { get; set; }

        public int INDTIPVGR { get; set; }

        public string DATINIVLD { get; set; }

        public string DATFIMVLD { get; set; }

        public int NUMMESINIVLD { get; set; }

        public int NUMMESFIMVLD { get; set; }

        public string DESOBSCNDCMC { get; set; }

        public string VLRPRDCES { get; set; }

        public string DATALT { get; set; }

        public int CODFNCALT { get; set; }
    }
}