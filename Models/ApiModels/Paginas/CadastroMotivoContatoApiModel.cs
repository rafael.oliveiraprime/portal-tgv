﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CadastroMotivoContatoApiModel
{

    public class CadastroMotivoContatoApiModel
    {
        public int CODMTVCTO { get; set; }

        public string DESMTVCTO { get; set; }

        public int CODFNCALT { get; set; }

        public string DATFNCALT { get; set; }

        public int CODFNCDST { get; set; }
        
        public string DATFNCDST { get; set; }

        public string STATUS { get; set; }

        public int MTVCTOATI { get; set; }

        public List<long> LISTMTVCTO { get; set; }

    }
}