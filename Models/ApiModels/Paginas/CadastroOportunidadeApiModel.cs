﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CadastroOportunidadeApiModel
{
    public class CadastroOportunidadeApiModel
    {       
        public int CODOPTVND { get; set; }

        public long CODCLI { get; set; }

        public int CODCLIFIL { get; set; }

        public string NOMCLI { get; set; }

        public string NUMCGCCLI { get; set; }

        public string NUMCGCCLIFIL { get; set; }

        public int CODCNIVNDTGV { get; set; }

        public Int64 NUMSEQPESIND { get; set; }

        public int CODCNLORICLI { get; set; }

        public string NOMPESCTO { get; set; }

        public string NUMTLFCTO { get; set; }

        public string NUMTLFCEL { get; set; }

        public string NOMCTORPNCLI { get; set; }

        public string DESENDETNRPNCLI { get; set; }

        public string STATUS { get; set; }

        public int CODFNCCAD { get; set; }

        public int CODFNCALT{ get; set; }

        public string DATFNCCAD { get; set; }

        public string EMAILESCOLHIDO { get; set; }

        public int CODFNC { get; set; }

        public int CODSVCTGV { get; set; }

        public int INDSTAOPTVND { get; set; }

        public List<int> LISTMARCADOS { get; set; }

        public List<int> LISTSERVICOS { get; set; }

        public string DESCAPROVACAO { get; set; }

        public string NOMECOSULT { get; set; }

        public string DESMTVCNC { get; set; }

        public string DESOBSDSC { get; set; }


        public List<long> LISTCODSVCTGV { get; set; }

        public string VLRSVC { get; set; }

        public string VLRDSC { get; set; }

        public int INDSTANGC { get; set; }

        public int INDSTADSC { get; set; }

        public string PERDSC { get; set; }


        public Int64 CODEDE { get; set; }

        public string NOMCTO { get; set; }

        public string DESENDETNCTO { get; set; }        

        public string DATFNCALT { get; set; }


        public string TIPOBSCLI { get; set; }

        public int CODMTVCTO { get; set; }

        public string DESOBS { get; set; }

        public string DATCAD { get; set; }

        public int NUMNIVGARNGC { get; set; }

        public int QDEPRDVND { get; set; }

        public int CODGRPEMPCLI { get; set; }

        public int CODGRPEMPFAT { get; set; }

        public int CODMTVCNC { get; set; }

        public List<int> LISTEXGRUPFAT { get; set; }

        public string LISTDOCS { get; set; }

        public List<long> CODDOCARZ { get; set; }

        public int CODFNCACT { get; set; }

        public string DESMTVAPV { get; set; }

    }

    public class inserirContatoPadraoOportunidade
    {
        public Int64 CODEDE { get; set; }

        public string NOMCTO { get; set; }

        public string DESENDETNCTO { get; set; }

        public string NUMTLFCEL { get; set; }

        public string NUMTLFCTO { get; set; }

        public int CODFNCCAD { get; set; }

        public int CODFNCALT { get; set; }

        public string DATFNCCAD { get; set; }

        public string DATFNCALT { get; set; }

    }

    public class linhaExcelOptImport
    {
        public int LINHA { get; set; }

        public long CODCLI { get; set; }

        public string MotivoErro { get; set; }

    }

    public class retornoImport
    {
        public int qtdSucesso { get; set; }

        public List<linhaExcelOptImport> listErro { get; set; }

    }
}