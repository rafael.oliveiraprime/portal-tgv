﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CadastroTipoContatoApiModel
{

    public class CadastroTipoContatoApiModel
    {
        public int CODTIPCTO { get; set; }

        public string DESTIPCTO { get; set; }

        public int CODFNCALT { get; set; }

        public string DATFNCALT { get; set; }

        public int CODFNCDST { get; set; }

        public string DATFNCDST { get; set; }

        public string STATUS { get; set; }

        public int TIPCTOATI { get; set; }

        public List<long> LISTCODTIPCTO { get; set; }
    }
}