﻿using System.Collections.Generic;

namespace CadastroTipoParceiroApiModel
{
    public class CadastroTipoParceiroApiModel
    {
        public int CODPAC { get; set; }

        public string NOMPAC { get; set; }

        public int CODFNCDST { get; set; }

        public string DATFNCDST { get; set; }

        public string STATUS { get; set; }

        public int TIPCTOATI { get; set; }

        public List<long> LISTCODPAC { get; set; }
    }
}