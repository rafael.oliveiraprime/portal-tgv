﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConsultaCestaServicoApiModel
{
    public class ConsultaCestaServicoApiModel
    {
        public int CODCLI { get; set; }
        public string NOMCLI { get; set; }
        public int ANOMESREF { get; set; }
        public int CODPRDCES { get; set; }
        public string DESPRDCES { get; set; }
        public string VLRMOVSVCPRDSMA { get; set; }
        public int CODFNCALT { get; set; }
        public string DATATU { get; set; }
    } 
}