﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CadastroOportunidadeGeralApiModel
{
    public class CadastroOportunidadeGeralApiModel
    {
        public int CODOPTVND { get; set; }

        public int CODCLI { get; set; }

        public int CODCLIFIL { get; set; }

        public string NOMCLIFIL { get; set; }

        public int CODEDE { get; set; }

        public string NOMCLI { get; set; }

        public string NUMCGCCLI { get; set; }

        public string NUMCGCCLIFIL { get; set; }

        public int CODCNIVNDTGV { get; set; }

        public Int64 NUMSEQPESIND { get; set; }

        public string NOMPESCTO { get; set; }

        public string STATUS { get; set; }

        public int CODFNCCAD { get; set; }

        public string DATFNCCAD { get; set; }

        public string DATAGDCTO { get; set; } 

        public int INDSTAOPTVND { get; set; }

        public int INDSTAOPTVNDFIL { get; set; }

        public int CODCNLORICLIFIL { get; set; }

        public Int64 NUMSEQPESINDFIL { get; set; }

        public string DATAGDCTOFIL { get; set; }

        public int CODCNIVNDTGVFIL { get; set; }


        public Int64 NUMSEQPESINDPES { get; set; }

        public string TIPFNCRPNPES { get; set; }

        public string MATRICULAPES { get; set; }

        public string NOMPESCTOPES { get; set; }

        public string NUMCPFPES { get; set; }

        public string NUMTLFPES { get; set; }

        public string NUMTLFCELPES { get; set; }

        public string DESENDETNPES { get; set; }

        public string DESCGRPES { get; set; }

        public string NOMEMPPES { get; set; }

                
        public int CODFNCALTRPN { get; set; }

        public List<long> LISTCODOPTVND { get; set; }


        public int TIPOBUSCA { get; set; }

        public int CODPFLACS { get; set; }
    }


    public class ImportaNegociacaoApiModel
    {
        public string[][] DATA_XLSX { get; set; }

        public List<int> CONSULTORES {get; set; }

        public List<string> NOMESCONSULTORES { get; set; }

    }

}