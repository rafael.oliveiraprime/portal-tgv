﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CadastroPessoaIndicacaoApiModel
{
    public class CadastroPessoaIndicacaoApiModel
    {
        public Int64 NUMSEQPESIND { get; set; }

        public string TIPFNCRPN { get; set; }

        public string INDICADOR { get; set; }

        public int CODFNC { get; set; }

        public Int64 CODFNCFIL { get; set; }

        public int CODFNCTRI { get; set; }
        
        public string NOMFNC { get; set; }

        public string NUMCPFFNC { get; set; }

        public string NUMCPFVER { get; set; }

        public string NUMCPFFIL { get; set; }

        public string DESCGR { get; set; }

        public string NOMEMP { get; set; }

        public int CODREP { get; set; }

        public string NOMREP { get; set; }

        public string NUMCPFREP { get; set; }     

        public string NUMCPF { get; set; }

        public string NOMPESCTO { get; set; }

        public int MATRICULA { get; set; }

        public string DESENDETN { get; set; }

        public string NUMTLF { get; set; }

        public string NUMTLFCEL { get; set; }               

        public string DATFNCCAD { get; set; }

        public int CODFNCCAD { get; set; }

        public string DATFNCALT { get; set; }

        public int CODFNCALT { get; set; }

        public string DATFNCDST { get; set; }

        public int CODFNCDST { get; set; }

        public string STATUS { get; set; }

        public int CPIATI { get; set; }    
        
        public List<long> LISTNUMSEQPESIND { get; set; }
    }
}