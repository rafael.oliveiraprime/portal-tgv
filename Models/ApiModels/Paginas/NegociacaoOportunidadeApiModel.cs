﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NegociacaoOportunidadeApiModel
{
    public class NegociacaoOportunidadeApiModel
    {
        public int CODOPTVND { get; set; }

        public int CODCLI { get; set; }

        public int CODCLIFIL { get; set; }

        public int CODEDE { get; set; }

        public string NOMCLI { get; set; }

        public string NUMCGCCLI { get; set; }

        public string NUMCGCCLIFIL { get; set; }

        public int CODCNIVNDTGV { get; set; }

        public Int64 NUMSEQPESIND { get; set; }

        public string NOMPESCTO { get; set; }

        public string STATUS { get; set; }

        public int CODFNCCAD { get; set; }

        public string DATFNCCAD { get; set; }

        public string NUMTEL { get; set; }

        public string NUMTELCEL { get; set; }


        public string TIPFNCRPN { get; set; }

        public string INDICADOR { get; set; }

        public int CODFNC { get; set; }

        public string NOMFNC { get; set; }

        public int CODREP { get; set; }

        public string NOMREP { get; set; }

        public string NUMCPF { get; set; }
        

        public Int64 NUMSEQPESINDPES { get; set; }

        public string TIPFNCRPNPES { get; set; }

        public string MATRICULAPES { get; set; }

        public string NOMPESCTOPES { get; set; }

        public string NUMCPFPES { get; set; }        

        public string NUMTLFPES { get; set; }

        public string NUMTLFCELPES { get; set; }

        public string DESENDETNPES { get; set; }


        public int TIPENDCLI { get; set; }        

        public string TAMAREFISVNDCLI { get; set; }

        public string TAMAREFISDPSCLI { get; set; }

        public string ENDERECO { get; set; }

        public string COMPLEMENTO { get; set; }

        public string CODCEP { get; set; }

        public string NOMBAI { get; set; }

        public string NOMCID { get; set; }

        public string CODESTUNI { get; set; }

        public string CANAL { get; set; }

        public string SEGMENTOMERCADO { get; set; }

        public string UNIDADENEGOCIO { get; set; }


        public int CODCNDCMC { get; set; }

        public int CODTIPCTO { get; set; }
    }

}