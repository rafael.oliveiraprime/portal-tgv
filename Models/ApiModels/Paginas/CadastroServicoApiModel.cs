﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CadastroServicoApiModel
{

    public class CadastroServicoApiModel
    {
        public int CODSVCTGV { get; set; }

        public string DESSVCTGV { get; set; }

        public string DESTIPSVC { get; set; }

        public int TIPSVC { get; set; }

        public string VLRSVC { get; set; }

        public string NUMPTOSVC { get; set; }

        public string STATUS { get; set; }

        public int PRDATI { get; set; }

        public int CODFNCCAD { get; set; }

        public int CODFNCALT { get; set; }

        public int CODFNCDST { get; set; }

        public string DATFNCCAD { get; set; }

        public string DATFNCALT { get; set; }

        public string DATFNCDST { get; set; }

        public int ATIVADO { get; set; }

        public int DESATIVADO { get; set; }

        public List<long> LISTCODSVC { get; set; }

        public string CDOANXCTTCLI { get; set; }

        public string CODDOCS { get; set; }

    }
}