﻿
using System;

namespace RelConciliacaoApiModel
{
    public class obterRelConciliacaoApiModel
    {
        public int CODCLI { get; set; }

        public string NOMCLI { get; set; }

        public string NUMCGCCLI { get; set; }

        public int INDSTAAPV { get; set; }

        public string DATENVDOCINI { get; set; }

        public string FILDAT { get; set; }

        public string DATENVDOCFIM { get; set; }
    }



}