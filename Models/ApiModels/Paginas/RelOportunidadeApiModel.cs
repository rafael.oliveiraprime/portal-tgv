﻿
using System;

namespace RelOportunidadeApiModel
{
    public class obterRelOportunidadeApiModel
    {
        public int CODCLI { get; set; }
        public string NUMCGCCLI { get; set; }
        public int CODSVCTGV { get; set; }
        public int STATUS { get; set; }
        public string DATAINICIO { get; set; }
        public string DATAFIM { get; set; }
        public int CODCNIVNDTGV { get; set; }
        public string MESANO { get; set; }
    }



}