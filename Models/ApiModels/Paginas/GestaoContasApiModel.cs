﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestaoContasApiModel
{
    public class GestaocontasApiModel
    {
        public int CODCLI { get; set; }

        public string NUMCGCCLI { get; set; }

        public int CODGRPEMPCLI { get; set; }

        public string NOMCIDEXD { get; set; }

        public string CodEstUni { get; set; }

        public int CODSITCLI { get; set; }

        public int TIPOBUSCA { get; set; }

        public int CODCLIEXT { get; set; }

        public string NOMCLI { get; set; }

    }

    public class GestaocontasServicoApiModel
    {
        public int CODCLI { get; set; }
    }


    public class AcoesServicoGestaoContasApiModel
    {
        public int CODCLI { get; set; }

        public int CODSVCTGV { get; set; }

        public List<long> LISTSERVICOS { get; set; }

        public int TIPOACAO { get; set; }

        public string DESOBS { get; set; }

        public string DATATVSVC { get; set; }

        public int CODFNCATV { get; set; }

        public string DATINISUSSVC { get; set; }

        public string DATFIMSUSSVC { get; set; }

        public int CODFNCSUS { get; set; }

        public string DATCNC { get; set; }

        public int CODMTVCNC { get; set; }

        public int CODFNCCNC { get; set; }

        public string INDSTAATV { get; set; }

        public decimal PERCOBMESATVSVC { get; set; }

        public decimal PERCOBMESCNCSVC { get; set; }

    }

    public class ServicoDescontosGestaodeContasApiModel
    {
        public int CODCLI { get; set; }

        public int CODSVCTGV { get; set; }

        public int CODCNDCMC { get; set; }

        public string PERDSC { get; set; }

        public string VLRDSC { get; set; }

        public string DATINIVLD { get; set; }

        public string DATFIMVLD { get; set; }

        public string DSOBS { get; set; }

        public string DESDSC { get; set; }

        public List<long> LISTCONDICOES { get; set; }

        public List<long> LISTVERIFVIGENCIA { get; set; }

        public int CODPRDCES { get; set; }
    }

    public class GestaoContasContaCorrenteApiModel
    {
        public int CODCLI { get; set; }

        public int INDDBTAUTPGT { get; set; }

        public int CODBCO { get; set; }

        public int? CODAGEBCO { get; set; }

        public string NUMDIGVRFAGE { get; set; }

        public string NUMCNTCRRCTT { get; set; }

        public string NUMDIGVRFCNTCRR { get; set; }

    }

    public class GestaoContasContaAlterarQuantidadeApiModel
    {
        public int CODCLI { get; set; }

        public List<long> LISTSERVICOS { get; set; }

        public int CODSVCTGV { get; set; }

        public string DESOBS { get; set; }

        public int QDEPRDVND { get; set; }


    }

    public class GestaoContasContaInserirServicoAvulsoApiModel
    {

        public Int64 ANOMESREF { get; set; }

        public Int64 CODCLI { get; set; }

        public string DESSVCTGV { get; set; }
        public string VLRSVC { get; set; }

        public int INDFATSVC { get; set; }

        public string VLRPCLTIT { get; set; }
        public string DATVNCNOTFSCSVC { get; set; }

        public string VLRPCLTIT2 { get; set; }
        public string DATVNCNOTFSCSVC2 { get; set; }

        public string VLRPCLTIT3 { get; set; }
        public string DATVNCNOTFSCSVC3 { get; set; }

        public string VLRPCLTIT4 { get; set; }
        public string DATVNCNOTFSCSVC4 { get; set; }

        public string VLRPCLTIT5 { get; set; }
        public string DATVNCNOTFSCSVC5 { get; set; }

        public string VLRPCLTIT6 { get; set; }
        public string DATVNCNOTFSCSVC6 { get; set; }

        public string DESJSTSLCSVC { get; set; }

        public Int64 NUMLOTPRENOTFSCSVC { get; set; }

        public Int64 CODFNCGRC { get; set; }


    }

    public class GestaoContasContaAlterarGrupoFatApiModel
    {
        public int CODCLI { get; set; }

        public int CODSVCTGV { get; set; }

        public int CODGRPEMPFAT { get; set; }

    }

    public class GestaoContasContaAlterarAutomacaoApiModel
    {
        public int CODCLI { get; set; }

        public int CODEMPAUT { get; set; }

    }


}