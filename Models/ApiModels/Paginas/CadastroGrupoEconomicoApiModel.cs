﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CadastroGrupoEconomicoApiModel
{

    public class CadastroGrupoEconomicoApiModel
    {
        public int CODGRPEMPCLI { get; set; }

        public string NOMGRPEMPCLI { get; set; }

        public int INDFATGRP { get; set; }

        public int CODCLIGRPFAT { get; set; }

        public string CNPJ { get; set; }

        public string RAZAOSOCIAL { get; set; }

        public Int64 CODFNC { get; set; }

        public List<long> LISTCODGRPEMPCLI { get; set; }
    }

    public class CadastroGrupoEconomicoMartinsApiModel
    {
        public Int64 CODCLI { get; set; }

        public string NOMCLI { get; set; }

        public string CNPJ { get; set; }

        public int GRUPREDE { get; set; }

        public int TIPOBUSCAMODAL { get; set; }

    }


    public class CadastroGrupoEconomicoXClientesApiModel
    {
        public int CODGRPEMPCLI { get; set; }

        public Int64 CODCLI { get; set; }

        public List<long> CODCLIENTES { get; set; }


        public string NOMGRPEMPCLI { get; set; }

        public int INDFATGRP { get; set; }

        public int CODCLIGRPFAT { get; set; }       
    }



}