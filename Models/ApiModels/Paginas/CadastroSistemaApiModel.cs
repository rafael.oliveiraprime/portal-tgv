﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CadastroSistemaApiModel
{

    public class CadastroSistemaApiModel
    {
        public int CODEMPAUT { get; set; }

        public int CODSISINF { get; set; }

        public string NOMSIS { get; set; }

        public int CODSGM { get; set; }

        public int INDSISHML { get; set; }

        public string NOMSISOPE { get; set; }

        public int TIPBBLSIS { get; set; }        
    }
}