﻿
using System;
using System.Collections.Generic;


namespace RelVencimentoPagamentoApiModel
{
    public class ObterVencimentoPagamento
    {

        public Int64 CODCLI { get; set; }

        public string NOMCLI { get; set; }

        public string CNPJ { get; set; }

        public int CODSITCLI { get; set; }        

        public string DATVNCTIT { get; set; }

        public string DATPGTTIT { get; set; }

        public string DATPGTTITINI { get; set; }

        public string DATPGTTITFIM { get; set; }

        public List<long> LISTCODSTATIT { get; set; }

        public string DATIMPRESSINI { get; set; }

        public string DATIMPRESSFIM { get; set; }

    }
}