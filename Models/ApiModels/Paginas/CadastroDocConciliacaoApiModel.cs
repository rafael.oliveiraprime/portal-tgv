﻿using System.Collections.Generic;

namespace CadastroDocConciliacaoApiModel
{

    public class CadastroDocConciliacaoApiModel
    {
        public int CODDOCARZ { get; set; }

        public string DESDOCARZ { get; set; }

        public int TIPDOC { get; set; }

        public int INDENV { get; set; }

        public string CDOANXDOCARZ { get; set; }

        public string DATCAD { get; set; }

        public int CODFNCCAD { get; set; }

        public string DATALT { get; set; }

        public int CODFNCALT { get; set; }

        public string DATDST { get; set; }

        public int CODFNCDST { get; set; }

        public int ATIVADO { get; set; }

        public int DESATIVADO { get; set; }

        public List<long> LISTCODDOC { get; set; }
    }

    public class obterDocRelacaoServicoApiModel
    {
        public int CODSVCTGV { get; set; }
    }


}