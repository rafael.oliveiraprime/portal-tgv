﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CadastroAutomacaoApiModel
{

    public class CadastroAutomacaoApiModel
    {
        public int CODEMPAUT { get; set; }

        public int CODEMPAUTSIS { get; set; }

        public string RAZSOCEMP { get; set; }

        public string NOMFNT { get; set; }

        public string NUMCGCEMP { get; set; }

        public string DESENDLGR { get; set; }

        public int CODPAC { get; set; }

        public int CODFNCCAD { get; set; }

        public int CODFNCALT { get; set; }

        public int CODFNCDST { get; set; }

        public string DATFNCCAD { get; set; }

        public string DATFNCALT { get; set; }

        public string DATFNCDST { get; set; }

        public int AUTATI { get; set; }

        public int AUTATIFIL { get; set; }

        public string STATUS { get; set; }

        public List<long> LISTCODEMPAUT { get; set; }

        public int CODCLI { get; set; }

        public string NOMCLI { get; set; }

        public string NUMCGCCLI { get; set; }

    }
}