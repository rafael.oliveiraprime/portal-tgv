﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CadastroCestaServicoApiModel
{

    public class CadastroCestaServicoApiModel
    {

        public int CODPRDCES { get; set; }

        public string DESPRDCES { get; set; }

        public int NUMSEQ { get; set; }

        public string TIPDDOATR { get; set; }

        public int FLGIPDSVC { get; set; }

        public string DATALT { get; set; }

        public string DATDST { get; set; }

        public int CODFNCCAD { get; set; }

        public int CODFNCALT { get; set; }

        public int CODFNCDST { get; set; }

        public int ATIVADO { get; set; }

        public int DESATIVADO { get; set; }

        public List<long> LISTCODPRDCES { get; set; }
    }
}