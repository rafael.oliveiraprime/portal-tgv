﻿
using System;

namespace RelcobrancaApiModel
{
    public class obterCobrancaApiModel
    {
        public int CODCLI { get; set; }

        public string ANOMES { get; set; }

        public int STATUS { get; set; }

        public int INDICADOR { get; set; }
    }


    public class AtualizarLinkBoletoTitApiModel
    {
        public int ANOMESREF { get; set; }
        public int INDFATSVC { get; set; }
        public Int64 CODCLI { get; set; }
        public Int64 NUMTITCOBFAT { get; set; }
        public int CODFILEMP { get; set; }
        public string DESCHVFAT { get; set; }
    }

}