﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CadastroConsultorApiModel
{
    public class CadastroConsultorApiModel
    {
        public int CODFNC { get; set; }

        public int CODPFLACS { get; set; }

        public int CODFNCFIL { get; set; }

        public int CONATIFIL { get; set; }

        public int CONATI { get; set; }

        public int CODFNCCAD { get; set; }

        public int CODFNCALT { get; set; }

        public int CODFNCDST { get; set; }

        public string NOMFNC { get; set; }

        public List<long> LISTCODFNC { get; set; }
    }
}