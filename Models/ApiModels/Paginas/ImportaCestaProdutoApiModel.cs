﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImportaCestaProdutoApiModel
{
    public class ImportaCestaProdutoApiModel
    {
        public Int64 CODCLI { get; set; }

        public List<string> LISTCODCLI { get; set; }

        public int ANOMESREF { get; set; }

        public List<string> LISTANOMESREF { get; set; }

        public int CODPRDCES { get; set; }

        public List<string> LISTCODPRDCES { get; set; }

        public string VLRPRDCES { get; set; }

        public List<string> LISTVLRPRDCES { get; set; }

        public int CODFNCALT { get; set; }

        public string DATATU { get; set; }

        public string[][] DATA_XLSX { get; set; }

        public string COLUNA { get; set; }

        public int LINHA { get; set; }

        public int PROBLEMA { get; set; }

        public int DELETAR { get; set; }

        //public string [] LISTPROBLEMA { get; set; }

        public List<string> LISTPROBLEMA { get; set; }

        public List<int> LISTVERIFCODCLI { get; set; }

    }    
}