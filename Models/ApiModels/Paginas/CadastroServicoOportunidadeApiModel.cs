﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CadastroServicoOportunidadeApiModel
{
    public class CadastroServicoOportunidadeApiModel
    {
        public int CODOPTVND { get; set; }

        public int? CODSVCTGV { get; set; }

        public List<long> LISTCODSVCTGV { get; set; }

        public List<long> LISTCODSVCTGVSEL { get; set; }

        public string NOMSVC { get; set; }

        public string VLRSVC { get; set; }

        public string VLRSVCTOT { get; set; }

        public string VLRSVCTOTGER { get; set; }

        public string DESTIPSVC { get; set; }

        public int NUMPTOSVC { get; set; }

        public string VLRDSC { get; set; }

        public string PERDSC { get; set; }

        public int INDSTANGC { get; set; }

        public string NOMSTATNGC { get; set; }

        public int INDSTADSC { get; set; }

        public int INDSTAOPTVND { get; set; }

        public string NOMSTATDSC { get; set; }

        public string DESENDETN { get; set; }

        public string NOMARQDOC { get; set; }

        public string NOMARQORI { get; set; }

        public string CONTRATOBANCO { get; set; }

        public int INDSVCVND { get; set; }

        public int QDEPRDVND { get; set; }
        
        public int INDFATGRP { get; set; }

        public int CODGRPEMPCLI { get; set; }

        public long CODGRPEMPFAT { get; set; }

        public int CODDOCARZ { get; set; }

        public List<long> LSTCODDOCARZ { get; set; }

        public string CDOANXDOCARZASN { get; set; }

    }

    public class DocumentoAlteradoApiModel
    {
        public int CODOPTVND { get; set; }
        public int CODSVCTGV { get; set; }
        public int CODDOCARZ { get; set; }
        public string CDOANXDOCARZASN { get; set; }
        public int INDSTADSC { get; set; }
    }
}