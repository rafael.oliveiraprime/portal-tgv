﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FaturamentoApiModel
{
    public class ParametrosFaturamentoApiModel
    {
        public int ANOMESREF { get; set; }

        public int INDFATSVC { get; set; }

        public string DATINIPCS { get; set; }

        public string DATFIMPCS { get; set; }

        public int CODFNCGRC { get; set; }

        public string DATENVFAT { get; set; }

        public int CODFNCFAT { get; set; }

        public Int64 NUMLOTPRENOTFSCSVC { get; set; }

        public string ANOMESREFPASS { get; set; }

    }

    public class obterDemonstrativoFaturamentoApiModel
    {
        public string ANOMESREF { get; set; }

        public string ANOMESREFMODFIL { get; set; }

        public string ANOMESREFPASS { get; set; }

        public string ANOMESREFPASSMODFIL { get; set; }

        public int VISAO { get; set; }

        public int TIPOVISAO { get; set; }

        public int VISAOMODFIL { get; set; }

        public int ITENSVALIDADOS { get; set; }

        public int TIPODIVERGENCIA { get; set; }

        public int STATUS { get; set; }

        public int STATUSMODFIL { get; set; }

        public int TIPOBUSCA { get; set; }

        public int CODSVCTGVMODFIL { get; set; }

        public Int64 CODCLIMODFIL { get; set; }

        public Int64 NUMCGCCLIMODFIL { get; set; }
        
        public string NOMCLIMODFIL { get; set; }

        public Int64 ANOMESVIGENTE { get; set; }

        public Int64 ANOMESVIGENTEPASS { get; set; }

    }


    public class cancelarLoteFaturamentoApiModel
    {
        public string ANOMESREF { get; set; }
    }

    public class validarLoteFaturamentoApiModel
    {
        public List<string>  LISTVALIDADOS { get; set; }

        public int INDVLRFATVLD { get; set; }

        public int ANOMESREF { get; set; }

        public int CODCLI { get; set; }

        public int CODSVCTGV { get; set; }

    }

    public class obterEmailFaturamento
    {
        public int TIPOBUSCAEMAIL { get; set; }
    }

    public class enviarLoteparaFaturamento
    {
        public int QTDTOTALCLI { get; set; }
        public string VALORTOTAL { get; set; }
        public int ANOMESREF { get; set; }
        public Int64 NUMLOTPRENOTFSCSVC { get; set; }
    }


}