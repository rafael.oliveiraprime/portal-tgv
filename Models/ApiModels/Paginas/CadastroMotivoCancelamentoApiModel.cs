﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CadastroMotivoCancApiModel
{
    public class CadastroMotivoCancApiModel
    {
        public int CODMTVCNC { get; set; }

        public string DESMTVCNC { get; set; }

        public int CODFNCALT { get; set; }

        public string DATFNCALT { get; set; }

        public int CODFNCDST { get; set; }

        public string DATFNCDST { get; set; }

        public string STATUS { get; set; }

        public int MTVCNCATI { get; set; }

        public List<long> LISTMTVCNC { get; set; }

        public int INDCNCATI { get; set; }


    }
}