﻿
using System;

namespace RelIndicacoesApiModel
{
    public class obterIndicacoesApiModel
    {
        public int CODCLI { get; set; }

        public string NUMCGCCLI { get; set; }

        public int CODSVCTGV { get; set; }

        public Int64 NUMSEQPESIND { get; set; }

        public string DATCAD { get; set; }

        public int STATUS { get; set; }

        public string DATCADINI { get; set; }

        public string DATCADFIM { get; set; }

    }
}