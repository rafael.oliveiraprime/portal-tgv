﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CondicoesComerciasApiModel
{
    public class CondicoesComerciasApiModel
    {
        public int CODCNDCMC { get; set; }

        public string DESCNDCMC { get; set; }

        public int ATIVO { get; set; }

        public int CODSVCTGV { get; set; }

        public int CODCLI { get; set; }

        public int CODOPTVND { get; set; }

        public int TIPOCONSULTA { get; set; }        
    }
}