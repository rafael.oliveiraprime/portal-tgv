﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TiposContatosApiModel
{
    public class TiposContatosApiModel
    {
        public int CODTIPCTO { get; set; }

        public string DESTIPCTO { get; set; }

        public int TIPCTOATI { get; set; }

        public int ATIVO { get; set; }
    }
}