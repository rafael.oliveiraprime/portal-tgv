﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotivosContatosApiModel
{
    public class MotivosContatosApiModel
    {
        public int CODMTVCTO { get; set; }

        public string DESMTVCTO { get; set; }

        public int MTVCTOATI { get; set; }

        public int ATIVO { get; set; }
    }
}