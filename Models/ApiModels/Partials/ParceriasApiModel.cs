﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParceriasApiModel
{
    public class ParceriasApiModel
    {
       public int CODPAC { get; set; }

       public string NOMPAC { get; set; }

       public int ATIVO { get; set; }
    }
}