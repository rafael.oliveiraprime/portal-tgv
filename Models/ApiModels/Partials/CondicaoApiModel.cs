﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CondicaoApiModel
{     
    public class CondicaoApiModel
    {
        public string CODCNDCMC { get; set; }

        public string DESCNDCMC { get; set; }

        public string VLRDSC { get; set; }

        public string PERDSC { get; set; }
    }
}