﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemasCodigosAutomacoesApiModel
{
    public class SistemasCodigosAutomacoesApiModel
    {
        public int CODSISINF { get; set; }

        public string NOMSIS { get; set; }

        public int CODEMPAUT { get; set; }
    }
}