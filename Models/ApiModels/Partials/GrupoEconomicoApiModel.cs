﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GrupoEconomicoApiModel
{
    public class GrupoEconomicoApiModel
    {
        public Int64 CODGRPEMPCLI { get; set; }

        public string NOMGRPEMPCLI { get; set; }
    }
}