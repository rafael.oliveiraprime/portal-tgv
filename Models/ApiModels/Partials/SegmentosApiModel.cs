﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SegmentosApiModel
{
    public class SegmentosApiModel
    {
        public int CODSGM { get; set; }

        public string DESSGM { get; set; }

        public int ATIVO { get; set; }
    }
}