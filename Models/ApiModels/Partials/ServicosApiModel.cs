﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServicosApiModel
{     
    public class ServicosApiModel
    {
        public int CODSVCTGV {get; set;}

        public string DESSVCTGV {get; set;}

        public int ATIVO {get; set;}       
    }
}