﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PessoasIndicacoesApiModel
{
    public class PessoasIndicacoesApiModel
    {
        public Int64 NUMSEQPESIND { get; set; }

        public string NOMPESCTO { get; set; }

        public int ATIVO { get; set; }
    }
}