﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConsultoresApiModel
{
    public class ConsultoresApiModel
    {
        public int CODFNC { get; set; }

        public string NOMFNC { get; set; }

        public int ATIVO { get; set; }
    }
}