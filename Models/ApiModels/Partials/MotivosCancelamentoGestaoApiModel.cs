﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotivosCancelamentoGestaoApiModel
{
    public class MotivosCancelamentoGestaoApiModel
    {        
        public int CODMTVCNC { get; set; }

        public string DESMTVCNC { get; set; }

        public int MTVCTOATI { get; set; }

        public int ATIVO { get; set; }
    }
}