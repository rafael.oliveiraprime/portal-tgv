﻿using System.ComponentModel.DataAnnotations;

namespace ControleLogModel
{
    public class ControleLog
    {
        public int CODSISINF { get; set; }
        public int NUMERR { get; set; }
        public string DATCAD { get; set; }
        public int CODFNC { get; set; }
        public string DESMDO { get; set; }
        public string DESPMTSIS { get; set; }
        public string DESCAMERR { get; set; }
        public string DESERRRDC { get; set; }
        public string DESERRDDO { get; set; }
    }

}