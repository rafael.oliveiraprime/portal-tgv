﻿using Microsoft.VisualBasic;
using System.DirectoryServices.Protocols;
using System.Net;
using System.DirectoryServices.AccountManagement;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using System;
using System.Security.Cryptography.X509Certificates;
using System.Linq;

public class ControleDeAcesso
{

    private static string _CAMINHO_LDAP = null;
    public static string CAMINHO_LDAP
    {
        get
        {
            if (string.IsNullOrEmpty(_CAMINHO_LDAP))
            {
                _CAMINHO_LDAP = System.Convert.ToString(ConfigurationManager.AppSettings["CAMINHO_LDAP"].ToString());
            }
            return _CAMINHO_LDAP;
        }
    }

    private static int _PORTA_LDAP = -1;
    public static int PORTA_LDAP
    {
        get
        {
            if (_PORTA_LDAP == -1)
            {
                _PORTA_LDAP = System.Convert.ToInt32(Convert.ToInt32(ConfigurationManager.AppSettings["PORTA_LDAP"].ToString()));
            }
            return _PORTA_LDAP;
        }
    }

    private static string _DISTINGUISHEDNAME_LDAP = null;
    public static string DISTINGUISHEDNAME_LDAP
    {
        get
        {
            if (string.IsNullOrEmpty(_DISTINGUISHEDNAME_LDAP))
            {
                _DISTINGUISHEDNAME_LDAP = System.Convert.ToString(ConfigurationManager.AppSettings["DISTINGUISHEDNAME_LDAP"].ToString());
            }
            return _DISTINGUISHEDNAME_LDAP;
        }
    }
    private static string _GRUPOSAUTORIZADOS = null;
    public static string GRUPOSAUTORIZADOS
    {
        get
        {
            if (string.IsNullOrEmpty(_GRUPOSAUTORIZADOS))
            {
                _GRUPOSAUTORIZADOS = System.Convert.ToString(ConfigurationManager.AppSettings["GRUPOSAUTORIZADOS"].ToString());
            }
            return _GRUPOSAUTORIZADOS;
        }
    }

    private static int _MODODESENVOLVIMENTO = -1;
    public static int MODODESENVOLVIMENTO
    {
        get
        {
            if (_MODODESENVOLVIMENTO == -1)
            {
                _MODODESENVOLVIMENTO = System.Convert.ToInt32(Convert.ToInt32(ConfigurationManager.AppSettings["MODODESENVOLVIMENTO"]));
            }
            return _MODODESENVOLVIMENTO;
        }
    }



    public static ConjuntoDePermissoesUsuario ObterConjuntoDePermissoesUsuario(string nomUsrRde = "", List<string> grupos = null)
    {
        var Session = HttpContext.Current.Session;
        ConjuntoDePermissoesUsuario retorno = new ConjuntoDePermissoesUsuario();
        //
        if (ReferenceEquals(Session, null) || ReferenceEquals(Session["ConjuntoDePermissoesUsuario"], null))
        {
            if (!nomUsrRde.Equals(string.Empty))
            {
                var bll = new ControleDeAcessoBLL();
                //
                retorno.InformacoesUsuario = bll.ObterInformacoesUsuario(nomUsrRde).FirstOrDefault();
                if (ReferenceEquals(grupos, null))
                {
                    grupos = new List<string>();
                }
                //
            }
        }
        else
        {
            if (nomUsrRde.Equals(string.Empty))
            {
                retorno = (ConjuntoDePermissoesUsuario)(Session["ConjuntoDePermissoesUsuario"]);
            }
            else
            {
                Session.Clear();
                Session.Abandon();
                return ObterConjuntoDePermissoesUsuario(nomUsrRde);
            }
        }
        //
        if (retorno.InformacoesUsuario != null)
        {
            Session["ConjuntoDePermissoesUsuario"] = retorno;
        }
        //
        return retorno;
    }

    public static List<string> AutenticaUsuario(string usuario, string senha)
    {
        List<string> retorno = new List<string>();
        try
        {
            if (MODODESENVOLVIMENTO != 1)
            {
                LdapConnection con = new LdapConnection(new LdapDirectoryIdentifier(CAMINHO_LDAP, PORTA_LDAP));
                con.SessionOptions.ProtocolVersion = 3;
                //
                con.SessionOptions.VerifyServerCertificate = new VerifyServerCertificateCallback((LdapConnection l, X509Certificate x) => { return true; });
                //
                con.Credential = new NetworkCredential(usuario, senha);
                con.AuthType = AuthType.Negotiate;
                //
                var ldapFilter = string.Format("(&(objectCategory=person)(objectClass=user)(&(sAMAccountName={0})))", usuario);
                //
                var getUserRequest = new SearchRequest(DISTINGUISHEDNAME_LDAP, ldapFilter, SearchScope.Subtree, new[] { "MemberOf" })
                {
                    SizeLimit = 1
                };
                //
                var SearchControl = new SearchOptionsControl(SearchOption.DomainScope);
                getUserRequest.Controls.Add(SearchControl);
                //
                var userResponse = (SearchResponse)(con.SendRequest(getUserRequest));
                retorno.AddRange(ObterGruposRedeUsuario(userResponse.Entries[0]));
                //
            }
            else
            {
                return default(List<string>);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("{0} Exception caught.", e);
            return new List<string>();
        }
        return retorno;
    }

    public static string[] ObterGruposRedeUsuario(SearchResultEntry usuario)
    {
        string grupos = "";
        string separador = "";
        for (int x = 0; x <= usuario.Attributes["MemberOf"].Count - 1; x++)
        {
            grupos += separador + usuario.Attributes["MemberOf"][x].ToString().Split(',')[0].Replace("CN=", "");
            separador = ",";
        }
        return grupos.ToUpper().Split(System.Convert.ToChar(Convert.ToChar(",")));
    }

    public static bool VerificarGrupoRedeUsuario(List<string> grupos)
    {
        if (MODODESENVOLVIMENTO == 1)
        {
            return true;
        }
        //
        bool encontrouGrupoAutorizado = false;
        //
        var arrGrps = new List<string>();
        //
        var arrGrpsPontoVirgula = ControleDeAcesso.GRUPOSAUTORIZADOS.Split(';');
        var arrGrpsVirgula = ControleDeAcesso.GRUPOSAUTORIZADOS.Split(',');
        //
        foreach (string grupoDoUsuario in arrGrpsPontoVirgula)
        {
            if (!string.IsNullOrEmpty(grupoDoUsuario))
            {
                if (grupoDoUsuario.Contains(","))
                    continue;
                //
                if (!arrGrps.Contains(grupoDoUsuario))
                    arrGrps.Add(grupoDoUsuario);
            }
        }
        //
        foreach (string grupoDoUsuario in arrGrpsVirgula)
        {
            if (!string.IsNullOrEmpty(grupoDoUsuario))
            {
                if (grupoDoUsuario.Contains(";"))
                    continue;
                //
                if (!arrGrps.Contains(grupoDoUsuario))
                    arrGrps.Add(grupoDoUsuario);
            }
        }
        //
        var count = arrGrps.Count;
        int i;

        for (i = 0; i < count; i++)
        {
            if (grupos.Contains(arrGrps[i].Trim()))
            {
                encontrouGrupoAutorizado = true;
            }
        }
       
        return encontrouGrupoAutorizado;
    }

    public class ConjuntoDePermissoesUsuario
    {
        public ControleDeAcessoTO.ObterInformacoesUsuario InformacoesUsuario { get; set; }
    }
}