﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

public class Utilitario
{
    public static void CriaLogErro(Exception ex, string Mensagem)
    {
        System.Diagnostics.EventLog evlLog = new System.Diagnostics.EventLog();
        evlLog.Source = "Application";
        evlLog.WriteEntry("PORTAL TGV: " + Mensagem + ": " + Environment.NewLine + Environment.NewLine +
            "Verifique a mensagem de erro a seguir para solucionar o problema." + Environment.NewLine +
            Environment.NewLine + ex.Message + Environment.NewLine + Environment.NewLine + "Metodo: " +
            ex.TargetSite.DeclaringType.FullName + ex.TargetSite.Name + Environment.NewLine + Environment.NewLine + "Origem: " +
            ex.Source + Environment.NewLine + Environment.NewLine + "Pilha de execução: " + ex.StackTrace, EventLogEntryType.Error);
    }

    public static void InsereLog(Exception ex, string metodo, string parametro, string camada, string Mensagem)
    {
        var objectInsert = new ControleLogModel.ControleLog();

        string MessagemL = "";
        string MessagemRedusuda = "";


        if (ex != null)
        {
            MessagemL = "PORTAL TGV: " + Mensagem + ": " + Environment.NewLine + Environment.NewLine +
                                "Verifique a mensagem de erro a seguir para solucionar o problema." + Environment.NewLine +
                                Environment.NewLine + ex.Message + Environment.NewLine + Environment.NewLine + "Metodo: " +
                                ex.TargetSite.DeclaringType.FullName + ex.TargetSite.Name + Environment.NewLine + Environment.NewLine + "Origem: " +
                                ex.Source + Environment.NewLine + Environment.NewLine + "Pilha de execução: " + ex.StackTrace;


            if (ex.Message.Length < 600)
                MessagemRedusuda = ex.Message;
            else
                MessagemRedusuda = ex.Message.Substring(0, 600);

        }
        else
        {
            MessagemL = "PORTAL TGV: " + Mensagem;
            MessagemRedusuda = Mensagem;

        }

        string Parametro = "";

        if (parametro.Length > 70)
            Parametro = parametro.Substring(0, 70);
        else
            Parametro = parametro;


        objectInsert.DESMDO = metodo;
        objectInsert.DESPMTSIS = Parametro;
        objectInsert.DESCAMERR = camada;
        objectInsert.DESERRRDC = MessagemRedusuda;
        objectInsert.DESERRDDO = MessagemL;

        var insert = new ControleLogBLL().inserirLog(objectInsert);
        objectInsert = null;             
    }

    public static string ToListStr(List<long> LIST)
    {
        string retr = string.Empty;
        bool firstTime = true;

        foreach (long Item in LIST)
        {
            if (firstTime)
            {
                firstTime = false;

                retr = (Item.ToString());
            }
            else
            {
                retr = (string.Format("{0}, {1}", retr, Item));
            }

        }

        return retr.Trim();
    }

    public static string ToListInt(List<int> LIST)
    {
        string retr = string.Empty;
        bool firstTime = true;

        foreach (int Item in LIST)
        {
            if (firstTime)
            {
                firstTime = false;

                retr = (Item.ToString());
            }
            else
            {
                retr = (string.Format("{0}, {1}", retr, Item));
            }

        }

        return retr.Trim();
    }
}