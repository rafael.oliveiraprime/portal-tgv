﻿using System.Collections.Generic;

namespace ImportaCestaProdutoTO
{
    public class obterImportaCestaProduto
    {
        public int CODCLI { get; set; }

        public string NOMCLI { get; set; }

        public int ANOMESREF { get; set; }

        public int CODPRDCES { get; set; }

        public string DESPRDCES { get; set; }

        public string VLRPRDCES { get; set; }

        public int CODFNCALT { get; set; }

        public string DATATU { get; set; }

        public string NUMCGCCLI { get; set; }
    }

    public class verificaExisteAnoMes
    {
        public int ANOMESREF { get; set; }
    }

    public class verificaExisteProduto
    {
        public int CODPRDCES { get; set; }
    }

    public class verificaTipoValorProduto
    {
        public int CODPRDCES { get; set; }

        public int TIPDDOATR { get; set; }
    }

    public class verificaExisteCliente
    {
        public int CODCLI { get; set; }
    }

    public class verificaExisteClienteMartins
    {
        public int CODCLI { get; set; }
    }

    public class verificaEnvioLote
    {
        public string DATENVFAT { get; set; }
    }
}

