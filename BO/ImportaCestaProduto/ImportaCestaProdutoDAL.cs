﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class ImportaCestaProdutoDAL : DAL
{
    public List<ImportaCestaProdutoTO.obterImportaCestaProduto> obterImportaCestaProduto(ImportaCestaProdutoApiModel.ImportaCestaProdutoApiModel objPesquisar)
    {
        var DALSQL = new ImportaCestaProdutoDALSQL();

        if (objPesquisar.CODPRDCES == 0)
            objPesquisar.CODPRDCES = -1;

        if (objPesquisar.ANOMESREF == 0)
            objPesquisar.ANOMESREF = -1;

        string cmdSql = DALSQL.obterImportaCestaProduto(objPesquisar.ANOMESREF);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODPRDCES", objPesquisar.CODPRDCES);
        
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);

        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<ImportaCestaProdutoTO.obterImportaCestaProduto>();
    }

    public int verificaExisteAnoMes(ImportaCestaProdutoApiModel.ImportaCestaProdutoApiModel objPesquisar)
    {
        var DALSQL = new ImportaCestaProdutoDALSQL();
        string cmdSql = DALSQL.verificaExisteAnoMes();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        var retorno = MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<ImportaCestaProdutoTO.verificaExisteAnoMes>();

        if (retorno.Count > 0)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public List<ImportaCestaProdutoTO.verificaExisteProduto> verificaExisteProduto()
    {
        var DALSQL = new ImportaCestaProdutoDALSQL();
        string cmdSql = DALSQL.verificaExisteProduto();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<ImportaCestaProdutoTO.verificaExisteProduto>();
    }

    public List<ImportaCestaProdutoTO.verificaTipoValorProduto> verificaTipoValorProduto()
    {
        var DALSQL = new ImportaCestaProdutoDALSQL();
        string cmdSql = DALSQL.verificaTipoValorProduto();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<ImportaCestaProdutoTO.verificaTipoValorProduto>();
    }

    public List<ImportaCestaProdutoTO.verificaExisteCliente> verificaExisteCliente()
    {
        var DALSQL = new ImportaCestaProdutoDALSQL();
        string cmdSql = DALSQL.verificaExisteCliente();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<ImportaCestaProdutoTO.verificaExisteCliente>();
    }

    public int verificaExisteClienteMartins(int CODCLI)
    {
        var DALSQL = new ImportaCestaProdutoDALSQL();
        string cmdSql = DALSQL.verificaExisteClienteMartins();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", CODCLI);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        var retorno = MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<ImportaCestaProdutoTO.verificaExisteCliente>();

        if (retorno.Count > 0)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public bool verificaEnvioLote(int ANOMESREF)
    {
        var DALSQL = new ImportaCestaProdutoDALSQL();
        string cmdSql = DALSQL.verificaEnvioLote();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //

        //Processo para verificar se no mês posterior existe lote enviado.
        string ANO = ANOMESREF.ToString().Substring(0, 4);
        string MES = ANOMESREF.ToString().Substring(4, 2);

        if (MES != "12")
        {
            MES = (Convert.ToInt32(MES) + 1).ToString();
            if (MES.Count() == 1)
            {
                MES = "0" + MES;
            } 
        }
        else
        {
            MES = "01";
            ANO = (Convert.ToInt32(ANO) +1).ToString();
        }

        ANOMESREF = Convert.ToInt32(ANO + MES);

        dbCommand.AddWithValue("ANOMESREF", ANOMESREF);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        var retorno = MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<ImportaCestaProdutoTO.verificaEnvioLote>();

        if (retorno.Count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool inserirImportaCestaProduto(ImportaCestaProdutoApiModel.ImportaCestaProdutoApiModel objInserir)
    {
        //var DALSQL = new ImportaCestaProdutoDALSQL();

        int i = 0;
        int count = objInserir.LISTCODCLI.Count();
        
        objInserir.CODFNCALT = ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        objInserir.DATATU = DateTime.Now.ToShortDateString();

        //string cmdSql = "INSERT INTO MRT.MOVCESPRDTGV(CODCLI, ANOMESREF, CODPRDCES, VLRPRDCES, CODFNCALT, DATATU)" + "\n" +
        //   " SELECT CODCLI, ANOMESREF, CODPRDCES, VLRPRDCES, CODFNCALT, DATATU " + "\n" +
        //   "FROM (" + "\n";

        //cmdSql = cmdSql + "SELECT " + objInserir.LISTCODCLI[i] + " CODCLI, " + objInserir.ANOMESREF + " ANOMESREF, " +
        //           objInserir.LISTCODPRDCES[i] + " CODPRDCES, '" + objInserir.LISTVLRPRDCES[i] + "' VLRPRDCES, " + objInserir.CODFNCALT + " CODFNCALT, " +
        //           " SYSDATE DATATU " + "FROM DUAL" + "\n";

        //for (i = 1; i < count; i++)
        //{
        //    cmdSql = cmdSql + " UNION " + "\n" + "SELECT " + objInserir.LISTCODCLI[i] + " CODCLI, " + objInserir.ANOMESREF + " ANOMESREF, " +
        //        objInserir.LISTCODPRDCES[i] + " CODPRDCES, '" + objInserir.LISTVLRPRDCES[i] + "' VLRPRDCES, " + objInserir.CODFNCALT + " CODFNCALT, " +
        //        "SYSDATE DATATU " + "FROM DUAL" + "\n";
        //}

        //cmdSql = cmdSql + ")";
        //

        string cmdSql = "INSERT ALL" + "\n";

        //cmdSql = cmdSql + " INTO MRT.MOVCESPRDTGV(CODCLI, ANOMESREF, CODPRDCES, VLRPRDCES, CODFNCALT, DATATU) VALUES (" + objInserir.LISTCODCLI[i] + "," + objInserir.ANOMESREF + "," + objInserir.LISTCODPRDCES[i] + ",'" + objInserir.LISTVLRPRDCES[i] + "'," + objInserir.CODFNCALT + ", SYSDATE )" + "\n";

        var it = 0;
        var totalfor = 3000;
        

        for (it = 0; it < count; it++)
        { 
            for (i = it; i < totalfor; i++)
            {
                cmdSql = cmdSql + " INTO MRT.MOVCESPRDTGV(CODCLI, ANOMESREF, CODPRDCES, VLRPRDCES, CODFNCALT, DATATU) VALUES (" + objInserir.LISTCODCLI[i] + "," + objInserir.ANOMESREF + "," + objInserir.LISTCODPRDCES[i] + ",'" + objInserir.LISTVLRPRDCES[i] + "'," + objInserir.CODFNCALT + ", SYSDATE )" + "\n";
            }

            cmdSql = cmdSql + " SELECT * FROM DUAL" + "\n";


            var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
            dbCommand.TrataDbCommandUniversal();
            Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));

            it = i - 1;
            totalfor = totalfor + 3000;
            if (totalfor > count)
                totalfor = totalfor - (totalfor - count);


            cmdSql = "INSERT ALL" + "\n";
        }

        //        
        return true;

    }

    public bool deletarImportaCestaProduto(ImportaCestaProdutoApiModel.ImportaCestaProdutoApiModel objAltera)
    {
        var DALSQL = new ImportaCestaProdutoDALSQL();
        string cmdSql = DALSQL.deletarImportaCestaProduto();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (objAltera.CODPRDCES == 0)
        {
            objAltera.CODPRDCES = -1;
        }
        //            
        dbCommand.AddWithValue("ANOMESREF", objAltera.ANOMESREF);
        dbCommand.AddWithValue("CODPRDCES", objAltera.CODPRDCES); 
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }
}