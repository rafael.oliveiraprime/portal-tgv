﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class ImportaCestaProdutoBLL
{
    public List<ImportaCestaProdutoTO.obterImportaCestaProduto> obterImportaCestaProduto(ImportaCestaProdutoApiModel.ImportaCestaProdutoApiModel objPesquisar)
    {
        try
        {
            var DAL = new ImportaCestaProdutoDAL();
            return DAL.obterImportaCestaProduto(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO ImportaR MOVIMENTO CESTA PRODUTOS.");
            Utilitario.InsereLog(ex, "obterImportaCestaProduto", "obterImportaCestaProduto.obterImportaCestaProduto", "ImportaCestaProdutoBLL", "ERRO AO ImportaR MOVIMENTO CESTA PRODUTOS.");
            throw;
        }
    }

    public int verificaExisteAnoMes(ImportaCestaProdutoApiModel.ImportaCestaProdutoApiModel objPesquisar)
    {
        try
        {           
            var DAL = new ImportaCestaProdutoDAL();
            return DAL.verificaExisteAnoMes(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO VERIFICAR EXISTE ANO/MES.");
            Utilitario.InsereLog(ex, "verificaExisteAnoMes", "", "ImportaCestaProdutoBLL", "ERRO AO VERIFICAR EXISTE ANO/MES.");
            throw;
        }
    }

    public List<ImportaCestaProdutoTO.verificaExisteProduto> verificaExisteProduto()
    {
        try
        {
            var DAL = new ImportaCestaProdutoDAL();
            return DAL.verificaExisteProduto();
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO VERIFICAR EXISTE PRODUTO.");
            Utilitario.InsereLog(ex, "verificaExisteProduto", "", "ImportaCestaProdutoBLL", "ERRO AO VERIFICAR EXISTE PRODUTO.");
            throw;
        }
    }

    public List<ImportaCestaProdutoTO.verificaTipoValorProduto> verificaTipoValorProduto()
    {
        try
        {
            var DAL = new ImportaCestaProdutoDAL();
            return DAL.verificaTipoValorProduto();
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO VERIFICAR VALOR PRODUTO ESTA CORRETO.");
            Utilitario.InsereLog(ex, "verificaTipoValorProduto", "", "ImportaCestaProdutoBLL", "ERRO AO VERIFICAR VALOR PRODUTO ESTA CORRETO.");
            throw;
        }
    }

    public List<ImportaCestaProdutoTO.verificaExisteCliente> verificaExisteCliente()
    {
        try
        {
            var DAL = new ImportaCestaProdutoDAL();
            return DAL.verificaExisteCliente();
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO VERIFICAR EXISTE PRODUTO.");
            Utilitario.InsereLog(ex, "verificaExisteProduto", "", "ImportaCestaProdutoBLL", "ERRO AO VERIFICAR EXISTE CLIENTE.");
            throw;
        }
    }

    public int verificaExisteClienteMartins(int CODCLI)
    {
        try
        {
            var DAL = new ImportaCestaProdutoDAL();
            return DAL.verificaExisteClienteMartins(CODCLI);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO VERIFICAR EXISTE PRODUTO.");
            Utilitario.InsereLog(ex, "verificaExisteClienteMartins", "", "ImportaCestaProdutoBLL", "ERRO AO VERIFICAR EXISTE CLIENTE MARTINS.");
            throw;
        }
    }

    public bool verificaEnvioLote(int ANOMESREF)
    {
        try
        {
            var DAL = new ImportaCestaProdutoDAL();
            return DAL.verificaEnvioLote(ANOMESREF);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO VERIFICAR EXISTE ENVIO LOTE.");
            Utilitario.InsereLog(ex, "verificaEnvioLote", "", "ImportaCestaProdutoBLL", "ERRO AO VERIFICAR EXISTE ENVIO LOTE.");
            throw;
        }
    }

    public bool inserirImportaCestaProduto(ImportaCestaProdutoApiModel.ImportaCestaProdutoApiModel objInserir)
    {
        try
        {
            var DAL = new ImportaCestaProdutoDAL();
            return DAL.inserirImportaCestaProduto(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO INSERIR MOVIMENTO CESTA PRODUTOS.");
            Utilitario.InsereLog(ex, "InserirImportaCestaProduto", "ImportaCestaProdutoApiModel.ImportaCestaProdutoApiModel", "ImportaCestaProdutoBLL", "ERRO AO INSERIR MOVIMENTO CESTA PRODUTOS.");
            objInserir.LISTPROBLEMA.Add(" Problema ao fazer a inserção do mês no movimento Cesta Produtos. ERRO:" + ex.Message);
            throw;
        }
    }

    public bool deletarImportaCestaProduto(ImportaCestaProdutoApiModel.ImportaCestaProdutoApiModel objAlterar)
    {
        try
        {
            var DAL = new ImportaCestaProdutoDAL();
            return DAL.deletarImportaCestaProduto(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO DELETAR MOVIMENTO CESTA PRODUTOS.");
            Utilitario.InsereLog(ex, "deletarImportaCestaProduto", "ImportaCestaProdutoApiModel.ImportaCestaProdutoApiModel", "ImportaCestaProdutoBLL", "ERRO AO DELETAR MOVIMENTO CESTA PRODUTOS.");
            objAlterar.LISTPROBLEMA.Add(" Problema ao fazer a deleção do mês no movimento Cesta Produtos. ERRO: " + ex.Message);
            throw;
        }
    }
}