﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class ImportaCestaProdutoDALSQL
{
    public string obterImportaCestaProduto(int anomes)
    {

        return new StringBuilder(@"
                                        SELECT MOV.CODCLI
                                            , NVL(CLI.NOMCLI, ' ') AS NOMCLI
                                            , CASE CLI.NUMCGCCLI WHEN CLI.NUMCGCCLI THEN (SUBSTR(CLI.NUMCGCCLI, 1, 2) || '.' || SUBSTR(CLI.NUMCGCCLI, 3, 3) || '.' || SUBSTR(CLI.NUMCGCCLI, 6, 3) || '/' || SUBSTR(CLI.NUMCGCCLI, 9,4) || '-' || SUBSTR(CLI.NUMCGCCLI, 13,2) ) ELSE NULL END NUMCGCCLI
                                            , MOV.ANOMESREF
                                            , MOV.CODPRDCES
                                            , CES.CODPRDCES
                                            , CES.DESPRDCES
                                            , MOV.CODFNCALT
                                            , MOV.DATATU
                                            , MOV.VLRPRDCES
                                        FROM MRT.MOVCESPRDTGV MOV
                                        INNER JOIN MRT.CADCLITGV CONTA ON CONTA.CODCLI = MOV.CODCLI 
                                        LEFT JOIN MRT.CADCESPRDTGV CES ON (MOV.CODPRDCES = CES.CODPRDCES)
                                        LEFT JOIN MRT.T0100043 CLI ON(MOV.CODCLI = CLI.CODCLI)
                                        WHERE 1 = 1    
                                          AND MOV.CODPRDCES = :CODPRDCES
                                          AND MOV.ANOMESREF = :ANOMESREF
                                          AND MOV.CODCLI = :CODCLI
                                        ORDER BY CLI.NOMCLI
                                    ").ToString();

    }

    public string verificaExisteAnoMes()
    {
        return @"
                    SELECT MOV.ANOMESREF                        
                    FROM MRT.MOVCESPRDTGV MOV                    
                    WHERE ANOMESREF = :ANOMESREF
                    AND ROWNUM <= 1                 
                ";
    }

    public string verificaExisteProduto()
    {
        return @"
                    SELECT CES.CODPRDCES
                    FROM MRT.CADCESPRDTGV CES                   
                    WHERE CES.DATDST IS NULL
                    AND CES.FLGIPDSVC = 1
                ";
    }

    public string verificaTipoValorProduto()
    {
        return @"
                    SELECT CES.CODPRDCES, CES.TIPDDOATR
                    FROM MRT.CADCESPRDTGV CES  
                    ORDER BY CES.CODPRDCES
                ";
    }

    public string verificaExisteCliente()
    {
        return @"
                    SELECT CODCLI
                    FROM MRT.CADCLITGV
                    UNION
                    SELECT CODCLI
                    FROM MRT.RLCCLIAREATDSUPSMA          
                ";
    }

    public string verificaExisteClienteMartins()
    {
        return @"
                    SELECT CODCLI
                    FROM MRT.T0100043	
                    WHERE DATDSTCLI IS NULL 
                    AND CODCLI = :CODCLI          
                ";
    }

    public string verificaEnvioLote()
    {
        return @"
                    SELECT DATENVFAT FROM MRT.MOVMESFATRSMTGV  
                     WHERE ANOMESREF = :ANOMESREF
                     AND DATENVFAT IS NOT NULL
                ";
    }

    public string inserirImportaCestaProduto()
    {
        return @" INSERT INTO MRT.MOVCESPRDTGV (CODCLI, ANOMESREF, CODPRDCES, VLRPRDCES, CODFNCALT, DATATU) 
                                        VALUES (:CODCLI, :ANOMESREF, :CODPRDCES, :VLRPRDCES, :CODFNCALT, SYSDATE) ";
    }

    public string deletarImportaCestaProduto()
    {
        return @" DELETE FROM MRT.MOVCESPRDTGV MOV
                                       WHERE MOV.ANOMESREF = :ANOMESREF 
                                             AND CODPRDCES = :CODPRDCES";
    }
}