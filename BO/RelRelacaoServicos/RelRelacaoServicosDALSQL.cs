﻿
public class RelRelacaoServicosDALSQL
{
    public string obterRelacaoServicos()
    {
        return @"    SELECT CLI.CODCLI CODIGO
                            ,svc.dessvctgv SERVICO
                            ,svc.vlrsvc VALOR
                            ,CASE WHEN INDSTAATV = 1 THEN 'EM IMPLANTAÇÃO'
                                    WHEN INDSTAATV = 2 THEN 'ATIVO'
                                    WHEN INDSTAATV = 3 THEN 'SUSPENSO'
                                    WHEN INDSTAATV = 4 THEN 'CANCELADO'
                                END AS STATUS
                            ,TO_CHAR(CLI.DATATVSVC, 'DD/MM/YYYY') AS ATIVACAO
                            ,TO_CHAR(CLI.DATCNC, 'DD/MM/YYYY') CANCELAMENTO
                        FROM MRT.CADCLISVCTGV CLI
                        LEFT JOIN MRT.CADSVCTGV SVC ON CLI.CODSVCTGV = svc.CODSVCTGV
                       WHERE 1 = 1
                         AND CLI.CODCLI = :CODCLI
                         AND INDSTAATV = :INDSTAATV
                         AND TRUNC(DATATVSVC) = TRUNC(TO_DATE(:DATATVSVC,'DD/MM/YYYY'))
                         ABD TRUNC(DATCNC) = TRUNC(TO_DATE(:DATCNC,'DD/MM/YYYY'))
                    ORDER BY CODCLI
                ";
    }





}