﻿
namespace RelRelacaoServicosTO
{
    public class obterRelacaoServicos
    {
        public int CODIGO { get; set; }
        public string SERVICO { get; set; }
        public decimal VALOR { get; set; }
        public string STATUS { get; set; }
        public string ATIVACAO { get; set; }
        public string CANCELAMENTO { get; set; }
    }
    
}