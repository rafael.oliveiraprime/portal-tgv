﻿using System;
using System.Collections.Generic;

public class RelRelacaoServicosBLL
{
    public List<RelRelacaoServicosTO.obterRelacaoServicos> obterRelacaoServicos(RelRelacaoServicosApiModel.obterRelRelacaoServicosApiModel objPesquisar)
    {
        try
        {
            var DAL = new RelRelacaoServicosDAL();
            return DAL.obterRelacaoServicos(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO RELATORIO DE PONTUACAO.");
            Utilitario.InsereLog(ex, "obterIndicacoes", "RelPontuacaoApiModel.obterPontuacaoApiModel", "RelPontuacaoBLL", "ERRO AO FAZER A CONSULTA DO RELATORIO DE PONTUACAO.");
            throw;
        }
    }

}