﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class RelRelacaoServicosDAL : DAL
{
    public List<RelRelacaoServicosTO.obterRelacaoServicos> obterRelacaoServicos(RelRelacaoServicosApiModel.obterRelRelacaoServicosApiModel objPesquisar)
    {
        var DALSQL = new RelRelacaoServicosDALSQL();
        string cmdSql = DALSQL.obterRelacaoServicos();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //    
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("INDSTAATV", objPesquisar.INDSTAATV);
        dbCommand.AddWithValue("DATATVSVC", objPesquisar.DATATVSVC);
        dbCommand.AddWithValue("DATCNC", objPesquisar.DATCNC);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<RelRelacaoServicosTO.obterRelacaoServicos>();
    }


}