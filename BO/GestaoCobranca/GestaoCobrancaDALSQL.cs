﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class GestaoCobrancaDALSQL
{
    public string buscaUltimoLoteEnviado()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT ANOMESREF, NUMLOTPRENOTFSCSVC
                                        FROM MRT.MOVMESFATRSMTGV
                                       WHERE INDFATSVC = 1
                                         AND ANOMESREF = (SELECT MAX(ANOMESREF) FROM MRT.MOVMESFATRSMTGV
                                                           WHERE INDFATSVC = 1 AND DATENVFAT IS NOT NULL)  
                

                                   ");
        return strBld.ToString();
    }


    public string VerificaBusca()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT COUNT(ANOMESREF)
                                        FROM MRT.MOVTITTGV
                                       WHERE INDFATSVC = 1
                                         AND ANOMESREF = :ANOMESREF                                         
                                   ");
        return strBld.ToString();
    }


    public string obterTitulosFatEnviada()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@"  SELECT MOVCAP.ANOMESREF
                                              ,MOV.INDFATSVC
                                              ,MOV.CODCLI
                                              ,TIT.NUMTITCOBCLI
                                              ,TIT.CODFILEMP
                                              ,TIT.DATVNCTIT
                                              ,TIT.NUMPCLTITNOTFSCSVC
                                              ,MOV.INDDBTAUTPGT
                                              ,TIT.VLRPCLTIT
                                              ,1 AS CODSTATIT
                                              ,TIT.NUMNOTFSCSVC 
                                              ,TIT.DATIPRNOTFSCSVC
                                              ,MOVCAP.NUMLOTPRENOTFSCSVC
                                         FROM MRT.MOVMESFATRSMTGV MOVCAP
                                        INNER JOIN MRT.MOVMESFATCLITGV MOV ON(MOVCAP.ANOMESREF = MOV.ANOMESREF AND MOVCAP.NUMLOTPRENOTFSCSVC =  MOV.NUMLOTPRENOTFSCSVC)
                                        INNER JOIN MRT.CADPRENOTFSCSVC CADPRE ON(CADPRE.NUMLOTPRENOTFSCSVC = MOVCAP.NUMLOTPRENOTFSCSVC AND MOV.CODCLI = CADPRE.CODDSNNOTFSC)
                                        INNER JOIN MRT.T0145594 TIT ON(CADPRE.NUMNOTFSCSVC = TIT.NUMNOTFSCSVC AND CADPRE.CODEMP = TIT.CODEMP AND CADPRE.CODFILEMP = TIT.CODFILEMP)
                                        WHERE MOVCAP.ANOMESREF = :ANOMESREF
                                          AND MOVCAP.NUMLOTPRENOTFSCSVC = :NUMLOTPRENOTFSCSVC
                                   ");
        return strBld.ToString();
    }

    public string inserirtitulos()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" INSERT INTO MRT.MOVTITTGV(ANOMESREF, INDFATSVC, CODCLI, NUMTITCOBFAT, CODFILEMP, DATVNCTIT, NUMPCLTITNOTFSCSVC, INDDBTAUTPGT, VLRPCLTIT, CODSTATIT, VLRSLDTIT, NUMLOTPRENOTFSCSVC)
                                                         VALUES(:ANOMESREF, :INDFATSVC, :CODCLI, :NUMTITCOBFAT, :CODFILEMP, :DATVNCTIT, :NUMPCLTITNOTFSCSVC, :INDDBTAUTPGT, :VLRPCLTIT, :CODSTATIT, :VLRSLDTIT, :NUMLOTPRENOTFSCSVC)
                                   ");

        return strBld.ToString();
    }


    public string updateMovcad()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" UPDATE MRT.MOVMESFATCLITGV SET NUMNOTFSCSVC = :NUMNOTFSCSVC,
                                                                     DATIPRNOTFSCSVC = :DATIPRNOTFSCSVC
                                       WHERE ANOMESREF = :ANOMESREF
                                         AND    CODCLI = :CODCLI ");

        return strBld.ToString();
    }


    public string obterCliTitulos()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" 

                                     WITH TABTOTAISRESPON AS (
                                                                SELECT GC.CODCLI AS CODCLICHECK
                                                                        ,GC.CODCLI
                                                                        ,CLI.NUMCGCCLI
                                                                        ,CLI.NOMCLI
                                                                        ,CASE WHEN GC.INDDBTAUTPGT = 1 THEN 'DEBITO AUTOMATICO'
                                                                            WHEN GC.INDDBTAUTPGT = 0 THEN 'BOLETO' END FORMAPG                   
                                                                        ,CASE WHEN GC.CODSITCLI = 1 THEN 'EM IMPLANTAÇÃO'
                                                                            WHEN GC.CODSITCLI = 2 THEN 'ATIVO'
                                                                            WHEN GC.CODSITCLI = 3 THEN 'SUSPENSO'
                                                                            WHEN GC.CODSITCLI = 4 THEN 'CANCELADO'  END STATUS                                                                        
                                                                        ,SUM(CASE WHEN (TIT.CODSTATIT NOT IN(1, 4)) THEN  TIT.VLRSLDTIT ELSE 0 END) AS VLRSLDTIT
                                                                        ,NVL(FNC.NOMFNC, ' ') AS NOMRES
                                                                        ,CASE WHEN FNC.NOMFNC IS NULL THEN ' '  
                                                                              ELSE (SELECT COALESCE(TO_CHAR(MAX(ANT.DATAGDCTO), 'DD/MM/YYYY HH24:MI'),' ') FROM MRT.CADOBSCLITGV ANT WHERE GC.CODCLI = ANT.CODCLI AND TIPOBSCLI = 11) 
                                                                         END AS DATAGDCTO                                                                                                                                                
                                                                        ,NVL(GPR.NOMGRPEMPCLI, PGPR.NOMGRPEMPCLI) AS NOMGRPEMPCLI
                                                                        ,CASE WHEN FNC.NOMFNC IS NULL THEN NULL  
                                                                              ELSE (SELECT MAX(ANT.DATAGDCTO) FROM MRT.CADOBSCLITGV ANT WHERE GC.CODCLI = ANT.CODCLI AND TIPOBSCLI = 11) 
                                                                         END AS DATAGDCTOORDER                                                                                                                                       
                                                                FROM MRT.CADCLITGV GC 
                                                                INNER JOIN MRT.T0100043 CLI ON (GC.CODCLI = CLI.CODCLI) 
                                                                INNER JOIN MRT.MOVTITTGV TIT ON (CLI.CODCLI = TIT.CODCLI)  
                                                                LEFT JOIN MRT.T0100361 FNC ON (FNC.CODFNC = GC.CODRPN)  
                                                                LEFT JOIN MRT.RLCCLIGRPEMPTGV RL ON (GC.CODCLI = RL.CODCLI)
                                                                LEFT JOIN MRT.CADGRPEMPTGV GPR ON (GPR.CODGRPEMPCLI = RL.CODGRPEMPCLI)  
                                                                LEFT JOIN MRT.CADGRPEMPTGV PGPR ON (PGPR.CODCLIGRPFAT = GC.CODCLI)
                                                                WHERE 1 = 1  
                                                                    AND TIT.CODCLI = :CODCLI                                                       
                                                                    AND CLI.NOMCLI LIKE '%' || UPPER(:NOMCLI) || '%'
                                                                    AND CLI.NUMCGCCLI = :NUMCGCCLI
                                                                    AND GC.CODRPN = :CODRPN
                                                                    AND TIT.CODSTATIT = :STATUS    
                                                                    AND GPR.CODGRPEMPCLI = :CODGRPEMPCLI 
                                                                    AND TIT.ANOMESREF = :ANOMESREF
                                                                GROUP BY GC.CODCLI
                                                                        ,CLI.NUMCGCCLI
                                                                        ,CLI.NOMCLI
                                                                        ,GC.INDDBTAUTPGT
                                                                        ,GC.CODSITCLI
                                                                        ,FNC.NOMFNC
                                                                        ,GPR.NOMGRPEMPCLI
                                                                        ,PGPR.NOMGRPEMPCLI
                                                            )
                                                            SELECT CODCLICHECK
                                                                   ,CODCLI
                                                                   ,NUMCGCCLI
                                                                   ,NOMCLI
                                                                   ,FORMAPG
                                                                   ,STATUS
                                                                   ,VLRSLDTIT        
                                                                   ,NOMRES
                                                                   ,DATAGDCTO
                                                                   ,NOMGRPEMPCLI                           
                                                              FROM TABTOTAISRESPON                                     
                                                             ORDER BY DATAGDCTOORDER  ASC
                                  ");

        return strBld.ToString();
    }



    public string ObterNotasFiscais()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT GC.CODCLI
                                             ,MOV.NUMNOTFSCSVC
                                             ,TIT.NUMTITCOBFAT                                             
                                             ,TO_CHAR(TIT.DATVNCTIT,'DD/MM/YYYY') AS DATVNCTIT
                                             ,TIT.VLRPCLTIT
                                             ,TIT.NUMPCLTITNOTFSCSVC
                                             ,VLRSLDTIT AS VALORPAGO
                                             ,TO_CHAR(TIT.DATPGTTIT,'DD/MM/YYYY') AS DATPGTTIT
                                             ,CASE WHEN TIT.CODSTATIT = 1 THEN 'ABERTO'
                                                   WHEN TIT.CODSTATIT = 2 THEN 'PAGO'  
                                                   WHEN TIT.CODSTATIT = 3 THEN 'ATRASO'
                                                   WHEN TIT.CODSTATIT = 5 THEN 'PREJUÍZO'
                                                   WHEN TIT.CODSTATIT = 4 THEN 'CANCELADO' END STATUS                   
                                             ,CASE WHEN TIT.INDDBTAUTPGT = 0 THEN 'BOLETO'
                                                   WHEN TIT.INDDBTAUTPGT = 1 THEN 'DEBITO' 
                                                   WHEN TIT.INDDBTAUTPGT = 2 THEN 'TRANSFERÊNCIA' END FORMAPAG       
                                             ,TIT.CODFILEMP
                                             , 1 AS CODEMP
                                             ,TO_CHAR(MOV.DATIPRNOTFSCSVC,'DD/MM/YYYY') AS DATIPRNOTFSCSVC
                                        FROM MRT.CADCLITGV GC
                                       INNER JOIN MRT.MOVTITTGV TIT ON(GC.CODCLI = TIT.CODCLI)
                                       INNER JOIN MRT.MOVMESFATCLITGV MOV ON(TIT.ANOMESREF = MOV.ANOMESREF AND TIT.CODCLI = MOV.CODCLI AND TIT.INDFATSVC = MOV.INDFATSVC AND TIT.NUMLOTPRENOTFSCSVC = MOV.NUMLOTPRENOTFSCSVC)                                            
                                       WHERE TIT.CODCLI = :CODCLI   
                                       ORDER BY TIT.DATVNCTIT DESC
                                  ");
        return strBld.ToString();
    }


    public string TotaisTitulos()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT  CODSTATIT
                                                ,CASE WHEN CODSTATIT = 1 THEN 'ABERTO'
                                                       WHEN CODSTATIT = 2 THEN 'PAGO'
                                                       WHEN CODSTATIT = 3 THEN 'ATRASO'
                                                       WHEN CODSTATIT = 4 THEN 'CANCELADO'
                                                       WHEN CODSTATIT = 5 THEN 'PREJUÍZO' END AS STATUS               
                                                ,COUNT(CODCLI) AS TOTAIS 
                                          FROM MRT.MOVTITTGV
                                        GROUP BY CODSTATIT
                                  ");
        return strBld.ToString();

    }


    public string AtualizaResponsavel(List<long> LISTCODCLI)
    {
        StringBuilder strBld; 

        strBld = new StringBuilder(@" UPDATE MRT.CADCLITGV SET CODRPN = :CODRPN
                                       WHERE CODCLI IN(#LISTCODCLI)
                                  ").Replace("#LISTCODCLI", Utilitario.ToListStr(LISTCODCLI));

        return strBld.ToString();
    }

    public string obterTotaisEquipes()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" WITH TABTOTAISRESPON AS (       
                                                                SELECT CODRPN
                                                                       ,COUNT(CODCLI) AS TOTAL
                                                                  FROM MRT.CADCLITGV
                                                                 WHERE (CODRPN <> 0 AND CODRPN IS NOT NULL) 
                                                                 GROUP BY CODRPN
                                                               )
                                            SELECT CON.CODFNC
                                                   ,TRIM(FNC.NOMFNC) AS NOMFNC
                                                   ,NVL(RES.TOTAL, 0) AS TOTAL
                                              FROM MRT.CADFNCSISTGV CON	
                                             INNER JOIN MRT.T0100361 FNC ON FNC.CODFNC = CON.CODFNC
                                              LEFT JOIN TABTOTAISRESPON RES ON CON.CODFNC = RES.CODRPN
                                             WHERE CON.DATDST IS NULL
                                               AND CON.CODPFLACS = 3
                                             ORDER BY NVL(RES.TOTAL,0), FNC.NOMFNC

                                  ");

        return strBld.ToString();
    }

    public string buscaClientesSemResponsavel()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" 
                                        SELECT CAD.CODCLI
                                          FROM MRT.CADCLITGV CAD
                                         INNER JOIN MRT.MOVTITTGV TIT ON (CAD.CODCLI = TIT.CODCLI) 
                                         WHERE (CAD.CODRPN = 0 OR CAD.CODRPN IS NULL) 
                                           AND TIT.CODSTATIT IN(3,5) 
                                         GROUP BY CAD.CODCLI
                                 ");

        return strBld.ToString();
    }

    public string obterCobrancaContato()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@"  SELECT DISTINCT CTO.CODEDE AS CODCLI, 
                                                     CTO.DESENDETNCTO,
                                                     TRIM(TIP.DESTIPCTO) AS DESTIPCTO,
                                                     TRIM(CTO.NOMCTO) AS NOMCTO
                                         FROM MRT.CADCTOTGV CTO
                                        INNER JOIN MRT.CADTIPCTOTGV TIP ON TIP.CODTIPCTO = CTO.CODTIPCTO
                                        WHERE CTO.CODTIPCTO = 7
                                          AND CTO.CODEDE = :CODCLI
                                  ");

        return strBld.ToString();

    }


    public string obterTotalContato()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT COUNT(CODFNC)
                                        FROM MRT.CADFNCSISTGV
                                       WHERE DATDST IS NULL
                                         AND CODPFLACS = 3
                                  ");

        return strBld.ToString();
    }

    
    public string AtualizaStatusTitulo(List<long> LISTTITULOS, int Status)
    {
        StringBuilder strBld;

        if (Status == 3)
        {
            strBld = new StringBuilder(@" UPDATE MRT.MOVTITTGV SET CODSTATIT = :CODSTATIT,
                                                                   VLRSLDTIT = VLRPCLTIT,
                                                                   DATPGTTIT = null
                                       WHERE CODCLI = :CODCLI
                                         AND NUMTITCOBFAT IN(#LISTTITULOS)
                                  ").Replace("#LISTTITULOS", Utilitario.ToListStr(LISTTITULOS));
        }
        else if (Status == 5)
        {
            strBld = new StringBuilder(@" UPDATE MRT.MOVTITTGV SET CODSTATIT = :CODSTATIT
                                       WHERE CODCLI = :CODCLI
                                         AND NUMTITCOBFAT IN(#LISTTITULOS)
                                  ").Replace("#LISTTITULOS", Utilitario.ToListStr(LISTTITULOS));

        }
        else if (Status == 2)
        {
            strBld = new StringBuilder(@" UPDATE MRT.MOVTITTGV SET CODSTATIT = :CODSTATIT,
                                                                   VLRSLDTIT = 0,
                                                                   DATPGTTIT = :DATPGTTIT
                                       WHERE CODCLI = :CODCLI
                                         AND NUMTITCOBFAT IN(#LISTTITULOS)
                                  ").Replace("#LISTTITULOS", Utilitario.ToListStr(LISTTITULOS));
        }
        else
        {
            strBld = new StringBuilder(@" UPDATE MRT.MOVTITTGV SET CODSTATIT = :CODSTATIT,
                                                                   VLRSLDTIT = 0
                                       WHERE CODCLI = :CODCLI
                                         AND NUMTITCOBFAT IN(#LISTTITULOS)
                                  ").Replace("#LISTTITULOS", Utilitario.ToListStr(LISTTITULOS));

        }


        return strBld.ToString();
    }


    public string VerificaExisteResponsavel()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT CODRPN
                                        FROM MRT.CADCLITGV
                                       WHERE (CODRPN = 0 OR CODRPN IS NULL) 
                                         AND CODCLI = :CODCLI
                                       GROUP BY CODCLI
                                  ");

        return strBld.ToString();
    }


    public string AtualizaResponsaveis()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" UPDATE MRT.CADCLITGV SET CODRPN = :CODRPN
                                       WHERE CODCLI = :CODCLI
                                  ");

        return strBld.ToString();
    }


    public string VerificaExistetIiTiEmAtraso()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT COUNT(NUMTITCOBFAT)
                                        FROM MRT.MOVTITTGV 
                                       WHERE CODSTATIT IN(3, 5)
                                         AND CODCLI = :CODCLI 
                                  ");

        return strBld.ToString();

    }


    public string obterServicosAvulsosParcelas()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT MOVCLI.CODCLI
                                             ,MOVCLI.CODSVCTGV 
                                             ,UPPER(MOVCLI.DESSVCTGV) AS DESSVCTGV 
                                             ,MOVPAR.NUMPCLTITNOTFSCSVC
                                             ,TO_CHAR(MOVPAR.DATVNCNOTFSCSVC,'DD/MM/YYYY') AS DATVNCNOTFSCSVC
                                             ,MOVPAR.VLRPCLTIT
                                             ,CASE WHEN MOVPAR.INDFATSVC = 122 THEN 'ATIVAÇÃO'
                                                   WHEN MOVPAR.INDFATSVC = 123 THEN 'MENSALIDADE'
                                                   WHEN MOVPAR.INDFATSVC = 124 THEN 'DESINSTALAÇÃO' END AS TIPO
                                        FROM MRT.MOVMESFATCLISVCTGV MOVCLI
                                        INNER JOIN MRT.MOVFATCLISVCPCLTGV MOVPAR ON (MOVCLI.CODCLI = MOVPAR.CODCLI AND MOVCLI.INDFATSVC = MOVPAR.INDFATSVC AND MOVCLI.NUMLOTPRENOTFSCSVC = MOVPAR.NUMLOTPRENOTFSCSVC)
                                        WHERE MOVCLI.CODCLI = :CODCLI 
                                        ORDER BY MOVPAR.NUMPCLTITNOTFSCSVC, MOVPAR.DATVNCNOTFSCSVC ASC 
                                  ");

        return strBld.ToString();

    }


    public string buscaUltimoLoteEnviadoAvulso()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT ANOMESREF
                                             ,NUMLOTPRENOTFSCSVC
                                        FROM MRT.MOVMESFATCLITGV
                                       WHERE INDFATSVC <> 1
                                         AND NUMNOTFSCSVC IS NULL
                                         AND CODCLI = :CODCLI               
                                   ");
        return strBld.ToString();
    }

    public string AtualizaNotaBoleto()
    {
        return @"   UPDATE MRT.T0145560 SET INDIPRBLT = 1
                         WHERE CODEMP = 1
                           AND CODFILEMP = 143
                           AND  TO_CHAR(DATIPRNOTFSCSVC,'MM/YYYY') = TO_CHAR(SYSDATE,'MM/YYYY')
                           AND INDIPRBLT = 0
                           AND CODDSNNOTFSC = :CODDSNNOTFSC ";
    }



}