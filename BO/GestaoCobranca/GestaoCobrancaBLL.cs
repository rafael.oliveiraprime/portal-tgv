﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Configuration;
using Vendas.TGV.Portal;
using Vendas.TGV.Portal.wsNFSe;

public class GestaoCobrancaBLL
{
    public string buscartitulos()
    {
        GestaoCobrancaTO.ultimoLoteEnviado _item = new GestaoCobrancaTO.ultimoLoteEnviado();
        GestaoCobrancaApiModel.GestaoCobrancaApiModel _itemtitulos = new GestaoCobrancaApiModel.GestaoCobrancaApiModel();


        try
        {
            var DAL = new GestaoCobrancaDAL();
            _item = DAL.buscaUltimoLoteEnviado();

            TimeSpan tsTimeout = TimeSpan.FromMinutes(5);

            if (_item != null)
            {
                var _verificabusca = DAL.VerificaBusca(_item);
                if (_verificabusca > 0)
                    return "1";
                

                var readertitulos = DAL.obterTitulosFatEnviada(_item);

                using (TransactionScope Scope = new TransactionScope(TransactionScopeOption.Required, tsTimeout))
                {

                    while (readertitulos.Read())
                    {
                        _itemtitulos.ANOMESREF = readertitulos.GetInt32(0);
                        _itemtitulos.INDFATSVC = readertitulos.GetInt32(1);
                        _itemtitulos.CODCLI = readertitulos.GetInt32(2);
                        _itemtitulos.NUMTITCOBFAT = readertitulos.GetInt32(3);
                        _itemtitulos.CODFILEMP = readertitulos.GetInt32(4);
                        _itemtitulos.DATVNCTIT = readertitulos.GetDateTime(5).ToString();
                        _itemtitulos.NUMPCLTITNOTFSCSVC = readertitulos.GetInt32(6);
                        _itemtitulos.INDDBTAUTPGT = readertitulos.GetInt32(7);
                        _itemtitulos.VLRPCLTIT = readertitulos.GetDecimal(8);
                        _itemtitulos.CODSTATIT = readertitulos.GetInt32(9);
                        _itemtitulos.NUMNOTFSCSVC = readertitulos.GetInt32(10);
                        _itemtitulos.DATIPRNOTFSCSVC = readertitulos.GetDateTime(11).ToString();
                        _itemtitulos.NUMLOTPRENOTFSCSVC = readertitulos.GetInt64(12);

                        DAL.inserirtitulos(_itemtitulos);
                        DAL.updatenumtitmovcad(_itemtitulos);

                        _item = new GestaoCobrancaTO.ultimoLoteEnviado();
                    }

                    Scope.Complete();
                }
                
            }

            return "0";
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DA GESTÃO DE CONTAS.");
            Utilitario.InsereLog(ex, "buscartitulos", " ", "GestaoContasBLL", "ERRO AO FAZER A CONSULTA DA GESTÃO DE CONTAS.");
            throw;
        }
    }

    public List<GestaoCobrancaTO.obterCliTitulos> obterCliTitulos(GestaoCobrancaApiModel.GestaoCobrancaApiModel _ObjPesquisar)
    {
        try
        {
            var DAL = new GestaoCobrancaDAL();

            if (_ObjPesquisar.NUMCGCCLI != null)
            {
                if (_ObjPesquisar.NUMCGCCLI != "-1")
                    _ObjPesquisar.NUMCGCCLI = Regex.Replace(_ObjPesquisar.NUMCGCCLI, "[/.-]", String.Empty);
                else
                    _ObjPesquisar.NUMCGCCLI = null;
            }


           if (_ObjPesquisar.ANOMESREFP != null)
            {
                if (_ObjPesquisar.ANOMESREFP.Trim() != "")
                    _ObjPesquisar.ANOMESREF = int.Parse(_ObjPesquisar.ANOMESREFP.Remove(4,1));
            }     

            return DAL.obterCliTitulos(_ObjPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO OBTER CLIENTES TITULOS");
            Utilitario.InsereLog(ex, "obterCliTitulos", " ", "GestaoContasBLL", "ERRO AO OBTER CLIENTES TITULOS");
            throw;
        }
    }


    
    public List<GestaoCobrancaTO.obterNotasTitulos> ObterNotasFiscais(GestaoCobrancaApiModel.GestaoCobrancaApiModel _ObjPesquisar)
    {
        try
        {
            var DAL = new GestaoCobrancaDAL();
            return DAL.ObterNotasFiscais(_ObjPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO OBTER NOTAS FISCAIS DO CLIENTE");
            Utilitario.InsereLog(ex, "ObterNotasFiscais", " ", "GestaoCobracaBLL", "ERRO AO OBTER NOTAS FISCAIS DO CLIENTE");
            throw;
        }
    }


    public string TransferirResponsavel(GestaoCobrancaApiModel.GestaoCobrancaTranfResponsavelApiModel _ObjTranferrir)
    {
        try
        {
            var DAL = new GestaoCobrancaDAL();
            DAL.AtualizaResponsavel(_ObjTranferrir);

            return "1";
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO TRANFERIR RESPONSAVEL CLIENTES COBRANCA");
            Utilitario.InsereLog(ex, "TransferirResponsavel", " ", "GestaoCobracaBLL", "ERRO AO TRANFERIR RESPONSAVEL CLIENTES COBRANCA");
            throw;
        }

    }        

    public List<GestaoCobrancaTO.TotaisEquipe> obterTotaisEquipes()
    {
        try
        {
            var DAL = new GestaoCobrancaDAL();
            return DAL.obterTotaisEquipes();
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO OBTER TOTAIS COSULTORES");
            Utilitario.InsereLog(ex, "obterTotaisEquipes", " ", "GestaoCobracaBLL", "ERRO AO OBTER TOTAIS COSULTORES");
            throw;
        }
    }

    
    public string Distribuir()
    {
        GestaoCobrancaTO.TotaisEquipe totaisEquipes = new GestaoCobrancaTO.TotaisEquipe();
        GestaoCobrancaApiModel.GestaoCobrancaTranfResponsavelApiModel _ObjTranferrir = new GestaoCobrancaApiModel.GestaoCobrancaTranfResponsavelApiModel();
        List<GestaoCobrancaTO.ClientesSemResp> clientesemResp = new List<GestaoCobrancaTO.ClientesSemResp>();

        try
        {
            var DAL = new GestaoCobrancaDAL();

            clientesemResp = DAL.buscaClientesSemResponsavel();
            if (clientesemResp != null)
            {
                if (clientesemResp.Count > 0)
                {
                    foreach (GestaoCobrancaTO.ClientesSemResp _item in clientesemResp)
                    {
                        totaisEquipes = DAL.obterTotaisEquipes().FirstOrDefault();

                        if (totaisEquipes != null)
                        {
                            _ObjTranferrir.CODRESPONSAVEL = Int32.Parse(totaisEquipes.CODFNC.ToString());
                            _ObjTranferrir.LISTCODCLI = new List<long>();
                            _ObjTranferrir.LISTCODCLI.Add(_item.CODCLI);


                            DAL.AtualizaResponsavel(_ObjTranferrir);
                        }

                        _ObjTranferrir = new GestaoCobrancaApiModel.GestaoCobrancaTranfResponsavelApiModel();
                        totaisEquipes = new GestaoCobrancaTO.TotaisEquipe();
                    }

                }
                else
                    return "2";
            }
            else
                return "2";


            return "1";
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO OBTER TOTAIS COSULTORES");
            Utilitario.InsereLog(ex, "obterTotaisEquipes", " ", "GestaoCobracaBLL", "ERRO AO OBTER TOTAIS COSULTORES");
            throw;
        }
    }



    public string VerificarDistribuicao()
    {
        List<GestaoCobrancaTO.ClientesSemResp> clientesemResp = new List<GestaoCobrancaTO.ClientesSemResp>();
        Int64 _totalConsultores = 0;

        try
        {
            var DAL = new GestaoCobrancaDAL();
            clientesemResp = DAL.buscaClientesSemResponsavel();
            if (clientesemResp != null)
            {
                if (clientesemResp.Count > 0)
                {
                    _totalConsultores = DAL.obterTotalContato();
                    if (_totalConsultores > 0)
                        return clientesemResp.Count.ToString() + ";" + _totalConsultores.ToString();
                    else
                        return clientesemResp.Count.ToString() + ";0" ;
                }
                else
                    return "N";
            }
            else 
                return "N";
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO OBTER VERIFICAR DISTRIBUIÇÃO");
            Utilitario.InsereLog(ex, "obterTotaisEquipes", " ", "GestaoCobracaBLL", "ERRO AO OBTER VERIFICAR DISTRIBUIÇÃO");
            throw;
        }

    }


    public string VisualisarBoleto(GestaoCobrancaApiModel.EnviarBoletoApiModel _ObjVisualizar)
    {
        List<GestaoCobrancaTO.ClientesSemResp> clientesemResp = new List<GestaoCobrancaTO.ClientesSemResp>();
        string _idparaboleto = "";

        if (_ObjVisualizar.CODEMP == 0)
            return "";


        if (_ObjVisualizar.CODFILEMP == 0)
            return "";


        if (_ObjVisualizar.NUMNOTFSCSVC == 0)
            return "";

        if (_ObjVisualizar.DATIPRNOTFSCSVC == null)
            return "";


        try
        {
            wsNFSe ws = new wsNFSe();
            if (ws != null)
                _idparaboleto = ws.getKeyNFSEemail(_ObjVisualizar.CODEMP, _ObjVisualizar.CODFILEMP, _ObjVisualizar.NUMNOTFSCSVC, DateTime.Parse(_ObjVisualizar.DATIPRNOTFSCSVC), 1);


            return _idparaboleto;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO VISUALIZAR O BOLETO");
            Utilitario.InsereLog(ex, "VisualisarBoleto", " ", "GestaoCobracaBLL", "ERRO AO VISUALIZAR O BOLETO");
            throw;
        }

    }

    
    public List<GestaoCobrancaTO.CobrancaContato> obterCobrancaContato(GestaoCobrancaApiModel.GestaoCobrancaApiModel _ObjPesquisar)
    {
        try
        {
            var DAL = new GestaoCobrancaDAL();
            return DAL.obterCobrancaContato(_ObjPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO CONTATO.");
            Utilitario.InsereLog(ex, "obterCadastroContato", "CadastroContatoApiModel.CadastroContatoApiModel", "CadastroContatoBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO CONTATO.");
            throw;
        }
    }

    public string EnviarBoleto(GestaoCobrancaApiModel.EnviarBoletoApiModel _ObjPesquisar)
    {
        string _templateemail = ConfigurationManager.AppSettings["caminhoTemplateBoletoEmail"];
        string _linkBoleto = ConfigurationManager.AppSettings["SisBoleto"];
        string _linkBoletoNSF = "";
        string Message = "";


        try
        {

            string _idparaboleto = "";

            if (_ObjPesquisar.CODEMP == 0)
                return "";


            if (_ObjPesquisar.CODFILEMP == 0)
                return "";


            if (_ObjPesquisar.NUMNOTFSCSVC == 0)
                return "";

            if (_ObjPesquisar.DATIPRNOTFSCSVC == null)
                return "";


            wsNFSe ws = new wsNFSe();
            if (ws != null)
                _idparaboleto = ws.getKeyNFSEemail(_ObjPesquisar.CODEMP, _ObjPesquisar.CODFILEMP, _ObjPesquisar.NUMNOTFSCSVC, DateTime.Parse(_ObjPesquisar.DATIPRNOTFSCSVC), 1);




            _linkBoleto = _linkBoleto + _idparaboleto;

            _linkBoletoNSF = "RPS: " + _ObjPesquisar.NUMNOTFSCSVC.ToString().PadLeft(10,'0') + " - NFSe: " + _ObjPesquisar.NUMNOTFSCSVC.ToString().PadLeft(10, '0');

            Message = GetTemplateHtml(_templateemail);
            Message = Message.Replace("###linkBoletoNSF", _linkBoletoNSF);
            Message = Message.Replace("###linkBoleto", _linkBoleto);

            if (Arquitetura.Classes.Util.EnviaEmailMartins(_ObjPesquisar.EMAIL.Trim(), null, "NFS-e Martins Com Serv Distr Sa", Message, null, Arquitetura.Classes.Util.TipoEmail.HTML))
            {

            }


            return "1";
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO OBTER VERIFICAR DISTRIBUIÇÃO");
            Utilitario.InsereLog(ex, "obterTotaisEquipes", " ", "GestaoCobracaBLL", "ERRO AO OBTER VERIFICAR DISTRIBUIÇÃO");
            throw;
        }

    }

    public static string GetTemplateHtml(string path)
    {
        var logicPath = System.Web.HttpContext.Current.Server.MapPath(path);
        return System.IO.File.ReadAllText(logicPath);
    }


    public string AlterarStatusTitulo(GestaoCobrancaApiModel.AlteraStatusTitulosApiModel _ObjPesquisar)
    {
        string _resulto = "0";
        GestaoCobrancaTO.TitulosResponsavel _existeresp = new GestaoCobrancaTO.TitulosResponsavel();


        var DAL = new GestaoCobrancaDAL();
        try
        {

            if (_ObjPesquisar.DESOBS != null)
            {
                var _codigofunc = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
                var dalAprovador = new AprovacaoDAL();
                AprovacaoTO.obterEmailAprovador _aprovador;
                _aprovador = dalAprovador.obterEmailAprovador(_codigofunc);


                if (_ObjPesquisar.DESOBS != "")
                {
                    string _tipostatus = "";

                    if (_ObjPesquisar.CODSTATIT == 2)
                        _tipostatus = "PAGO";
                    else if (_ObjPesquisar.CODSTATIT == 3)
                        _tipostatus = "ATRASO";
                    else if (_ObjPesquisar.CODSTATIT == 5)
                        _tipostatus = "PREJUÍZO";
                    else if (_ObjPesquisar.CODSTATIT == 4)
                        _tipostatus = "CANCELADO";


                    var DALANOTACAO = new CadastroAnotacaoDAL();
                    CadastroAnotacaoApiModel.CadastroAnotacaoApiModel _itemAnotacao = new CadastroAnotacaoApiModel.CadastroAnotacaoApiModel();

                    _itemAnotacao.CODCLI = _ObjPesquisar.CODCLI;
                    _itemAnotacao.CODOPTVND = -1;

                    _itemAnotacao.DESOBS = "Título alterado para " +  _tipostatus + " por " + _aprovador.NOMFNC.Trim() + " para data " + DateTime.Now.ToShortDateString() + ", dia " + DateTime.Now.ToShortDateString() + " às " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Hour.ToString() + " \n Justificativa: " + _ObjPesquisar.DESOBS.Trim();
                    _itemAnotacao.CODMTVCTO = 15;

                    _itemAnotacao.TIPOBSCLI = "11";
                    _itemAnotacao.DATCAD = DateTime.Now.ToString();

                    DALANOTACAO.inserirCadastroAnotacaoGeral(_itemAnotacao);
                }
            }


            if (DAL.AtualizaStatusTitulo(_ObjPesquisar))
            {
               if ((_ObjPesquisar.CODSTATIT != 3) && (_ObjPesquisar.CODSTATIT != 4))
               {
                    if (DAL.VerificaExistetIiTiEmAtraso(_ObjPesquisar.CODCLI) <= 0)
                    {
                        DAL.AtaulizaResponsaveis(0, _ObjPesquisar.CODCLI);
                    }
                }
               _resulto = "1";
            }

            return _resulto;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO ALTERAR STATUS DO TITULO");
            Utilitario.InsereLog(ex, "AlterarStatusTitulo", " ", "GestaoCobrancaBLL", "ERRO AO ALTERAR STATUS DO TITULO");
            throw;
        }

    }

    
    public List<GestaoCobrancaTO.obterServicosAvulsosParcelas> obterServicosAvulsosParcelas(GestaoCobrancaApiModel.GestaoCobrancaApiModel _ObjPesquisar)
    {
        try
        {
            var DAL = new GestaoCobrancaDAL();
            return DAL.obterServicosAvulsosParcelas(_ObjPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO OBTER NOTAS FISCAIS DO CLIENTE");
            Utilitario.InsereLog(ex, "ObterNotasFiscais", " ", "GestaoCobracaBLL", "ERRO AO OBTER NOTAS FISCAIS DO CLIENTE");
            throw;
        }
    }


    public string buscartitulosAvulso(GestaoCobrancaApiModel.GestaoCobrancaApiModel _ObjPesquisar)
    {
        List<GestaoCobrancaTO.ultimoLoteEnviadoAvulso> _ListItens = new List<GestaoCobrancaTO.ultimoLoteEnviadoAvulso>();
        GestaoCobrancaApiModel.GestaoCobrancaApiModel _itemtitulos = new GestaoCobrancaApiModel.GestaoCobrancaApiModel();
        GestaoCobrancaTO.ultimoLoteEnviado _itemLote = new GestaoCobrancaTO.ultimoLoteEnviado();

        bool _existeitens = false;

        try
        {
            var DAL = new GestaoCobrancaDAL();
            _ListItens = DAL.buscaUltimoLoteEnviadoAvulso(_ObjPesquisar);

            if (_ListItens != null)
            {
                if (_ListItens.Count > 0)
                {
                    foreach (GestaoCobrancaTO.ultimoLoteEnviadoAvulso _item in _ListItens)
                    {
                        _itemLote.ANOMESREF = _item.ANOMESREF;
                        _itemLote.NUMLOTPRENOTFSCSVC = _item.NUMLOTPRENOTFSCSVC;

                        var readertitulos = DAL.obterTitulosFatEnviada(_itemLote);

                        using (TransactionScope Scope = new TransactionScope(TransactionScopeOption.Required))
                        {
                            while (readertitulos.Read())
                            {
                                _existeitens = true;
                                _itemtitulos.ANOMESREF = readertitulos.GetInt32(0);
                                _itemtitulos.INDFATSVC = readertitulos.GetInt32(1);
                                _itemtitulos.CODCLI = readertitulos.GetInt32(2);
                                _itemtitulos.NUMTITCOBFAT = readertitulos.GetInt32(3);
                                _itemtitulos.CODFILEMP = readertitulos.GetInt32(4);
                                _itemtitulos.DATVNCTIT = readertitulos.GetDateTime(5).ToString();
                                _itemtitulos.NUMPCLTITNOTFSCSVC = readertitulos.GetInt32(6);
                                _itemtitulos.INDDBTAUTPGT = readertitulos.GetInt32(7);
                                _itemtitulos.VLRPCLTIT = readertitulos.GetDecimal(8);
                                _itemtitulos.CODSTATIT = readertitulos.GetInt32(9);
                                _itemtitulos.NUMNOTFSCSVC = readertitulos.GetInt32(10);
                                _itemtitulos.DATIPRNOTFSCSVC = readertitulos.GetDateTime(11).ToString();
                                _itemtitulos.NUMLOTPRENOTFSCSVC = readertitulos.GetInt64(12);

                                DAL.inserirtitulos(_itemtitulos);
                                DAL.updatenumtitmovcad(_itemtitulos);

                            }

                            Scope.Complete();

                            _itemLote = new GestaoCobrancaTO.ultimoLoteEnviado();
                        }


                        if (!_existeitens)
                            return "1";
                    }
                }
                else
                    return "1";
            }
            else
                return "1";


            return "0";
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DA GESTÃO DE CONTAS.");
            Utilitario.InsereLog(ex, "buscartitulos", " ", "GestaoContasBLL", "ERRO AO FAZER A CONSULTA DA GESTÃO DE CONTAS.");
            throw;
        }
    }

    public bool AtualizaNotaBoleto(Int64 CODCLI)
    {
        try
        {
            var DAL = new GestaoCobrancaDAL();
            return DAL.AtualizaNotaBoleto(CODCLI);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO ATUALIZAR NOTA TABELAS COBRANÇA EXTERNA");
            Utilitario.InsereLog(ex, "AtualizaNotaBoleto", " ", "GestaoCobracaBLL", "ERRO ATUALIZAR NOTA TABELAS COBRANÇA EXTERNA");
            throw;
        }
    }



}