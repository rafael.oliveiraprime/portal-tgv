﻿using System.Collections.Generic;
using System;

namespace GestaoCobrancaTO
{
    public class obterCliTitulos
    {
        public Int64 CODCLICHECK { get; set; }
        public Int64 CODCLI { get; set; }
        public string NOMCLI { get; set; }
        public string NUMCGCCLI { get; set; }
        public string NOMRES { get; set; }
        public string DATAGDCTO { get; set; }
        public decimal VLRSLDTIT { get; set; }
        public string STATUS { get; set; }
        public string FORMAPG { get; set; }
        public string NOMGRPEMPCLI { get; set; }        
    }

    public class ultimoLoteEnviado
    {
        public Int64 ANOMESREF { get; set; }
        public Int64 NUMLOTPRENOTFSCSVC { get; set; }
    }


    public class obterNotasTitulos
    {
        public Int64 CODCLI { get; set; }
        public int NUMNOTFSCSVC { get; set; }
        public int NUMTITCOBFAT { get; set; }
        public string DATVNCTIT { get; set; }
        public decimal VLRPCLTIT { get; set; }
        public int NUMPCLTITNOTFSCSVC { get; set; }
        public decimal VALORPAGO { get; set; }
        public string DATPGTTIT { get; set; }
        public string STATUS { get; set; }
        public string FORMAPAG { get; set; }
        public int CODFILEMP { get; set; }
        public int CODEMP { get; set; }
        public string DATIPRNOTFSCSVC { get; set; }
    }

    public class TotaisEquipe
    {
        public Int64 CODFNC { get; set; }
        public string NOMFNC { get; set; }
        public int TOTAL { get; set; }
    }

    public class ClientesSemResp
    {
        public Int64 CODCLI { get; set; }
    }


    public class CobrancaContato
    {
        public int CODCLI { get; set; }

        public string DESENDETNCTO { get; set; }

        public string NOMCTO { get; set; }

        public string DESTIPCTO { get; set; }

    }


    public class TitulosResponsavel
    {
        public int CODSTATIT { get; set; }

        public int CODRPN { get; set; }
    }



    public class obterServicosAvulsosParcelas
    {
        public int CODCLI { get; set; }

        public string DESSVCTGV { get; set; }

        //public string DESJSTSLCSVC { get; set; }

        public int NUMPCLTITNOTFSCSVC { get; set; }

        public decimal VLRPCLTIT { get; set; }

        public string DATVNCNOTFSCSVC { get; set; }

        public string TIPO { get; set; }
    }

    public class ultimoLoteEnviadoAvulso
    {
        public Int64 ANOMESREF { get; set; }
        public Int64 NUMLOTPRENOTFSCSVC { get; set; }
    }
}