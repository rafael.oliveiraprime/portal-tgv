﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrmApiModel;
using System.Data;

public class GestaoCobrancaDAL : DAL
{

    public GestaoCobrancaTO.ultimoLoteEnviado buscaUltimoLoteEnviado()
    {
        try
        {
            var DALSQL = new GestaoCobrancaDALSQL();
            string cmdSql = DALSQL.buscaUltimoLoteEnviado();

            var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
            //
            dbCommand.TrataDbCommandUniversal(true, true, true, true);
            //
            return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<GestaoCobrancaTO.ultimoLoteEnviado>().FirstOrDefault();
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO ANO MÊS ULTIMA FATURA.");
            Utilitario.InsereLog(ex, "buscartitulos", "", "GestaoCobrancaDAL", "ERRO ANO MÊS ULTIMA FATURA.");
            throw;
        }
    }

    public int VerificaBusca(GestaoCobrancaTO.ultimoLoteEnviado _item)
    {
        var DALSQL = new GestaoCobrancaDALSQL();
        string cmdSql = DALSQL.VerificaBusca();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        dbCommand.AddWithValue("ANOMESREF", _item.ANOMESREF);
        //
        return Convert.ToInt32(MRT001.ExecuteScalar(dbCommand));
    }

    public IDataReader obterTitulosFatEnviada(GestaoCobrancaTO.ultimoLoteEnviado _item)
    {
        var DALSQL = new GestaoCobrancaDALSQL();
        string cmdSql = DALSQL.obterTitulosFatEnviada();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        dbCommand.AddWithValue("ANOMESREF", _item.ANOMESREF);
        dbCommand.AddWithValue("NUMLOTPRENOTFSCSVC", _item.NUMLOTPRENOTFSCSVC);
        //
        return MRT001.ExecuteReader(dbCommand);
    }

    public bool inserirtitulos(GestaoCobrancaApiModel.GestaoCobrancaApiModel _objInserir)
    {
        var DALSQL = new GestaoCobrancaDALSQL();
        string cmdSql = DALSQL.inserirtitulos();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", _objInserir.ANOMESREF);
        dbCommand.AddWithValue("INDFATSVC", _objInserir.INDFATSVC);
        dbCommand.AddWithValue("CODCLI", _objInserir.CODCLI);
        dbCommand.AddWithValue("NUMTITCOBFAT", _objInserir.NUMTITCOBFAT);
        dbCommand.AddWithValue("CODFILEMP", _objInserir.CODFILEMP);
        dbCommand.AddWithValue("DATVNCTIT", DateTime.Parse(_objInserir.DATVNCTIT));
        dbCommand.AddWithValue("NUMPCLTITNOTFSCSVC", _objInserir.NUMPCLTITNOTFSCSVC);
        dbCommand.AddWithValue("INDDBTAUTPGT", _objInserir.INDDBTAUTPGT);
        dbCommand.AddWithValue("VLRPCLTIT", _objInserir.VLRPCLTIT);
        dbCommand.AddWithValue("CODSTATIT", _objInserir.CODSTATIT);
        dbCommand.AddWithValue("VLRSLDTIT", _objInserir.VLRPCLTIT);
        dbCommand.AddWithValue("NUMLOTPRENOTFSCSVC", _objInserir.NUMLOTPRENOTFSCSVC);

        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool updatenumtitmovcad(GestaoCobrancaApiModel.GestaoCobrancaApiModel _objInserir)
    {
        var DALSQL = new GestaoCobrancaDALSQL();
        string cmdSql = DALSQL.updateMovcad();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", _objInserir.ANOMESREF);
        dbCommand.AddWithValue("CODCLI", _objInserir.CODCLI);
        dbCommand.AddWithValue("DATIPRNOTFSCSVC", DateTime.Parse(_objInserir.DATIPRNOTFSCSVC));
        dbCommand.AddWithValue("NUMNOTFSCSVC", _objInserir.NUMNOTFSCSVC);

        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public List<GestaoCobrancaTO.obterCliTitulos> obterCliTitulos(GestaoCobrancaApiModel.GestaoCobrancaApiModel _ObjPesquisar)
    {
        var DALSQL = new GestaoCobrancaDALSQL();
        string cmdSql = DALSQL.obterCliTitulos();
        //

        if (_ObjPesquisar.STATUS == 0)
            _ObjPesquisar.STATUS = -1;

        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", _ObjPesquisar.CODCLI);
        dbCommand.AddWithValue("NOMCLI", _ObjPesquisar.NOMCLI);
        dbCommand.AddWithValue("NUMCGCCLI", _ObjPesquisar.NUMCGCCLI);
        dbCommand.AddWithValue("CODRPN", _ObjPesquisar.CODRPN);
        dbCommand.AddWithValue("STATUS", _ObjPesquisar.STATUS);
        dbCommand.AddWithValue("CODGRPEMPCLI", _ObjPesquisar.CODGRPEMPCLI);
        dbCommand.AddWithValue("ANOMESREF", _ObjPesquisar.ANOMESREF);

        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<GestaoCobrancaTO.obterCliTitulos>();
    }

    public List<GestaoCobrancaTO.obterNotasTitulos> ObterNotasFiscais(GestaoCobrancaApiModel.GestaoCobrancaApiModel _ObjPesquisar)
    {
        var DALSQL = new GestaoCobrancaDALSQL();
        string cmdSql = DALSQL.ObterNotasFiscais();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", _ObjPesquisar.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<GestaoCobrancaTO.obterNotasTitulos>();
    }


    public bool AtualizaResponsavel(GestaoCobrancaApiModel.GestaoCobrancaTranfResponsavelApiModel _ObjTranferrir)
    {
        var DALSQL = new GestaoCobrancaDALSQL();
        string cmdSql = DALSQL.AtualizaResponsavel(_ObjTranferrir.LISTCODCLI);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODRPN", _ObjTranferrir.CODRESPONSAVEL);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public List<GestaoCobrancaTO.TotaisEquipe> obterTotaisEquipes()
    {
        var DALSQL = new GestaoCobrancaDALSQL();
        string cmdSql = DALSQL.obterTotaisEquipes();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<GestaoCobrancaTO.TotaisEquipe>();
    }



    public List<GestaoCobrancaTO.ClientesSemResp> buscaClientesSemResponsavel()
    {
        var DALSQL = new GestaoCobrancaDALSQL();
        string cmdSql = DALSQL.buscaClientesSemResponsavel();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<GestaoCobrancaTO.ClientesSemResp>();
    }

    public List<GestaoCobrancaTO.CobrancaContato> obterCobrancaContato(GestaoCobrancaApiModel.GestaoCobrancaApiModel _ObjPesquisar)
    {
        var DALSQL = new GestaoCobrancaDALSQL();
        string cmdSql = DALSQL.obterCobrancaContato();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", _ObjPesquisar.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<GestaoCobrancaTO.CobrancaContato>();
    }


    public Int64 obterTotalContato()
    {
        var DALSQL = new GestaoCobrancaDALSQL();
        string cmdSql = DALSQL.obterTotalContato();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return Convert.ToInt64(MRT001.ExecuteScalar(dbCommand));

    }

    public bool AtualizaStatusTitulo(GestaoCobrancaApiModel.AlteraStatusTitulosApiModel _ObjPesquisar)
    {
        var DALSQL = new GestaoCobrancaDALSQL();
        string cmdSql = DALSQL.AtualizaStatusTitulo(_ObjPesquisar.LISTTITULOS, _ObjPesquisar.CODSTATIT);
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);

        dbCommand.AddWithValue("CODSTATIT", _ObjPesquisar.CODSTATIT);
        dbCommand.AddWithValue("CODCLI", _ObjPesquisar.CODCLI);

        if (_ObjPesquisar.CODSTATIT == 2)
            dbCommand.AddWithValue("DATPGTTIT", DateTime.Parse(_ObjPesquisar.DATPGTTIT));

        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public GestaoCobrancaTO.TitulosResponsavel VerificaExisteResponsavel(Int64 CODCLI)
    {
        var DALSQL = new GestaoCobrancaDALSQL();
        string cmdSql = DALSQL.VerificaExisteResponsavel();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", CODCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<GestaoCobrancaTO.TitulosResponsavel>().FirstOrDefault();
    }


    public bool AtaulizaResponsaveis(Int64 CODRPN, Int64 CODCLI)
    {
        var DALSQL = new GestaoCobrancaDALSQL();
        string cmdSql = DALSQL.AtualizaResponsaveis();

        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);

        dbCommand.AddWithValue("CODRPN", CODRPN);
        dbCommand.AddWithValue("CODCLI", CODCLI);

        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public Int64 VerificaExistetIiTiEmAtraso(Int64 CODCLI)
    {
        var DALSQL = new GestaoCobrancaDALSQL();
        string cmdSql = DALSQL.VerificaExistetIiTiEmAtraso();

        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);

        dbCommand.AddWithValue("CODCLI", CODCLI);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToInt64(MRT001.ExecuteScalar(dbCommand));


    }

    public List<GestaoCobrancaTO.obterServicosAvulsosParcelas> obterServicosAvulsosParcelas(GestaoCobrancaApiModel.GestaoCobrancaApiModel _ObjPesquisar)
    {
        
        var DALSQL = new GestaoCobrancaDALSQL();
        string cmdSql = DALSQL.obterServicosAvulsosParcelas();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", _ObjPesquisar.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<GestaoCobrancaTO.obterServicosAvulsosParcelas>();
    

    }

    public List<GestaoCobrancaTO.ultimoLoteEnviadoAvulso> buscaUltimoLoteEnviadoAvulso(GestaoCobrancaApiModel.GestaoCobrancaApiModel _ObjPesquisar)
    {
        var DALSQL = new GestaoCobrancaDALSQL();
        string cmdSql = DALSQL.buscaUltimoLoteEnviadoAvulso();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", _ObjPesquisar.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<GestaoCobrancaTO.ultimoLoteEnviadoAvulso>();
    }

    public bool AtualizaNotaBoleto(Int64 CODCLI)
    {
        var DALSQL = new GestaoCobrancaDALSQL();

        string cmdSql = DALSQL.AtualizaNotaBoleto();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue(":CODDSNNOTFSC", CODCLI);
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


}