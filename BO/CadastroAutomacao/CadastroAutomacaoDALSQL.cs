﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class CadastroAutomacaoDALSQL
{
    public string obterCadastroAutomacao(int flgAtivo, int codEmpPauAut)
    {
        StringBuilder strBld = new StringBuilder(@"
                 SELECT AUT.CODEMPAUT, AUT.CODEMPAUT CODEMPAUTCHECK, TRIM(AUT.RAZSOCEMP) AS RAZSOCEMP, TRIM(AUT.NOMFNT) AS NOMFNT, 
                 CASE NUMCGCEMP WHEN NUMCGCEMP THEN (SUBSTR(AUT.NUMCGCEMP, 1, 2) || '.' || 
                 SUBSTR(AUT.NUMCGCEMP, 3, 3) || '.' ||
                 SUBSTR(AUT.NUMCGCEMP, 6, 3) || '/' || SUBSTR(AUT.NUMCGCEMP, 9,4) || '-' ||
                 SUBSTR(AUT.NUMCGCEMP, 13,2) ) ELSE NULL END NUMCGCEMP, 
                 AUT.DESENDLGR, AUT.CODPAC, 
                 CASE WHEN AUT.CODPAC != 0 THEN TIP.NOMPAC END NOMPAC,
                 CASE AUT.CODFNCDST WHEN AUT.CODFNCDST THEN 'DESATIVADO' ELSE 'ATIVADO' END STATUS,                 
                 TRIM(FNC_CAD.NOMFNC) FNCCAD,
                 TO_CHAR(AUT.DATCAD,'DD/MM/YYYY') DATFNCCAD,
                 TRIM(FNC_ALT.NOMFNC) FNCALT,
                 TO_CHAR(AUT.DATALT,'DD/MM/YYYY') DATFNCALT,
                 CASE AUT.DATDST WHEN AUT.DATDST THEN TRIM(FNC_DST.NOMFNC) ELSE NULL END FNCDST,
                 CASE AUT.DATDST WHEN AUT.DATDST THEN TO_CHAR(AUT.DATDST,'DD/MM/YYYY') ELSE NULL END DATFNCDST, CLI.CODCLI
                 FROM MRT.CADEMPAUTTGV AUT
                 INNER JOIN MRT.CADTIPPACTGV TIP ON TIP.CODPAC = AUT.CODPAC                 
                 LEFT JOIN MRT.T0100361 FNC_CAD ON FNC_CAD.CODFNC = AUT.CODFNCCAD
                 LEFT JOIN MRT.T0100361 FNC_ALT ON FNC_ALT.CODFNC = AUT.CODFNCALT
                 LEFT JOIN MRT.T0100361 FNC_DST ON FNC_DST.CODFNC = AUT.CODFNCDST 
                 LEFT JOIN MRT.T0100043 CLI ON CLI.NUMCGCCLI = AUT.NUMCGCEMP
                 WHERE 1 = 1
                 AND AUT.CODEMPAUT = :CODEMPAUT 
                 AND AUT.NUMCGCEMP = :NUMCGCEMP
                ");

        if (flgAtivo == 1)
        {
            strBld.AppendLine(" AND AUT.DATDST IS NULL ");

        }
        if (flgAtivo == 2)
        {
            strBld.AppendLine(" AND AUT.DATDST IS NOT NULL ");
        }

        strBld.AppendLine("ORDER BY STATUS, AUT.CODEMPAUT");

        return strBld.ToString();
    }

    public string inserirCadastroAutomacao()
    {
        return @"
                 INSERT INTO MRT.CADEMPAUTTGV (CODEMPAUT, RAZSOCEMP, NOMFNT, NUMCGCEMP, DESENDLGR, 
                 CODPAC, DATCAD, CODFNCCAD, DATALT, CODFNCALT)
                 VALUES ((SELECT COALESCE(MAX(CODEMPAUT),0)+1 FROM MRT.CADEMPAUTTGV),                 
                 UPPER(:RAZSOCEMP),UPPER(:NOMFNT), :NUMCGCEMP, UPPER(:DESENDLGR), :CODPAC, SYSDATE, 
                 :CODFNCCAD, SYSDATE, :CODFNCALT)
                ";
    }

    public string alterarCadastroAutomacao()
    {
        return @"
                 UPDATE MRT.CADEMPAUTTGV SET
                 RAZSOCEMP = UPPER(:RAZSOCEMP),
                 NOMFNT = UPPER(:NOMFNT),
                 NUMCGCEMP = :NUMCGCEMP,
                 DESENDLGR = UPPER(:DESENDLGR),
                 CODPAC = :CODPAC,
                 CODFNCALT = :CODFNCALT,
                 DATALT = SYSDATE                
                 WHERE CODEMPAUT = :CODEMPAUT
                ";
    }

    public string verificarCadastroAutomacaoCNPJ()
    {
        StringBuilder strBld = new StringBuilder(@"
                 SELECT COUNT(NUMCGCEMP) FROM MRT.CADEMPAUTTGV 
                 WHERE NUMCGCEMP = :NUMCGCEMP
                ");
        return strBld.ToString();
    }

    public string obterAutomacaoSistema()
    {
        return @"
                 SELECT AUT.CODEMPAUT, UPPER(TRIM(AUT.RAZSOCEMP)) AS RAZSOCEMP, UPPER(TRIM(AUT.NOMFNT)) AS NOMFNT, 
                 CASE NUMCGCEMP WHEN NUMCGCEMP THEN (SUBSTR(AUT.NUMCGCEMP, 1, 2) || '.' || 
                 SUBSTR(AUT.NUMCGCEMP, 3, 3) || '.' ||
                 SUBSTR(AUT.NUMCGCEMP, 6, 3) || '/' || SUBSTR(AUT.NUMCGCEMP, 9,4) || '-' ||
                 SUBSTR(AUT.NUMCGCEMP, 13,2) ) ELSE NULL END NUMCGCEMP, 
                 UPPER(TRIM(AUT.DESENDLGR)) AS DESENDLGR, AUT.CODPAC, 
                 CASE WHEN AUT.CODPAC != 0 THEN TIP.NOMPAC END NOMPAC,              
                 TRIM(FNC_CAD.NOMFNC) || ' em ' || AUT.DATCAD DATFNCCAD,
                 TRIM(FNC_ALT.NOMFNC) || ' em ' || AUT.DATALT DATFNCALT,
                 CASE FNC_DST.CODFNC WHEN FNC_DST.CODFNC THEN TRIM(FNC_DST.NOMFNC) || ' em ' || AUT.DATDST ELSE NULL END DATFNCDST,
                 CLI.CODCLI
                 FROM MRT.CADEMPAUTTGV AUT
                 INNER JOIN MRT.CADTIPPACTGV TIP ON TIP.CODPAC = AUT.CODPAC                 
                 LEFT JOIN MRT.T0100361 FNC_CAD ON FNC_CAD.CODFNC = AUT.CODFNCCAD
                 LEFT JOIN MRT.T0100361 FNC_ALT ON FNC_ALT.CODFNC = AUT.CODFNCALT
                 LEFT JOIN MRT.T0100361 FNC_DST ON FNC_DST.CODFNC = AUT.CODFNCDST 
                 LEFT JOIN MRT.T0100043 CLI ON CLI.NUMCGCCLI = AUT.NUMCGCEMP                
                 WHERE TRIM(AUT.RAZSOCEMP) = UPPER(TRIM(:RAZSOCEMP))
                 AND TRIM(NUMCGCEMP) = TRIM(:NUMCGCEMP)
                ";        
    }  

    public string ativarCadastroAutomacao(List<long> LISTCODEMPAUT)
    {

        return @"
                 UPDATE MRT.CADEMPAUTTGV SET                 
                 CODFNCDST = NULL,
                 DATDST = NULL
                 WHERE CODEMPAUT IN(#LISTCODEMPAUT)
                ".Replace("#LISTCODEMPAUT", Utilitario.ToListStr(LISTCODEMPAUT));
    }

    public string desativarCadastroAutomacao(List<long> LISTCODEMPAUT)
    {
        return @"
                 UPDATE MRT.CADEMPAUTTGV SET                                 
                 CODFNCDST = :CODFNCDST,
                 DATDST = SYSDATE
                 WHERE CODEMPAUT IN(#LISTCODEMPAUT)
                ".Replace("#LISTCODEMPAUT", Utilitario.ToListStr(LISTCODEMPAUT));
    }


    public string ObterRelAutomocao()
    {
        return @" SELECT GC.CODCLI 
                         ,TRIM(CLI.NOMCLI) AS NOMCLI
                         ,AU.RAZSOCEMP
                    FROM MRT.CADCLITGV GC
                   INNER JOIN MRT.T0100043 CLI ON CLI.CODCLI = GC.CODCLI   
                   INNER JOIN MRT.CADEMPAUTTGV AU ON GC.CODEMPAUT = AU.CODEMPAUT
                   WHERE 1 = 1 
                     AND GC.CODCLI = :CODCLI
                     AND AU.CODEMPAUT = :CODEMPAUT
                     AND CLI.NOMCLI LIKE '%' || UPPER(:NOMCLI) || '%'
                     AND CLI.NUMCGCCLI = :NUMCGCCLI

                 ";

    }

}