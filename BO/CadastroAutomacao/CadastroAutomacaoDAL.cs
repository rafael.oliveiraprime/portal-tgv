﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

public class CadastroAutomacaoDAL : DAL
{
    public List<CadastroAutomacaoTO.obterCadastroAutomacao> obterCadastroAutomacao(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objPesquisar)
    {
        string NUMCGCEMP;           

        if (objPesquisar.NUMCGCEMP == "-1")
        {
            objPesquisar.NUMCGCEMP = "";
            NUMCGCEMP = null;
        }        

        if (objPesquisar.NUMCGCEMP != null)
        {
            NUMCGCEMP = Regex.Replace(objPesquisar.NUMCGCEMP, "[/.-]", String.Empty);
            NUMCGCEMP = NUMCGCEMP + " ";
        }
        else
        {
            NUMCGCEMP = null;
        }

        var DALSQL = new CadastroAutomacaoDALSQL();
        string cmdSql = DALSQL.obterCadastroAutomacao(objPesquisar.AUTATI, objPesquisar.CODEMPAUT);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODEMPAUT", objPesquisar.CODEMPAUT);
        dbCommand.AddWithValue("NUMCGCEMP", NUMCGCEMP);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroAutomacaoTO.obterCadastroAutomacao>();
    }

    public bool inserirCadastroAutomacao(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objInserir)
    {
        var DALSQL = new CadastroAutomacaoDALSQL();
        string cmdSql = DALSQL.inserirCadastroAutomacao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        objInserir.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        objInserir.CODFNCCAD = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //                    
        if(objInserir.CODPAC == -1)
        {
            objInserir.CODPAC = 0;
        }
        //
        string NUMCGCEMP;

        if (objInserir.NUMCGCEMP != null)
        {
            NUMCGCEMP = Regex.Replace(objInserir.NUMCGCEMP, "[/.-]", String.Empty);
        }
        else
        {
            NUMCGCEMP = null;
        }
        //
        dbCommand.AddWithValue("RAZSOCEMP", objInserir.RAZSOCEMP);
        dbCommand.AddWithValue("NOMFNT", objInserir.NOMFNT);
        dbCommand.AddWithValue("NUMCGCEMP", NUMCGCEMP);
        dbCommand.AddWithValue("DESENDLGR", objInserir.DESENDLGR);
        dbCommand.AddWithValue("CODPAC", objInserir.CODPAC);
        dbCommand.AddWithValue("CODFNCCAD", objInserir.CODFNCCAD);
        dbCommand.AddWithValue("CODFNCALT", objInserir.CODFNCALT);       
        //              
        dbCommand.TrataDbCommandUniversal();

        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool alterarCadastroAutomacao(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objAlterar)
    {
        var DALSQL = new CadastroAutomacaoDALSQL();
        string cmdSql = DALSQL.alterarCadastroAutomacao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        objAlterar.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;

        string NUMCGCEMP;

        if (objAlterar.NUMCGCEMP != null) { 
        NUMCGCEMP = Regex.Replace(objAlterar.NUMCGCEMP, "[/.-]", String.Empty);
        }
        else
        {
            NUMCGCEMP = null;
        }
        //
        dbCommand.AddWithValue("CODEMPAUT", objAlterar.CODEMPAUT);
        dbCommand.AddWithValue("RAZSOCEMP", objAlterar.RAZSOCEMP);
        dbCommand.AddWithValue("NOMFNT", objAlterar.NOMFNT);
        dbCommand.AddWithValue("NUMCGCEMP", NUMCGCEMP);
        dbCommand.AddWithValue("DESENDLGR", objAlterar.DESENDLGR);
        dbCommand.AddWithValue("CODPAC", objAlterar.CODPAC);
        dbCommand.AddWithValue("CODFNCALT", objAlterar.CODFNCALT);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }
    

    public int VerificaCNPJ(string NUMCGCEMP)
    {
        if (NUMCGCEMP !="")
        {
            NUMCGCEMP = Regex.Replace(NUMCGCEMP, "[/.-]", String.Empty);
            NUMCGCEMP = NUMCGCEMP + " ";
        }
        else
        {
            NUMCGCEMP = null;
        }

        var DALSQL = new CadastroAutomacaoDALSQL();
        string cmdSql = DALSQL.verificarCadastroAutomacaoCNPJ();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("NUMCGCEMP", NUMCGCEMP);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return Convert.ToInt32(MRT001.ExecuteScalar(dbCommand));    
    }

    public CadastroAutomacaoTO.obterAutomacaoSistema obterAutomacaoSistema(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objPesquisar)
    {
        string NUMCGCEMP;      

        if (objPesquisar.NUMCGCEMP == "-1")
        {
            objPesquisar.NUMCGCEMP = "";
            NUMCGCEMP = null;
        }

        if (objPesquisar.NUMCGCEMP != null)
        {
            NUMCGCEMP = Regex.Replace(objPesquisar.NUMCGCEMP, "[/.-]", String.Empty);
            NUMCGCEMP = NUMCGCEMP + " ";
        }
        else
        {
            NUMCGCEMP = null;
        }

        var DALSQL = new CadastroAutomacaoDALSQL();
        string cmdSql = DALSQL.obterAutomacaoSistema();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("RAZSOCEMP", objPesquisar.RAZSOCEMP);
        dbCommand.AddWithValue("NUMCGCEMP", NUMCGCEMP);
        //
        dbCommand.TrataDbCommandUniversal();
        //      
        var retorno = MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroAutomacaoTO.obterAutomacaoSistema>();
        if (retorno.Count > 0)
        {
            return retorno.FirstOrDefault();
        }
        else
        {
            return new CadastroAutomacaoTO.obterAutomacaoSistema();
        }
    }

    public bool ativarCadastroAutomacao(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objAlterar)
    {
        var DALSQL = new CadastroAutomacaoDALSQL();
        string cmdSql = DALSQL.ativarCadastroAutomacao(objAlterar.LISTCODEMPAUT);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //                 
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool desativarCadastroAutomacao(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objAlterar)
    {
        var DALSQL = new CadastroAutomacaoDALSQL();
        string cmdSql = DALSQL.desativarCadastroAutomacao(objAlterar.LISTCODEMPAUT);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCDST = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //                     
        dbCommand.AddWithValue("CODFNCDST", objAlterar.CODFNCDST);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public List<CadastroAutomacaoTO.obterRelAutomacao> obterRelAutomacao(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objPesquisar)
    {
        string NUMCGCEMP;

        if (objPesquisar.NUMCGCEMP == "-1")
        {
            objPesquisar.NUMCGCEMP = "";
            NUMCGCEMP = null;
        }

        if (objPesquisar.NUMCGCEMP != null)
        {
            NUMCGCEMP = Regex.Replace(objPesquisar.NUMCGCEMP, "[/.-]", String.Empty);
            //NUMCGCEMP = NUMCGCEMP;
        }
        else
        {
            NUMCGCEMP = null;
        }

        var DALSQL = new CadastroAutomacaoDALSQL();
        string cmdSql = DALSQL.ObterRelAutomocao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("CODEMPAUT", objPesquisar.CODEMPAUT);
        dbCommand.AddWithValue("NOMCLI", objPesquisar.NOMCLI);
        dbCommand.AddWithValue("NUMCGCCLI", NUMCGCEMP);

        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroAutomacaoTO.obterRelAutomacao>();
    }



}