﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CadastroAutomacaoTO
{
    public class obterCadastroAutomacao
    {
        public int CODEMPAUT { get; set; }

        public int CODEMPAUTCHECK { get; set; }

        public string RAZSOCEMP { get; set; }

        public string NOMFNT { get; set; }

        public string NUMCGCEMP { get; set; }

        public int CODCLI { get; set; }

        public string DESENDLGR { get; set; }

        public int CODPAC { get; set; }

        public string NOMPAC { get; set; }

        public string STATUS { get; set; }

        public string FNCCAD { get; set; }

        public string DATFNCCAD { get; set; }

        public string FNCALT { get; set; }

        public string DATFNCALT { get; set; }

        public string FNCDST { get; set; }

        public string DATFNCDST { get; set; }
    }

    public class obterAutomacaoSistema
    {
        public int CODEMPAUT { get; set; }

        public string RAZSOCEMP { get; set; }

        public string NOMFNT { get; set; }

        public string NUMCGCEMP { get; set; }

        public int CODCLI { get; set; }

        public string DESENDLGR { get; set; }

        public int CODPAC { get; set; }

        public string DATFNCCAD { get; set; }

        public string DATFNCALT { get; set; }

        public string DATFNCDST { get; set; }
    }


    public class obterRelAutomacao
    {
        public int CODCLI { get; set; }

        public string NOMCLI { get; set; }

        public string RAZSOCEMP { get; set; }

    }

}