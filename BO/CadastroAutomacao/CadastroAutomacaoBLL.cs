﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CadastroAutomacaoBLL
{
    public List<CadastroAutomacaoTO.obterCadastroAutomacao> obterCadastroAutomacao(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroAutomacaoDAL();
            return DAL.obterCadastroAutomacao(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO AUTOMACAO.");
            Utilitario.InsereLog(ex, "obterCadastroAutomacao", "CadastroAutomacaoApiModel.CadastroAutomacaoApiModel", "CadastroAutomacaoBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO AUTOMACAO.");
            throw;
        }
    }

    //public List<CadastroAutomacaoTO.obterCadastroAutomacao> obterCadastroAutomacao(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objPesquisar)
    //{
    //    try
    //    {
    //        var DAL = new CadastroAutomacaoDAL();

    //        List<CadastroAutomacaoTO.obterCadastroAutomacao> automacoes = DAL.obterCadastroAutomacao(objPesquisar);

    //        var combosDAL = new CombosDAL();

    //        foreach (CadastroAutomacaoTO.obterCadastroAutomacao aut in automacoes)
    //        {
    //            var sistemas = combosDAL.obterSistemasCodigosAutomacoes(new SistemasCodigosAutomacoesApiModel.SistemasCodigosAutomacoesApiModel()
    //            {
    //                CODEMPAUT = aut.CODEMPAUT
    //            });

    //            string htmlAux = string.Empty;

    //            foreach (CombosTO.obterSistemasCodigosAutomacoes sist in sistemas)
    //            {
    //                htmlAux = string.Format("{0}{1}", htmlAux, string.Format("<option>{0} - {1}</option>", sist.CODSISINF, sist.NOMSIS));
    //            }
    //            aut.SISTEMAS = htmlAux;
    //        }
    //        return automacoes;
    //    }
    //    catch (Exception ex)
    //    {
    //        Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO AUTOMACAO.");
    //        Utilitario.InsereLog(ex, "obterCadastroAutomacao", "CadastroAutomacaoApiModel.CadastroAutomacaoApiModel", "CadastroAutomacaoBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO AUTOMACAO.");
    //        throw;
    //    }
    //}

    //public CadastroAutomacaoTO.obterCadastroAutomacao atualizarSistema(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objPesquisar)
    //{
    //    try
    //    {
    //        var DAL = new CadastroAutomacaoDAL();

    //        CadastroAutomacaoTO.obterCadastroAutomacao automacao = DAL.obterCadastroAutomacao(objPesquisar).FirstOrDefault();

    //        if (automacao != null)
    //        {
    //            var combosDAL = new CombosDAL();

    //            var sistemas = combosDAL.obterSistemasCodigosAutomacoes(new SistemasCodigosAutomacoesApiModel.SistemasCodigosAutomacoesApiModel()
    //            {
    //                CODEMPAUT = automacao.CODEMPAUT
    //            });

    //            string htmlAux = string.Empty;

    //            foreach (CombosTO.obterSistemasCodigosAutomacoes sist in sistemas)
    //            {
    //                htmlAux = string.Format("{0}{1}", htmlAux, string.Format("<option>{0} - {1}</option>", sist.CODSISINF, sist.NOMSIS));
    //            }

    //            automacao.SISTEMAS = htmlAux;
    //        }

    //        return automacao;
    //    }
    //    catch (Exception ex)
    //    {
    //        Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATUALIZACAO DOS SISTEMAS NA TELA DE CADASTRO AUTOMACAO.");
    //        Utilitario.InsereLog(ex, "atualizarSistema", "CadastroAutomacaoApiModel.CadastroAutomacaoApiModel", "CadastroAutomacaoBLL", "ERRO AO FAZER A ATUALIZACAO DOS SISTEMAS NA TELA DE CADASTRO AUTOMACAO.");
    //        throw;
    //    }
    //}

    public bool inserirCadastroAutomacao(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objInserir)
    {
        try
        {
            var DAL = new CadastroAutomacaoDAL();
            return DAL.inserirCadastroAutomacao(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO CADASTRO AUTOMACAO.");
            Utilitario.InsereLog(ex, "inserirCadastroAutomacao", "CadastroAutomacaoApiModel.CadastroAutomacaoApiModel", "CadastroAutomacaoBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO AUTOMACAO.");
            throw;
        }
    }

    public bool alterarCadastroAutomacao(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroAutomacaoDAL();
            return DAL.alterarCadastroAutomacao(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO CADASTRO AUTOMACAO.");
            Utilitario.InsereLog(ex, "alterarCadastroAutomacao", "CadastroAutomacaoApiModel.CadastroAutomacaoApiModel", "CadastroAutomacaoBLL", "ERRO AO FAZER A ALTERACAO DO CADASTRO AUTOMACAO.");
            throw;
        }
    }

    public int VerificaCNPJ(string NUMCGCEMP)
    {
        try
        {
            var DAL = new CadastroAutomacaoDAL();
            return DAL.VerificaCNPJ(NUMCGCEMP);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A VERIFICAÇÃO DO CADASTRO AUTOMACAO POR CNPJ.");
            Utilitario.InsereLog(ex, "VerificaCNPJ", "NUMCGCEMP", "CadastroAutomacaoBLL", "ERRO AO FAZER A VERIFICAÇÃO DO CADASTRO AUTOMACAO POR CNPJ.");
            throw;
        }
    }

    public CadastroAutomacaoTO.obterAutomacaoSistema obterAutomacaoSistema(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroAutomacaoDAL();
            return DAL.obterAutomacaoSistema(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DA AUTOMACAO SISTEMA.");
            Utilitario.InsereLog(ex, "obterAutomacaoSistema", "CadastroAutomacaoApiModel.CadastroAutomacaoApiModel", "CadastroAutomacaoBLL", "ERRO AO FAZER A CONSULTA DA AUTOMACAO SISTEMA.");
            throw;
        }
    }

    public bool ativarCadastroAutomacao(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroAutomacaoDAL();
            return DAL.ativarCadastroAutomacao(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATIVACAO DO CADASTRO AUTOMACAO.");
            Utilitario.InsereLog(ex, "ativarCadastroAutomacao", "CadastroAutomacaoApiModel.CadastroAutomacaoApiModel", "CadastroAutomacaoBLL", "ERRO AO FAZER A ATIVACAO DO CADASTRO AUTOMACAO.");
            throw;
        }
    }

    public bool desativarCadastroAutomacao(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroAutomacaoDAL();
            return DAL.desativarCadastroAutomacao(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATIVACAO DO CADASTRO AUTOMACAO.");
            Utilitario.InsereLog(ex, "ativarCadastroAutomacao", "CadastroAutomacaoApiModel.CadastroAutomacaoApiModel", "CadastroAutomacaoBLL", "ERRO AO FAZER A ATIVACAO DO CADASTRO AUTOMACAO.");
            throw;
        }
    }


    public List<CadastroAutomacaoTO.obterRelAutomacao> obterRelAutomacao(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroAutomacaoDAL();
            return DAL.obterRelAutomacao(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO AUTOMACAO.");
            Utilitario.InsereLog(ex, "obterCadastroAutomacao", "CadastroAutomacaoApiModel.CadastroAutomacaoApiModel", "CadastroAutomacaoBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO AUTOMACAO.");
            throw;
        }
    }


}