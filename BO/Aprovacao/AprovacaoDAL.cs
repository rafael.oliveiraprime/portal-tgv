﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

public class AprovacaoDAL : DAL
{

    public int obtemStatusOportunidade(int _codopt)
    {
        var DALSQL = new AprovacaoDALSQL();
        string cmdSql = DALSQL.obtemStatusOportunidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("CODOPTVND", _codopt);

        return Convert.ToInt32(MRT001.ExecuteScalar(dbCommand));
    }

    public bool alterarCadastroOportunidade(AprovacaoApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        var DALSQL = new AprovacaoDALSQL();
        int _aprovar = 0;

        if (objAlterar.INDSTAOPTVND == 6)
            _aprovar = 1;

        string cmdSql = DALSQL.alterarCadastroOportunidade(_aprovar);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("INDSTAOPTVND", objAlterar.INDSTAOPTVND);
        dbCommand.AddWithValue("CODFNCAPV", objAlterar.CODFNCAPV);

        if (_aprovar == 1)
        {
            dbCommand.AddWithValue("DATHRAAPV", DateTime.Parse(objAlterar.DATHRAAPV));
            //dbCommand.AddWithValue("DESOBSDSC", objAlterar.DESOBSDSC);
        }
        else
        {
            dbCommand.AddWithValue("DATHRARPV", DateTime.Parse(objAlterar.DATHRARPV));
            dbCommand.AddWithValue("DESMTVRPV", objAlterar.DESMTVRPV);

        }

        dbCommand.AddWithValue("CODOPTVND", objAlterar.CODOPTVND);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public AprovacaoTO.obterEmailAprovador obterEmailAprovador(int _codAprovador)
    {
        var DALSQL = new AprovacaoDALSQL();
        string cmdSql = DALSQL.obterEmailAprovador();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("CODFNC", _codAprovador);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<AprovacaoTO.obterEmailAprovador>().FirstOrDefault();
    }



}