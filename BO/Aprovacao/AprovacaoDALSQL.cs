﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class AprovacaoDALSQL
{

    public string obtemStatusOportunidade()
    {
        return @" SELECT INDSTAOPTVND 
                    FROM MRT.CADOPTVNDCLITGV 
                   WHERE CODOPTVND = :CODOPTVND
                ";
    }


    public string obterEmailAprovador()
    {
        return @" SELECT RE.CODFNC, 
                         FNC_CAD.NOMFNC, 
                         RE.DESUSRRDEMRT || '@martins.com.br' AS EMAILAPROVADOR 
                    FROM MRT.CADUSRRDE RE
                    LEFT JOIN MRT.T0100361 FNC_CAD ON RE.CODFNC = FNC_CAD.CODFNC 
                   WHERE RE.CODFNC = :CODFNC  
                ";

    }

    public string alterarCadastroOportunidade(int _aprova)
    {
        StringBuilder strBld = new StringBuilder("");

        if (_aprova == 1)
        {
            strBld = new StringBuilder(@" UPDATE MRT.CADOPTVNDCLITGV SET INDSTAOPTVND = :INDSTAOPTVND,
                                                                            CODFNCAPV = :CODFNCAPV,
                                                                            DATHRAAPV = :DATHRAAPV
                                                                                WHERE CODOPTVND = :CODOPTVND");
        }
        else
        {
            strBld = new StringBuilder(@" UPDATE MRT.CADOPTVNDCLITGV SET INDSTAOPTVND = :INDSTAOPTVND,
                                                                            CODFNCAPV = :CODFNCAPV,
                                                                            DATHRARPV = :DATHRARPV,
                                                                            DESMTVRPV = :DESMTVRPV
                                                                                WHERE CODOPTVND = :CODOPTVND");
        }

        return strBld.ToString();
    }



}