﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Arquitetura.Classes;
using System.IO;

using Microsoft.Office.Core;
using System.Text;
using System.Configuration;
using System.Globalization;

using System.Text.RegularExpressions;
using DocumentFormat.OpenXml.Packaging;

using DocumentFormat.OpenXml;
using System.Drawing;
using OpenXmlPowerTools;
using System.Xml.Linq;
using DocumentFormat.OpenXml.Wordprocessing;

using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Web.Script.Serialization;

using System.Transactions;


public class AprovacaoBLL
{
    public int alterarCadastroOportunidade(AprovacaoApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        string _dadosLink = "";
        string _templateemail = ConfigurationManager.AppSettings["caminhoContratoTemplateEmailRespostaAprovacao"];

        OportunidadeContatoTO.obterDadosBasicos _dadoscliente;
        var DAL = new AprovacaoDAL();
        var DALOPTCONT = new OportunidadeContatoDAL();


        CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel _itemservico = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
        CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel _itemservicoperdido = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
        var dalservicooportunit = new CadastroServicoOportunidadeDAL();


        try
        {
            CryptUtil _decript = new CryptUtil();
            _dadosLink = _decript.ActionDecrypt(objAlterar.DESCRIPT);

            var codopt = int.Parse(_dadosLink.Split('&')[0].ToString().Split('=')[1]);
            var status = int.Parse(_dadosLink.Split('&')[1].ToString().Split('=')[1]);
            var codoap = int.Parse(_dadosLink.Split('&')[2].ToString().Split('=')[1]);
            int codfnc = int.Parse(_dadosLink.Split('&')[3].ToString().Split('=')[1]);
            int codcli = int.Parse(_dadosLink.Split('&')[4].ToString().Split('=')[1]);

            int _statusAtualopt = DAL.obtemStatusOportunidade(codopt);
            if (_statusAtualopt == 5)
            {
                using (TransactionScope Scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    int _codigoAprovador = int.Parse(ConfigurationManager.AppSettings["codigoAprovador"]);

                    var dalAprovador = new AprovacaoDAL();
                    AprovacaoTO.obterEmailAprovador _aprovador;
                    _aprovador = dalAprovador.obterEmailAprovador(_codigoAprovador);

                    DateTime _datainsercao = DateTime.Now;
                    string _horas = _datainsercao.Hour.ToString() + " : " + _datainsercao.Minute.ToString();

                    if (status != 6)
                    {
                        var DALANOTACAO = new CadastroAnotacaoDAL();
                        CadastroAnotacaoApiModel.CadastroAnotacaoApiModel _itemAnotacao = new CadastroAnotacaoApiModel.CadastroAnotacaoApiModel();

                        _itemAnotacao.CODCLI = codcli;
                        _itemAnotacao.CODOPTVND = codopt;
                        _itemAnotacao.DESOBS = "Desconto Reprovado por " + _aprovador.NOMFNC.Trim() + ", dia " + _datainsercao.ToShortDateString() + " às " + _horas + " \n Justificativa: " + objAlterar.DESMTVRPV.Trim();

                        _itemAnotacao.TIPOBSCLI = "2";
                        _itemAnotacao.CODMTVCTO = 10;
                        _itemAnotacao.DATCAD = _datainsercao.ToString();
                        _itemAnotacao.CODFNCCAD = _codigoAprovador;

                        DALANOTACAO.inserirCadastroAnotacaoGeral(_itemAnotacao);
                    }
                    else
                    {
                        var DALANOTACAO = new CadastroAnotacaoDAL();
                        CadastroAnotacaoApiModel.CadastroAnotacaoApiModel _itemAnotacao = new CadastroAnotacaoApiModel.CadastroAnotacaoApiModel();

                        _itemAnotacao.CODCLI = codcli;
                        _itemAnotacao.CODOPTVND = codopt;
                        _itemAnotacao.DESOBS = "Desconto Aprovado por " + _aprovador.NOMFNC.Trim() + ", dia " + _datainsercao.ToShortDateString() + " às " + _horas;

                        _itemAnotacao.TIPOBSCLI = "2";
                        _itemAnotacao.CODMTVCTO = 10;
                        _itemAnotacao.DATCAD = _datainsercao.ToString();
                        _itemAnotacao.CODFNCCAD = _codigoAprovador;

                        DALANOTACAO.inserirCadastroAnotacaoGeral(_itemAnotacao);
                    }

                    objAlterar.CODOPTVND = codopt;
                    objAlterar.INDSTAOPTVND = status;
                    objAlterar.CODFNCAPV = codoap;

                    if (status == 6)
                        objAlterar.DATHRAAPV = DateTime.Now.ToShortDateString();
                    else
                    {
                        objAlterar.CODFNCAPV = _codigoAprovador;
                        objAlterar.DATHRARPV = _datainsercao.ToString();
                    }

                    string _tipoAprovacao = "";
                    string _aprovacao = "";
                    string Message = "";

                    AprovacaoTO.obterEmailAprovador _cosultor;
                    _cosultor = dalAprovador.obterEmailAprovador(codfnc);

                    if (status == 6)
                    {
                        _tipoAprovacao = "DESCONTO APROVADO";
                        _aprovacao = "APROVADO";
                    }
                    else
                    {
                        _tipoAprovacao = "DESCONTO REJEITADO";
                        _aprovacao = "REJEITADO";
                    }

                    _dadoscliente = DALOPTCONT.obterDadoBasicos(codcli).FirstOrDefault();

                    Message = GetTemplateHtml(_templateemail);
                    Message = Message.Replace("###Oportunidade", codopt.ToString());
                    Message = Message.Replace("###cliente", _dadoscliente.CODCLI.ToString() + " - " + _dadoscliente.NOMCLI);
                    Message = Message.Replace("###Aprovacao", _aprovacao);

                    string Assunto = _tipoAprovacao;

                    Arquitetura.Classes.Util.EnviaEmailMartins(_cosultor.EMAILAPROVADOR, null, Assunto, Message.ToString(), null, Arquitetura.Classes.Util.TipoEmail.HTML);


                    DAL.alterarCadastroOportunidade(objAlterar);
                    Scope.Complete();

                    if (status == 6)
                        return 1;
                    else if (status == 7)
                        return 2;
                    else
                        return 3;
                }
            }
            else
            {
                return 4;
            }
    }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO CADASTRO OPORTUNIDADE. APROVAÇÃO");
            Utilitario.InsereLog(ex, "alterarCadastroOportunidade", "CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel", "AprovacaoBLL", "ERRO AO FAZER A ALTERACAO DO CADASTRO OPORTUNIDADE. APROVAÇÃO");
            throw;
        }
    }

    public AprovacaoTO.obterEmailAprovador obterEmailAprovador(int _codAprovador)
    {
        var DAL = new AprovacaoDAL();
        return DAL.obterEmailAprovador(_codAprovador);         
    }


    public bool Aprovar(AprovacaoApiModel.CadastroOportunidadeApiModel objAlterar)
    {

        CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel _itemservicoperdido = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
        var dalservicooportunit = new CadastroServicoOportunidadeDAL();

        try
        {
            string _templateemail = ConfigurationManager.AppSettings["caminhoContratoTemplateEmailRespostaAprovacao"];
            string Message = "";
            OportunidadeContatoTO.obterDadosBasicos _dadoscliente;
            int _codigoAprovador = int.Parse(ConfigurationManager.AppSettings["codigoAprovador"]);

            var dalAprovador = new AprovacaoDAL();

            AprovacaoTO.obterEmailAprovador _aprovador;
            _aprovador = dalAprovador.obterEmailAprovador(_codigoAprovador);

            DateTime _datainsercao = DateTime.Now;
            string _horas = _datainsercao.Hour.ToString() + " : " + _datainsercao.Minute.ToString();

            using (TransactionScope Scope = new TransactionScope(TransactionScopeOption.Required))
            {

                var DALANOTACAO = new CadastroAnotacaoDAL();
                CadastroAnotacaoApiModel.CadastroAnotacaoApiModel _itemAnotacao = new CadastroAnotacaoApiModel.CadastroAnotacaoApiModel();

                _itemAnotacao.CODCLI = objAlterar.CODCLI;
                _itemAnotacao.CODOPTVND = objAlterar.CODOPTVND;
                _itemAnotacao.DESOBS = "Desconto Aprovado por " + _aprovador.NOMFNC.Trim() + ", dia " + _datainsercao.ToShortDateString() + " às " + _horas;

                _itemAnotacao.TIPOBSCLI = "2";
                _itemAnotacao.CODMTVCTO = 10;
                _itemAnotacao.DATCAD = _datainsercao.ToString();
                DALANOTACAO.inserirCadastroAnotacaoGeral(_itemAnotacao);


                AprovacaoTO.obterEmailAprovador _cosultor;
                int _codigoCosultor = objAlterar.CDFNC;
                //email para o cosultor da negociação
                _cosultor = dalAprovador.obterEmailAprovador(_codigoCosultor);

                var DALOPTCONT = new OportunidadeContatoDAL();
                _dadoscliente = DALOPTCONT.obterDadoBasicos(objAlterar.CODCLI).FirstOrDefault();


                string _aprovacao = "APROVADO";

                Message = GetTemplateHtml(_templateemail);
                Message = Message.Replace("###Oportunidade", objAlterar.CODOPTVND.ToString());
                Message = Message.Replace("###cliente", _dadoscliente.CODCLI.ToString() + " - " + _dadoscliente.NOMCLI);
                Message = Message.Replace("###Aprovacao", _aprovacao);

                string Assunto = "Desconto aprovado!";

                Arquitetura.Classes.Util.EnviaEmailMartins(_cosultor.EMAILAPROVADOR, null, Assunto, Message.ToString(), null, Arquitetura.Classes.Util.TipoEmail.HTML);

                //_list_servicoscliente = DALOPTCONT.obterServicos(objAlterar.CODOPTVND);
                //if (_list_servicoscliente.Count > 0)
                //{
                //    foreach (OportunidadeContatoTO.obterServico itemServico in _list_servicoscliente)
                //    {
                //        if (itemServico.INDSTANGC == 5)
                //        {
                //            _itemservicoperdido.CODSVCTGV = itemServico.CODSVCTGV;
                //            _itemservicoperdido.CODOPTVND = itemServico.CODOPTVND;
                //            _itemservicoperdido.INDSTANGC = 1;

                //            dalservicooportunit.alterarStatusServicoOportunidade(_itemservicoperdido);
                //            _itemservicoperdido = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
                //        }
                //    }
                //}

                objAlterar.INDSTAOPTVND = 6;
                objAlterar.CODFNCAPV = _codigoAprovador;
                objAlterar.DATHRAAPV = _datainsercao.ToString();
                //objAlterar.DESOBSDSC = objAlterar.DESOBSDSC;

                var DAL = new AprovacaoDAL();
                var _inseriu = DAL.alterarCadastroOportunidade(objAlterar);

                Scope.Complete();

                return _inseriu;
            }
        }
        catch(Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER APROVAÇÃO DE DESCONTO.");
            Utilitario.InsereLog(ex, "Aprovar", "AprovacaoApiModel.CadastroOportunidadeApiModel", "AprovacaoBLL", "ERRO AO FAZER APROVAÇÃO DE DESCONTO.");
            throw;
        }
    }


    public bool Reprovar(AprovacaoApiModel.CadastroOportunidadeApiModel objAlterar)
    {

        CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel _itemservicoperdido = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
        var dalservicooportunit = new CadastroServicoOportunidadeDAL();

        try
        {
            int _codigoAprovador = int.Parse(ConfigurationManager.AppSettings["codigoAprovador"]);

            string _templateemail = ConfigurationManager.AppSettings["caminhoContratoTemplateEmailRespostaAprovacao"];
            OportunidadeContatoTO.obterDadosBasicos _dadoscliente;

            string Message = "";
            var dalAprovador = new AprovacaoDAL();

            AprovacaoTO.obterEmailAprovador _aprovador;
            _aprovador = dalAprovador.obterEmailAprovador(_codigoAprovador);

            DateTime _datainsercao = DateTime.Now;
            string _horas = _datainsercao.Hour.ToString() + " : " + _datainsercao.Minute.ToString();

            var DALANOTACAO = new CadastroAnotacaoDAL();
            CadastroAnotacaoApiModel.CadastroAnotacaoApiModel _itemAnotacao = new CadastroAnotacaoApiModel.CadastroAnotacaoApiModel();

            using (TransactionScope Scope = new TransactionScope(TransactionScopeOption.Required))
            {

                _itemAnotacao.CODCLI = objAlterar.CODCLI;
                _itemAnotacao.CODOPTVND = objAlterar.CODOPTVND;
                _itemAnotacao.DESOBS = "Desconto Rejeitado por " + _aprovador.NOMFNC.Trim() + ", dia " + _datainsercao.ToShortDateString() + " às " + _horas + " \n Justificativa: " + objAlterar.DESOBSDSC.Trim();

                _itemAnotacao.TIPOBSCLI = "2";
                _itemAnotacao.CODMTVCTO = 10;
                _itemAnotacao.DATCAD = _datainsercao.ToString();

                DALANOTACAO.inserirCadastroAnotacaoGeral(_itemAnotacao);

                int _codigoCosultor = objAlterar.CDFNC;

                AprovacaoTO.obterEmailAprovador _cosultor;
                _cosultor = dalAprovador.obterEmailAprovador(_codigoCosultor);
                //_aprovador.EMAILAPROVADOR = _aprovador.EMAILAPROVADOR;

                var DALOPTCONT = new OportunidadeContatoDAL();
                _dadoscliente = DALOPTCONT.obterDadoBasicos(objAlterar.CODCLI).FirstOrDefault();

                string _aprovacao = "REJEITADO";

                Message = GetTemplateHtml(_templateemail);
                Message = Message.Replace("###Oportunidade", objAlterar.CODOPTVND.ToString());
                Message = Message.Replace("###cliente", _dadoscliente.CODCLI.ToString() + " - " + _dadoscliente.NOMCLI);
                Message = Message.Replace("###Aprovacao", _aprovacao);

                string Assunto = "DESCONTO REJEITADO";

                Arquitetura.Classes.Util.EnviaEmailMartins(_cosultor.EMAILAPROVADOR, null, Assunto, Message.ToString(), null, Arquitetura.Classes.Util.TipoEmail.HTML);

                //_list_servicoscliente = DALOPTCONT.obterServicos(objAlterar.CODOPTVND);
                //if (_list_servicoscliente.Count > 0)
                //{
                //    foreach (OportunidadeContatoTO.obterServico itemServico in _list_servicoscliente)
                //    {
                //        if (itemServico.INDSTANGC == 5)
                //        {
                //            _itemservicoperdido.CODSVCTGV = itemServico.CODSVCTGV;
                //            _itemservicoperdido.CODOPTVND = itemServico.CODOPTVND;
                //            _itemservicoperdido.INDSTANGC = 1;

                //            dalservicooportunit.alterarStatusServicoOportunidade(_itemservicoperdido);
                //            _itemservicoperdido = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
                //        }
                //    }
                //}

                objAlterar.INDSTAOPTVND = 7;
                objAlterar.CODFNCAPV = _codigoAprovador;
                objAlterar.DATHRARPV = _datainsercao.ToString();
                objAlterar.DESMTVRPV = objAlterar.DESOBSDSC;
                //objAlterar.DESOBSDSC = objAlterar.DESOBSDSC;

                var DAL = new AprovacaoDAL();
                var _isereiu = DAL.alterarCadastroOportunidade(objAlterar);
                Scope.Complete();

                return _isereiu;
            }
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER REPORVAR DESCONTO.");
            Utilitario.InsereLog(ex, "Reprovar", "AprovacaoApiModel.CadastroOportunidadeApiModel", "AprovacaoBLL", "ERRO AO FAZER REPROVAR DESCONTO.");
            throw;
        }
    }

    public int VerificaStatus(AprovacaoApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        var DAL = new AprovacaoDAL();
        var DALOPTCONT = new OportunidadeContatoDAL();

        CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel _itemservico = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
        CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel _itemservicoperdido = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
        var dalservicooportunit = new CadastroServicoOportunidadeDAL();

        string _dadosLink = "";

        try
        {
            CryptUtil _decript = new CryptUtil();
            _dadosLink = _decript.ActionDecrypt(objAlterar.DESCRIPT);

            var codopt = int.Parse(_dadosLink.Split('&')[0].ToString().Split('=')[1]);

            int _statusAtualopt = DAL.obtemStatusOportunidade(codopt);
            if (_statusAtualopt != 5)
                return 1;
            else
                return 2;

        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO CONSULTAR STATUS.");
            Utilitario.InsereLog(ex, "VerificaStatus", "AprovacaoApiModel.CadastroOportunidadeApiModel", "AprovacaoBLL", "ERRO AO CONSULTAR STATUS.");
            throw;
        }

    }


    public static string GetTemplateHtml(string path)
    {
        var logicPath = System.Web.HttpContext.Current.Server.MapPath(path);
        return System.IO.File.ReadAllText(logicPath);
    }

}


