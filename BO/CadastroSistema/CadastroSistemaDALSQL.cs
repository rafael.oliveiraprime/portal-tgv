﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class CadastroSistemaDALSQL
{
    public string obterCadastroSistemaAutomacao()
    {
        return @"
                 SELECT SIS.CODEMPAUT, SIS.CODSISINF, TRIM(SIS.NOMSIS) AS NOMSIS, SIS.CODSGM, SGM.DESSGM,
                 CASE WHEN INDSISHML = 0 THEN 'NÃO' WHEN INDSISHML = 1 THEN 'SIM' END NOMINDSISHML,
                 INDSISHML, TRIM(NOMSISOPE) AS NOMSISOPE, TIPBBLSIS,
                 CASE WHEN TIPBBLSIS = 1 THEN 'CLISITEF' WHEN TIPBBLSIS = 2 THEN 'CLIENT MODULAR' END NOMTIPBBLSIS
                 FROM MRT.CADSISEMPAUTTGV SIS
                 INNER JOIN MRT.CADSGMSISTGV SGM ON SGM.CODSGM = SIS.CODSGM
                 WHERE SIS.CODEMPAUT = :CODEMPAUT
                ";
    }

    public string inserirCadastroSistema()
    {
        return @"
                 INSERT INTO MRT.CADSISEMPAUTTGV (CODEMPAUT, CODSISINF, NOMSIS, CODSGM, INDSISHML, NOMSISOPE, TIPBBLSIS) 
                 VALUES (:CODEMPAUT, 
                 (SELECT COALESCE(MAX(CODSISINF),0)+1 FROM MRT.CADSISEMPAUTTGV WHERE CODEMPAUT = :CODEMPAUT), 
                 UPPER(:NOMSIS), :CODSGM, :INDSISHML, UPPER(:NOMSISOPE), :TIPBBLSIS)
                ";
    }

    public string alterarCadastroSistemaAutomacao()
    {
        return @"
                 UPDATE MRT.CADSISEMPAUTTGV SET
                 NOMSIS = UPPER(:NOMSIS),
                 CODSGM = :CODSGM,
                 INDSISHML = :INDSISHML,
                 NOMSISOPE = :NOMSISOPE,
                 TIPBBLSIS = :TIPBBLSIS 
                 WHERE CODEMPAUT = :CODEMPAUT
                 AND CODSISINF = :CODSISINF
                ";
    }
}

