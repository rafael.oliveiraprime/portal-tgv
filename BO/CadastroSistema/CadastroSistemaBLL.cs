﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CadastroSistemaBLL
{
    public List<CadastroSistemaTO.obterCadastroSistemaAutomacao> obterCadastroSistemaAutomacao(CadastroSistemaApiModel.CadastroSistemaApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroSistemaDAL();
            return DAL.obterCadastroSistemaAutomacao(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO SISTEMA.");
            Utilitario.InsereLog(ex, "obterCadastroSistemaAutomacao", "CadastroSistemaApiModel.CadastroSistemaApiModel", "CadastroSistemaBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO SISTEMA.");
            throw;
        }
    }

    public bool inserirCadastroSistema(CadastroSistemaApiModel.CadastroSistemaApiModel objInserir)
    {
        try
        {
            var DAL = new CadastroSistemaDAL();
            return DAL.inserirCadastroSistema(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO CADASTRO SISTEMA.");
            Utilitario.InsereLog(ex, "inserirCadastroSistema", "CadastroSistemaApiModel.CadastroSistemaApiModel", "CadastroSistemaBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO SISTEMA.");
            throw;
        }
    }

    public bool alterarCadastroSistemaAutomacao(CadastroSistemaApiModel.CadastroSistemaApiModel objInserir)
    {
        try
        {
            var DAL = new CadastroSistemaDAL();
            return DAL.alterarCadastroSistemaAutomacao(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO CADASTRO SISTEMA.");
            Utilitario.InsereLog(ex, "alterarCadastroSistemaAutomacao", "CadastroSistemaApiModel.CadastroSistemaApiModel", "CadastroSistemaBLL", "ERRO AO FAZER A ALTERACAO DO CADASTRO SISTEMA.");
            throw;
        }
    }
}