﻿using System.Collections.Generic;

namespace CadastroSistemaTO
{
    public class obterCadastroSistemaAutomacao
    {
        public int CODEMPAUT { get; set; }

        public int CODSISINF { get; set; }

        public string NOMSIS { get; set; }

        public int CODSGM { get; set; }

        public string DESSGM { get; set; }

        public string NOMINDSISHML { get; set; }

        public int INDSISHML { get; set; }
        
        public string NOMSISOPE { get; set; }

        public int TIPBBLSIS { get; set; }

        public string NOMTIPBBLSIS { get; set; }
    }
}