﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CadastroSistemaDAL : DAL
{
    public List<CadastroSistemaTO.obterCadastroSistemaAutomacao> obterCadastroSistemaAutomacao(CadastroSistemaApiModel.CadastroSistemaApiModel objPesquisar)
    {
        var DALSQL = new CadastroSistemaDALSQL();
        string cmdSql = DALSQL.obterCadastroSistemaAutomacao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("CODEMPAUT", objPesquisar.CODEMPAUT);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroSistemaTO.obterCadastroSistemaAutomacao>();
    }

    public bool inserirCadastroSistema(CadastroSistemaApiModel.CadastroSistemaApiModel objInserir)
    {
        var DALSQL = new CadastroSistemaDALSQL();
        string cmdSql = DALSQL.inserirCadastroSistema();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);             
        //                    
        if (objInserir.CODSGM == -1)
        {
            objInserir.CODSGM = 0;
        }

        if (objInserir.TIPBBLSIS == -1)
        {
            objInserir.TIPBBLSIS = 0;
        }
        //
        dbCommand.AddWithValue("CODEMPAUT", objInserir.CODEMPAUT);        
        dbCommand.AddWithValue("NOMSIS", objInserir.NOMSIS);
        dbCommand.AddWithValue("CODSGM", objInserir.CODSGM);
        dbCommand.AddWithValue("INDSISHML", objInserir.INDSISHML);
        dbCommand.AddWithValue("NOMSISOPE", objInserir.NOMSISOPE);
        dbCommand.AddWithValue("TIPBBLSIS", objInserir.TIPBBLSIS);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool alterarCadastroSistemaAutomacao(CadastroSistemaApiModel.CadastroSistemaApiModel objAlterar)
    {
        var DALSQL = new CadastroSistemaDALSQL();
        string cmdSql = DALSQL.alterarCadastroSistemaAutomacao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //                    
        if (objAlterar.CODSGM == -1)
        {
            objAlterar.CODSGM = 0;
        }

        if (objAlterar.TIPBBLSIS == -1)
        {
            objAlterar.TIPBBLSIS = 0;
        }
        //
        dbCommand.AddWithValue("CODEMPAUT", objAlterar.CODEMPAUT);
        dbCommand.AddWithValue("NOMSIS", objAlterar.NOMSIS);
        dbCommand.AddWithValue("CODSISINF", objAlterar.CODSISINF);
        dbCommand.AddWithValue("CODSGM", objAlterar.CODSGM);
        dbCommand.AddWithValue("INDSISHML", objAlterar.INDSISHML);
        dbCommand.AddWithValue("NOMSISOPE", objAlterar.NOMSISOPE);
        dbCommand.AddWithValue("TIPBBLSIS", objAlterar.TIPBBLSIS);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

}