﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class RelCancelamentosDAL : DAL
{
    public List<RelCancelamentosTO.obterCancelamentos> obterCancelamentos(RelCancelamentosApiModel.obterCancelamentosApiModel objPesquisar)
    {
        var DALSQL = new RelCancelamentosDALSQL();
        string cmdSql = DALSQL.obterCancelamentos();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //    
        dbCommand.AddWithValue("MESINI", objPesquisar.MESINI);
        dbCommand.AddWithValue("MESFIM", objPesquisar.MESFIM);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<RelCancelamentosTO.obterCancelamentos>();
    }
}