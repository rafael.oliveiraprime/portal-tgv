﻿
public class RelCancelamentosDALSQL
{
    public string obterCancelamentos()
    {
        return @"
                 SELECT C.CODCLI AS CODIGO
                ,CLI.NOMCLI AS CLIENTE
                ,SR.DESSVCTGV AS SERVICO
                ,CA.DESMTVCNC AS CATEGORIZACAO
                , TO_CHAR(S.DATCNC,'DD/MM/YYYY') AS CANCELAMENTO
                ,OB.DESOBS AS MOTIVO
                FROM MRT.CADCLITGV C
                INNER JOIN MRT.T0100043 CLI ON CLI.CODCLI = C.CODCLI 
                INNER JOIN MRT.CADCLISVCTGV S ON C.CODCLI = S.CODCLI
                INNER JOIN MRT.CADSVCTGV SR ON (S.CODSVCTGV = SR.CODSVCTGV) 
                LEFT JOIN MRT.CADOBSCLITGV OB ON (S.CODCLI = OB.CODCLI AND OB.CODMTVCTO = 12 AND OB.TIPOBSCLI = 5)
                LEFT JOIN MRT.CADMTVCNCSVCTGV CA ON (S.CODMTVCNC = CA.CODMTVCNC)
                WHERE S.INDSTAATV = 4 
                AND S.DATCNC BETWEEN TO_DATE(:MESINI,'DD/MM/YYYY') AND TO_DATE(:MESFIM,'DD/MM/YYYY')
                ORDER BY S.DATCNC
                ";
    }

}