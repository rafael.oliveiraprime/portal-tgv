﻿
namespace RelCancelamentosTO
{
    public class obterCancelamentos
    {
        public int CODIGO { get; set; }
        public string CLIENTE { get; set; }
        public string SERVICO { get; set; }
        public string CATEGORIZACAO { get; set; }
        public string CANCELAMENTO { get; set; }
        public string MOTIVO { get; set; }
    }
}