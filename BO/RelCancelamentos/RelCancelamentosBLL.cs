﻿using System;
using System.Collections.Generic;

public class RelCancelamentosBLL
{
    public List<RelCancelamentosTO.obterCancelamentos> obterCancelamentos(RelCancelamentosApiModel.obterCancelamentosApiModel objPesquisar)
    {
        try
        {
            var DAL = new RelCancelamentosDAL();
            return DAL.obterCancelamentos(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO RELATORIO DE Cancelamentos.");
            Utilitario.InsereLog(ex, "obterCancelamentos", "RelCancelamentosApiModel.obterCancelamentosApiModel", "RelIndicoesBLL", "ERRO AO FAZER A CONSULTA DO RELATORIO DE Cancelamentos.");
            throw;
        }
    }
}