﻿
namespace CadastroTipoParceiroTO
{
    public class obterCadastroTipoParceiro
    {
        public int CODPAC { get; set; }

        public int CODPACCHECK { get; set; }

        public string NOMPAC { get; set; }        

        public string DATFNCDST { get; set; }

        public string FNCDST { get; set; }

        public string STATUS { get; set; }
    }
}