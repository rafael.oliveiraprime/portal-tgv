﻿using System;
using System.Collections.Generic;

public class CadastroTipoParceiroBLL
{
    public List<CadastroTipoParceiroTO.obterCadastroTipoParceiro> obterCadastroTipoParceiro(CadastroTipoParceiroApiModel.CadastroTipoParceiroApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroTipoParceiroDAL();
            return DAL.obterCadastroTipoParceiro(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO DE.");
            Utilitario.InsereLog(ex, "obterCadastroTipoParceiro", "CadastroTipoParceiroApiModel.CadastroTipoParceiroApiModel", "CadastroTipoParceiroBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO DEPARTAMENTO.");
            throw;
        }
    }

    public bool inserirCadastroTipoParceiro(CadastroTipoParceiroApiModel.CadastroTipoParceiroApiModel objInserir)
    {
        try
        {
            var DAL = new CadastroTipoParceiroDAL();
            return DAL.inserirCadastroTipoParceiro(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO CADASTRO TIPO PARCEIRO.");
            Utilitario.InsereLog(ex, "inserirCadastroTipoParceiro", "CadastroTipoParceiroApiModel.CadastroTipoParceiroApiModel", "CadastroTipoParceiroBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO DEPARTAMENTO.");
            throw;
        }
    }

    public bool alterarCadastroTipoParceiro(CadastroTipoParceiroApiModel.CadastroTipoParceiroApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroTipoParceiroDAL();
            return DAL.alterarCadastroTipoParceiro(objAlterar);
        }

        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO CADASTRO TIPO PARCEIRO.");
            Utilitario.InsereLog(ex, "inserirCadastroTipoParceiro", "CadastroTipoParceiroApiModel.CadastroTipoParceiroApiModel", "CadastroTipoParceiroBLL", "ERRO AO FAZER A ALTERACAO DO CADASTRO DEPARTAMENTO.");
            throw;
        }
    }

    public bool ativarCadastroTipoParceiro(CadastroTipoParceiroApiModel.CadastroTipoParceiroApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroTipoParceiroDAL();
            return DAL.ativarCadastroTipoParceiro(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATIVACAO DO CADASTRO TIPO PARCEIRO.");
            Utilitario.InsereLog(ex, "ativarCadastroTipoParceiro", "CadastroTipoParceiroApiModel.CadastroTipoParceiroApiModel", "CadastroTipoParceiroBLL", "ERRO AO FAZER A ATIVACAO DO CADASTRO TIPO PARCEIRO.");
            throw;
        }
    }

    public bool desativarCadastroTipoParceiro(CadastroTipoParceiroApiModel.CadastroTipoParceiroApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroTipoParceiroDAL();
            return DAL.desativarCadastroTipoParceiro(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A DESATIVACAO DO CADASTRO MOTIVO PARCEIRO.");
            Utilitario.InsereLog(ex, "desativarCadastroTipoParceiro", "CadastroTipoParceiroApiModel.CadastroTipoParceiroApiModel", "CadastroTipoParceiroBLL", "ERRO AO FAZER A DESATIVACAO DO CADASTRO TIPO PARCEIRO.");
            throw;
        }
    }
}
