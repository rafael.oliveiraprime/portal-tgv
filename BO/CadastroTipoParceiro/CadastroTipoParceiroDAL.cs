﻿using System;
using System.Collections.Generic;

/// <summary>
/// Summary description for CadastroTipoParceiroDAL
/// </summary>
public class CadastroTipoParceiroDAL : DAL
{
    public List<CadastroTipoParceiroTO.obterCadastroTipoParceiro> obterCadastroTipoParceiro(CadastroTipoParceiroApiModel.CadastroTipoParceiroApiModel objPesquisar)
    {
        var DALSQL = new CadastroTipoParceiroDALSQL();
        string cmdSql = DALSQL.obterCadastroTipoParceiro(objPesquisar.TIPCTOATI, objPesquisar.CODPAC);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("CODPAC", objPesquisar.CODPAC);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroTipoParceiroTO.obterCadastroTipoParceiro>();
    }

    public bool inserirCadastroTipoParceiro(CadastroTipoParceiroApiModel.CadastroTipoParceiroApiModel objInserir)
    {
        var DALSQL = new CadastroTipoParceiroDALSQL();
        string cmdSql = DALSQL.inserirCadastroTipoParceiro();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("NOMPAC", objInserir.NOMPAC);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool alterarCadastroTipoParceiro(CadastroTipoParceiroApiModel.CadastroTipoParceiroApiModel objAlterar)
    {
        var DALSQL = new CadastroTipoParceiroDALSQL();
        string cmdSql = DALSQL.alterarCadastroTipoParceiro();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        dbCommand.AddWithValue("CODPAC", objAlterar.CODPAC);
        dbCommand.AddWithValue("NOMPAC", objAlterar.NOMPAC);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool ativarCadastroTipoParceiro(CadastroTipoParceiroApiModel.CadastroTipoParceiroApiModel objAlterar)
    {
        var DALSQL = new CadastroTipoParceiroDALSQL();
        string cmdSql = DALSQL.ativarCadastroTipoParceiro(objAlterar.LISTCODPAC);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool desativarCadastroTipoParceiro(CadastroTipoParceiroApiModel.CadastroTipoParceiroApiModel objAlterar)
    {
        var DALSQL = new CadastroTipoParceiroDALSQL();
        string cmdSql = DALSQL.desativarCadastroTipoParceiro(objAlterar.LISTCODPAC);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCDST = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //              
        dbCommand.AddWithValue("CODFNCDST", objAlterar.CODFNCDST);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }
}