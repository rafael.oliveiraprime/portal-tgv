﻿using System.Collections.Generic;
using System.Text;

public class CadastroTipoParceiroDALSQL
{
    public string obterCadastroTipoParceiro(int flgAtivo, int codPac)
    {
        StringBuilder strBld = new StringBuilder(@"                 
                 SELECT TIP.CODPAC, TIP.CODPAC CODPACCHECK, TIP.NOMPAC,                   
                 CASE TIP.CODFNCDST WHEN TIP.CODFNCDST THEN 'DESATIVADO' ELSE 'ATIVADO' END STATUS,                              
                 CASE TIP.DATDST WHEN TIP.DATDST THEN TRIM(FNC_DST.NOMFNC) ELSE NULL END FNCDST,
                 CASE TIP.DATDST WHEN TIP.DATDST THEN TO_CHAR(TIP.DATDST,'DD/MM/YYYY') ELSE NULL END DATFNCDST
                 FROM MRT.CADTIPPACTGV TIP
                 LEFT JOIN MRT.T0100361 FNC_DST ON FNC_DST.CODFNC = TIP.CODFNCDST 
                 WHERE TIP.CODPAC = :CODPAC
                ");

        if (flgAtivo == 1)
        {
            if (codPac == -1)
            {
                strBld.AppendLine(" WHERE DATDST IS NULL ");
            }
            else
            {
                strBld.AppendLine(" AND DATDST IS NULL ");
            }
        }
        if (flgAtivo == 2)
        {
            if (codPac == -1)
            {
                strBld.AppendLine(" WHERE DATDST IS NOT NULL ");
            }
            else
            {
                strBld.AppendLine(" AND DATDST IS NOT NULL ");
            }
        }

        strBld.AppendLine("ORDER BY STATUS, CODPAC");

        return strBld.ToString();
    }

    public string inserirCadastroTipoParceiro()
    {
        return @"
                 INSERT INTO MRT.CADTIPPACTGV (CODPAC, NOMPAC)
                 VALUES ((SELECT COALESCE(MAX(CODPAC),0)+1 FROM MRT.CADTIPPACTGV),
                 UPPER(:NOMPAC))
                ";
    }

    public string alterarCadastroTipoParceiro()
    {
        return @"
                 UPDATE MRT.CADTIPPACTGV SET
                 NOMPAC = UPPER(:NOMPAC)                                
                 WHERE CODPAC = :CODPAC
                ";

    }

    public string ativarCadastroTipoParceiro(List<long> LISTCODPAC)
    {
        return @"
                 UPDATE MRT.CADTIPPACTGV SET                 
                 CODFNCDST = NULL,
                 DATDST = NULL
                 WHERE CODPAC IN(#LISTCODPAC)
                ".Replace("#LISTCODPAC", Utilitario.ToListStr(LISTCODPAC));
    }

    public string desativarCadastroTipoParceiro(List<long> LISTCODPAC)
    {
        return @"
                 UPDATE MRT.CADTIPPACTGV SET
                 CODFNCDST = :CODFNCDST,
                 DATDST = SYSDATE
                 WHERE CODPAC IN(#LISTCODPAC)
                ".Replace("#LISTCODPAC", Utilitario.ToListStr(LISTCODPAC));
    }
}