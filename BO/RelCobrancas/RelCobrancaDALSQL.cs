﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class RelCobrancaDALSQL
{
    public string ObterCobranca(int _tipobusca)
    {

        StringBuilder strBld;


        if ((_tipobusca == 1) || (_tipobusca == 0) || (_tipobusca == -1))
        {
            strBld = new StringBuilder(@"   WITH EMAILS AS (    SELECT TIT.ANOMESREF, TIT.CODCLI CODIGO_MARTINS,                   
                                               TIT.NUMTITCOBFAT TITULO,
                                               TIT.DATVNCTIT VENCIMENTO,
                                               TIT.VLRSLDTIT VALOR,
                                               CASE
                                               WHEN TIT.CODSTATIT = 1 THEN  'ABERTO'
                                               WHEN TIT.CODSTATIT = 3 THEN  'ATRASO'
                                               END STATUS,
                                               CASE
                                               WHEN TIT.INDDBTAUTPGT = 1 THEN 'DEBITO AUTOMATICO'
                                               WHEN TIT.INDDBTAUTPGT = 0 THEN 'BOLETO'
                                               END FORMA_PAGAMENTO,
                                               CASE
                                               WHEN TIT.INDFATSVC = 1 THEN 'MENSAL'
                                               WHEN TIT.INDFATSVC <> 1 THEN 'AVULSO'
                                               END INDICADOR_FATURAMENTO,
                                               CTO.NOMCTO NOME_CLIENTE,
                                               CTO.DESENDETNCTO EMAIL,
                                               FAT.NUMNOTFSCSVC,
                                               FAT.DATIPRNOTFSCSVC,
                                               NVL(TIT.DESCHVFAT, ' ') AS DESCHVFAT,
                                               TIT.INDFATSVC
                                        FROM MRT.MOVTITTGV TIT
                                        INNER JOIN MRT.CADCTOTGV CTO ON TIT.CODCLI = CTO.CODEDE
                                        INNER JOIN MRT.MOVMESFATCLITGV FAT ON (TIT.ANOMESREF = FAT.ANOMESREF AND TIT.CODCLI = FAT.CODCLI AND TIT.INDFATSVC = FAT.INDFATSVC  AND TIT.NUMLOTPRENOTFSCSVC = FAT.NUMLOTPRENOTFSCSVC)
                                        WHERE TIT.codstatit in (1,3)
                                        AND CTO.codtipcto = 7
                                        AND CTO.DATDST IS NULL
                                        AND TIT.ANOMESREF = :ANOMES            
                                        AND TIT.CODCLI = :CODCLI
                                        AND TIT.INDFATSVC = :INDICADOR            
                                        AND TIT.CODSTATIT = :STATUS
                                        GROUP BY TIT.ANOMESREF,
                                                 TIT.CODCLI,
                                                 TIT.NUMTITCOBFAT,
                                                 TIT.DATVNCTIT,
                                                 TIT.VLRSLDTIT,
                                                 TIT.CODSTATIT,
                                                 TIT.INDDBTAUTPGT,
                                                 TIT.INDFATSVC,
                                                 CTO.NOMCTO,
                                                 CTO.DESENDETNCTO,
                                                 FAT.NUMNOTFSCSVC,
                                                 FAT.DATIPRNOTFSCSVC,
                                                 TIT.DESCHVFAT),
                            CONTATOS AS(
                                          select CODEDE,
                                          CON.NOMCLI AS RAZAO_SOCIAL,
                                          (CASE CON.NUMCGCCLI WHEN CON.NUMCGCCLI THEN (SUBSTR(CON.NUMCGCCLI, 1, 2) || '.' || SUBSTR(CON.NUMCGCCLI, 3, 3) || '.' || SUBSTR(CON.NUMCGCCLI, 6, 3) || '/' || SUBSTR(CON.NUMCGCCLI, 9,4) || '-' || SUBSTR(CON.NUMCGCCLI, 13,2) ) ELSE '' END) AS CNPJ,
                                          listagg(SUBSTR(case when trim(NUMTLFCEL) is null then '' else ' @' || trim(NUMTLFCEL) end ||
                                          case when trim(NUMTLFCTO) is null then '' else ' @' || trim(NUMTLFCTO) end ||
                                          case when trim(NUMTLFCTOSCD) is null then '' else ' @' || trim(NUMTLFCTOSCD) end ||
                                          case when trim(NUMTLFCTOTCR) is null then '' else ' @' || trim(NUMTLFCTOTCR) end ||
                                          case when trim(NUMTLFCTOQRT) is null then '' else ' @' || trim(NUMTLFCTOQRT) end ||
                                          case when trim(NUMTLFCTOQNT) is null then '' else ' @' || trim(NUMTLFCTOQNT) end ||
                                          case when trim(CON.NUMTLFCLI) is null then '' else ' @' || trim(CON.NUMTLFCLI) end ||
                                          case when trim(CON.NUMTLFCELCLI) is null then '' else ' @' || trim(CON.NUMTLFCELCLI) end, 2), ' @')
                                          within group (order by 1) as TELEFONES
                                          from MRT.CADCTOTGV TEL
                                          INNER JOIN MRT.T0100043 CON ON TEL.CODEDE = CON.CODCLI
                                          WHERE 1 = 1 
                                            AND CODEDE = :CODCLI
                                          group by CODEDE, CON.NOMCLI, CON.NUMCGCCLI
                                          order by CODEDE
                                        )
                            SELECT E.ANOMESREF,
                                   E.CODIGO_MARTINS,
                                   C.RAZAO_SOCIAL,
                                   C.CNPJ,
                                   E.TITULO,
                                   E.VENCIMENTO,
                                   E.VALOR,
                                   E.STATUS,
                                   E.FORMA_PAGAMENTO,
                                   E.INDICADOR_FATURAMENTO,
                                   E.NOME_CLIENTE,
                                   E.EMAIL,
                                   REPLACE(C.TELEFONES,'-', '') AS TELEFONES,
                                   E.NUMNOTFSCSVC,
                                   E.DATIPRNOTFSCSVC,
                                   E.DESCHVFAT,
                                   E.INDFATSVC
                            FROM EMAILS E
                            INNER JOIN CONTATOS C ON E.CODIGO_MARTINS = C.CODEDE
                            GROUP BY E.ANOMESREF,
                                     E.CODIGO_MARTINS,
                                     C.RAZAO_SOCIAL, 
                                     C.CNPJ,
                                     E.TITULO,
                                     E.VENCIMENTO,
                                     E.VALOR,
                                     E.STATUS,
                                     E.FORMA_PAGAMENTO,
                                     E.INDICADOR_FATURAMENTO,
                                     E.NOME_CLIENTE,
                                     E.EMAIL,
                                     C.TELEFONES,
                                     E.NUMNOTFSCSVC,
                                     E.DATIPRNOTFSCSVC,
                                     E.DESCHVFAT,
                                     E.INDFATSVC
                                     order by E.CODIGO_MARTINS");
        }
        else
        {
            strBld = new StringBuilder(@"   WITH EMAILS AS (    SELECT TIT.ANOMESREF, TIT.CODCLI CODIGO_MARTINS,                   
                                               TIT.NUMTITCOBFAT TITULO,
                                               TIT.DATVNCTIT VENCIMENTO,
                                               TIT.VLRSLDTIT VALOR,
                                               CASE
                                               WHEN TIT.CODSTATIT = 1 THEN  'ABERTO'
                                               WHEN TIT.CODSTATIT = 3 THEN  'ATRASO'
                                               END STATUS,
                                               CASE
                                               WHEN TIT.INDDBTAUTPGT = 1 THEN 'DEBITO AUTOMATICO'
                                               WHEN TIT.INDDBTAUTPGT = 0 THEN 'BOLETO'
                                               END FORMA_PAGAMENTO,
                                               CASE
                                               WHEN TIT.INDFATSVC = 1 THEN 'MENSAL'
                                               WHEN TIT.INDFATSVC <> 1 THEN 'AVULSO'
                                               END INDICADOR_FATURAMENTO,
                                               CTO.NOMCTO NOME_CLIENTE,
                                               CTO.DESENDETNCTO EMAIL,
                                               FAT.NUMNOTFSCSVC,
                                               FAT.DATIPRNOTFSCSVC,
                                               NVL(TIT.DESCHVFAT, ' ') AS DESCHVFAT,
                                               TIT.INDFATSVC
                                        FROM MRT.MOVTITTGV TIT
                                        INNER JOIN MRT.CADCTOTGV CTO ON TIT.CODCLI = CTO.CODEDE
                                        WHERE TIT.codstatit in (1,3)
                                        AND CTO.codtipcto = 7
                                        AND CTO.DATDST IS NULL
                                        AND TIT.ANOMESREF = :ANOMES            
                                        AND TIT.CODCLI = :CODCLI
                                        AND TIT.INDFATSVC <> 1            
                                        AND TIT.CODSTATIT = :STATUS
                                        GROUP BY TIT.ANOMESREF,
                                                 TIT.CODCLI,
                                                 TIT.NUMTITCOBFAT,
                                                 TIT.DATVNCTIT,
                                                 TIT.VLRSLDTIT,
                                                 TIT.CODSTATIT,
                                                 TIT.INDDBTAUTPGT,
                                                 TIT.INDFATSVC,
                                                 CTO.NOMCTO,
                                                 CTO.DESENDETNCTO,
                                                 FAT.NUMNOTFSCSVC,
                                                 FAT.DATIPRNOTFSCSVC,
                                                 TIT.DESCHVFAT),
                            CONTATOS AS(
                                          select CODEDE,
                                          CON.NOMCLI AS RAZAO_SOCIAL,
                                          listagg(SUBSTR(case when trim(NUMTLFCEL) is null then '' else ' @' || trim(NUMTLFCEL) end ||
                                          case when trim(NUMTLFCTO) is null then '' else ' @' || trim(NUMTLFCTO) end ||
                                          case when trim(NUMTLFCTOSCD) is null then '' else ' @' || trim(NUMTLFCTOSCD) end ||
                                          case when trim(NUMTLFCTOTCR) is null then '' else ' @' || trim(NUMTLFCTOTCR) end ||
                                          case when trim(NUMTLFCTOQRT) is null then '' else ' @' || trim(NUMTLFCTOQRT) end ||
                                          case when trim(NUMTLFCTOQNT) is null then '' else ' @' || trim(NUMTLFCTOQNT) end ||
                                          case when trim(CON.NUMTLFCLI) is null then '' else ' @' || trim(CON.NUMTLFCLI) end ||
                                          case when trim(CON.NUMTLFCELCLI) is null then '' else ' @' || trim(CON.NUMTLFCELCLI) end, 2), ' @')
                                          within group (order by 1) as TELEFONES
                                          from MRT.CADCTOTGV TEL
                                          INNER JOIN MRT.T0100043 CON ON TEL.CODEDE = CON.CODCLI
                                          WHERE 1 = 1 
                                            AND CODEDE = :CODCLI
                                          group by CODEDE, CON.NOMCLI
                                          order by CODEDE
                                        )
                            SELECT E.ANOMESREF,
                                   E.CODIGO_MARTINS,
                                   C.RAZAO_SOCIAL,
                                   E.TITULO,
                                   E.VENCIMENTO,
                                   E.VALOR,
                                   E.STATUS,
                                   E.FORMA_PAGAMENTO,
                                   E.INDICADOR_FATURAMENTO,
                                   E.NOME_CLIENTE,
                                   E.EMAIL,
                                   REPLACE(C.TELEFONES,'-', '') AS TELEFONES,
                                   E.NUMNOTFSCSVC,
                                   E.DATIPRNOTFSCSVC,
                                   E.DESCHVFAT,
                                   E.INDFATSVC
                            FROM EMAILS E
                            INNER JOIN CONTATOS C ON E.CODIGO_MARTINS = C.CODEDE
                            GROUP BY E.ANOMESREF,
                                     E.CODIGO_MARTINS,
                                     C.RAZAO_SOCIAL, 
                                     E.TITULO,
                                     E.VENCIMENTO,
                                     E.VALOR,
                                     E.STATUS,
                                     E.FORMA_PAGAMENTO,
                                     E.INDICADOR_FATURAMENTO,
                                     E.NOME_CLIENTE,
                                     E.EMAIL,
                                     C.TELEFONES,
                                     E.NUMNOTFSCSVC,
                                     E.DATIPRNOTFSCSVC,
                                     E.DESCHVFAT,
                                     E.INDFATSVC
                                     order by E.CODIGO_MARTINS");


        }

        return strBld.ToString();



    }


    public string ObterUnica()
    {
        return @"    SELECT '61701363000149' AS GF_CNPJ
                            ,'973473' AS GF_CODIGO_MARTINS
                            ,SUBSTR(FAT.ANOMESREF,5,2) || '/' || SUBSTR(FAT.ANOMESREF,0,4) AS MES_ANO
                            ,CAD.NUMCGCCLI AS CNPJ
                            ,SVC.DESSVCTGV AS SOLUCAO
                            ,TRIM(TO_CHAR(FAT.VLRTOTSVC,'L99G999G999D99','NLS_NUMERIC_CHARACTERS = '',.'' NLS_CURRENCY = ''R$ '' ')) AS TOTAL
                            ,CLI.DATATVSVC AS DATA_ATIVACAO
                            ,CLI.DATCNC AS DATA_CANCELAMENTO
                       FROM MRT.CADCLISVCTGV cli
                      INNER JOIN MRT.T0100043 cad ON cli.codcli = cad.codcli
                      INNER JOIN MRT.RLCCLIGRPEMPTGV gru ON cli.codcli = gru.codcli 
                      INNER JOIN MRT.MOVMESFATCLISVCTGV fat ON cli.codcli = fat.codcli and cli.CODSVCTGV = fat.CODSVCTGV and fat.INDSTAATV in( 2, 4)  and fat.CODCLIGRPFAT is null 
                      INNER JOIN MRT.CADSVCTGV SVC ON cli.CODSVCTGV = svc.CODSVCTGV
                      WHERE FAT.DATATVSVC IS NOT NULL
                        AND gru.CODGRPEMPCLI = 20
                        AND FAT.ANOMESREF = :MESANO
                ";
    }


    public string AtualizaEndBoleto()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@"  UPDATE MRT.MOVTITTGV SET DESCHVFAT = :DESCHVFAT
                                                      WHERE ANOMESREF = :ANOMESREF
                                                        AND INDFATSVC = :INDFATSVC
                                                        AND CODCLI = :CODCLI 
                                                        AND NUMTITCOBFAT = :NUMTITCOBFAT 
                                                        AND CODFILEMP = :CODFILEMP 
                                                 ");

        return strBld.ToString();

    }


}