﻿using System;
using System.Collections.Generic;
using Vendas.TGV.Portal.wsNFSe;


public class RelcobrancaBLL
{
    public List<RelCobrancaTO.obterCobranca> ObterCobranca(RelcobrancaApiModel.obterCobrancaApiModel objPesquisar)
    {

        try
        {
            var DAL = new RelCobrancaDAL();

            List<RelCobrancaTO.obterCobranca> clientesemResp = new List<RelCobrancaTO.obterCobranca>();
            List<RelCobrancaTO.obterCobranca> clientesBoleto = new List<RelCobrancaTO.obterCobranca>();
            RelcobrancaApiModel.AtualizarLinkBoletoTitApiModel _atauliza = new RelcobrancaApiModel.AtualizarLinkBoletoTitApiModel();

            string _idparaboleto = "";


            clientesemResp = DAL.ObterCobranca(objPesquisar);

            foreach (RelCobrancaTO.obterCobranca _item in clientesemResp)
            {

                if (_item.DESCHVFAT.Trim() == "")
                {
                    if (_item.NUMNOTFSCSVC == "")
                        _item.NUMNOTFSCSVC = "";

                    if (_item.DATIPRNOTFSCSVC == "")
                        _item.DATIPRNOTFSCSVC = "";



                    if ((_item.NUMNOTFSCSVC != "") && (_item.DATIPRNOTFSCSVC != ""))
                    {
                        wsNFSe ws = new wsNFSe();
                        if (ws != null)
                        {
                            _idparaboleto = ws.getKeyNFSEemail(1, 143, int.Parse(_item.NUMNOTFSCSVC), DateTime.Parse(_item.DATIPRNOTFSCSVC), 1);
                        }

                    }

                    _item.DESCHVFAT = "http://www.martins.com.br/FaturamentoServico.Web/ImprimeRPSBoleto.aspx?ID=" + _idparaboleto;

                    _atauliza.ANOMESREF = _item.ANOMESREF;
                    _atauliza.CODCLI = _item.CODIGO_MARTINS;
                    _atauliza.CODFILEMP = 143;
                    _atauliza.NUMTITCOBFAT = _item.TITULO;
                    _atauliza.INDFATSVC = _item.INDFATSVC;
                    _atauliza.DESCHVFAT = _item.DESCHVFAT;

                    DAL.AtualizaEndBoleto(_atauliza);
                    _atauliza = new RelcobrancaApiModel.AtualizarLinkBoletoTitApiModel();

                }

                clientesBoleto.Add(_item);
            }


            return clientesBoleto; 
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO RELATORIO DE PONTUACAO.");
            Utilitario.InsereLog(ex, "obterIndicacoes", "RelPontuacaoApiModel.obterPontuacaoApiModel", "RelPontuacaoBLL", "ERRO AO FAZER A CONSULTA DO RELATORIO DE PONTUACAO.");
            throw;
        }
    }



}