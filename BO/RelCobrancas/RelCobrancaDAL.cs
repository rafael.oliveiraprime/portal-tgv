﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class RelCobrancaDAL : DAL
{
    public List<RelCobrancaTO.obterCobranca> ObterCobranca(RelcobrancaApiModel.obterCobrancaApiModel objPesquisar)
    {
        var DALSQL = new RelCobrancaDALSQL();
        string cmdSql = DALSQL.ObterCobranca(objPesquisar.INDICADOR);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //    
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("ANOMES", Regex.Replace(objPesquisar.ANOMES, "[/.-]", String.Empty));
        dbCommand.AddWithValue("STATUS", objPesquisar.STATUS);

        if ((objPesquisar.INDICADOR == 1) || (objPesquisar.INDICADOR == 0) || (objPesquisar.INDICADOR == -1))
            dbCommand.AddWithValue("INDICADOR", objPesquisar.INDICADOR);

        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<RelCobrancaTO.obterCobranca>();
    }

    public bool AtualizaEndBoleto(RelcobrancaApiModel.AtualizarLinkBoletoTitApiModel ObjPesquisar)
    {
        var DALSQL = new RelCobrancaDALSQL();
        string cmdSql = DALSQL.AtualizaEndBoleto();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", ObjPesquisar.ANOMESREF);
        dbCommand.AddWithValue("CODCLI", ObjPesquisar.CODCLI);
        dbCommand.AddWithValue("INDFATSVC", ObjPesquisar.INDFATSVC);
        dbCommand.AddWithValue("NUMTITCOBFAT", ObjPesquisar.NUMTITCOBFAT);
        dbCommand.AddWithValue("CODFILEMP", ObjPesquisar.CODFILEMP);
        dbCommand.AddWithValue("DESCHVFAT", ObjPesquisar.DESCHVFAT);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }




}