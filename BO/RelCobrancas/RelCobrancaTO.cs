﻿
namespace RelCobrancaTO
{
    public class obterCobranca
    {
        public int ANOMESREF { get; set; }
        public int CODIGO_MARTINS { get; set; }
        public string RAZAO_SOCIAL { get; set; }
        public int TITULO { get; set; }
        public string VENCIMENTO { get; set; }
        public decimal VALOR { get; set; }
        public string STATUS { get; set; }
        public string FORMA_PAGAMENTO { get; set; }
        public string INDICADOR_FATURAMENTO { get; set; }
        public string NOME_CLIENTE { get; set; }
        public string EMAIL { get; set; }
        public string TELEFONES { get; set; }
        public string NUMNOTFSCSVC { get; set; }
        public string DATIPRNOTFSCSVC { get; set; }
        public string DESCHVFAT { get; set; }
        public int INDFATSVC { get; set; }
        public string CNPJ { get; set; }

    }


}