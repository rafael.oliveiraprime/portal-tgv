﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class CadastroCondicaoComercialServicoOportunidadeDALSQL
{
    public string obterCadastroCondicaoComercialServicoOportunidade()
    {
        return @" SELECT OPTCND.CODOPTVND, 
                         OPTCND.CODSVCTGV, 
                         OPTCND.CODCNDCMC, 
                         CASE WHEN CNDCMC.DESCNDCMC IS NULL THEN OPTCND.DESDSC ELSE CNDCMC.DESCNDCMC END AS DESCNDCMC, 
                         OPTCND.VLRDSC,
                         OPTCND.PERDSC,
                         OPTCND.INDSTA,
                         NVL(CSP.DESPRDCES, ' ') AS CESTPROD,
                         OPTCND.INDTIPVGR,
                         OPTCND.DATINIVLD,
                         OPTCND.DATFIMVLD,
                         OPTCND.NUMMESINIVLD,
                         OPTCND.NUMMESFIMVLD
                    FROM MRT.RLCOPTCNDCMCTGV OPTCND		
                    LEFT JOIN MRT.CADCNDCMCTGV CNDCMC ON CNDCMC.CODCNDCMC = OPTCND.CODCNDCMC
                    LEFT JOIN MRT.CADCESPRDTGV CSP ON OPTCND.CODPRDCES = CSP.CODPRDCES
                   WHERE OPTCND.CODOPTVND = :CODOPTVND
                     AND OPTCND.CODSVCTGV = :CODSVCTGV
                   ORDER BY OPTCND.CODOPTVND, OPTCND.CODSVCTGV, OPTCND.CODCNDCMC
                ";
    }

    public string obterCadastroCondicaoComercialServicoOportunidadeSelect(List<long> LISTCODCNDCMC)
    {
        return @"
                SELECT CODCNDCMC,
                TRIM(DESCNDCMC) AS DESCNDCMC,
                VLRDSC,
                PERDSC
                FROM MRT.CADCNDCMCTGV
                WHERE CODCNDCMC IN (#LSTCODCNDCMC)
                ".Replace("#LSTCODCNDCMC", ToList(LISTCODCNDCMC));
    }

    public string ObterDadosServicoCliente(List<long> LISTCODSVCTGV)
    {
        return @"
                SELECT 
                SR.DESSVCTGV
            FROM MRT.CADCLISVCTGV GCS
            INNER JOIN MRT.CADSVCTGV SR ON (GCS.CODSVCTGV = SR.CODSVCTGV)
            WHERE 1 = 1
            AND GCS.CODCLI = :CODCLI
            AND GCS.CODSVCTGV IN (#LISTCODSVCTGV)"
            .Replace("#LISTCODSVCTGV", ToList(LISTCODSVCTGV));
    }

    public string inserirCadastroCondicaoComercialServicoOportunidade(List<long> LISTCODCNDCMC)
    {
        StringBuilder strBld = new StringBuilder(@"
             INSERT INTO MRT.RLCOPTCNDCMCTGV
             SELECT :CODOPTVND, :CODSVCTGV, CODCNDCMC, 1, VLRDSC, PERDSC
             FROM MRT.CADCNDCMCTGV 
        ");

        strBld.AppendFormat("WHERE CODCNDCMC IN ({0})", ToList(LISTCODCNDCMC)).AppendLine();
        strBld.AppendLine("AND CODCNDCMC NOT IN (SELECT CODCNDCMC FROM MRT.RLCOPTCNDCMCTGV WHERE CODOPTVND = :CODOPTVND AND CODSVCTGV = :CODSVCTGV)");


        return strBld.ToString();
    }

    public string deletarCadastroCondicaoComercialServicoOportunidade(List<long> LISTCODCNDCMC)
    {
        return @"                                
                 DELETE FROM MRT.RLCOPTCNDCMCTGV 
                 WHERE CODSVCTGV = :CODSVCTGV
                 AND CODOPTVND = :CODOPTVND
                 AND CODCNDCMC IN (#LSTCODCNDCMC)
                ".Replace("#LSTCODCNDCMC", ToList(LISTCODCNDCMC));
    }

    public string ToList(List<long> LISTCODSVCTGV)
    {
        string retr = string.Empty;
        bool firstTime = true;

        foreach (long Item in LISTCODSVCTGV)
        {
            if (firstTime)
            {
                firstTime = false;

                retr = (Item.ToString());
            }
            else
            {
                retr = (string.Format("{0}, {1}", retr, Item));
            }

        }
        return retr.Trim();
    }


    public string inserirCadastroCondicaoComercialServicoOportunidadeNovo(bool _descmanual)
    {
        StringBuilder strBld;

        if (_descmanual)
        {
            strBld = new StringBuilder(@" INSERT INTO MRT.RLCOPTCNDCMCTGV (CODOPTVND, CODSVCTGV, CODCNDCMC, INDSTA, VLRDSC, PERDSC, DESDSC, INDTIPVGR, DATINIVLD, DATFIMVLD, NUMMESINIVLD, NUMMESFIMVLD, CODPRDCES) 
                                                                   VALUES (:CODOPTVND, 
                                                                           :CODSVCTGV, 
                                                                            (SELECT (NVL(MAX(CODCNDCMC), 8999)) + 1 FROM MRT.RLCOPTCNDCMCTGV DSC INNER JOIN MRT.CADOPTVNDCLITGV OPT ON(DSC.CODOPTVND = OPT.CODOPTVND) WHERE CODCLI = :CODCLI AND CODCNDCMC >= 9000),
                                                                            :INDSTA, 
                                                                            :VLRDSC, 
                                                                            :PERDSC, 
                                                                             UPPER(:DESDSC), 
                                                                            :INDTIPVGR, 
                                                                            :DATINIVLD, 
                                                                            :DATFIMVLD, 
                                                                            :NUMMESINIVLD, 
                                                                            :NUMMESFIMVLD,
                                                                            :CODPRDCES)
                                                 ");
        }
        else
        {
            strBld = new StringBuilder(@" INSERT INTO MRT.RLCOPTCNDCMCTGV (CODOPTVND, CODSVCTGV, CODCNDCMC, INDSTA, VLRDSC, PERDSC, DESDSC, INDTIPVGR, DATINIVLD, DATFIMVLD, NUMMESINIVLD, NUMMESFIMVLD, CODPRDCES)
                                                                   VALUES (:CODOPTVND, :CODSVCTGV, :CODCNDCMC, :INDSTA, :VLRDSC, :PERDSC, :DESDSC, :INDTIPVGR, :DATINIVLD, :DATFIMVLD, :NUMMESINIVLD, :NUMMESFIMVLD, :CODPRDCES)
                                                 ");
        }

        return strBld.ToString();
    }

    public string verificarVigencia()
    {
        return @" SELECT COALESCE(TO_CHAR(DATINIVLD, 'DD/MM/YYYY'),'') DATINIVLD
                    ,  COALESCE(TO_CHAR(DATFIMVLD, 'DD/MM/YYYY'),'') DATFIMVLD
                    , NUMMESINIVLD 
                    , NUMMESFIMVLD
                     FROM MRT.CADCNDCMCTGV CON	 
                     WHERE CODCNDCMC = :CODCNDCMC	                    
                ";
    }

    public string deletarCadastroCondicaoComercialOportunidade()
    {
        return @"                                
                 DELETE FROM MRT.RLCOPTCNDCMCTGV 
                 WHERE CODSVCTGV = :CODSVCTGV
                 AND CODOPTVND = :CODOPTVND";
    }


}