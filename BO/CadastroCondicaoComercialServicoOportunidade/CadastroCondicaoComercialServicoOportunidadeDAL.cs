﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using CadastroCondicaoComercialServicoOportunidadeTO;

public class CadastroCondicaoComercialServicoOportunidadeDAL : DAL
{
    public List<obterCadastroCondicaoComercialServicoOportunidadeNovo> obterCadastroCondicaoComercialServicoOportunidade(CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel objPesquisar)
    {
        var DALSQL = new CadastroCondicaoComercialServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.obterCadastroCondicaoComercialServicoOportunidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //      
        dbCommand.AddWithValue("CODOPTVND", objPesquisar.CODOPTVND);
        dbCommand.AddWithValue("CODSVCTGV", objPesquisar.CODSVCTGV);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<obterCadastroCondicaoComercialServicoOportunidadeNovo>();
    }

    public List<obterCadastroCondicaoComercialServicoOportunidadeSelect> obterCadastroCondicaoComercialServicoOportunidadeSelect(CadastroCondicaoComercialServicoOportunidadeApiModel.obterCadastroCondicaoComercialServicoOportunidadeSelect objPesquisar)
    {
        var DALSQL = new CadastroCondicaoComercialServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.obterCadastroCondicaoComercialServicoOportunidadeSelect(objPesquisar.LISTCODCNDCMC);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<obterCadastroCondicaoComercialServicoOportunidadeSelect>();
    }

    public List<GestaoContasTO.obterServicosCliente> ObterDadosServicoCliente(CadastroCondicaoComercialServicoOportunidadeApiModel.obterDadosClienteServico objPesquisar)
    {
        var DALSQL = new CadastroCondicaoComercialServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.ObterDadosServicoCliente(objPesquisar.LISTCODSVCTGV);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);

        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<GestaoContasTO.obterServicosCliente>();
    }

    public bool inserirCadastroCondicaoComercialServicoOportunidade(CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel objInserir)
    {
        var DALSQL = new CadastroCondicaoComercialServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.inserirCadastroCondicaoComercialServicoOportunidade(objInserir.LISTCODCNDCMC);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODOPTVND", objInserir.CODOPTVND);
        dbCommand.AddWithValue("CODSVCTGV", objInserir.CODSVCTGV);
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool deletarCadastroCondicaoComercialServicoOportunidade(CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel objDeletar)
    {
        var DALSQL = new CadastroCondicaoComercialServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.deletarCadastroCondicaoComercialServicoOportunidade(objDeletar.LISTCODCNDCMC);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        dbCommand.AddWithValue("CODSVCTGV", objDeletar.CODSVCTGV);
        dbCommand.AddWithValue("CODOPTVND", objDeletar.CODOPTVND);
        //                            
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool inserirCadastroCondicaoComercialServicoOportunidadeNovo(CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeNovoApiModel objInserir)
    {
        var DALSQL = new CadastroCondicaoComercialServicoOportunidadeDALSQL();

        if ((objInserir.CODCNDCMC == 0) || (objInserir.CODCNDCMC == -1))
        {
            if (objInserir.DESDSC == null)
            {
                objInserir.DESDSC = "Desc. Manual";
            }

            if (objInserir.DESDSC.Trim() == "")
            {
                objInserir.DESDSC = "Desc. Manual";
            }
        }


        var _descmanual = false;
        if ((objInserir.CODCNDCMC == 0) || (objInserir.CODCNDCMC == -1))
            _descmanual = true;


        string cmdSql = DALSQL.inserirCadastroCondicaoComercialServicoOportunidadeNovo(_descmanual);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODOPTVND", objInserir.CODOPTVND);
        dbCommand.AddWithValue("CODSVCTGV", objInserir.CODSVC);
        dbCommand.AddWithValue("CODCNDCMC", objInserir.CODCNDCMC);

        if (objInserir.VLRDSC == null)
            objInserir.VLRDSC = "0";

        if (objInserir.PERDSC == null)
            objInserir.PERDSC = "0";

        dbCommand.AddWithValue("INDSTA", objInserir.INDSTA);
        dbCommand.AddWithValue("VLRDSC", decimal.Parse(objInserir.VLRDSC));
        dbCommand.AddWithValue("PERDSC", decimal.Parse(objInserir.PERDSC));

        dbCommand.AddWithValue("DESDSC", objInserir.DESDSC);
        dbCommand.AddWithValue("INDTIPVGR", objInserir.INDTIPVGR);

        if (objInserir.DATINIVLD != null)
        {
            objInserir.DATINIVLD =  "01" + "/" + objInserir.DATINIVLD.Split('/')[1] + "/" +   objInserir.DATINIVLD.Split('/')[0];
            dbCommand.AddWithValue("DATINIVLD", Convert.ToDateTime(objInserir.DATINIVLD));
        }
        else
            dbCommand.AddWithValue("DATINIVLD", objInserir.DATINIVLD);

        if (objInserir.DATFIMVLD != null)
        {
            objInserir.DATFIMVLD = DateTime.DaysInMonth(int.Parse(objInserir.DATFIMVLD.Split('/')[0]), int.Parse(objInserir.DATFIMVLD.Split('/')[1])).ToString().PadLeft(2,'0') + "/" + objInserir.DATFIMVLD.Split('/')[1] + "/" + objInserir.DATFIMVLD.Split('/')[0];
            dbCommand.AddWithValue("DATFIMVLD", Convert.ToDateTime(objInserir.DATFIMVLD));
        }
        else
            dbCommand.AddWithValue("DATFIMVLD", objInserir.DATFIMVLD);


        dbCommand.AddWithValue("NUMMESINIVLD", objInserir.NUMMESINIVLD);
        dbCommand.AddWithValue("NUMMESFIMVLD", objInserir.NUMMESFIMVLD);

        if ((objInserir.CODPRDCES != -1) && (objInserir.CODPRDCES != 0))
            dbCommand.AddWithValue("CODPRDCES", objInserir.CODPRDCES);
        else
            dbCommand.AddWithValue("CODPRDCES", null);


        dbCommand.AddWithValue("CODCLI", objInserir.CODCLI);


        //
        dbCommand.TrataDbCommandUniversal(true, false, true, true);
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public List<CadastroCondicaoComercialServicoOportunidadeTO.verificarVigencia> verificarVigencia(CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeNovoApiModel objPesquisar)
    {
        var DALSQL = new CadastroCondicaoComercialServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.verificarVigencia();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //      
        dbCommand.AddWithValue("CODCNDCMC", objPesquisar.CODCNDCMC);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroCondicaoComercialServicoOportunidadeTO.verificarVigencia>();
    }

    public bool deletarCadastroCondicaoComercialOportunidade(CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel objDeletar)
    {
        var DALSQL = new CadastroCondicaoComercialServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.deletarCadastroCondicaoComercialOportunidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        dbCommand.AddWithValue("CODSVCTGV", objDeletar.CODSVCTGV);
        dbCommand.AddWithValue("CODOPTVND", objDeletar.CODOPTVND);
        //                            
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

}