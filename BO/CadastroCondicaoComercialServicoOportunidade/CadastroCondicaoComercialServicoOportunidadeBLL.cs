﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CadastroCondicaoComercialServicoOportunidadeBLL
{
    public List<CadastroCondicaoComercialServicoOportunidadeTO.obterCadastroCondicaoComercialServicoOportunidade> obterCadastroCondicaoComercialServicoOportunidade(CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel objPesquisar)
    {
        var _listlocal = new List<CadastroCondicaoComercialServicoOportunidadeTO.obterCadastroCondicaoComercialServicoOportunidadeNovo>();
        var _listresult = new List<CadastroCondicaoComercialServicoOportunidadeTO.obterCadastroCondicaoComercialServicoOportunidade>();
        var _itemresult = new CadastroCondicaoComercialServicoOportunidadeTO.obterCadastroCondicaoComercialServicoOportunidade();

        try
        {
            var DAL = new CadastroCondicaoComercialServicoOportunidadeDAL();

            
            _listlocal = DAL.obterCadastroCondicaoComercialServicoOportunidade(objPesquisar);
            foreach(CadastroCondicaoComercialServicoOportunidadeTO.obterCadastroCondicaoComercialServicoOportunidadeNovo _item in _listlocal)
            {
                _itemresult.CODOPTVND = _item.CODOPTVND;
                _itemresult.CODCNDCMC = _item.CODCNDCMC;
                _itemresult.CODSVCTGV = _item.CODSVCTGV;
                _itemresult.DESCNDCMC = _item.DESCNDCMC;
                _itemresult.CESTPROD = _item.CESTPROD;

                _itemresult.VLRDSC = null;
                _itemresult.PERDSC = null;

                if (_item.INDTIPVGR == 1 || _item.INDTIPVGR == 0)
                {
                    if (_item.DATINIVLD != null)
                    {
                        _itemresult.DATINI = DateTime.Parse(_item.DATINIVLD).ToShortDateString();
                    }

                    if (_item.DATFIMVLD != null)
                    {
                        _itemresult.DATFIM = DateTime.Parse(_item.DATFIMVLD).ToShortDateString();
                    }

                }
                else
                {
                    if (_item.NUMMESINIVLD != null)
                    {
                        if (_item.NUMMESINIVLD != "0")
                        {

                            _itemresult.DATINI = _item.NUMMESINIVLD.ToString() + "º Mês";
                        }
                    }

                    if (_item.NUMMESFIMVLD != null)
                    {
                        if (_item.NUMMESFIMVLD != "0")
                        {
                            _itemresult.DATFIM = _item.NUMMESFIMVLD.ToString() + "º Mês";
                        }
                    }

                }

                //if (_item.PERDSC != null)
                //{
                    if (_item.PERDSC != 0)
                    {
                        _itemresult.PERDSC = _item.PERDSC;
                    }
                //}

                //if (_item.VLRDSC != null)
                //{
                    if (_item.VLRDSC != 0)
                    {
                        _itemresult.VLRDSC = _item.VLRDSC;
                    }
                //}


                _listresult.Add(_itemresult);

                _itemresult = null;
                _itemresult = new CadastroCondicaoComercialServicoOportunidadeTO.obterCadastroCondicaoComercialServicoOportunidade();
            }            

            return _listresult;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A PESQUISA DA CONDICAO COMERCIAL SERVICO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "obterCadastroCondicaoComercialServicoOportunidade", "CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel", "CadastroCondicaoComercialServicoOportunidadeBLL", "ERRO AO FAZER A PESQUISA DA CONDICAO COMERCIAL SERVICO OPORTUNIDADE.");
            throw;
        }
    }

    public List<GestaoContasTO.obterServicosCliente> ObterDadosServicoCliente(CadastroCondicaoComercialServicoOportunidadeApiModel.obterDadosClienteServico objPesquisar)
    {
        try
        {
            var DAL = new CadastroCondicaoComercialServicoOportunidadeDAL();
            return DAL.ObterDadosServicoCliente(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO OBTER A PESQUISA DA CONDICAO COMERCIAL SERVICO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "obterCadastroCondicaoComercialServicoOportunidadeSelect", "CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel", "CadastroCondicaoComercialServicoOportunidadeBLL", "ERRO AO OBTER A PESQUISA DA CONDICAO COMERCIAL SERVICO OPORTUNIDADE.");
            throw;
        }
    }

    public List<CadastroCondicaoComercialServicoOportunidadeTO.obterCadastroCondicaoComercialServicoOportunidadeSelect> obterCadastroCondicaoComercialServicoOportunidadeSelect(CadastroCondicaoComercialServicoOportunidadeApiModel.obterCadastroCondicaoComercialServicoOportunidadeSelect objPesquisar)
    {
        try
        {
            var DAL = new CadastroCondicaoComercialServicoOportunidadeDAL();
            return DAL.obterCadastroCondicaoComercialServicoOportunidadeSelect(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO OBTER A PESQUISA DA CONDICAO COMERCIAL SERVICO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "obterCadastroCondicaoComercialServicoOportunidadeSelect", "CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel", "CadastroCondicaoComercialServicoOportunidadeBLL", "ERRO AO OBTER A PESQUISA DA CONDICAO COMERCIAL SERVICO OPORTUNIDADE.");
            throw;
        }
    }

    public bool inserirCadastroCondicaoComercialServicoOportunidade(CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel objInserir)
    {
        try
        {
            var DAL = new CadastroCondicaoComercialServicoOportunidadeDAL(); 
            return DAL.inserirCadastroCondicaoComercialServicoOportunidade(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DA CONDICAO COMERCIAL SERVICO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "inserirCadastroCondicaoComercialServicoOportunidade", "CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel", "CadastroCondicaoComercialServicoOportunidadeBLL", "ERRO AO FAZER A INSERCAO DA CONDICAO COMERCIAL SERVICO OPORTUNIDADE.");
            throw;
        }
    }

    public bool deletarCadastroCondicaoComercialServicoOportunidade(CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel objDeletar)
    {
        try
        {
            var DAL = new CadastroCondicaoComercialServicoOportunidadeDAL();

            if (objDeletar.STATTUSDESC == "2")
            {
                var daloportunidade = new CadastroOportunidadeDAL();
                CadastroOportunidadeApiModel.CadastroOportunidadeApiModel _AlterarStatusOpt = new CadastroOportunidadeApiModel.CadastroOportunidadeApiModel();
                _AlterarStatusOpt.CODOPTVND = objDeletar.CODOPTVND;
                _AlterarStatusOpt.INDSTAOPTVND = 1;

                daloportunidade.alterarCadastroOportunidade(_AlterarStatusOpt);
            }

            return DAL.deletarCadastroCondicaoComercialServicoOportunidade(objDeletar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A DELECAO DA CONDICAO COMERCIAL DO SERVICO DA OPOTUNIDADE.");
            Utilitario.InsereLog(ex, "deletarCadastroCondicaoComercialServicoOportunidade", "CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel", "CadastroCondicaoComercialServicoOportunidadeBLL", "ERRO AO FAZER A DELECAO DA CONDICAO COMERCIAL DO SERVICO DA OPOTUNIDADE.");
            throw;
        }
    }

    public bool inserirCadastroCondicaoComercialServicoOportunidadeNovo(CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeNovoApiModel objInserir)
    {
        try
        {
            var DAL = new CadastroCondicaoComercialServicoOportunidadeDAL();

            if (objInserir.INDTIPVGR == 2)
            {
                if ((objInserir.NUMMESINIVLD == 0) & (objInserir.NUMMESFIMVLD == 0))
                    objInserir.INDTIPVGR = 0;
            }
            else
            {
                if ((objInserir.DATINIVLD == null) & (objInserir.DATFIMVLD == null))
                    objInserir.INDTIPVGR = 0;

            }

            if (objInserir.STATTUSDESC == "2")
            {
                var daloportunidade = new CadastroOportunidadeDAL();
                CadastroOportunidadeApiModel.CadastroOportunidadeApiModel _AlterarStatusOpt = new CadastroOportunidadeApiModel.CadastroOportunidadeApiModel();
                _AlterarStatusOpt.CODOPTVND = objInserir.CODOPTVND;
                _AlterarStatusOpt.INDSTAOPTVND = 1;

                daloportunidade.alterarCadastroOportunidade(_AlterarStatusOpt);
            }

            return DAL.inserirCadastroCondicaoComercialServicoOportunidadeNovo(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DA CONDICAO COMERCIAL SERVICO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "inserirCadastroCondicaoComercialServicoOportunidade", "CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel", "CadastroCondicaoComercialServicoOportunidadeBLL", "ERRO AO FAZER A INSERCAO DA CONDICAO COMERCIAL SERVICO OPORTUNIDADE.");
            throw;
        }
    }

    public List<CadastroCondicaoComercialServicoOportunidadeTO.verificarVigencia> verificarVigencia(CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeNovoApiModel objInserir)
    {
        try
        {
            var DAL = new CadastroCondicaoComercialServicoOportunidadeDAL();                      
            return DAL.verificarVigencia(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DA CONDICAO COMERCIAL SERVICO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "inserirCadastroCondicaoComercialServicoOportunidade", "CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel", "CadastroCondicaoComercialServicoOportunidadeBLL", "ERRO AO FAZER A INSERCAO DA CONDICAO COMERCIAL SERVICO OPORTUNIDADE.");
            throw;
        }
    }
}