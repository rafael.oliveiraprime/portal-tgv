﻿using System.Collections.Generic;

namespace CadastroCondicaoComercialServicoOportunidadeTO
{
    public class obterCadastroCondicaoComercialServicoOportunidade
    {
        public int CODCNDCMC { get; set; }

        public string DESCNDCMC { get; set; }

        public decimal? VLRDSC { get; set; }

        public decimal? PERDSC { get; set; }        

        public int CODOPTVND { get; set; }

        public int CODSVCTGV { get; set; }      
        
        public int INDSTA { get; set; }       

        public string CESTPROD { get; set; }

        public string DATINI { get; set; }

        public string DATFIM { get; set; }

    }    

    public class obterCadastroCondicaoComercialServicoOportunidadeSelect
    {

        public int CODCNDCMC { get; set; }

        public string DESCNDCMC { get; set; }

        public decimal VLRDSC { get; set; }

        public decimal PERDSC { get; set; }

    }

    public class obterCadastroCondicaoComercialServicoOportunidadeNovo
    {
        public int CODOPTVND { get; set; }

        public int CODSVCTGV { get; set; }

        public int CODCNDCMC { get; set; }

        public string DESCNDCMC { get; set; }

        public decimal VLRDSC { get; set; }

        public decimal PERDSC { get; set; }

        public int INDSTA { get; set; }

        public string CESTPROD { get; set; }

        public int INDTIPVGR { get; set; }

        public string DATINIVLD { get; set; }

        public string DATFIMVLD { get; set; }

        public string NUMMESINIVLD { get; set; }

        public string NUMMESFIMVLD { get; set; }
    }

    public class verificarVigencia
    {        
        public string DATINIVLD { get; set; }

        public string DATFIMVLD { get; set; }

        public int NUMMESINIVLD { get; set; }

        public int NUMMESFIMVLD { get; set; }
    }
}