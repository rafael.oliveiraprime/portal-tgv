﻿using System.Collections.Generic;
using System;


namespace ImportarEstruturaTO
{
    public class obterProcessamento
    {
        public string DATAEXECUCAO { get; set; }

        public string DESEXECUCAO { get; set; }

    }


    public class obterEstrutura
    {
        public Int64 CODCLI { get; set; }
        public string NOMCLI { get; set; }
        public string NOMFILEMP { get; set; }
        public Int64 CODFNCRPN { get; set; }
        public string NOMFNC { get; set; }
        public int HIER { get; set; }
        public string EMAIL { get; set; }
        public string TELEFONE { get; set; }
        public string SUPERIOR { get; set; }
    }

}

