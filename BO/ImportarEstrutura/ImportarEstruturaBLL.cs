﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;


public class ImportarEstruturaBLL
{
    public List<ImportarEstruturaTO.obterProcessamento> obterProcessamento()
    {
        try
        {
            var DAL = new ImportarEstruturaDAL();
            return DAL.obterProcessamento();
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO ImportaR MOVIMENTO CESTA PRODUTOS.");
            Utilitario.InsereLog(ex, "obterImportaCestaProduto", "obterImportaCestaProduto.obterImportaCestaProduto", "ImportaCestaProdutoBLL", "ERRO AO ImportaR MOVIMENTO CESTA PRODUTOS.");
            throw;
        }
    }

    public List<ImportarEstruturaTO.obterEstrutura> obterEstrutura(ImportaEstruturaApiModel.ImportaEstruturaApiModel objPesquisar)
    {
        try
        {
            var DAL = new ImportarEstruturaDAL();

            if (objPesquisar.NUMCGCCLI != null)
            {
                objPesquisar.NUMCGCCLI = Regex.Replace(objPesquisar.NUMCGCCLI, "[/.-]", String.Empty);
            }


            return DAL.obterEstrutura(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO ImportaR MOVIMENTO CESTA PRODUTOS.");
            Utilitario.InsereLog(ex, "obterImportaCestaProduto", "obterImportaCestaProduto.obterImportaCestaProduto", "ImportaCestaProdutoBLL", "ERRO AO ImportaR MOVIMENTO CESTA PRODUTOS.");
            throw;
        }
    }

}