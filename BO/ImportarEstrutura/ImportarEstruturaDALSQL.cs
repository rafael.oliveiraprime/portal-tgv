﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class ImportarEstruturaDALSQL
{

    public string obterProcessamento()
    {
        return @" SELECT TO_CHAR(DATALT,'DD/MM/YYYY HH:MM') AS DATAEXECUCAO
                         ,(CASE WHEN CODFNCDST = 1  THEN 'PROCESSANDO' ELSE 'PROCESSADO' END) AS DESEXECUCAO   
                   FROM MRT.CADETTCMCTGV   
                  GROUP BY TO_CHAR(DATALT,'DD/MM/YYYY HH:MM'), CODFNCDST";
    }

    public string obterEstrutura()
    {
        return @" SELECT ST.CODCLI  
                         ,initcap(CLI.NOMCLI)  NOMCLI
                         ,initcap(FIL.NOMFILEMP) NOMFILEMP
                         ,ST.CODFNCRPN CODFNCRPN
                         ,initcap(FNC.NOMFNC) NOMFNC
                         ,ST.NUMORD AS HIER
                         ,initcap(EMAIL.DESENDETN) AS EMAIL
                         ,CASE LENGTH(TRIM(ST.LSTTLFCTO)) 
                             WHEN 11 THEN  ( '(' || SUBSTR(ST.LSTTLFCTO, 1, 2) || ')' || SUBSTR(ST.LSTTLFCTO, 3, 5) || '-' ||  SUBSTR(ST.LSTTLFCTO, 8, 4) ) 
                             WHEN 10 THEN  ( '(' || SUBSTR(ST.LSTTLFCTO, 1, 2) || ') ' || SUBSTR(ST.LSTTLFCTO, 3, 4) || '-' || SUBSTR(ST.LSTTLFCTO, 7, 4) ) 
                        ELSE ST.LSTTLFCTO
                         END TELEFONE
                         ,SUP.NOMFNC AS SUPERIOR
                    FROM MRT.CADETTCMCTGV ST   
                   INNER JOIN MRT.T0100043 CLI ON ST.CODCLI = CLI.CODCLI  
                    LEFT JOIN MRT.T0112963 FIL ON ST.CODEMP = FIL.CODEMP AND ST.CODFILEMP = FIL.CODFILEMP
                    LEFT JOIN MRT.T0100361 FNC ON ST.CODFNCRPN = FNC.CODFNC
                    LEFT JOIN MRT.CADDDOPESINDTGV EMAIL ON FNC.CODFNC = EMAIL.CODFNC
                    LEFT JOIN MRT.T0100361 SUP ON SUP.CODFNC = ST.CODFNCSUPRPN
                   WHERE 1 = 1 
                     AND ST.CODCLI = :CODCLI ";
    }

}