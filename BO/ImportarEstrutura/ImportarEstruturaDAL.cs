﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class ImportarEstruturaDAL : DAL
{

    public List<ImportarEstruturaTO.obterProcessamento> obterProcessamento()
    {
        var DALSQL = new ImportarEstruturaDALSQL();
        string cmdSql = DALSQL.obterProcessamento();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<ImportarEstruturaTO.obterProcessamento>();
    }


    public List<ImportarEstruturaTO.obterEstrutura> obterEstrutura(ImportaEstruturaApiModel.ImportaEstruturaApiModel objPesquisar)
    {
        var DALSQL = new ImportarEstruturaDALSQL();
        string cmdSql = DALSQL.obterEstrutura();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("NUMCGCCLI", objPesquisar.NUMCGCCLI);
        dbCommand.AddWithValue("NOMCLI", objPesquisar.NOMCLI);
        dbCommand.AddWithValue("CODFILEMP", objPesquisar.CODFILEMP);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<ImportarEstruturaTO.obterEstrutura>();
    }

}