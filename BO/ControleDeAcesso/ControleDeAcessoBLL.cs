﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Data;

public class ControleDeAcessoBLL
{
    public List<ControleDeAcessoTO.ObterInformacoesUsuario> ObterInformacoesUsuario(string NOMUSRRCF)
    {
        try
        {
            ControleDeAcessoDAL DAL = new ControleDeAcessoDAL();
            return DAL.ObterInformacoesUsuario(NOMUSRRCF);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CONTROLE DE ACESSO.");
            throw;
        }
    }
}