﻿using System.Collections.Generic;

namespace ControleDeAcessoTO
{
    public class ObterInformacoesUsuario
    {
        public ObterInformacoesUsuario()
        {
            GRUPOSAD = new List<string>();
        }
        public int CODFNC { get; set; }
        public string NOMFNC { get; set; }
        public string NOMUSRRCF { get; set; }
        public string DESENDCREETNFNC { get; set; }
        
        [ModuloClasse.AtributoPropriedade(NaoPopular = true)]
        public List<string> GRUPOSAD { get; set; }
    }
}