﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;

public class ControleDeAcessoDAL : DAL
{
    public List<ControleDeAcessoTO.ObterInformacoesUsuario> ObterInformacoesUsuario(string NOMUSRRCF)
    {
        ControleDeAcessoDALSQL DALSQL = new ControleDeAcessoDALSQL();
        string cmdSql = System.Convert.ToString(DALSQL.ObterInformacoesUsuario());
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("NOMUSRRCF", NOMUSRRCF);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);

        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<ControleDeAcessoTO.ObterInformacoesUsuario>();
    }
}