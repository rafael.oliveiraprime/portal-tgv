﻿using System.Text;
using Microsoft.VisualBasic;

public class ControleDeAcessoDALSQL
{
    public string ObterInformacoesUsuario()
    {
        return @"
            SELECT A.CODFNC, A.NOMFNC, UPPER(TRIM(B.NOMUSRRCF)) AS NOMUSRRCF, LOWER(TRIM(A.DESENDCREETNFNC)) AS DESENDCREETNFNC
                FROM MRT.T0100361 A
                INNER JOIN MRT.T0104596 B ON (B.CODFNC = A.CODFNC)
                WHERE UPPER(TRIM(B.NOMUSRRCF)) = UPPER(TRIM(:NOMUSRRCF))
                  AND A.DATDEMFNC IS NULL
                ORDER BY NOMFNC ASC
                ";
    }
}