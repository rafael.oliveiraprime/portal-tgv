﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CadastroAnotacaoBLL
{
    public List<CadastroAnotacaoTO.obterCadastroAnotacao> obterCadastroAnotacao(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroAnotacaoDAL();
            return DAL.obterCadastroAnotacao(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO ANOTACAO.");
            Utilitario.InsereLog(ex, "obterCadastroAnotacao", "CadastroAnotacaoApiModel.CadastroAnotacaoApiModel", "CadastroAnotacaoBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO ANOTACAO.");
            throw;
        }
    }

    public List<CadastroAnotacaoTO.obterCadastroAnotacaoOportunidade> obterCadastroAnotacaoOportunidade(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroAnotacaoDAL();
            return DAL.obterCadastroAnotacaoOportunidade(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO ANOTACAO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "obterCadastroAnotacaoOportunidade", "CadastroAnotacaoApiModel.CadastroAnotacaoApiModel", "CadastroAnotacaoBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO ANOTACAO OPORTUNIDADE.");
            throw;
        }
    }

    public CadastroAnotacaoTO.obterPrimeiraAnotacaoOportunidade obterPrimeiraAnotacaoOportunidade(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroAnotacaoDAL();
            return DAL.obterPrimeiraAnotacaoOportunidade(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DA PRIMEIRA ANOTACAO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "obterPrimeiraAnotacaoOportunidade", "CadastroAnotacaoApiModel.CadastroAnotacaoApiModel", "CadastroAnotacaoBLL", "ERRO AO FAZER A CONSULTA DA PRIMEIRA ANOTACAO OPORTUNIDADE.");
            throw;
        }
    }

    public List<CadastroAnotacaoTO.obterCadastroAnotacaoCrm> obterCadastroAnotacaoCrm(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroAnotacaoDAL();
            return DAL.obterCadastroAnotacaoCrm(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO ANOTACAO CRM.");
            Utilitario.InsereLog(ex, "obterCadastroAnotacaoCrm", "CadastroAnotacaoApiModel.CadastroAnotacaoApiModel", "CadastroAnotacaoBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO ANOTACAO CRM.");
            throw;
        }
    }

    public bool inserirCadastroAnotacaoGeral(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objInserir)
    {
        try
        {
            var DAL = new CadastroAnotacaoDAL();
            //
            if (objInserir.NUMNIVGARNGC == -1)
            {
                objInserir.NUMNIVGARNGC = 0;
            }
            //
            if (objInserir.CODOPTVND == -1)
            {
                objInserir.CODOPTVND = 0;
            }
            //
            if (objInserir.DATAGDCTO == null) { 
                objInserir.DATAGDCTO = objInserir.DATAGD + " " + objInserir.HORAGD;
            }
            else if (objInserir.HORAGDCTO != null)
            {

                DateTime _data = DateTime.Parse(objInserir.DATAGDCTO);
                DateTime _datahorainsercao = new DateTime(_data.Year, _data.Month, _data.Day, int.Parse(objInserir.HORAGDCTO.Split(':')[0]), int.Parse(objInserir.HORAGDCTO.Split(':')[1]), 00);

                if (objInserir.HORAGDCTO.Trim() != "")
                    objInserir.DATAGDCTO = _datahorainsercao.ToString();

            }

            if (objInserir.DATAGDCTO == " " || objInserir.DATAGD == "" || objInserir.HORAGD == "")
            {
                objInserir.DATAGDCTO = null;
            }

            objInserir.DATCAD = DateTime.Now.ToString();

            return DAL.inserirCadastroAnotacaoGeral(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO CADASTRO ANOTACAO GERAL.");
            Utilitario.InsereLog(ex, "inserirCadastroAnotacaoGeral", "CadastroAnotacaoApiModel.CadastroAnotacaoApiModel", "CadastroAnotacaoBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO ANOTACAO GERAL.");
            throw;
        }
    }

    public bool inserirCadastroAnotacaoNegociacao(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objInserir)
    {
        try
        {
            var DAL = new CadastroAnotacaoDAL();
            return DAL.inserirCadastroAnotacaoNegociacao(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO CADASTRO ANOTACAO NEGOCIACAO.");
            Utilitario.InsereLog(ex, "inserirCadastroAnotacaoNegociacao", "CadastroAnotacaoApiModel.CadastroAnotacaoApiModel", "CadastroAnotacaoBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO ANOTACAO NEGOCIACAO.");
            throw;
        }
    }

    public bool inserirCadastroAnotacaoOportunidadeNovo(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objInserir)
    {
        try
        {
            var DAL = new CadastroAnotacaoDAL();

            //objInserir.TIPOBSCLI = "1";
            //objInserir.CODMTVCTO = 0;
            //objInserir.DATAGDCTO = DateTime.Now.AddDays(1).ToShortDateString();
            //objInserir.DATCAD = DateTime.Now.ToShortDateString();

            //DAL.inserirCadastroAnotacaoGeral(objInserir);
            //INSERT INTO MRT.CADOBSCLITGV(CODCLI, NUMSEQOBS, TIPOBSCLI, CODMTVCTO, DESOBS, DATAGDCTO, CODFNCCAD, DATCAD, CODOPTVND)
            //     VALUES(:CODCLI,
            //     (SELECT COALESCE(MAX(NUMSEQOBS), 0) + 1 FROM MRT.CADOBSCLITGV WHERE CODCLI = :CODCLI),
            //     1, 0, :DESOBS, SYSDATE + 1, :CODFNCCAD, SYSDATE, (SELECT COALESCE(MAX(CODOPTVND), 0) FROM MRT.CADOPTVNDCLITGV))


            return DAL.inserirCadastroAnotacaoOportunidadeNovo(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO CADASTRO ANOTACAO OPORTUNIDADE NOVO.");
            Utilitario.InsereLog(ex, "inserirCadastroAnotacaoOportunidadeNovo", "CadastroAnotacaoApiModel.CadastroAnotacaoApiModel", "CadastroAnotacaoBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO ANOTACAO OPORTUNIDADE NOVO.");
            throw;
        }
    }

    public bool inserirCadastroAnotacaoOportunidade(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objInserir)
    {
        try
        {
            var DAL = new CadastroAnotacaoDAL();
            return DAL.inserirCadastroAnotacaoOportunidade(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO CADASTRO ANOTACAO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "inserirCadastroAnotacaoOportunidade", "CadastroAnotacaoApiModel.CadastroAnotacaoApiModel", "CadastroAnotacaoBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO ANOTACAO OPORTUNIDADE.");
            throw;
        }
    }

    public bool alterarCadastroAnotacao(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroAnotacaoDAL();
            return DAL.alterarCadastroAnotacao(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTARACAO DO CADASTRO ANOTACAO.");
            Utilitario.InsereLog(ex, "alterarCadastroAnotacao", "CadastroAnotacaoApiModel.CadastroAnotacaoApiModel", "CadastroAnotacaoBLL", "ERRO AO FAZER A ALTARACAO DO CADASTRO ANOTACAO.");
            throw;
        }
    }


    public bool obterCadastroAnotacaoNegociacaoObsDesconto(int _CODOPTVND)
    {
        List<CadastroAnotacaoTO.obterCadastroAnotacaoNegociacaoObsDesconto> _list = new List<CadastroAnotacaoTO.obterCadastroAnotacaoNegociacaoObsDesconto>();
        bool _existeobsdesconto = false;

        try
        {
            var DAL = new CadastroAnotacaoDAL();
            _list = DAL.obterCadastroAnotacaoNegociacaoObsDesconto(_CODOPTVND);

            if (_list.Count > 0)
                _existeobsdesconto = true;


            return _existeobsdesconto;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO ANOTACAO NEGOCIACAO.");
            Utilitario.InsereLog(ex, "obterCadastroAnotacaoNegociacaoObsDesconto", "CadastroAnotacaoApiModel.CadastroAnotacaoApiModel", "CadastroAnotacaoBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO ANOTACAO NEGOCIACAO.");
            throw;
        }
    }


}