﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class CadastroAnotacaoDALSQL
{
    public string obterCadastroAnotacao(List<long> LISTTIPOBSCLI)
    {
        return @"
                 SELECT ANT.CODCLI, ANT.NUMSEQOBS, 
                 MTV.DESMTVCTO, 
                 ANT.DESOBS AS DESOBSCOMPLETA, 
                 SUBSTR(ANT.DESOBS, 1, 50) AS DESOBSRESUMIDA,
                 TRIM(ANT.NUMNIVGARNGC) NUMNIVGARNGC, 
                 COALESCE(TO_CHAR(ANT.DATAGDCTO, 'DD/MM/YYYY HH24:MI'),' ') AS DATAGDCTO,
                 COALESCE(TO_CHAR(ANT.DATCAD, 'DD/MM/YYYY HH24:MI'),' ') AS DATCAD,
                 FNC.NOMFNC, ANT.CODOPTVND, ANT.TIPOBSCLI
                 FROM MRT.CADOBSCLITGV ANT
                 LEFT JOIN MRT.CADMTVCTOTGV MTV ON MTV.CODMTVCTO = ANT.CODMTVCTO 
                 LEFT JOIN MRT.T0100361 FNC ON FNC.CODFNC = ANT.CODFNCCAD
                 WHERE ANT.CODCLI = :CODCLI 
                   AND ANT.CODOPTVND = :CODOPTVND
                   AND ANT.TIPOBSCLI IN (#LISTDESOBS)            
                 ORDER BY ANT.DATCAD DESC
                ".Replace("#LISTDESOBS", ToList(LISTTIPOBSCLI)); 
    }

    public string obterCadastroAnotacaoOportunidade()
    {
        return @"
                 SELECT ANT.CODCLI, ANT.NUMSEQOBS, 
                 MTV.DESMTVCTO, 
                 ANT.DESOBS AS DESOBSCOMPLETA, 
                 SUBSTR(ANT.DESOBS, 1, 50) AS DESOBSRESUMIDA,
                 ANT.NUMNIVGARNGC, ANT.DATAGDCTO, FNC.NOMFNC, ANT.DATCAD, ANT.CODOPTVND
                 FROM MRT.CADOBSCLITGV ANT
                 LEFT JOIN MRT.CADMTVCTOTGV MTV ON MTV.CODMTVCTO = ANT.CODMTVCTO 
                 LEFT JOIN MRT.T0100361 FNC ON FNC.CODFNC = ANT.CODFNCCAD
                 WHERE ANT.CODOPTVND = :CODOPTVND
                 AND ANT.CODCLI = :CODCLI  
                ";
    }

    public string obterPrimeiraAnotacaoOportunidade()
    {
        return @"
                 SELECT DESOBS                               
                 FROM MRT.CADOBSCLITGV                 
                 WHERE CODOPTVND = :CODOPTVND
                 AND CODCLI = :CODCLI 
                 AND TIPOBSCLI = 1
                ";
    }

    public string obterCadastroAnotacaoCrm()
    {
        return @"
                 SELECT ANT.CODCLI, ANT.NUMSEQOBS, 
                 TRIM(MTV.DESMTVCTO) DESMTVCTO, 
                 ANT.DESOBS DESOBSCOMPLETA, 
                 TRIM(ANT.NUMNIVGARNGC) NUMNIVGARNGC, 
                 COALESCE(TO_CHAR(ANT.DATAGDCTO, 'DD/MM/YYYY HH24:MM'),' ') AS DATAGDCTO,
                 COALESCE(TRIM(FNC.NOMFNC),' ') NOMFNC, 
                 COALESCE(TO_CHAR(ANT.DATCAD, 'DD/MM/YYYY'),' ') AS DATCAD,
                 ANT.TIPOBSCLI,
                 DECODE(ANT.TIPOBSCLI, 1, 'IND.', 2, 'NEG.', 3, 'ACEITE', 4, 'DESC.', 5, 'GEST.', 10, 'DOSSIE', 11, 'COBRA', ' ') NOMTIPOBSCLI,
                 DECODE(ANT.TIPOBSCLI, 1, '#904921', 2, '#75a433', 3, '#f3a619', 4, '#c84320', 5, '#232859', 10, '#2c8e86', 11, '#d9dade', ' ') CORTIPOBSCLI
                 FROM MRT.CADOBSCLITGV ANT
                 LEFT JOIN MRT.CADMTVCTOTGV MTV ON MTV.CODMTVCTO = ANT.CODMTVCTO 
                 LEFT JOIN MRT.T0100361 FNC ON FNC.CODFNC = ANT.CODFNCCAD
                 WHERE ANT.CODCLI = :CODCLI 
                 AND CODFNCCAD = :CODCON
                 AND TIPOBSCLI = :TIPANT
                 AND ANT.CODMTVCTO = :MTVANT
                 AND TRUNC(DATAGDCTO) = TRUNC(TO_DATE(:DATAGD,'DD/MM/YYYY'))
                 AND TRUNC(DATCAD) = TRUNC(TO_DATE(:DATCADFIL,'DD/MM/YYYY'))
                 ORDER BY ANT.NUMSEQOBS DESC
                ";
    }

    public string inserirCadastroAnotacaoNegociacao()
    {
        return @"
                 INSERT INTO MRT.CADOBSCLITGV (CODCLI, NUMSEQOBS, TIPOBSCLI, CODMTVCTO, DESOBS, NUMNIVGARNGC, DATAGDCTO, CODFNCCAD, DATCAD, CODOPTVND)
                 VALUES (:CODCLI, 
                 (SELECT COALESCE(MAX(NUMSEQOBS),0)+1 FROM MRT.CADOBSCLITGV WHERE CODCLI = :CODCLI),
                 2, :CODMTVCTO, :DESOBS, :NUMNIVGARNGC, :DATAGDCTO, :CODFNCCAD, SYSDATE, :CODOPTVND)
                ";
    }    

    //public string inserirCadastroAnotacaoOportunidadeNovo(string unions)
    //{

    //    StringBuilder strBld = new StringBuilder(@"
    //         INSERT INTO MRT.CADOBSCLITGV
    //         SELECT :CODCLI, 
    //         (SELECT COALESCE(MAX(NUMSEQOBS),0)+1 FROM MRT.CADOBSCLITGV)
    //         :TIPOBSCLI, :CODMTVCTO, :DATCAD
    //         (SELECT COALESCE(MAX(CODOPTVND),0) FROM MRT.CADOPTVNDCLITGV), 
    //         CODSVCTGV, 0, 1, 1
    //         FROM MRT.CADSVCTGV 
    //    ");

    //    //StringBuilder strBld = new StringBuilder(@"
    //    //         INSERT INTO MRT.CADOBSCLITGV (CODCLI, NUMSEQOBS, TIPOBSCLI, CODMTVCTO, DESOBS, CODFNCCAD, DATCAD, CODOPTVND)
    //    //         VALUES (:CODCLI, 
    //    //         (SELECT COALESCE(MAX(NUMSEQOBS),0)+1 FROM MRT.CADOBSCLITGV),
    //    //         1, 1, UPPER(:DESOBS), :CODFNCCAD, SYSDATE, (SELECT COALESCE(MAX(CODOPTVND),0) FROM MRT.CADOPTVNDCLITGV))
    //    //        ");

    //    //strBld.AppendFormat("WHERE DESOBS IN ({0})", u).AppendLine();

    //    return strBld.ToString();
    //}

    //public string inserirCadastroAnotacaoOportunidadeNovo(List<long> LISTDESOBS)
    //{
    //    return @"
    //             INSERT INTO MRT.CADOBSCLITGV
    //             WITH TEMP_OBS AS (SELECT #LISTDESOBS AS OBS FROM DUAL)
    //             , tabAux AS (
    //             SELECT REGEXP_SUBSTR (obs, '[^,]+', 1, ROWNUM) || ',' SPLIT
    //             FROM temp_obs
    //             CONNECT BY LEVEL <= LENGTH (REGEXP_REPLACE (obs, '[^,]+')) + 1 
    //             )

    //             SELECT :CODCLI
    //             , (SELECT COALESCE(MAX(NUMSEQOBS),0) FROM MRT.CADOBSCLITGV) + rownum
    //             , 1
    //             , 1
    //             , split
    //             , 0
    //             , null
    //             , sysdate
    //             , :CODFNCCAD
    //             , (SELECT COALESCE(MAX(CODOPTVND),0) FROM MRT.CADOPTVNDCLITGV)             
    //             FROM tabaux
    //             ".Replace("#LISTDESOBS", ToList(LISTDESOBS)); ;
    //}

    public string inserirCadastroAnotacaoOportunidade()
    {
        return @"
                 INSERT INTO MRT.CADOBSCLITGV (CODCLI, NUMSEQOBS, TIPOBSCLI, CODMTVCTO, DESOBS, DATAGDCTO, CODFNCCAD, DATCAD, CODOPTVND)
                 VALUES (:CODCLI, 
                 (SELECT COALESCE(MAX(NUMSEQOBS),0)+1 FROM MRT.CADOBSCLITGV WHERE CODCLI = :CODCLI),
                 1, 1, :DESOBS, SYSDATE + 1, :CODFNCCAD, SYSDATE, :CODOPTVND)
                ";
    }

    public string inserirCadastroAnotacaoOportunidadeNovo()
    {
        return @"
                 INSERT INTO MRT.CADOBSCLITGV (CODCLI, NUMSEQOBS, TIPOBSCLI, CODMTVCTO, DESOBS, DATAGDCTO, CODFNCCAD, DATCAD, CODOPTVND)
                 VALUES (:CODCLI, 
                 (SELECT COALESCE(MAX(NUMSEQOBS),0)+1 FROM MRT.CADOBSCLITGV WHERE CODCLI = :CODCLI),
                 1, 0, :DESOBS, SYSDATE + 1, :CODFNCCAD, SYSDATE, (SELECT COALESCE(MAX(CODOPTVND),0) FROM MRT.CADOPTVNDCLITGV))
                ";
    }

    public string alterarCadastroAnotacao()
    {
        return @"
                 UPDATE MRT.CADOBSCLITGV SET
                 TIPOBSCLI = :TIPOBSCLI,
                 CODMTVCTO = :CODMTVCTO,
                 DESOBS = :DESOBS,
                 NUMNIVGARNGC = :NUMNIVGARNGC,
                 DATAGDCTO =:DATAGDCTO
                 WHERE CODCLI = :CODCLI
                 AND NUMSEQOBS = :NUMSEQOBS
                 AND CODOPTVND =:CODOPTVND
                ";
    }

    public string inserirCadastroAnotacaoGeral()
    {
        return @"
                 INSERT INTO MRT.CADOBSCLITGV (CODCLI, NUMSEQOBS, TIPOBSCLI, CODMTVCTO, DESOBS, CODFNCCAD, DATCAD, CODOPTVND, DATAGDCTO, NUMNIVGARNGC)
                 VALUES (:CODCLI, 
                 (SELECT COALESCE(MAX(NUMSEQOBS),0)+1 FROM MRT.CADOBSCLITGV WHERE CODCLI = :CODCLI ),
                 :TIPOBSCLI, :CODMTVCTO, :DESOBS, :CODFNCCAD, :DATCAD, :CODOPTVND, :DATAGDCTO, :NUMNIVGARNGC)
                ";
    }


    public string obterCadastroAnotacaoNegociacaoObsDesconto()
    {
        return @"
                 SELECT ANT.CODCLI
                   FROM MRT.CADOBSCLITGV ANT
                   LEFT JOIN MRT.CADMTVCTOTGV MTV ON MTV.CODMTVCTO = ANT.CODMTVCTO 
                   LEFT JOIN MRT.T0100361 FNC ON FNC.CODFNC = ANT.CODFNCCAD
                  WHERE ANT.CODOPTVND = :CODOPTVND
                    AND ANT.TIPOBSCLI = 2
                    AND ANT.CODMTVCTO = 10
                ";
    }

    public string ToList(List<long> LISTOBS)
    {
        string retr = string.Empty;
        bool firstTime = true;

        foreach (long Item in LISTOBS)
        {
            if (firstTime)
            {
                firstTime = false;

                retr = (Item.ToString());
            }
            else
            {
                retr = (string.Format("{0}, {1}", retr, Item));
            }
        }

        return retr.Trim();
    }
}