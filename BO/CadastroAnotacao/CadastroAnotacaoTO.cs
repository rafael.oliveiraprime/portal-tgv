﻿using System.Collections.Generic;

namespace CadastroAnotacaoTO
{
    public class obterCadastroAnotacao
    {
        public int CODCLI { get; set; }

        public int NUMSEQOBS { get; set; }

        public string DESMTVCTO { get; set; }

        public string NOMFNC { get; set; }

        public string DESOBSCOMPLETA { get; set; }

        public string DESOBSRESUMIDA { get; set; }

        public string TIPOBSCLI { get; set; }

        public int NUMNIVGARNGC { get; set; }

        public string DATAGDCTO { get; set; }

        public string DATCAD { get; set; }
    }

    public class obterCadastroAnotacaoOportunidade
    {
        public int CODCLI { get; set; }

        public int NUMSEQOBS { get; set; }

        public string DESMTVCTO { get; set; }

        public string NOMFNC { get; set; }

        public string DESOBSCOMPLETA { get; set; }

        public string DESOBSRESUMIDA { get; set; }

        public int NUMNIVGARNGC { get; set; }

        public string DATAGDCTO { get; set; }

        public string DATCAD { get; set; }

        public int CODOPTVND { get; set; }
    }

    public class obterPrimeiraAnotacaoOportunidade
    {
        public string DESOBS { get; set; }        
    }

    public class obterCadastroAnotacaoNegociacaoObsDesconto
    {
        public int CODCLI { get; set; }
    }

    public class obterCadastroAnotacaoCrm
    {
        public int CODCLI { get; set; }

        public int NUMSEQOBS { get; set; }

        public string DESMTVCTO { get; set; }

        public string DESOBSCOMPLETA { get; set; }

        public int NUMNIVGARNGC { get; set; }

        public string DATAGDCTO { get; set; }

        public string NOMFNC { get; set; }

        public string DATCAD { get; set; }

        public string TIPOBSCLI { get; set; }

        public string NOMTIPOBSCLI { get; set; }

        public string CORTIPOBSCLI { get; set; }
    }
}