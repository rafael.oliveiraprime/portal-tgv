﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

public class CadastroAnotacaoDAL : DAL
{
    public List<CadastroAnotacaoTO.obterCadastroAnotacao> obterCadastroAnotacao(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objPesquisar)
    {
        var DALSQL = new CadastroAnotacaoDALSQL();
        string cmdSql = DALSQL.obterCadastroAnotacao(objPesquisar.LISTTIPOBSCLI);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (objPesquisar.CODCLI == 0)
        {
            objPesquisar.CODCLI = -1;
        }
        //
        if (objPesquisar.CODOPTVND == 0)
        {
            objPesquisar.CODOPTVND = -1;
        }
        //        
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("CODOPTVND", objPesquisar.CODOPTVND);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroAnotacaoTO.obterCadastroAnotacao>();
    }

    public List<CadastroAnotacaoTO.obterCadastroAnotacaoOportunidade> obterCadastroAnotacaoOportunidade(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objPesquisar)
    {
        var DALSQL = new CadastroAnotacaoDALSQL();
        string cmdSql = DALSQL.obterCadastroAnotacaoOportunidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (objPesquisar.CODOPTVND == -1)
        {
            objPesquisar.CODOPTVND = 0;
        }
        //        
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("CODOPTVND", objPesquisar.CODOPTVND);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroAnotacaoTO.obterCadastroAnotacaoOportunidade>();
    }

    public CadastroAnotacaoTO.obterPrimeiraAnotacaoOportunidade obterPrimeiraAnotacaoOportunidade(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objPesquisar)
    {
        var DALSQL = new CadastroAnotacaoDALSQL();
        string cmdSql = DALSQL.obterPrimeiraAnotacaoOportunidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //               
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("CODOPTVND", objPesquisar.CODOPTVND);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        var retorno = MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroAnotacaoTO.obterPrimeiraAnotacaoOportunidade>();
        if (retorno.Count == 1)
        {
            return retorno.FirstOrDefault();
        }
        else
        {
            return new CadastroAnotacaoTO.obterPrimeiraAnotacaoOportunidade();
        }
    }

    public List<CadastroAnotacaoTO.obterCadastroAnotacaoCrm> obterCadastroAnotacaoCrm(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objPesquisar)
    {
        var DALSQL = new CadastroAnotacaoDALSQL();
        string cmdSql = DALSQL.obterCadastroAnotacaoCrm();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("CODCON", objPesquisar.CODCONFIL);
        dbCommand.AddWithValue("TIPANT", objPesquisar.TIPANTFIL);
        dbCommand.AddWithValue("MTVANT", objPesquisar.MTVANTFIL);

        if (objPesquisar.DATAGDFIL == null)
            dbCommand.AddWithValue("DATAGD", objPesquisar.DATAGDFIL);
        else
            dbCommand.AddWithValue("DATAGD", Convert.ToDateTime(objPesquisar.DATAGDFIL).ToShortDateString());
        //
        if (objPesquisar.DATCADFIL == null)
            dbCommand.AddWithValue("DATCADFIL", objPesquisar.DATCADFIL);
        else
            dbCommand.AddWithValue("DATCADFIL", Convert.ToDateTime(objPesquisar.DATCADFIL).ToShortDateString());
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroAnotacaoTO.obterCadastroAnotacaoCrm>();
    }

    public bool inserirCadastroAnotacaoNegociacao(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objInserir)
    {
        var DALSQL = new CadastroAnotacaoDALSQL();
        string cmdSql = DALSQL.inserirCadastroAnotacaoNegociacao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //                    
        objInserir.CODFNCCAD = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //
        if (objInserir.NUMNIVGARNGC == -1)
        {
            objInserir.NUMNIVGARNGC = 0;
        }
        //
        if (objInserir.CODOPTVND == -1)
        {
            objInserir.CODOPTVND = 0;
        }
        //
        string dataAgendamento = objInserir.DATAGDCTO + " " + objInserir.HORAGDCTO;
        //
        dbCommand.AddWithValue("CODCLI", objInserir.CODCLI);
        dbCommand.AddWithValue("CODMTVCTO", objInserir.CODMTVCTO);
        dbCommand.AddWithValue("DESOBS", objInserir.DESOBS);
        dbCommand.AddWithValue("NUMNIVGARNGC", objInserir.NUMNIVGARNGC);
        //
        if (dataAgendamento != " ")
        {
            dbCommand.AddWithValue("DATAGDCTO", Convert.ToDateTime(dataAgendamento));

        }
        else
        {
            dbCommand.AddWithValue("DATAGDCTO", null);
        }

        dbCommand.AddWithValue("CODFNCCAD", objInserir.CODFNCCAD);
        dbCommand.AddWithValue("CODOPTVND", objInserir.CODOPTVND);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    //public bool inserirCadastroAnotacaoOportunidadeNovo(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objInserir)
    //{
    //    string unions = string.Empty;

    //    foreach (string desc in objInserir.LISTDESOBS) {
    //        unions = string.Concat(unions,string.Format("SELECT \"{0}\" AS DESOBS FROM DUAL {1} ", desc,
    //            (desc.Equals(objInserir.LISTDESOBS.LastOrDefault()) ? string.Empty : "UNION")));
    //    }

    //    var DALSQL = new CadastroAnotacaoDALSQL();
    //    string cmdSql = DALSQL.inserirCadastroAnotacaoOportunidadeNovo(unions);
    //    //
    //    var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
    //    //                    
    //    objInserir.CODFNCCAD = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
    //    //
    //    dbCommand.AddWithValue("CODCLI", objInserir.CODCLI);
    //    dbCommand.AddWithValue("DESOBS", objInserir.DESOBS);
    //    dbCommand.AddWithValue("CODFNCCAD", objInserir.CODFNCCAD);
    //    //
    //    dbCommand.TrataDbCommandUniversal();
    //    //               
    //    return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    //}

    public bool inserirCadastroAnotacaoOportunidadeNovo(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objInserir)
    {
        var DALSQL = new CadastroAnotacaoDALSQL();
        string cmdSql = DALSQL.inserirCadastroAnotacaoOportunidadeNovo();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //                    
        objInserir.CODFNCCAD = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //
        if (objInserir.DESOBS == null)
        {
            objInserir.DESOBS = " ";
        }
        //
        dbCommand.AddWithValue("CODCLI", objInserir.CODCLI);
        dbCommand.AddWithValue("DESOBS", objInserir.DESOBS);
        dbCommand.AddWithValue("CODFNCCAD", objInserir.CODFNCCAD);
        //
        dbCommand.TrataDbCommandUniversal();
        //              
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool inserirCadastroAnotacaoOportunidade(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objInserir)
    {
        var DALSQL = new CadastroAnotacaoDALSQL();
        string cmdSql = DALSQL.inserirCadastroAnotacaoOportunidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //                    
        objInserir.CODFNCCAD = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //
        dbCommand.AddWithValue("CODCLI", objInserir.CODCLI);
        dbCommand.AddWithValue("CODOPTVND", objInserir.CODOPTVND);
        dbCommand.AddWithValue("DESOBS", objInserir.DESOBS);
        dbCommand.AddWithValue("CODFNCCAD", objInserir.CODFNCCAD);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool alterarCadastroAnotacao(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objAlterar)
    {
        var DALSQL = new CadastroAnotacaoDALSQL();
        string cmdSql = DALSQL.alterarCadastroAnotacao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("NUMSEQOBS", objAlterar.NUMSEQOBS);
        dbCommand.AddWithValue("CODCLI", objAlterar.CODCLI);
        dbCommand.AddWithValue("TIPOBSCLI", objAlterar.TIPOBSCLI);
        dbCommand.AddWithValue("CODMTVCTO", objAlterar.CODMTVCTO);
        dbCommand.AddWithValue("DESOBS", objAlterar.DESOBS);
        dbCommand.AddWithValue("NUMNIVGARNGC", objAlterar.NUMNIVGARNGC);
        dbCommand.AddWithValue("DATAGDCTO", Convert.ToDateTime(objAlterar.DATAGDCTO));
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public bool inserirCadastroAnotacaoGeral(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objInserir)
    {
        var DALSQL = new CadastroAnotacaoDALSQL();
        string cmdSql = DALSQL.inserirCadastroAnotacaoGeral();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (objInserir.CODFNCCAD == 0)
            objInserir.CODFNCCAD = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //
        dbCommand.AddWithValue("CODCLI", objInserir.CODCLI);
        dbCommand.AddWithValue("TIPOBSCLI", objInserir.TIPOBSCLI);
        dbCommand.AddWithValue("CODMTVCTO", objInserir.CODMTVCTO);
        dbCommand.AddWithValue("DESOBS", objInserir.DESOBS);
        dbCommand.AddWithValue("CODFNCCAD", objInserir.CODFNCCAD);
        dbCommand.AddWithValue("DATCAD", DateTime.Parse(objInserir.DATCAD));
        dbCommand.AddWithValue("CODOPTVND", objInserir.CODOPTVND);
        dbCommand.AddWithValue("NUMNIVGARNGC", objInserir.NUMNIVGARNGC);
        //
        //Processo para verificar adicionar a data como null
        if (objInserir.DATAGDCTO == null)
        {
            dbCommand.AddWithValue("DATAGDCTO", objInserir.DATAGDCTO);
        }
        else
        {
            dbCommand.AddWithValue("DATAGDCTO", DateTime.Parse(objInserir.DATAGDCTO));
        }
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public List<CadastroAnotacaoTO.obterCadastroAnotacaoNegociacaoObsDesconto> obterCadastroAnotacaoNegociacaoObsDesconto(int _CODOPTVND)
    {
        var DALSQL = new CadastroAnotacaoDALSQL();
        string cmdSql = DALSQL.obterCadastroAnotacaoNegociacaoObsDesconto();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("CODOPTVND", _CODOPTVND);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroAnotacaoTO.obterCadastroAnotacaoNegociacaoObsDesconto>();
    }
}