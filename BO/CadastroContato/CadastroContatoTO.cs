﻿using System.Collections.Generic;

namespace CadastroContatoTO
{
    public class obterCadastroContato
    {
        public int CODCTO { get; set; }

        public int TIPEDE { get; set; }

        public int CODEDE { get; set; }

        public int CODTIPCTO { get; set; }

        public string DESTIPCTO { get; set; }

        public string NOMCTO { get; set; }

        public string DESENDETNCTO { get; set; }

        public string NUMTLFCEL { get; set; }

        public string NUMTLFCTO { get; set; }

        public string NUMTLFCTOSCD { get; set; }

        public string NUMTLFCTOTCR { get; set; }

        public string NUMTLFCTOQRT { get; set; }

        public string NUMTLFCTOQNT { get; set; }

        public string FNCCAD { get; set; }

        public string DATFNCCAD { get; set; }

        public string FNCALT { get; set; }

        public string DATFNCALT { get; set; }

        public string FNCDST { get; set; }

        public string DATFNCDST { get; set; }

        public string STATUS { get; set; }
    }
}