﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CadastroContatoBLL
{
    public List<CadastroContatoTO.obterCadastroContato> obterCadastroContato(CadastroContatoApiModel.CadastroContatoApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroContatoDAL();
            return DAL.obterCadastroContato(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO CONTATO.");
            Utilitario.InsereLog(ex, "obterCadastroContato", "CadastroContatoApiModel.CadastroContatoApiModel", "CadastroContatoBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO CONTATO.");
            throw;
        }
    }

    public bool inserirCadastroContato(CadastroContatoApiModel.CadastroContatoApiModel objInserir)
    {
        try
        {
            var DAL = new CadastroContatoDAL();
            return DAL.inserirCadastroContato(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO CADASTRO CONTATO.");
            Utilitario.InsereLog(ex, "inserirCadastroContato", "CadastroContatoApiModel.CadastroContatoApiModel", "CadastroContatoBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO CONTATO.");
            throw;
        }
    }

    public bool alterarCadastroContato(CadastroContatoApiModel.CadastroContatoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroContatoDAL();
            return DAL.alterarCadastroContato(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO CADASTRO CONTATO.");
            Utilitario.InsereLog(ex, "alterarCadastroContato", "CadastroContatoApiModel.CadastroContatoApiModel", "CadastroContatoBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO CONTATO.");
            throw;
        }
    }

    public bool ativarCadastroContato(CadastroContatoApiModel.CadastroContatoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroContatoDAL();
            return DAL.ativarCadastroContato(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATIVACAO DO CADASTRO CONTATO.");
            Utilitario.InsereLog(ex, "ativarCadastroContato", "CadastroContatoApiModel.CadastroContatoApiModel", "CadastroContatoBLL", "ERRO AO FAZER A ATIVACAO DO CADASTRO CONTATO.");
            throw;
        }
    }

    public bool desativarCadastroContato(CadastroContatoApiModel.CadastroContatoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroContatoDAL();
            return DAL.desativarCadastroContato(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATIVACAO DO CADASTRO CONTATO.");
            Utilitario.InsereLog(ex, "desativarCadastroContato", "CadastroContatoApiModel.CadastroContatoApiModel", "CadastroContatoBLL", "ERRO AO FAZER A ATIVACAO DO CADASTRO CONTATO.");
            throw;
        }
    }
}