﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

public class CadastroContatoDAL : DAL
{
    public List<CadastroContatoTO.obterCadastroContato> obterCadastroContato(CadastroContatoApiModel.CadastroContatoApiModel objPesquisar)
    {
        var DALSQL = new CadastroContatoDALSQL();
        string cmdSql = DALSQL.obterCadastroContato(objPesquisar.CTOATI);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (objPesquisar.CODCTO == 0)
        {
            objPesquisar.CODCTO = -1;
        }
        dbCommand.AddWithValue("CODCTO", objPesquisar.CODCTO);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroContatoTO.obterCadastroContato>();
    }

    public bool inserirCadastroContato(CadastroContatoApiModel.CadastroContatoApiModel objInserir)
    {
        var DALSQL = new CadastroContatoDALSQL();
        string cmdSql = DALSQL.inserirCadastroContato();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objInserir.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        objInserir.CODFNCCAD = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;

        string NUMTLFCEL;

        if (objInserir.NUMTLFCEL != null)
        {
            NUMTLFCEL = Regex.Replace(objInserir.NUMTLFCEL, "[-)( ]", String.Empty);
        }
        else
        {
            NUMTLFCEL = null;
        }

        string NUMTLFCTO;
        if (objInserir.NUMTLFCTO != null)
        {
            NUMTLFCTO = Regex.Replace(objInserir.NUMTLFCTO, "[-)( ]", String.Empty);
        }
        else
        {
            NUMTLFCTO = null;
        }

        string NUMTLFCTOSCD;
        if (objInserir.NUMTLFCTOSCD != null)
        {
            NUMTLFCTOSCD = Regex.Replace(objInserir.NUMTLFCTOSCD, "[-)( ]", String.Empty);
        }
        else
        {
            NUMTLFCTOSCD = null;
        }

        string NUMTLFCTOTCR;
        if (objInserir.NUMTLFCTOTCR != null)
        {
            NUMTLFCTOTCR = Regex.Replace(objInserir.NUMTLFCTOTCR, "[-)( ]", String.Empty);
        }
        else
        {
            NUMTLFCTOTCR = null;
        }

        string NUMTLFCTOQRT;
        if (objInserir.NUMTLFCTOQRT != null)
        {
            NUMTLFCTOQRT = Regex.Replace(objInserir.NUMTLFCTOQRT, "[-)( ]", String.Empty);
        }
        else
        {
            NUMTLFCTOQRT = null;
        }

        string NUMTLFCTOQNT;
        if (objInserir.NUMTLFCTOQNT != null)
        {
            NUMTLFCTOQNT = Regex.Replace(objInserir.NUMTLFCTOQNT, "[-)( ]", String.Empty);
        }
        else
        {
            NUMTLFCTOQNT = null;
        }
        
        if(objInserir.TIPEDE == -1)
        {
            objInserir.TIPEDE = 0;
        }

        dbCommand.AddWithValue("CODEDE", objInserir.CODEDE);
        dbCommand.AddWithValue("TIPEDE", objInserir.TIPEDE);
        dbCommand.AddWithValue("CODTIPCTO", objInserir.CODTIPCTO);
        dbCommand.AddWithValue("NOMCTO", objInserir.NOMCTO);
        dbCommand.AddWithValue("DESENDETNCTO", objInserir.DESENDETNCTO);
        dbCommand.AddWithValue("NUMTLFCEL", NUMTLFCEL);
        dbCommand.AddWithValue("NUMTLFCTO", NUMTLFCTO);
        dbCommand.AddWithValue("NUMTLFCTOSCD", NUMTLFCTOSCD);
        dbCommand.AddWithValue("NUMTLFCTOTCR", NUMTLFCTOTCR);
        dbCommand.AddWithValue("NUMTLFCTOQRT", NUMTLFCTOQRT);
        dbCommand.AddWithValue("NUMTLFCTOQNT", NUMTLFCTOQNT);        
        dbCommand.AddWithValue("CODFNCCAD", objInserir.CODFNCCAD);
        dbCommand.AddWithValue("CODFNCALT", objInserir.CODFNCALT);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool alterarCadastroContato(CadastroContatoApiModel.CadastroContatoApiModel objAlterar)
    {
        var DALSQL = new CadastroContatoDALSQL();
        string cmdSql = DALSQL.alterarCadastroContato();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //
        string NUMTLFCEL;

        if (objAlterar.NUMTLFCEL != null)
        {
            NUMTLFCEL = Regex.Replace(objAlterar.NUMTLFCEL, "[-)( ]", String.Empty);
        }
        else
        {
            NUMTLFCEL = null;
        }

        string NUMTLFCTO;
        if (objAlterar.NUMTLFCTO != null)
        {
            NUMTLFCTO = Regex.Replace(objAlterar.NUMTLFCTO, "[-)( ]", String.Empty);
        }
        else
        {
            NUMTLFCTO = null;
        }

        string NUMTLFCTOSCD;
        if (objAlterar.NUMTLFCTOSCD != null)
        {
            NUMTLFCTOSCD = Regex.Replace(objAlterar.NUMTLFCTOSCD, "[-)( ]", String.Empty);
        }
        else
        {
            NUMTLFCTOSCD = null;
        }

        string NUMTLFCTOTCR;
        if (objAlterar.NUMTLFCTOTCR != null)
        {
            NUMTLFCTOTCR = Regex.Replace(objAlterar.NUMTLFCTOTCR, "[-)( ]", String.Empty);
        }
        else
        {
            NUMTLFCTOTCR = null;
        }

        string NUMTLFCTOQRT;
        if (objAlterar.NUMTLFCTOQRT != null)
        {
            NUMTLFCTOQRT = Regex.Replace(objAlterar.NUMTLFCTOQRT, "[-)( ]", String.Empty);
        }
        else
        {
            NUMTLFCTOQRT = null;
        }

        string NUMTLFCTOQNT;
        if (objAlterar.NUMTLFCTOQNT != null)
        {
            NUMTLFCTOQNT = Regex.Replace(objAlterar.NUMTLFCTOQNT, "[-)( ]", String.Empty);
        }
        else
        {
            NUMTLFCTOQNT = null;
        }

        if (objAlterar.TIPEDE == -1)
        {
            objAlterar.TIPEDE = 0;
        }
        
        //
        dbCommand.AddWithValue("CODCTO", objAlterar.CODCTO);
        dbCommand.AddWithValue("CODTIPCTO", objAlterar.CODTIPCTO);
        dbCommand.AddWithValue("NOMCTO", objAlterar.NOMCTO);
        dbCommand.AddWithValue("DESENDETNCTO", objAlterar.DESENDETNCTO);
        dbCommand.AddWithValue("NUMTLFCEL", NUMTLFCEL);
        dbCommand.AddWithValue("NUMTLFCTO", NUMTLFCTO);
        dbCommand.AddWithValue("NUMTLFCTOSCD", NUMTLFCTOSCD);
        dbCommand.AddWithValue("NUMTLFCTOTCR", NUMTLFCTOTCR);
        dbCommand.AddWithValue("NUMTLFCTOQRT", NUMTLFCTOQRT);
        dbCommand.AddWithValue("NUMTLFCTOQNT", NUMTLFCTOQNT);       
        dbCommand.AddWithValue("CODFNCALT", objAlterar.CODFNCALT);
        //       
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool ativarCadastroContato(CadastroContatoApiModel.CadastroContatoApiModel objAlterar)
    {
        var DALSQL = new CadastroContatoDALSQL();
        string cmdSql = DALSQL.ativarCadastroContato(objAlterar.LISTCODCTO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool desativarCadastroContato(CadastroContatoApiModel.CadastroContatoApiModel objAlterar)
    {
        var DALSQL = new CadastroContatoDALSQL();
        string cmdSql = DALSQL.desativarCadastroContato(objAlterar.LISTCODCTO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCDST = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //              
        dbCommand.AddWithValue("CODFNCDST", objAlterar.CODFNCDST);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }
}