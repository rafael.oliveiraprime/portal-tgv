﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class CadastroContatoDALSQL
{
    public string obterCadastroContato(int flgAtivo)
    {
        StringBuilder strBld = new StringBuilder(@"
                 SELECT CTO.CODCTO, CTO.TIPEDE, CTO.CODEDE, CTO.CODTIPCTO, TRIM(TIP.DESTIPCTO) AS DESTIPCTO, 
                 TRIM(CTO.NOMCTO) AS NOMCTO, 
                 TRIM(CTO.DESENDETNCTO) AS DESENDETNCTO,                 
                 CASE WHEN LENGTH(TRIM(CTO.NUMTLFCEL)) =11 THEN ( '(' || SUBSTR(CTO.NUMTLFCEL, 1, 2) || ')' || SUBSTR(CTO.NUMTLFCEL, 3, 5) || '-' ||  SUBSTR(CTO.NUMTLFCEL, 8, 4)) WHEN LENGTH(TRIM(CTO.NUMTLFCEL)) =10 THEN '(' || SUBSTR(CTO.NUMTLFCEL, 1, 2) || ') ' || SUBSTR(CTO.NUMTLFCEL, 3, 4) || '-' || SUBSTR(CTO.NUMTLFCEL, 7, 4) END NUMTLFCEL,                 
                 CASE WHEN LENGTH(TRIM(CTO.NUMTLFCTO)) =11 THEN ( '(' || SUBSTR(CTO.NUMTLFCTO, 1, 2) || ')' || SUBSTR(CTO.NUMTLFCTO, 3, 5) || '-' ||  SUBSTR(CTO.NUMTLFCTO, 8, 4)) WHEN LENGTH(TRIM(CTO.NUMTLFCTO)) =10 THEN '(' || SUBSTR(CTO.NUMTLFCTO, 1, 2) || ') ' || SUBSTR(CTO.NUMTLFCTO, 3, 4) || '-' || SUBSTR(CTO.NUMTLFCTO, 7, 4) END NUMTLFCTO,                 
                 CASE WHEN LENGTH(TRIM(CTO.NUMTLFCTOSCD)) =11 THEN ( '(' || SUBSTR(CTO.NUMTLFCTOSCD, 1, 2) || ')' || SUBSTR(CTO.NUMTLFCTOSCD, 3, 5) || '-' ||  SUBSTR(CTO.NUMTLFCTOSCD, 8, 4)) WHEN LENGTH(TRIM(CTO.NUMTLFCTOSCD)) =10 THEN '(' || SUBSTR(CTO.NUMTLFCTOSCD, 1, 2) || ') ' || SUBSTR(CTO.NUMTLFCTOSCD, 3, 4) || '-' || SUBSTR(CTO.NUMTLFCTOSCD, 7, 4) END NUMTLFCTOSCD,                 
                 CASE WHEN LENGTH(TRIM(CTO.NUMTLFCTOTCR)) =11 THEN ( '(' || SUBSTR(CTO.NUMTLFCTOTCR, 1, 2) || ')' || SUBSTR(CTO.NUMTLFCTOTCR, 3, 5) || '-' ||  SUBSTR(CTO.NUMTLFCTOTCR, 8, 4)) WHEN LENGTH(TRIM(CTO.NUMTLFCTOTCR)) =10 THEN '(' || SUBSTR(CTO.NUMTLFCTOTCR, 1, 2) || ') ' || SUBSTR(CTO.NUMTLFCTOTCR, 3, 4) || '-' || SUBSTR(CTO.NUMTLFCTOTCR, 7, 4) END NUMTLFCTOTCR,                 
                 CASE WHEN LENGTH(TRIM(CTO.NUMTLFCTOQRT)) =11 THEN ( '(' || SUBSTR(CTO.NUMTLFCTOQRT, 1, 2) || ')' || SUBSTR(CTO.NUMTLFCTOQRT, 3, 5) || '-' ||  SUBSTR(CTO.NUMTLFCTOQRT, 8, 4)) WHEN LENGTH(TRIM(CTO.NUMTLFCTOQRT)) =10 THEN '(' || SUBSTR(CTO.NUMTLFCTOQRT, 1, 2) || ') ' || SUBSTR(CTO.NUMTLFCTOQRT, 3, 4) || '-' || SUBSTR(CTO.NUMTLFCTOQRT, 7, 4) END NUMTLFCTOQRT,                 
                 CASE WHEN LENGTH(TRIM(CTO.NUMTLFCTOQNT)) =11 THEN ( '(' || SUBSTR(CTO.NUMTLFCTOQNT, 1, 2) || ')' || SUBSTR(CTO.NUMTLFCTOQNT, 3, 5) || '-' ||  SUBSTR(CTO.NUMTLFCTOQNT, 8, 4)) WHEN LENGTH(TRIM(CTO.NUMTLFCTOQNT)) =10 THEN '(' || SUBSTR(CTO.NUMTLFCTOQNT, 1, 2) || ') ' || SUBSTR(CTO.NUMTLFCTOQNT, 3, 4) || '-' || SUBSTR(CTO.NUMTLFCTOQNT, 7, 4) END NUMTLFCTOQNT,                 
                 CASE CTO.CODFNCDST WHEN CTO.CODFNCDST THEN 'DESATIVADO' ELSE 'ATIVADO' END STATUS,                 
                 TRIM(FNC_CAD.NOMFNC) FNCCAD,
                 TO_CHAR(CTO.DATCAD,'DD/MM/YYYY') DATFNCCAD,
                 TRIM(FNC_ALT.NOMFNC) FNCALT,
                 TO_CHAR(CTO.DATALT,'DD/MM/YYYY') DATFNCALT,
                 CASE CTO.DATDST WHEN CTO.DATDST THEN TRIM(FNC_DST.NOMFNC) ELSE NULL END FNCDST,
                 CASE CTO.DATDST WHEN CTO.DATDST THEN TO_CHAR(CTO.DATDST,'DD/MM/YYYY') ELSE NULL END DATFNCDST
                 FROM MRT.CADCTOTGV CTO 
                 LEFT JOIN MRT.CADTIPCTOTGV TIP ON TIP.CODTIPCTO = CTO.CODTIPCTO
                 LEFT JOIN MRT.T0100361 FNC_CAD ON FNC_CAD.CODFNC = CTO.CODFNCCAD
                 LEFT JOIN MRT.T0100361 FNC_ALT ON FNC_ALT.CODFNC = CTO.CODFNCALT
                 LEFT JOIN MRT.T0100361 FNC_DST ON FNC_DST.CODFNC = CTO.CODFNCDST   
                 WHERE 1=1
                 AND CTO.CODCTO = :CODCTO
                ");

        if (flgAtivo == 1)
        {
            strBld.AppendLine(" AND CTO.DATDST IS NULL ");

        }
        if (flgAtivo == 2)
        {
            strBld.AppendLine(" AND CTO.DATDST IS NOT NULL ");
        }

        strBld.AppendLine("ORDER BY STATUS, CTO.CODCTO");

        return strBld.ToString();
    }

    public string inserirCadastroContato()
    {
        return @"
                 INSERT INTO MRT.CADCTOTGV (CODCTO, TIPEDE, CODEDE, CODTIPCTO, NOMCTO, DESENDETNCTO, NUMTLFCEL, NUMTLFCTO, NUMTLFCTOSCD,
                 NUMTLFCTOTCR, NUMTLFCTOQRT, NUMTLFCTOQNT, CODFNCCAD, DATCAD, CODFNCALT, DATALT)
                 VALUES ((SELECT COALESCE(MAX(CODCTO),0)+1 FROM MRT.CADCTOTGV),
                 :TIPEDE, :CODEDE, :CODTIPCTO, UPPER(:NOMCTO), LOWER(:DESENDETNCTO), :NUMTLFCEL, 
                 :NUMTLFCTO, :NUMTLFCTOSCD, :NUMTLFCTOTCR, :NUMTLFCTOQRT, :NUMTLFCTOQNT, :CODFNCCAD,
                 SYSDATE, :CODFNCALT, SYSDATE)
                ";
    }

    public string alterarCadastroContato()
    {
        return @"
                 UPDATE MRT.CADCTOTGV SET                 
                 CODTIPCTO = :CODTIPCTO,
                 NOMCTO = UPPER(:NOMCTO),
                 DESENDETNCTO = LOWER(:DESENDETNCTO),
                 NUMTLFCEL = :NUMTLFCEL,
                 NUMTLFCTO = :NUMTLFCTO,
                 NUMTLFCTOSCD = :NUMTLFCTOSCD,
                 NUMTLFCTOTCR = :NUMTLFCTOTCR,
                 NUMTLFCTOQRT = :NUMTLFCTOQRT,
                 NUMTLFCTOQNT = :NUMTLFCTOQNT,                 
                 CODFNCALT = :CODFNCALT,
                 DATALT = SYSDATE                 
                 WHERE CODCTO = :CODCTO
                ";
    }

    public string ativarCadastroContato(List<long> LISTCODCTO)
    {
        return @"
                 UPDATE MRT.CADCTOTGV SET                 
                 CODFNCDST = NULL,
                 DATDST = NULL
                 WHERE CODCTO IN(#LISTCODCTO)
                ".Replace("#LISTCODCTO", Utilitario.ToListStr(LISTCODCTO));
    }

    public string desativarCadastroContato(List<long> LISTCODCTO)
    {
        return @"
                 UPDATE MRT.CADCTOTGV SET
                 CODFNCDST = :CODFNCDST,
                 DATDST = SYSDATE
                 WHERE CODCTO IN(#LISTCODCTO)
                ".Replace("#LISTCODCTO", Utilitario.ToListStr(LISTCODCTO));
    }
}