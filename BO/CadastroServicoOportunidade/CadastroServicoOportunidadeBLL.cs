﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CadastroServicoOportunidadeBLL
{
    public List<CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade> obterCadastroServicoOportunidade(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objPesquisar)
    {
        try
        {
            int _codoportunidade = 0;
            string _descservico = "";
            string _NOMINDSTANGC = "";
            string _INDSTANGC = "";
            decimal _VALORCOND = 0;
            decimal _PERCOND = 0;
            int _NUMPTO= 0;
            string _DESTIPSVC="";
            int _CODCNDCMC = 0;

            string _STATUSENVIO = "";
            string _STATUSACEITE = "";
            string _CDOANXCTTCLI = "";

            int _INDSVCVND = 0;

            string _email = "";

            int _codservico = 0;
            decimal _valorservico = 0;
            decimal _valordescserv = 0;
            decimal _perserv = 0;

            decimal _valorServDesc = 0;

            int _codCondicao = 0;

            decimal _valorServCond = 0;

            decimal _valorFinal = 0;
            decimal _valorFinalServico = 0;

            Boolean _finalizouCond = false;
            bool _tevedescontozerado = false;
            decimal _valororiginal = 0;

            int QDEPRDVND = 0;

            int CODGRPEMPFAT = 0;
            string NOMGRPEMPCLI = "";

            bool _existedesconto = false;

            List<CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade> _listretorn = new List<CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade>();
            CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade _itemInsert = new CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade();

            var DAL = new CadastroServicoOportunidadeDAL();
            List<CadastroServicoOportunidadeTO.obterCadastroServicoOportunidadeLocal> _listLocal = DAL.obterCadastroServicoOportunidade(objPesquisar);

            var DAL_Doc_Serv = new CadastroDocConciliacaoDAL();
            List<CadastroDocConciliacaoTO.obterDocRelacaoServico> _listDocs = new List<CadastroDocConciliacaoTO.obterDocRelacaoServico>();
            CadastroDocConciliacaoApiModel.obterDocRelacaoServicoApiModel _consultar = new CadastroDocConciliacaoApiModel.obterDocRelacaoServicoApiModel(); 


            foreach (CadastroServicoOportunidadeTO.obterCadastroServicoOportunidadeLocal _item in  _listLocal)
            {
                if (_item.CODSVCTGV != _codservico)
                {

                    if (_finalizouCond)
                    {
                        //
                        if (_tevedescontozerado)
                            _valorFinalServico = (_valorServDesc - _valororiginal) - _valorServCond;
                        else
                            _valorFinalServico = _valorServDesc - _valorServCond;


                        _itemInsert.CODOPTVND = _codoportunidade;  
                        _itemInsert.CODSVCTGV = _codservico;
                        _itemInsert.VLRSVC = _valorservico; //.ToString();
                        _itemInsert.PERDSC = _perserv;//.ToString("n2");

                        _itemInsert.VLRDSC = _valordescserv;//.ToString("n2");

                        if (_perserv > 0)
                            _itemInsert.VLRTOTDES = ((_valordescserv + _valorServCond) + (_valorservico * (_perserv / 100)));//.ToString("n2");
                        else
                            _itemInsert.VLRTOTDES = (_valordescserv + _valorServCond);//.ToString("n2");


                        _itemInsert.DESSVCTGV = _descservico;
                        _itemInsert.NOMINDSTANGC = _NOMINDSTANGC;
                        _itemInsert.INDSTANGC = _INDSTANGC;
                        _itemInsert.VALORCOND = _VALORCOND;
                        _itemInsert.PERCOND = _PERCOND;
                        _itemInsert.NUMPTOSVC = _NUMPTO;
                        _itemInsert.DESTIPSVC = _DESTIPSVC;
                        _itemInsert.CODCNDCMC = _CODCNDCMC;

                        _itemInsert.STATUSENVIO = _STATUSENVIO;
                        _itemInsert.STATUSACEITE = _STATUSACEITE;
                        _itemInsert.DESENDETN = _email;
                        _itemInsert.CDOANXCTTCLI = _CDOANXCTTCLI;
                        _itemInsert.INDSVCVND = _INDSVCVND;
                        _itemInsert.QDEPRDVND = QDEPRDVND;

                        if (_valorFinalServico > 0)
                            _itemInsert.VLRSVCTOT = Math.Round(_valorFinalServico, 2);//.ToString();
                        else
                            _itemInsert.VLRSVCTOT = 0;


                        if (_existedesconto)
                            _itemInsert.EXISTDESC = 1;
                        else
                            _itemInsert.EXISTDESC = 0;


                        _itemInsert.CODGRPEMPFAT = CODGRPEMPFAT;
                        _itemInsert.NOMGRPEMPCLI = NOMGRPEMPCLI;

                        _valorServCond = 0;
                        _finalizouCond = false;
                        _tevedescontozerado = false;
                        _valororiginal = 0;

                        _codCondicao = 0;

                        _STATUSENVIO = "";
                        _STATUSACEITE = "";
                        _email = "";
                        _CDOANXCTTCLI = "";

                        _existedesconto = false;


                        _consultar.CODSVCTGV = _codservico;
                        _listDocs = DAL_Doc_Serv.obterDocRelacaoServico(_consultar);
                        _itemInsert._listDocs = _listDocs;



                        _listretorn.Add(_itemInsert);

                        _itemInsert = null;
                        _itemInsert = new CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade();

                        _listDocs = null;
                        _consultar = null;
                        _listDocs = new List<CadastroDocConciliacaoTO.obterDocRelacaoServico>();
                        _consultar = new CadastroDocConciliacaoApiModel.obterDocRelacaoServicoApiModel();


                    }

                    _valorServDesc = 0;
                    _valorservico = 0;
                    _valordescserv = 0;
                    _perserv = 0;

                    _codservico = _item.CODSVCTGV;
                    _valorservico = _item.VLRSVC;

                    _codoportunidade = _item.CODOPTVND;
                    _descservico = _item.DESSVCTGV;
                    _NOMINDSTANGC = _item.NOMINDSTANGC;
                    _INDSTANGC = _item.INDSTANGC;
                    _VALORCOND = _item.VALORCOND;
                    _PERCOND = _item.PERCOND;
                    _NUMPTO = _item.NUMPTOSVC;
                    _DESTIPSVC = _item.DESTIPSVC;
                    _CODCNDCMC = _item.CODCNDCMC;
                    QDEPRDVND = _item.QDEPRDVND;

                    CODGRPEMPFAT = _item.CODGRPEMPFAT;

                    NOMGRPEMPCLI = _item.NOMGRPEMPCLI;


                    if ((_item.INDSTANGC == "2") || (_item.INDSTANGC == "6") || (_item.INDSTANGC == "7"))
                    {
                        _STATUSENVIO = _item.STATUSENVIO;
                        _STATUSACEITE = _item.STATUSACEITE;
                        _email = _item.DESENDETN;
                        _CDOANXCTTCLI = _item.CDOANXCTTCLI;
                    }

                    _INDSVCVND = _item.INDSVCVND;

                    //if (_item.VLRDSC != null)
                    //{
                        _valordescserv = decimal.Parse(_item.VLRDSC.ToString("n3"));
                        if (_valordescserv > 0)
                        {
                            _valorServDesc = _valorservico - _valordescserv;

                            if (_valorServDesc == 0)
                                _tevedescontozerado = true;
                        }
                    //}

                    //if (_item.PERDSC != null)
                    //{
                        _perserv = decimal.Parse(_item.PERDSC.ToString("n3"));
                        if (_perserv > 0)
                        {
                            _valorServDesc = _valorservico - (_valorservico * (_perserv / 100));

                            if (_valorServDesc == 0)
                                _tevedescontozerado = true;
                        }
                    //}

                    if (_valorServDesc == 0)
                    {
                        _valororiginal = _valorservico;
                        _valorServDesc = _valorservico;
                    }
                    //_valorFinalServico += _valorServDesc;
                }

                if ((_item.CODCNDCMC != _codCondicao) && (_item.CODSVCTGV == _codservico))
                {
                    _finalizouCond = true;

                    _codCondicao = _item.CODCNDCMC;


                    if (_item.CODCNDCMC >= 9000)
                        _existedesconto = true;

                    //if ((_item.VALORCOND != null) && (_item.VALORCOND > 0))
                    //{
                    //_vlrDescCond = _item.VALORCOND;
                    //_valorServCond += decimal.Parse(_vlrDescCond.ToString("n2"));
                    //}

                    //if ((_item.PERCOND != null) && (_item.PERCOND > 0))
                    //{
                    //_vlrPerCond = _item.PERCOND;
                    //_valorServCond += decimal.Parse((_valorServDesc * (_vlrPerCond / 100)).ToString("n3"));
                    //}
                }
                else
                {
                    if (_item.CODCNDCMC == 0)
                        _finalizouCond = true;
                }

            }

            if (_finalizouCond)
            {
                if (_tevedescontozerado)
                    _valorFinalServico = (_valorServDesc - _valororiginal) - _valorServCond;
                else
                    _valorFinalServico = _valorServDesc - _valorServCond;


                _itemInsert.CODOPTVND = _codoportunidade;
                _itemInsert.CODSVCTGV = _codservico;
                _itemInsert.VLRSVC = _valorservico;//.ToString();

                _itemInsert.PERDSC = _perserv;//.ToString("n2");
                _itemInsert.VLRDSC = _valordescserv;//.ToString("n2");

                _itemInsert.DESSVCTGV = _descservico;
                _itemInsert.NOMINDSTANGC = _NOMINDSTANGC;
                _itemInsert.INDSTANGC = _INDSTANGC;
                _itemInsert.VALORCOND = _VALORCOND;
                _itemInsert.PERCOND = _PERCOND;
                _itemInsert.NUMPTOSVC = _NUMPTO;
                _itemInsert.DESTIPSVC = _DESTIPSVC;
                _itemInsert.CODCNDCMC = _CODCNDCMC;
                _itemInsert.STATUSENVIO = _STATUSENVIO;
                _itemInsert.STATUSACEITE = _STATUSACEITE;
                _itemInsert.DESENDETN = _email;
                _itemInsert.CDOANXCTTCLI = _CDOANXCTTCLI;
                _itemInsert.INDSVCVND = _INDSVCVND;
                _itemInsert.QDEPRDVND = QDEPRDVND;

                //_itemInsert.VLRSVCTOT = Math.Round(_valorFinalServico, 2);//.ToString();
                if (_valorFinalServico > 0)
                    _itemInsert.VLRSVCTOT = Math.Round(_valorFinalServico, 2);//.ToString();
                else
                    _itemInsert.VLRSVCTOT = 0;

                

                if (_perserv > 0)
                    _itemInsert.VLRTOTDES = ((_valordescserv + _valorServCond) + (_valorservico * (_perserv / 100)));//.ToString("n2");
                else
                    _itemInsert.VLRTOTDES = (_valordescserv + _valorServCond);//.ToString("n2");


                if (_valorFinalServico > 0)
                    _valorFinal += _valorFinalServico;
                else
                    _valorFinal += 0;


                if (_existedesconto)
                    _itemInsert.EXISTDESC = 1;
                else
                    _itemInsert.EXISTDESC = 0;


                _itemInsert.CODGRPEMPFAT = CODGRPEMPFAT;
                _itemInsert.NOMGRPEMPCLI = NOMGRPEMPCLI;


                _valorServCond = 0;
                _finalizouCond = false;

                _existedesconto = false;


                _consultar.CODSVCTGV = _codservico;
                _listDocs = DAL_Doc_Serv.obterDocRelacaoServico(_consultar);
                _itemInsert._listDocs = _listDocs;



                _listretorn.Add(_itemInsert);
                _itemInsert = null;
                _itemInsert = new CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade();


                _listDocs = null;
                _consultar = null;
                _listDocs = new List<CadastroDocConciliacaoTO.obterDocRelacaoServico>();
                _consultar = new CadastroDocConciliacaoApiModel.obterDocRelacaoServicoApiModel();
            }


            foreach (var c in _listretorn)
                c.VLRSVCTOTGER = Math.Round(_valorFinal, 2);//.ToString();


            return _listretorn;

        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO SERVIÇO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "obterCadastroServicoOportunidade", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO SERVIÇO OPORTUNIDADE.");
            throw;
        }
    }

    public CadastroServicoOportunidadeTO.obterServicosDesejados obterServicosDesejados(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objPesquisar)
    {
        List<CadastroServicoOportunidadeTO.obterServicosDesejados> _obterServicos = new List<CadastroServicoOportunidadeTO.obterServicosDesejados>();
        CadastroServicoOportunidadeTO.obterServicosDesejados _descricoesservicos = new CadastroServicoOportunidadeTO.obterServicosDesejados();
        string descricoesservicos = "";


        try
        {
            var DAL = new CadastroServicoOportunidadeDAL();
            _obterServicos = DAL.obterServicosDesejados(objPesquisar);

            if (_obterServicos != null)
            {
                if (_obterServicos.Count > 0)
                {
                    foreach(CadastroServicoOportunidadeTO.obterServicosDesejados _item in _obterServicos)
                    {
                        descricoesservicos = descricoesservicos + "  " + _item.CODNOMSVC;
                    }
                    _descricoesservicos.CODNOMSVC = descricoesservicos;
                }
                else
                    _descricoesservicos.CODNOMSVC = "";
            }
            else
                _descricoesservicos.CODNOMSVC = "";

            return _descricoesservicos;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO OBTER A PESQUISA DO CADASTRO SERVICO DESEJADO NEGOCIACAO.");
            Utilitario.InsereLog(ex, "obterServicosDesejados", "CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO OBTER A PESQUISA DO SERVICO DESEJADO NEGOCIACAO.");
            throw;
        }
    }

    public List<CadastroServicoOportunidadeTO.obterCadastroServicoOportunidadeSelect> obterCadastroServicoOportunidadeSelect(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroServicoOportunidadeDAL();
            return DAL.obterCadastroServicoOportunidadeSelect(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO OBTER A PESQUISA DO CADASTRO SERVICO OPORTUNIDADE SELECT2.");
            Utilitario.InsereLog(ex, "obterCadastroServicoOportunidadeSelect", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO OBTER A PESQUISA DO CADASTRO SERVICO OPORTUNIDADE SELECT2.");
            throw;
        }
    }


    public List<CadastroServicoOportunidadeTO.obterAcompanhamentoConciliacao> obterAcompanhamentoConciliacao(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroServicoOportunidadeDAL();

            var retornoDal = DAL.obterAcompanhamentoConciliacao(objPesquisar);
            string minuto, retorno1, retorno2;

            foreach (var item in retornoDal)
            {
                retorno1 = string.Empty;
                retorno2 = string.Empty;

                if (item.Recebido != null && item.Recebido != string.Empty)
                {
                    retorno1 = item.Recebido.Split(':')[0];
                    minuto = item.Recebido.Split(':')[1];
                    item.Recebido = retorno1 +':' + minuto;
                    
                }
                if (item.Enviado != null && item.Enviado != string.Empty)
                {
                    retorno2 = item.Enviado.Split(':')[0];
                    minuto = item.Enviado.Split(':')[1];
                    item.Enviado = retorno2 + ':' + minuto;
                }
            }
            return retornoDal;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO OBTER A PESQUISA DO CADASTRO SERVICO OPORTUNIDADE SELECT2.");
            Utilitario.InsereLog(ex, "obterCadastroServicoOportunidadeSelect", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO OBTER A PESQUISA DO CADASTRO SERVICO OPORTUNIDADE SELECT2.");
            throw;
        }
    }

    public List<CadastroServicoOportunidadeTO.Documento> obterDocumento(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroServicoOportunidadeDAL();
            return DAL.obterDocumento(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO OBTER A PESQUISA DO CADASTRO SERVICO OPORTUNIDADE SELECT2.");
            Utilitario.InsereLog(ex, "obterCadastroServicoOportunidadeSelect", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO OBTER A PESQUISA DO CADASTRO SERVICO OPORTUNIDADE SELECT2.");
            throw;
        }
    }

    public List<CadastroServicoOportunidadeTO.ObterDocumentoNovo> obterDocumentoNovo(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroServicoOportunidadeDAL();

            //objPesquisar.LISTCODSVCTGV = DAL.obterServicOportunidade(objPesquisar.CODOPTVND);
            return DAL.obterDocumentoNovo(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO OBTER A PESQUISA DO CADASTRO SERVICO OPORTUNIDADE SELECT2.");
            Utilitario.InsereLog(ex, "obterCadastroServicoOportunidadeSelect", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO OBTER A PESQUISA DO CADASTRO SERVICO OPORTUNIDADE SELECT2.");
            throw;
        }
    }

    

    public string obterServicoPendente(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objPesquisar)
    {
        CadastroServicoOportunidadeTO.obterServicoPendente _itens;

        string result = "";
        try
        {
            var DAL = new CadastroServicoOportunidadeDAL();
            _itens = DAL.obterServicoPendente(objPesquisar);

            if (_itens != null)
            {
                if (_itens.NEGOCIACAO > 0)
                    result = "1";
                else
                {
                    if (_itens.VENDAGANHA > 0)
                        result = "0";
                    else if (_itens.VENDAPERDIDA > 0)
                        result = "2";
                }
            }
            else
                result = "3";

            return result;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO OBTER A PESQUISA DO SERVICO PENDENTE.");
            Utilitario.InsereLog(ex, "obterServicoPendente", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO OBTER A PESQUISA DO SERVICO PENDENTE.");
            throw;
        }
    }

    public bool inserirCadastroServicoOportunidadeNovo(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objInserir)
    {
        CadastroServicoTO.obterCadastroServico _servicovalores = new CadastroServicoTO.obterCadastroServico();
        CadastroServicoApiModel.CadastroServicoApiModel _servicooportunit = new CadastroServicoApiModel.CadastroServicoApiModel();

        var DAL = new CadastroServicoOportunidadeDAL();
        var dalservico = new CadastroServicoDAL();

        try
        {
            foreach (long _item in objInserir.LISTCODSVCTGV)
            {
                _servicooportunit.CODSVCTGV = int.Parse(_item.ToString());
                _servicovalores = dalservico.obterCadastroServico(_servicooportunit).FirstOrDefault();

                objInserir.VLRSVC = _servicovalores.VLRSVC.ToString();
                objInserir.CODOPTVND = objInserir.CODOPTVND;
                objInserir.CODSVCTGV = int.Parse(_item.ToString());
                objInserir.VLRDSC = "0";
                objInserir.INDSTANGC = 1;
                objInserir.INDSTADSC = 1;
                objInserir.PERDSC = "0";
                objInserir.INDSVCVND = 0;

                DAL.inserirCadastroServicoOportunidadeNovo(objInserir);
                _servicovalores = new CadastroServicoTO.obterCadastroServico();
            }
            return true;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO CADASTRO SERVICO OPORTUNIDADE NOVO.");
            Utilitario.InsereLog(ex, "inserirCadastroServicoOportunidadeNovo", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO SERVICO OPORTUNIDADE NOVO.");
            throw;
        }
    }

    public bool inserirCadastroServicoOportunidadeEditar(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objInserir)
    {

        CadastroServicoTO.obterCadastroServico _servicovalores = new CadastroServicoTO.obterCadastroServico();
        CadastroServicoApiModel.CadastroServicoApiModel _servicooportunit = new CadastroServicoApiModel.CadastroServicoApiModel();

        var DAL = new CadastroServicoOportunidadeDAL();
        var dalservico = new CadastroServicoDAL();

        try
        {
            foreach (long _item in  objInserir.LISTCODSVCTGV)
            {
                _servicooportunit.CODSVCTGV = int.Parse(_item.ToString());
                _servicovalores = dalservico.obterCadastroServico(_servicooportunit).FirstOrDefault();

                objInserir.VLRSVC = _servicovalores.VLRSVC.ToString();
                objInserir.CODOPTVND = objInserir.CODOPTVND;
                objInserir.CODSVCTGV = int.Parse(_item.ToString());
                objInserir.VLRDSC = "0";
                objInserir.INDSTANGC = 1;
                objInserir.INDSTADSC = 1;
                objInserir.PERDSC = "0";
                objInserir.INDSVCVND = 0;


                if (objInserir.INDFATGRP == 1)
                    objInserir.CODGRPEMPFAT = objInserir.CODGRPEMPCLI;


                DAL.inserirCadastroServicoOportunidadeEditar(objInserir);
                _servicovalores = new CadastroServicoTO.obterCadastroServico();
            }

            return true; 
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO CADASTRO SERVICO OPORTUNIDADE EDITAR.");
            Utilitario.InsereLog(ex, "inserirCadastroServicoOportunidadeEditar", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO SERVICO OPORTUNIDADE EDITAR.");
            throw;
        }
    }

    public bool inserirCadastroServicoOportunidadeImport(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objInserir)
    {

        CadastroServicoTO.obterCadastroServico _servicovalores = new CadastroServicoTO.obterCadastroServico();
        CadastroServicoApiModel.CadastroServicoApiModel _servicooportunit = new CadastroServicoApiModel.CadastroServicoApiModel();

        var DAL = new CadastroServicoOportunidadeDAL();
        var dalservico = new CadastroServicoDAL();

        try
        {
            _servicooportunit.CODSVCTGV = Convert.ToInt32(objInserir.CODSVCTGV);
                _servicovalores = dalservico.obterCadastroServico(_servicooportunit).FirstOrDefault();

                objInserir.VLRSVC = _servicovalores.VLRSVC.ToString();
                objInserir.CODOPTVND = objInserir.CODOPTVND;
            objInserir.CODSVCTGV = objInserir.CODSVCTGV;
                objInserir.VLRDSC = "0";
                objInserir.INDSTANGC = 1;
                objInserir.INDSTADSC = 1;
                objInserir.PERDSC = "0";
                objInserir.INDSVCVND = 0;


                if (objInserir.INDFATGRP == 1)
                    objInserir.CODGRPEMPFAT = objInserir.CODGRPEMPCLI;


                DAL.inserirCadastroServicoOportunidadeEditar(objInserir);
                _servicovalores = new CadastroServicoTO.obterCadastroServico();
            

            return true;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO CADASTRO SERVICO OPORTUNIDADE DO IMPORT.");
            Utilitario.InsereLog(ex, "inserirCadastroServicoOportunidadeEditar", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO SERVICO OPORTUNIDADE EDITAR.");
            throw;
        }
    }

    public bool alterarCadastroServicoOportunidade(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroServicoOportunidadeDAL();
            return DAL.alterarCadastroServicoOportunidade(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO CADASTRO SERVICO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "inserirCadastroServicoOportunidadeEditar", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO SERVICO OPORTUNIDADE EDITAR.");
            throw;
        }
    }

    public bool alterarDocumentoAutorizado(CadastroServicoOportunidadeApiModel.DocumentoAlteradoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroServicoOportunidadeDAL();
            return DAL.alterarDocumentoAutorizado(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO CADASTRO SERVICO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "inserirCadastroServicoOportunidadeEditar", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO SERVICO OPORTUNIDADE EDITAR.");
            throw;
        }
    }

    public Int32 VerificaDocumentosPendente(CadastroServicoOportunidadeApiModel.DocumentoAlteradoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroServicoOportunidadeDAL();
            return DAL.VerificaDocumentosPendente(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO CADASTRO SERVICO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "inserirCadastroServicoOportunidadeEditar", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO SERVICO OPORTUNIDADE EDITAR.");
            throw;
        }
    }

    public Int32 VerificaServicosPendente(CadastroServicoOportunidadeApiModel.DocumentoAlteradoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroServicoOportunidadeDAL();
            return DAL.VerificaServicosPendente(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO CADASTRO SERVICO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "inserirCadastroServicoOportunidadeEditar", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO SERVICO OPORTUNIDADE EDITAR.");
            throw;
        }
    }

    public Int32 verificaContratoAssinado(CadastroServicoOportunidadeApiModel.DocumentoAlteradoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroServicoOportunidadeDAL();
            return DAL.verificaContratoAssinado(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO CADASTRO SERVICO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "inserirCadastroServicoOportunidadeEditar", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO SERVICO OPORTUNIDADE EDITAR.");
            throw;
        }
    }

    public bool alterarStatusServico(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroServicoOportunidadeDAL();
            return DAL.alterarStatusServico(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO STATUS SERVICO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "alterarStatusServicoOportunidade", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO FAZER A ALTERACAO DO STATUS SERVICO OPORTUNIDADE.");
            throw;
        }
    }

    public bool alterarStatusOpotunidade(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroServicoOportunidadeDAL();
            return DAL.alterarStatusOportunidade(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO STATUS SERVICO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "alterarStatusServicoOportunidade", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO FAZER A ALTERACAO DO STATUS SERVICO OPORTUNIDADE.");
            throw;
        }
    }
    

    public bool alterarStatusServicoOportunidade(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroServicoOportunidadeDAL();
            return DAL.alterarStatusServicoOportunidade(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO STATUS SERVICO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "alterarStatusServicoOportunidade", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO FAZER A ALTERACAO DO STATUS SERVICO OPORTUNIDADE.");
            throw;
        }
    }

    public bool alterarDescontoCadastroServicoOportunidade(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroServicoOportunidadeDAL();

            if (objAlterar.VLRDSC == null)
                objAlterar.VLRDSC = "0";

            if (objAlterar.PERDSC == null)
                objAlterar.PERDSC = "0";

            return DAL.alterarDescontoCadastroServicoOportunidade(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO DESCONTO DO CADASTRO SERVICO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "alterarDescontoCadastroServicoOportunidade", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO FAZER A ALTERACAO DO DESCONTO DO CADASTRO SERVICO OPORTUNIDADE.");
            throw;
        }
    }

    public bool deletarCadastroServicoOportunidade(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objDeletar)
    {
        try
        {
            var DAL = new CadastroServicoOportunidadeDAL();
            var DALDESC = new CadastroCondicaoComercialServicoOportunidadeDAL();

            CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel objDeletarDesc = new CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel();

            bool deletoulista = false;

            if (objDeletar.LISTCODSVCTGV != null )
            {
                if (objDeletar.LISTCODSVCTGV.Count > 0)
                {
                    foreach (int _codioservico in objDeletar.LISTCODSVCTGV)
                    {
                        deletoulista = true;
                        objDeletar.CODSVCTGV = _codioservico;
                        DAL.deletarCadastroServicoOportunidade(objDeletar);

                        objDeletarDesc.CODOPTVND = objDeletar.CODOPTVND;
                        objDeletarDesc.CODSVCTGV = _codioservico;
                        DALDESC.deletarCadastroCondicaoComercialOportunidade(objDeletarDesc);
                        objDeletarDesc = new CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel(); 
                    }
                }
            }

            if (!deletoulista)
            {
                DAL.deletarCadastroServicoOportunidade(objDeletar);
                DALDESC.deletarCadastroCondicaoComercialServicoOportunidade(objDeletarDesc);

                objDeletarDesc.CODOPTVND = objDeletar.CODOPTVND;
                objDeletarDesc.CODSVCTGV = objDeletar.CODSVCTGV.Value;

                objDeletarDesc = new CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel();
            }


            return true;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A DELECAO DO CADASTRO SERVICO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "deletarCadastroServicoOportunidade", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO FAZER A DELECAO DO CADASTRO SERVICO OPORTUNIDADE.");
            throw;
        }
    }
    
    public bool alterarEmailServicoOportunidade(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objAlterar, bool _enviouemail)
    {
        try
        {
            var DAL = new CadastroServicoOportunidadeDAL();
            return DAL.alterarEmailServicoOportunidade(objAlterar, _enviouemail);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO DESCONTO DO CADASTRO SERVICO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "alterarDescontoCadastroServicoOportunidade", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO FAZER A ALTERACAO DO DESCONTO DO CADASTRO SERVICO OPORTUNIDADE.");
            throw;
        }
    }

    public bool alterarpdftesteServicoOportunidade(int CODOPTVND, int CODSVCTGV, string pdfteste)
    {
        try
        {
            var DAL = new CadastroServicoOportunidadeDAL();
            return DAL.alterarpdftesteServicoOportunidade(CODOPTVND, CODSVCTGV, pdfteste);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO DESCONTO DO CADASTRO SERVICO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "alterarDescontoCadastroServicoOportunidade", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO FAZER A ALTERACAO DO DESCONTO DO CADASTRO SERVICO OPORTUNIDADE.");
            throw;
        }
    }


    public bool alterastatussevicooportunidade(int _codservico)
    {

        return true;
    }


    public int ObterSehouveDesconto(int _codoportunidade)
    {

        try
        { 
            int _existedesconto = 0;
            var DAL = new CadastroServicoOportunidadeDAL();

            CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objPesquisar = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
            objPesquisar.CODOPTVND = _codoportunidade;

            List<CadastroServicoOportunidadeTO.obterCadastroServicoOportunidadeLocal> _listLocal = DAL.obterCadastroServicoOportunidade(objPesquisar);

            foreach (CadastroServicoOportunidadeTO.obterCadastroServicoOportunidadeLocal _item in _listLocal)
            {
                if (_item.CODCNDCMC >= 9000)
                    _existedesconto = 1;
            }

            //if (_existedesconto == 1)
            //{
            //    var BLL = new CadastroAnotacaoBLL();
            //    _existeobs = BLL.obterCadastroAnotacaoNegociacaoObsDesconto(_codoportunidade);

            //    if (_existeobs)
            //        _existedesconto = 2;
            //}

            return _existedesconto;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO SERVIÇO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "ObterSehouveDesconto", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO SERVIÇO OPORTUNIDADE.");
            throw;
        }
    }

    public int VerificaDocServicosPendentes(int CODOPT, int CODSVCTGV)
    {
        var DAL = new CadastroServicoOportunidadeDAL();
        return DAL.VerificaDocServicosPendentes(CODOPT, CODSVCTGV);
    }

    //public List<CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade> ProcessarServicoOportunidade(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objPesquisar)
    //{
    //    try
    //    {
    //        int _codoportunidade = 0;
    //        string _descservico = "";
    //        string _NOMINDSTANGC = "";
    //        string _INDSTANGC = "";
    //        decimal _VALORCOND = 0;
    //        decimal _PERCOND = 0;
    //        int _NUMPTO = 0;
    //        string _DESTIPSVC = "";
    //        int _CODCNDCMC = 0;

    //        string _STATUSENVIO = "";
    //        string _STATUSACEITE = "";
    //        string _CDOANXCTTCLI = "";

    //        string _email = "";

    //        int _codservico = 0;
    //        decimal _valorservico = 0;
    //        decimal _valordescserv = 0;
    //        decimal _perserv = 0;

    //        decimal _valorServDesc = 0;

    //        int _codCondicao = 0;

    //        decimal _vlrPerCond = 0;
    //        decimal _vlrDescCond = 0;
    //        decimal _valorServCond = 0;

    //        decimal _valorFinal = 0;
    //        decimal _valorFinalServico = 0;

    //        Boolean _finalizouCond = false;
    //        bool _tevedescontozerado = false;
    //        decimal _valororiginal = 0;


    //        bool _existedesconto = false;

    //        List<CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade> _listretorn = new List<CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade>();
    //        CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade _itemInsert = new CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade();

    //        var DAL = new CadastroServicoOportunidadeDAL();
    //        List<CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade> _listLocal = DAL.obterprocessamentoOportunidade(objPesquisar);

    //        foreach (CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade _item in _listLocal)
    //        {
    //            if (_item.CODSVCTGV != _codservico)
    //            {

    //                if (_finalizouCond)
    //                {
    //                    //
    //                    if (_tevedescontozerado)
    //                        _valorFinalServico = (_valorServDesc - _valororiginal) - _valorServCond;
    //                    else
    //                        _valorFinalServico = _valorServDesc - _valorServCond;


    //                    _itemInsert.CODOPTVND = _codoportunidade;
    //                    _itemInsert.CODSVCTGV = _codservico;
    //                    _itemInsert.VLRSVC = _valorservico; //.ToString();
    //                    _itemInsert.PERDSC = _perserv;//.ToString("n2");

    //                    _itemInsert.VLRDSC = _valordescserv;//.ToString("n2");

    //                    if (_perserv > 0)
    //                        _itemInsert.VLRTOTDES = ((_valordescserv + _valorServCond) + (_valorservico * (_perserv / 100)));//.ToString("n2");
    //                    else
    //                        _itemInsert.VLRTOTDES = (_valordescserv + _valorServCond);//.ToString("n2");


    //                    _itemInsert.DESSVCTGV = _descservico;
    //                    _itemInsert.NOMINDSTANGC = _NOMINDSTANGC;
    //                    _itemInsert.INDSTANGC = _INDSTANGC;
    //                    _itemInsert.VALORCOND = _VALORCOND;
    //                    _itemInsert.PERCOND = _PERCOND;
    //                    _itemInsert.NUMPTOSVC = _NUMPTO;
    //                    _itemInsert.DESTIPSVC = _DESTIPSVC;
    //                    _itemInsert.CODCNDCMC = _CODCNDCMC;


    //                    if (_valorFinalServico > 0)
    //                        _itemInsert.VLRSVCTOT = Math.Round(_valorFinalServico, 2);//.ToString();
    //                    else
    //                        _itemInsert.VLRSVCTOT = 0;


    //                    if (_existedesconto)
    //                        _itemInsert.EXISTDESC = 1;
    //                    else
    //                        _itemInsert.EXISTDESC = 0;


    //                    _valorServCond = 0;
    //                    _finalizouCond = false;
    //                    _tevedescontozerado = false;
    //                    _valororiginal = 0;

    //                    _codCondicao = 0;

    //                    _STATUSENVIO = "";
    //                    _STATUSACEITE = "";
    //                    _email = "";
    //                    _CDOANXCTTCLI = "";

    //                    _existedesconto = false;

    //                    _listretorn.Add(_itemInsert);
    //                    _itemInsert = null;
    //                    _itemInsert = new CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade();
    //                }

    //                _valorServDesc = 0;
    //                _valorservico = 0;
    //                _valordescserv = 0;
    //                _perserv = 0;

    //                _codservico = _item.CODSVCTGV;
    //                _valorservico = _item.VLRSVC;

    //                _codoportunidade = _item.CODOPTVND;
    //                _descservico = _item.DESSVCTGV;
    //                _NOMINDSTANGC = _item.NOMINDSTANGC;
    //                _INDSTANGC = _item.INDSTANGC;
    //                _VALORCOND = _item.VALORCOND;
    //                _PERCOND = _item.PERCOND;
    //                _NUMPTO = _item.NUMPTOSVC;
    //                _DESTIPSVC = _item.DESTIPSVC;
    //                _CODCNDCMC = _item.CODCNDCMC;

    //                if (_item.VLRDSC != null)
    //                {
    //                    _valordescserv = decimal.Parse(_item.VLRDSC.ToString("n3"));
    //                    if (_valordescserv > 0)
    //                    {
    //                        _valorServDesc = _valorservico - _valordescserv;

    //                        if (_valorServDesc == 0)
    //                            _tevedescontozerado = true;
    //                    }
    //                }

    //                if (_item.PERDSC != null)
    //                {
    //                    _perserv = decimal.Parse(_item.PERDSC.ToString("n3"));
    //                    if (_perserv > 0)
    //                    {
    //                        _valorServDesc = _valorservico - (_valorservico * (_perserv / 100));

    //                        if (_valorServDesc == 0)
    //                            _tevedescontozerado = true;
    //                    }
    //                }

    //                if (_valorServDesc == 0)
    //                {
    //                    _valororiginal = _valorservico;
    //                    _valorServDesc = _valorservico;
    //                }

    //                _valorFinalServico += _valorServDesc;
    //            }

    //            if ((_item.CODCNDCMC != _codCondicao) && (_item.CODSVCTGV == _codservico))
    //            {
    //                _vlrPerCond = 0;
    //                _vlrDescCond = 0;
    //                _finalizouCond = true;

    //                _codCondicao = _item.CODCNDCMC;


    //                if (_item.CODCNDCMC >= 9000)
    //                    _existedesconto = true;

    //                if ((_item.VALORCOND != null) && (_item.VALORCOND > 0))
    //                {
    //                    _vlrDescCond = _item.VALORCOND;
    //                    _valorServCond += decimal.Parse(_vlrDescCond.ToString("n2"));
    //                }

    //                if ((_item.PERCOND != null) && (_item.PERCOND > 0))
    //                {
    //                    _vlrPerCond = _item.PERCOND;
    //                    _valorServCond += decimal.Parse((_valorServDesc * (_vlrPerCond / 100)).ToString("n3"));
    //                }
    //            }
    //            else
    //            {
    //                if (_item.CODCNDCMC == 0)
    //                    _finalizouCond = true;
    //            }

    //        }

    //        if (_finalizouCond)
    //        {
    //            if (_tevedescontozerado)
    //                _valorFinalServico = (_valorServDesc - _valororiginal) - _valorServCond;
    //            else
    //                _valorFinalServico = _valorServDesc - _valorServCond;


    //            _itemInsert.CODOPTVND = _codoportunidade;
    //            _itemInsert.CODSVCTGV = _codservico;
    //            _itemInsert.VLRSVC = _valorservico;//.ToString();

    //            _itemInsert.PERDSC = _perserv;//.ToString("n2");
    //            _itemInsert.VLRDSC = _valordescserv;//.ToString("n2");

    //            _itemInsert.DESSVCTGV = _descservico;
    //            _itemInsert.NOMINDSTANGC = _NOMINDSTANGC;
    //            _itemInsert.INDSTANGC = _INDSTANGC;
    //            _itemInsert.VALORCOND = _VALORCOND;
    //            _itemInsert.PERCOND = _PERCOND;
    //            _itemInsert.NUMPTOSVC = _NUMPTO;
    //            _itemInsert.DESTIPSVC = _DESTIPSVC;
    //            _itemInsert.CODCNDCMC = _CODCNDCMC;
    //            _itemInsert.STATUSENVIO = _STATUSENVIO;
    //            _itemInsert.STATUSACEITE = _STATUSACEITE;
    //            _itemInsert.DESENDETN = _email;
    //            _itemInsert.CDOANXCTTCLI = _CDOANXCTTCLI;


    //            //_itemInsert.VLRSVCTOT = Math.Round(_valorFinalServico, 2);//.ToString();
    //            if (_valorFinalServico > 0)
    //                _itemInsert.VLRSVCTOT = Math.Round(_valorFinalServico, 2);//.ToString();
    //            else
    //                _itemInsert.VLRSVCTOT = 0;



    //            if (_perserv > 0)
    //                _itemInsert.VLRTOTDES = ((_valordescserv + _valorServCond) + (_valorservico * (_perserv / 100)));//.ToString("n2");
    //            else
    //                _itemInsert.VLRTOTDES = (_valordescserv + _valorServCond);//.ToString("n2");


    //            if (_valorFinalServico > 0)
    //                _valorFinal += _valorFinalServico;
    //            else
    //                _valorFinal += 0;


    //            if (_existedesconto)
    //                _itemInsert.EXISTDESC = 1;
    //            else
    //                _itemInsert.EXISTDESC = 0;


    //            _valorServCond = 0;
    //            _finalizouCond = false;

    //            _existedesconto = false;

    //            _listretorn.Add(_itemInsert);
    //            _itemInsert = null;
    //            _itemInsert = new CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade();
    //        }


    //        foreach (var c in _listretorn)
    //            c.VLRSVCTOTGER = Math.Round(_valorFinal, 2);//.ToString();


    //        return _listretorn;

    //    }
    //    catch (Exception ex)
    //    {
    //        Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO SERVIÇO OPORTUNIDADE.");
    //        Utilitario.InsereLog(ex, "obterCadastroServicoOportunidade", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO SERVIÇO OPORTUNIDADE.");
    //        throw;
    //    }
    //}




}