﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

public class CadastroServicoOportunidadeDAL : DAL
{
    public List<CadastroServicoOportunidadeTO.obterCadastroServicoOportunidadeLocal> obterCadastroServicoOportunidade(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objPesquisar)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();

        var _verificaCodServico = false;
        if (objPesquisar.CODSVCTGV > 0)
            _verificaCodServico = true;


        string cmdSql = DALSQL.obterCadastroServicoOportunidade(_verificaCodServico);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (objPesquisar.CODOPTVND == -1)
        {
            objPesquisar.CODOPTVND = 0;
        }
        //
        dbCommand.AddWithValue("CODOPTVND", objPesquisar.CODOPTVND);

        if (_verificaCodServico)
            dbCommand.AddWithValue("CODSVCTGV", objPesquisar.CODSVCTGV);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroServicoOportunidadeTO.obterCadastroServicoOportunidadeLocal>();
    }

    public List<CadastroServicoOportunidadeTO.obterServicosDesejados> obterServicosDesejados(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objPesquisar)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();        

        string cmdSql = DALSQL.obterServicosDesejados();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        // 
        dbCommand.AddWithValue("CODOPTVND", objPesquisar.CODOPTVND);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroServicoOportunidadeTO.obterServicosDesejados>();
    }

    public List<CadastroServicoOportunidadeTO.obterCadastroServicoOportunidadeSelect> obterCadastroServicoOportunidadeSelect(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objPesquisar)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.obterCadastroServicoOportunidadeSelect(objPesquisar.LISTCODSVCTGV);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroServicoOportunidadeTO.obterCadastroServicoOportunidadeSelect>();
    }

    public List<CadastroServicoOportunidadeTO.obterAcompanhamentoConciliacao> obterAcompanhamentoConciliacao(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objPesquisar)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();

        string cmdSql = DALSQL.obterAcompanhamentoConciliacao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        // 
        dbCommand.AddWithValue("CODOPTVND", objPesquisar.CODOPTVND);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroServicoOportunidadeTO.obterAcompanhamentoConciliacao>();
    }

    public List<CadastroServicoOportunidadeTO.Documento> obterDocumento(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objPesquisar)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();
        string cmdSql = string.Empty;
        //var teste = objPesquisar.LSTCODDOCARZ.ToList();
        if (objPesquisar.LSTCODDOCARZ != null && objPesquisar.LISTCODSVCTGV != null)
            cmdSql = DALSQL.obterDocumento(objPesquisar.LSTCODDOCARZ, objPesquisar.LISTCODSVCTGV);
        else
            cmdSql = DALSQL.obterDocumento();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        // 
        if (objPesquisar.LSTCODDOCARZ != null && objPesquisar.LSTCODDOCARZ.Count == 1)
            dbCommand.AddWithValue("CODDOCARZ", objPesquisar.LSTCODDOCARZ.FirstOrDefault());
        else if (objPesquisar.CODDOCARZ > 0)
            dbCommand.AddWithValue("CODDOCARZ", objPesquisar.CODDOCARZ);


        if (objPesquisar.LISTCODSVCTGV != null && objPesquisar.LISTCODSVCTGV.Count == 1)
            dbCommand.AddWithValue("LISTCODSVCTGV", objPesquisar.LISTCODSVCTGV.FirstOrDefault());
        else if (objPesquisar.CODSVCTGV >0)
            dbCommand.AddWithValue("CODSVCTGV", objPesquisar.CODSVCTGV);

        dbCommand.AddWithValue("CODOPTVND", objPesquisar.CODOPTVND);
        //
        dbCommand.TrataDbCommandUniversal(true,true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroServicoOportunidadeTO.Documento>();
    }

    public List<CadastroServicoOportunidadeTO.ObterDocumentoNovo> obterDocumentoNovo(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objPesquisar)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.obterDocumentoNovo();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODOPTVND", objPesquisar.CODOPTVND);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroServicoOportunidadeTO.ObterDocumentoNovo>();
    }

    public CadastroServicoOportunidadeTO.obterServicoPendente obterServicoPendente(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objPesquisar)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.obterServicoPendente();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);       
        //
        dbCommand.AddWithValue("CODOPTVND", objPesquisar.CODOPTVND);
        //
        dbCommand.TrataDbCommandUniversal();
        //    
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroServicoOportunidadeTO.obterServicoPendente>().FirstOrDefault();
    }

    public List<long> obterServicOportunidade(int codOpt)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.obterServicOportunidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODOPTVND", codOpt);
        //
        dbCommand.TrataDbCommandUniversal();
        //    
        List<long> lisRetorno = new List<long>();
        IDataReader data = MRT001.ExecuteReader(dbCommand);
        while (data.Read())
        {
            lisRetorno.Add(Convert.ToInt64(data["CODSVCTGV"]));
        }
        data.Close();
        data.Dispose();

        return lisRetorno ;
    }
    

    public bool inserirCadastroServicoOportunidadeNovo(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objInserir)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();

        string cmdSql = DALSQL.inserirCadastroServicoOportunidadeNovo(objInserir.LISTCODSVCTGV);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("VLRSVC", objInserir.VLRSVC);
        dbCommand.AddWithValue("CODSVCTGV", objInserir.CODSVCTGV);
        dbCommand.AddWithValue("VLRDSC", objInserir.VLRDSC);
        dbCommand.AddWithValue("INDSTANGC", objInserir.INDSTANGC);
        dbCommand.AddWithValue("INDSTADSC", objInserir.INDSTADSC);
        dbCommand.AddWithValue("PERDSC", objInserir.PERDSC);
        dbCommand.AddWithValue("INDSVCVND", objInserir.INDSVCVND);

        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool inserirCadastroServicoOportunidadeEditar(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objInserir)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.inserirCadastroServicoOportunidadeEditar(objInserir.LISTCODSVCTGV);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("VLRSVC", objInserir.VLRSVC);
        dbCommand.AddWithValue("CODOPTVND", objInserir.CODOPTVND);
        dbCommand.AddWithValue("CODSVCTGV", objInserir.CODSVCTGV);
        dbCommand.AddWithValue("VLRDSC", objInserir.VLRDSC);
        dbCommand.AddWithValue("INDSTANGC", objInserir.INDSTANGC);
        dbCommand.AddWithValue("INDSTADSC", objInserir.INDSTADSC);
        dbCommand.AddWithValue("PERDSC", objInserir.PERDSC);
        dbCommand.AddWithValue("INDSVCVND", objInserir.INDSVCVND);        
        dbCommand.AddWithValue("QDEPRDVND", objInserir.QDEPRDVND);

        if (objInserir.CODGRPEMPFAT != 0)
            dbCommand.AddWithValue("CODGRPEMPFAT", objInserir.CODGRPEMPFAT);
        else
            dbCommand.AddWithValue("CODGRPEMPFAT", null);



        //                       
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool alterarCadastroServicoOportunidade(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objAlterar)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.alterarCadastroServicoOportunidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        dbCommand.AddWithValue("CODOPTVND", objAlterar.CODOPTVND);
        dbCommand.AddWithValue("CODSVCTGV", objAlterar.CODSVCTGV);
        dbCommand.AddWithValue("VLRSVC", decimal.Parse(objAlterar.VLRSVC));
        dbCommand.AddWithValue("VLRDSC", decimal.Parse(objAlterar.VLRDSC));
        dbCommand.AddWithValue("INDSTADSC", objAlterar.INDSTADSC);
        dbCommand.AddWithValue("INDSTANGC", objAlterar.INDSTANGC);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }
    public bool alterarDocumentoAutorizado(CadastroServicoOportunidadeApiModel.DocumentoAlteradoApiModel objAlterar)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.alterarDocumentoAutorizado();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        dbCommand.AddWithValue("CDOANXDOCARZASN", objAlterar.CDOANXDOCARZASN);
        dbCommand.AddWithValue("CODOPTVND", objAlterar.CODOPTVND);
        dbCommand.AddWithValue("CODSVCTGV", objAlterar.CODSVCTGV);
        dbCommand.AddWithValue("CODDOCARZ", objAlterar.CODDOCARZ);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public Int32 VerificaDocumentosPendente(CadastroServicoOportunidadeApiModel.DocumentoAlteradoApiModel _item)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.VerificaDocumentosPendente();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        dbCommand.AddWithValue("CODOPTVND", _item.CODOPTVND);
        //
        return Convert.ToInt32(MRT001.ExecuteScalar(dbCommand));
    }

    public Int32 VerificaServicosPendente(CadastroServicoOportunidadeApiModel.DocumentoAlteradoApiModel _item)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.VerificaServicosPendente();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        dbCommand.AddWithValue("CODOPTVND", _item.CODOPTVND);
        //
        return Convert.ToInt32(MRT001.ExecuteScalar(dbCommand));
    }

    public int verificaContratoAssinado(CadastroServicoOportunidadeApiModel.DocumentoAlteradoApiModel _item)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.verificaContratoAssinado();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        dbCommand.AddWithValue("CODOPTVND", _item.CODOPTVND);
        //
        return Convert.ToInt32(MRT001.ExecuteScalar(dbCommand));
    }

    public bool alterarStatusOportunidade(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objAlterar)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.alterarStatusOportunidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (objAlterar.INDSTAOPTVND <= 0)
            objAlterar.INDSTAOPTVND = 3;
        //            
        dbCommand.AddWithValue("INDSTAOPTVND", objAlterar.INDSTAOPTVND);
        dbCommand.AddWithValue("CODOPTVND", objAlterar.CODOPTVND);

        //
        return Convert.ToBoolean(MRT001.ExecuteReader(dbCommand).RecordsAffected == 1);
    }

    public bool alterarStatusServico(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objAlterar)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.alterarStatusServico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (objAlterar.INDSTANGC <= 0)
            objAlterar.INDSTANGC = 2;
        //
        dbCommand.AddWithValue("INDSTANGC", objAlterar.INDSTANGC);
        dbCommand.AddWithValue("CODOPTVND", objAlterar.CODOPTVND);
        dbCommand.AddWithValue("CODSVCTGV", objAlterar.CODSVCTGV);

        //
        dbCommand.TrataDbCommandUniversal(true, true);
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool alterarStatusServicoOportunidade(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objAlterar)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.alterarStatusServicoOportunidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        dbCommand.AddWithValue("CODOPTVND", objAlterar.CODOPTVND);
        dbCommand.AddWithValue("CODSVCTGV", objAlterar.CODSVCTGV);
        dbCommand.AddWithValue("INDSTANGC", objAlterar.INDSTANGC);
        dbCommand.AddWithValue("INDSVCVND", objAlterar.INDSVCVND);

        //
        dbCommand.TrataDbCommandUniversal(true, true);
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool alterarDescontoCadastroServicoOportunidade(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objAlterar)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.alterarDescontoCadastroServicoOportunidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        dbCommand.AddWithValue("CODOPTVND", objAlterar.CODOPTVND);
        dbCommand.AddWithValue("CODSVCTGV", objAlterar.CODSVCTGV);
        dbCommand.AddWithValue("VLRDSC", decimal.Parse(objAlterar.VLRDSC));
        dbCommand.AddWithValue("PERDSC", decimal.Parse(objAlterar.PERDSC));        
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool deletarCadastroServicoOportunidade(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objDeletar)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.deletarCadastroServicoOportunidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        dbCommand.AddWithValue("CODOPTVND", objDeletar.CODOPTVND);
        dbCommand.AddWithValue("CODSVCTGV", objDeletar.CODSVCTGV);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }
    
    public bool alterarEmailServicoOportunidade(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objAlterar, bool _enviouemail)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.alterarEmailServicoOportunidade(_enviouemail);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        dbCommand.AddWithValue("DESENDETN", objAlterar.DESENDETN);        
        dbCommand.AddWithValue("NOMARQORI", objAlterar.NOMARQORI);
        dbCommand.AddWithValue("CDOANXCTTCLI", objAlterar.CONTRATOBANCO);
        dbCommand.AddWithValue("CODOPTVND", objAlterar.CODOPTVND);
        dbCommand.AddWithValue("CODSVCTGV", objAlterar.CODSVCTGV);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public bool alterarpdftesteServicoOportunidade(int CODOPTVND, int CODSVCTGV, string pdfteste)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.alterarpdftesteServicoOportunidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        dbCommand.AddWithValue("CDOANXCTTCLI", pdfteste);
        dbCommand.AddWithValue("CODOPTVND", CODOPTVND);
        dbCommand.AddWithValue("CODSVCTGV", CODSVCTGV);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool alterarStatusServicoOportunidadeSemEnvio(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objAlterar)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.alterarStatusServicoOportunidadeSemEnvio();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        dbCommand.AddWithValue("CODOPTVND", objAlterar.CODOPTVND);
        dbCommand.AddWithValue("CODSVCTGV", objAlterar.CODSVCTGV);
        dbCommand.AddWithValue("INDSTANGC", objAlterar.INDSTANGC);
        dbCommand.AddWithValue("INDSVCVND", objAlterar.INDSVCVND);

        //
        dbCommand.TrataDbCommandUniversal(true, true);
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool alterarDocsStatusServicoOportunidade(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objAlterar)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.alterarDocsStatusServicoOportunidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        dbCommand.AddWithValue("CODOPTVND", objAlterar.CODOPTVND);
        dbCommand.AddWithValue("INDSTANGC", objAlterar.INDSTANGC);
        dbCommand.AddWithValue("INDSVCVND", objAlterar.INDSVCVND);

        //
        dbCommand.TrataDbCommandUniversal(true, true);
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public int VerificaDocServicosPendentes(int CODOPT, int CODSVCTGV)
    {
        var DALSQL = new CadastroServicoOportunidadeDALSQL();
        string cmdSql = DALSQL.VerificaDocServicosPendentes();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODOPTVND", CODOPT);
        dbCommand.AddWithValue("CODSVCTGV", CODSVCTGV);

        //
        dbCommand.TrataDbCommandUniversal(true, false, true, false, true);
        //
        return Convert.ToInt32(MRT001.ExecuteScalar(dbCommand));
    }
}