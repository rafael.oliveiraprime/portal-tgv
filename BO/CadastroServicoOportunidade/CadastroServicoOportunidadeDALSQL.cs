﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class CadastroServicoOportunidadeDALSQL
{
    public string obterCadastroServicoOportunidade(bool _verificaCodServico)
    {
        StringBuilder strBld = new StringBuilder(@"
                      SELECT SV.CODOPTVND, SV.CODSVCTGV, 
                                           SV.VLRSVC,                         
                                           SV.VLRDSC,                                 
                                           SV.PERDSC,                                  
                             CSV.DESSVCTGV,
                             CASE WHEN SV.INDSTANGC = 1 THEN 'EM NEGOCIAÇÃO' 
                                  WHEN SV.INDSTANGC = 2 THEN 'VENDA GANHA' 
                                  WHEN SV.INDSTANGC = 3 THEN 'VENDA PERDIDA' 
                                  WHEN SV.INDSTANGC = 4 THEN 'ABRIR SITE SURVEY' 
                                  WHEN SV.INDSTANGC = 5 THEN 'AGUARDANDO APROVAÇÃO DE DESCONTO' 
                                  WHEN SV.INDSTANGC = 6 THEN 'AGUARDANDO ACEITE DE CONTRATO' 
                                  WHEN SV.INDSTANGC = 7 THEN 'AGUARDANDO ASS. DE DOCUMENTO' END NOMINDSTANGC,
                             SV.INDSTANGC,
                             CNDSV.VLRDSC AS VALORCOND,                                  
                             CNDSV.PERDSC AS PERCOND,                                  
                             0 VLRSVCTOT,
                             0 VLRSVCTOTGER,
                             0 NUMPTOSVC,       
                             '' DESTIPSVC,
                             CNDSV.CODCNDCMC,
                             '0' VLRTOTDES,
                             CASE SV.DATENVDOC WHEN SV.DATENVDOC THEN TO_CHAR(SV.DATENVDOC,'DD/MM/YYYY') ELSE 'NÃO ENVIADO' END STATUSENVIO,
                             CASE SV.DATACEDOC WHEN SV.DATACEDOC THEN TO_CHAR(SV.DATACEDOC,'DD/MM/YYYY') ELSE '' END STATUSACEITE,
                             SV.DESENDETN,
                             CASE WHEN CADDESC.DESCNDCMC IS NULL THEN CNDSV.DESDSC ELSE CADDESC.DESCNDCMC END AS DESCNDCMC,
                             0 EXISTDESC,
                             NVL(SV.CDOANXCTTCLI, ' ') AS CDOANXCTTCLI,
                             NOMARQORI,
                             INDSVCVND,
                             NVL(QDEPRDVND, 1) AS QDEPRDVND
                             ,SV.CODGRPEMPFAT 
                             ,CADGRUP.NOMGRPEMPCLI
                        FROM MRT.RLCOPTVNDSVCTGV SV
                       INNER JOIN MRT.CADSVCTGV CSV ON(SV.CODSVCTGV = CSV.CODSVCTGV)
                        LEFT JOIN MRT.RLCOPTCNDCMCTGV CNDSV ON(SV.CODSVCTGV = CNDSV.CODSVCTGV AND SV.CODOPTVND =  CNDSV.CODOPTVND)
                        LEFT JOIN MRT.CADCNDCMCTGV CADDESC ON(CNDSV.CODCNDCMC = CADDESC.CODCNDCMC)
                        LEFT JOIN MRT.CADGRPEMPTGV CADGRUP ON(SV.CODGRPEMPFAT = CADGRUP.CODGRPEMPCLI)
                      WHERE SV.CODOPTVND = :CODOPTVND
                ");

        if (_verificaCodServico)
            strBld.AppendFormat(" AND SV.CODSVCTGV = :CODSVCTGV ").AppendLine();


        return strBld.ToString();
    }

    public string obterAcompanhamentoConciliacao()
    {
        return @"
               SELECT
                       --RLCSVCDOC.CODOPTVND
                      DOC.CODDOCARZ CodDocumento
                      ,DOC.DESDOCARZ Documento
                      ,SVC.CODSVCTGV CodServico
                      ,SVC.DESSVCTGV Servico
                      ,RLCSVCDOC.DATENVDOC Enviado
                      ,RLCSVCDOC.DATACEDOC Recebido
                      ,RLCSVCDOC.INDSTAAPV Status
                FROM MRT.RLCSVCDOCARZTGV RLCSVCDOC
                LEFT JOIN MRT.CADDOCARZCLITGV DOC 
                    ON DOC.CODDOCARZ = RLCSVCDOC.CODDOCARZ
                LEFT JOIN MRT.CADSVCTGV SVC  
                    ON SVC.CODSVCTGV = RLCSVCDOC.CODSVCTGV
                WHERE 1=1
                AND DOC.CODDOCARZ IS NOT NULL
                AND RLCSVCDOC.CODOPTVND = :CODOPTVND
                --AND RLCSVCOPT.INDSTANGC NOT IN (3)
                ORDER BY SVC.DESSVCTGV, DOC.DESDOCARZ
                "; 

    }

    public string obterDocumento(List<long> LSTCODDOCARZ, List<long> LSTCODSVCTGV)
    {
        StringBuilder strBld;
        strBld = new StringBuilder(@"
               SELECT
                    SVC.DESSVCTGV NomeServico,
                    DOC.CDOANXDOCARZ DocumentoEnviado,
                    DOC.CDOANXDOCARZASN DocumentoAssinado
                FROM MRT.RLCSVCDOCARZTGV DOC
                    INNER JOIN MRT.CADSVCTGV SVC 
                ON SVC.CODSVCTGV = DOC.CODSVCTGV
                WHERE 1=1
                    AND DOC.CODOPTVND = :CODOPTVND 
                    AND DOC.CODDOCARZ IN (#LSTCODDOCARZ) 
                    AND DOC.CODSVCTGV IN (#LSTCODSVCTGV) 
                ".Replace("#LSTCODDOCARZ", ToList(LSTCODDOCARZ))
                 .Replace("#LSTCODSVCTGV", ToList(LSTCODSVCTGV))
                 );
        return strBld.ToString();
    }

    public string obterDocumento()
    {
        return @"
               SELECT
                    SVC.DESSVCTGV NomeServico,
                    DOC.CDOANXDOCARZ DocumentoEnviado,
                    DOC.CDOANXDOCARZASN DocumentoAssinado
                FROM MRT.RLCSVCDOCARZTGV DOC
                    INNER JOIN MRT.CADSVCTGV SVC 
                        ON SVC.CODSVCTGV = DOC.CODSVCTGV
                WHERE 1=1
                    AND DOC.CODDOCARZ = :CODDOCARZ
                    AND DOC.CODOPTVND = :CODOPTVND
                    
                ";
    }

    public string obterDocumentoNovo()
    {
        return @"
                 SELECT SVCDOC.CODDOCARZ Codigo,
                        DOC.DESDOCARZ Nome
                    FROM MRT.CADSVCTGV SVC
                  INNER JOIN MRT.RLCSVCDOCTGV SVCDOC 
                    ON SVC.CODSVCTGV = SVCDOC.CODSVCTGV
                  INNER JOIN MRT.CADDOCARZCLITGV DOC 
                    ON DOC.CODDOCARZ = SVCDOC.CODDOCARZ 
                    AND DOC.INDENV = 1 
                    AND DOC.DATDST IS NULL
                  WHERE SVCDOC.CODSVCTGV IN (SELECT SV.CODSVCTGV 
                                                FROM MRT.RLCOPTVNDSVCTGV SV
                                             WHERE SV.CODOPTVND = :CODOPTVND)
                      AND SVCDOC.CODDOCARZ NOT IN 
                         (
                          SELECT SVCDOC.CODDOCARZ
                              FROM MRT.RLCOPTVNDSVCTGV SV
                                  LEFT JOIN MRT.RLCSVCDOCARZTGV SVCDOC 
                                    ON SVCDOC.CODOPTVND = SV.CODOPTVND AND SVCDOC.CODSVCTGV = SV.CODSVCTGV
                                WHERE SV.CODOPTVND = :CODOPTVND
                                AND SVCDOC.CODOPTVND IS NOT NULL
                           )
                ";
        
    }

    public string VerificaDocumentosPendente()
    {
        return @"
               SELECT COUNT(*) QTDE 
                    FROM MRT.RLCSVCDOCARZTGV RLCSVCDOC 
                        WHERE RLCSVCDOC.CODOPTVND = :CODOPTVND 
                        AND RLCSVCDOC.INDSTAAPV = 1
                ";
    }

    public string VerificaServicosPendente()
    {
        return @"
               SELECT COUNT(*) QTDE 
                    FROM MRT.RLCOPTVNDSVCTGV RLCSVCOPT 
                        WHERE RLCSVCOPT.CODOPTVND = :CODOPTVND 
                        AND RLCSVCOPT.INDSTANGC NOT IN (2,3)
                ";
    }

    public string verificaContratoAssinado()
    {
        return @"
               SELECT INDSTANGC
                    FROM MRT.RLCOPTVNDSVCTGV 
                        WHERE CODOPTVND = :CODOPTVND
                ";//retorna uma lista
    }

    public string obterServicosDesejados()
    {
        return @"
                SELECT SV.CODSVCTGV || ' - ' || CSV.DESSVCTGV AS CODNOMSVC
                FROM MRT.RLCOPTVNDSVCTGV SV
                INNER JOIN MRT.CADSVCTGV CSV ON(SV.CODSVCTGV = CSV.CODSVCTGV)
                WHERE SV.CODOPTVND = :CODOPTVND
                ";
    }

    public string obterCadastroServicoOportunidadeSelect(List<long> LISTCODSVCTGV)
    {
        return @"
                SELECT CODSVCTGV,
                TRIM(DESSVCTGV) AS DESSVCTGV,
                VLRSVC,
                'EM NEGOCIAÇÃO' AS NOMINDSTANGC
                FROM MRT.CADSVCTGV
                WHERE CODSVCTGV IN (#LISTCODSVCTGV)
                ".Replace("#LISTCODSVCTGV", ToList(LISTCODSVCTGV));
    }

   
    public string obterServicOportunidade()
    {
        return @"  SELECT distinct(SVCDOC.CODSVCTGV) as CODSVCTGV
                                  FROM MRT.RLCSVCDOCARZTGV SVCDOC
                                WHERE SVCDOC.CODOPTVND = :CODOPTVND;
                ";

    }
    public string obterServicoPendente()
    {
        return @" SELECT DISTINCT (SELECT COUNT(NG.INDSTANGC) FROM  MRT.RLCOPTVNDSVCTGV NG WHERE NG.CODOPTVND = SV.CODOPTVND AND NG.INDSTANGC IN (1, 4, 5, 6)) AS  NEGOCIACAO,
                                  (SELECT COUNT(NG.INDSTANGC) FROM  MRT.RLCOPTVNDSVCTGV NG WHERE NG.CODOPTVND = SV.CODOPTVND AND NG.INDSTANGC IN (2)) AS  VENDAGANHA,
                                  (SELECT COUNT(NG.INDSTANGC) FROM  MRT.RLCOPTVNDSVCTGV NG WHERE NG.CODOPTVND = SV.CODOPTVND AND NG.INDSTANGC IN (3)) AS  VENDAPERDIDA
                    FROM MRT.RLCOPTVNDSVCTGV SV
                   WHERE SV.CODOPTVND = :CODOPTVND ";

    }

    public string inserirCadastroServicoOportunidadeNovo(List<long> LISTCODSVCTGV)
    {

        StringBuilder strBld = new StringBuilder(@" INSERT INTO MRT.RLCOPTVNDSVCTGV (VLRSVC, CODOPTVND, CODSVCTGV, VLRDSC, INDSTANGC, INDSTADSC, PERDSC, INDSVCVND) 
                                                                             VALUES (:VLRSVC, (SELECT COALESCE(MAX(CODOPTVND),0) FROM MRT.CADOPTVNDCLITGV),
                                                                                     :CODSVCTGV, :VLRDSC, :INDSTANGC, :INDSTADSC, :PERDSC, :INDSVCVND)");
        //StringBuilder strBld = new StringBuilder(@"
        //     INSERT INTO MRT.RLCOPTVNDSVCTGV
        //     SELECT VLRSVC, 
        //     (SELECT COALESCE(MAX(CODOPTVND),0) FROM MRT.CADOPTVNDCLITGV), 
        //     CODSVCTGV, 0, 1, 1, 0, null, null, null, null
        //     FROM MRT.CADSVCTGV 
        //");
        //strBld.AppendFormat("WHERE CODSVCTGV IN ({0})", ToList(LISTCODSVCTGV)).AppendLine();

        return strBld.ToString();
    }

    public string inserirCadastroServicoOportunidadeEditar(List<long> LISTCODSVCTGV)
    {
        StringBuilder strBld = new StringBuilder(@" INSERT INTO MRT.RLCOPTVNDSVCTGV (VLRSVC, CODOPTVND, CODSVCTGV, VLRDSC, INDSTANGC, INDSTADSC, PERDSC, INDSVCVND, QDEPRDVND, CODGRPEMPFAT)
                                                                             VALUES (:VLRSVC, :CODOPTVND, :CODSVCTGV, :VLRDSC, :INDSTANGC, :INDSTADSC, :PERDSC, :INDSVCVND, :QDEPRDVND, :CODGRPEMPFAT)");

        return strBld.ToString();
    }

    public string alterarCadastroServicoOportunidade()
    {
        return @"                 
                 UPDATE MRT.RLCOPTVNDSVCTGV SET
                 VLRSVC = :VLRSVC,
                 VLRDSC = :VLRDSC,
                 INDSTANGC = :INDSTANGC,
                 INDSTADSC = :INDSTADSC                
                 WHERE CODOPTVND = :CODOPTVND
                 AND CODSVCTGV = :CODSVCTGV
                ";
    }

    public string alterarDocumentoAutorizado()
    {
        return @"                 
                 UPDATE MRT.RLCSVCDOCARZTGV SET
                 CDOANXDOCARZASN = :CDOANXDOCARZASN,
                 DATACEDOC = sysdate,
                 INDSTAAPV = 2                
                 WHERE CODOPTVND = :CODOPTVND
                 AND CODSVCTGV = :CODSVCTGV
                 AND CODDOCARZ = :CODDOCARZ
                ";
    }

    public string alterarStatusServicoOportunidade()
    {
        return @"                 
                 UPDATE MRT.RLCOPTVNDSVCTGV SET INDSTANGC = :INDSTANGC,
                                                INDSVCVND = :INDSVCVND 
                  WHERE CODOPTVND = :CODOPTVND
                   AND CODSVCTGV = :CODSVCTGV
                ";
    }

    public string alterarStatusOportunidade()
    {
        return @"                 
                 UPDATE MRT.CADOPTVNDCLITGV 
                        SET INDSTAOPTVND = :INDSTAOPTVND,
                            DATFIMNGCOPT = SYSDATE 
                  WHERE 1=1
                    AND CODOPTVND = :CODOPTVND
                ";
    }

    public string alterarStatusServico()
    {
        return @"                 
                 UPDATE MRT.RLCOPTVNDSVCTGV 
                        SET INDSTANGC = :INDSTANGC
                  WHERE CODOPTVND = :CODOPTVND
                ";
    }

    public string alterarDescontoCadastroServicoOportunidade()
    {
        return @"                 
                 UPDATE MRT.RLCOPTVNDSVCTGV SET                 
                 VLRDSC = TO_NUMBER(:VLRDSC), 
                 PERDSC = TO_NUMBER(:PERDSC) 
                 WHERE CODOPTVND = :CODOPTVND
                 AND CODSVCTGV = :CODSVCTGV
                ";
    }

    public string deletarCadastroServicoOportunidade()
    {
        return @"                                
                 DELETE FROM MRT.RLCOPTVNDSVCTGV 
                 WHERE CODOPTVND = :CODOPTVND
                 AND CODSVCTGV = :CODSVCTGV
                ";
    }

    public string ToList(List<long> LISTCODSVCTGV)
    {
        string retr = string.Empty;
        bool firstTime = true;

        foreach (long Item in LISTCODSVCTGV)
        {
            if (firstTime)
            {
                firstTime = false;

                retr = (Item.ToString());
            }
            else
            {
                retr = (string.Format("{0}, {1}", retr, Item));
            }

        }

        return retr.Trim();
    }


    public string alterarEmailServicoOportunidade(bool _enviouemail)
    {
        StringBuilder strBld;

        if (_enviouemail)
        {
            strBld = new StringBuilder(@"
                                                    UPDATE MRT.RLCOPTVNDSVCTGV SET
                                                                                    DESENDETN = :DESENDETN,
                                                                                    DATENVDOC = SYSDATE,
                                                                                    NOMARQORI = :NOMARQORI,
                                                                                    CDOANXCTTCLI = :CDOANXCTTCLI
                                                                  WHERE CODOPTVND = :CODOPTVND
                                                                    AND CODSVCTGV = :CODSVCTGV
                                        ");
        }
        else
        {
            strBld = new StringBuilder(@"
                                                    UPDATE MRT.RLCOPTVNDSVCTGV SET
                                                                                    DESENDETN = :DESENDETN,
                                                                                    NOMARQORI = :NOMARQORI,
                                                                                    CDOANXCTTCLI = :CDOANXCTTCLI
                                                                  WHERE CODOPTVND = :CODOPTVND
                                                                    AND CODSVCTGV = :CODSVCTGV
                                        ");

        }

        return strBld.ToString();

    }


    public string alterarpdftesteServicoOportunidade()
    {
        StringBuilder strBld;

            strBld = new StringBuilder(@"
                                                    UPDATE MRT.RLCOPTVNDSVCTGV SET
                                                                                    CDOANXCTTCLI = :CDOANXCTTCLI
                                                                  WHERE CODOPTVND = :CODOPTVND
                                                                    AND CODSVCTGV = :CODSVCTGV
                                        ");

        return strBld.ToString();

    }

    public string alterarStatusServicoOportunidadeSemEnvio()
    {
        return @"                 
                 UPDATE MRT.RLCOPTVNDSVCTGV SET INDSTANGC = :INDSTANGC,
                                                INDSVCVND = :INDSVCVND,
                                                DATACEDOC = SYSDATE
                  WHERE CODOPTVND = :CODOPTVND
                   AND CODSVCTGV = :CODSVCTGV
                ";

    }

    public string alterarDocsStatusServicoOportunidade()
    {
        return @"                 
                 UPDATE MRT.RLCOPTVNDSVCTGV SET INDSTANGC = :INDSTANGC,
                                                INDSVCVND = :INDSVCVND 
                  WHERE CODOPTVND = :CODOPTVND
                ";
    }

    public string VerificaDocServicosPendentes()
    {
        StringBuilder strBld = new StringBuilder(@" SELECT COUNT(CODOPTVND) 
                                                      FROM MRT.RLCSVCDOCARZTGV
                                                     WHERE INDSTAAPV = 1
                                                       AND CODOPTVND = :CODOPTVND
                                                       AND CODSVCTGV = :CODSVCTGV
                                                  ");


        return strBld.ToString();
    }
}