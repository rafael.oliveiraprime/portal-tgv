﻿using System;
using System.Collections.Generic;

namespace CadastroServicoOportunidadeTO
{
    public class obterCadastroServicoOportunidade
    {
        public int CODOPTVND { get; set; }

        public int CODSVCTGV { get; set; }

        public string DESSVCTGV { get; set; }

        public decimal VLRSVC { get; set; }

        public decimal VLRDSC { get; set; }   
        
        public decimal VLRSVCTOT { get; set; }

        public decimal VLRSVCTOTGER { get; set; }

        public string INDSTANGC { get; set; }

        public string NOMINDSTANGC { get; set; }

        public string DESTIPSVC { get; set; }

        public int NUMPTOSVC { get; set; }

        public decimal PERDSC { get; set; }

        public decimal VALORCOND { get; set; }

        public decimal PERCOND { get; set; }

        public int CODCNDCMC { get; set; }

        public decimal VLRTOTDES { get; set; }

        public string STATUSENVIO { get; set; }

        public string STATUSACEITE { get; set; }

        public string DESENDETN { get; set; }

        public string DESCNDCMC { get; set; }

        public int EXISTDESC { get; set; }

        public string CDOANXCTTCLI { get; set; }

        public string NOMARQORI { get; set; }

        public int INDSVCVND { get; set; }

        public int QDEPRDVND { get; set; }

        public int CODGRPEMPFAT { get; set; }

        public string NOMGRPEMPCLI { get; set; }

        public List<CadastroDocConciliacaoTO.obterDocRelacaoServico> _listDocs { get; set; }
    }


    public class obterAcompanhamentoConciliacao
    {
        public int CodDocumento { get; set; }
        public string Documento { get; set; }
        public int CodServico { get; set; }
        public string Servico { get; set; }
        public string Enviado { get; set; }
        public string Recebido { get; set; }
        public int Status { get; set; }
    }

    public class Documento
    {
        public string NomeServico { get; set; }
        public string DocumentoEnviado { get; set; }
        public string DocumentoAssinado { get; set; }
    }

    public class ObterDocumentoNovo
    {
        public int Codigo { get; set; }
        public string Nome { get; set; }
    }

    public class obterDocumento
    {
        public string Documento { get; set; }
        public string Motivo { get; set; }
    }

    public class obterServicosDesejados
    {
        public string CODNOMSVC { get; set; }
    }

    public class obterCadastroServicoOportunidadeSelect
    {
        public int CODSVCTGV { get; set; }

        public string DESSVCTGV { get; set; }

        public decimal VLRSVC { get; set; }

        public string NOMINDSTANGC { get; set; }

    }

    public class obterServicoPendente
    {      
        public int NEGOCIACAO { get; set; }

        public int VENDAGANHA { get; set; }

        public int VENDAPERDIDA { get; set; }

    }


    public class obterCadastroServicoOportunidadeLocal
    {
        public int CODOPTVND { get; set; }

        public int CODSVCTGV { get; set; }

        public string DESSVCTGV { get; set; }

        public decimal VLRSVC { get; set; }

        public decimal VLRDSC { get; set; }

        public decimal VLRSVCTOT { get; set; }

        public decimal VLRSVCTOTGER { get; set; }

        public string INDSTANGC { get; set; }

        public string NOMINDSTANGC { get; set; }

        public string DESTIPSVC { get; set; }

        public int NUMPTOSVC { get; set; }

        public decimal PERDSC { get; set; }

        public decimal VALORCOND { get; set; }

        public decimal PERCOND { get; set; }

        public int CODCNDCMC { get; set; }

        public decimal VLRTOTDES { get; set; }

        public string STATUSENVIO { get; set; }

        public string STATUSACEITE { get; set; }

        public string DESENDETN { get; set; }

        public string DESCNDCMC { get; set; }

        public int EXISTDESC { get; set; }

        public string CDOANXCTTCLI { get; set; }

        public string NOMARQORI { get; set; }

        public int INDSVCVND { get; set; }

        public int QDEPRDVND { get; set; }

        public int CODGRPEMPFAT { get; set; }

        public string NOMGRPEMPCLI { get; set; }
    }

}