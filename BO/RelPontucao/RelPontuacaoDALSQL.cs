﻿using System.Collections;
using System.Linq;
using System.Text;
using System.Web;


public class RelPontuacaoDALSQL
{
    public string obterPontuacao(string LISTCODSVCTGV, string LISTSTATUS, string LISTCONSUL)
    {
        string listaServico = "";
        string ListaStatus = "";
        string ListaConsul = "";

        if (LISTCODSVCTGV != "0")
            listaServico = "AND S.CODSVCTGV IN( #LISTCODSVCTGV )".Replace("#LISTCODSVCTGV", LISTCODSVCTGV);


        if (LISTSTATUS != "0")
            ListaStatus = "AND S.INDSTANGC IN( #STATUS )".Replace("#STATUS", LISTSTATUS);


        if (LISTCONSUL != "0")
            ListaConsul = "AND C.CODCNIVNDTGV IN( #CONSUL )".Replace("#CONSUL", LISTCONSUL);



        return @"    SELECT S.CODOPTVND
                            ,C.CODCLI
                            ,CLI.NOMCLI
                            ,CLI.NUMCGCCLI
                            ,C.CODCNIVNDTGV
                            ,S.CODSVCTGV              
                            ,SVC.DESSVCTGV
                            ,SUBSTR(FNC.NOMFNC,1,25) AS NOMCON
                            ,CASE WHEN S.INDSTANGC = 1 THEN 'EM NEGOCIAÇÃO' 
                                WHEN S.INDSTANGC = 2 THEN 'VENDA GANHA' 
                                WHEN S.INDSTANGC = 3 THEN 'VENDA PERDIDA' 
                                WHEN S.INDSTANGC = 4 THEN 'ABRIR SITE SURVEY' 
                                WHEN S.INDSTANGC = 5 THEN 'AGUARDANDO APROVAÇÃO DE DESCONTO' 
                                WHEN S.INDSTANGC = 6 THEN 'AGUARDANDO ACEITE DE CONTRATO'                               
                                WHEN S.INDSTANGC = 7 THEN 'AGUARDANDO ASS. DE DOCUMENTO' END NOMINDSTANGC                              
                            ,S.DATACEDOC
                            ,S.VLRSVC * NVL(S.QDEPRDVND,1) AS VLRSVC
                            ,SUM(NVL(D.VLRDSC,0)) AS VLRDSC
                            ,SUM(NVL(D.PERDSC,0)) AS PERDSC
                            ,ROUND(( (S.VLRSVC * NVL(S.QDEPRDVND,1)) - SUM(NVL(D.VLRDSC,0))) * DECODE(SUM(NVL(D.PERDSC,0)),0,1,1-(SUM(D.PERDSC)/100)),2) VLRTOTSVC
                            ,CASE WHEN S.CODSVCTGV = 15 THEN (ROUND(( (S.VLRSVC * NVL(S.QDEPRDVND,1)) - SUM(NVL(D.VLRDSC,0))) * DECODE(SUM(NVL(D.PERDSC,0)),0,1,1-(SUM(D.PERDSC)/100)),2) * 0.63) 
                                  ELSE ROUND(( (S.VLRSVC * NVL(S.QDEPRDVND,1)) - SUM(NVL(D.VLRDSC,0))) * DECODE(SUM(NVL(D.PERDSC,0)),0,1,1-(SUM(D.PERDSC)/100)),2) END AS VLRTOTREC            
                            ,TRIM(CPI.NOMPESCTO) AS NOMPESCTOPES
                            ,CASE WHEN C.CODCNLORICLI=1 THEN 'SITE (PORTAL)'
                                WHEN C.CODCNLORICLI=2 THEN 'TELEFONE'
                                WHEN C.CODCNLORICLI=3 THEN 'E-MAIL'
                                WHEN C.CODCNLORICLI=4 THEN 'WHATSAPP'
                                WHEN C.CODCNLORICLI=5 THEN 'PARCERIA' 
                                WHEN C.CODCNLORICLI=6 THEN 'CAMPANHA'
                                WHEN C.CODCNLORICLI=7 THEN 'OUTRO' 
                                WHEN C.CODCNLORICLI=8 THEN 'PROSPECT' 
                                WHEN C.CODCNLORICLI=9 THEN 'PRÓPRIA' 
                                WHEN C.CODCNLORICLI=10 THEN 'GERENTE TRIBANCO' 
                                WHEN C.CODCNLORICLI=11 THEN 'GERENTE MARTINS'
                                WHEN C.CODCNLORICLI=12 THEN 'RCA'
                                WHEN C.CODCNLORICLI=13 THEN 'EX-SMART'
                                WHEN C.CODCNLORICLI=14 THEN 'TRIBANCO - GERENCIADOR' 
                                WHEN C.CODCNLORICLI=15 THEN 'MIGRAÇÃO SERVIÇO' 
                                WHEN C.CODCNLORICLI=16 THEN 'TGV - GERENCIADOR' 
                                END NOMCNLORICLI
                                ,TO_CHAR(TRUNC(C.DATFIMNGCOPT), 'DD/MM/YYYY') AS DATFIMNGCOPT
                                ,TO_CHAR(TRUNC(CC.DATATVSVC), 'DD/MM/YYYY') AS DATATVSVC
                                ,TO_CHAR(TRUNC(CC.DATCNC), 'DD/MM/YYYY') AS DATCNC          
                                ,NVL(TO_CHAR(TRUNC(C.DATFIMNGCOPT - C.DATCAD)), 'EM ABERTO') AS SLA
                                ,CASE WHEN CC.INDSTAATV = 1 THEN 'EM IMPLANTAÇÃO'
                                    WHEN CC.INDSTAATV = 2 THEN 'ATIVO'
                                    WHEN CC.INDSTAATV = 3 THEN 'SUSPENSO'
                                    WHEN CC.INDSTAATV = 4 THEN 'CANCELADO'  END STATUSSERVICO                       
                       FROM MRT.CADOPTVNDCLITGV C
                      INNER JOIN MRT.T0100043 CLI ON CLI.CODCLI = C.CODCLI                          
                      INNER JOIN MRT.RLCOPTVNDSVCTGV S ON C.CODOPTVND = S.CODOPTVND-- AND T.CODSVCTGV = S.CODSVCTGV
                       LEFT JOIN MRT.CADCLISVCTGV CC ON C.CODCLI = CC.CODCLI AND S.CODSVCTGV = CC.CODSVCTGV
                       LEFT JOIN MRT.CADSVCTGV SVC ON SVC.CODSVCTGV = S.CODSVCTGV                         
                      INNER JOIN MRT.T0100361 FNC ON FNC.CODFNC = C.CODCNIVNDTGV
                       LEFT JOIN MRT.RLCOPTCNDCMCTGV D ON  S.CODOPTVND = D.CODOPTVND AND S.CODSVCTGV = D.CODSVCTGV 
                       LEFT JOIN MRT.CADCNDCMCTGV M ON M.CODCNDCMC = D.CODCNDCMC                          
                       LEFT JOIN MRT.CADDDOPESINDTGV CPI ON C.NUMSEQPESIND = CPI.NUMSEQPESIND
                      WHERE 1 = 1
                        AND C.CODCLI = :CODCLI 
                        AND CLI.NUMCGCCLI = :NUMCGCCLI 
                        #SERVICOS
                        #STATUS
                        #CONSUL
                        AND TRUNC(S.DATACEDOC) BETWEEN TRUNC(TO_DATE(:DATAINICIO,'DD/MM/YYYY')) AND TRUNC(TO_DATE(:DATAFIM,'DD/MM/YYYY'))
                      GROUP BY S.CODOPTVND
                               ,C.CODCLI
                               ,CLI.NOMCLI                               
                               ,CLI.NUMCGCCLI
                               ,C.CODCNIVNDTGV
                               ,S.CODSVCTGV
                               ,SVC.DESSVCTGV
                               ,S.DATACEDOC                               
                               ,S.INDSTANGC
                               ,S.VLRSVC
                               ,S.QDEPRDVND
                               ,FNC.NOMFNC
                               ,CPI.NOMPESCTO
                               ,C.CODCNLORICLI
                               ,C.DATFIMNGCOPT
                               ,CC.DATATVSVC
                               ,CC.DATCNC
                               ,C.DATCAD
                               ,CC.INDSTAATV
                     ORDER BY c.codcli, CLI.NOMCLI
                ".Replace("#SERVICOS", listaServico).Replace("#STATUS", ListaStatus).Replace("#CONSUL", ListaConsul);
    }

    public string obterTotaisReceita(string LISTCODSVCTGV, string LISTSTATUS, string LISTCONSUL)
    {

        string listaServico = "";
        string ListaStatus = "";
        string ListaConsul = "";
        string ListaConsulSomatoria = "";

        if (LISTCODSVCTGV != "0")
            listaServico = "AND S.CODSVCTGV IN( #LISTCODSVCTGV )".Replace("#LISTCODSVCTGV", LISTCODSVCTGV);

        if (LISTSTATUS != "0")
            ListaStatus = "AND S.INDSTANGC IN( #STATUS )".Replace("#STATUS", LISTSTATUS);

        if (LISTCONSUL != "0")
        {
            ListaConsul = "AND C.CODCNIVNDTGV IN( #CONSUL )".Replace("#CONSUL", LISTCONSUL);
            ListaConsulSomatoria = "AND S.CODCNIVNDTGV IN( #CONSUL )".Replace("#CONSUL", LISTCONSUL);
        }

        return @"     WITH RECEITA AS (
                                        SELECT S.CODOPTVND
                                                ,C.CODCLI
                                                ,CLI.NOMCLI
                                                ,CLI.NUMCGCCLI
                                                ,C.CODCNIVNDTGV
                                                ,S.CODSVCTGV              
                                                ,SVC.DESSVCTGV
                                                ,SUBSTR(FNC.NOMFNC,1,25) AS NOMCON
                                                ,CASE WHEN S.INDSTANGC = 1 THEN 'EM NEGOCIAÇÃO' 
                                                    WHEN S.INDSTANGC = 2 THEN 'VENDA GANHA' 
                                                    WHEN S.INDSTANGC = 3 THEN 'VENDA PERDIDA' 
                                                    WHEN S.INDSTANGC = 4 THEN 'ABRIR SITE SURVEY' 
                                                    WHEN S.INDSTANGC = 5 THEN 'AGUARDANDO APROVAÇÃO DE DESCONTO' 
                                                    WHEN S.INDSTANGC = 6 THEN 'AGUARDANDO ACEITE DE CONTRATO'                               
                                                    WHEN S.INDSTANGC = 7 THEN 'AGUARDANDO ASS. DE DOCUMENTO' END NOMINDSTANGC                              
                                                ,S.DATACEDOC
                                                ,S.VLRSVC * NVL(S.QDEPRDVND,1) AS VLRSVC
                                                ,SUM(NVL(D.VLRDSC,0)) AS VLRDSC
                                                ,SUM(NVL(D.PERDSC,0)) AS PERDSC
                                                ,ROUND(( (S.VLRSVC * NVL(S.QDEPRDVND,1)) - SUM(NVL(D.VLRDSC,0))) * DECODE(SUM(NVL(D.PERDSC,0)),0,1,1-(SUM(D.PERDSC)/100)),2) VLRTOTSVC
                                                ,CASE WHEN S.CODSVCTGV = 15 THEN (ROUND(( (S.VLRSVC * NVL(S.QDEPRDVND,1)) - SUM(NVL(D.VLRDSC,0))) * DECODE(SUM(NVL(D.PERDSC,0)),0,1,1-(SUM(D.PERDSC)/100)),2) * 0.63) 
                                                      ELSE ROUND(( (S.VLRSVC * NVL(S.QDEPRDVND,1)) - SUM(NVL(D.VLRDSC,0))) * DECODE(SUM(NVL(D.PERDSC,0)),0,1,1-(SUM(D.PERDSC)/100)),2) END AS VLRTOTREC            
                                                ,TRIM(CPI.NOMPESCTO) AS NOMPESCTOPES
                                                ,CASE WHEN C.CODCNLORICLI=1 THEN 'SITE (PORTAL)'
                                                    WHEN C.CODCNLORICLI=2 THEN 'TELEFONE'
                                                    WHEN C.CODCNLORICLI=3 THEN 'E-MAIL'
                                                    WHEN C.CODCNLORICLI=4 THEN 'WHATSAPP'
                                                    WHEN C.CODCNLORICLI=5 THEN 'PARCERIA' 
                                                    WHEN C.CODCNLORICLI=6 THEN 'CAMPANHA'
                                                    WHEN C.CODCNLORICLI=7 THEN 'OUTRO' 
                                                    WHEN C.CODCNLORICLI=8 THEN 'PROSPECT' 
                                                    WHEN C.CODCNLORICLI=9 THEN 'PRÓPRIA' 
                                                    WHEN C.CODCNLORICLI=10 THEN 'GERENTE TRIBANCO' 
                                                    WHEN C.CODCNLORICLI=11 THEN 'GERENTE MARTINS'
                                                    WHEN C.CODCNLORICLI=12 THEN 'RCA'
                                                    WHEN C.CODCNLORICLI=13 THEN 'EX-SMART'
                                                    WHEN C.CODCNLORICLI=14 THEN 'TRIBANCO - GERENCIADOR' 
                                                    WHEN C.CODCNLORICLI=15 THEN 'MIGRAÇÃO SERVIÇO' 
                                                    WHEN C.CODCNLORICLI=16 THEN 'TGV - GERENCIADOR' 
                                                    END NOMCNLORICLI
                                                    ,TO_CHAR(TRUNC(C.DATFIMNGCOPT), 'DD/MM/YYYY') AS DATFIMNGCOPT
                                                    ,TO_CHAR(TRUNC(CC.DATATVSVC), 'DD/MM/YYYY') AS DATATVSVC
                                                    ,TO_CHAR(TRUNC(CC.DATCNC), 'DD/MM/YYYY') AS DATCNC          
                                                    ,NVL(TO_CHAR(TRUNC(C.DATFIMNGCOPT - C.DATCAD)), 'EM ABERTO') AS SLA
                                                    ,S.INDSTANGC
                                           FROM MRT.CADOPTVNDCLITGV C
                                          INNER JOIN MRT.T0100043 CLI ON CLI.CODCLI = C.CODCLI                          
                                          INNER JOIN MRT.RLCOPTVNDSVCTGV S ON C.CODOPTVND = S.CODOPTVND-- AND T.CODSVCTGV = S.CODSVCTGV
                                           LEFT JOIN MRT.CADCLISVCTGV CC ON C.CODCLI = CC.CODCLI AND S.CODSVCTGV = CC.CODSVCTGV
                                           LEFT JOIN MRT.CADSVCTGV SVC ON SVC.CODSVCTGV = S.CODSVCTGV                         
                                          INNER JOIN MRT.T0100361 FNC ON FNC.CODFNC = C.CODCNIVNDTGV
                                           LEFT JOIN MRT.RLCOPTCNDCMCTGV D ON  S.CODOPTVND = D.CODOPTVND AND S.CODSVCTGV = D.CODSVCTGV 
                                           LEFT JOIN MRT.CADCNDCMCTGV M ON M.CODCNDCMC = D.CODCNDCMC                          
                                           LEFT JOIN MRT.CADDDOPESINDTGV CPI ON C.NUMSEQPESIND = CPI.NUMSEQPESIND
                                          WHERE 1 = 1
                                            AND C.CODCLI = :CODCLI 
                                            AND CLI.NUMCGCCLI = :NUMCGCCLI 
                                            #SERVICOS  
                                            #STATUS
                                            #CONSUL
                                            AND TRUNC(S.DATACEDOC) BETWEEN TRUNC(TO_DATE(:DATAINICIO,'DD/MM/YYYY')) AND TRUNC(TO_DATE(:DATAFIM,'DD/MM/YYYY'))
                                          GROUP BY S.CODOPTVND
                                                   ,C.CODCLI
                                                   ,CLI.NOMCLI                               
                                                   ,CLI.NUMCGCCLI
                                                   ,C.CODCNIVNDTGV
                                                   ,S.CODSVCTGV
                                                   ,SVC.DESSVCTGV
                                                   ,S.DATACEDOC                               
                                                   ,S.INDSTANGC
                                                   ,S.VLRSVC
                                                   ,S.QDEPRDVND
                                                   ,FNC.NOMFNC
                                                   ,CPI.NOMPESCTO
                                                   ,C.CODCNLORICLI
                                                   ,C.DATFIMNGCOPT
                                                   ,CC.DATATVSVC
                                                   ,CC.DATCNC
                                                   ,C.DATCAD
                                          ORDER BY c.codcli, CLI.NOMCLI
                                    )
                                      SELECT DESSVCTGV
                                             ,SUM(VLRTOTREC) AS VLRTOTSVC 
                                             ,(SELECT SUM(VLRTOTREC) FROM RECEITA S WHERE 1 = 1  #STATUS  #LCONSULSOMA) AS TOTALRECEITA
                                        FROM RECEITA S               
                                       WHERE 1 = 1
                                             #STATUS
                                             #LCONSULSOMA
                                       GROUP BY DESSVCTGV
                                       ORDER BY DESSVCTGV
                 ".Replace("#SERVICOS", listaServico).Replace("#STATUS", ListaStatus).Replace("#CONSUL", ListaConsul).Replace("#LCONSULSOMA", ListaConsulSomatoria);

    }




    public string ObterUnica()
    {
        return @"    SELECT '61701363000149' AS GF_CNPJ
                            ,'973473' AS GF_CODIGO_MARTINS
                            ,SUBSTR(FAT.ANOMESREF,5,2) || '/' || SUBSTR(FAT.ANOMESREF,0,4) AS MES_ANO
                            ,CAD.NUMCGCCLI AS CNPJ
                            ,SVC.DESSVCTGV AS SOLUCAO
                            ,TRIM(TO_CHAR(FAT.VLRTOTSVC,'L99G999G999D99','NLS_NUMERIC_CHARACTERS = '',.'' NLS_CURRENCY = ''R$ '' ')) AS TOTAL
                            ,CLI.DATATVSVC AS DATA_ATIVACAO
                            ,CLI.DATCNC AS DATA_CANCELAMENTO
                       FROM MRT.CADCLISVCTGV cli
                      INNER JOIN MRT.T0100043 cad ON cli.codcli = cad.codcli
                      INNER JOIN MRT.RLCCLIGRPEMPTGV gru ON cli.codcli = gru.codcli 
                      INNER JOIN MRT.MOVMESFATCLISVCTGV fat ON cli.codcli = fat.codcli and cli.CODSVCTGV = fat.CODSVCTGV and fat.INDSTAATV in( 2, 4)  and fat.CODCLIGRPFAT is null 
                      INNER JOIN MRT.CADSVCTGV SVC ON cli.CODSVCTGV = svc.CODSVCTGV
                      WHERE FAT.DATATVSVC IS NOT NULL
                        AND gru.CODGRPEMPCLI = 20
                        AND FAT.ANOMESREF = :MESANO
                ";
    }


}