﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;



public class RelPontuacaoDAL : DAL
{
    public List<RelPontuacaoTO.obterPontuacao> obterPontuacao(RelPontuacaoApiModel.obterPontuacaoApiModel objPesquisar)
    {
        var DALSQL = new RelPontuacaoDALSQL();
        string cmdSql = DALSQL.obterPontuacao(objPesquisar.LISTCODSVCTGV, objPesquisar.LISTSTATUS, objPesquisar.LISTCONSUL);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //    
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("NUMCGCCLI", Regex.Replace(objPesquisar.NUMCGCCLI, "[/.-]", String.Empty));
        dbCommand.AddWithValue("DATAINICIO", objPesquisar.DATAINICIO);
        dbCommand.AddWithValue("DATAFIM", objPesquisar.DATAFIM);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<RelPontuacaoTO.obterPontuacao>();
    }

    public List<RelPontuacaoTO.obterTotaisReceita> obterTotaisReceita(RelPontuacaoApiModel.obterPontuacaoApiModel objPesquisar)
    {
        var DALSQL = new RelPontuacaoDALSQL();
        string cmdSql = DALSQL.obterTotaisReceita(objPesquisar.LISTCODSVCTGV, objPesquisar.LISTSTATUS, objPesquisar.LISTCONSUL);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //    
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);

        if (objPesquisar.NUMCGCCLI != null)
            dbCommand.AddWithValue("NUMCGCCLI", Regex.Replace(objPesquisar.NUMCGCCLI, "[/.-]", String.Empty));
        else
            dbCommand.AddWithValue("NUMCGCCLI", "");

        dbCommand.AddWithValue("DATAINICIO", objPesquisar.DATAINICIO);
        dbCommand.AddWithValue("DATAFIM", objPesquisar.DATAFIM);

        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<RelPontuacaoTO.obterTotaisReceita>();
    }

    public List<RelPontuacaoTO.ObterUnica> ObterUnica(RelPontuacaoApiModel.ObterUnicaApiModel objPesquisar)
    {
        var DALSQL = new RelPontuacaoDALSQL();
        string cmdSql = DALSQL.ObterUnica();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //    
        dbCommand.AddWithValue("MESANO", objPesquisar.MESANO);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<RelPontuacaoTO.ObterUnica>();
    }


}