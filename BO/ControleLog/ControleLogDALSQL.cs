﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class ControleLogDALSQL
{   
    public string inserirLog()
    {
        return @"
                  INSERT INTO MRT.MOVLOGSISTGVSMA (NUMERR, CODSISINF, DATCAD, CODFNC, DESMDO, DESPMTSIS, DESCAMERR, DESERRRDC, DESERRDDO)
                                           VALUES ((SELECT COALESCE(MAX(NUMERR),0)+1 FROM MRT.MOVLOGSISTGVSMA),
                                                    :CODSISINF, SYSDATE, :CODFNC, :DESMDO, :DESPMTSIS, :DESCAMERR, :DESERRRDC, :DESERRDDO)
                ";
    }
   
}