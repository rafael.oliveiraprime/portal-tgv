﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class ControleLogDAL : DAL
{
    public bool inserirLog(ControleLogModel.ControleLog objInserir)
    {
        var DALSQL = new ControleLogDALSQL();
        string cmdSql = DALSQL.inserirLog();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objInserir.CODSISINF = 1;
        objInserir.CODFNC = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //
        dbCommand.AddWithValue("CODSISINF", objInserir.CODSISINF);
        dbCommand.AddWithValue("CODFNC", objInserir.CODFNC);
        dbCommand.AddWithValue("DESMDO", objInserir.DESMDO);
        dbCommand.AddWithValue("DESPMTSIS", objInserir.DESPMTSIS);
        dbCommand.AddWithValue("DESCAMERR", objInserir.DESCAMERR);
        dbCommand.AddWithValue("DESERRRDC", objInserir.DESERRRDC);
        dbCommand.AddWithValue("DESERRDDO", objInserir.DESERRDDO);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

}