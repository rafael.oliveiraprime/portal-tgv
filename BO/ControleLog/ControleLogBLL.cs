﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class ControleLogBLL
{
    public bool inserirLog(ControleLogModel.ControleLog objInserir)
    {
        try
        {
            var DAL = new ControleLogDAL();
            return DAL.inserirLog(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO INSERIR LOG DE ERRO NO BANCO.");
            throw;
        }
    }
}