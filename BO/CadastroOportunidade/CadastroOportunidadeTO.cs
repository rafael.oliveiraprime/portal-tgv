﻿using System;
using System.Collections.Generic;

namespace CadastroOportunidadeTO
{
    public class obterCadastroOportunidade
    {
        public int CODOPTVND { get; set; }

        public int CODCLI { get; set; }

        public string NOMCLI { get; set; }

        public int CODCNIVNDTGV { get; set; }

        public Int64 NUMSEQPESIND { get; set; }

        public int CODCNLORICLI { get; set; }

        public string NOMCNLORICLI { get; set; }

        public string NUMTLFCTO { get; set; }

        public string NUMTLFCEL { get; set; }

        public string NOMPESCTO { get; set; }

        public string STATUS { get; set; }

        public string DATFNCCAD { get; set; }

        public string PRIMERADESOBS { get; set; }
    }

    public class obterCadastroOportunidadeCliente
    {
        public int CODCLI { get; set; }

        public string NOMCLI { get; set; }
    }

    public class obterClientes
    {
        public int CODCLI { get; set; }   
        
        public string NUMCGCCLI { get; set; }

        public int CODGRPEMPCLI { get; set; }

        public int CODGRPEMPCLIPRINC { get; set; }

        public string NOMGRPEMPCLI { get; set; }

        public string NOMGRPEMPCLIPRINC { get; set; }

    }

    public class obterClientesCnpj
    {
        public int CODCLI { get; set; }

        public string NOMCLI { get; set; }
    }

    public class obterConsultorDistribuicao 
    {
        public int CODFNC { get; set; }

        public int ULTIMOCODIGO { get; set; }        
    }

    public class obterMaximoOportunidade
    {
        public int CODOPTVND { get; set; }
    }

    public class servicosContratos
    {
        public int CODOPTVND { get; set; }
        public int CODSVCTGV { get; set; }
        public string CDOANXCTTCLI { get; set; }
        public string NOMARQORI { get; set; }

    }

    public class ServicosDocumento
    {
        public Int64 CODDOCARZ { get; set; }
        public string CDOANXDOCARZ { get; set; }
        public int INDENV { get; set; }
    }


    public class ServicosDocumentoNovo
    {
        public Int64 CODDOCARZ { get; set; }
        public string CDOANXDOCARZ { get; set; }
        public int INDENV { get; set; }
        public string DESSVCTGV { get; set; }
        public int CODSVCTGV { get; set; }
    }

    public class obterConsultaContrato
    {
        public int CODOPTVND { get; set; }

        public int CODSVCTGV { get; set; }

        public string DESSVCTGV { get; set; }

        public string NOMARQORI { get; set; }

        public string DATENVDOC { get; set; }

        public string DATACEDOC { get; set; }

        public string CDOANXCTTCLI { get; set; }

        public int INDSTAOPTVND { get; set; }
    }

}