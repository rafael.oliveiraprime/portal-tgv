﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Arquitetura.Classes;
using System.IO;

using Microsoft.Office.Core;
using System.Text;
using System.Configuration;
using System.Globalization;

using System.Text.RegularExpressions;
using DocumentFormat.OpenXml.Packaging;

using DocumentFormat.OpenXml;
using System.Drawing;
using OpenXmlPowerTools;
using System.Xml.Linq;
using DocumentFormat.OpenXml.Wordprocessing;

using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Web.Script.Serialization;

using System.Transactions;



public class CadastroOportunidadeBLL
{
    public List<CadastroOportunidadeTO.obterCadastroOportunidade> obterCadastroOportunidade(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objPesquisar)
    {

        try
        {
            var DAL = new CadastroOportunidadeDAL();
            return DAL.obterCadastroOportunidade(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "inserirCadastroMotivoContato", "CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel", "CadastroOportunidadeBLL", "ERRO AO FAZER A INSERCAO DO MOTIVO CONTATO.");
            throw;
        }
    }

    public CadastroOportunidadeTO.obterCadastroOportunidadeCliente obterCadastroOportunidadeCliente(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroOportunidadeDAL();
            return DAL.obterCadastroOportunidadeCliente(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CLIENTE DO CADASTRO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "obterCadastroOportunidadeCliente", "CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel", "CadastroOportunidadeBLL", "ERRO AO FAZER A CONSULTA DO CLIENTE DO CADASTRO OPORTUNIDADE.");
            throw;
        }
    }

    public bool inserirCadastroOportunidade(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir)
    {
        var bllConsultor = new CadastroOportunidadeBLL();
        var dalgrupoEco = new CadastroGrupoEconomicoDAL();
        var dalconta = new GestaoContasDAL();

        CadastroGrupoEconomicoTO.BuscaGrupoEconomico _grupoeco = new CadastroGrupoEconomicoTO.BuscaGrupoEconomico();
        CadastroGrupoEconomicoTO.ExisteGrupoCli _existe = new CadastroGrupoEconomicoTO.ExisteGrupoCli();
        CadastroGrupoEconomicoTO.VerificaSeEDebito _VerificaDebito = new CadastroGrupoEconomicoTO.VerificaSeEDebito();
        CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel objInserirGrupo = new CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel();

        try
        {

            int[] _A = { 9, 10, 11, 14, 16, 17 };

            if (_A.Any(x => objInserir.CODCNLORICLI.Equals(x)))
            {
                var apiConsultor = new CadastroOportunidadeApiModel.CadastroOportunidadeApiModel() { };
                var consultorOportunidade = bllConsultor.obterConsultorDistribuicao(apiConsultor);

                objInserir.CODCNIVNDTGV = consultorOportunidade.CODFNC;
            }
            else
                objInserir.CODCNIVNDTGV = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;



            var DAL = new CadastroOportunidadeDAL();
            return DAL.inserirCadastroOportunidade(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO CADASTRO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "inserirCadastroOportunidade", "CadastroOportunidadeApiModel.CadastroOportunidadeApiModel", "CadastroOportunidadeBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO OPORTUNIDADE.");
            throw (ex);
        }
    }

    public bool inserirContatoPadraoOportunidade(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir)
    {
        try
        {
            var DAL = new CadastroOportunidadeDAL();
            return DAL.inserirContatoPadraoOportunidade(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO CADASTRO CONTATO PADRAO.");
            Utilitario.InsereLog(ex, "inserirContatoPadraoOportunidade", "CadastroOportunidadeApiModel.inserirContatoPadraoOportunidade", "CadastroOportunidadeBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO CONTATO PADRAO.");
            throw (ex);
        }
    }

    public bool inserirCadastroServicoOportunidadeNovo(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir, int grupoServico)
    {
        CadastroServicoTO.obterCadastroServico _servicovalores = new CadastroServicoTO.obterCadastroServico();
        CadastroServicoApiModel.CadastroServicoApiModel _servicooportunit = new CadastroServicoApiModel.CadastroServicoApiModel();

        var DAL = new CadastroOportunidadeDAL();
        var dalservico = new CadastroServicoDAL();

        try
        {
            foreach (long _item in objInserir.LISTCODSVCTGV)
            {
                _servicooportunit.CODSVCTGV = int.Parse(_item.ToString());
                _servicovalores = dalservico.obterCadastroServico(_servicooportunit).FirstOrDefault();

                objInserir.VLRSVC = _servicovalores.VLRSVC.ToString();
                objInserir.CODOPTVND = objInserir.CODOPTVND;
                objInserir.CODSVCTGV = int.Parse(_item.ToString());
                objInserir.VLRDSC = "0";
                objInserir.INDSTANGC = 1;
                objInserir.INDSTADSC = 1;

                if (grupoServico != 0)
                    objInserir.CODGRPEMPFAT = grupoServico;



                DAL.inserirCadastroServicoOportunidadeNovo(objInserir);
                _servicovalores = new CadastroServicoTO.obterCadastroServico();
            }
            return true;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO CADASTRO SERVICO OPORTUNIDADE NOVO.");
            Utilitario.InsereLog(ex, "inserirCadastroServicoOportunidadeNovo", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO SERVICO OPORTUNIDADE NOVO.");
            throw;
        }
    }

    public string alterarCadastroOportunidade(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        List<OportunidadeContatoTO.obterServico> _list_servicoscliente;
        List<OportunidadeContatoTO.obterServico> _list_servicosVendaGanha = new List<OportunidadeContatoTO.obterServico>();

        List<OportunidadeContatoTO.obterDadosBasicos> _list_dadoclient;
        var dalservicooportunit = new CadastroServicoOportunidadeDAL();
        CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel _itemservico = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();

        OportunidadeContatoTO.obterDadosBasicos _item;
        CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade _servicovalores;

        var DALOPTCONT = new OportunidadeContatoDAL();

        CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objAlterarEmail = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
        CryptUtil _encript = new CryptUtil();

        string _templateemail = ConfigurationManager.AppSettings["caminhoContratoTemplateEmail"];
        string _templateemail2 = ConfigurationManager.AppSettings["caminhoContratoTemplateEmail2"];
        string[] _codigosemail2 = ConfigurationManager.AppSettings["codigosEmail2"].Split(';');

        string _templateemail3 = ConfigurationManager.AppSettings["caminhoContratoTemplateEmail3"];
        string _templateemail4 = ConfigurationManager.AppSettings["caminhoContratoTemplateEmail4"];


        string _templateemailSohAnexos = ConfigurationManager.AppSettings["caminhoTemplateEmailDocumento"];


        string Message = "";
        bool _emailenviado = false;
        string _sitecontrato = ConfigurationManager.AppSettings["stieContato"];

        string _dadossitecontrato = "";
        string _linksitecontrato = "";

        string _noarquivopdf = "";

        string _servicosemarquivo = "";

        bool _tevevendaganha = false;
        bool _tevecociliacaoenviado = false;


        Exception erro;

        List<CadastroOportunidadeTO.ServicosDocumento> ServicoDocs = new List<CadastroOportunidadeTO.ServicosDocumento>();

        string _AssuntoEmail = "";

        try
        {
            using (TransactionScope Scope = new TransactionScope(TransactionScopeOption.Required))
            {
                var excessaoEnvioContratoGruopFat = objAlterar.LISTEXGRUPFAT;


                var marcados = objAlterar.LISTMARCADOS;
                foreach (int _marc in marcados)
                {
                    _itemservico.CODSVCTGV = _marc;
                    _itemservico.CODOPTVND = objAlterar.CODOPTVND;
                    _itemservico.INDSVCVND = 1; //novo status vendido 
                    _itemservico.INDSTANGC = 6;

                    if (excessaoEnvioContratoGruopFat != null)
                    {
                        if (excessaoEnvioContratoGruopFat.Exists(x => x == _marc))
                        {
                            var DalDocs = new CadastroOportunidadeDAL();
                            ServicoDocs = DalDocs.BuscarDocumentosServicos(_itemservico.CODSVCTGV.Value);
                            if (ServicoDocs != null)
                            {
                                if (ServicoDocs.Count > 0)
                                    _itemservico.INDSTANGC = 7;
                                else { 
                                    _tevevendaganha = true;
                                    _itemservico.INDSTANGC = 2;
                                }

                            }
                            else
                            {
                                _tevevendaganha = true;
                                _itemservico.INDSTANGC = 2;
                            }

                            dalservicooportunit.alterarStatusServicoOportunidadeSemEnvio(_itemservico);
                        }
                        else
                            dalservicooportunit.alterarStatusServicoOportunidade(_itemservico);

                    }
                    else
                        dalservicooportunit.alterarStatusServicoOportunidade(_itemservico);


                    _itemservico = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
                }
                _itemservico = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();



                _list_servicoscliente = DALOPTCONT.obterServicos(objAlterar.CODOPTVND);
                foreach (OportunidadeContatoTO.obterServico item in _list_servicoscliente)
                {
                    if (marcados.Exists(x => x == item.CODSVCTGV))
                    {
                        if (item.INDSTANGC == 6)
                        {
                            //if (!VerificaExisteTemplate(item.CODSVCTGV))
                            //{
                            //    _servicosemarquivo += item.DESSVCTGV.ToString() + ", \n";
                            //}
                            if (item.TEMPLATESERVICO == null)
                                _servicosemarquivo += item.DESSVCTGV.ToString() + ", \n";
                            else if (item.TEMPLATESERVICO.Trim() == "")
                                _servicosemarquivo += item.DESSVCTGV.ToString() + ", \n";
                            else
                                _list_servicosVendaGanha.Add(item);
                        }
                        else if (item.INDSTANGC == 7)
                        {
                            _list_servicosVendaGanha.Add(item);
                        }

                    }
                    else
                    {
                        _itemservico.CODOPTVND = objAlterar.CODOPTVND;
                        _itemservico.CODSVCTGV = item.CODSVCTGV;
                        _itemservico.INDSTANGC = 3;
                        _itemservico.INDSVCVND = 2; //novo status vendido 

                        dalservicooportunit.alterarStatusServicoOportunidade(_itemservico);
                        _itemservico = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
                    }
                }
                _itemservico = null;


                if (_servicosemarquivo.Trim() != "")
                {
                    return "1; codigos=" + _servicosemarquivo.ToString();
                }

                if (_list_servicosVendaGanha.Count > 0)
                {

                    if ((objAlterar.EMAILESCOLHIDO != null) && (objAlterar.EMAILESCOLHIDO.Trim() != ""))
                    {
                        _list_dadoclient = DALOPTCONT.obterDadoBasicos(objAlterar.CODCLI);

                        if ((_list_dadoclient != null) && (_list_dadoclient.Count > 0))
                        {
                            _item = _list_dadoclient.FirstOrDefault();

                            foreach (OportunidadeContatoTO.obterServico item in _list_servicosVendaGanha)
                            {
                                objAlterarEmail.CODOPTVND = item.CODOPTVND;
                                objAlterarEmail.CODSVCTGV = item.CODSVCTGV;
                                _servicovalores = RetornarVlorServico(objAlterarEmail);

                                _dadossitecontrato = string.Format("cliente={0}&servico={1}&oprtunidade={2}&token={3}", objAlterar.CODCLI, item.CODSVCTGV, item.CODOPTVND, "kajsroijarj7432847kjfs");
                                _linksitecontrato = string.Format("{0}?{1}", _sitecontrato, _encript.ActionEncrypt(_dadossitecontrato));

                                List<AnexoEmailTO.AnexoEmail> _listAnexos = new List<AnexoEmailTO.AnexoEmail>();

                                if (objAlterar.LISTDOCS != null)
                                {
                                    if (objAlterar.LISTDOCS.Count() > 0)
                                        _listAnexos = PreencherPorReplaceDocs(item.CODOPTVND, item.CODSVCTGV, objAlterar.LISTDOCS, _item, objAlterar.EMAILESCOLHIDO);

                                }

                                _AssuntoEmail = "O seu contrato já está disponivel";
                                if (item.INDSTANGC != 7)
                                {
                                    if (_codigosemail2.Contains(item.CODSVCTGV.ToString()))
                                    {
                                        if (_listAnexos.Count == 0)
                                            Message = GetTemplateHtml(_templateemail2);
                                        else
                                            Message = GetTemplateHtml(_templateemail4);

                                    }
                                    else
                                    {
                                        if (_listAnexos.Count == 0)
                                            Message = GetTemplateHtml(_templateemail);
                                        else
                                            Message = GetTemplateHtml(_templateemail3);

                                    }
                                }
                                else
                                {
                                    _AssuntoEmail = "Documento Gerenciador disponível";
                                    Message = GetTemplateHtml(_templateemailSohAnexos);
                                }


                                Message = Message.Replace("###Cliente", string.Format("{0} - {1} - {2} ", _item.CODCLI, _item.NOMCLI.Trim(), _item.NUMCGCCLI));


                                Message = Message.Replace("###linkContrato", _linksitecontrato);
                                Message = Message.Replace("###linkCDesc", "CONTRATO - " + string.Format("{0}", _servicovalores.DESSVCTGV));


                                Message = Message.Replace("###nomServico", string.Format("{0}", _servicovalores.DESSVCTGV));
                                Message = Message.Replace("###valorservico", string.Format("{0}", _servicovalores.VLRSVCTOT.ToString("n2")));
                                _noarquivopdf = PreencherPorReplace(item.CODOPTVND, item.CODSVCTGV, _item, item.TEMPLATESERVICO);




                                if (_noarquivopdf != "")
                                {
                                    if (Arquitetura.Classes.Util.EnviaEmailMartins(objAlterar.EMAILESCOLHIDO.Split(';')[0].Trim(), null, _AssuntoEmail, Message, _listAnexos, Arquitetura.Classes.Util.TipoEmail.HTML))
                                    {
                                        _emailenviado = true;
                                        objAlterarEmail.DESENDETN = objAlterar.EMAILESCOLHIDO.Split(';')[0].Trim();
                                        objAlterarEmail.NOMARQORI = _noarquivopdf.Split(';')[0];
                                        objAlterarEmail.CONTRATOBANCO = _noarquivopdf.Split(';')[1];

                                        if (_listAnexos.Count <= 0)
                                            dalservicooportunit.alterarEmailServicoOportunidade(objAlterarEmail, _emailenviado);
                                        else
                                        {
                                            objAlterarEmail.INDSTADSC = 2;
                                            _tevecociliacaoenviado = true;
                                            dalservicooportunit.alterarEmailServicoOportunidade(objAlterarEmail, _emailenviado);
                                        }

                                    }
                                    else
                                    {
                                        Scope.Dispose();
                                        _emailenviado = false;
                                        erro = new Exception("EMAIL NÃO FOI ENVIADO");
                                        Utilitario.InsereLog(null, "alterarCadastroOportunidade", "int codopt, int codservico, OportunidadeContatoTO.obterDadosBasicos item", "CadastroOportunidadeBLL", "EMAIL NÃO FOI ENVIADO");
                                        throw erro;
                                    }
                                }
                                else
                                {
                                    Scope.Dispose();
                                    erro = new Exception("NÃO EXISTE O AQRUIVO PARA PREENCHER");
                                    Utilitario.InsereLog(null, "alterarCadastroOportunidade", "int codopt, int codservico, OportunidadeContatoTO.obterDadosBasicos item", "CadastroOportunidadeBLL", "NÃO EXISTE O AQRUIVO PARA PREENCHER");
                                    throw erro;
                                }
                            }
                        }
                        else
                        {
                            Scope.Dispose();
                            erro = new Exception("NÃO FOI ENCONTRADO OS DADOS DO CLINETE");
                            Utilitario.InsereLog(null, "alterarCadastroOportunidade", "int codopt, int codservico, OportunidadeContatoTO.obterDadosBasicos item", "CadastroOportunidadeBLL", "NÃO FOI ENCONTRADO OS DADOS DO CLINETE");
                            throw erro;
                        }
                    }
                    else
                    {
                        Scope.Dispose();
                        erro = new Exception("NÃO EXISTE EMAIL ESCOLHIDO");
                        Utilitario.InsereLog(null, "alterarCadastroOportunidade", "int codopt, int codservico, OportunidadeContatoTO.obterDadosBasicos item", "CadastroOportunidadeBLL", "NÃO EXISTE EMAIL ESCOLHIDO");
                        throw erro;
                    }

                    if (_tevecociliacaoenviado)
                        objAlterar.INDSTAOPTVND = 8;
                    else
                    objAlterar.INDSTAOPTVND = 2;

                }
                else
                {
                    if (_tevevendaganha)
                        objAlterar.INDSTAOPTVND = 2;
                    else
                        objAlterar.INDSTAOPTVND = 4;
                }

                var DAL = new CadastroOportunidadeDAL();
                if (DAL.alterarCadastroOportunidade(objAlterar))
                {
                    Scope.Complete();
                    return "0";
                }
                else
                {
                    Scope.Dispose();
                    erro = new Exception("NÃO AO ALTERAR A OPORTUNIDADE");
                    Utilitario.InsereLog(null, "alterarCadastroOportunidade", "int codopt, int codservico, OportunidadeContatoTO.obterDadosBasicos item", "CadastroOportunidadeBLL", "NÃO AO ALTERAR A OPORTUNIDADE");
                    throw erro;
                }
            }
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO CADASTRO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "alterarCadastroOportunidade", "CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel", "CadastroOportunidadeBLL", "ERRO AO FAZER A ALTERACAO DO CADASTRO OPORTUNIDADE.");
            throw;
        }
    }

    public CadastroOportunidadeTO.obterClientes obterClientes(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroOportunidadeDAL();
            return DAL.obterClientes(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CLIENTE DO CADASTRO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "obterClientes", "CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel", "CadastroOportunidadeBLL", "ERRO AO FAZER A CONSULTA DO CLIENTE DO CADASTRO OPORTUNIDADE.");
            throw;
        }
    }

    public CadastroOportunidadeTO.obterClientesCnpj obterClientesCnpj(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroOportunidadeDAL();
            return DAL.obterClientesCnpj(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CLIENTE DO CADASTRO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "obterClientesCnpj", "CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel", "CadastroOportunidadeBLL", "ERRO AO FAZER A CONSULTA DO CLIENTE DO CADASTRO OPORTUNIDADE.");
            throw ex;
        }
    }

    public CadastroOportunidadeTO.obterConsultorDistribuicao obterConsultorDistribuicao(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroOportunidadeDAL();
            return DAL.obterConsultorDistribuicao(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CONSULTOR.");
            Utilitario.InsereLog(ex, "obterConsultorDistribuicao", "CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel", "CadastroOportunidadeBLL", "ERRO AO FAZER A CONSULTA DO CONSULTOR.");
            throw;
        }
    }

    public bool alterarConsultorOportunidadeNovo(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        try
        {
            var bllConsultor = new CadastroOportunidadeBLL();
            var apiConsultor = new CadastroOportunidadeApiModel.CadastroOportunidadeApiModel() { };
            var consultorOportunidade = bllConsultor.obterConsultorDistribuicao(apiConsultor);

            objAlterar.CODCNIVNDTGV = consultorOportunidade.CODFNC;

            var DAL = new CadastroOportunidadeDAL();
            return DAL.alterarConsultorOportunidadeNovo(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO CONSULTOR DA OPORTUNIDADE NOVO.");
            Utilitario.InsereLog(ex, "alterarConsultorOportunidadeNovo", "CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel", "CadastroOportunidadeBLL", "ERRO AO FAZER A ALTERACAO DO CONSULTOR DA OPORTUNIDADE NOVO.");
            throw;
        }
    }

    //public bool alterarConsultorOportunidadeEditar(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    //{
    //    try
    //    {
    //        var bllConsultor = new CadastroOportunidadeBLL();
    //        var apiConsultor = new CadastroOportunidadeApiModel.CadastroOportunidadeApiModel() { };
    //        var consultorOportunidade = bllConsultor.obterConsultorDistribuicao(apiConsultor);

    //        if (consultorOportunidade.CODCNIVNDTGV > 0)
    //        {
    //            objAlterar.CODCNIVNDTGV = consultorOportunidade.CODCNIVNDTGV;
    //        }
    //        else
    //        {
    //            var bllCadastroConsultor = new CadastroOportunidadeBLL();
    //            var apiCadastroConsultor = new CadastroOportunidadeApiModel.CadastroOportunidadeApiModel() { };
    //            var CadastroConsultor = bllCadastroConsultor.obterConsultorDistribuicao(apiCadastroConsultor);

    //            objAlterar.CODCNIVNDTGV = CadastroConsultor.CODCNIVNDTGV;

    //            objAlterar.CODCNIVNDTGV = 3;

    //        }

    //        var DAL = new CadastroOportunidadeDAL();
    //        return DAL.alterarConsultorOportunidadeEditar(objAlterar);
    //    }
    //    catch (Exception ex)
    //    {
    //        Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO CONSULTOR DA OPORTUNIDADE EDITAR.");
    //        throw;
    //    }
    //}

    public string PreencherPorReplace(int codopt, int codservico, OportunidadeContatoTO.obterDadosBasicos item, string TEMPLATESERVICO)
    {

        var _caminhoServico = ConfigurationManager.AppSettings["caminhoTemplateContratoVenda"];
        var Caminhocontrato = System.Web.HttpContext.Current.Server.MapPath("~\\UploadedFiles");  //ConfigurationManager.AppSettings["caminhoContratoClienteAceite"];
        var caminhouploadbackup = ConfigurationManager.AppSettings["caminhoContratoClienteAceiteBackup"];

        var _nomeArquivo = "";
        var _nomeArquivoPdf = "";

        _nomeArquivo = TEMPLATESERVICO.Split(';')[0].Trim();
        string base64 = TEMPLATESERVICO.Split(';')[1].Trim();

        FileInfo fileexitente = new FileInfo(@Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + item.CODCLI.ToString().PadLeft(3, '0') + _nomeArquivo);
        if (!fileexitente.Exists)
        {
            Byte[] bytes = Convert.FromBase64String(base64);
            File.WriteAllBytes(@Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + item.CODCLI.ToString().PadLeft(3, '0') + "_" + _nomeArquivo, bytes);
        }

        //_nomeArquivo = BuscarTemplateServico(codservico);

        _nomeArquivoPdf = _nomeArquivo.Substring(0, _nomeArquivo.Length - 5) + ".pdf";

        Exception erro;

        var _horaminutsegundodiamesano = "";

        FileInfo file = new FileInfo(@Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + item.CODCLI.ToString().PadLeft(3, '0') + "_" + _nomeArquivo);
        try
        {
            if (file.Exists)
            {
                //file.CopyTo(@Caminhocontrato + "\\" + item.CODCLI.ToString().PadLeft(3, '0') + "__" + _nomeArquivo, true);

                //using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(@Caminhocontrato + "\\" + item.CODCLI.ToString().PadLeft(3, '0') + "__" + _nomeArquivo, true))
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(@Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + item.CODCLI.ToString().PadLeft(3, '0') + "_" + _nomeArquivo, true))
                {
                    string docText = null;
                    using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                    {
                        docText = sr.ReadToEnd();
                    }

                    Regex regexText = new Regex("@RAZAOSOCIAL");
                    if (item.NOMCLI != null)
                        docText = regexText.Replace(docText, item.NOMCLI.Replace("&", " ").Replace("-", " "));
                    else
                        docText = regexText.Replace(docText, "");


                    Regex regexTextRua = new Regex("@RUA");
                    if (item.ENDERECO != null)
                        docText = regexTextRua.Replace(docText, item.ENDERECO);
                    else
                        docText = regexTextRua.Replace(docText, "");


                    Regex regexTextCidade = new Regex("@CIDADE");
                    if (item.NOMCID != null)
                        docText = regexTextCidade.Replace(docText, item.NOMCID);
                    else
                        docText = regexTextCidade.Replace(docText, "");


                    Regex regexTextEstado = new Regex("@ESTADO");
                    if (item.CODESTUNI != null)
                        docText = regexTextEstado.Replace(docText, item.CODESTUNI);
                    else
                        docText = regexTextEstado.Replace(docText, "");


                    Regex regexTextCep = new Regex("@CEP");
                    if (item.CODCEP != null)
                        docText = regexTextCep.Replace(docText, item.CODCEP);
                    else
                        docText = regexTextCep.Replace(docText, "");

                    Regex regexTextCnpj = new Regex("@CNPJ");
                    if (item.NUMCGCCLI != null)
                        docText = regexTextCnpj.Replace(docText, item.NUMCGCCLI);
                    else
                        docText = regexTextCnpj.Replace(docText, "");


                    CultureInfo culture = new CultureInfo("pt-BR");
                    DateTimeFormatInfo dtfi = culture.DateTimeFormat;
                    string mes = culture.TextInfo.ToTitleCase(dtfi.GetMonthName(DateTime.Now.Month));

                    Regex regexTextDia = new Regex("@DIA");
                    docText = regexTextDia.Replace(docText, DateTime.Now.Day.ToString());

                    Regex regexTextMes = new Regex("@MES");
                    docText = regexTextMes.Replace(docText, mes);

                    Regex regexTextAno = new Regex("@ANO");
                    docText = regexTextAno.Replace(docText, DateTime.Now.Year.ToString());

                    using (StreamWriter sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                    {
                        sw.Write(docText);
                    }

                    wordDoc.Close();
                }

                //using (WordprocessingDocument doc = WordprocessingDocument.Open(@Caminhocontrato + "\\" + item.CODCLI.ToString().PadLeft(3, '0') + "__" + _nomeArquivo, true))
                using (WordprocessingDocument doc = WordprocessingDocument.Open(@Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + item.CODCLI.ToString().PadLeft(3, '0') + "_" + _nomeArquivo, true))
                {
                    var body = doc.MainDocumentPart.Document.Body;

                    DocumentFormat.OpenXml.Wordprocessing.Paragraph newPara = new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run
                         (new Break() { Type = BreakValues.Page },
                         new Text("text on the new page")));

                    body.Append(newPara);

                    doc.MainDocumentPart.Document.Save();
                    doc.Close();
                }

                _horaminutsegundodiamesano = DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString();

                //FileInfo _filepdf = new FileInfo(@Caminhocontrato + "\\" + item.CODCLI.ToString().PadLeft(3, '0') + "_" + _nomeArquivoPdf);
                //if (_filepdf.Exists)
                //{
                //    _filepdf.CopyTo(caminhouploadbackup + "\\" + _horaminutsegundodiamesano + item.CODCLI.ToString().PadLeft(3, '0') + "_" + _nomeArquivoPdf);
                //    _filepdf.Delete();
                //}

                SautinSoft.PdfMetamorphosis p = new SautinSoft.PdfMetamorphosis();
                if (p != null)
                {
                    //string docxPath = @Caminhocontrato + "\\" + item.CODCLI.ToString().PadLeft(3, '0') + "__" + _nomeArquivo;
                    //string pdfPath = Path.ChangeExtension(docxPath, ".pdf");

                    string docxPath = @Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + item.CODCLI.ToString().PadLeft(3, '0') + "_" + _nomeArquivo;
                    string pdfPath = Path.ChangeExtension(docxPath, ".pdf");

                    int result = p.DocxToPdfConvertFile(docxPath, pdfPath);

                    //if (result == 0)
                    //System.Diagnostics.Process.Start(pdfPath);
                }
                p = null;

                //PdfReader _opa = new PdfReader(@Caminhocontrato + "\\" + item.CODCLI.ToString().PadLeft(3, '0') + "__" + _nomeArquivoPdf);
                PdfReader _opa = new PdfReader(@Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + item.CODCLI.ToString().PadLeft(3, '0') + "_" + _nomeArquivoPdf);

                var numero = _opa.NumberOfPages;
                _opa.Close();

                //string sourceFile = @Caminhocontrato + "\\" + item.CODCLI.ToString().PadLeft(3, '0') + "__" + _nomeArquivoPdf;
                //string destFile = @Caminhocontrato + "\\" + item.CODCLI.ToString().PadLeft(3, '0') + "_" + _nomeArquivoPdf;

                string sourceFile = @Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + item.CODCLI.ToString().PadLeft(3, '0') + "_" + _nomeArquivoPdf;
                string destFile = @Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + item.CODCLI.ToString().PadLeft(3, '0') + "__" + _nomeArquivoPdf;

                //Remove all pages except 1,2,3,4 and 6               
                //for ( i = 1 numero)
                int[] _manterpaginas = new int[numero - 1];
                for (int i = 1; i < numero; i++)
                {
                    _manterpaginas[i - 1] = i;
                }
                removePagesFromPdf(sourceFile, destFile, _manterpaginas);


                FileInfo _filedoc = new FileInfo(@Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + item.CODCLI.ToString().PadLeft(3, '0') + "_" + _nomeArquivo);
                if (_filedoc.Exists)
                    _filedoc.Delete();


                FileInfo _filepdf2 = new FileInfo(@Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + item.CODCLI.ToString().PadLeft(3, '0') + "_" + _nomeArquivoPdf);
                if (_filepdf2.Exists)
                {
                    _filepdf2.Delete();
                }

                Byte[] data = File.ReadAllBytes(destFile);
                String filedoc = Convert.ToBase64String(data);


                FileInfo _filepdf3 = new FileInfo(@Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + item.CODCLI.ToString().PadLeft(3, '0') + "__" + _nomeArquivoPdf);
                if (_filepdf3.Exists)
                {
                    _filepdf3.Delete();
                }

                return codopt.ToString().PadLeft(3, '0') + "_" + item.CODCLI.ToString().PadLeft(3, '0') + "__" + _nomeArquivoPdf + " ; " + filedoc;
            }
            else
            {
                erro = new Exception("NÃO EXISTE O AQRUIVO PARA PREENCHER");
                Utilitario.CriaLogErro(null, "NÃO EXISTE O AQRUIVO PARA PREENCHER");
                Utilitario.InsereLog(null, "PreencherPorReplace", "int codopt, int codservico, OportunidadeContatoTO.obterDadosBasicos item", "CadastroOportunidadeBLL", "NÃO EXISTE O AQRUIVO PARA PREENCHER");
                throw erro;
            }
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO PREENCHER O AQRUIVO WORD.");
            Utilitario.InsereLog(ex, "PreencherPorReplace", "int codopt, int codservico, OportunidadeContatoTO.obterDadosBasicos item", "CadastroOportunidadeBLL", "ERRO AO PREENCHER O AQRUIVO WORD.");
            throw ex;
        }
    }


    public List<AnexoEmailTO.AnexoEmail> PreencherPorReplaceDocs(Int32 codopt, Int32 _CODSERVICO, string ListDocs, OportunidadeContatoTO.obterDadosBasicos itemDadosCli, string _emialResponsvaeleNome)
    {

        var _caminhoServico = ConfigurationManager.AppSettings["caminhoTemplateContratoVenda"];
        var Caminhocontrato = System.Web.HttpContext.Current.Server.MapPath("~\\UploadedFiles");
        var caminhouploadbackup = ConfigurationManager.AppSettings["caminhoContratoClienteAceiteBackup"];
        List<CadastroOportunidadeTO.ServicosDocumento> ServicoDocs = new List<CadastroOportunidadeTO.ServicosDocumento>();

        var DalDocs = new CadastroOportunidadeDAL();
        ServicoDocs = DalDocs.BuscarDocumentosServicos(_CODSERVICO);

        string _nomeArquivo = "";
        string base64 = "";
        string _nomeArquivoPdf = "";
        string _nomeArquivoPdfEnvio = "";

        AnexoEmailTO.AnexoEmail _item = new AnexoEmailTO.AnexoEmail();
        List<AnexoEmailTO.AnexoEmail> _anexos = new List<AnexoEmailTO.AnexoEmail>();


        try
        {

            if (ServicoDocs != null)
            {
                foreach (CadastroOportunidadeTO.ServicosDocumento item in ServicoDocs)
                {
                    if (ListDocs.Split(';').Contains(item.CODDOCARZ.ToString()))
                    {

                        _nomeArquivo = item.CDOANXDOCARZ.Split(';')[0].Trim();
                        base64 = item.CDOANXDOCARZ.Split(';')[1].Trim();

                        _nomeArquivoPdf = _nomeArquivo.Substring(0, _nomeArquivo.Length - 5) + ".pdf";

                        _nomeArquivoPdfEnvio = itemDadosCli.NUMCGCCLI.Trim() + "_" + _nomeArquivo.Substring(0, _nomeArquivo.Length - 5) + "_" + codopt.ToString().PadLeft(3, '0') + ".pdf";


                        FileInfo fileexitente = new FileInfo(@Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + itemDadosCli.CODCLI.ToString().PadLeft(3, '0') + _nomeArquivo);
                        if (!fileexitente.Exists)
                        {
                            Byte[] bytes = Convert.FromBase64String(base64);
                            File.WriteAllBytes(@Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + itemDadosCli.CODCLI.ToString().PadLeft(3, '0') + "_" + _nomeArquivo, bytes);
                        }


                        Exception erro;

                        FileInfo file = new FileInfo(@Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + itemDadosCli.CODCLI.ToString().PadLeft(3, '0') + "_" + _nomeArquivo);
                        if (file.Exists)
                        {
                            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(@Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + itemDadosCli.CODCLI.ToString().PadLeft(3, '0') + "_" + _nomeArquivo, true))
                            {
                                string docText = null;
                                using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                                {
                                    docText = sr.ReadToEnd();
                                }

                                Regex regexText = new Regex("@RAZAOSOCIAL");
                                if (itemDadosCli.NOMCLI != null)
                                    docText = regexText.Replace(docText, itemDadosCli.NOMCLI.Replace("&", " ").Replace("-", " "));
                                else
                                    docText = regexText.Replace(docText, "");


                                Regex regexTextRua = new Regex("@RUA");
                                if (itemDadosCli.ENDERECO != null)
                                    docText = regexTextRua.Replace(docText, itemDadosCli.ENDERECO);
                                else
                                    docText = regexTextRua.Replace(docText, "");


                                Regex regexTextCidade = new Regex("@CIDADE");
                                if (itemDadosCli.NOMCID != null)
                                    docText = regexTextCidade.Replace(docText, itemDadosCli.NOMCID);
                                else
                                    docText = regexTextCidade.Replace(docText, "");


                                Regex regexTextEstado = new Regex("@ESTADO");
                                if (itemDadosCli.CODESTUNI != null)
                                    docText = regexTextEstado.Replace(docText, itemDadosCli.CODESTUNI);
                                else
                                    docText = regexTextEstado.Replace(docText, "");


                                Regex regexTextCep = new Regex("@CEP");
                                if (itemDadosCli.CODCEP != null)
                                    docText = regexTextCep.Replace(docText, itemDadosCli.CODCEP);
                                else
                                    docText = regexTextCep.Replace(docText, "");

                                Regex regexTextCnpj = new Regex("@CNPJ");
                                if (itemDadosCli.NUMCGCCLI != null)
                                    docText = regexTextCnpj.Replace(docText, itemDadosCli.NUMCGCCLI);
                                else
                                    docText = regexTextCnpj.Replace(docText, "");


                                Regex regexTexEndereco = new Regex("@ENDERECO");
                                if (_emialResponsvaeleNome != "")
                                    docText = regexTexEndereco.Replace(docText, itemDadosCli.ENDERECO.Trim() + ", " + itemDadosCli.NOMBAI.Trim());
                                else
                                    docText = regexTexEndereco.Replace(docText, "");


                                Regex regexUF = new Regex("@UF");
                                if (_emialResponsvaeleNome != "")
                                    docText = regexUF.Replace(docText, itemDadosCli.CODESTUNI);
                                else
                                    docText = regexUF.Replace(docText, "");


                                Regex regexNomeFantasia = new Regex("@NOMEFANTASIA");
                                if (_emialResponsvaeleNome != "")
                                    docText = regexNomeFantasia.Replace(docText, itemDadosCli.NOMFNTCLI.Trim());
                                else
                                    docText = regexNomeFantasia.Replace(docText, "");



                                CultureInfo culture = new CultureInfo("pt-BR");
                                DateTimeFormatInfo dtfi = culture.DateTimeFormat;
                                string mes = culture.TextInfo.ToTitleCase(dtfi.GetMonthName(DateTime.Now.Month));

                                Regex regexTextDia = new Regex("@DIA");
                                docText = regexTextDia.Replace(docText, DateTime.Now.Day.ToString());

                                Regex regexTextMes = new Regex("@MES");
                                docText = regexTextMes.Replace(docText, mes);

                                Regex regexTextAno = new Regex("@ANO");
                                docText = regexTextAno.Replace(docText, DateTime.Now.Year.ToString());

                                using (StreamWriter sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                                {
                                    sw.Write(docText);
                                }

                                wordDoc.Close();
                            }

                            using (WordprocessingDocument doc = WordprocessingDocument.Open(@Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + itemDadosCli.CODCLI.ToString().PadLeft(3, '0') + "_" + _nomeArquivo, true))
                            {
                                var body = doc.MainDocumentPart.Document.Body;

                                DocumentFormat.OpenXml.Wordprocessing.Paragraph newPara = new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run
                                     (new Break() { Type = BreakValues.Page },
                                     new Text("text on the new page")));

                                body.Append(newPara);

                                doc.MainDocumentPart.Document.Save();
                                doc.Close();
                            }


                            SautinSoft.PdfMetamorphosis p = new SautinSoft.PdfMetamorphosis();
                            if (p != null)
                            {

                                string docxPath = @Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + itemDadosCli.CODCLI.ToString().PadLeft(3, '0') + "_" + _nomeArquivo;
                                string pdfPath = Path.ChangeExtension(docxPath, ".pdf");

                                int result = p.DocxToPdfConvertFile(docxPath, pdfPath);
                            }
                            p = null;

                            PdfReader _opa = new PdfReader(@Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + itemDadosCli.CODCLI.ToString().PadLeft(3, '0') + "_" + _nomeArquivoPdf);

                            var numero = _opa.NumberOfPages;
                            _opa.Close();

                            string sourceFile = @Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + itemDadosCli.CODCLI.ToString().PadLeft(3, '0') + "_" + _nomeArquivoPdf;
                            string destFile = @Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + itemDadosCli.CODCLI.ToString().PadLeft(3, '0') + "__" + _nomeArquivoPdf;

                            //Remove all pages except 1,2,3,4 and 6               
                            //for ( i = 1 numero)
                            int[] _manterpaginas = new int[numero - 1];
                            for (int i = 1; i < numero; i++)
                            {
                                _manterpaginas[i - 1] = i;
                            }
                            removePagesFromPdf(sourceFile, destFile, _manterpaginas);


                            Byte[] data = File.ReadAllBytes(destFile);
                            String filedoc = Convert.ToBase64String(data);


                            if (item.INDENV == 1)
                            {
                                _item = new AnexoEmailTO.AnexoEmail();
                                string text = System.IO.File.ReadAllText(sourceFile);
                                _item.FileByte = data; //Encoding.ASCII.GetBytes(text);
                                _item.FileName = _nomeArquivoPdfEnvio;
                                _anexos.Add(_item);
                            }

                            DalDocs.inserirDocmentosBandeiraOportunidade(itemDadosCli.CODCLI, item.CODDOCARZ, codopt, _CODSERVICO, _nomeArquivoPdfEnvio + "; " + filedoc);


                            FileInfo _filedoc = new FileInfo(@Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + itemDadosCli.CODCLI.ToString().PadLeft(3, '0') + "_" + _nomeArquivo);
                            if (_filedoc.Exists)
                                _filedoc.Delete();


                            FileInfo _filepdf2 = new FileInfo(@Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + itemDadosCli.CODCLI.ToString().PadLeft(3, '0') + "_" + _nomeArquivoPdf);
                            if (_filepdf2.Exists)
                            {
                                _filepdf2.Delete();
                            }

                            FileInfo _filepdf3 = new FileInfo(@Caminhocontrato + "\\" + codopt.ToString().PadLeft(3, '0') + "_" + itemDadosCli.CODCLI.ToString().PadLeft(3, '0') + "__" + _nomeArquivoPdf);
                            if (_filepdf3.Exists)
                            {
                                _filepdf3.Delete();
                            }


                        }
                        else
                        {
                            erro = new Exception("NÃO EXISTE O AQRUIVO PARA PREENCHER");
                            Utilitario.CriaLogErro(null, "NÃO EXISTE O AQRUIVO PARA PREENCHER");
                            Utilitario.InsereLog(null, "PreencherPorReplaceDocs", "int codopt, int codservico, OportunidadeContatoTO.obterDadosBasicos item", "CadastroOportunidadeBLL", "NÃO EXISTE O AQRUIVO PARA PREENCHER");
                            throw erro;
                        }

                    }
                }
            }


            return _anexos;


        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO PREENCHER O AQRUIVO WORD.");
            Utilitario.InsereLog(ex, "PreencherPorReplace", "int codopt, int codservico, OportunidadeContatoTO.obterDadosBasicos item", "CadastroOportunidadeBLL", "ERRO AO PREENCHER O AQRUIVO WORD.");
            throw ex;
        }


    }


    //public void Substitui(Word.Document oDoc, object parametro, object texto)
    //{
    //    object missing = System.Reflection.Missing.Value;

    //    //Troca o conteúdo de alguns tags 
    //    Word.Range oRng = oDoc.Range(ref missing, ref missing);

    //    object FindText = parametro;
    //    object ReplaceWith = texto;
    //    object MatchWholeWord = true;
    //    object Forward = false;

    //    oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
    //    ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);
    //}

    public static string GetTemplateHtml(string path)
    {
        var logicPath = System.Web.HttpContext.Current.Server.MapPath(path);
        return System.IO.File.ReadAllText(logicPath);
    }


    public bool RenviarEmailServicoOportunidade(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        List<OportunidadeContatoTO.obterServico> _list_servicoscliente;
        List<OportunidadeContatoTO.obterDadosBasicos> _list_dadoclient;

        OportunidadeContatoTO.obterDadosBasicos _item;
        CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade _servicovalores;

        var DALOPTCONT = new OportunidadeContatoDAL();
        var dalservicooportunit = new CadastroServicoOportunidadeDAL();

        CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objAlterarEmail = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();

        string Message = "";
        bool _emailenviado = false;
        string _sitecontrato = ConfigurationManager.AppSettings["stieContato"];

        var Caminhocontrato = ConfigurationManager.AppSettings["caminhoContratoClienteAceite"];
        string _templateemail = ConfigurationManager.AppSettings["caminhoContratoTemplateEmail"];
        string _templateemail2 = ConfigurationManager.AppSettings["caminhoContratoTemplateEmail2"];
        string[] _codigosemail2 = ConfigurationManager.AppSettings["codigosEmail2"].Split(';');


        string _dadossitecontrato = "";
        string _linksitecontrato = "";

        CryptUtil _encript = new CryptUtil();

        Exception erro;
        try
        {
            if (1 == 1)
            {
                _list_servicoscliente = DALOPTCONT.obterServicos(objAlterar.CODOPTVND);
                _list_dadoclient = DALOPTCONT.obterDadoBasicos(objAlterar.CODCLI);
                _item = _list_dadoclient.FirstOrDefault();

                foreach (OportunidadeContatoTO.obterServico item in _list_servicoscliente)
                {
                    if (objAlterar.CODSVCTGV == item.CODSVCTGV)
                    {
                        objAlterarEmail.CODOPTVND = item.CODOPTVND;
                        objAlterarEmail.CODSVCTGV = item.CODSVCTGV;
                        _servicovalores = RetornarVlorServico(objAlterarEmail);

                        _dadossitecontrato = string.Format("cliente={0}&servico={1}&oprtunidade={2}&token={3}", objAlterar.CODCLI, item.CODSVCTGV, item.CODOPTVND, "kajsroijarj7432847kjfs");
                        _linksitecontrato = string.Format("{0}?{1}", _sitecontrato, _encript.ActionEncrypt(_dadossitecontrato));

                        if (_codigosemail2.Contains(item.CODSVCTGV.ToString()))
                            Message = GetTemplateHtml(_templateemail2);
                        else
                            Message = GetTemplateHtml(_templateemail);


                        Message = Message.Replace("###Cliente", string.Format("{0} - {1} - {2} ", _item.CODCLI, _item.NOMCLI.Trim(), _item.NUMCGCCLI));


                        Message = Message.Replace("###linkContrato", _linksitecontrato);
                        Message = Message.Replace("###linkCDesc", "CONTRATO - " + string.Format("{0}", _servicovalores.DESSVCTGV));

                        Message = Message.Replace("###nomServico", string.Format("{0}", _servicovalores.DESSVCTGV));
                        Message = Message.Replace("###valorservico", string.Format("{0}", _servicovalores.VLRSVCTOT.ToString("n2")));

                        //var _nomeArquivo = BuscarTemplateServico(item.CODSVCTGV);
                        var _nomeArquivo = "";
                        if (_servicovalores.CDOANXCTTCLI.Trim() == "")
                        {
                            if (item.TEMPLATESERVICO != null)
                            {
                                if (item.TEMPLATESERVICO.Trim() != null)
                                    _nomeArquivo = item.TEMPLATESERVICO.Split(';')[0].Trim();
                            }
                        }
                        else
                            _nomeArquivo = _servicovalores.NOMARQORI; //_servicovalores.CDOANXCTTCLI.Split(';')[0].Trim();



                        if (_nomeArquivo.Trim() != "")
                        {
                            var _nomeArquivoPdf = _nomeArquivo.Substring(0, _nomeArquivo.Length - 5) + ".pdf";
                            FileInfo _filepdf = new FileInfo(@Caminhocontrato + "\\" + item.CODOPTVND.ToString().PadLeft(3, '0') + "_" + objAlterar.CODCLI + "_" + _nomeArquivoPdf);

                            if (!_filepdf.Exists)
                                _nomeArquivoPdf = PreencherPorReplace(item.CODOPTVND, item.CODSVCTGV, _item, item.TEMPLATESERVICO);
                            else
                            {
                                //_nomeArquivoPdf = item.CODOPTVND.ToString().PadLeft(3, '0') + "_" + objAlterar.CODCLI.ToString().PadLeft(3, '0') + "_" + _nomeArquivoPdf;
                                _nomeArquivoPdf = PreencherPorReplace(item.CODOPTVND, item.CODSVCTGV, _item, item.TEMPLATESERVICO);
                            }

                            if (objAlterar.EMAILESCOLHIDO != null)
                            {
                                if ((_nomeArquivoPdf != "") && (objAlterar.EMAILESCOLHIDO != ""))
                                {

                                    if (Arquitetura.Classes.Util.EnviaEmailMartins(objAlterar.EMAILESCOLHIDO.Split(';')[0].Trim(), null, "O seu contrato já está disponivel", Message, null, Arquitetura.Classes.Util.TipoEmail.HTML))
                                        _emailenviado = true;
                                    else
                                        _emailenviado = false;


                                    objAlterarEmail.DESENDETN = objAlterar.EMAILESCOLHIDO.Split(';')[0].Trim();

                                    if (_nomeArquivoPdf.Contains(";"))
                                    {
                                        objAlterarEmail.NOMARQORI = _nomeArquivoPdf.Split(';')[0].Trim();
                                        objAlterarEmail.CONTRATOBANCO = _nomeArquivoPdf.Split(';')[1].Trim();
                                    }
                                    else
                                        objAlterarEmail.NOMARQORI = _nomeArquivoPdf;


                                    dalservicooportunit.alterarEmailServicoOportunidade(objAlterarEmail, _emailenviado);
                                }
                            }
                        }
                        else
                        {
                            erro = new Exception("NÃO EXISTE O AQRUIVO PARA PREENCHER");
                            Utilitario.InsereLog(null, "alterarCadastroOportunidade", "int codopt, int codservico, OportunidadeContatoTO.obterDadosBasicos item", "CadastroOportunidadeBLL", "NÃO EXISTE O AQRUIVO PARA PREENCHER");
                            throw erro;
                        }
                    }
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO RENVIAR O EMAIL .");
            Utilitario.InsereLog(ex, "RenviarEmailServicoOportunidade", "CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar", "CadastroOportunidadeBLL", "ERRO AO REENVIAR O EMAIL.");
            throw;
        }
    }
    public bool RenviarEmailServicoOportunidadeDocumento(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        List<OportunidadeContatoTO.obterServico> _list_servicoscliente;
        List<OportunidadeContatoTO.obterDadosBasicos> _list_dadoclient;

        OportunidadeContatoTO.obterDadosBasicos _item;
        CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade _servicovalores;

        var DALOPTCONT = new OportunidadeContatoDAL();
        var dalservicooportunit = new CadastroServicoOportunidadeDAL();
        var oportunidadeBLL = new CadastroServicoOportunidadeBLL();

        var objPesquisar = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel() { LSTCODDOCARZ = objAlterar.CODDOCARZ, LISTCODSVCTGV = objAlterar.LISTCODSVCTGV, CODOPTVND = objAlterar.CODOPTVND };
        var retornoDoc = oportunidadeBLL.obterDocumento(objPesquisar);

        string mensagemDocumento = string.Empty;
        List<AnexoEmailTO.AnexoEmail> _listAnexos = new List<AnexoEmailTO.AnexoEmail>();

        if (retornoDoc.Any())
        {
            foreach (var item in retornoDoc)
            {
                mensagemDocumento = item.DocumentoEnviado;
                Byte[] bytes;
                AnexoEmailTO.AnexoEmail itemEmail = new AnexoEmailTO.AnexoEmail();

                if (mensagemDocumento.Contains(";"))
                {
                    bytes = Convert.FromBase64String(mensagemDocumento.Split(';')[1]);
                    itemEmail.FileName = mensagemDocumento.Split(';')[0];
                }
                else
                {
                    bytes = Convert.FromBase64String(mensagemDocumento);
                    itemEmail.FileName = "Documento_" + objAlterar.CODOPTVND + "_" + objAlterar.CODSVCTGV + "_" + DateTime.Now.ToString("ddMMyyyy") + ".pdf";
                }
                //string text = System.IO.File.ReadAllText(sourceFile);
                itemEmail.FileByte = bytes; //Encoding.ASCII.GetBytes(text);

                _listAnexos.Add(itemEmail);
            }
        }
        else
            return false;
        //TODO RECEBER LISTA DE SERVICOS

        List<string> ListRetornoNomeServicos = new List<string>();
        foreach (var item in retornoDoc)
        {
            if (!ListRetornoNomeServicos.Contains(item.NomeServico))
                ListRetornoNomeServicos.Add(item.NomeServico);
        }

        string _templateDocumentoEmail = ConfigurationManager.AppSettings["caminhoDocumentoTemplateEmail"];
        var Message = GetTemplateHtml(_templateDocumentoEmail);
        var nomeServico = String.Join(";<p></p> <p></p>", ListRetornoNomeServicos);
        Message = Message.Replace("###nomServico", nomeServico);

        CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objAlterarEmail = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();

        CryptUtil _encript = new CryptUtil();

        var msgAssuntoEmail = "Documento Gerenciador disponível";
        Exception erro;
        try
        {
            _list_servicoscliente = DALOPTCONT.obterServicos(objAlterar.CODOPTVND);
            _list_dadoclient = DALOPTCONT.obterDadoBasicos(objAlterar.CODCLI);
            _item = _list_dadoclient.FirstOrDefault();
            if (!string.IsNullOrEmpty(_item.NOMCLI))
                Message = Message.Replace("###Cliente", _item.NOMCLI);

            if (objAlterar.EMAILESCOLHIDO != null && objAlterar.EMAILESCOLHIDO != "")
            {
                if (Arquitetura.Classes.Util.EnviaEmailMartins(objAlterar.EMAILESCOLHIDO.Split(';')[0].Trim(), null, msgAssuntoEmail, Message, _listAnexos, Arquitetura.Classes.Util.TipoEmail.HTML))
                    return true;
            }
            else
            {
                erro = new Exception("NÃO EXISTE O AQRUIVO PARA PREENCHER");
                Utilitario.InsereLog(null, "alterarCadastroOportunidade", "int codopt, int codservico, OportunidadeContatoTO.obterDadosBasicos item", "CadastroOportunidadeBLL", "NÃO EXISTE O AQRUIVO PARA PREENCHER");
                throw erro;
            }

            return true;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO RENVIAR O EMAIL .");
            Utilitario.InsereLog(ex, "RenviarEmailServicoOportunidade", "CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar", "CadastroOportunidadeBLL", "ERRO AO REENVIAR O EMAIL.");
            throw;
        }
    }

    public bool RenviarEmailServicoOportunidadeDocumentoNovo(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        List<OportunidadeContatoTO.obterServico> _list_servicoscliente;
        List<OportunidadeContatoTO.obterDadosBasicos> _list_dadoclient;

        OportunidadeContatoTO.obterDadosBasicos _item;
        CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade _servicovalores;

        var DALOPTCONT = new OportunidadeContatoDAL();
        var dalservicooportunit = new CadastroServicoOportunidadeDAL();
        var oportunidadeBLL = new CadastroServicoOportunidadeBLL();

        //var objPesquisar = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel() { LSTCODDOCARZ = objAlterar.CODDOCARZ, LISTCODSVCTGV = objAlterar.LISTCODSVCTGV, CODOPTVND = objAlterar.CODOPTVND };
        //var retornoDoc = oportunidadeBLL.obterDocumento(objPesquisar);
        
        var DalDocs = new CadastroOportunidadeDAL();
        var ServicoDocs = DalDocs.BuscarDocumentosServicosNovo(objAlterar.CODDOCARZ, objAlterar.LISTCODSVCTGV);

        string mensagemDocumento = string.Empty;
        List<AnexoEmailTO.AnexoEmail> _listAnexos = new List<AnexoEmailTO.AnexoEmail>();
        List<string> ListRetornoNomeServicos = new List<string>();

        //remove serviço sem documento

        List<long> auxCodSvc = new List<long>();
        _list_dadoclient = DALOPTCONT.obterDadoBasicos(objAlterar.CODCLI);
        _item = _list_dadoclient.FirstOrDefault();
        if (ServicoDocs.Any())
        {
            //Remove os serviços que não tem relação
            foreach (var item in objAlterar.LISTCODSVCTGV)
            {
                if (ServicoDocs.Exists(x => x.CODSVCTGV == item))
                    auxCodSvc.Add(item);
            }
            objAlterar.LISTCODSVCTGV = auxCodSvc;
            foreach (var item in ServicoDocs)
            {
                mensagemDocumento = item.CDOANXDOCARZ;
                Byte[] bytes;
                AnexoEmailTO.AnexoEmail itemEmail = new AnexoEmailTO.AnexoEmail();

                //mensagemDocumento = _nomeArquivoPdf;
                if (mensagemDocumento.Contains(";"))
                {
                    bytes = Convert.FromBase64String(mensagemDocumento.Split(';')[1]);
                    itemEmail.FileName = mensagemDocumento.Split(';')[0];
                }
                else
                {
                    bytes = Convert.FromBase64String(mensagemDocumento);
                    itemEmail.FileName = "Documento_" + objAlterar.CODOPTVND + "_" + objAlterar.CODSVCTGV + "_" +DateTime.Now.ToString("ddMMyyyy")+ ".pdf";
                }
                //string text = System.IO.File.ReadAllText(sourceFile);
                itemEmail.FileByte = bytes; //Encoding.ASCII.GetBytes(text);
                
                _listAnexos.Add(itemEmail);
                if (!ListRetornoNomeServicos.Contains(item.DESSVCTGV))
                    ListRetornoNomeServicos.Add(item.DESSVCTGV);
                //Insere banco

                //DalDocs.inserirDocmentosBandeiraOportunidade(objAlterar.CODCLI, item.CODDOCARZ, objAlterar.CODOPTVND, item.CODSVCTGV, mensagemDocumento);
            }
        }
        else
            return false;
        //TODO RECEBER LISTA DE SERVICOS
        List<AnexoEmailTO.AnexoEmail> anexos = new List<AnexoEmailTO.AnexoEmail>();
        string listDocumento = "";
        int i = 1;
        foreach (var item in objAlterar.CODDOCARZ)
        {
            listDocumento += item;
            if (objAlterar.CODDOCARZ.Count != i)
            listDocumento += ";";
            i++;
        }
        var listAnexoxRetorno = new List<AnexoEmailTO.AnexoEmail>();
        foreach (var item in objAlterar.LISTCODSVCTGV)
        {
            listAnexoxRetorno = PreencherPorReplaceDocs(objAlterar.CODOPTVND, int.Parse(item.ToString()), listDocumento, _item, _item.NOMCLI);

            foreach (var item2 in listAnexoxRetorno)
            {
                anexos.Add(item2);
            }
        }

        string _templateDocumentoEmail = ConfigurationManager.AppSettings["caminhoDocumentoTemplateEmail"];
        var Message = GetTemplateHtml(_templateDocumentoEmail);
        var nomeServico = String.Join(";<p></p> <p></p>", ListRetornoNomeServicos);
        Message = Message.Replace("###nomServico", nomeServico);

        CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objAlterarEmail = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();

        CryptUtil _encript = new CryptUtil();

        var msgAssuntoEmail = "Documento Gerenciador disponível";
        Exception erro;
        try
        {
            _list_servicoscliente = DALOPTCONT.obterServicos(objAlterar.CODOPTVND);
           
            if (!string.IsNullOrEmpty(_item.NOMCLI))
                Message = Message.Replace("###Cliente", _item.NOMCLI);

            

            if (objAlterar.EMAILESCOLHIDO != null && objAlterar.EMAILESCOLHIDO != "")
            {
                if (Arquitetura.Classes.Util.EnviaEmailMartins(objAlterar.EMAILESCOLHIDO.Split(';')[0].Trim(), null, msgAssuntoEmail, Message, anexos, Arquitetura.Classes.Util.TipoEmail.HTML))
                    return true;
            }
            else
            {
                erro = new Exception("NÃO EXISTE O AQRUIVO PARA PREENCHER");
                Utilitario.InsereLog(null, "alterarCadastroOportunidade", "int codopt, int codservico, OportunidadeContatoTO.obterDadosBasicos item", "CadastroOportunidadeBLL", "NÃO EXISTE O AQRUIVO PARA PREENCHER");
                throw erro;
            }
                                   
            return true;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO RENVIAR O EMAIL .");
            Utilitario.InsereLog(ex, "RenviarEmailServicoOportunidade", "CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar", "CadastroOportunidadeBLL", "ERRO AO REENVIAR O EMAIL.");
            throw;
        }
    }

    public string BuscarTemplateServico(int _codservico)
    {
        var _caminhoServico = ConfigurationManager.AppSettings["caminhoTemplateContratoVenda"];

        try
        {

            var ex = new Exception();
            string[] dirs = Directory.GetFiles(@_caminhoServico, _codservico.ToString().PadLeft(3, '0') + "_" + "*");

            Utilitario.InsereLog(null, "BuscarTemplateServico", "BuscarTemplateServico", "BuscarTemplateServico", "quantidade achada  " + dirs.Count().ToString());

            int _totalsplit = 0;
            foreach (string dir in dirs)
            {
                _totalsplit = dir.Split('\\').Count();

                Utilitario.InsereLog(null, "BuscarTemplateServico", "BuscarTemplateServico", "BuscarTemplateServico", "quantidade totalizada  " + _totalsplit.ToString());

                Utilitario.InsereLog(null, "BuscarTemplateServico", "BuscarTemplateServico", "BuscarTemplateServico", "split do arquivo  " + dir.Split('\\')[_totalsplit - 1]);

                return dir.Split('\\')[_totalsplit - 1];
            }

            return "";
        }
        catch (Exception e)
        {
            Utilitario.CriaLogErro(e, "ERRO VERIFICAR VINCULO ARQUIVO.");
            Utilitario.InsereLog(e, "BuscarTemplateServico", "_codservico", "CadastroOportunidadeBLL", "ERRO VERIFICAR VINCULO ARQUIVO.");
            return "";
        }

    }


    public CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade RetornarVlorServico(CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objPesquisar)
    {
        try
        {
            int _codoportunidade = 0;
            string _descservico = "";
            string _NOMINDSTANGC = "";
            string _INDSTANGC = "";
            decimal _VALORCOND = 0;
            decimal _PERCOND = 0;
            int _NUMPTO = 0;
            string _DESTIPSVC = "";
            int _CODCNDCMC = 0;

            string _STATUSENVIO = "";
            string _STATUSACEITE = "";

            int _codservico = 0;
            decimal _valorservico = 0;
            decimal _valordescserv = 0;
            decimal _perserv = 0;

            decimal _valorServDesc = 0;

            int _codCondicao = 0;

            decimal _valorServCond = 0;

            decimal _valorFinal = 0;
            decimal _valorFinalServico = 0;

            string _CDOANXCTTCLI = "";
            string _NOMARQORI = "";
            int _INDSVCVND = 0;

            int CODGRPEMPFAT = 0;
            string NOMGRPEMPCLI = "";


            Boolean _finalizouCond = false;

            List<CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade> _listretorn = new List<CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade>();
            CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade _itemInsert = new CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade();

            var DAL = new CadastroServicoOportunidadeDAL();
            List<CadastroServicoOportunidadeTO.obterCadastroServicoOportunidadeLocal> _listLocal = DAL.obterCadastroServicoOportunidade(objPesquisar);

            foreach (CadastroServicoOportunidadeTO.obterCadastroServicoOportunidadeLocal _item in _listLocal)
            {
                if (_item.CODSVCTGV != _codservico)
                {

                    if (_finalizouCond)
                    {
                        _valorFinalServico = _valorServDesc - _valorServCond;

                        _itemInsert.CODOPTVND = _codoportunidade;
                        _itemInsert.CODSVCTGV = _codservico;
                        _itemInsert.VLRSVC = _valorservico;
                        _itemInsert.PERDSC = _perserv;//_perserv.ToString("n2");

                        _itemInsert.VLRDSC = _valordescserv;// _valordescserv.ToString("n2");

                        if (_perserv > 0)
                            _itemInsert.VLRTOTDES = ((_valordescserv + _valorServCond) + (_valorservico * (_perserv / 100))); //.ToString("n2");
                        else
                            _itemInsert.VLRTOTDES = (_valordescserv + _valorServCond);//.ToString("n2");


                        _itemInsert.DESSVCTGV = _descservico;
                        _itemInsert.NOMINDSTANGC = _NOMINDSTANGC;
                        _itemInsert.INDSTANGC = _INDSTANGC;
                        _itemInsert.VALORCOND = _VALORCOND;
                        _itemInsert.PERCOND = _PERCOND;
                        _itemInsert.NUMPTOSVC = _NUMPTO;
                        _itemInsert.DESTIPSVC = _DESTIPSVC;
                        _itemInsert.CODCNDCMC = _CODCNDCMC;

                        _itemInsert.STATUSENVIO = _STATUSENVIO;

                        _itemInsert.STATUSACEITE = _STATUSACEITE;

                        _itemInsert.VLRSVCTOT = Math.Round(_valorFinalServico, 2);//.ToString();


                        _valorFinal += _valorFinalServico;
                        _itemInsert.CDOANXCTTCLI = _CDOANXCTTCLI;
                        _itemInsert.NOMARQORI = _NOMARQORI;

                        _itemInsert.INDSVCVND = _INDSVCVND;

                        _itemInsert.CODGRPEMPFAT = CODGRPEMPFAT;
                        _itemInsert.NOMGRPEMPCLI = NOMGRPEMPCLI;


                        _valorServCond = 0;
                        _finalizouCond = false;

                        _listretorn.Add(_itemInsert);
                        _itemInsert = null;
                        _itemInsert = new CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade();
                    }

                    _valorServDesc = 0;
                    _valorservico = 0;
                    _valordescserv = 0;
                    _perserv = 0;

                    _codservico = _item.CODSVCTGV;
                    _valorservico = _item.VLRSVC;

                    _codoportunidade = _item.CODOPTVND;
                    _descservico = _item.DESSVCTGV;
                    _NOMINDSTANGC = _item.NOMINDSTANGC.Trim();
                    _INDSTANGC = _item.INDSTANGC;
                    _VALORCOND = _item.VALORCOND;
                    _PERCOND = _item.PERCOND;
                    _NUMPTO = _item.NUMPTOSVC;
                    _DESTIPSVC = _item.DESTIPSVC;
                    _CODCNDCMC = _item.CODCNDCMC;
                    _STATUSENVIO = _item.STATUSENVIO;
                    _STATUSACEITE = _item.STATUSACEITE;
                    _CDOANXCTTCLI = _item.CDOANXCTTCLI;
                    _NOMARQORI = _item.NOMARQORI;

                    _INDSVCVND = _item.INDSVCVND;

                    CODGRPEMPFAT = _item.CODGRPEMPFAT;

                    if (_item.NOMGRPEMPCLI != null)
                        NOMGRPEMPCLI = _item.NOMGRPEMPCLI;


                    //if (_item.VLRDSC != null)
                    //{
                    //_valordescserv = decimal.Parse(_item.VLRDSC.ToString("n3"));
                    //if (_valordescserv > 0)
                    //    _valorServDesc = _valorservico - _valordescserv;
                    //}

                    //if (_item.PERDSC != null)
                    //{
                    //_perserv = decimal.Parse(_item.PERDSC.ToString("n3"));
                    //if (_perserv > 0)
                    //    _valorServDesc = _valorservico - (_valorservico * (_perserv / 100));
                    //}

                    if (_valorServDesc == 0)
                        _valorServDesc = _valorservico;

                    //_valorFinalServico += _valorServDesc;
                }

                if ((_item.CODCNDCMC != _codCondicao) && (_item.CODSVCTGV == _codservico))
                {
                    _finalizouCond = true;

                    _codCondicao = _item.CODCNDCMC;

                    //if ((_item.VALORCOND != null) && (_item.VALORCOND > 0))
                    //{
                    //    //_vlrDescCond = _item.VALORCOND;
                    //    //_valorServCond += decimal.Parse(_vlrDescCond.ToString("n2"));
                    //}

                    //if ((_item.PERCOND != null) && (_item.PERCOND > 0))
                    //{
                    //    //_vlrPerCond = _item.PERCOND;
                    //    //_valorServCond += decimal.Parse((_valorServDesc * (_vlrPerCond / 100)).ToString("n3"));
                    //}
                }
                else
                {
                    if (_item.CODCNDCMC == 0)
                        _finalizouCond = true;
                }

            }

            if (_finalizouCond)
            {
                _valorFinalServico = _valorServDesc - _valorServCond;

                _itemInsert.CODOPTVND = _codoportunidade;
                _itemInsert.CODSVCTGV = _codservico;
                _itemInsert.VLRSVC = _valorservico;

                _itemInsert.PERDSC = _perserv;//.ToString("n2");
                _itemInsert.VLRDSC = _valordescserv;//.ToString("n2");

                _itemInsert.DESSVCTGV = _descservico;
                _itemInsert.NOMINDSTANGC = _NOMINDSTANGC;
                _itemInsert.INDSTANGC = _INDSTANGC;
                _itemInsert.VALORCOND = _VALORCOND;
                _itemInsert.PERCOND = _PERCOND;
                _itemInsert.NUMPTOSVC = _NUMPTO;
                _itemInsert.DESTIPSVC = _DESTIPSVC;
                _itemInsert.CODCNDCMC = _CODCNDCMC;
                _itemInsert.STATUSENVIO = _STATUSENVIO;
                _itemInsert.STATUSACEITE = _STATUSACEITE;
                _itemInsert.CDOANXCTTCLI = _CDOANXCTTCLI;
                _itemInsert.NOMARQORI = _NOMARQORI;
                _itemInsert.INDSVCVND = _INDSVCVND;

                _itemInsert.CODGRPEMPFAT = CODGRPEMPFAT;

                if (_itemInsert.NOMGRPEMPCLI != null)
                    _itemInsert.NOMGRPEMPCLI = NOMGRPEMPCLI;

                _itemInsert.VLRSVCTOT = Math.Round(_valorFinalServico, 2);//.ToString();

                if (_perserv > 0)
                    _itemInsert.VLRTOTDES = ((_valordescserv + _valorServCond) + (_valorservico * (_perserv / 100)));//.ToString("n2");
                else
                    _itemInsert.VLRTOTDES = (_valordescserv + _valorServCond);//.ToString("n2");


                _valorFinal += _valorFinalServico;

                _valorServCond = 0;
                _finalizouCond = false;

                _itemInsert.VLRSVCTOT = Math.Round(_valorFinalServico, 2);//.ToString();

                _listretorn.Add(_itemInsert);
                _itemInsert = null;
                _itemInsert = new CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade();
            }

            return _listretorn.FirstOrDefault();

        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO SERVIÇO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "obterCadastroServicoOportunidade", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoOportunidadeBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO SERVIÇO OPORTUNIDADE.");
            throw;
        }
    }

    public bool VerificaExisteTemplate(int _codigoservico)
    {
        try
        {
            var _caminhoServico = ConfigurationManager.AppSettings["caminhoTemplateContratoVenda"];
            var _nomeArquivo = BuscarTemplateServico(_codigoservico);

            FileInfo file = new FileInfo(@_caminhoServico + "\\" + _nomeArquivo);


            Utilitario.InsereLog(null, "VerificaExisteTemplate", "VerificaExisteTemplate", "VerificaExisteTemplate", "CAMINHO SERVIDOR :  " + @_caminhoServico + "\\" + _nomeArquivo);


            if (file.Exists)
                return true;
            else
                return false;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO VERIFICAR EXIXTE TEMPLATE");
            Utilitario.InsereLog(ex, "VerificaExisteTemplate", "_codigoservico", "CadastroServicoOportunidadeBLL", "ERRO AO VERIFICAR EXIXTE TEMPLATE.");
            throw ex;
        }
    }

    public void removePagesFromPdf(String sourceFile, String destinationFile, params int[] pagesToKeep)
    {
        //Used to pull individual pages from our source
        PdfReader r = new PdfReader(sourceFile);
        //Create our destination file
        using (FileStream fs = new FileStream(destinationFile, FileMode.Create, FileAccess.Write, FileShare.None))
        {
            using (iTextSharp.text.Document doc = new iTextSharp.text.Document())
            {
                using (PdfWriter w = PdfWriter.GetInstance(doc, fs))
                {
                    //Open the desitination for writing
                    doc.Open();
                    //Loop through each page that we want to keep
                    foreach (int page in pagesToKeep)
                    {
                        //Add a new blank page to destination document
                        doc.NewPage();
                        //Extract the given page from our reader and add it directly to the destination PDF
                        w.DirectContent.AddTemplate(w.GetImportedPage(r, page), 0, 0);
                    }
                    //Close our document
                    doc.Close();
                }
            }
        }
        r.Close();
    }


    public bool CancelaCadastroOportunidade(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        var dalservicooportunit = new CadastroServicoOportunidadeDAL();
        var _itemservico = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();

        try
        {
            _itemservico.CODOPTVND = objAlterar.CODOPTVND;
            _itemservico.INDSTANGC = 3;
            _itemservico.CODSVCTGV = null;
            dalservicooportunit.alterarStatusServicoOportunidade(_itemservico);

            int _codigofunc = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
            var dalAprovador = new AprovacaoDAL();
            AprovacaoTO.obterEmailAprovador _aprovador;
            _aprovador = dalAprovador.obterEmailAprovador(_codigofunc);


            DateTime _datainsercao = DateTime.Now;
            string _horas = _datainsercao.Hour.ToString() + " : " + _datainsercao.Minute.ToString();


            if (objAlterar.DESMTVCNC != null)
            {
                if (objAlterar.DESMTVCNC != "")
                {
                    var DALANOTACAO = new CadastroAnotacaoDAL();
                    CadastroAnotacaoApiModel.CadastroAnotacaoApiModel _itemAnotacao = new CadastroAnotacaoApiModel.CadastroAnotacaoApiModel();

                    _itemAnotacao.CODCLI = objAlterar.CODCLI;
                    _itemAnotacao.CODOPTVND = objAlterar.CODOPTVND;
                    //_itemAnotacao.DESOBS = objAlterar.DESMTVCNC;
                    _itemAnotacao.DESOBS = "Negociação Cancelada por " + _aprovador.NOMFNC.Trim() + ", dia " + _datainsercao.ToShortDateString() + " às " + _horas + " \n Justificativa: " + objAlterar.DESMTVCNC.Trim();


                    _itemAnotacao.TIPOBSCLI = "2";
                    _itemAnotacao.CODMTVCTO = 14;
                    _itemAnotacao.DATCAD = DateTime.Now.ToString();

                    DALANOTACAO.inserirCadastroAnotacaoGeral(_itemAnotacao);
                }
            }

            objAlterar.INDSTAOPTVND = 4;
            objAlterar.CODFNC = _codigofunc;

            return new CadastroOportunidadeDAL().alterarCadastroOportunidade(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO VERIFICAR EXIXTE TEMPLATE");
            Utilitario.InsereLog(ex, "VerificaExisteTemplate", "_codigoservico", "CadastroServicoOportunidadeBLL", "ERRO AO VERIFICAR EXIXTE TEMPLATE.");
            throw ex;
        }

    }


    public bool EnviarParaAprovacao(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        Exception erro;

        string _templateemail = ConfigurationManager.AppSettings["caminhoContratoTemplateEmailAprovacao"];
        string _stieAprovacao = ConfigurationManager.AppSettings["stieAprovacao"];
        int _codigoAprovador = int.Parse(ConfigurationManager.AppSettings["codigoAprovador"]);

        string _linksiteAprovacao = "";
        string _linksiteRejeicao = "";

        string _dadossitecontratoAprovacao = "";
        string _dadossitecontratoRejeicao = "";

        CryptUtil _encript = new CryptUtil();

        string Message = "";

        var dalservicooportunit = new CadastroServicoOportunidadeDAL();
        CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel _itemservico = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();

        AprovacaoTO.obterEmailAprovador _aprovador;
        var DALOPTCONT = new OportunidadeContatoDAL();

        List<OportunidadeContatoTO.obterServico> _list_servicoscliente;


        try
        {

            using (TransactionScope Scope = new TransactionScope(TransactionScopeOption.Required))
            {

                var marcados = objAlterar.LISTSERVICOS;
                //foreach (int _marc in marcados)
                //{
                //    _itemservico.CODSVCTGV = _marc;
                //    _itemservico.CODOPTVND = objAlterar.CODOPTVND;
                //    _itemservico.INDSTANGC = 5;
                //    dalservicooportunit.alterarStatusServicoOportunidade(_itemservico);
                //    _itemservico = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
                //}

                _list_servicoscliente = DALOPTCONT.obterServicos(objAlterar.CODOPTVND);
                foreach (OportunidadeContatoTO.obterServico item in _list_servicoscliente)
                {
                    if (marcados.Exists(x => x == item.CODSVCTGV))
                    {
                        _itemservico.CODSVCTGV = item.CODSVCTGV;
                        _itemservico.CODOPTVND = objAlterar.CODOPTVND;
                        _itemservico.INDSTANGC = 1;
                        _itemservico.INDSVCVND = 1; //vendido 

                        dalservicooportunit.alterarStatusServicoOportunidade(_itemservico);
                        _itemservico = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
                    }
                    else
                    {
                        _itemservico.CODOPTVND = objAlterar.CODOPTVND;
                        _itemservico.CODSVCTGV = item.CODSVCTGV;
                        _itemservico.INDSTANGC = 1;
                        _itemservico.INDSVCVND = 2; //nao vendido

                        dalservicooportunit.alterarStatusServicoOportunidade(_itemservico);
                        _itemservico = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
                    }
                }

                if (objAlterar.DESCAPROVACAO != null)
                {
                    if (objAlterar.DESCAPROVACAO != "")
                    {
                        var DALANOTACAO = new CadastroAnotacaoDAL();
                        CadastroAnotacaoApiModel.CadastroAnotacaoApiModel _itemAnotacao = new CadastroAnotacaoApiModel.CadastroAnotacaoApiModel();

                        _itemAnotacao.CODCLI = objAlterar.CODCLI;
                        _itemAnotacao.CODOPTVND = objAlterar.CODOPTVND;
                        _itemAnotacao.DESOBS = objAlterar.DESCAPROVACAO;

                        _itemAnotacao.TIPOBSCLI = "2";
                        _itemAnotacao.CODMTVCTO = 10;
                        _itemAnotacao.DATCAD = DateTime.Now.ToString();

                        DALANOTACAO.inserirCadastroAnotacaoGeral(_itemAnotacao);


                        objAlterar.DESOBSDSC = objAlterar.DESCAPROVACAO;
                    }
                }

                //var _listlocal = new List<CadastroCondicaoComercialServicoOportunidadeTO.obterCadastroCondicaoComercialServicoOportunidadeNovo>();
                //CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel objBuscaServDesc = new CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel();

                OportunidadeContatoTO.obterDadosBasicos _dadoscliente;

                var DALDesc = new CadastroCondicaoComercialServicoOportunidadeDAL();
                var dalAprovador = new AprovacaoDAL();

                _dadoscliente = DALOPTCONT.obterDadoBasicos(objAlterar.CODCLI).FirstOrDefault();

                //foreach (CadastroCondicaoComercialServicoOportunidadeTO.obterCadastroCondicaoComercialServicoOportunidadeNovo _item in _listlocal)
                //{
                //    if (marcados.Contains(_item.CODSVCTGV))
                //    {
                //        if (_item.CODCNDCMC >= 9000)
                //        {
                //            if (codigojafoi != _item.CODSVCTGV.ToString())
                //            {
                //                codigojafoi = _item.CODSVCTGV.ToString();
                //                codigosServicosemail = codigosServicosemail + _item.CODSVCTGV + ",";
                //            }
                //        }
                //    }            
                //}

                _dadossitecontratoAprovacao = "oportunidade=" + objAlterar.CODOPTVND.ToString() + "&tipoaprovacao=6" + "&codparovador=" + _codigoAprovador.ToString() + "&codconsultor=" + objAlterar.CODFNC.ToString() + "&codcliente=" + _dadoscliente.CODCLI.ToString();
                _dadossitecontratoRejeicao = "oportunidade=" + objAlterar.CODOPTVND.ToString() + "&tipoaprovacao=7" + "&codparovador=" + _codigoAprovador.ToString() + "&codconsultor=" + objAlterar.CODFNC.ToString() + "&codcliente=" + _dadoscliente.CODCLI.ToString();

                _linksiteAprovacao = string.Format("{0}?{1}", _stieAprovacao, _encript.ActionEncrypt(_dadossitecontratoAprovacao));
                _linksiteRejeicao = string.Format("{0}?rpv={1}", _stieAprovacao, _encript.ActionEncrypt(_dadossitecontratoRejeicao));

                //Montar e enviar email; 
                Message = GetTemplateHtml(_templateemail);
                Message = Message.Replace("###codigocliente", _dadoscliente.CODCLI.ToString());
                Message = Message.Replace("###nomecliente", _dadoscliente.NOMCLI);
                Message = Message.Replace("###cnpjcliente", _dadoscliente.NUMCGCCLI);
                Message = Message.Replace("###nomeconsultor", objAlterar.NOMECOSULT);


                if (_dadoscliente.CODGRPEMPCLI != 0)
                {
                    Message = Message.Replace("###codigoGrupo", "<strong class='strong-min-tgv'> Grupo Econômico: </strong> " + _dadoscliente.CODGRPEMPCLI.ToString());
                }
                else
                {
                    Message = Message.Replace("###codigoGrupo", "<strong class='strong-min-tgv'> Grupo Econômico: </strong> NÂO");
                }


                Message = Message.Replace("###LinkAprovar", _linksiteAprovacao);
                Message = Message.Replace("###LinkADesc", "APROVAR");

                Message = Message.Replace("###LinkRejeitar", _linksiteRejeicao);
                Message = Message.Replace("###LinkRDesc", "REJEITAR");


                Message = Message.Replace("###justificativadesconto", objAlterar.DESCAPROVACAO);
                Message = Message.Replace("###servicos", BodyMessageAprovacao(objAlterar.CODOPTVND));

                _aprovador = dalAprovador.obterEmailAprovador(_codigoAprovador);

                Arquitetura.Classes.Util.EnviaEmailMartins(_aprovador.EMAILAPROVADOR, null, "Aprovar desconto", Message, null, Arquitetura.Classes.Util.TipoEmail.HTML);

                objAlterar.INDSTAOPTVND = 5;

                var DAL = new CadastroOportunidadeDAL();
                if (DAL.alterarCadastroOportunidade(objAlterar))
                {
                    Scope.Complete();
                    return true;
                }
                else
                {
                    Scope.Dispose();
                    erro = new Exception("NÃO AO ALTERAR A OPORTUNIDADE");
                    Utilitario.InsereLog(null, "EnviarParaAprovacao", "CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar", "CadastroOportunidadeBLL", "NÃO AO ALTERAR A OPORTUNIDADE");
                    throw erro;
                    //return false;
                }

            }
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO ENVIAR A APROVAÇÃO");
            Utilitario.InsereLog(ex, "EnviarParaAprovacao", "CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar", "CadastroOportunidadeBLL", "ERRO AO ENVIAR A APROVAÇÃO");
            throw ex;
        }


    }

    public string BodyMessageAprovacao(int _codoportunidade)
    {
        StringBuilder _head;
        StringBuilder _body;

        StringBuilder _servicos = new StringBuilder("");
        CadastroServicoOportunidadeTO.obterCadastroServicoOportunidade _servicovalores;
        List<OportunidadeContatoTO.obterServico> _list_servicoscliente;

        var DALOPTCONT = new OportunidadeContatoDAL();
        _list_servicoscliente = DALOPTCONT.obterServicos(_codoportunidade);

        var DAL = new CadastroCondicaoComercialServicoOportunidadeDAL();

        //{0} Codigo servico, {1} nome servico, {2} valor servico,  
        _head = new StringBuilder(@"<div style='text-align:left'>  <strong class='strong-min-tgv'>Serviço {0}: </strong> {1} <strong class='strong-min-tgv'>Valor: </strong> {2}</div>
                                        <br/> 
                                        <table id='example' class='display' cellspacing='0' width='100%'>
                                            <thead>
                                                <tr class='titulo-grid'>
                                                    <th>Tipo</th>
					                                <th>Nome do Desconto</th>
                                                    <th>Vinculado a Cesta de Produtos</th>
                                                    <th>
                                                        Desconto<br />
                                                        %
                                                    </th>
                                                    <th>
                                                        Desconto<br />
                                                        R$
                                                    </th>
                                                    <th>Ínicio</th>
                                                    <th>Fim</th>
                                                </tr>
                                            </thead>
                                            <tbody>");

        //{0} TipoDesconto, {1} nome desconto, {2} viculo cesta produto, {3} percentual desconto, {4} valor desconto, {5} data ou mes vigencia, {6} data fim , ou mes fim vigencia    
        _body = new StringBuilder(@"<tr style='background-color:white;'>
                                        <td>{0}</td>
				                        <td>{1}</td>
				                        <td>{2}</td>
				                        <td>{3}</td>
				                        <td>{4}</td>
				                        <td>{5}</td>
				                        <td>{6}</td>
			                        </tr>");



        CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel objValoresServico = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();

        int _codigoServico = 0;
        string _tipodesconto = "";
        string _datafim = "";
        string _datainicio = "";

        var dalservicooportunit = new CadastroServicoOportunidadeDAL();
        CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel _itemservico = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
        CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel _itemservicoperdido = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();


        if (_list_servicoscliente.Count > 0)
        {
            foreach (OportunidadeContatoTO.obterServico itemServico in _list_servicoscliente)
            {
                if (itemServico.INDSVCVND == 1)
                {
                    CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel objBuscaServDesc = new CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel();
                    List<CadastroCondicaoComercialServicoOportunidadeTO.obterCadastroCondicaoComercialServicoOportunidadeNovo> _listDescontos = new List<CadastroCondicaoComercialServicoOportunidadeTO.obterCadastroCondicaoComercialServicoOportunidadeNovo>();

                    objBuscaServDesc.CODOPTVND = _codoportunidade;
                    objBuscaServDesc.CODSVCTGV = itemServico.CODSVCTGV;


                    _listDescontos = DAL.obterCadastroCondicaoComercialServicoOportunidade(objBuscaServDesc);
                    _codigoServico = itemServico.CODSVCTGV;
                    objValoresServico.CODSVCTGV = itemServico.CODSVCTGV;
                    objValoresServico.CODOPTVND = itemServico.CODOPTVND;
                    _servicovalores = RetornarVlorServico(objValoresServico);

                    _servicos.AppendFormat(_head.ToString(), _servicovalores.CODSVCTGV, _servicovalores.DESSVCTGV, _servicovalores.VLRSVC.ToString("n2"));
                    //}

                    foreach (CadastroCondicaoComercialServicoOportunidadeTO.obterCadastroCondicaoComercialServicoOportunidadeNovo _item in _listDescontos)
                    {

                        //forechdesconteo
                        //{0} TipoDesconto, {1} nome desconto, {2} viculo cesta produto, {3} percentual desconto, {4} valor desconto, {5} data ou mes vigencia, {6} data fim , ou mes fim vigencia    
                        if (_item.CODCNDCMC >= 9000)
                            _tipodesconto = "Manual";
                        else
                            _tipodesconto = "Padrão";

                        _datainicio = "";
                        _datafim = "";
                        if (_item.INDTIPVGR == 1 || _item.INDTIPVGR == 0)
                        {
                            if (_item.DATINIVLD != null)
                            {
                                _datainicio = DateTime.Parse(_item.DATINIVLD).ToShortDateString();
                            }

                            if (_item.DATFIMVLD != null)
                            {
                                _datafim = DateTime.Parse(_item.DATFIMVLD).ToShortDateString();
                            }

                        }
                        else
                        {
                            if (_item.NUMMESINIVLD != null)
                            {
                                if (_item.NUMMESINIVLD != "0")
                                {

                                    _datainicio = _item.NUMMESINIVLD.ToString() + "º Mês";
                                }
                            }

                            if (_item.NUMMESFIMVLD != null)
                            {
                                if (_item.NUMMESFIMVLD != "0")
                                {
                                    _datafim = _item.NUMMESFIMVLD.ToString() + "º Mês";
                                }
                            }

                        }


                        _servicos.AppendFormat(_body.ToString(), _tipodesconto, _item.DESCNDCMC, _item.CESTPROD, _item.PERDSC.ToString("n2"), _item.VLRDSC.ToString("n2"), _datainicio, _datafim);
                    }

                    _servicos.Append("</tbody></table><br/>");
                }
                else
                {
                    _itemservicoperdido.CODSVCTGV = _itemservico.CODSVCTGV;
                    _itemservicoperdido.CODOPTVND = _itemservico.CODOPTVND;
                    _itemservicoperdido.INDSTANGC = 1;
                    _itemservicoperdido.INDSVCVND = 2; // nao vendido 

                    dalservicooportunit.alterarStatusServicoOportunidade(_itemservico);
                    _itemservicoperdido = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
                }
            }
        }

        return _servicos.ToString();
    }



    public bool InserirContratosBanco()
    {

        string _caminhocontratoscliente = ConfigurationManager.AppSettings["caminhoContratoClienteAceite"];
        List<CadastroOportunidadeTO.servicosContratos> _listservicosContratos;

        var dal = new CadastroOportunidadeDAL();
        var dalserv = new CadastroServicoOportunidadeDAL();

        string destFile;
        Byte[] data;
        String filedoc;

        try
        {
            _listservicosContratos = dal.obterServicoContratos();
            //var datareader = dal.obterServicoContratoss();

            foreach (CadastroOportunidadeTO.servicosContratos item in _listservicosContratos)
            {

                if (item.NOMARQORI.Trim() != "")
                {
                    if (item.CDOANXCTTCLI.Trim() == "")
                    {
                        destFile = _caminhocontratoscliente + "\\" + item.NOMARQORI.Trim();

                        FileInfo _filepdf2 = new FileInfo(@destFile);
                        if (_filepdf2.Exists)
                        {
                            data = File.ReadAllBytes(destFile);
                            filedoc = Convert.ToBase64String(data);

                            //salvarbanco 
                            dalserv.alterarpdftesteServicoOportunidade(item.CODOPTVND, item.CODSVCTGV, filedoc);


                            _filepdf2.Delete();
                        }
                    }
                }
            }



            return true;

        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO IMPORTAR CONTRATOS");
            Utilitario.InsereLog(ex, "InserirContratosBanco", "", "CadastroOportunidadeBLL", "ERRO AO IMPORTAR CONTRATOS");
            throw ex;
        }
    }


    public string VisualizarContrato(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        List<OportunidadeContatoTO.obterServico> _list_servicoscliente;
        var DALOPTCONT = new OportunidadeContatoDAL();


        string _servidor = HttpContext.Current.Server.MapPath("~");
        string base64 = "";

        try
        {
            _list_servicoscliente = DALOPTCONT.obterServico(objAlterar.CODOPTVND, objAlterar.CODSVCTGV);
            foreach (OportunidadeContatoTO.obterServico item in _list_servicoscliente)
            {
                if (item.TEMPLATESERVICO != "")
                {
                    base64 = item.NOMARQORI + "; " + item.TEMPLATESERVICO;
                }
            }

            return base64;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO RENVIAR O EMAIL .");
            Utilitario.InsereLog(ex, "RenviarEmailServicoOportunidade", "CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar", "CadastroOportunidadeBLL", "ERRO AO REENVIAR O EMAIL.");
            throw;
        }
    }

    public bool ConcluirCadastroOportunidade(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        var dalservicooportunit = new CadastroServicoOportunidadeDAL();
        var _itemservico = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
        List<OportunidadeContatoTO.obterServico> _list_servicoscliente;

        var DALOPTCONT = new OportunidadeContatoDAL();

        try
        {

            var marcados = objAlterar.LISTSERVICOS;

            _list_servicoscliente = DALOPTCONT.obterServicos(objAlterar.CODOPTVND);

            //if(_list_servicoscliente.Count > 0)
            //{
                foreach (OportunidadeContatoTO.obterServico item in _list_servicoscliente)
                {
                    if (marcados.Exists(x => x == item.CODSVCTGV))
                    {
                        _itemservico.CODSVCTGV = item.CODSVCTGV;
                        _itemservico.CODOPTVND = objAlterar.CODOPTVND;
                        _itemservico.INDSTANGC = objAlterar.INDSTANGC;
                        _itemservico.INDSVCVND = 1; //vendido 

                        dalservicooportunit.alterarStatusServicoOportunidadeSemEnvio(_itemservico);
                        _itemservico = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
                    }
                    else
                    {
                        _itemservico.CODSVCTGV = item.CODSVCTGV;
                        _itemservico.CODOPTVND = objAlterar.CODOPTVND;
                        _itemservico.INDSTANGC = objAlterar.INDSTANGC;
                        _itemservico.INDSVCVND = 2; //nao vendido

                        dalservicooportunit.alterarStatusServicoOportunidade(_itemservico);
                        _itemservico = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
                    }
                }

            //}
            //else
            //{
            //    _itemservico.CODSVCTGV = objAlterar.CODSVCTGV;
            //    _itemservico.CODOPTVND = objAlterar.CODOPTVND;
            //    _itemservico.INDSTANGC = objAlterar.INDSTANGC;
            //    _itemservico.INDSVCVND = 2; //nao vendido
            //    dalservicooportunit.alterarStatusServicoOportunidade(_itemservico);
            //    _itemservico = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
            //}
            


            int _codigofunc = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;

            DateTime _datainsercao = DateTime.Now;
            string _horas = _datainsercao.Hour.ToString() + " : " + _datainsercao.Minute.ToString();


            objAlterar.INDSTAOPTVND = 3;
            objAlterar.CODFNC = _codigofunc;

            return new CadastroOportunidadeDAL().alterarCadastroOportunidadeGrupoEconomico(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO VERIFICAR EXIXTE TEMPLATE");
            Utilitario.InsereLog(ex, "VerificaExisteTemplate", "_codigoservico", "CadastroServicoOportunidadeBLL", "ERRO AO VERIFICAR EXIXTE TEMPLATE.");
            throw ex;
        }

    }



    public bool AtualizaGrupoServico(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir)
    {

        try
        {
            return new CadastroOportunidadeDAL().AtualizaGrupoServico(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO ATUALIZAR GRUPO DE FATURAMETO");
            Utilitario.InsereLog(ex, "AtualizaGrupoServico", "CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir", "CadastroServicoOportunidadeBLL", "ERRO AO ATUALIZAR GRUPO DE FATURAMETO");
            throw ex;
        }

    }


    public bool AtualizaGrupoNegocicaoServico(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir)
    {

        try
        {
            return new CadastroOportunidadeDAL().AtualizaGrupoNegocicaoServico(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO ATUALIZAR GRUPO DE FATURAMETO DA NEGOCIAÇÃO");
            Utilitario.InsereLog(ex, "AtualizaGrupoNegocicaoServico", "CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir", "CadastroServicoOportunidadeBLL", "ERRO AO ATUALIZAR GRUPO DE FATURAMETO DA NEGOCIAÇÃO");
            throw ex;
        }

    }


    public bool AtualizaAllGrupoNegocicaoServico(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir)
    {

        try
        {
            return new CadastroOportunidadeDAL().AtualizaAllGrupoNegocicaoServico(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO ATUALIZAR DOS OS SERVIÇOS DO GRUPO DE FATURAMETO DA NEGOCIAÇÃO");
            Utilitario.InsereLog(ex, "AtualizaGrupoNegocicaoServico", "CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir", "CadastroServicoOportunidadeBLL", "ERRO AO ATUALIZAR DOS OS SERVIÇOS DO GRUPO DE FATURAMETO DA NEGOCIAÇÃO");
            throw ex;
        }

    }


    public bool AlterarOportunidadeIndicadoPor(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objEditar)
    {
        try
        {
            return new CadastroOportunidadeDAL().AlterarOportunidadeIndicadoPor(objEditar);

        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO ATUALIZAR INDICADO POR DA NEGOCIAÇÃO");
            Utilitario.InsereLog(ex, "AlterarOportunidadeIndicadoPor", "CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir", "CadastroOportunidadeBLL", "ERRO AO ATUALIZAR INDOICADO POR DA NEGOCIAÇÃO");
            throw ex;
        }

    }

    public bool AlterarOportunidadeOrigemIndicacao(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objEditar)
    {
        try
        {
            return new CadastroOportunidadeDAL().AlterarOportunidadeOrigemIndicacao(objEditar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO ATUALIZAR INDOICADO POR DA NEGOCIAÇÃO");
            Utilitario.InsereLog(ex, "AlterarOportunidadeOrigemIndicacao", "CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir", "CadastroOportunidadeBLL", "ERRO AO ATUALIZAR INDICADO POR DA NEGOCIAÇÃO");
            throw ex;
        }

    }

    public bool ForcarVendaGanhaDocumentosOportunidade(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        var dalservicooportunit = new CadastroServicoOportunidadeDAL();
        var _itemservico = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
        var _atulizouservico = false;
        var _atulizarOportunidade = false;

        try
        {
            _itemservico.CODOPTVND = objAlterar.CODOPTVND;
            /*
              WHEN SV.INDSTANGC = 2 THEN 'VENDA GANHA' 
                                  WHEN SV.INDSTANGC = 3 THEN 'VENDA PERDIDA' 
                                  WHEN SV.INDSTANGC = 4 THEN 'ABRIR SITE SURVEY' 
                                  WHEN SV.INDSTANGC = 5 THEN 'AGUARDANDO APROVAÇÃO DE DESCONTO' 
                                  WHEN SV.INDSTANGC = 6 THEN 'AGUARDANDO ACEITE DE CONTRATO' 
                                  WHEN SV.INDSTANGC = 7 THEN 'AGUARDANDO ASS. DE DOCUMENTO' END NOMINDSTANGC,
            */
            _itemservico.INDSTANGC = 2;
            /*
            INDSVCVND = 1; //vendido 
            INDSVCVND = 2; //não vendido
            */
            _itemservico.INDSVCVND = 1;

            _atulizouservico = dalservicooportunit.alterarDocsStatusServicoOportunidade(_itemservico);
            if (_atulizouservico)
            {
                int _codigofunc = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
                /*
                 *    WHEN INDSTAOPTVND = 1 THEN 'EM NEGOCIAÇÃO'
                      WHEN INDSTAOPTVND = 2 THEN 'CONTRATO ENVIADO'
                      WHEN INDSTAOPTVND = 3 THEN 'VENDA GANHA'
                      WHEN INDSTAOPTVND = 4 THEN 'VENDA PERDIDA',

                      WHEN INDSTAOPTVND = 5 THEN 'AGUARDANDO APROVAÇÃO DE DESCONTO',
                      WHEN INDSTAOPTVND = 6 THEN 'DESCONTO APROVADO' ,
                      WHEN INDSTAOPTVND = 7 THEN 'DESCONTO REJEITADO',
                      WHEN INDSTAOPTVND = 8 THEN 'AGUARDANDO DOCUMENTAÇÃO' END STATUS,
                */
                objAlterar.INDSTAOPTVND = 3;
                objAlterar.CODFNC = _codigofunc;


                _atulizarOportunidade = new CadastroOportunidadeDAL().alterarCadastroOportunidadeDocs(objAlterar);
            }

            return _atulizarOportunidade;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO ATUALIZAR VENDA GANHA NA ABA DE CONCILIAÇÃO");
            Utilitario.InsereLog(ex, "AlterarOportunidadeOrigemIndicacao", "CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir", "CadastroOportunidadeBLL", "ERRO AO ATUALIZAR VENDA GANHA NA ABA DE CONCILIAÇÃO");
            throw ex;
        }

    }

    public List<CadastroOportunidadeTO.obterConsultaContrato> obterContratos(int CODOPT)
    {
        var DALSQL = new CadastroOportunidadeDAL();
        return DALSQL.obterContratos(CODOPT);
    }

    public bool alterarStatusOportunidade(int CODOPT, int INDSTAOPTVND)
    {
        var DALSQL = new CadastroOportunidadeDAL();
        return DALSQL.alterarStatusOportunidade(CODOPT, INDSTAOPTVND);
    }

}


