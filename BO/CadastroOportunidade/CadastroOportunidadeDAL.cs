﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Data;


public class CadastroOportunidadeDAL : DAL
{
    public List<CadastroOportunidadeTO.obterCadastroOportunidade> obterCadastroOportunidade(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objPesquisar)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();
        string cmdSql = DALSQL.obterCadastroOportunidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (objPesquisar.CODOPTVND == 0)
        {
            objPesquisar.CODOPTVND = -1;
        }
        if (objPesquisar.NUMCGCCLI == "-1")
        {
            objPesquisar.NUMCGCCLI = null;
        }
        string NUMCGCCLI;

        if (objPesquisar.NUMCGCCLI != null)
        {
            NUMCGCCLI = Regex.Replace(objPesquisar.NUMCGCCLI, "[/.-]", String.Empty);
        }
        else
        {
            NUMCGCCLI = null;
        }

        if (objPesquisar.NUMCGCCLIFIL == "-1")
        {
            objPesquisar.NUMCGCCLIFIL = null;
        }
        string NUMCGCCLIFIL;

        if (objPesquisar.NUMCGCCLIFIL != null)
        {
            NUMCGCCLIFIL = Regex.Replace(objPesquisar.NUMCGCCLIFIL, "[/.-]", String.Empty);
        }
        else
        {
            NUMCGCCLIFIL = null;
        }

        dbCommand.AddWithValue("CODOPTVND", objPesquisar.CODOPTVND);
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("NUMCGCCLI", NUMCGCCLI);
        dbCommand.AddWithValue("CODCLIFIL", objPesquisar.CODCLIFIL);
        dbCommand.AddWithValue("NUMCGCCLIFIL", NUMCGCCLIFIL);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroOportunidadeTO.obterCadastroOportunidade>();
    }

    public CadastroOportunidadeTO.obterCadastroOportunidadeCliente obterCadastroOportunidadeCliente(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objPesquisa)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();
        string cmdSql = DALSQL.obterCadastroOportunidadeCliente();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        dbCommand.AddWithValue("CODCLI", objPesquisa.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        var retorno = MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroOportunidadeTO.obterCadastroOportunidadeCliente>();

        if (retorno.Count > 0)
        {
            return retorno.FirstOrDefault();
        }
        else
        {
            return new CadastroOportunidadeTO.obterCadastroOportunidadeCliente();
        }
    }

    public CadastroOportunidadeTO.obterMaximoOportunidade obterMaximoOportunidade()
    {
        var DALSQL = new CadastroOportunidadeDALSQL();
        string cmdSql = DALSQL.obterMaximoOportunidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        var retorno = MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroOportunidadeTO.obterMaximoOportunidade>();
        if (retorno.Count > 0)
        {
            return retorno.FirstOrDefault();
        }
        else
        {
            return new CadastroOportunidadeTO.obterMaximoOportunidade();
        }
    }

    public bool inserirCadastroOportunidade(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();
        string cmdSql = DALSQL.inserirCadastroOportunidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //                    
        objInserir.CODFNCCAD = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //        
        string NUMTLFCTO;
        //
        if (objInserir.NUMTLFCTO != null)
        {
            NUMTLFCTO = Regex.Replace(objInserir.NUMTLFCTO, "[-)( ]", String.Empty);
        }
        else
        {
            NUMTLFCTO = null;
        }
        //
        string NUMTLFCEL;
        if (objInserir.NUMTLFCEL != null)
        {
            NUMTLFCEL = Regex.Replace(objInserir.NUMTLFCEL, "[-)( ]", String.Empty);
        }
        else
        {
            NUMTLFCEL = null;
        }
        //
        dbCommand.AddWithValue("CODCLI", objInserir.CODCLI);
        dbCommand.AddWithValue("CODCNIVNDTGV", objInserir.CODCNIVNDTGV);
        dbCommand.AddWithValue("NUMSEQPESIND", objInserir.NUMSEQPESIND);
        dbCommand.AddWithValue("CODCNLORICLI", objInserir.CODCNLORICLI);
        dbCommand.AddWithValue("NUMTLFCTO", NUMTLFCTO);
        dbCommand.AddWithValue("NUMTLFCEL", NUMTLFCEL);
        dbCommand.AddWithValue("NOMCTORPNCLI", objInserir.NOMCTORPNCLI);
        dbCommand.AddWithValue("DESENDETNRPNCLI", objInserir.DESENDETNRPNCLI);
        dbCommand.AddWithValue("CODFNCCAD", objInserir.CODFNCCAD);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }
    
    public bool inserirCadastroOportunidadeImport(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();
        string cmdSql = DALSQL.inserirCadastroOportunidadeImport();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //                    
        objInserir.CODFNCCAD = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //        
        dbCommand.AddWithValue("CODCLI", objInserir.CODCLI);        
        dbCommand.AddWithValue("CODOPTVND", objInserir.CODOPTVND);
        dbCommand.AddWithValue("CODCNIVNDTGV", objInserir.CODCNIVNDTGV);
        dbCommand.AddWithValue("NUMSEQPESIND", objInserir.NUMSEQPESIND);
        dbCommand.AddWithValue("CODCNLORICLI", objInserir.CODCNLORICLI);
        dbCommand.AddWithValue("NUMTLFCTO", objInserir.NUMTLFCTO);
        dbCommand.AddWithValue("NUMTLFCEL", objInserir.NUMTLFCEL);
        dbCommand.AddWithValue("NOMCTORPNCLI", objInserir.NOMCTORPNCLI);
        dbCommand.AddWithValue("DESENDETNRPNCLI", objInserir.DESENDETNRPNCLI);
        dbCommand.AddWithValue("CODFNCCAD", objInserir.CODFNCCAD);
        //dbCommand.AddWithValue("CODSVCTGV", objInserir.CODSVCTGV);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool inserirCadastroServicoOportunidadeNovo(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();

        string cmdSql = DALSQL.inserirCadastroServicoOportunidadeNovo(objInserir.LISTCODSVCTGV);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("VLRSVC", objInserir.VLRSVC);
        dbCommand.AddWithValue("CODSVCTGV", objInserir.CODSVCTGV);
        dbCommand.AddWithValue("VLRDSC", objInserir.VLRDSC);
        dbCommand.AddWithValue("INDSTANGC", objInserir.INDSTANGC);
        dbCommand.AddWithValue("INDSTADSC", objInserir.INDSTADSC);
        dbCommand.AddWithValue("PERDSC", objInserir.PERDSC);
        dbCommand.AddWithValue("QDEPRDVND", objInserir.QDEPRDVND);


        if (objInserir.CODGRPEMPFAT != 0)
            dbCommand.AddWithValue("CODGRPEMPFAT", objInserir.CODGRPEMPFAT);
        else
            dbCommand.AddWithValue("CODGRPEMPFAT", null);


        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool alterarCadastroOportunidade(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();
        int _cancelamento = 0;
        int _obsdesc = 0;

        if (objAlterar.DESOBSDSC != null)
            if (objAlterar.DESOBSDSC.Trim() != "")
                _obsdesc = 1;


        if (objAlterar.DESMTVCNC != null)
            if (objAlterar.DESMTVCNC.Trim() != "")
                _cancelamento = 1;


        string cmdSql = DALSQL.alterarCadastroOportunidade(_cancelamento, _obsdesc);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("CODOPTVND", objAlterar.CODOPTVND);
        dbCommand.AddWithValue("INDSTAOPTVND", objAlterar.INDSTAOPTVND);

        if (_obsdesc == 1)
            dbCommand.AddWithValue("DESOBSDSC", objAlterar.DESOBSDSC);

        
        if (_cancelamento == 1) { 
            dbCommand.AddWithValue("DESMTVCNC", objAlterar.DESMTVCNC);
            dbCommand.AddWithValue("CODFNCCNC", objAlterar.CODFNC);
            dbCommand.AddWithValue("CODMTVCNC", objAlterar.CODMTVCNC);

        }
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool inserirContatoPadraoOportunidade(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();
        string cmdSql = DALSQL.inserirContatoPadraoOportunidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objInserir.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        objInserir.CODFNCCAD = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;

        string NUMTLFCEL;

        if (objInserir.NUMTLFCEL != null)
        {
            NUMTLFCEL = Regex.Replace(objInserir.NUMTLFCEL, "[-)( ]", String.Empty);
        }
        else
        {
            NUMTLFCEL = null;
        }

        string NUMTLFCTO;
        if (objInserir.NUMTLFCTO != null)
        {
            NUMTLFCTO = Regex.Replace(objInserir.NUMTLFCTO, "[-)( ]", String.Empty);
        }
        else
        {
            NUMTLFCTO = null;
        }
        //
        dbCommand.AddWithValue("CODEDE", objInserir.CODEDE);
        dbCommand.AddWithValue("NOMCTO", objInserir.NOMCTO);
        dbCommand.AddWithValue("DESENDETNCTO", objInserir.DESENDETNCTO);
        dbCommand.AddWithValue("NUMTLFCEL", NUMTLFCEL);
        dbCommand.AddWithValue("NUMTLFCTO", NUMTLFCTO);
        dbCommand.AddWithValue("CODFNCCAD", objInserir.CODFNCCAD);
        dbCommand.AddWithValue("CODFNCALT", objInserir.CODFNCALT);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public CadastroOportunidadeTO.obterClientes obterClientes(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objPesquisa)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();
        string cmdSql = DALSQL.obterClientes();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        dbCommand.AddWithValue("CODCLI", objPesquisa.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        var retorno = MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroOportunidadeTO.obterClientes>();
        if (retorno.Count == 1)
        {
            return retorno.FirstOrDefault();
        }
        else
        {
            return new CadastroOportunidadeTO.obterClientes();
        }
    }

    public CadastroOportunidadeTO.obterClientesCnpj obterClientesCnpj(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objPesquisar)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();
        string cmdSql = DALSQL.obterClientesCnpj();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (objPesquisar.NUMCGCCLI == "-1")
        {
            objPesquisar.NUMCGCCLI = null;
        }
        string NUMCGCCLI;

        if (objPesquisar.NUMCGCCLI != null)
        {
            NUMCGCCLI = Regex.Replace(objPesquisar.NUMCGCCLI, "[/.-]", String.Empty);
        }
        else
        {
            NUMCGCCLI = null;
        }
        //
        dbCommand.AddWithValue("NUMCGCCLI", NUMCGCCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        var retorno = MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroOportunidadeTO.obterClientesCnpj>();

        if (retorno.Count == 1)
        {
            return retorno.FirstOrDefault();
        }
        else
        {
            return new CadastroOportunidadeTO.obterClientesCnpj();
        }
    }

    public CadastroOportunidadeTO.obterConsultorDistribuicao obterConsultorDistribuicao(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objPesquisa)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();
        string cmdSql = DALSQL.obterConsultorDistribuicao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        var retorno = MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroOportunidadeTO.obterConsultorDistribuicao>();

        if (retorno.Count > 0)
        {
            return retorno.FirstOrDefault();
        }
        else
        {
            return new CadastroOportunidadeTO.obterConsultorDistribuicao();
        }

    }

    public bool alterarConsultorOportunidadeNovo(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();
        string cmdSql = DALSQL.alterarConsultorOportunidadeNovo();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        dbCommand.AddWithValue("CODCNIVNDTGV", objAlterar.CODCNIVNDTGV);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    //public bool alterarConsultorOportunidadeEditar(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    //{
    //    var DALSQL = new CadastroOportunidadeDALSQL();
    //    string cmdSql = DALSQL.alterarConsultorOportunidadeEditar();

    //    var dbCommand = MRT001.GetSqlStringCommand(cmdSql);

    //    dbCommand.AddWithValue("CODOPTVND", objAlterar.CODOPTVND);
    //    dbCommand.AddWithValue("CODCNIVNDTGV", objAlterar.CODCNIVNDTGV);

    //    dbCommand.TrataDbCommandUniversal();

    //    return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    //}

    public List<CadastroOportunidadeTO.servicosContratos> obterServicoContratos()
    {
        var DALSQL = new CadastroOportunidadeDALSQL();
        string cmdSql = DALSQL.obterServicoContratos();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroOportunidadeTO.servicosContratos>();
    }

    public IDataReader obterServicoContratoss()
    {
        var DALSQL = new CadastroOportunidadeDALSQL();
        string cmdSql = DALSQL.obterServicoContratos();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        return MRT001.ExecuteReader(dbCommand);
    }


    public bool alterarCadastroOportunidadeGrupoEconomico(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();


        string cmdSql = DALSQL.alterarCadastroOportunidadeGrupoEconomico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("CODOPTVND", objAlterar.CODOPTVND);
        dbCommand.AddWithValue("INDSTAOPTVND", objAlterar.INDSTAOPTVND);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    
    public bool AtualizaGrupoServico(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();

        string cmdSql = DALSQL.AtualizaGrupoServico(objInserir.CODGRPEMPFAT);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        if (objInserir.CODGRPEMPFAT > 0)
            dbCommand.AddWithValue("CODGRPEMPFAT", objInserir.CODGRPEMPFAT);


        dbCommand.AddWithValue("CODOPTVND", objInserir.CODOPTVND);
        dbCommand.AddWithValue("CODSVCTGV", objInserir.CODSVCTGV);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    
    public bool AtualizaGrupoNegocicaoServico(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();

        string cmdSql = DALSQL.AtualizaGrupoNegocicaoServico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("CODGRPEMPFAT", objInserir.CODGRPEMPFAT);
        dbCommand.AddWithValue("CODCLI", objInserir.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public bool AtualizaAllGrupoNegocicaoServico(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();

        string cmdSql = DALSQL.AtualizaAllGrupoNegocicaoServico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("CODGRPEMPFAT", objInserir.CODGRPEMPFAT);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public bool AtualizaApagaGrupoNegocicaoServico(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();

        string cmdSql = DALSQL.AtualizaApagaGrupoNegocicaoServico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("CODCLI", objInserir.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public bool AlterarOportunidadeIndicadoPor(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();

        string cmdSql = DALSQL.AlterarOportunidadeIndicadoPor();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("NUMSEQPESIND", objInserir.NUMSEQPESIND);
        dbCommand.AddWithValue("CODOPTVND", objInserir.CODOPTVND);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    
    public bool AlterarOportunidadeOrigemIndicacao(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();

        string cmdSql = DALSQL.AlterarOportunidadeOrigemIndicacao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("CODCNLORICLI", objInserir.CODCNLORICLI);
        dbCommand.AddWithValue("CODOPTVND", objInserir.CODOPTVND);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public List<CadastroOportunidadeTO.ServicosDocumento> BuscarDocumentosServicos(Int32 _codservico)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();
        string cmdSql = DALSQL.BuscarDocumentosServicos();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODSVCTGV", _codservico);
        //
        dbCommand.TrataDbCommandUniversal();
        //              
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroOportunidadeTO.ServicosDocumento>();
    }
    public List<CadastroOportunidadeTO.ServicosDocumentoNovo> BuscarDocumentosServicosNovo(List<long> LSTCODDOCARZ, List<long> LSTCODSVCTGV)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();
        string cmdSql = DALSQL.BuscarDocumentosServicosNovo(LSTCODDOCARZ, LSTCODSVCTGV);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal();
        //              
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroOportunidadeTO.ServicosDocumentoNovo>();
    }

    public bool inserirDocmentosBandeiraOportunidade(Int64 CODCLI, Int64 CODDOCARZ, Int64 CODOPTVND, Int64 CODSVCTGV, string CDOANXDOCARZ)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();

        string cmdSql = DALSQL.inserirDocmentosBandeiraOportunidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", CODCLI);
        dbCommand.AddWithValue("CODDOCARZ", CODDOCARZ);
        dbCommand.AddWithValue("CODOPTVND", CODOPTVND);
        dbCommand.AddWithValue("CODSVCTGV", CODSVCTGV);
        dbCommand.AddWithValue("CDOANXDOCARZ", CDOANXDOCARZ);
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool alterarCadastroOportunidadeDocs(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();

        string cmdSql = DALSQL.alterarCadastroOportunidadeDocs();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("CODOPTVND", objAlterar.CODOPTVND);
        dbCommand.AddWithValue("INDSTAOPTVND", objAlterar.INDSTAOPTVND);
        dbCommand.AddWithValue("CODFNCACT", objAlterar.CODFNC);
        dbCommand.AddWithValue("DESMTVAPV", objAlterar.DESMTVAPV);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public List<CadastroOportunidadeTO.obterConsultaContrato> obterContratos(int CODOPT)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();
        string cmdSql = DALSQL.obterContratos();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODOPTVND", CODOPT);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroOportunidadeTO.obterConsultaContrato>();
    }

    public bool alterarStatusOportunidade(int CODOPT, int INDSTAOPTVND)
    {
        var DALSQL = new CadastroOportunidadeDALSQL();
        string cmdSql = DALSQL.alterarStatusOportunidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);

        dbCommand.AddWithValue("CODOPTVND", CODOPT);
        dbCommand.AddWithValue("INDSTAOPTVND", INDSTAOPTVND);

        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }



}