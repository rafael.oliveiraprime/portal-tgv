﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class CadastroOportunidadeDALSQL
{
    public string obterCadastroOportunidade()
    {
        return @"
                 SELECT OPT.CODCLI, CLI.NOMCLI, OPT.CODOPTVND, OPT.CODCNIVNDTGV,
                 CASE WHEN OPT.CODCNLORICLI=1 THEN 'SITE (PORTAL)'
                 WHEN OPT.CODCNLORICLI=2 THEN 'TELEFONE'
                 WHEN OPT.CODCNLORICLI=3 THEN 'E-MAIL'
                 WHEN OPT.CODCNLORICLI=4 THEN 'WHATSAPP'
                 WHEN OPT.CODCNLORICLI=5 THEN 'PARCERIA' 
                 WHEN OPT.CODCNLORICLI=6 THEN 'CAMPANHA'
                 WHEN OPT.CODCNLORICLI=7 THEN 'OUTRO' 
                 WHEN OPT.CODCNLORICLI=8 THEN 'PROSPECT' 
                 WHEN OPT.CODCNLORICLI=9 THEN 'INDICAÇÃO PRÓPRIA' 
                 WHEN OPT.CODCNLORICLI=10 THEN 'INDICAÇÃO GERENTE TRIBANCO' 
                 WHEN OPT.CODCNLORICLI=11 THEN 'INDICAÇÃO GERENTE MARTINS'
                 WHEN OPT.CODCNLORICLI=12 THEN 'RCA'
                 WHEN OPT.CODCNLORICLI=13 THEN 'EX-SMART'
                 WHEN OPT.CODCNLORICLI=14 THEN 'INDICAÇÃO TRIBANCO - GERENCIADOR' 
                 WHEN OPT.CODCNLORICLI=15 THEN 'MIGRAÇÃO SERVIÇO' 
                 WHEN OPT.CODCNLORICLI=16 THEN 'INDICAÇÃO TGV - GERENCIADOR' 
                 WHEN OPT.CODCNLORICLI=17 THEN 'INDICAÇÃO PARCERIA' 
                 WHEN OPT.CODCNLORICLI=18 THEN 'IMPORT' 
                    END NOMCNLORICLI,
                 OPT.CODCNLORICLI,
                 CASE WHEN LENGTH(TRIM(OPT.NUMTLFCTO)) =11 THEN ( '(' || SUBSTR(OPT.NUMTLFCTO, 1, 2) || ')' || SUBSTR(OPT.NUMTLFCTO, 3, 5) || '-' ||  SUBSTR(OPT.NUMTLFCTO, 8, 4)) WHEN LENGTH(TRIM(OPT.NUMTLFCTO)) =10 THEN '(' || SUBSTR(OPT.NUMTLFCTO, 1, 2) || ') ' || SUBSTR(OPT.NUMTLFCTO, 3, 4) || '-' || SUBSTR(OPT.NUMTLFCTO, 7, 4) END NUMTLFCTO,
                 CASE WHEN LENGTH(TRIM(OPT.NUMTLFCEL)) =11 THEN ( '(' || SUBSTR(OPT.NUMTLFCEL, 1, 2) || ')' || SUBSTR(OPT.NUMTLFCEL, 3, 5) || '-' ||  SUBSTR(OPT.NUMTLFCEL, 8, 4)) WHEN LENGTH(TRIM(OPT.NUMTLFCEL)) =10 THEN '(' || SUBSTR(OPT.NUMTLFCEL, 1, 2) || ') ' || SUBSTR(OPT.NUMTLFCEL, 3, 4) || '-' || SUBSTR(OPT.NUMTLFCEL, 7, 4) END NUMTLFCEL,
                 OPT.NUMSEQPESIND, TRIM(IND.NOMPESCTO) AS NOMPESCTO, 
                 CASE WHEN INDSTAOPTVND = 1 THEN 'EM NEGOCIAÇÃO' 
                      WHEN INDSTAOPTVND = 2 THEN 'CONTRATO ENVIADO' 
                      WHEN INDSTAOPTVND = 3 THEN 'VENDA GANHA'                      
                      WHEN INDSTAOPTVND = 4 THEN 'VENDA PERDIDA',

                      WHEN INDSTAOPTVND = 5 THEN 'AGUARDANDO APROVAÇÃO DE DESCONTO',
                      WHEN INDSTAOPTVND = 6 THEN 'DESCONTO APROVADO' ,
                      WHEN INDSTAOPTVND = 7 THEN 'DESCONTO REJEITADO',
                      WHEN INDSTAOPTVND = 8 THEN 'AGUARDANDO DOCUMENTAÇÃO' END STATUS,

                 TRIM(FNC_CAD.NOMFNC) || ' em ' || OPT.DATCAD DATFNCCAD
                 FROM MRT.CADOPTVNDCLITGV OPT
                 LEFT JOIN MRT.T0100361 FNC_CAD ON FNC_CAD.CODFNC = OPT.CODFNCCAD
                 LEFT JOIN MRT.CADDDOPESINDTGV IND ON IND.NUMSEQPESIND = OPT.NUMSEQPESIND
                 LEFT JOIN MRT.T0100043 CLI ON CLI.CODCLI = OPT.CODCLI
                 WHERE OPT.INDSTAOPTVND = 1
                 AND CLI.NUMCGCCLI = :NUMCGCCLI
                 AND OPT.CODOPTVND = :CODOPTVND
                 AND OPT.CODCLI = :CODCLI
                 AND CLI.NUMCGCCLI = :NUMCGCCLIFIL                 
                 AND OPT.CODCLI = :CODCLIFIL
                ";
    }

    public string obterCadastroOportunidadeCliente()
    {
        return @"
                 SELECT OPT.CODCLI, CLI.NOMCLI           
                 FROM MRT.CADOPTVNDCLITGV OPT 
                 LEFT JOIN MRT.T0100043 CLI ON CLI.CODCLI = OPT.CODCLI
                 WHERE INDSTAOPTVND IN (1,2,5,6,7)
                 AND OPT.CODCLI = :CODCLI                  
                ";
    }

    public string obterMaximoOportunidade()
    {
        return @" SELECT COALESCE(MAX(CODOPTVND),0) AS CODOPTVND 
                               FROM MRT.CADOPTVNDCLITGV";
    }

    public string inserirCadastroOportunidade()
    {
        return @"
                 INSERT INTO MRT.CADOPTVNDCLITGV (CODCLI, CODOPTVND, CODCNIVNDTGV, NUMSEQPESIND, CODCNLORICLI, NUMTLFCTO, NUMTLFCEL, NOMCTORPNCLI, DESENDETNRPNCLI, INDSTAOPTVND, CODFNCCAD, DATCAD)
                 VALUES (:CODCLI, 
                 (SELECT COALESCE(MAX(CODOPTVND),0)+1 FROM MRT.CADOPTVNDCLITGV),
                 :CODCNIVNDTGV, :NUMSEQPESIND, :CODCNLORICLI, :NUMTLFCTO, :NUMTLFCEL,  UPPER(:NOMCTORPNCLI), LOWER(:DESENDETNRPNCLI), 1, :CODFNCCAD, SYSDATE)
                ";
    }

    public string inserirCadastroOportunidadeImport()
    {
        return @"
                 INSERT INTO MRT.CADOPTVNDCLITGV (CODCLI, CODOPTVND, CODCNIVNDTGV, NUMSEQPESIND, CODCNLORICLI, NUMTLFCTO, NUMTLFCEL, NOMCTORPNCLI, DESENDETNRPNCLI, INDSTAOPTVND, CODFNCCAD, DATCAD)
                 VALUES (:CODCLI, :CODOPTVND, :CODCNIVNDTGV, :NUMSEQPESIND, :CODCNLORICLI, :NUMTLFCTO, :NUMTLFCEL,  UPPER(:NOMCTORPNCLI), LOWER(:DESENDETNRPNCLI), 1, :CODFNCCAD, SYSDATE)
                ";
    }

    public string inserirCadastroServicoOportunidadeNovo(List<long> LISTCODSVCTGV)
    {

        StringBuilder strBld = new StringBuilder(@" INSERT INTO MRT.RLCOPTVNDSVCTGV (VLRSVC, CODOPTVND, CODSVCTGV, VLRDSC, INDSTANGC, INDSTADSC, PERDSC, QDEPRDVND, CODGRPEMPFAT) 
                                                                             VALUES (:VLRSVC, (SELECT COALESCE(MAX(CODOPTVND),0) FROM MRT.CADOPTVNDCLITGV),
                                                                                     :CODSVCTGV, :VLRDSC, :INDSTANGC, :INDSTADSC, :PERDSC, :QDEPRDVND, :CODGRPEMPFAT)");
        return strBld.ToString();
    }

    public string alterarCadastroOportunidade(int _cancelamento, int observacadesc)
    {
        StringBuilder strBld;

        if (observacadesc == 1)
        {
            if (_cancelamento == 1)
            {
                strBld = new StringBuilder(@"
                 UPDATE MRT.CADOPTVNDCLITGV SET
                                                 INDSTAOPTVND = :INDSTAOPTVND,
                                                 DESMTVCNC = :DESMTVCNC,
                                                 CODFNCCNC = :CODFNCCNC,
                                                 DATFIMNGCOPT = SYSDATE,    
                                                 DESOBSDSC = :DESOBSDSC,
                                                 DATCNC = SYSDATE,
                                                 CODMTVCNC = :CODMTVCNC 
                 WHERE CODOPTVND = :CODOPTVND");
            }
            else
            {
                strBld = new StringBuilder(@"
                 UPDATE MRT.CADOPTVNDCLITGV SET
                 INDSTAOPTVND = :INDSTAOPTVND,
                 DESOBSDSC = :DESOBSDSC
                 WHERE CODOPTVND = :CODOPTVND");
            }


        }
        else
        {
            if (_cancelamento == 1)
            {
                strBld = new StringBuilder(@"
                 UPDATE MRT.CADOPTVNDCLITGV SET
                                                 INDSTAOPTVND = :INDSTAOPTVND,
                                                 DESMTVCNC = :DESMTVCNC,
                                                 CODFNCCNC = :CODFNCCNC,
                                                 DATFIMNGCOPT = SYSDATE,           
                                                 DATCNC = SYSDATE,
                                                 CODMTVCNC = :CODMTVCNC
                 WHERE CODOPTVND = :CODOPTVND");
            }
            else
            {
                strBld = new StringBuilder(@"
                 UPDATE MRT.CADOPTVNDCLITGV SET
                 INDSTAOPTVND = :INDSTAOPTVND
                 WHERE CODOPTVND = :CODOPTVND");
            }
        }

        return strBld.ToString();
    }

    public string inserirContatoPadraoOportunidade()
    {
        return @"
                 INSERT INTO MRT.CADCTOTGV (CODCTO, TIPEDE, CODEDE, CODTIPCTO, NOMCTO, DESENDETNCTO, NUMTLFCEL, NUMTLFCTO, CODFNCCAD, DATCAD, CODFNCALT, DATALT)
                 VALUES ((SELECT COALESCE(MAX(CODCTO),0)+1 FROM MRT.CADCTOTGV),
                 0, :CODEDE, 8, UPPER(:NOMCTO), LOWER(:DESENDETNCTO), :NUMTLFCEL, 
                 :NUMTLFCTO, :CODFNCCAD, SYSDATE, :CODFNCALT, SYSDATE)
                ";
    }

    public string obterClientes()
    {
        return @"     SELECT CLI.CODCLI, TRIM(NOMCLI) AS NOMCLI, 
                             CASE NUMCGCCLI WHEN NUMCGCCLI THEN (SUBSTR(NUMCGCCLI, 1, 2) || '.' || SUBSTR(NUMCGCCLI, 3, 3) || '.' || SUBSTR(NUMCGCCLI, 6, 3) || '/' || SUBSTR(NUMCGCCLI, 9,4) || '-' || SUBSTR(NUMCGCCLI, 13,2) ) ELSE NULL END NUMCGCCLI
                            ,GPR.CODGRPEMPCLI
                            ,GPRCAP.NOMGRPEMPCLI
                            ,(SELECT CODGRPEMPCLI FROM MRT.CADGRPEMPTGV CDGPR WHERE CDGPR.CODCLIGRPFAT = CLI.CODCLI) AS CODGRPEMPCLIPRINC
                            ,(SELECT NOMGRPEMPCLI FROM MRT.CADGRPEMPTGV CDGPR WHERE CDGPR.CODCLIGRPFAT = CLI.CODCLI) AS NOMGRPEMPCLIPRINC
                        FROM MRT.T0100043	CLI	                  
                        LEFT JOIN MRT.RLCCLIGRPEMPTGV GPR ON (CLI.CODCLI = GPR.CODCLI)
                        LEFT JOIN MRT.CADGRPEMPTGV GPRCAP ON (GPRCAP.CODGRPEMPCLI = GPR.CODGRPEMPCLI)      
                       WHERE 1 = 1                        
                         AND CLI.CODCLI = :CODCLI   
                       ORDER BY CLI.NOMCLI
                ";
    }

    public string obterClientesCnpj()
    {
        return @"
                  SELECT CODCLI, TRIM(NOMCLI) AS NOMCLI
                  FROM MRT.T0100043			                  
                  WHERE 1 = 1                      
                  AND NUMCGCCLI = :NUMCGCCLI                   
                ";
    }

    public string obterConsultorDistribuicao()
    {
        return @"
                    WITH ORDENACAO AS (SELECT CON.CODFNC
                            ,TRIM(FNC.NOMFNC) AS NOMFNC
                            ,(SELECT NVL(MAX(OPT.CODOPTVND), 0) FROM MRT.CADOPTVNDCLITGV OPT WHERE OPT.CODCNIVNDTGV = CON.CODFNC AND INDSTAOPTVND = 1 AND OPT.CODCNLORICLI IN(9, 10, 11, 14, 16, 17) ) ULTIMOCODIGO              
                        FROM MRT.CADFNCSISTGV CON 
                        LEFT JOIN MRT.T0100361 FNC ON FNC.CODFNC = CON.CODFNC 
                        WHERE DATDST IS NULL
                        AND CODPFLACS = 1
                        ORDER BY NOMFNC   
                        )
                        SELECT CODFNC, ULTIMOCODIGO FROM ORDENACAO ORDER BY ULTIMOCODIGO
                ";

        //return @"
        //          SELECT CPI.NUMSEQPESIND AS CODCNIVNDTGV, 
        //          (SELECT COUNT(OPT.CODCNIVNDTGV) FROM MRT.CADOPTVNDCLITGV OPT WHERE OPT.NUMSEQPESIND = CPI.NUMSEQPESIND) QUANTIDADE      
        //          FROM MRT.CADDDOPESINDTGV CPI 
        //          ORDER BY QUANTIDADE, CPI.DATCAD ASC       
        //        ";        
    }

    public string obterConsultorRandom()
    {
        return @"
                  SELECT CODCNIVNDTGV 
                  FROM MRT.CADOPTVNDCLITGV                                                 
                ";
    }

    public string alterarConsultorOportunidadeNovo()
    {
        return @"
                 UPDATE MRT.CADOPTVNDCLITGV SET
                 CODCNIVNDTGV = :CODCNIVNDTGV,                                 
                 INDSTAOPTVND = 1                                 
                 WHERE CODOPTVND = (SELECT COALESCE(MAX(CODOPTVND),0) FROM MRT.CADOPTVNDCLITGV)
                ";
    }

    //public string alterarConsultorOportunidadeEditar()
    //{
    //    return @"
    //             UPDATE MRT.CADOPTVNDCLITGV SET
    //             CODCNIVNDTGV = :CODCNIVNDTGV,                                 
    //             INDSTAOPTVND = 2                                 
    //             WHERE CODOPTVND = :CODOPTVND              
    //            ";
    //}

    public String obterServicoContratos()
    {
        StringBuilder strBld = new StringBuilder(@"
                                                             SELECT CODOPTVND,
                                                                    CODSVCTGV,
                                                                    nvl(CDOANXCTTCLI, ' ') AS CDOANXCTTCLI,      
                                                                    nvl(NOMARQORI, ' ') AS NOMARQORI
                                                                FROM MRT.RLCOPTVNDSVCTGV
                                                            WHERE indstangc <> 3
                                                              AND CDOANXCTTCLI IS NULL
                                                            ORDER BY CODOPTVND, CODSVCTGV

                                                ");

        return strBld.ToString();
    }


    public string alterarCadastroOportunidadeGrupoEconomico()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@"
            UPDATE MRT.CADOPTVNDCLITGV SET INDSTAOPTVND = :INDSTAOPTVND,
                                           DATFIMNGCOPT = SYSDATE 
             WHERE CODOPTVND = :CODOPTVND");

        return strBld.ToString();
    }


    public string AtualizaGrupoServico(int codgrupo)
    {        
        StringBuilder strBld;

        if (codgrupo > 0)
        {
            strBld = new StringBuilder(@" UPDATE MRT.RLCOPTVNDSVCTGV SET CODGRPEMPFAT = :CODGRPEMPFAT
                                       WHERE CODOPTVND = :CODOPTVND 
                                         AND CODSVCTGV = :CODSVCTGV ");
        }
        else
        {
            strBld = new StringBuilder(@" UPDATE MRT.RLCOPTVNDSVCTGV SET CODGRPEMPFAT = null
                                       WHERE CODOPTVND = :CODOPTVND 
                                         AND CODSVCTGV = :CODSVCTGV ");
        }


        return strBld.ToString();
    }


    public string AtualizaGrupoNegocicaoServico()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" UPDATE MRT.RLCOPTVNDSVCTGV SET CODGRPEMPFAT = :CODGRPEMPFAT
                                       WHERE CODOPTVND = (SELECT CODOPTVND FROM MRT.CADOPTVNDCLITGV WHERE CODCLI = :CODCLI AND INDSTAOPTVND NOT IN(3, 4) )
                                   ");
        return strBld.ToString();
    }

    public string AtualizaAllGrupoNegocicaoServico()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" UPDATE MRT.RLCOPTVNDSVCTGV SET CODGRPEMPFAT = null
                                       WHERE CODGRPEMPFAT = :CODGRPEMPFAT
                                   ");

        return strBld.ToString();
    }

    public string AtualizaApagaGrupoNegocicaoServico()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" UPDATE MRT.RLCOPTVNDSVCTGV SET CODGRPEMPFAT = null
                                       WHERE CODOPTVND = (SELECT CODOPTVND FROM MRT.CADOPTVNDCLITGV WHERE CODCLI = :CODCLI AND INDSTAOPTVND NOT IN(3, 4) )
                                   ");
        return strBld.ToString();
    }


    public string alterarConsultorOportunidadeEditar()
    {
        return @"
                 UPDATE MRT.CADOPTVNDCLITGV SET
                 CODCNIVNDTGV = :CODCNIVNDTGV,                                 
                 INDSTAOPTVND = 2                                 
                 WHERE CODOPTVND = :CODOPTVND              
                ";
    }

    public string AlterarOportunidadeIndicadoPor()
    {
        return @"
                 UPDATE MRT.CADOPTVNDCLITGV SET NUMSEQPESIND = :NUMSEQPESIND
                  WHERE CODOPTVND = :CODOPTVND            
                ";

    }


    public string AlterarOportunidadeOrigemIndicacao()
    {
        return @"
                 UPDATE MRT.CADOPTVNDCLITGV SET CODCNLORICLI = :CODCNLORICLI
                  WHERE CODOPTVND = :CODOPTVND            
                ";

    }


    public string BuscarDocumentosServicos()
    {
        return @" SELECT RL.CODDOCARZ, DOC.CDOANXDOCARZ, DOC.INDENV 
                    FROM MRT.RLCSVCDOCTGV RL
                    INNER JOIN MRT.CADDOCARZCLITGV DOC ON (DOC.CODDOCARZ = RL.CODDOCARZ)
                   WHERE 1 = 1
                    AND RL.CODSVCTGV = :CODSVCTGV        
                ";

    }

    public string BuscarDocumentosServicosNovo(List<long> LSTCODDOCARZ, List<long> LSTCODSVCTGV)
    {
        StringBuilder strBld;
        strBld = new StringBuilder(@" SELECT RL.CODDOCARZ, DOC.CDOANXDOCARZ, DOC.INDENV, SVC.DESSVCTGV , SVC.CODSVCTGV
                    FROM MRT.RLCSVCDOCTGV RL
                    INNER JOIN MRT.CADDOCARZCLITGV DOC ON (DOC.CODDOCARZ = RL.CODDOCARZ)
                    INNER JOIN MRT.CADSVCTGV SVC ON SVC.CODSVCTGV = RL.CODSVCTGV
                   WHERE 1 = 1
                    AND RL.CODSVCTGV IN (#LSTCODSVCTGV)
                    AND RL.CODDOCARZ IN (#LSTCODDOCARZ)
                ".Replace("#LSTCODDOCARZ", ToList(LSTCODDOCARZ))
                 .Replace("#LSTCODSVCTGV", ToList(LSTCODSVCTGV))
                 );
        return strBld.ToString();
    }
    public string ToList(List<long> LISTCODSVCTGV)
    {
        string retr = string.Empty;
        bool firstTime = true;

        foreach (long Item in LISTCODSVCTGV)
        {
            if (firstTime)
            {
                firstTime = false;

                retr = (Item.ToString());
            }
            else
            {
                retr = (string.Format("{0}, {1}", retr, Item));
            }

        }

        return retr.Trim();
    }

    public string inserirDocmentosBandeiraOportunidade()
    {
        return @" INSERT INTO MRT.RLCSVCDOCARZTGV (CODCLI, CODDOCARZ, CODOPTVND, CODSVCTGV, DATENVDOC, INDSTAAPV, CDOANXDOCARZ)
                                           VALUES (:CODCLI, :CODDOCARZ, :CODOPTVND, :CODSVCTGV, SYSDATE, 1, :CDOANXDOCARZ)
                ";
    }

    public string alterarCadastroOportunidadeDocs()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@"
            UPDATE MRT.CADOPTVNDCLITGV SET
                                            INDSTAOPTVND = :INDSTAOPTVND,
                                            DESMTVAPV = :DESMTVAPV,
                                            CODFNCCNC = :CODFNCACT,
                                            DATFIMNGCOPT = SYSDATE,
                                            DATHRAFNCACT = SYSDATE
            WHERE CODOPTVND = :CODOPTVND");

        return strBld.ToString();
    }


    public string obterContratos()
    {
        StringBuilder strBld = new StringBuilder(@"
                                                        SELECT CON.CODOPTVND, CON.CODSVCTGV, SER.DESSVCTGV, NVL(CON.NOMARQORI, ' teste ') AS NOMARQORI, NVL(CON.DATENVDOC, '') AS DATENVDOC, 
                                                               NVL(CON.DATACEDOC, '') AS DATACEDOC, CON.CDOANXCTTCLI, 0 AS INDSTAOPTVND
                                                            FROM MRT.RLCOPTVNDSVCTGV CON
                                                            INNER JOIN MRT.CADSVCTGV SER ON (CON.CODSVCTGV = SER.CODSVCTGV) 
                                                            WHERE CODOPTVND = :CODOPTVND
                                                              AND INDSTANGC in (2, 6) 
                                                  ");
        return strBld.ToString();
    }

    public string alterarStatusOportunidade()
    {
        return @"
                 UPDATE MRT.CADOPTVNDCLITGV SET INDSTAOPTVND = :INDSTAOPTVND,
                        DATFIMNGCOPT = SYSDATE
                   WHERE CODOPTVND = :CODOPTVND
                ";
    }

}