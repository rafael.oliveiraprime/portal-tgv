﻿using System.Collections.Generic;

namespace AnexoEmailTO
{
    public class AnexoEmail
    {
        public byte[] FileByte { get; set; }
        public string FileName { get; set; }
    }
}