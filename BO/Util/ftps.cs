﻿using Arquitetura.ControllerCommon.Ftps;
using Chilkat;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;


namespace Vendas.TGV.Portal.BO.Util
{
    public class Ftps
    {
        public string ChilkatUnlockCode()
        {
            ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
            fileMap.ExeConfigFilename = Constants.PathUnlock;
            Configuration config = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
            AppSettingsSection appSettings = config.AppSettings;
            string key = appSettings.Settings[Constants.ChilkatUnlockKey].Value;
            return key;
        }

        public string SimpleFTPDownload(string hostName, string userName, string password, string filePath, string fileName, List<string> hostPath = null)
        {
            Ftp2 ftp = new Ftp2();

            bool success;

            //  Any string unlocks the component.
            if (Constants.ModoDesenvolvimento.Equals("1"))
                success = ftp.UnlockComponent("Anything for 30-day trial");
            else
                success = ftp.UnlockComponent(ChilkatUnlockCode());
            if (!success)
                return "ERRO REALIZAR UNLOCK: " + ftp.LastErrorText;

            //  If this example does not work, try using passive mode
            //  by setting this to true.
            //TODO producao mudar para true(ativo)
            ftp.Passive = Constants.FtpPassive;

            ftp.Hostname = hostName;
            ftp.Username = userName;
            ftp.Password = password;

            //  Establish an AUTH SSL secure channel after connection
            //  on the standard FTP port 21.
            ftp.AuthTls = true;

            //  The Ssl property is for establishing an implicit SSL connection
            //  on port 990.  Do not set it.
            ftp.Ssl = false;

            //  Connect and login to the FTP server.
            success = ftp.Connect();
            if (!success)
                return "ERRO REALIZAR CONNECT: " + ftp.LastErrorText;

            //Create directory
            //  Passar desta forma  o FilePaht "~/xxx/xxx"
            string localPath = HttpContext.Current.Server.MapPath(Path.Combine("~", filePath));
            if (!Directory.Exists(localPath))
                Directory.CreateDirectory(localPath);

            //  Change to the remote directory where the file is located.
            //  This step is only necessary if the file is not in the root directory
            //  for the FTP account.
            if (hostPath != null)
            {
                string remoteDirRetr = string.Empty;
                hostPath.ForEach(hst =>
                {
                    success = ftp.ChangeRemoteDir(hst);
                    if (!success)
                    {
                        remoteDirRetr = "ERRO REALIZAR CHANGEREMOTEDIR: " + ftp.LastErrorText;
                        return;
                    }
                });
                if (!string.IsNullOrWhiteSpace(remoteDirRetr))
                    return remoteDirRetr;
            }

            string localFilename = Path.Combine(localPath, fileName);

            //  Download a file.
            success = ftp.GetFile(fileName, localFilename);
            if (!success)
                return "ERRO REALIZAR DOWNLOAD: " + ftp.LastErrorText;

            success = ftp.Disconnect();
            return string.Empty;
        }

        public string SimpleFTPUpload(string hostName, string userName, string password, string filePath, string fileName, List<string> hostPath = null,string customFolder = null)
        { 
            Chilkat.Ftp2 ftp = new Chilkat.Ftp2();

            bool success;

            //  Any string unlocks the component.
            if (Constants.ModoDesenvolvimento.Equals("1"))
                success = ftp.UnlockComponent("Anything for 30-day trial");
            else
                success = ftp.UnlockComponent(ChilkatUnlockCode());
            if (!success)
                return "ERRO REALIZAR UNLOCK: " + ftp.LastErrorText;

            ftp.Hostname = hostName;
            ftp.Username = userName;
            ftp.Password = password;

            //  Connect and login to the FTP server.
            success = ftp.Connect();
            if (!success)
                return "ERRO REALIZAR CONNECT: " + ftp.LastErrorText;

            if (customFolder != null)
            {
                bool dirExists = ftp.ChangeRemoteDir(customFolder);
                if (!dirExists)
                {
                    success = ftp.CreateRemoteDir(customFolder);
                    if (!success)
                        return "ERRO REALIZAR CREATEREMOTEDIR: " + ftp.LastErrorText;
                    success = ftp.ChangeRemoteDir(customFolder);
                    if (!success)
                        return "ERRO REALIZAR CHANGEREMOTEDIR: " + ftp.LastErrorText;
                }
            }

            //  Change to the remote directory where the file is located.
            //  This step is only necessary if the file is not in the root directory
            //  for the FTP account.
            if (hostPath != null)
            {
                string remoteDirRetr = string.Empty;
                hostPath.ForEach(hst =>
                {
                    success = ftp.ChangeRemoteDir(hst);
                    if (!success)
                    {
                        remoteDirRetr = "ERRO REALIZAR CHANGEREMOTEDIR: " + ftp.LastErrorText;
                        return;
                    }
                });
                if (!string.IsNullOrWhiteSpace(remoteDirRetr))
                    return remoteDirRetr;
            }
            //  Upload a file.
            //  Passar desta forma  o FilePaht "~/xxx/xxx"
            string localFilename = Path.Combine(filePath, fileName);
            //  Passar desta forma o FileName "imagem.jpg, arquivo.docx, etc..."
            string remoteFilename = fileName;

            success = ftp.PutFile(localFilename, remoteFilename);
            if (!success)
                return "ERRO REALIZAR UPLOAD: " + ftp.LastErrorText;

            success = ftp.Disconnect();

            return string.Empty;
        }
    }
}