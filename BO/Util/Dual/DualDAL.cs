﻿using System.Collections.Generic;

public class DualDAL : DAL
{
    public List<DualTO.obterDual> obterDual()
    {
        var DALSQL = new DualDALSQL();
        string cmdSql = DALSQL.obterDual();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<DualTO.obterDual>();
    }

    public List<DualTO.obterDualServico> obterDualServico()
    {
        var DALSQL = new DualDALSQL();
        string cmdSql = DALSQL.obterDualServico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<DualTO.obterDualServico>();
    }

    public List<DualTO.obterDualDesconto> obterDualDesconto()
    {
        var DALSQL = new DualDALSQL();
        string cmdSql = DALSQL.obterDualDesconto();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<DualTO.obterDualDesconto>();
    }

    public List<DualTO.obterDuaBranco> obterDuaBranco()
    {
        var DALSQL = new DualDALSQL();
        string cmdSql = DALSQL.obterDuaBranco();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<DualTO.obterDuaBranco>();
    }

    public List<DualTO.obterDualAnotacao> obterDualAnotacao()
    {
        var DALSQL = new DualDALSQL();
        string cmdSql = DALSQL.obterDualAnotacao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<DualTO.obterDualAnotacao>();
    }

    public List<DualTO.obterNotasFiscais> obterNotasFiscais()
    {
        var DALSQL = new DualDALSQL();
        string cmdSql = DALSQL.obterNotasFiscais();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<DualTO.obterNotasFiscais>();
    }
}