﻿
namespace DualTO
{
    public class obterDual
    {
        public int CODCLI { get; set; }

        public string NOMCLI { get; set; }

        public string NUMCGCCLI { get; set; }

        public int NRONF { get; set; }

        public string NROTIT { get; set; }

        public string NOMRES { get; set; }

        public string DATVEN { get; set; }

        public int SLDDEV { get; set; }
    }

    public class obterDualServico
    {
        public string CODSVCTGV { get; set; }

        public string DESSVCTGV { get; set; }

        public string VLRTAB { get; set; }

        public string VLRFINDSC { get; set; }

        public string STATUSSERVICO { get; set; }

        public string DATAATIVACAO { get; set; }

        public string INICIOSUSPENSAO { get; set; }

        public string FIMSUSPENSAO { get; set; }

        public string DATACANCELAMENTO { get; set; }

        public string MOTIVOCANCELAMENTO { get; set; }
    }

    public class obterDualDesconto
    {
        public int CODCNDCMC { get; set; }

        public string DESCNDCMC { get; set; }

        public string VLRDSC { get; set; }

        public string PERDSC { get; set; }

        public string DATINI { get; set; }

        public string CESTPROD { get; set; }

        public string DATFIM { get; set; }

        public string STATUS { get; set; }

        public string DATAALTERACAO { get; set; }
    }

    public class obterDuaBranco
    {
        public string PROBLEMA { get; set; }
    }

    public class obterDualAnotacao
    {
        public int NUMSEQ { get; set; }
        public string DESATV { get; set; }
        public string CORTIP { get; set; }
        public string NOMFNC { get; set; }
        public string STATUS { get; set; }
        public string AGDATV { get; set; }
        public string FIMAGDATV { get; set; }
    }

    public class obterNotasFiscais
    {
        public int NRONOTA { get; set; }
        public int NROTIT { get; set; }
        public string DESSVCTGV { get; set; }
        public string DATVEN { get; set; }
        public string VLRNFE { get; set; }
        public string PARNFE { get; set; }
        public string DATPAG { get; set; }
        public string VLRPAG { get; set; }
        public string STATUS { get; set; }
        public string FRMPAG { get; set; }
    }
}