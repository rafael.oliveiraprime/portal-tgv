﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class DualBLL
{
    public List<DualTO.obterDual> obterDual()
    {
        var DAL = new DualDAL();
        return DAL.obterDual();
    }

    public List<DualTO.obterDualServico> obterDualServico()
    {
        var DAL = new DualDAL();
        return DAL.obterDualServico();
    }

    public List<DualTO.obterDualDesconto> obterDualDesconto()
    {
        var DAL = new DualDAL();
        return DAL.obterDualDesconto();
    }

    public List<DualTO.obterDuaBranco> obterDuaBranco()
    {
        var DAL = new DualDAL();
        return DAL.obterDuaBranco();
    }

    public List<DualTO.obterDualAnotacao> obterDualAnotacao()
    {
        var DAL = new DualDAL();
        return DAL.obterDualAnotacao();
    }

    public List<DualTO.obterNotasFiscais> obterNotasFiscais()
    {
        var DAL = new DualDAL();
        return DAL.obterNotasFiscais();
    }
}