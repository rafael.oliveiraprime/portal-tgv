﻿using System.Collections.Generic;

namespace ConsultaCestaServicoTO
{
    public class obterConsultaCestaServico
    {
        public int CODCLI { get; set; }
        public int ANOMESREF { get; set; }
        public int CODPRDCES { get; set; }
        public decimal VLRMOVSVCPRDSMA { get; set; }
        public string NOMCLI { get; set; }
        public string DESPRDCES { get; set; }
        public int CODFNCALT { get; set; }
        public string DATATU { get; set; }


    }
}

