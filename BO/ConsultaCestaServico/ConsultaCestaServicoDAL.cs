﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class ConsultaCestaServicoDAL : DAL
{
    public List<ConsultaCestaServicoTO.obterConsultaCestaServico> ObterConsultaCestaServico(ConsultaCestaServicoApiModel.ConsultaCestaServicoApiModel objPesquisar)
    {
        var DALSQL = new ConsultaCestaServicoDALSQL();

        if (objPesquisar.CODPRDCES == 0)
            objPesquisar.CODPRDCES = -1;

        if (objPesquisar.ANOMESREF == 0)
            objPesquisar.ANOMESREF = -1;

        string cmdSql = DALSQL.obterConsultaCestaServico(objPesquisar.CODPRDCES, objPesquisar.ANOMESREF);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODPRDCES", objPesquisar.CODPRDCES);
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<ConsultaCestaServicoTO.obterConsultaCestaServico>();
    }

    public bool InserirConsultaCestaServico(ConsultaCestaServicoApiModel.ConsultaCestaServicoApiModel objInserir)
    {
        var DALSQL = new ConsultaCestaServicoDALSQL();
        string cmdSql = DALSQL.inserirConsultaCestaServico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objInserir.CODFNCALT = ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //
        dbCommand.AddWithValue("CODCLI", objInserir.CODCLI);
        dbCommand.AddWithValue("ANOMESREF", objInserir.ANOMESREF);
        dbCommand.AddWithValue("CODPRDCES", objInserir.CODPRDCES);
        dbCommand.AddWithValue("VLRMOVSVCPRDSMA", objInserir.VLRMOVSVCPRDSMA);
        dbCommand.AddWithValue("CODFNCALT", objInserir.CODFNCALT);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool alterarConsultaCestaServico(ConsultaCestaServicoApiModel.ConsultaCestaServicoApiModel objAlterar)
    {
        var DALSQL = new ConsultaCestaServicoDALSQL();
        string cmdSql = DALSQL.alterarConsultaCestaServico(1);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        //objAlterar.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC.ToString();
        //if (1 == 1)
        //    objAlterar.CODFNCDST = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC.ToString();
        //
        //dbCommand.AddWithValue("DESPRDCES", objAlterar.DESPRDCES);
        //dbCommand.AddWithValue("NUMSEQ", objAlterar.NUMSEQ);
        //dbCommand.AddWithValue("TIPDDOATR", objAlterar.TIPDDOATR);
        //dbCommand.AddWithValue("FLGIPDSVC", objAlterar.FLGIPDSVC);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }
    

    public bool deletarConsultaCestaServico(ConsultaCestaServicoApiModel.ConsultaCestaServicoApiModel objAltera)
    {
        var DALSQL = new ConsultaCestaServicoDALSQL();
        string cmdSql = DALSQL.deletarConsultaCestaServico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        dbCommand.AddWithValue("CODCLI", objAltera.CODCLI);
        dbCommand.AddWithValue("ANOMESREF", objAltera.ANOMESREF);
        dbCommand.AddWithValue("CODPRDCES", objAltera.CODPRDCES);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


}