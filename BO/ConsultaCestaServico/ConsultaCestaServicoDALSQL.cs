﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class ConsultaCestaServicoDALSQL
{
    public string obterConsultaCestaServico(int _codservico, int _anomes)
    {
         StringBuilder strBld = new StringBuilder(@"
                                                    SELECT M.CODCLI,
                                                           NVL(CLI.NOMCLI, ' ') AS NOMCLI,
                                                           M.ANOMESREF,
                                                           M.CODPRDCES,
                                                           S.CODPRDCES,
                                                           M.CODFNCALT,
                                                           M.DATATU,
                                                           VLRPRDCES
                                                      FROM MRT.MOVCESPRDTGV M
                                                      LEFT JOIN MRT.CADCESPRDTGV S ON (M.CODPRDCES = S.CODPRDCES)
                                                      LEFT JOIN MRT.T0100043 CLI ON(M.CODCLI = CLI.CODCLI)
                                                     WHERE 1 = 1                                                                                                                
                                                  ");

        if (_codservico > 0)
        {
            strBld.AppendLine(" AND M.CODPRDCES = :CODPRDCES ");
        }

        if (_anomes > 0)
        {            
            strBld.AppendLine(" AND M.ANOMESREF = :ANOMESREF ");
        }

        strBld.AppendLine("ORDER BY CLI.NOMCLI");

        return strBld.ToString();
    }    

    public string inserirConsultaCestaServico()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" INSERT INTO MRT.MOVCESPRDTGV (CODCLI, ANOMESREF, CODPRDCES, VLRMOVSVCPRDSMA, CODFNCALT, DATATU) 
                                                               VALUES (:CODCLI, :ANOMESREF, :CODPRDCES, :VLRMOVSVCPRDSMA, :CODFNCALT, SYSDATE) ");

        return strBld.ToString();
    }

    public string alterarConsultaCestaServico(int desativar)
    {
        StringBuilder strBldAti = new StringBuilder(@"
                     UPDATE MRT.CADCESPRDTGV SET
                            DESPRDCES = UPPER(:DESPRDCES), 
                            NUMSEQ = :NUMSEQ, 
                            TIPDDOATR = :TIPDDOATR, 
                            FLGIPDSVC = :FLGIPDSVC,
                            DATALT = SYSDATE,
                            CODFNCDST = NULL,
                            DATDST = NULL
                      WHERE CODPRDCES = :CODPRDCES
                ");

        StringBuilder strBldDst = new StringBuilder(@"
                     UPDATE MRT.CADCESPRDTGV SET
                            DESPRDCES = UPPER(:DESPRDCES), 
                            NUMSEQ = :NUMSEQ, 
                            TIPDDOATR = :TIPDDOATR, 
                            FLGIPDSVC = :FLGIPDSVC,
                            DATALT = SYSDATE,
                            CODFNCDST = NULL,
                            DATDST = NULL,
                            CODFNCDST = :CODFNCDST,
                            DATDST = SYSDATE
                     WHERE  CODPRDCES = :CODPRDCES
                ");

        if (desativar == 1) {
            return strBldDst.ToString();
        }
        else
        {
            return strBldAti.ToString();
        }

    }

    public string deletarConsultaCestaServico()
    {
        StringBuilder strBld;
        strBld = new StringBuilder(@" DELETE FROM MRT.MOVCESPRDTGV 
                                       WHERE ANOMESREF = :ANOMESREF 
                                         AND CODCLI = :CODCLI
                                         AND CODPRDCES = :CODPRDCES ");
        return strBld.ToString();
    }

}