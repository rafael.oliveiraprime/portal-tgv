﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class ConsultaCestaServicoBLL
{
    public List<ConsultaCestaServicoTO.obterConsultaCestaServico> obterConsultaCestaServico(ConsultaCestaServicoApiModel.ConsultaCestaServicoApiModel objPesquisar)
    {
        try
        {
            var DAL = new ConsultaCestaServicoDAL();
            return DAL.ObterConsultaCestaServico(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO CONSULTAR MOVIMENTO CESTA PRODUTOS.");
            Utilitario.InsereLog(ex, "ObterConsultaCestaServico", "ConsultaCestaServicoApiModel.ConsultaCestaServicoApiModel", "ConsultaCestaServicoBLL", "ERRO AO CONSULTAR MOVIMENTO CESTA PRODUTOS.");
            throw;
        }
    }

    public bool InserirConsultaCestaServico(ConsultaCestaServicoApiModel.ConsultaCestaServicoApiModel objInserir)
    {
        try
        {
            var DAL = new ConsultaCestaServicoDAL();
            return DAL.InserirConsultaCestaServico(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO INSERIR MOVIMENTO CESTA PRODUTOS.");
            Utilitario.InsereLog(ex, "InserirConsultaCestaServico", "ConsultaCestaServicoApiModel.ConsultaCestaServicoApiModel", "ConsultaCestaServicoBLL", "ERRO AO INSERIR MOVIMENTO CESTA PRODUTOS.");
            throw;
        }
    }

    public bool alterarConsultaCestaServico(ConsultaCestaServicoApiModel.ConsultaCestaServicoApiModel objAlterar)
    {
        try
        {
            var DAL = new ConsultaCestaServicoDAL();
            return DAL.alterarConsultaCestaServico(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO ALTERAR MOVIMENTO CESTA PRODUTOS.");
            Utilitario.InsereLog(ex, "alterarConsultaCestaServico", "ConsultaCestaServicoApiModel.ConsultaCestaServicoApiModel", "ConsultaCestaServicoBLL", "ERRO AO ALTERAR MOVIMENTO CESTA PRODUTOS.");
            throw;
        }
    }

    public bool deletarConsultaCestaServico(ConsultaCestaServicoApiModel.ConsultaCestaServicoApiModel objAlterar)
    {
        try
        {
            var DAL = new ConsultaCestaServicoDAL();
            return DAL.deletarConsultaCestaServico(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO DELETAR MOVIMENTO CESTA PRODUTOS.");
            Utilitario.InsereLog(ex, "deletarConsultaCestaServico", "ConsultaCestaServicoApiModel.ConsultaCestaServicoApiModel", "ConsultaCestaServicoBLL", "ERRO AO DELETAR MOVIMENTO CESTA PRODUTOS.");
            throw;
        }
    }
}