﻿using System;
using System.Collections.Generic;

namespace CadastroPessoaIndicacaoTO
{
    public class obterCadastroPessoaIndicacao
    {
        public Int64 NUMSEQPESIND { get; set; }

        public Int64 NUMSEQPESINDCHECK { get; set; }

        public string TIPFNCRPN { get; set; }

        public string INDICADOR { get; set; }

        public string CODFNC { get; set; }

        public string NOMFNC { get; set; }

        public string CODREP { get; set; }

        public string NOMREP { get; set; }

        public string NUMCPF { get; set; }

        public string DESCGR { get; set; }

        public string NOMEMP { get; set; }

        public string NOMPESCTO { get; set; }

        public string MATRICULA { get; set; }

        public string DESENDETN { get; set; }

        public string NUMTLF { get; set; }

        public string NUMTLFCEL { get; set; }

        public string FNCCAD { get; set; }

        public string DATFNCCAD { get; set; }

        public string FNCALT { get; set; }

        public string DATFNCALT { get; set; }

        public string FNCDST { get; set; }

        public string DATFNCDST { get; set; }

        public string STATUS { get; set; }
    }

    public class obterFuncionarios
    {
        public int CODFNC { get; set; }

        public string NOMFNC { get; set; }

        public string NUMCPFFNC { get; set; }        
    }

    public class obterRepresentantes
    {
        public int CODREP { get; set; }

        public string NOMREP { get; set; }

        public string NUMCPFREP { get; set; }        
    }

    public class verificaExisteMaisDeUmCPF
    {    
        public int QTDNUMCPF { get; set; }
    }
}