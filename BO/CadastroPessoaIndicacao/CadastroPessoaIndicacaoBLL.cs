﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CadastroPessoaIndicacaoBLL
{
    public List<CadastroPessoaIndicacaoTO.obterCadastroPessoaIndicacao> obterCadastroPessoaIndicacao(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroPessoaIndicacaoDAL();
            return DAL.obterCadastroPessoaIndicacao(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO PESSOA INDICACAO.");
            Utilitario.InsereLog(ex, "obterCadastroPessoaIndicacao", "CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel", "CadastroPessoaIndicacaoBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO PESSOA INDICACAO.");
            throw;
        }
    }

    public bool inserirCadastroPessoaIndicacao(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objInserir)
    {
        try
        {
            var DAL = new CadastroPessoaIndicacaoDAL();
            return DAL.inserirCadastroPessoaIndicacao(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO CADASTRO PESSOA INDICACAO.");
            Utilitario.InsereLog(ex, "inserirCadastroPessoaIndicacao", "CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel", "CadastroPessoaIndicacaoBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO PESSOA INDICACAO.");
            throw;
        }
    }

    public bool alterarCadastroPessoaIndicacao(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroPessoaIndicacaoDAL();
            return DAL.alterarCadastroPessoaIndicacao(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO CADASTRO PESSOA INDICACAO.");
            Utilitario.InsereLog(ex, "alterarCadastroPessoaIndicacao", "CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel", "CadastroPessoaIndicacaoBLL", "ERRO AO FAZER A ALTERACAO DO CADASTRO PESSOA INDICACAO.");
            throw;
        }
    }

    public bool ativarCadastroPessoaIndicacao(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroPessoaIndicacaoDAL();
            return DAL.ativarCadastroPessoaIndicacao(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATIVACAO DO CADASTRO PESSOA INDICACAO.");
            Utilitario.InsereLog(ex, "ativarCadastroPessoaIndicacao", "CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel", "CadastroPessoaIndicacaoBLL", "ERRO AO FAZER A ATIVACAO DO CADASTRO PESSOA INDICACAO.");
            throw;
        }
    }

    public bool desativarCadastroPessoaIndicacao(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroPessoaIndicacaoDAL();
            return DAL.desativarCadastroPessoaIndicacao(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATIVACAO DO CADASTRO PESSOA INDICACAO.");
            Utilitario.InsereLog(ex, "ativarCadastroPessoaIndicacao", "CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel", "CadastroPessoaIndicacaoBLL", "ERRO AO FAZER A ATIVACAO DO CADASTRO PESSOA INDICACAO.");
            throw;
        }
    }

    public CadastroPessoaIndicacaoTO.obterFuncionarios obterFuncionarios(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroPessoaIndicacaoDAL();
            return DAL.obterFuncionarios(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO FUNCIONARIO NO CADASTRO PESSOA INDICACAO.");
            Utilitario.InsereLog(ex, "obterFuncionarios", "CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel", "CadastroPessoaIndicacaoBLL", "ERRO AO FAZER A CONSULTA DO FUNCIONARIO NO CADASTRO PESSOA INDICACAO.");
            throw;
        }
    }

    public CadastroPessoaIndicacaoTO.obterRepresentantes obterRepresentantes(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroPessoaIndicacaoDAL();
            return DAL.obterRepresentantes(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO REPRESENTANTE NO CADASTRO PESSOA INDICACAO.");
            Utilitario.InsereLog(ex, "obterRepresentantes", "CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel", "CadastroPessoaIndicacaoBLL", "ERRO AO FAZER A CONSULTA DO REPRESENTANTE NO CADASTRO PESSOA INDICACAO.");
            throw;
        }
    }

    public int VerificaCPF(string NUMCPF)
    {
        try
        {
            var DAL = new CadastroPessoaIndicacaoDAL();
            return DAL.VerificaCPF(NUMCPF);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A VERIFICACAO DE CPF NO CADASTRO PESSOA INDICACAO.");
            Utilitario.InsereLog(ex, "VerificaCPF", "NUMCPF", "CadastroPessoaIndicacaoBLL", "ERRO AO FAZER A VERIFICACAO DE CPF NO CADASTRO PESSOA INDICACAO.");
            throw;
        }
    }    

    public CadastroPessoaIndicacaoTO.verificaExisteMaisDeUmCPF verificaExisteMaisDeUmCPF(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroPessoaIndicacaoDAL();
            return DAL.verificaExisteMaisDeUmCPF(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A VERIFICACAO DE CPF MAIS DE UM NO CADASTRO PESSOA INDICACAO.");
            Utilitario.InsereLog(ex, "verificaExisteMaisDeUmCPF", "NUMCPF", "CadastroPessoaIndicacaoBLL", "ERRO AO FAZER A VERIFICACAO DE CPF MAIS DE UM NO CADASTRO PESSOA INDICACAO.");
            throw;
        }
    }
}