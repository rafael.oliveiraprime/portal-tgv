﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

public class CadastroPessoaIndicacaoDAL : DAL
{
    public List<CadastroPessoaIndicacaoTO.obterCadastroPessoaIndicacao> obterCadastroPessoaIndicacao(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objPesquisar)
    {
        var DALSQL = new CadastroPessoaIndicacaoDALSQL();
        string cmdSql = DALSQL.obterCadastroPessoaIndicacao(objPesquisar.CPIATI, objPesquisar.NUMSEQPESIND, objPesquisar.CODFNCFIL);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (objPesquisar.NUMSEQPESIND == 0)
        {
            objPesquisar.NUMSEQPESIND = -1;
        }
        //
        string NUMCPFFIL;
        if (objPesquisar.NUMCPFFIL != null)
        {
            NUMCPFFIL = Regex.Replace(objPesquisar.NUMCPFFIL, "[-.]", String.Empty);
        }
        else
        {
            NUMCPFFIL = null;
        }
        dbCommand.AddWithValue("CODFNCFIL", objPesquisar.CODFNCFIL);
        dbCommand.AddWithValue("NUMCPFFIL", NUMCPFFIL + "   ");
        dbCommand.AddWithValue("NUMSEQPESIND", objPesquisar.NUMSEQPESIND);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroPessoaIndicacaoTO.obterCadastroPessoaIndicacao>();
    }

    public bool inserirCadastroPessoaIndicacao(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objInserir)
    {
        var DALSQL = new CadastroPessoaIndicacaoDALSQL();
        string cmdSql = DALSQL.inserirCadastroPessoaIndicacao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objInserir.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        objInserir.CODFNCCAD = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //
        string NUMTLF;
        //
        if (objInserir.NUMTLF != null)
        {
            NUMTLF = Regex.Replace(objInserir.NUMTLF, "[-)( ]", String.Empty);
        }
        else
        {
            NUMTLF = null;
        }
        //
        string NUMTLFCEL;
        if (objInserir.NUMTLFCEL != null)
        {
            NUMTLFCEL = Regex.Replace(objInserir.NUMTLFCEL, "[-)( ]", String.Empty);
        }
        else
        {
            NUMTLFCEL = null;
        }
        //
        string NUMCPF;
        if (objInserir.NUMCPF != null)
        {
            NUMCPF = Regex.Replace(objInserir.NUMCPF, "[-.]", String.Empty);
        }
        else
        {
            NUMCPF = null;
        }
        //
        if (objInserir.CODREP == -1)
        {
            objInserir.CODREP = 0;
        }
        //
        if (objInserir.CODFNC == -1)
        {
            objInserir.CODFNC = 0;
        }
        //
        int CODFNC;
        if (objInserir.CODFNC == -1 || objInserir.CODFNC == 0)
        {
            if (objInserir.CODFNCTRI != -1)
            {
                CODFNC = objInserir.CODFNCTRI;
            }
            else
            {
                CODFNC = 0;    
            }
        }
        else
        {
            CODFNC = objInserir.CODFNC;
        }
        //
        dbCommand.AddWithValue("TIPFNCRPN", objInserir.TIPFNCRPN);
        dbCommand.AddWithValue("CODFNC", CODFNC);
        dbCommand.AddWithValue("CODREP", objInserir.CODREP);        
        dbCommand.AddWithValue("NUMCPF", NUMCPF);
        dbCommand.AddWithValue("DESCGR", objInserir.DESCGR);
        dbCommand.AddWithValue("NOMEMP", objInserir.NOMEMP);

        if (objInserir.NOMPESCTO.Length > 30)
            dbCommand.AddWithValue("NOMPESCTO", objInserir.NOMPESCTO.Substring(1, 30));
        else
            dbCommand.AddWithValue("NOMPESCTO", objInserir.NOMPESCTO);

        dbCommand.AddWithValue("DESENDETN", objInserir.DESENDETN);
        dbCommand.AddWithValue("NUMTLF", NUMTLF);
        dbCommand.AddWithValue("NUMTLFCEL", NUMTLFCEL);
        dbCommand.AddWithValue("CODFNCCAD", objInserir.CODFNCCAD);
        dbCommand.AddWithValue("CODFNCALT", objInserir.CODFNCALT);
        //                      
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool alterarCadastroPessoaIndicacao(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objAlterar)
    {
        var DALSQL = new CadastroPessoaIndicacaoDALSQL();
        string cmdSql = DALSQL.alterarCadastroPessoaIndicacao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //   
        string NUMTLF;
        //
        if (objAlterar.NUMTLF != null)
        {
            NUMTLF = Regex.Replace(objAlterar.NUMTLF, "[-)( ]", String.Empty);
        }
        else
        {
            NUMTLF = null;
        }
        //
        string NUMTLFCEL;
        if (objAlterar.NUMTLFCEL != null)
        {
            NUMTLFCEL = Regex.Replace(objAlterar.NUMTLFCEL, "[-)( ]", String.Empty);
        }
        else
        {
            NUMTLFCEL = null;
        }
        //
        string NUMCPF;
        if (objAlterar.NUMCPF != null)
        {
            NUMCPF = Regex.Replace(objAlterar.NUMCPF, "[-.]", String.Empty);
        }
        else
        {
            NUMCPF = null;
        }
        //
        if (objAlterar.CODREP == -1)
        {
            objAlterar.CODREP = 0;
        }
        //
        //
        int CODFNC;
        if (objAlterar.CODFNC == -1)
        {
            if (objAlterar.CODFNCTRI != -1)
            {
                CODFNC = objAlterar.CODFNCTRI;
            }
            else
            {
                CODFNC = 0;
            }
        }
        else
        {
            CODFNC = objAlterar.CODFNC;
        }
        //
        dbCommand.AddWithValue("NUMSEQPESIND", objAlterar.NUMSEQPESIND);
        dbCommand.AddWithValue("TIPFNCRPN", objAlterar.TIPFNCRPN);
        dbCommand.AddWithValue("CODFNC", CODFNC);
        dbCommand.AddWithValue("CODREP", objAlterar.CODREP);
        dbCommand.AddWithValue("NUMCPF", NUMCPF);
        dbCommand.AddWithValue("DESCGR", objAlterar.DESCGR);
        dbCommand.AddWithValue("NOMEMP", objAlterar.NOMEMP);

        if (objAlterar.NOMPESCTO.Length > 30)
            dbCommand.AddWithValue("NOMPESCTO", objAlterar.NOMPESCTO.Substring(1,30));
        else
            dbCommand.AddWithValue("NOMPESCTO", objAlterar.NOMPESCTO);


        dbCommand.AddWithValue("DESENDETN", objAlterar.DESENDETN);
        dbCommand.AddWithValue("NUMTLF", NUMTLF);
        dbCommand.AddWithValue("NUMTLFCEL", NUMTLFCEL);
        dbCommand.AddWithValue("CODFNCALT", objAlterar.CODFNCALT);
        //     
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool ativarCadastroPessoaIndicacao(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objAlterar)
    {
        var DALSQL = new CadastroPessoaIndicacaoDALSQL();
        string cmdSql = DALSQL.ativarCadastroPessoaIndicacao(objAlterar.LISTNUMSEQPESIND);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool desativarCadastroPessoaIndicacao(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objAlterar)
    {
        var DALSQL = new CadastroPessoaIndicacaoDALSQL();
        string cmdSql = DALSQL.desativarCadastroPessoaIndicacao(objAlterar.LISTNUMSEQPESIND);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCDST = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //                     
        dbCommand.AddWithValue("CODFNCDST", objAlterar.CODFNCDST);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public CadastroPessoaIndicacaoTO.obterFuncionarios obterFuncionarios(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objPesquisa)
    {
        var DALSQL = new CadastroPessoaIndicacaoDALSQL();
        string cmdSql = DALSQL.obterFuncionarios();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        dbCommand.AddWithValue("CODFNC", objPesquisa.CODFNC);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        var retorno = MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroPessoaIndicacaoTO.obterFuncionarios>();
        if (retorno.Count == 1)
        {
            return retorno.FirstOrDefault();
        }
        else
        {
            return new CadastroPessoaIndicacaoTO.obterFuncionarios();
        }
    }

    public CadastroPessoaIndicacaoTO.obterRepresentantes obterRepresentantes(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objPesquisa)
    {
        var DALSQL = new CadastroPessoaIndicacaoDALSQL();
        string cmdSql = DALSQL.obterRepresentantes();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        dbCommand.AddWithValue("CODREP", objPesquisa.CODREP);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        var retorno = MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroPessoaIndicacaoTO.obterRepresentantes>();
        if (retorno.Count == 1)
        {
            return retorno.FirstOrDefault();
        }
        else
        {
            return new CadastroPessoaIndicacaoTO.obterRepresentantes();
        }
    }


    public int VerificaCPF(string NUMCPF)
    {
        var DALSQL = new CadastroPessoaIndicacaoDALSQL();
        string cmdSql = DALSQL.verificaCPF();
        
        //
        if (NUMCPF != "")
        {
            NUMCPF = Regex.Replace(NUMCPF, "[/.-]", String.Empty);
        }
        else
        {
            NUMCPF = null;
        }

        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        dbCommand.AddWithValue("NUMCPF", NUMCPF);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return Convert.ToInt32(MRT001.ExecuteScalar(dbCommand));
    }         

    public CadastroPessoaIndicacaoTO.verificaExisteMaisDeUmCPF verificaExisteMaisDeUmCPF(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objPesquisa)
    {
        var DALSQL = new CadastroPessoaIndicacaoDALSQL();
        string cmdSql = DALSQL.verificaExisteMaisDeUmCPF();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        string NUMCPF = objPesquisa.NUMCPFVER;
        if (NUMCPF != "")
        {
            NUMCPF = Regex.Replace(NUMCPF, "[/.-]", String.Empty);
        }
        else
        {
            NUMCPF = null;
        }
        //
        dbCommand.AddWithValue("NUMCPFVER", NUMCPF);
        dbCommand.AddWithValue("NUMSEQPESIND", objPesquisa.NUMSEQPESIND);
        //              
        var retorno = MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroPessoaIndicacaoTO.verificaExisteMaisDeUmCPF>();
        //
        if (retorno.Count > 0)
        {
            return retorno.FirstOrDefault();
        }
        else
        {
            return new CadastroPessoaIndicacaoTO.verificaExisteMaisDeUmCPF();
        }
    }
}