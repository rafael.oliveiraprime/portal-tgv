﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class CadastroPessoaIndicacaoDALSQL
{
    public string obterCadastroPessoaIndicacao(int flgAtivo, Int64 codNumSeqPesInd, Int64 codFnc)
    {
        StringBuilder strBld = new StringBuilder(@"                 
                 SELECT CPI.NUMSEQPESIND,
                 CPI.NUMSEQPESIND NUMSEQPESINDCHECK,
                 CPI.TIPFNCRPN, 
                 CASE WHEN  CPI.TIPFNCRPN = 'M' THEN 'FUNC.MARTINS' WHEN CPI.TIPFNCRPN = 'R' THEN ' RCA AUTÔNOMO' WHEN CPI.TIPFNCRPN = 'T' THEN 'GERENTE TRIBANCO' WHEN CPI.TIPFNCRPN = 'O' THEN 'OUTROS' END INDICADOR,
                 CASE WHEN CPI.CODFNC != 0 THEN CPI.CODFNC ELSE NULL END CODFNC,
                 CASE WHEN CPI.CODFNC != 0 THEN CPI.CODFNC WHEN CPI.CODREP != 0 THEN CPI.CODREP ELSE NULL END MATRICULA,
                 FNC.NOMFNC, 
                 CASE WHEN CPI.CODREP != 0 THEN CPI.CODREP ELSE NULL END CODREP,
                 REP.NOMREP, 
                 CASE CPI.NUMCPF WHEN CPI.NUMCPF THEN (SUBSTR(CPI.NUMCPF, 1, 3) || '.' || SUBSTR(CPI.NUMCPF, 4, 3) || '.' || SUBSTR(CPI.NUMCPF, 7, 3) || '-' || SUBSTR(CPI.NUMCPF, 10,2)) ELSE NULL END NUMCPF,
                 CPI.DESCGR, CPI.NOMEMP,
                 TRIM(CPI.NOMPESCTO) AS NOMPESCTO,
                 TRIM(CPI.DESENDETN) AS DESENDETN,
                 CASE WHEN LENGTH(TRIM(CPI.NUMTLF)) =11 THEN ( '(' || SUBSTR(CPI.NUMTLF, 1, 2) || ')' || SUBSTR(CPI.NUMTLF, 3, 5) || '-' ||  SUBSTR(CPI.NUMTLF, 8, 4)) WHEN LENGTH(TRIM(CPI.NUMTLF)) =10 THEN '(' || SUBSTR(CPI.NUMTLF, 1, 2) || ') ' || SUBSTR(CPI.NUMTLF, 3, 4) || '-' || SUBSTR(CPI.NUMTLF, 7, 4) END NUMTLF,
                 CASE WHEN LENGTH(TRIM(CPI.NUMTLFCEL)) =11 THEN ( '(' || SUBSTR(CPI.NUMTLFCEL, 1, 2) || ')' || SUBSTR(CPI.NUMTLFCEL, 3, 5) || '-' || SUBSTR(CPI.NUMTLFCEL, 8, 4)) WHEN LENGTH(TRIM(CPI.NUMTLFCEL)) =10 THEN '(' || SUBSTR(CPI.NUMTLFCEL, 1, 2) || ') ' || SUBSTR(CPI.NUMTLFCEL, 3, 4) || '-' || SUBSTR(CPI.NUMTLFCEL, 7, 4) END NUMTLFCEL,
                 CASE CPI.CODFNCDST WHEN CPI.CODFNCDST THEN 'DESATIVADO' ELSE 'ATIVADO' END STATUS,                 
                 TRIM(FNC_CAD.NOMFNC) FNCCAD,
                 TO_CHAR(CPI.DATCAD,'DD/MM/YYYY') DATFNCCAD,
                 TRIM(FNC_ALT.NOMFNC) FNCALT,
                 TO_CHAR(CPI.DATALT,'DD/MM/YYYY') DATFNCALT,
                 CASE CPI.DATDST WHEN CPI.DATDST THEN TRIM(FNC_DST.NOMFNC) ELSE NULL END FNCDST,
                 CASE CPI.DATDST WHEN CPI.DATDST THEN TO_CHAR(CPI.DATDST,'DD/MM/YYYY') ELSE NULL END DATFNCDST
                 FROM MRT.CADDDOPESINDTGV CPI
                 LEFT JOIN MRT.T0100361 FNC_CAD ON FNC_CAD.CODFNC = CPI.CODFNCCAD
                 LEFT JOIN MRT.T0100361 FNC_ALT ON FNC_ALT.CODFNC = CPI.CODFNCALT
                 LEFT JOIN MRT.T0100361 FNC_DST ON FNC_DST.CODFNC = CPI.CODFNCDST 
                 LEFT JOIN MRT.T0100361 FNC ON FNC.CODFNC = CPI.CODFNC
                 LEFT JOIN MRT.T0100116 REP ON REP.CODREP = CPI.CODREP
                 WHERE 1 = 1
                 AND CPI.NUMSEQPESIND = :NUMSEQPESIND
                 AND CPI.CODFNC = :CODFNCFIL
                 AND CPI.NUMCPF = :NUMCPFFIL
                ");

        if (flgAtivo == 1)
        {            
                strBld.AppendLine(" AND DATDST IS NULL ");            
        }
        if (flgAtivo == 2)
        {   
                strBld.AppendLine(" AND DATDST IS NOT NULL ");            
        }

        strBld.AppendLine("ORDER BY STATUS, NUMSEQPESIND");

        return strBld.ToString();
    }

    public string inserirCadastroPessoaIndicacao()
    {
        return @"
                 INSERT INTO MRT.CADDDOPESINDTGV (NUMSEQPESIND, TIPFNCRPN, CODFNC, CODREP, NUMCPF, DESCGR, NOMEMP, NOMPESCTO, 
                 DESENDETN, NUMTLF, NUMTLFCEL, CODFNCCAD, DATCAD, CODFNCALT, DATALT)
                 VALUES ((SELECT COALESCE(MAX(NUMSEQPESIND),0)+1 FROM MRT.CADDDOPESINDTGV),
                 :TIPFNCRPN, :CODFNC, :CODREP, :NUMCPF, UPPER(:DESCGR), UPPER(:NOMEMP), UPPER(:NOMPESCTO), 
                 LOWER(:DESENDETN), :NUMTLF, :NUMTLFCEL, :CODFNCCAD, 
                 SYSDATE, :CODFNCALT, SYSDATE)
                ";
    }   

    public string alterarCadastroPessoaIndicacao()
    {
        return @"
                 UPDATE MRT.CADDDOPESINDTGV SET
                 TIPFNCRPN =:TIPFNCRPN,
                 CODFNC = :CODFNC,
                 CODREP = :CODREP,
                 NUMCPF = :NUMCPF,
                 DESCGR = UPPER(:DESCGR),
                 NOMEMP = UPPER(:NOMEMP),
                 NOMPESCTO = UPPER(:NOMPESCTO),
                 DESENDETN = LOWER(:DESENDETN),
                 NUMTLF = :NUMTLF, 
                 NUMTLFCEL = :NUMTLFCEL,                 
                 CODFNCALT = :CODFNCALT,
                 DATALT = SYSDATE
                 WHERE NUMSEQPESIND = :NUMSEQPESIND
                ";
    }

    public string ativarCadastroPessoaIndicacao(List<long> LISTNUMSEQPESIND)
    {

        return @"
                 UPDATE MRT.CADDDOPESINDTGV SET                 
                 CODFNCDST = NULL,
                 DATDST = NULL
                 WHERE NUMSEQPESIND IN(#LISTNUMSEQPESIND)
                ".Replace("#LISTNUMSEQPESIND", Utilitario.ToListStr(LISTNUMSEQPESIND));
    }

    public string desativarCadastroPessoaIndicacao(List<long> LISTNUMSEQPESIND)
    {
        return @"
                 UPDATE MRT.CADDDOPESINDTGV SET                                 
                 CODFNCDST = :CODFNCDST,
                 DATDST = SYSDATE
                 WHERE NUMSEQPESIND IN(#LISTNUMSEQPESIND)
                ".Replace("#LISTNUMSEQPESIND", Utilitario.ToListStr(LISTNUMSEQPESIND));
    }

    public string obterFuncionarios()
    {
        return @"
                 SELECT CODFNC, TRIM(NOMFNC) AS NOMFNC, 
                 CASE WHEN NUMCPFFNC <> 0 THEN (SUBSTR(NUMCPFFNC, 1, 3) || '.' || 
                 SUBSTR(NUMCPFFNC, 4, 3) || '.' ||
                 SUBSTR(NUMCPFFNC, 7, 3) || '-' || SUBSTR(NUMCPFFNC, 10,2)) END NUMCPFFNC
                 FROM MRT.T0100361			                  
                 WHERE 1 = 1   
                 AND CODFNC = :CODFNC
                ";
    }

    public string obterRepresentantes()
    {
        return @"
                 SELECT CODREP, TRIM(NOMREP) AS NOMREP,                  
                 CASE WHEN NUMCPFREP <> 0 THEN (SUBSTR(NUMCPFREP, 1, 3) || '.' || 
                 SUBSTR(NUMCPFREP, 4, 3) || '.' ||
                 SUBSTR(NUMCPFREP, 7, 3) || '-' || SUBSTR(NUMCPFREP, 10,2)) END NUMCPFREP
                 FROM MRT.T0100116			                  
                 WHERE 1 = 1                    
                 AND CODREP = :CODREP
                ";
    }


    public string verificaCPF()
    {
        return @"
                 SELECT COUNT(NUMCPF) 
                 FROM MRT.CADDDOPESINDTGV 
                 WHERE TRIM(NUMCPF) = TRIM(:NUMCPF)
                ";

    }

    public string verificaExisteMaisDeUmCPF()
    {
        return @"
                  SELECT COUNT(NUMCPF) AS QTDNUMCPF
                 FROM MRT.CADDDOPESINDTGV 
                 WHERE TRIM(NUMCPF) = TRIM(:NUMCPFVER)  
                 AND NUMSEQPESIND <> :NUMSEQPESIND             
                ";
    }
}