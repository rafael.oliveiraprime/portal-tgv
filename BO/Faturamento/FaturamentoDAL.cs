﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrmApiModel;

public class FaturamentoDAL : DAL
{
    public bool InserirInicioLoteFat(FaturamentoApiModel.ParametrosFaturamentoApiModel objPesquisar)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.InserirInicioLoteFat();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        dbCommand.AddWithValue("INDFATSVC", objPesquisar.INDFATSVC);
        dbCommand.AddWithValue("CODFNCGRC", objPesquisar.CODFNCGRC);
        dbCommand.AddWithValue("NUMLOTPRENOTFSCSVC", objPesquisar.NUMLOTPRENOTFSCSVC);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public bool FinalizaLoteFat(FaturamentoApiModel.ParametrosFaturamentoApiModel objPesquisar)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.FinalizaLoteFat();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool AtualizaStatusEnvioFaturamentoLote(FaturamentoApiModel.ParametrosFaturamentoApiModel objPesquisar)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.AtualizaStatusEnvioFaturamentoLote();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODFNCFAT", objPesquisar.CODFNCFAT);
        dbCommand.AddWithValue("NUNSEQUENCIA", objPesquisar.NUMLOTPRENOTFSCSVC);
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool deletetaLoteFat(FaturamentoApiModel.ParametrosFaturamentoApiModel objPesquisar)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.deletetaLoteFat();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public List<FaturamentoTO.verificaLotesAbertos> VerificaLoteAberto()
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.VerificaLoteAberto();
        //       
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<FaturamentoTO.verificaLotesAbertos>();
    }

    public List<FaturamentoTO.verificaLotesEnviado> VerificaLoteEnviado()
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.VerificaLoteEnviado();
        //       
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<FaturamentoTO.verificaLotesEnviado>();
    }

    public bool InserirMovimentoClientesContas(FaturamentoApiModel.ParametrosFaturamentoApiModel objPesquisar)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.InserirMovimentoClientesContas();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        dbCommand.AddWithValue("CODFNCGRC", objPesquisar.CODFNCGRC);
        dbCommand.AddWithValue("NUMLOTPRENOTFSCSVC", objPesquisar.NUMLOTPRENOTFSCSVC);

        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public bool InserirMovimnetoServicosDescontos(FaturamentoApiModel.ParametrosFaturamentoApiModel objPesquisar)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.InserirMovimnetoServicosDescontos();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        dbCommand.AddWithValue("ANOMESREFANT", objPesquisar.ANOMESREFPASS);
        dbCommand.AddWithValue("NUMLOTPRENOTFSCSVC", objPesquisar.NUMLOTPRENOTFSCSVC);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public List<FaturamentoTO.obterLotesFaturamneto> ObterLotesFaturamentoPrincipal(FaturamentoApiModel.obterDemonstrativoFaturamentoApiModel objPesquisar)
    {
        var DALSQL = new FaturamentoDALSQL();

        string cmdSql = DALSQL.ObterLotesFaturamentoPrincipal(objPesquisar);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        dbCommand.AddWithValue("ANOMESREFPASS", objPesquisar.ANOMESREFPASS);
        dbCommand.AddWithValue("CODCLIMODFIL", objPesquisar.CODCLIMODFIL);
        dbCommand.AddWithValue("CODSVCTGVMODFIL", objPesquisar.CODSVCTGVMODFIL);
        dbCommand.AddWithValue("NUMCGCCLIMODFIL", objPesquisar.NUMCGCCLIMODFIL);

        if(objPesquisar.NOMCLIMODFIL != "-1") 
        dbCommand.AddWithValue("NOMCLIMODFIL", objPesquisar.NOMCLIMODFIL);
        else
            dbCommand.AddWithValue("NOMCLIMODFIL", -1);


        dbCommand.AddWithValue("ANOMESVIGENTE", objPesquisar.ANOMESVIGENTE);
        dbCommand.AddWithValue("ANOMESVIGENTEPASS", objPesquisar.ANOMESVIGENTEPASS);

        //
        dbCommand.TrataDbCommandUniversal(true, false, true, false, false);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<FaturamentoTO.obterLotesFaturamneto>();
    }
    
    public List<FaturamentoTO.obterLotesFaturamneto> ObterLotesFaturamentoPrincipalGrupoFat(FaturamentoApiModel.obterDemonstrativoFaturamentoApiModel objPesquisar)
    {
        var DALSQL = new FaturamentoDALSQL();

        string cmdSql = DALSQL.ObterLotesFaturamentoPrincipalGrupoFat(objPesquisar);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        dbCommand.AddWithValue("ANOMESREFPASS", objPesquisar.ANOMESREFPASS);
        dbCommand.AddWithValue("CODCLIMODFIL", objPesquisar.CODCLIMODFIL);
        dbCommand.AddWithValue("CODSVCTGVMODFIL", objPesquisar.CODSVCTGVMODFIL);
        dbCommand.AddWithValue("NUMCGCCLIMODFIL", objPesquisar.NUMCGCCLIMODFIL);

        if (objPesquisar.NOMCLIMODFIL != "-1")
            dbCommand.AddWithValue("NOMCLIMODFIL", objPesquisar.NOMCLIMODFIL);
        else
            dbCommand.AddWithValue("NOMCLIMODFIL", -1);


        dbCommand.AddWithValue("ANOMESVIGENTE", objPesquisar.ANOMESVIGENTE);
        dbCommand.AddWithValue("ANOMESVIGENTEPASS", objPesquisar.ANOMESVIGENTEPASS);

        //
        dbCommand.TrataDbCommandUniversal(true, false, true, false, false);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<FaturamentoTO.obterLotesFaturamneto>();
    }

    
    public List<FaturamentoTO.obterLotesFaturamneto> ObterLotesFaturamentoPrincipalSemGrupoFat(FaturamentoApiModel.obterDemonstrativoFaturamentoApiModel objPesquisar)
    {
        var DALSQL = new FaturamentoDALSQL();

        string cmdSql = DALSQL.ObterLotesFaturamentoPrincipalSemGrupoFat(objPesquisar);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        dbCommand.AddWithValue("ANOMESREFPASS", objPesquisar.ANOMESREFPASS);
        dbCommand.AddWithValue("CODCLIMODFIL", objPesquisar.CODCLIMODFIL);
        dbCommand.AddWithValue("CODSVCTGVMODFIL", objPesquisar.CODSVCTGVMODFIL);
        dbCommand.AddWithValue("NUMCGCCLIMODFIL", objPesquisar.NUMCGCCLIMODFIL);

        if (objPesquisar.NOMCLIMODFIL != "-1")
            dbCommand.AddWithValue("NOMCLIMODFIL", objPesquisar.NOMCLIMODFIL);
        else
            dbCommand.AddWithValue("NOMCLIMODFIL", -1);


        dbCommand.AddWithValue("ANOMESVIGENTE", objPesquisar.ANOMESVIGENTE);
        dbCommand.AddWithValue("ANOMESVIGENTEPASS", objPesquisar.ANOMESVIGENTEPASS);

        //
        dbCommand.TrataDbCommandUniversal(true, false, true, false, false);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<FaturamentoTO.obterLotesFaturamneto>();
    }

    public List<FaturamentoTO.obterLotesFaturamneto> obterLoteGrupoEconomicoAgrupadoPrincipal(FaturamentoApiModel.obterDemonstrativoFaturamentoApiModel objPesquisar)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.obterLoteGrupoEconomicoAgrupadoPrincipal(objPesquisar);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        dbCommand.AddWithValue("ANOMESREFPASS", objPesquisar.ANOMESREFPASS);
        dbCommand.AddWithValue("CODCLIMODFIL", objPesquisar.CODCLIMODFIL);
        dbCommand.AddWithValue("CODSVCTGVMODFIL", objPesquisar.CODSVCTGVMODFIL);
        dbCommand.AddWithValue("NUMCGCCLIMODFIL", objPesquisar.NUMCGCCLIMODFIL);

        if (objPesquisar.NOMCLIMODFIL != "-1")
            dbCommand.AddWithValue("NOMCLIMODFIL", objPesquisar.NOMCLIMODFIL);
        else
            dbCommand.AddWithValue("NOMCLIMODFIL", -1);

        //
        dbCommand.TrataDbCommandUniversal(true, false, true, false, false);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<FaturamentoTO.obterLotesFaturamneto>();
    }

    public List<FaturamentoTO.obterLotesFaturamneto> obterLoteGrupoEconomicoAgrupadoGrupoFatPrincipal(FaturamentoApiModel.obterDemonstrativoFaturamentoApiModel objPesquisar)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.obterLoteGrupoEconomicoAgrupadoGrupoFatPrincipal(objPesquisar);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        dbCommand.AddWithValue("ANOMESREFPASS", objPesquisar.ANOMESREFPASS);
        dbCommand.AddWithValue("CODCLIMODFIL", objPesquisar.CODCLIMODFIL);
        dbCommand.AddWithValue("CODSVCTGVMODFIL", objPesquisar.CODSVCTGVMODFIL);
        dbCommand.AddWithValue("NUMCGCCLIMODFIL", objPesquisar.NUMCGCCLIMODFIL);

        if (objPesquisar.NOMCLIMODFIL != "-1")
            dbCommand.AddWithValue("NOMCLIMODFIL", objPesquisar.NOMCLIMODFIL);
        else
            dbCommand.AddWithValue("NOMCLIMODFIL", -1);

        //
        dbCommand.TrataDbCommandUniversal(true, false, true, false, false);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<FaturamentoTO.obterLotesFaturamneto>();
    }

    
    public List<FaturamentoTO.obterLotesFaturamneto> obterLoteGrupoEconomicoAgrupadoSemGrupoFatPrincipal(FaturamentoApiModel.obterDemonstrativoFaturamentoApiModel objPesquisar)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.obterLoteGrupoEconomicoAgrupadoSemGrupoFatPrincipal(objPesquisar);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        dbCommand.AddWithValue("ANOMESREFPASS", objPesquisar.ANOMESREFPASS);
        dbCommand.AddWithValue("CODCLIMODFIL", objPesquisar.CODCLIMODFIL);
        dbCommand.AddWithValue("CODSVCTGVMODFIL", objPesquisar.CODSVCTGVMODFIL);
        dbCommand.AddWithValue("NUMCGCCLIMODFIL", objPesquisar.NUMCGCCLIMODFIL);

        if (objPesquisar.NOMCLIMODFIL != "-1")
            dbCommand.AddWithValue("NOMCLIMODFIL", objPesquisar.NOMCLIMODFIL);
        else
            dbCommand.AddWithValue("NOMCLIMODFIL", -1);

        //
        dbCommand.TrataDbCommandUniversal(true, false, true, false, false);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<FaturamentoTO.obterLotesFaturamneto>();
    }

    public List<FaturamentoTO.obterLotesFaturamneto> obterLotesGrupoEconomicoAnalitico(FaturamentoApiModel.obterDemonstrativoFaturamentoApiModel objPesquisar)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.obterLotesGrupoEconomicoAnalitico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        dbCommand.AddWithValue("ANOMESREFPASS", objPesquisar.ANOMESREFPASS);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<FaturamentoTO.obterLotesFaturamneto>();
    }

    public bool CancelarParametroLote(FaturamentoApiModel.cancelarLoteFaturamentoApiModel objCancelar)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.CancelarParametroLote();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objCancelar.ANOMESREF);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool CancelarClientesLote(FaturamentoApiModel.cancelarLoteFaturamentoApiModel objCancelar)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.CancelarClientesLote();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objCancelar.ANOMESREF);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool CanclearDescontosLote(FaturamentoApiModel.cancelarLoteFaturamentoApiModel objCancelar)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.CanclearDescontosLote();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objCancelar.ANOMESREF);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public bool atulizarValidados(FaturamentoApiModel.validarLoteFaturamentoApiModel objCancelar)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.atulizarValidados();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("INDVLRFATVLD", objCancelar.INDVLRFATVLD);
        dbCommand.AddWithValue("ANOMESREF", objCancelar.ANOMESREF);
        dbCommand.AddWithValue("CODCLI", objCancelar.CODCLI);
        dbCommand.AddWithValue("CODSVCTGV", objCancelar.CODSVCTGV);

        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public int obtemNumeroLoteFaturamento()
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.obtemNumeroLoteFaturamento();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToInt32(MRT001.ExecuteScalar(dbCommand));
    }

    public List<FaturamentoTO.obterSomatoriaEmailFaturamento> obterSomatoriaFaturamentos(bool _emailempty)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.obterSomatoriaFaturamentos(_emailempty);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<FaturamentoTO.obterSomatoriaEmailFaturamento>();
    }

    public FaturamentoTO.obterTotaisFaturamento obterTotaisFaturamento()
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.obterTotaisFaturamento();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<FaturamentoTO.obterTotaisFaturamento>().FirstOrDefault();
    }

    public int InsereTabelaEnvioFaturamento(Int64 _numerosequecia, int _codfuncuionario)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.InsereTabelaEnvioFaturamento();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);

        dbCommand.AddWithValue("NUNSEQUENCIA", _numerosequecia);
        dbCommand.AddWithValue("CODFNC", _codfuncuionario);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToInt32(MRT001.ExecuteNonQuery(dbCommand));
    }

    public List<FaturamentoTO.obterTotaisConferencia> obterTotaisConferencia(FaturamentoApiModel.obterDemonstrativoFaturamentoApiModel objPesquisar)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.obterTotaisConferencia();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        dbCommand.AddWithValue("ANOMESREFPASS", objPesquisar.ANOMESREFPASS);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<FaturamentoTO.obterTotaisConferencia>().ToList();
    }

    public List<FaturamentoTO.obterLotesFaturamneto> ObterLotesFaturamentoPrincipalImput(FaturamentoApiModel.obterDemonstrativoFaturamentoApiModel objPesquisar)
    {
        var DALSQL = new FaturamentoDALSQL();

        string cmdSql = DALSQL.ObterLotesFaturamentoPrincipalImput(objPesquisar);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        dbCommand.AddWithValue("ANOMESREFPASS", objPesquisar.ANOMESREFPASS);
        //dbCommand.AddWithValue("CODCLIMODFIL", objPesquisar.CODCLIMODFIL);
        //dbCommand.AddWithValue("CODSVCTGVMODFIL", objPesquisar.CODSVCTGVMODFIL);
        //dbCommand.AddWithValue("NUMCGCCLIMODFIL", objPesquisar.NUMCGCCLIMODFIL);

        //if (objPesquisar.NOMCLIMODFIL != "-1")
        //    dbCommand.AddWithValue("NOMCLIMODFIL", objPesquisar.NOMCLIMODFIL);
        //else
        //    dbCommand.AddWithValue("NOMCLIMODFIL", -1);
        //
        dbCommand.AddWithValue("ANOMESVIGENTE", objPesquisar.ANOMESVIGENTE);
        dbCommand.AddWithValue("ANOMESVIGENTEPASS", objPesquisar.ANOMESVIGENTEPASS);

        dbCommand.TrataDbCommandUniversal(true, false, true, false, false);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<FaturamentoTO.obterLotesFaturamneto>();
    }


    public bool InserirMovimentoParametroAvulso(GestaoContasApiModel.GestaoContasContaInserirServicoAvulsoApiModel objPesquisar)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.InserirMovimentoParametroAvulso();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        dbCommand.AddWithValue("INDFATSVC", objPesquisar.INDFATSVC);
        dbCommand.AddWithValue("CODFNCGRC", objPesquisar.CODFNCGRC);
        dbCommand.AddWithValue("CODFNCFAT", objPesquisar.CODFNCGRC);
        dbCommand.AddWithValue("NUMLOTPRENOTFSCSVC", objPesquisar.NUMLOTPRENOTFSCSVC);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public bool InserirMovimentoClientesContasServicoAvulso(GestaoContasApiModel.GestaoContasContaInserirServicoAvulsoApiModel objPesquisar)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.InserirMovimentoClientesContasServicoAvulso();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        dbCommand.AddWithValue("INDFATSVC", objPesquisar.INDFATSVC);
        dbCommand.AddWithValue("CODFNCGRC", objPesquisar.CODFNCGRC);
        dbCommand.AddWithValue("NUMLOTPRENOTFSCSVC", objPesquisar.NUMLOTPRENOTFSCSVC);
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public bool InserirMovimentoClientesServicoAvulso(GestaoContasApiModel.GestaoContasContaInserirServicoAvulsoApiModel objPesquisar)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.InserirMovimentoClientesServicoAvulso();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        dbCommand.AddWithValue("INDFATSVC", objPesquisar.INDFATSVC);
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("NUMLOTPRENOTFSCSVC", objPesquisar.NUMLOTPRENOTFSCSVC);
        dbCommand.AddWithValue("VLRSVC", decimal.Parse(objPesquisar.VLRSVC));
        dbCommand.AddWithValue("VLRTOTSVC", decimal.Parse(objPesquisar.VLRSVC));
        dbCommand.AddWithValue("INDVLRFATVLD", '1');
        dbCommand.AddWithValue("DESSVCTGV", objPesquisar.DESSVCTGV);
        dbCommand.AddWithValue("DESJSTSLCSVC", objPesquisar.DESJSTSLCSVC);

        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public bool InserirMovimentoClientesParcelasServicoAvulso(GestaoContasApiModel.GestaoContasContaInserirServicoAvulsoApiModel objPesquisar)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.InserirMovimentoClientesParcelasServicoAvulso();
        var result = false;
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //

        if (objPesquisar.DATVNCNOTFSCSVC != null)
        { 
            dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
            dbCommand.AddWithValue("INDFATSVC", objPesquisar.INDFATSVC);
            dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
            dbCommand.AddWithValue("NUMPCLTITNOTFSCSVC", 1);
            dbCommand.AddWithValue("DATVNCNOTFSCSVC", DateTime.Parse(objPesquisar.DATVNCNOTFSCSVC));
            dbCommand.AddWithValue("VLRPCLTIT", decimal.Parse(objPesquisar.VLRPCLTIT));
            dbCommand.AddWithValue("NUMLOTPRENOTFSCSVC", objPesquisar.NUMLOTPRENOTFSCSVC);
            //
            dbCommand.TrataDbCommandUniversal();
            //
            result = Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
        }


        if (objPesquisar.DATVNCNOTFSCSVC2 != null)
        {
            cmdSql = DALSQL.InserirMovimentoClientesParcelasServicoAvulso();
            dbCommand = MRT001.GetSqlStringCommand(cmdSql);

            dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
            dbCommand.AddWithValue("INDFATSVC", objPesquisar.INDFATSVC);
            dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
            dbCommand.AddWithValue("NUMPCLTITNOTFSCSVC", 2);
            dbCommand.AddWithValue("DATVNCNOTFSCSVC", DateTime.Parse(objPesquisar.DATVNCNOTFSCSVC2));
            dbCommand.AddWithValue("VLRPCLTIT", decimal.Parse(objPesquisar.VLRPCLTIT2));
            dbCommand.AddWithValue("NUMLOTPRENOTFSCSVC", objPesquisar.NUMLOTPRENOTFSCSVC);
            //
            dbCommand.TrataDbCommandUniversal();
            //
            result = Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
        }


        if (objPesquisar.DATVNCNOTFSCSVC3 != null)
        {
            cmdSql = DALSQL.InserirMovimentoClientesParcelasServicoAvulso();
            dbCommand = MRT001.GetSqlStringCommand(cmdSql);

            dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
            dbCommand.AddWithValue("INDFATSVC", objPesquisar.INDFATSVC);
            dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
            dbCommand.AddWithValue("NUMPCLTITNOTFSCSVC", 3);
            dbCommand.AddWithValue("DATVNCNOTFSCSVC", DateTime.Parse(objPesquisar.DATVNCNOTFSCSVC3));
            dbCommand.AddWithValue("VLRPCLTIT", decimal.Parse(objPesquisar.VLRPCLTIT3));
            dbCommand.AddWithValue("NUMLOTPRENOTFSCSVC", objPesquisar.NUMLOTPRENOTFSCSVC);
            //
            dbCommand.TrataDbCommandUniversal();
            //
            result = Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
        }


        if (objPesquisar.DATVNCNOTFSCSVC4 != null)
        {
            cmdSql = DALSQL.InserirMovimentoClientesParcelasServicoAvulso();
            dbCommand = MRT001.GetSqlStringCommand(cmdSql);

            dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
            dbCommand.AddWithValue("INDFATSVC", objPesquisar.INDFATSVC);
            dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
            dbCommand.AddWithValue("NUMPCLTITNOTFSCSVC", 4);
            dbCommand.AddWithValue("DATVNCNOTFSCSVC", DateTime.Parse(objPesquisar.DATVNCNOTFSCSVC4));
            dbCommand.AddWithValue("VLRPCLTIT", decimal.Parse(objPesquisar.VLRPCLTIT4));
            dbCommand.AddWithValue("NUMLOTPRENOTFSCSVC", objPesquisar.NUMLOTPRENOTFSCSVC);
            //
            dbCommand.TrataDbCommandUniversal();
            //
            result = Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
        }

        if (objPesquisar.DATVNCNOTFSCSVC5 != null)
        {
            cmdSql = DALSQL.InserirMovimentoClientesParcelasServicoAvulso();
            dbCommand = MRT001.GetSqlStringCommand(cmdSql);

            dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
            dbCommand.AddWithValue("INDFATSVC", objPesquisar.INDFATSVC);
            dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
            dbCommand.AddWithValue("NUMPCLTITNOTFSCSVC", 5);
            dbCommand.AddWithValue("DATVNCNOTFSCSVC", DateTime.Parse(objPesquisar.DATVNCNOTFSCSVC5));
            dbCommand.AddWithValue("VLRPCLTIT", decimal.Parse(objPesquisar.VLRPCLTIT5));
            dbCommand.AddWithValue("NUMLOTPRENOTFSCSVC", objPesquisar.NUMLOTPRENOTFSCSVC);
            //
            dbCommand.TrataDbCommandUniversal();
            //
            result = Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
        }

        if (objPesquisar.DATVNCNOTFSCSVC6 != null)
        {
            cmdSql = DALSQL.InserirMovimentoClientesParcelasServicoAvulso();
            dbCommand = MRT001.GetSqlStringCommand(cmdSql);

            dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
            dbCommand.AddWithValue("INDFATSVC", objPesquisar.INDFATSVC);
            dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
            dbCommand.AddWithValue("NUMPCLTITNOTFSCSVC", 6);
            dbCommand.AddWithValue("DATVNCNOTFSCSVC", DateTime.Parse(objPesquisar.DATVNCNOTFSCSVC6));
            dbCommand.AddWithValue("VLRPCLTIT", decimal.Parse(objPesquisar.VLRPCLTIT6));
            dbCommand.AddWithValue("NUMLOTPRENOTFSCSVC", objPesquisar.NUMLOTPRENOTFSCSVC);
            //
            dbCommand.TrataDbCommandUniversal();
            //
            result = Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
        }


        return result;

    }



    public bool EnviarFaturamentoClienteAvulso(GestaoContasApiModel.GestaoContasContaInserirServicoAvulsoApiModel objPesquisar)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.EnviarFaturamentoClienteAvulso();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODFNC", objPesquisar.CODFNCGRC);
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("NUMLOTPRENOTFSCSVC", objPesquisar.NUMLOTPRENOTFSCSVC);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool EnviarFaturamentoServicoAvulso(GestaoContasApiModel.GestaoContasContaInserirServicoAvulsoApiModel objPesquisar)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.EnviarFaturamentoServicoAvulso();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("NUMLOTPRENOTFSCSVC", objPesquisar.NUMLOTPRENOTFSCSVC);

        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public bool EnviarFaturamentoServicoParcelasAvulso(GestaoContasApiModel.GestaoContasContaInserirServicoAvulsoApiModel objPesquisar)
    {
        var DALSQL = new FaturamentoDALSQL();
        string cmdSql = DALSQL.EnviarFaturamentoServicoParcelasAvulso();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", objPesquisar.ANOMESREF);
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("NUMLOTPRENOTFSCSVC", objPesquisar.NUMLOTPRENOTFSCSVC);

        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


}