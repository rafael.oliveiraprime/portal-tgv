﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class FaturamentoDALSQL
{
    public string InserirInicioLoteFat()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" INSERT INTO MRT.MOVMESFATRSMTGV (ANOMESREF, INDFATSVC, DATINIPCS, CODFNCGRC, NUMLOTPRENOTFSCSVC)
                                                                  VALUES (:ANOMESREF, :INDFATSVC, SYSDATE, :CODFNCGRC, :NUMLOTPRENOTFSCSVC) 
                                      ");
        return strBld.ToString();

    }


    public string FinalizaLoteFat()
    {
        StringBuilder strBld = new StringBuilder(@"  UPDATE MRT.MOVMESFATRSMTGV SET DATFIMPCS = SYSDATE
                                                      WHERE ANOMESREF = :ANOMESREF
                                                 ");

        return strBld.ToString();
    }

    public string AtualizaStatusEnvioFaturamentoLote()
    {
        StringBuilder strBld = new StringBuilder(@"  UPDATE MRT.MOVMESFATRSMTGV SET DATENVFAT = SYSDATE,
                                                                                    CODFNCFAT = :CODFNCFAT
                                                      WHERE ANOMESREF = :ANOMESREF
                                                        AND NUMLOTPRENOTFSCSVC = :NUNSEQUENCIA
                                                 ");

        return strBld.ToString();
    }

    public string deletetaLoteFat()
    {
        StringBuilder strBld = new StringBuilder(@"  DELETE FROM MRT.MOVMESFATRSMTGV WHERE ANOMESREF = :ANOMESREF ");

        return strBld.ToString();
    }


    public string VerificaLoteAberto()
    {
        StringBuilder strBld = new StringBuilder(@"  SELECT ANOMESREF
                                                            ,CASE DATFIMPCS WHEN DATFIMPCS THEN TO_CHAR(DATFIMPCS,'DD/MM/YYYY') ELSE ' ' END DATFIMPCS 
                                                       FROM MRT.MOVMESFATRSMTGV  
                                                      ORDER BY ANOMESREF
                                                 ");

        return strBld.ToString();

    }


    public string VerificaLoteEnviado()
    {
        StringBuilder strBld = new StringBuilder(@"  SELECT ANOMESREF
                                                            ,CASE DATFIMPCS WHEN DATFIMPCS THEN TO_CHAR(DATFIMPCS,'DD/MM/YYYY') ELSE ' ' END DATFIMPCS 
                                                            ,CASE DATENVFAT WHEN DATENVFAT THEN TO_CHAR(DATENVFAT,'DD/MM/YYYY') ELSE ' ' END DATENVFAT       
                                                       FROM MRT.MOVMESFATRSMTGV  
                                                      WHERE rownum = 1 
                                                      ORDER BY ANOMESREF DESC
                                                 ");

        return strBld.ToString();

    }

    public string InserirMovimentoClientesContas()
    {

        StringBuilder strBld;

        strBld = new StringBuilder(@" 
                                        INSERT INTO MRT.MOVMESFATCLITGV(ANOMESREF, INDFATSVC, CODCLI, CODSITCLI, VLRBLTFAT, CODGRPEMPCLI, INDFATGRP, CODCLIGRPFAT, INDDBTAUTPGT, CODBCO, CODAGEBCO, NUMDIGVRFAGE, NUMCNTCRRCTT, NUMDIGVRFCNTCRR, DATGRC, CODFNCGRC, NUMLOTPRENOTFSCSVC)
                                                                    SELECT :ANOMESREF AS ANOMESREF
                                                                           ,1 AS INDFATSVC
                                                                           , GC.CODCLI
                                                                           ,CODSITCLI
                                                                           ,0 AS VLRBLTFAT
                                                                           ,GE.CODGRPEMPCLI AS CODGRPEMPCLI
                                                                           ,0 AS INDFATGRP
                                                                           ,NULL AS CODCLIGRPFAT
                                                                           ,GC.INDDBTAUTPGT
                                                                           ,GC.CODBCO
                                                                           ,GC.CODAGEBCO
                                                                           ,GC.NUMDIGVRFAGE
                                                                           ,GC.NUMCNTCRRCTT
                                                                           ,GC.NUMDIGVRFCNTCRR
                                                                           ,SYSDATE AS DATGRC
                                                                           ,:CODFNCGRC AS CODFNCGRC
                                                                           ,:NUMLOTPRENOTFSCSVC
                                                                        FROM MRT.CADCLITGV GC
                                                                        LEFT JOIN MRT.RLCCLIGRPEMPTGV GEC ON(GC.CODCLI = GEC.CODCLI)
                                                                        LEFT JOIN MRT.CADGRPEMPTGV GE ON(GE.CODGRPEMPCLI = GEC.CODGRPEMPCLI)                                 
                                   ");

        return strBld.ToString();

    }

    public string InserirMovimnetoServicosDescontos()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" INSERT INTO MRT.MOVMESFATCLISVCTGV (ANOMESREF, CODCLI, CODSVCTGV, INDFATSVC, INDSTAATV, VLRSVC, VLRTOTDSC, VLRTOTSVC, DATACEDOC, DATATVSVC, DATCNC, CODMTVCNC, DATINISUSSVC, DATFIMSUSSVC, NUMLOTPRENOTFSCSVC, CODCLIGRPFAT) 
                                                       WITH TABVALORES AS (
                                                                             SELECT :ANOMESREF AS ANOMESREF
                                                                                    ,C.CODCLI
                                                                                    ,S.CODSVCTGV
                                                                                    ,1 AS INDFATSVC 
                                                                                    ,S.INDSTAATV
                                                                                    ,(S.VLRSVC * NVL(S.QDEPRDVND,1)) AS VLRSVC
                                                                                    , SUM(CASE WHEN TRIM(P.VLRPRDCES) = COALESCE(TRIM(M.VLRPRDCES),'SIM') THEN NVL(D.PERDSC,0)
                                                                                           WHEN D.CODPRDCES IS NULL THEN NVL(D.PERDSC,0)
                                                                                           ELSE 0 END) PERDSCTOT
                                                                                    , SUM(CASE WHEN TRIM(P.VLRPRDCES) = COALESCE(TRIM(M.VLRPRDCES),'SIM') THEN NVL(D.VLRDSC,0)
                                                                                           WHEN D.CODPRDCES IS NULL THEN NVL(D.VLRDSC,0)
                                                                                           ELSE 0 END) VLRDSCTOT
                                                                                    ,S.DATACEDOC AS DATACEDOC                      
                                                                                    ,S.DATATVSVC AS DATATVSVC
                                                                                    ,S.DATCNC AS DATCNC
                                                                                    ,S.CODMTVCNC AS CODMTVCNC
                                                                                    ,S.DATINISUSSVC AS DATINISUSSVC
                                                                                    ,S.DATFIMSUSSVC AS DATFIMSUSSVC   
                                                                                    ,CASE WHEN TO_CHAR(S.DATATVSVC,'YYYYMM') = :ANOMESREFANT 
                                                                                          THEN NVL(S.PERCOBMESATVSVC,1) 
                                                                                          WHEN TO_CHAR(S.DATCNC,'YYYYMM') = :ANOMESREFANT 
                                                                                          THEN NVL(S.PERCOBMESCNCSVC,1) 
                                                                                          ELSE 1 END PERCOBMES
                                                                                    ,COUNT(D.CODCLI) QTD
                                                                                    ,GP.CODCLIGRPFAT
                                                                                FROM MRT.CADCLITGV C --ON T.CODCLI = C.CODCLI 
                                                                               INNER JOIN MRT.CADCLISVCTGV S ON C.CODCLI = S.CODCLI-- AND T.CODSVCTGV = S.CODSVCTGV
                                                                                LEFT JOIN MRT.CADCLISVCDSCTGV D ON S.CODCLI = D.CODCLI 
                                                                                      AND S.CODSVCTGV = D.CODSVCTGV 
                                                                                      AND :ANOMESREFANT BETWEEN COALESCE(TO_CHAR(D.DATINIVLD,'YYYYMM'),:ANOMESREFANT) AND COALESCE(TO_CHAR(D.DATFIMVLD,'YYYYMM'), :ANOMESREFANT) 
                                                                                      AND D.DATDST IS NULL
                                                                                LEFT JOIN MRT.MOVCESPRDTGV P ON P.CODPRDCES = D.CODPRDCES AND C.CODCLI = P.CODCLI  AND P.ANOMESREF = :ANOMESREFANT
                                                                                LEFT JOIN MRT.CADCNDCMCTGV M ON M.CODCNDCMC = D.CODCNDCMC
                                                                                LEFT JOIN MRT.CADGRPEMPTGV GP ON S.CODGRPEMPFAT = GP.CODGRPEMPCLI
                                                                            GROUP BY C.CODCLI
                                                                                    ,S.CODSVCTGV
                                                                                    ,S.VLRSVC
                                                                                    ,S.INDSTAATV
                                                                                    ,S.DATACEDOC                      
                                                                                    ,S.DATATVSVC
                                                                                    ,S.DATCNC
                                                                                    ,S.CODMTVCNC
                                                                                    ,S.DATINISUSSVC
                                                                                    ,S.DATFIMSUSSVC
                                                                                    ,S.PERCOBMESATVSVC 
                                                                                    ,S.PERCOBMESCNCSVC
                                                                                    ,S.QDEPRDVND
                                                                                    ,GP.CODCLIGRPFAT
                                                                            ) 
                                                                            SELECT  T.ANOMESREF
                                                                                 , T.CODCLI
                                                                                 , T.CODSVCTGV
                                                                                 , T.INDFATSVC 
                                                                                 , T.INDSTAATV
                                                                                 , T.VLRSVC * T.PERCOBMES VLRSVC
                                                                                 ,(T.VLRSVC * T.PERCOBMES) - ROUND(((T.VLRSVC * T.PERCOBMES) - (VLRDSCTOT * T.PERCOBMES)) * DECODE(PERDSCTOT,0,1,1-(PERDSCTOT/100)),2) VLRTOTDSC     
                                                                                 , ROUND(((T.VLRSVC*T.PERCOBMES) - (VLRDSCTOT*T.PERCOBMES)) * DECODE(PERDSCTOT,0,1,1-(PERDSCTOT/100)),2) VLRTOTSVC
                                                                                 , T.DATACEDOC                      
                                                                                 , T.DATATVSVC
                                                                                 , T.DATCNC
                                                                                 , T.CODMTVCNC
                                                                                 , T.DATINISUSSVC
                                                                                 , T.DATFIMSUSSVC
                                                                                 , :NUMLOTPRENOTFSCSVC
                                                                                 , T.CODCLIGRPFAT
                                                                            FROM TABVALORES T
                                  ");

        return strBld.ToString();

    }

    public string atulizarValidados()
    {

        StringBuilder strBld;

        strBld = new StringBuilder(@"  UPDATE MRT.MOVMESFATCLISVCTGV SET INDVLRFATVLD = :INDVLRFATVLD
                                        WHERE ANOMESREF = :ANOMESREF   
                                          AND CODCLI    = :CODCLI
                                          AND CODSVCTGV = :CODSVCTGV
                                  ");

        return strBld.ToString();

    }


    public string ObterLotesFaturamentoPrincipal(FaturamentoApiModel.obterDemonstrativoFaturamentoApiModel _objPesquisar)
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@"     WITH TABPRINCIPAL AS ( SELECT M.ANOMESREF
                                                                        , M.INDFATSVC
                                                                        , M.CODCLI
                                                                        , S.CODSVCTGV
                                                                        , S.INDSTAATV                                                                                
                                                                        , S.VLRTOTSVC           
                                                                        , S.CODCLIGRPFAT
                                                                        , M.CODGRPEMPCLI
                                                                        , S.DATATVSVC
                                                                        , S.DATCNC
                                                                        , S.DATINISUSSVC
                                                                        , S.DATFIMSUSSVC
                                                                        , S.INDVLRFATVLD
                                                                    FROM MRT.MOVMESFATCLITGV  M
                                                                   INNER JOIN MRT.MOVMESFATCLISVCTGV S ON S.ANOMESREF = M.ANOMESREF AND S.CODCLI = M.CODCLI AND M.INDFATSVC = S.INDFATSVC
                                                                   INNER JOIN MRT.T0100043 CL ON(M.CODCLI = CL.CODCLI)
                                                                   WHERE S.INDSTAATV <> 1 
                                                                     AND M.INDFATSVC = 1
                                                                     AND S.DATATVSVC IS NOT NULL
                                                                     AND (S.DATCNC IS NULL OR TO_CHAR(S.DATCNC, 'YYYYMM') >= :ANOMESVIGENTE) 
                                                                     AND M.ANOMESREF = :ANOMESREF
                                                                     AND M.CODCLI = :CODCLIMODFIL
                                                                     AND S.CODSVCTGV = :CODSVCTGVMODFIL
                                                                     AND CL.NUMCGCCLI = :NUMCGCCLIMODFIL
                                                                     AND CL.NOMCLI LIKE '%' || UPPER(:NOMCLIMODFIL) || '%'
                                                                   #INPUTOUTPUT
                                                               ), 
                                                TABPASSADO AS ( SELECT M.ANOMESREF
                                                                       , M.INDFATSVC
                                                                       , M.CODCLI
                                                                       , S.CODSVCTGV                                                                               
                                                                       , S.VLRTOTSVC  
                                                                       , S.INDSTAATV
                                                                       , S.DATATVSVC DATATVSVCPASS
                                                                       , S.DATCNC DATCNCPASS
                                                                       , S.DATINISUSSVC DATINISUSSVCPASS
                                                                       , S.DATFIMSUSSVC DATFIMSUSSVCPASS
                                                                       , S.INDVLRFATVLD INDVLRFATVLDPASS
                                                                  FROM MRT.MOVMESFATCLITGV  M
                                                                 INNER JOIN MRT.MOVMESFATCLISVCTGV S ON S.ANOMESREF = M.ANOMESREF AND S.CODCLI = M.CODCLI AND M.INDFATSVC = S.INDFATSVC
                                                                 INNER JOIN MRT.T0100043 CL ON(M.CODCLI = CL.CODCLI)
                                                                 WHERE S.INDSTAATV <> 1
                                                                   AND M.INDFATSVC = 1
                                                                   AND S.DATATVSVC IS NOT NULL
                                                                   AND (S.DATCNC IS NULL OR TO_CHAR(S.DATCNC, 'YYYYMM') >= :ANOMESVIGENTEPASS) 
                                                                   AND M.ANOMESREF = :ANOMESREFPASS
                                                                   AND M.CODCLI = :CODCLIMODFIL 
                                                                   AND S.CODSVCTGV = :CODSVCTGVMODFIL
                                                                   AND CL.NUMCGCCLI = :NUMCGCCLIMODFIL
                                                                   AND CL.NOMCLI LIKE '%' || UPPER(:NOMCLIMODFIL) || '%'
                                                                   #INPUTOUTPUTPASS
                                                               )");



            strBld.Append(@" SELECT COALESCE(A.ANOMESREF,B.ANOMESREF) ANOMESREFP
                                                    , CLI.CODCLI
                                                    , CLI.NOMCLI
                                                    , CG.NOMGRPEMPCLI
                                                    , GE.NOMGRPEMPCLI AS NOMGRUPOCLI
                                                    , NVL(CG.CODGRPEMPCLI, 0) CODGRPEMPCLI
                                                    , B.ANOMESREF  ANOMESPASS
                                                    , SUM(B.VLRTOTSVC) VLRTOTSVCPASS
                                                    , A.ANOMESREF ANOMESREF  
                                                    , SUM(A.VLRTOTSVC) VLRTOTSVC ");


            strBld.Append(@", SV.DESSVCTGV DESSERPASS
                            , CASE WHEN B.INDSTAATV = 1 THEN 'EM IMPLANTAÇÃO'
                                WHEN B.INDSTAATV = 2 THEN 'ATIVO'
                                WHEN B.INDSTAATV = 3 THEN 'SUSPENSO'
                                WHEN B.INDSTAATV = 4 THEN 'CANCELADO'  END STATUSPASS 
                            , SV.CODSVCTGV CODSVCTGV
                            , SV.DESSVCTGV
                            , CASE WHEN A.INDSTAATV = 1 THEN 'EM IMPLANTAÇÃO'
                                WHEN A.INDSTAATV = 2 THEN 'ATIVO'
                                WHEN A.INDSTAATV = 3 THEN 'SUSPENSO'
                                WHEN A.INDSTAATV = 4 THEN 'CANCELADO'  END STATUS   
                            , TO_CHAR(A.DATATVSVC,'DD/MM/YYYY') DATATVSVC
                            , TO_CHAR(A.DATCNC,'DD/MM/YYYY') DATCNC
                            , TO_CHAR(A.DATINISUSSVC,'DD/MM/YYYY') DATINISUSSVC
                            , TO_CHAR(A.DATFIMSUSSVC,'DD/MM/YYYY') DATFIMSUSSVC
                            , TO_CHAR(B.DATATVSVCPASS,'DD/MM/YYYY') DATATVSVCPASS
                            , TO_CHAR(B.DATCNCPASS,'DD/MM/YYYY') DATCNCPASS
                            , TO_CHAR(B.DATINISUSSVCPASS,'DD/MM/YYYY') DATINISUSSVCPASS
                            , TO_CHAR(B.DATFIMSUSSVCPASS,'DD/MM/YYYY') DATFIMSUSSVCPASS
                            , 0 QTDATIVOSPASS
                            , 0 VALORPASSADO
                            , 0 QTDATIVOS
                            , 0 VALORTOTAL
                            , case when (SUM(A.VLRTOTSVC) = SUM(B.VLRTOTSVC)) then 0 else 1 end  as MENSALIDADE 
                            , case when ((B.INDSTAATV > A.INDSTAATV ) OR (B.INDSTAATV IS NULL)) then 1 else 0 end  as INPUT                                 
                            , case when (B.INDSTAATV < A.INDSTAATV ) then 1 else 0 end  as OUTPUT
                            , case when (B.INDSTAATV = A.INDSTAATV ) then 0 else 1 end  as STATUSSEMDIVERGENCIA                                
                            , A.INDVLRFATVLD AS VALIDADADOS 
                        ");
                                
 

            strBld.Append(@" FROM TABPRINCIPAL A
                             FULL JOIN TABPASSADO B ON A.CODCLI = B.CODCLI AND A.CODSVCTGV = B.CODSVCTGV
                            INNER JOIN MRT.T0100043 CLI ON CLI.CODCLI = COALESCE(A.CODCLI,B.CODCLI)   
                             LEFT JOIN MRT.T0100043 CLIFAT ON CLIFAT.CODCLI = A.CODCLIGRPFAT
                             LEFT JOIN MRT.CADGRPEMPTGV CG ON CG.CODCLIGRPFAT = A.CODCLIGRPFAT
                             LEFT JOIN MRT.CADGRPEMPTGV GE ON GE.CODGRPEMPCLI = A.CODGRPEMPCLI
                            INNER JOIN MRT.CADSVCTGV SV ON SV.CODSVCTGV = COALESCE(A.CODSVCTGV,B.CODSVCTGV) ");


            if (_objPesquisar.STATUS == 1)
                strBld.Append(" AND A.INDSTAATV = 2");
            else if (_objPesquisar.STATUS == 2)
                strBld.Append(" AND A.INDSTAATV = 3");
            else if (_objPesquisar.STATUS == 3)
                strBld.Append(" AND A.INDSTAATV = 4");


            if (_objPesquisar.TIPODIVERGENCIA == 1)
            {
                strBld.Append(" AND A.VLRTOTSVC = B.VLRTOTSVC " +
                              " AND A.INDSTAATV = B.INDSTAATV ");

                strBld.Replace("#INPUTOUTPUTPASS", "");
                strBld.Replace("#INPUTOUTPUT", "");
            }
            else if (_objPesquisar.TIPODIVERGENCIA == 2)
            {
                strBld.Append(" AND A.VLRTOTSVC <> B.VLRTOTSVC ");

                //strBld.Replace("#INPUTOUTPUTPASS", "");
                //strBld.Replace("#INPUTOUTPUT", "");

                strBld.Replace("#INPUTOUTPUTPASS", "AND S.INDSTAATV = 2 ");
                strBld.Replace("#INPUTOUTPUT", "AND S.INDSTAATV = 2 ");
            }
            else if (_objPesquisar.TIPODIVERGENCIA == 3)
            {

                strBld.Append(" AND A.INDSTAATV <> B.INDSTAATV ");

                strBld.Replace("#INPUTOUTPUTPASS", "AND S.INDSTAATV IN(3, 4)");
                strBld.Replace("#INPUTOUTPUT", "AND S.INDSTAATV IN(2)");
            }
            else if (_objPesquisar.TIPODIVERGENCIA == 4)
            {
                strBld.Append(" AND A.INDSTAATV <> B.INDSTAATV ");

                strBld.Replace("#INPUTOUTPUTPASS", "AND S.INDSTAATV IN(2)");
                strBld.Replace("#INPUTOUTPUT", "AND S.INDSTAATV IN(3, 4)");
            }
            else if (_objPesquisar.TIPODIVERGENCIA == 5)
            {
                strBld.Append(" AND A.INDVLRFATVLD = 1 ");

                strBld.Replace("#INPUTOUTPUTPASS", "AND S.INDVLRFATVLD = 1");
                strBld.Replace("#INPUTOUTPUT", "AND S.INDVLRFATVLD = 1");
            }
            else
            {
                strBld.Replace("#INPUTOUTPUTPASS", "");
                strBld.Replace("#INPUTOUTPUT", "");
            }

            strBld.Append(@" GROUP BY A.ANOMESREF
                                     , B.ANOMESREF
                                     , CLI.CODCLI 
                                     , CLI.NOMCLI 
                                     , CG.NOMGRPEMPCLI
                                     , CG.CODGRPEMPCLI
                                     , GE.NOMGRPEMPCLI ");


            strBld.Append(@" , SV.CODSVCTGV 
                    , SV.DESSVCTGV
                    , B.ANOMESREF  
                    , B.INDSTAATV 
                    , A.INDSTAATV    
                    , A.DATATVSVC
                    , A.DATCNC
                    , A.DATINISUSSVC
                    , A.DATFIMSUSSVC
                    , B.DATATVSVCPASS
                    , B.DATCNCPASS
                    , B.DATINISUSSVCPASS
                    , B.DATFIMSUSSVCPASS
                    , A.INDVLRFATVLD
                    ");

            strBld.Append(@" ORDER BY CLI.NOMCLI, CLI.CODCLI ");


        return strBld.ToString();
    }


    public string obterLoteGrupoEconomicoAgrupadoPrincipal(FaturamentoApiModel.obterDemonstrativoFaturamentoApiModel _objPesquisar)
    {

        StringBuilder strBld;

        strBld = new StringBuilder(@"   WITH TABPRINCIPAL AS ( SELECT A.ANOMESREF
                                                                        , C.CODCLI
                                                                        , C.NOMCLI
                                                                        --, DECODE(GC.INDDBTAUTPGT,0,1,0) INDBLTIPR
                                                                        , SUM(S.VLRTOTSVC) AS VLRTOTSVC
                                                                        , M.INDFATGRP
                                                                        , S.CODCLIGRPFAT AS CODGRPEMPCLI
                                                                        , ' ' AS STATUS
                                                                        , S.INDVLRFATVLD
                                                             FROM MRT.MOVMESFATRSMTGV A
                                                            INNER JOIN MRT.MOVMESFATCLITGV M ON A.ANOMESREF = M.ANOMESREF AND A.INDFATSVC = M.INDFATSVC
                                                            INNER JOIN MRT.MOVMESFATCLISVCTGV S ON S.ANOMESREF = M.ANOMESREF AND S.INDFATSVC = M.INDFATSVC AND S.CODCLI = M.CODCLI
                                                            INNER JOIN MRT.T0100043 C ON C.CODCLI = DECODE(S.CODCLIGRPFAT, null, M.CODCLI, S.CODCLIGRPFAT)
                                                            WHERE S.INDSTAATV = 2
                                                              AND A.INDFATSVC = 1
                                                              AND A.ANOMESREF = :ANOMESREF                    
                                                              AND M.CODCLI = :CODCLIMODFIL 
                                                              AND S.CODSVCTGV = :CODSVCTGVMODFIL
                                                              AND CL.NUMCGCCLI = :NUMCGCCLIMODFIL
                                                              AND CL.NOMCLI LIKE '%' || UPPER(:NOMCLIMODFIL) || '%'
                                                            GROUP BY A.ANOMESREF   
                                                                 , C.CODCLI
                                                                 , C.NOMCLI
                                                                 , M.INDDBTAUTPGT
                                                                 , M.INDFATGRP
                                                                 , S.CODCLIGRPFAT                         
                                                                 , S.INDVLRFATVLD                        
                                                                 ),
                                              TABPASSADO AS ( SELECT A.ANOMESREF  AS ANOMESREFPASS
                                                                 , C.CODCLI  
                                                                 , C.NOMCLI  
                                                                 --, DECODE(GC.INDDBTAUTPGT,0,1,0) INDBLTIPR
                                                                 , SUM(S.VLRTOTSVC) AS VLRTOTSVCPASS
                                                                 , M.INDFATGRP
                                                                 , S.CODCLIGRPFAT AS CODGRPEMPCLI
                                                                 , ' ' STATUS
                                                            FROM MRT.MOVMESFATRSMTGV A
                                                            INNER JOIN MRT.MOVMESFATCLITGV M ON A.ANOMESREF = M.ANOMESREF AND A.INDFATSVC = M.INDFATSVC
                                                            INNER JOIN MRT.MOVMESFATCLISVCTGV S ON S.ANOMESREF = M.ANOMESREF AND S.INDFATSVC = M.INDFATSVC AND S.CODCLI = M.CODCLI
                                                            INNER JOIN MRT.T0100043 C ON C.CODCLI = DECODE(S.CODCLIGRPFAT, null, M.CODCLI, S.CODCLIGRPFAT)
                                                            WHERE S.INDSTAATV = 2
                                                              AND A.INDFATSVC = 1
                                                              AND A.ANOMESREF = :ANOMESREFPASS
                                                              AND M.CODCLI = :CODCLIMODFIL 
                                                              AND S.CODSVCTGV = :CODSVCTGVMODFIL
                                                              AND CL.NUMCGCCLI = :NUMCGCCLIMODFIL
                                                              AND CL.NOMCLI LIKE '%' || UPPER(:NOMCLIMODFIL) || '%'
                                                            GROUP BY A.ANOMESREF   
                                                                 , C.CODCLI
                                                                 , C.NOMCLI
                                                                 , M.INDDBTAUTPGT
                                                                 , M.INDFATGRP
                                                                 , S.CODCLIGRPFAT
                                                                 )                                                                                                                       
                                                                    SELECT COALESCE(TB.ANOMESREF,TBP.ANOMESREFPASS) ANOMESREFP
                                                                            ,TB.CODCLI
                                                                            ,TB.NOMCLI
                                                                            ,CG.NOMGRPEMPCLI
                                                                            , ' ' AS NOMGRUPOCLI
		                                                                        ,NVL(CG.CODGRPEMPCLI, 0) CODGRPEMPCLI                                    
		                                                                        ,TBP.ANOMESREFPASS ANOMESPASS                                    
		                                                                        ,TB.ANOMESREF  ANOMESREF                                                                        
                                                                            ,TB.VLRTOTSVC AS VLRTOTSVC
                                                                            ,TBP.VLRTOTSVCPASS AS VLRTOTSVCPASS 
                                                                            ,TB.STATUS
                                                                            ,TBP.STATUS AS STATUSPASS
                                                                            ,0 CODSVCTGV
                                                                            ,' ' DESSVCTGV 
                                                                            , ' ' DATATVSVC
                                                                            , ' ' DATCNC
                                                                            , ' ' DATINISUSSVC
                                                                            , ' ' DATFIMSUSSVC
                                                                            , ' ' DATATVSVCPASS
                                                                            , ' ' DATCNCPASS
                                                                            , ' ' DATINISUSSVCPASS
                                                                            , ' ' DATFIMSUSSVCPASS
                                                                            , 0 QTDATIVOSPASS
                                                                            , ' ' DESSERPASS
                                                                            , 0 VALORPASSADO
                                                                            , 0 QTDATIVOS
                                                                            , 0 VALORTOTAL
                                                                            , 0 as MENSALIDADE --case when (TB.VLRTOTSVC = TBP.VLRTOTSVCPASS) then 0 else 1 end 
                                                                            , 0 as INPUT                                 
                                                                            , 0 as OUTPUT
                                                                            , 0 STATUSSEMDIVERGENCIA                                
                                                                            , TB.INDVLRFATVLD AS VALIDADADOS                                                                                                        
                                                                       FROM TABPRINCIPAL TB
                                                                       LEFT JOIN TABPASSADO TBP ON(TB.CODCLI = TBP.CODCLI)
                                                                       LEFT JOIN MRT.CADGRPEMPTGV CG ON CG.CODCLIGRPFAT = TB.CODGRPEMPCLI
                                                                       WHERE 1 = 1 
                                    ");

        strBld.Append(@" ORDER BY CG.NOMGRPEMPCLI ");



        return strBld.ToString();

    }
    
    public string obterLoteGrupoEconomicoAgrupadoGrupoFatPrincipal(FaturamentoApiModel.obterDemonstrativoFaturamentoApiModel _objPesquisar)
    {

        StringBuilder strBld;

        strBld = new StringBuilder(@"   WITH TABPRINCIPAL AS ( SELECT A.ANOMESREF
                                                                        , C.CODCLI
                                                                        , C.NOMCLI
                                                                        --,DECODE(GC.INDDBTAUTPGT,0,1,0) INDBLTIPR
                                                                        , SUM(S.VLRTOTSVC) AS VLRTOTSVC
                                                                        , M.INDFATGRP
                                                                        , S.CODCLIGRPFAT AS CODGRPEMPCLI
                                                                        , ' ' AS STATUS
                                                                        , S.INDVLRFATVLD
                                                             FROM MRT.MOVMESFATRSMTGV A
                                                            INNER JOIN MRT.MOVMESFATCLITGV M ON A.ANOMESREF = M.ANOMESREF AND A.INDFATSVC = M.INDFATSVC
                                                            INNER JOIN MRT.MOVMESFATCLISVCTGV S ON S.ANOMESREF = M.ANOMESREF AND S.INDFATSVC = M.INDFATSVC AND S.CODCLI = M.CODCLI
                                                            INNER JOIN MRT.T0100043 C ON C.CODCLI = DECODE(S.CODCLIGRPFAT, null, M.CODCLI, S.CODCLIGRPFAT)
                                                            WHERE S.INDSTAATV = 2
                                                              AND A.INDFATSVC = 1
                                                              AND S.CODCLIGRPFAT IS NOT NULL
                                                              AND A.ANOMESREF = :ANOMESREF                    
                                                              AND M.CODCLI = :CODCLIMODFIL 
                                                              AND S.CODSVCTGV = :CODSVCTGVMODFIL
                                                              AND CL.NUMCGCCLI = :NUMCGCCLIMODFIL
                                                              AND CL.NOMCLI LIKE '%' || UPPER(:NOMCLIMODFIL) || '%'
                                                            GROUP BY A.ANOMESREF   
                                                                 , C.CODCLI
                                                                 , C.NOMCLI
                                                                 , M.INDDBTAUTPGT
                                                                 , M.INDFATGRP
                                                                 , S.CODCLIGRPFAT                         
                                                                 , S.INDVLRFATVLD                        
                                                                 ),
                                              TABPASSADO AS ( SELECT A.ANOMESREF  AS ANOMESREFPASS
                                                                 , C.CODCLI  
                                                                 , C.NOMCLI  
                                                                 --, DECODE(GC.INDDBTAUTPGT,0,1,0) INDBLTIPR
                                                                 , SUM(S.VLRTOTSVC) AS VLRTOTSVCPASS
                                                                 , M.INDFATGRP
                                                                 , S.CODCLIGRPFAT AS CODGRPEMPCLI
                                                                 , ' ' STATUS
                                                            FROM MRT.MOVMESFATRSMTGV A
                                                            INNER JOIN MRT.MOVMESFATCLITGV M ON A.ANOMESREF = M.ANOMESREF AND A.INDFATSVC = M.INDFATSVC
                                                            INNER JOIN MRT.MOVMESFATCLISVCTGV S ON S.ANOMESREF = M.ANOMESREF AND S.INDFATSVC = M.INDFATSVC AND S.CODCLI = M.CODCLI
                                                            INNER JOIN MRT.T0100043 C ON C.CODCLI = DECODE(S.CODCLIGRPFAT, null, M.CODCLI, S.CODCLIGRPFAT)
                                                            WHERE S.INDSTAATV = 2
                                                              AND A.INDFATSVC = 1
                                                              AND S.CODCLIGRPFAT IS NOT NULL
                                                              AND A.ANOMESREF = :ANOMESREFPASS
                                                              AND M.CODCLI = :CODCLIMODFIL 
                                                              AND S.CODSVCTGV = :CODSVCTGVMODFIL
                                                              AND CL.NUMCGCCLI = :NUMCGCCLIMODFIL
                                                              AND CL.NOMCLI LIKE '%' || UPPER(:NOMCLIMODFIL) || '%'
                                                            GROUP BY A.ANOMESREF   
                                                                 , C.CODCLI
                                                                 , C.NOMCLI
                                                                 , M.INDDBTAUTPGT
                                                                 , M.INDFATGRP
                                                                 , S.CODCLIGRPFAT
                                                                 )                                                                                                                       
                                                                    SELECT COALESCE(TB.ANOMESREF,TBP.ANOMESREFPASS) ANOMESREFP
                                                                            ,TB.CODCLI
                                                                            ,TB.NOMCLI
                                                                            ,CG.NOMGRPEMPCLI
                                                                            , ' ' AS NOMGRUPOCLI
		                                                                        ,NVL(CG.CODGRPEMPCLI, 0) CODGRPEMPCLI                                    
		                                                                        ,TBP.ANOMESREFPASS ANOMESPASS                                    
		                                                                        ,TB.ANOMESREF  ANOMESREF                                                                        
                                                                            ,TB.VLRTOTSVC AS VLRTOTSVC
                                                                            ,TBP.VLRTOTSVCPASS AS VLRTOTSVCPASS 
                                                                            ,TB.STATUS
                                                                            ,TBP.STATUS AS STATUSPASS
                                                                            ,0 CODSVCTGV
                                                                            ,' ' DESSVCTGV 
                                                                            , ' ' DATATVSVC
                                                                            , ' ' DATCNC
                                                                            , ' ' DATINISUSSVC
                                                                            , ' ' DATFIMSUSSVC
                                                                            , ' ' DATATVSVCPASS
                                                                            , ' ' DATCNCPASS
                                                                            , ' ' DATINISUSSVCPASS
                                                                            , ' ' DATFIMSUSSVCPASS
                                                                            , 0 QTDATIVOSPASS
                                                                            , ' ' DESSERPASS
                                                                            , 0 VALORPASSADO
                                                                            , 0 QTDATIVOS
                                                                            , 0 VALORTOTAL
                                                                            , 0 as MENSALIDADE --case when (TB.VLRTOTSVC = TBP.VLRTOTSVCPASS) then 0 else 1 end 
                                                                            , 0 as INPUT                                 
                                                                            , 0 as OUTPUT
                                                                            , 0 STATUSSEMDIVERGENCIA                                
                                                                            , TB.INDVLRFATVLD AS VALIDADADOS                                                                                                        
                                                                       FROM TABPRINCIPAL TB
                                                                       LEFT JOIN TABPASSADO TBP ON(TB.CODCLI = TBP.CODCLI)
                                                                       LEFT JOIN MRT.CADGRPEMPTGV CG ON CG.CODCLIGRPFAT = TB.CODGRPEMPCLI
                                                                       WHERE 1 = 1 
                                    ");

        strBld.Append(@" ORDER BY CG.NOMGRPEMPCLI ");



        return strBld.ToString();

    }

    
    public string obterLoteGrupoEconomicoAgrupadoSemGrupoFatPrincipal(FaturamentoApiModel.obterDemonstrativoFaturamentoApiModel _objPesquisar)
    {

        StringBuilder strBld;

        strBld = new StringBuilder(@"   WITH TABPRINCIPAL AS ( SELECT A.ANOMESREF
                                                                        , C.CODCLI
                                                                        , C.NOMCLI
                                                                        --,DECODE(GC.INDDBTAUTPGT,0,1,0) INDBLTIPR
                                                                        , SUM(S.VLRTOTSVC) AS VLRTOTSVC
                                                                        , M.INDFATGRP
                                                                        , S.CODCLIGRPFAT AS CODGRPEMPCLI
                                                                        , ' ' AS STATUS
                                                                        , S.INDVLRFATVLD
                                                             FROM MRT.MOVMESFATRSMTGV A
                                                            INNER JOIN MRT.MOVMESFATCLITGV M ON A.ANOMESREF = M.ANOMESREF AND A.INDFATSVC = M.INDFATSVC
                                                            INNER JOIN MRT.MOVMESFATCLISVCTGV S ON S.ANOMESREF = M.ANOMESREF AND S.INDFATSVC = M.INDFATSVC AND S.CODCLI = M.CODCLI
                                                            INNER JOIN MRT.T0100043 C ON C.CODCLI = DECODE(S.CODCLIGRPFAT, null, M.CODCLI, S.CODCLIGRPFAT)
                                                            WHERE S.INDSTAATV = 2
                                                              AND A.INDFATSVC = 1
                                                              AND S.CODCLIGRPFAT IS NULL
                                                              AND A.ANOMESREF = :ANOMESREF                    
                                                              AND M.CODCLI = :CODCLIMODFIL 
                                                              AND S.CODSVCTGV = :CODSVCTGVMODFIL
                                                              AND CL.NUMCGCCLI = :NUMCGCCLIMODFIL
                                                              AND CL.NOMCLI LIKE '%' || UPPER(:NOMCLIMODFIL) || '%'
                                                            GROUP BY A.ANOMESREF   
                                                                 , C.CODCLI
                                                                 , C.NOMCLI
                                                                 , M.INDDBTAUTPGT
                                                                 , M.INDFATGRP
                                                                 , S.CODCLIGRPFAT                         
                                                                 , S.INDVLRFATVLD                        
                                                                 ),
                                              TABPASSADO AS ( SELECT A.ANOMESREF  AS ANOMESREFPASS
                                                                 , C.CODCLI  
                                                                 , C.NOMCLI  
                                                                 --, DECODE(GC.INDDBTAUTPGT,0,1,0) INDBLTIPR
                                                                 , SUM(S.VLRTOTSVC) AS VLRTOTSVCPASS
                                                                 , M.INDFATGRP
                                                                 , S.CODCLIGRPFAT AS CODGRPEMPCLI
                                                                 , ' ' STATUS
                                                            FROM MRT.MOVMESFATRSMTGV A
                                                            INNER JOIN MRT.MOVMESFATCLITGV M ON A.ANOMESREF = M.ANOMESREF AND A.INDFATSVC = M.INDFATSVC
                                                            INNER JOIN MRT.MOVMESFATCLISVCTGV S ON S.ANOMESREF = M.ANOMESREF AND S.INDFATSVC = M.INDFATSVC AND S.CODCLI = M.CODCLI
                                                            INNER JOIN MRT.T0100043 C ON C.CODCLI = DECODE(S.CODCLIGRPFAT, null, M.CODCLI, S.CODCLIGRPFAT)
                                                            WHERE S.INDSTAATV = 2
                                                              AND A.INDFATSVC = 1
                                                              AND S.CODCLIGRPFAT IS NULL
                                                              AND A.ANOMESREF = :ANOMESREFPASS
                                                              AND M.CODCLI = :CODCLIMODFIL 
                                                              AND S.CODSVCTGV = :CODSVCTGVMODFIL
                                                              AND CL.NUMCGCCLI = :NUMCGCCLIMODFIL
                                                              AND CL.NOMCLI LIKE '%' || UPPER(:NOMCLIMODFIL) || '%'
                                                            GROUP BY A.ANOMESREF   
                                                                 , C.CODCLI
                                                                 , C.NOMCLI
                                                                 , M.INDDBTAUTPGT
                                                                 , M.INDFATGRP
                                                                 , S.CODCLIGRPFAT
                                                                 )                                                                                                                       
                                                                    SELECT COALESCE(TB.ANOMESREF,TBP.ANOMESREFPASS) ANOMESREFP
                                                                            ,TB.CODCLI
                                                                            ,TB.NOMCLI
                                                                            ,CG.NOMGRPEMPCLI
                                                                            , ' ' AS NOMGRUPOCLI
		                                                                        ,NVL(CG.CODGRPEMPCLI, 0) CODGRPEMPCLI                                    
		                                                                        ,TBP.ANOMESREFPASS ANOMESPASS                                    
		                                                                        ,TB.ANOMESREF  ANOMESREF                                                                        
                                                                            ,TB.VLRTOTSVC AS VLRTOTSVC
                                                                            ,TBP.VLRTOTSVCPASS AS VLRTOTSVCPASS 
                                                                            ,TB.STATUS
                                                                            ,TBP.STATUS AS STATUSPASS
                                                                            ,0 CODSVCTGV
                                                                            ,' ' DESSVCTGV 
                                                                            , ' ' DATATVSVC
                                                                            , ' ' DATCNC
                                                                            , ' ' DATINISUSSVC
                                                                            , ' ' DATFIMSUSSVC
                                                                            , ' ' DATATVSVCPASS
                                                                            , ' ' DATCNCPASS
                                                                            , ' ' DATINISUSSVCPASS
                                                                            , ' ' DATFIMSUSSVCPASS
                                                                            , 0 QTDATIVOSPASS
                                                                            , ' ' DESSERPASS
                                                                            , 0 VALORPASSADO
                                                                            , 0 QTDATIVOS
                                                                            , 0 VALORTOTAL
                                                                            , 0 as MENSALIDADE --case when (TB.VLRTOTSVC = TBP.VLRTOTSVCPASS) then 0 else 1 end 
                                                                            , 0 as INPUT                                 
                                                                            , 0 as OUTPUT
                                                                            , 0 STATUSSEMDIVERGENCIA                                
                                                                            , TB.INDVLRFATVLD AS VALIDADADOS                                                                                                        
                                                                       FROM TABPRINCIPAL TB
                                                                       LEFT JOIN TABPASSADO TBP ON(TB.CODCLI = TBP.CODCLI)
                                                                       LEFT JOIN MRT.CADGRPEMPTGV CG ON CG.CODCLIGRPFAT = TB.CODGRPEMPCLI
                                                                       WHERE 1 = 1 
                                    ");

        strBld.Append(@" ORDER BY CG.NOMGRPEMPCLI ");



        return strBld.ToString();

    }


    public string obterLotesGrupoEconomicoAnalitico()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" 
                                        WITH TABCLIPASSADO AS (
                                                SELECT MV.ANOMESREF
                                                        ,MV.CODCLI
                                                        ,CLI.NOMCLI
                                                        ,CG.NOMGRPEMPCLI
                                                        ,MVS.CODSVCTGV
                                                        ,SR.DESSVCTGV
                                                        ,CASE WHEN MVS.INDSTAATV = 1 THEN 'EM IMPLANTAÇÃO'
                                                              WHEN MVS.INDSTAATV = 2 THEN 'ATIVO'
                                                              WHEN MVS.INDSTAATV = 3 THEN 'SUSPENSO'
                                                              WHEN MVS.INDSTAATV = 4 THEN 'CANCELADO'  END STATUSPASS                                                                                                                
                                                        ,MVS.VLRTOTSVC 
                                                        ,(SELECT COUNT(OP.CODCLI) FROM MRT.MOVMESFATCLITGV OP WHERE OP.ANOMESREF = :ANOMESREFPASS AND  OP.CODSITCLI = 2) AS QTDATIVOS                                                
                                                        ,(SELECT SUM(OP.VLRTOTSVC) FROM MRT.MOVMESFATCLISVCTGV OP WHERE OP.ANOMESREF = :ANOMESREFPASS AND  OP.INDSTAATV = 2) AS VALORTOTAL                                                
                                                    FROM MRT.MOVMESFATCLITGV  MV
                                                    INNER JOIN MRT.T0100043 CLI ON CLI.CODCLI = MV.CODCLI   
                                                    INNER JOIN MRT.MOVMESFATCLISVCTGV MVS ON MVS.ANOMESREF = MV.ANOMESREF AND MVS.CODCLI = MV.CODCLI
                                                    INNER JOIN MRT.CADGRPEMPTGV CG ON CG.CODCLIGRPFAT = MVS.CODCLIGRPFAT 
                                                    INNER JOIN MRT.CADSVCTGV SR ON (MVS.CODSVCTGV = SR.CODSVCTGV)
                                                    WHERE MV.ANOMESREF = :ANOMESREFPASS 
                                                      AND (MVS.DATCNC IS NULL OR TO_CHAR(MVS.DATCNC, 'YYYYMM') >= :ANOMESVIGENTEPASS)
                                        )
                                        SELECT MV.ANOMESREF
                                                ,MV.CODCLI
                                                ,CLI.NOMCLI
                                                ,CG.NOMGRPEMPCLI
                                                ,' ' AS NOMGRUPOCLI
                                                ,MVS.CODSVCTGV
                                                ,SR.DESSVCTGV
                                                ,CASE WHEN MVS.INDSTAATV = 1 THEN 'EM IMPLANTAÇÃO'
                                                      WHEN MVS.INDSTAATV = 2 THEN 'ATIVO'
                                                      WHEN MVS.INDSTAATV = 3 THEN 'SUSPENSO'
                                                      WHEN MVS.INDSTAATV = 4 THEN 'CANCELADO'  END STATUS                                                                                                                                                               
                                                ,MVS.VLRTOTSVC 
                                                ,MVS.DATATVSVC
                                                ,MVS.DATCNC
                                                ,MVS.DATINISUSSVC
                                                ,MVS.DATFIMSUSSVC
                                                ,TBP.ANOMESREF AS ANOMESPASS
                                                ,TBP.CODSVCTGV AS CODSERPASS
                                                ,TBP.DESSVCTGV AS DESSERPASS
                                                ,TBP.STATUSPASS
                                                ,TBP.VLRTOTSVC AS VLRTOTSVCPASS 
                                                ,TBP.QTDATIVOS AS QTDATIVOSPASS
                                                ,TBP.VALORTOTAL AS VALORPASSADO
                                                ,(SELECT COUNT(OP.CODCLI) FROM MRT.MOVMESFATCLITGV OP WHERE OP.ANOMESREF = :ANOMESREF AND  OP.CODSITCLI = 2) AS QTDATIVOS                                                                                                
                                                ,(SELECT SUM(OP.VLRTOTSVC) FROM MRT.MOVMESFATCLISVCTGV OP WHERE OP.ANOMESREF = :ANOMESREF AND  OP.INDSTAATV = 2) AS VALORTOTAL     
                                            FROM MRT.MOVMESFATCLITGV  MV
                                           INNER JOIN MRT.T0100043 CLI ON CLI.CODCLI = MV.CODCLI   
                                           INNER JOIN MRT.MOVMESFATCLISVCTGV MVS ON MVS.ANOMESREF = MV.ANOMESREF AND MVS.CODCLI = MV.CODCLI
                                           INNER JOIN MRT.CADGRPEMPTGV CG ON CG.CODCLIGRPFAT = MVS.CODCLIGRPFAT 
                                           INNER JOIN MRT.CADSVCTGV SR ON MVS.CODSVCTGV = SR.CODSVCTGV
                                            LEFT JOIN TABCLIPASSADO TBP ON TBP.CODCLI = MV.CODCLI AND TBP.CODSVCTGV = MVS.CODSVCTGV
                                           WHERE MV.ANOMESREF = :ANOMESREF 
                                             AND (MVS.DATCNC IS NULL OR TO_CHAR(MVS.DATCNC, 'YYYYMM') >= :ANOMESVIGENTE)
                                           ORDER BY CG.NOMGRPEMPCLI, CLI.NOMCLI                                     
                                  ");

        return strBld.ToString();

    }






    public string CancelarParametroLote()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" DELETE FROM MRT.MOVMESFATRSMTGV WHERE ANOMESREF = :ANOMESREF ");

        return strBld.ToString();
    }

    public string CancelarClientesLote()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" DELETE FROM MRT.MOVMESFATCLITGV WHERE ANOMESREF = :ANOMESREF ");

        return strBld.ToString();
    }

    public string CanclearDescontosLote()
    {       
        StringBuilder strBld;

        strBld = new StringBuilder(@" DELETE FROM MRT.MOVMESFATCLISVCTGV WHERE ANOMESREF = :ANOMESREF ");

        return strBld.ToString();
    }

    public string obtemNumeroLoteFaturamento()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT MRT.CADPRENOTFSCSVC_NUMLOTPRENOT.NEXTVAL FROM DUAL  ");

        return strBld.ToString();
    }


    public string obterSomatoriaFaturamentos(bool _emailempty)
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" with TABELA_ENVIO as ( SELECT A.ANOMESREF
                                                                     , C.CODCLI
                                                                     , C.NOMCLI
                                                                     , SUBSTR(RTRIM(NVL(LGR.DESTIPLGR, ' ')) ||' '||  RTRIM(E.DESLGR) ||', '|| TRIM(E.NUMENDLGR),1,35) ENDDSN
                                                                     , E.CODBAI
                                                                     , E.CODCEP
                                                                     , TRIM(C.NUMINSESTCLI) NUMINSESTCLI
                                                                     , C.NUMCGCCLI
                                                                     --, ' ' DESMSG
                                                                     , LGR.DESABVTIPLGR
                                                                     , TRIM(E.DESLGR) DESLGR
                                                                     , TRIM(E.NUMENDLGR) NUMENDLGR
                                                                     , DECODE( TRIM(E.DESCPLEND),'',' ',TRIM(E.DESCPLEND)) DESCPLEND
                                                                     , ' ' ENDCREETN -- ver com roberto qual é contato pra enviar
                                                                     , 1 AS INDBLTIPR
                                                                  --, DECODE(GC.INDDBTAUTPGT,0,1,0) INDBLTIPR
                                                                  --   , S.CODSVCTGV
                                                                     , SUM(S.VLRTOTSVC) AS VLRTOTSVC
                                                                     , M.INDFATGRP
                                                                     , A.NUMLOTPRENOTFSCSVC
                                                                FROM MRT.MOVMESFATRSMTGV A
                                                                INNER JOIN MRT.MOVMESFATCLITGV M ON A.ANOMESREF = M.ANOMESREF AND A.INDFATSVC = M.INDFATSVC
                                                                INNER JOIN MRT.MOVMESFATCLISVCTGV S ON S.ANOMESREF = M.ANOMESREF AND S.INDFATSVC = M.INDFATSVC AND S.CODCLI = M.CODCLI
                                                                INNER JOIN MRT.T0100043 C ON C.CODCLI = DECODE(S.CODCLIGRPFAT, null, M.CODCLI, S.CODCLIGRPFAT)
                                                                INNER JOIN MRT.T0138938 E ON E.CODCLI = C.CODCLI
                                                                                    AND E.TIPENDCLI = (SELECT MAX(E2.TIPENDCLI)
                                                                                                       FROM MRT.T0138938 E2
                                                                                                       WHERE E2.CODCLI = C.CODCLI
                                                                                                       AND E2.TIPENDCLI IN (0,1,2))
                                                                LEFT JOIN MRT.T0103905 CPL ON CPL.CODCPLBAI = E.CODCPLBAI
                                                                LEFT JOIN MRT.T0138946 LGR ON LGR.TIPLGR = E.TIPLGR
                                                                WHERE A.DATENVFAT IS NULL
                                                                  AND S.INDSTAATV = 2 -- CONFIRMAR SE ESSE É O STATUS DOS SERVIÇOS ATIVOS
                                                                  AND A.INDFATSVC = 1
                                                                GROUP BY A.ANOMESREF   
                                                                     , C.CODCLI
                                                                     , C.NOMCLI
                                                                     , SUBSTR(RTRIM(NVL(LGR.DESTIPLGR, ' ')) ||' '||  RTRIM(E.DESLGR) ||', '|| TRIM(E.NUMENDLGR),1,35) 
                                                                     , E.CODBAI
                                                                     , E.CODCEP
                                                                     , C.NUMINSESTCLI
                                                                     , C.NUMCGCCLI
                                                                     , LGR.DESABVTIPLGR
                                                                     , E.DESLGR
                                                                     , E.NUMENDLGR
                                                                     , E.DESCPLEND
                                                                     --, M.INDDBTAUTPGT
                                                                     , M.INDFATGRP
                                                                     , A.NUMLOTPRENOTFSCSVC
                                                                     )                                                                      
                                                                    SELECT TB.ANOMESREF
                                                                          , TB.CODCLI
                                                                          , TB.NOMCLI
                                                                          , GC.INDDBTAUTPGT AS INDBLTIPR
                                                                          , TB.VLRTOTSVC
                                                                          , TB.INDFATGRP
                                                                          , TB.NUMLOTPRENOTFSCSVC
                                                                          ,(SELECT NVL(DESENDETNCTO, '') FROM MRT.CADCTOTGV CTO WHERE CTO.CODEDE = TB.CODCLI AND CTO.CODTIPCTO = 7 AND ROWNUM = 1 AND CTO.DATDST IS NULL) ENDCREETN
                                                                      FROM TABELA_ENVIO TB
                                                                     INNER JOIN MRT.CADCLITGV GC ON GC.CODCLI = TB.CODCLI
                                                                     WHERE VLRTOTSVC >= 30  
                                  ");



        //if (_emailempty)
        //    strBld.Append(@" AND CTO.DESENDETNCTO IS NULL ");   
        //strBld.Append(@" GROUP BY A.ANOMESREF    
        //                        , C.CODCLI
        //                        , C.NOMCLI
        //                        , M.INDDBTAUTPGT
        //                        , M.INDFATGRP
        //                        , CTO.DESENDETNCTO ");

        return strBld.ToString();
    }

    public string obterTotaisFaturamento()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" with TABELA_ENVIO as ( SELECT A.ANOMESREF
                                                                     , C.CODCLI
                                                                     , C.NOMCLI
                                                                     , SUBSTR(RTRIM(NVL(LGR.DESTIPLGR, ' ')) ||' '||  RTRIM(E.DESLGR) ||', '|| TRIM(E.NUMENDLGR),1,35) ENDDSN
                                                                     , E.CODBAI
                                                                     , E.CODCEP
                                                                     , TRIM(C.NUMINSESTCLI) NUMINSESTCLI
                                                                     , C.NUMCGCCLI
                                                                     --, ' ' DESMSG
                                                                     , LGR.DESABVTIPLGR
                                                                     , TRIM(E.DESLGR) DESLGR
                                                                     , TRIM(E.NUMENDLGR) NUMENDLGR
                                                                     , DECODE( TRIM(E.DESCPLEND),'',' ',TRIM(E.DESCPLEND)) DESCPLEND
                                                                     , ' ' ENDCREETN -- ver com roberto qual é contato pra enviar
                                                                  -- , DECODE(GC.INDDBTAUTPGT,0,1,0) INDBLTIPR
                                                                  -- , S.CODSVCTGV
                                                                     , SUM(S.VLRTOTSVC) AS VLRTOTSVC
                                                                     , M.INDFATGRP
                                                                FROM MRT.MOVMESFATRSMTGV A
                                                                INNER JOIN MRT.MOVMESFATCLITGV M ON A.ANOMESREF = M.ANOMESREF AND A.INDFATSVC = M.INDFATSVC
                                                                INNER JOIN MRT.MOVMESFATCLISVCTGV S ON S.ANOMESREF = M.ANOMESREF AND S.INDFATSVC = M.INDFATSVC AND S.CODCLI = M.CODCLI
                                                                INNER JOIN MRT.T0100043 C ON C.CODCLI = DECODE(S.CODCLIGRPFAT, null, M.CODCLI, S.CODCLIGRPFAT)
                                                                INNER JOIN MRT.T0138938 E ON E.CODCLI = C.CODCLI
                                                                                    AND E.TIPENDCLI = (SELECT MAX(E2.TIPENDCLI)
                                                                                                       FROM MRT.T0138938 E2
                                                                                                       WHERE E2.CODCLI = C.CODCLI
                                                                                                       AND E2.TIPENDCLI IN (0,1,2))
                                                                LEFT JOIN MRT.T0103905 CPL ON CPL.CODCPLBAI = E.CODCPLBAI
                                                                LEFT JOIN MRT.T0138946 LGR ON LGR.TIPLGR = E.TIPLGR
                                                                WHERE A.DATENVFAT IS NULL
                                                                  AND S.INDSTAATV = 2 -- CONFIRMAR SE ESSE É O STATUS DOS SERVIÇOS ATIVOS
                                                                  AND A.INDFATSVC = 1
                                                                GROUP BY A.ANOMESREF   
                                                                     , C.CODCLI
                                                                     , C.NOMCLI
                                                                     , SUBSTR(RTRIM(NVL(LGR.DESTIPLGR, ' ')) ||' '||  RTRIM(E.DESLGR) ||', '|| TRIM(E.NUMENDLGR),1,35) 
                                                                     , E.CODBAI
                                                                     , E.CODCEP
                                                                     , C.NUMINSESTCLI
                                                                     , C.NUMCGCCLI
                                                                     , LGR.DESABVTIPLGR
                                                                     , E.DESLGR
                                                                     , E.NUMENDLGR
                                                                     , E.DESCPLEND
                                                                     --, CTO.DESENDETNCTO 
                                                                     --, M.INDDBTAUTPGT
                                                                     , M.INDFATGRP
                                                                     )                                                                      
                                                                    SELECT COUNT(ANOMESREF) AS QTDTOTAL, SUM(VLRTOTSVC) AS VLRTOTAL  FROM TABELA_ENVIO WHERE VLRTOTSVC >= 30  ");

        return strBld.ToString();

    }

    public string InsereTabelaEnvioFaturamento()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@"INSERT ALL
                                    INTO MRT.CADPRENOTFSCSVC VALUES (CODEMP, CODFILEMP, NUMLOTPRENOTFSCSVC, NUMPRENOTFSCSVC, CODUNDNGC, TIPDSNNOTFSC, CODEMPCADFRN, CODFILEMPCADFRN, CODDSNNOTFSC, NOMDSN, ENDDSN, CODBAI, CODCEPCLI, NUMINSEST, NUMCGCDSN, DESMSG, NUMNOTFSCSVC, DATIPRNOTFSCSVC, CODFNCGRC, DATHRAFNCGRC, IDTSITNOTFSCSVC, DESABVTIPLGR, DESLGR, NUMENDLGR, DESCPLEND, ENDCREETN, INDIPRBLT)
                                    INTO MRT.CADPREITENOTFSCSVC VALUES (CODEMP, CODFILEMP, NUMLOTPRENOTFSCSVC, NUMPRENOTFSCSVC, CODSVC, VLRUNTBRTSVC, VLRTOTDSC, QDEUNDSVC, INDMESREF, QDEPCLTITNOTFSC, VLRDSCCNSMRT, VLRDSCCNSTBO, VLROTRDSCCNS)           
                                                 with TABELA_ENVIO as ( SELECT A.ANOMESREF
                                                                         , C.CODCLI
                                                                         , C.NOMCLI
                                                                         , SUBSTR(RTRIM(NVL(LGR.DESTIPLGR, ' ')) ||' '||  RTRIM(E.DESLGR) ||', '|| TRIM(E.NUMENDLGR),1,35) ENDDSN
                                                                         , E.CODBAI
                                                                         , E.CODCEP
                                                                         , TRIM(C.NUMINSESTCLI) NUMINSESTCLI
                                                                         , C.NUMCGCCLI
                                                                         --, ' ' DESMSG
                                                                         , LGR.DESABVTIPLGR
                                                                         , TRIM(E.DESLGR) DESLGR
                                                                         , TRIM(E.NUMENDLGR) NUMENDLGR
                                                                         , DECODE( TRIM(E.DESCPLEND),'',' ',TRIM(E.DESCPLEND)) DESCPLEND
                                                                         , ' ' ENDCREETN -- ver com roberto qual é contato pra enviar
                                                                         ,1 AS INDBLTIPR
                                                                         --, DECODE(M.INDDBTAUTPGT,0,1,0) INDBLTIPR
                                                                      --   , S.CODSVCTGV
                                                                         , SUM(S.VLRTOTSVC) AS VLRTOTSVC
                                                                         , M.INDFATGRP
                                                                    FROM MRT.MOVMESFATRSMTGV A
                                                                    INNER JOIN MRT.MOVMESFATCLITGV M ON A.ANOMESREF = M.ANOMESREF AND A.INDFATSVC = M.INDFATSVC
                                                                    INNER JOIN MRT.MOVMESFATCLISVCTGV S ON S.ANOMESREF = M.ANOMESREF AND S.INDFATSVC = M.INDFATSVC AND S.CODCLI = M.CODCLI
                                                                    INNER JOIN MRT.T0100043 C ON C.CODCLI = DECODE(S.CODCLIGRPFAT, null, M.CODCLI, S.CODCLIGRPFAT)
                                                                    INNER JOIN MRT.T0138938 E ON E.CODCLI = C.CODCLI
                                                                                        AND E.TIPENDCLI = (SELECT MAX(E2.TIPENDCLI)
                                                                                                           FROM MRT.T0138938 E2
                                                                                                           WHERE E2.CODCLI = C.CODCLI
                                                                                                           AND E2.TIPENDCLI IN (0,1,2))
                                                                    LEFT JOIN MRT.T0103905 CPL ON CPL.CODCPLBAI = E.CODCPLBAI
                                                                    LEFT JOIN MRT.T0138946 LGR ON LGR.TIPLGR = E.TIPLGR
                                                                    WHERE A.DATENVFAT IS NULL
                                                                      AND S.INDSTAATV = 2 -- CONFIRMAR SE ESSE É O STATUS DOS SERVIÇOS ATIVOS
                                                                      AND A.INDFATSVC = 1
                                                                    GROUP BY A.ANOMESREF   
                                                                         , C.CODCLI
                                                                         , C.NOMCLI
                                                                         , SUBSTR(RTRIM(NVL(LGR.DESTIPLGR, ' ')) ||' '||  RTRIM(E.DESLGR) ||', '|| TRIM(E.NUMENDLGR),1,35) 
                                                                         , E.CODBAI
                                                                         , E.CODCEP
                                                                         , C.NUMINSESTCLI
                                                                         , C.NUMCGCCLI
                                                                         , LGR.DESABVTIPLGR
                                                                         , E.DESLGR
                                                                         , E.NUMENDLGR
                                                                         , E.DESCPLEND
                                                                         --, CTO.DESENDETNCTO 
                                                                         --, M.INDDBTAUTPGT
                                                                         , M.INDFATGRP
                                                                         )                                                                     
                                                                            SELECT  1 AS CODEMP
                                                                                    ,143 AS CODFILEMP
                                                                                    ,:NUNSEQUENCIA AS NUMLOTPRENOTFSCSVC
                                                                                    ,ROWNUM AS NUMPRENOTFSCSVC 
                                                                                    ,1 AS CODUNDNGC
                                                                                    ,0 AS TIPDSNNOTFSC
                                                                                    ,1 AS CODEMPCADFRN
                                                                                    ,143 AS CODFILEMPCADFRN
                                                                                    ,TB.CODCLI AS CODDSNNOTFSC
                                                                                    ,TB.NOMCLI AS NOMDSN
                                                                                    ,TB.ENDDSN AS ENDDSN
                                                                                    ,TB.CODBAI AS CODBAI
                                                                                    ,TB.CODCEP AS CODCEPCLI
                                                                                    ,TB.NUMINSESTCLI AS NUMINSEST
                                                                                    ,TB.NUMCGCCLI AS  NUMCGCDSN
                                                                                    ,' ' AS DESMSG
                                                                                    ,0  AS NUMNOTFSCSVC -- NÃO SEI 
                                                                                    ,TO_DATE(SYSDATE,'YYYY-MM-DD') AS DATIPRNOTFSCSVC
                                                                                    ,:CODFNC AS CODFNCGRC
                                                                                    ,SYSTIMESTAMP AS DATHRAFNCGRC
                                                                                    ,0 AS IDTSITNOTFSCSVC
                                                                                    ,TB.DESABVTIPLGR AS DESABVTIPLGR
                                                                                    ,TB.DESLGR AS DESLGR
                                                                                    ,TB.NUMENDLGR AS NUMENDLGR
                                                                                    ,TB.DESCPLEND AS DESCPLEND
                                                                                    ,CTO.DESENDETNCTO AS ENDCREETN
                                                                                    ,DECODE(GC.INDDBTAUTPGT,0,1,0) AS INDIPRBLT
                                                                                   ,123 AS CODSVC
                                                                                   ,TB.VLRTOTSVC AS VLRUNTBRTSVC
                                                                                   ,0 AS VLRTOTDSC
                                                                                   ,1 AS QDEUNDSVC
                                                                                   ,1 AS INDMESREF
                                                                                   ,1 AS QDEPCLTITNOTFSC
                                                                                   , null AS VLRDSCCNSMRT
                                                                                   , null AS VLRDSCCNSTBO
                                                                                   , null AS VLROTRDSCCNS
                                                                                FROM TABELA_ENVIO TB
                                                                                LEFT JOIN MRT.CADCTOTGV CTO ON CTO.CODEDE = TB.CODCLI AND CTO.CODTIPCTO = 7 AND CTO.DATDST IS NULL -- FISCAL                                                                            
                                                                               INNER JOIN MRT.CADCLITGV GC ON GC.CODCLI = TB.CODCLI
                                                                               WHERE VLRTOTSVC >= 30

                                                          ");

        return strBld.ToString();
    }



    public string obterTotaisConferencia()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@"     WITH TABPRINCIPAL AS (  SELECT A.ANOMESREF
                                                                        , C.CODCLI
                                                                        , SUM(S.VLRTOTSVC) AS VLRTOTSVC
                                                                FROM MRT.MOVMESFATRSMTGV A
                                                                INNER JOIN MRT.MOVMESFATCLITGV M ON A.ANOMESREF = M.ANOMESREF AND A.INDFATSVC = M.INDFATSVC
                                                                INNER JOIN MRT.MOVMESFATCLISVCTGV S ON S.ANOMESREF = M.ANOMESREF AND S.INDFATSVC = M.INDFATSVC AND S.CODCLI = M.CODCLI
                                                                INNER JOIN MRT.T0100043 C ON C.CODCLI = DECODE(S.CODCLIGRPFAT, null, M.CODCLI, S.CODCLIGRPFAT)
                                                                WHERE A.ANOMESREF = :ANOMESREF
                                                                  AND S.INDSTAATV = 2 
                                                                  AND A.INDFATSVC = 1
                                                               HAVING SUM(S.VLRTOTSVC) >= 30
                                                                GROUP BY A.ANOMESREF, C.CODCLI
                                                            ),                          
                                            TABPASSADO AS (  SELECT A.ANOMESREF
                                                                    , C.CODCLI
                                                                    , SUM(S.VLRTOTSVC) AS VLRTOTSVC
                                                                FROM MRT.MOVMESFATRSMTGV A
                                                                INNER JOIN MRT.MOVMESFATCLITGV M ON A.ANOMESREF = M.ANOMESREF AND A.INDFATSVC = M.INDFATSVC
                                                                INNER JOIN MRT.MOVMESFATCLISVCTGV S ON S.ANOMESREF = M.ANOMESREF AND S.INDFATSVC = M.INDFATSVC AND S.CODCLI = M.CODCLI
                                                                INNER JOIN MRT.T0100043 C ON C.CODCLI = DECODE(S.CODCLIGRPFAT, null, M.CODCLI, S.CODCLIGRPFAT)
                                                                WHERE A.ANOMESREF = :ANOMESREFPASS
                                                                  AND S.INDSTAATV = 2
                                                                  AND A.INDFATSVC = 1
                                                                HAVING SUM(S.VLRTOTSVC) >= 30
                                                                GROUP BY A.ANOMESREF, C.CODCLI
                                                            ) SELECT ANOMESREF ANOMESREF, COUNT(CODCLI) QUANTICLI, SUM(VLRTOTSVC) VLRTOTSVC--, B.ANOMESREF ANOMESREFPASS, COUNT(B.CODCLI) QUANTICLIPASS, SUM(B.VLRTOTSVC) VLRTOTSVCPASS                                  
                                                                FROM TABPRINCIPAL 
                                                            GROUP BY ANOMESREF
                                                            UNION
                                                            SELECT ANOMESREF, COUNT(CODCLI) QUANTICLI, SUM(VLRTOTSVC) VLRTOTSVC                                
                                                                FROM TABPASSADO 
                                                            GROUP BY ANOMESREF
                                ");


        return strBld.ToString();
    }

    public string ObterLotesFaturamentoPrincipalImput(FaturamentoApiModel.obterDemonstrativoFaturamentoApiModel _objPesquisar)
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@"     WITH TABPRINCIPAL AS ( SELECT M.ANOMESREF
                                                                      , M.INDFATSVC
                                                                      , M.CODCLI
                                                                      , S.CODSVCTGV
                                                                      , S.INDSTAATV                                                                                
                                                                      , S.VLRTOTSVC           
                                                                      , S.CODCLIGRPFAT
                                                                      , M.CODGRPEMPCLI
                                                                      , S.DATATVSVC
                                                                      , S.DATCNC
                                                                      , S.DATINISUSSVC
                                                                      , S.DATFIMSUSSVC
                                                                      , S.INDVLRFATVLD
                                                                  FROM MRT.MOVMESFATCLITGV  M
                                                                 INNER JOIN MRT.MOVMESFATCLISVCTGV S ON S.ANOMESREF = M.ANOMESREF AND S.CODCLI = M.CODCLI AND M.INDFATSVC = S.INDFATSVC
                                                                 INNER JOIN MRT.T0100043 CL ON(M.CODCLI = CL.CODCLI)
                                                                 WHERE S.INDSTAATV <> 1 
                                                                   AND M.INDFATSVC = 1
                                                                   AND M.ANOMESREF = :ANOMESREF
                                                                   AND (S.DATCNC IS NULL OR TO_CHAR(S.DATCNC, 'YYYYMM') >= :ANOMESVIGENTE) 
                                                             ), 
                                              TABPASSADO AS ( SELECT M.ANOMESREF
                                                                     , M.INDFATSVC
                                                                     , M.CODCLI
                                                                     , S.CODSVCTGV                                                                               
                                                                     , S.VLRTOTSVC  
                                                                     , S.INDSTAATV
                                                                     , S.DATATVSVC DATATVSVCPASS
                                                                     , S.DATCNC DATCNCPASS
                                                                     , S.DATINISUSSVC DATINISUSSVCPASS
                                                                     , S.DATFIMSUSSVC DATFIMSUSSVCPASS
                                                                     , S.INDVLRFATVLD INDVLRFATVLDPASS
                                                                FROM MRT.MOVMESFATCLITGV  M
                                                               INNER JOIN MRT.MOVMESFATCLISVCTGV S ON S.ANOMESREF = M.ANOMESREF AND S.CODCLI = M.CODCLI AND M.INDFATSVC = S.INDFATSVC
                                                               INNER JOIN MRT.T0100043 CL ON(M.CODCLI = CL.CODCLI)
                                                               WHERE S.INDSTAATV <> 1
                                                                 AND M.INDFATSVC = 1
                                                                 AND M.ANOMESREF = :ANOMESREFPASS
                                                                 AND (S.DATCNC IS NULL OR TO_CHAR(S.DATCNC, 'YYYYMM') >= :ANOMESVIGENTEPASS) 
                                                             ) 
                                           SELECT COALESCE(A.ANOMESREF,B.ANOMESREF) ANOMESREFP
                                                  , CLI.CODCLI
                                                  , CLI.NOMCLI
                                                  , CG.NOMGRPEMPCLI
                                                  , NVL(CG.CODGRPEMPCLI, 0) CODGRPEMPCLI
                                                  , GE.NOMGRPEMPCLI AS NOMGRUPOCLI
                                                  , B.ANOMESREF  ANOMESPASS
                                                  , SUM(B.VLRTOTSVC) VLRTOTSVCPASS
                                                  , A.ANOMESREF ANOMESREF  
                                                  , SUM(A.VLRTOTSVC) VLRTOTSVC , SV.DESSVCTGV DESSERPASS
                                            , CASE WHEN B.INDSTAATV = 1 THEN 'EM IMPLANTAÇÃO'
                                                                                WHEN B.INDSTAATV = 2 THEN 'ATIVO'
                                                                                WHEN B.INDSTAATV = 3 THEN 'SUSPENSO'
                                                                                WHEN B.INDSTAATV = 4 THEN 'CANCELADO'  END STATUSPASS 
                                                                            , SV.CODSVCTGV CODSVCTGV
                                                                            , SV.DESSVCTGV
                                                                            , CASE WHEN A.INDSTAATV = 1 THEN 'EM IMPLANTAÇÃO'
                                                                                WHEN A.INDSTAATV = 2 THEN 'ATIVO'
                                                                                WHEN A.INDSTAATV = 3 THEN 'SUSPENSO'
                                                                                WHEN A.INDSTAATV = 4 THEN 'CANCELADO'  END STATUS   
                                                                            , TO_CHAR(A.DATATVSVC,'DD/MM/YYYY') DATATVSVC
                                                                            , TO_CHAR(A.DATCNC,'DD/MM/YYYY') DATCNC
                                                                            , TO_CHAR(A.DATINISUSSVC,'DD/MM/YYYY') DATINISUSSVC
                                                                            , TO_CHAR(A.DATFIMSUSSVC,'DD/MM/YYYY') DATFIMSUSSVC
                                                                            , TO_CHAR(B.DATATVSVCPASS,'DD/MM/YYYY') DATATVSVCPASS
                                                                            , TO_CHAR(B.DATCNCPASS,'DD/MM/YYYY') DATCNCPASS
                                                                            , TO_CHAR(B.DATINISUSSVCPASS,'DD/MM/YYYY') DATINISUSSVCPASS
                                                                            , TO_CHAR(B.DATFIMSUSSVCPASS,'DD/MM/YYYY') DATFIMSUSSVCPASS
                                                                            , 0 QTDATIVOSPASS
                                                                            , 0 VALORPASSADO
                                                                            , 0 QTDATIVOS
                                                                            , 0 VALORTOTAL
                                                                            ,  0 AS MENSALIDADE 
                                                                            ,  1 AS INPUT                                 
                                                                            ,  0 AS OUTPUT
                                                                            ,  0 AS STATUSSEMDIVERGENCIA                                
                                                                            , A.INDVLRFATVLD AS VALIDADADOS 
                                                                         FROM TABPRINCIPAL A
                                                                         FULL JOIN TABPASSADO B ON A.INDFATSVC = B.INDFATSVC AND A.CODCLI = B.CODCLI AND A.CODSVCTGV = B.CODSVCTGV
                                                                        INNER JOIN MRT.T0100043 CLI ON CLI.CODCLI = COALESCE(A.CODCLI,B.CODCLI)   
                                                                         LEFT JOIN MRT.T0100043 CLIFAT ON CLIFAT.CODCLI = A.CODCLIGRPFAT
                                                                         LEFT JOIN MRT.CADGRPEMPTGV CG ON CG.CODCLIGRPFAT = A.CODCLIGRPFAT
                                                                         LEFT JOIN MRT.CADGRPEMPTGV GE ON GE.CODGRPEMPCLI = A.CODGRPEMPCLI
                                                                         LEFT JOIN MRT.CADSVCTGV SV ON SV.CODSVCTGV = COALESCE(A.CODSVCTGV,B.CODSVCTGV)  
                                                                        WHERE B.INDSTAATV IS NULL AND A.INDSTAATV = 2
                                                                        GROUP BY A.ANOMESREF
                                                                                 , B.ANOMESREF
                                                                                 , CLI.CODCLI 
                                                                                 , CLI.NOMCLI 
                                                                                 , CG.NOMGRPEMPCLI
                                                                                 , CG.CODGRPEMPCLI  
                                                                                 , GE.NOMGRPEMPCLI
                                                                                 , SV.CODSVCTGV 
                                                                                , SV.DESSVCTGV
                                                                                , B.ANOMESREF  
                                                                                , B.INDSTAATV 
                                                                                , A.INDSTAATV    
                                                                                , A.DATATVSVC
                                                                                , A.DATCNC
                                                                                , A.DATINISUSSVC
                                                                                , A.DATFIMSUSSVC
                                                                                , B.DATATVSVCPASS
                                                                                , B.DATCNCPASS
                                                                                , B.DATINISUSSVCPASS
                                                                                , B.DATFIMSUSSVCPASS
                                                                                , A.INDVLRFATVLD
                                                                                 ORDER BY CLI.NOMCLI, CLI.CODCLI 
                                        ");


        return strBld.ToString();
    }


    public string InserirMovimentoParametroAvulso()
    {

        StringBuilder strBld;

        strBld = new StringBuilder(@" 
                                        INSERT INTO MRT.MOVMESFATRSMTGV ( ANOMESREF, INDFATSVC, DATINIPCS, DATFIMPCS, CODFNCGRC, DATENVFAT, CODFNCFAT, NUMLOTPRENOTFSCSVC)
                                                                 VALUES ( :ANOMESREF, :INDFATSVC, SYSDATE, SYSDATE, :CODFNCGRC, SYSDATE, :CODFNCFAT, :NUMLOTPRENOTFSCSVC)
                                   ");

        return strBld.ToString();

    }

    public string InserirMovimentoClientesContasServicoAvulso()
    {

        StringBuilder strBld;

        strBld = new StringBuilder(@" 
                                        INSERT INTO MRT.MOVMESFATCLITGV (ANOMESREF, INDFATSVC, CODCLI, CODSITCLI, VLRBLTFAT, CODGRPEMPCLI, INDFATGRP, CODCLIGRPFAT, INDDBTAUTPGT, CODBCO, CODAGEBCO, NUMDIGVRFAGE, NUMCNTCRRCTT, NUMDIGVRFCNTCRR, DATGRC, CODFNCGRC, NUMLOTPRENOTFSCSVC, DATIPRNOTFSCSVC)
                                                                        SELECT :ANOMESREF AS ANOMESREF
                                                                               ,:INDFATSVC AS INDFATSVC
                                                                               ,CODCLI
                                                                               ,CODSITCLI
                                                                               ,0 AS VLRBLTFAT
                                                                               ,0 AS CODGRPEMPCLI
                                                                               ,0
                                                                               ,null
                                                                               ,0
                                                                               ,CODBCO
                                                                               ,CODAGEBCO
                                                                               ,NUMDIGVRFAGE
                                                                               ,NUMCNTCRRCTT
                                                                               ,NUMDIGVRFCNTCRR
                                                                               ,SYSDATE AS DATGRC
                                                                               ,:CODFNCGRC AS CODFNCGRC
                                                                               ,:NUMLOTPRENOTFSCSVC
                                                                               ,SYSDATE AS DATIPRNOTFSCSVC
                                                                          FROM MRT.CADCLITGV
                                                                         WHERE CODCLI = :CODCLI 
                                   ");

        return strBld.ToString();

    }


    public string InserirMovimentoClientesServicoAvulso()
    {

        StringBuilder strBld;

        strBld = new StringBuilder(@" 
                                        INSERT INTO MRT.MOVMESFATCLISVCTGV ( ANOMESREF, INDFATSVC, CODCLI, CODSVCTGV, NUMLOTPRENOTFSCSVC, INDSTAATV, VLRSVC, VLRTOTSVC, INDVLRFATVLD, DESSVCTGV, DESJSTSLCSVC)
                                                                    VALUES ( :ANOMESREF, :INDFATSVC, :CODCLI, 9000, :NUMLOTPRENOTFSCSVC, 2, :VLRSVC, :VLRTOTSVC, :INDVLRFATVLD, :DESSVCTGV, :DESJSTSLCSVC)
                                   ");

        return strBld.ToString();

    }


    public string InserirMovimentoClientesParcelasServicoAvulso()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" 
                                        INSERT INTO MRT.MOVFATCLISVCPCLTGV ( ANOMESREF, INDFATSVC, CODCLI, CODSVCTGV, NUMPCLTITNOTFSCSVC, DATVNCNOTFSCSVC, VLRPCLTIT, NUMLOTPRENOTFSCSVC)
                                                                    VALUES ( :ANOMESREF, :INDFATSVC, :CODCLI, 9000, :NUMPCLTITNOTFSCSVC, :DATVNCNOTFSCSVC, :VLRPCLTIT, :NUMLOTPRENOTFSCSVC)
                                   ");

        return strBld.ToString();

    }


    public string EnviarFaturamentoClienteAvulso()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@"   INSERT INTO MRT.CADPRENOTFSCSVC (CODEMP, CODFILEMP, NUMLOTPRENOTFSCSVC, NUMPRENOTFSCSVC , CODUNDNGC, TIPDSNNOTFSC, CODEMPCADFRN, CODFILEMPCADFRN, CODDSNNOTFSC, NOMDSN, ENDDSN, CODBAI, CODCEPCLI, NUMINSEST, NUMCGCDSN, DESMSG, NUMNOTFSCSVC, DATIPRNOTFSCSVC, CODFNCGRC, DATHRAFNCGRC, IDTSITNOTFSCSVC, DESABVTIPLGR, DESLGR, NUMENDLGR, DESCPLEND, ENDCREETN, INDIPRBLT) 
                                                                          SELECT 1 AS CODEMP 
                                                                                , 143 AS CODFILEMP 
                                                                                , MOV.NUMLOTPRENOTFSCSVC AS NUMLOTPRENOTFSCSVC 
                                                                                , ROWNUM AS NUMPRENOTFSCSVC 
                                                                                , 1 AS CODUNDNGC 
                                                                                , 0 AS TIPDSNNOTFSC 
                                                                                , 1 AS CODEMPCADFRN 
                                                                                , 143 AS CODFILEMPCADFRN 
                                                                                , MOV.CODCLI AS CODDSNNOTFSC 
                                                                                , CLI.NOMCLI AS NOMDSN 
                                                                                , SUBSTR(RTRIM(NVL(LGR.DESTIPLGR, ' ')) ||' '||  RTRIM(END.DESLGR) ||', '|| TRIM(END.NUMENDLGR),1,35) AS ENDDSN 
                                                                                , END.CODBAI AS CODBAI 
                                                                                , END.CODCEP AS CODCEPCLI 
                                                                                , TRIM(CLI.NUMINSESTCLI) AS NUMINSEST 
                                                                                , CLI.NUMCGCCLI AS  NUMCGCDSN 
                                                                                , ' ' AS DESMSG 
                                                                                , 0  AS NUMNOTFSCSVC -- NÃO SEI  
                                                                                , TO_DATE(SYSDATE,'YYYY-MM-DD') AS DATIPRNOTFSCSVC 
                                                                                , :CODFNC AS CODFNCGRC 
                                                                                , SYSTIMESTAMP AS DATHRAFNCGRC 
                                                                                , 0 AS IDTSITNOTFSCSVC 
                                                                                , LGR.DESABVTIPLGR AS DESABVTIPLGR 
                                                                                , END.DESLGR AS DESLGR 
                                                                                , END.NUMENDLGR AS NUMENDLGR 
                                                                                , DECODE( TRIM(END.DESCPLEND),'',' ',TRIM(END.DESCPLEND)) AS DESCPLEND
                                                                                , CTO.DESENDETNCTO AS ENDCREETN 
                                                                                , 1 AS INDIPRBLT 
                                                                          FROM MRT.MOVMESFATCLITGV MOV
                                                                          INNER JOIN MRT.T0100043 CLI ON (CLI.CODCLI = MOV.CODCLI)
                                                                          INNER JOIN MRT.T0138938 END ON END.CODCLI = CLI.CODCLI AND END.TIPENDCLI = (SELECT MAX(E2.TIPENDCLI) 
                                                                                                                                                FROM MRT.T0138938 E2
                                                                                                                                               WHERE E2.CODCLI = CLI.CODCLI
                                                                                                                                                 AND E2.TIPENDCLI IN (0,1,2))
                                                                          LEFT JOIN MRT.T0103905 CPL ON CPL.CODCPLBAI = END.CODCPLBAI
                                                                          LEFT JOIN MRT.T0138946 LGR ON LGR.TIPLGR = END.TIPLGR
                                                                          LEFT JOIN MRT.CADCTOTGV CTO ON CTO.CODEDE = CLI.CODCLI AND CTO.CODTIPCTO = 7 AND CTO.DATDST IS NULL
                                                                          WHERE MOV.NUMLOTPRENOTFSCSVC = :NUMLOTPRENOTFSCSVC
                                                                            AND MOV.CODCLI = :CODCLI 
                                                                            AND MOV.ANOMESREF = :ANOMESREF


                                   ");

        return strBld.ToString();
    }


    public string EnviarFaturamentoServicoAvulso()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@"   INSERT INTO MRT.CADPREITENOTFSCSVC (CODEMP, CODFILEMP, NUMLOTPRENOTFSCSVC, NUMPRENOTFSCSVC, CODSVC, VLRUNTBRTSVC, VLRTOTDSC, QDEUNDSVC, INDMESREF, QDEPCLTITNOTFSC, VLRDSCCNSMRT, VLRDSCCNSTBO, VLROTRDSCCNS)           
                                                                              SELECT 1 AS CODEMP 
                                                                                     ,143 AS CODFILEMP 
                                                                                     ,NUMLOTPRENOTFSCSVC AS NUMLOTPRENOTFSCSVC
                                                                                     ,1 AS NUMPRENOTFSCSVC
                                                                                     ,INDFATSVC AS CODSVC
                                                                                     ,VLRTOTSVC AS VLRUNTBRTSVC
                                                                                     ,0 AS VLRTOTDSC 
                                                                                     ,1 AS QDEUNDSVC
                                                                                     ,1 AS INDMESREF
                                                                                     ,1 AS QDEPCLTITNOTFSC
                                                                                     , null AS VLRDSCCNSMRT
                                                                                     , null AS VLRDSCCNSTBO
                                                                                     , null AS VLROTRDSCCNS
                                                                              FROM MRT.MOVMESFATCLISVCTGV 
                                                                              WHERE NUMLOTPRENOTFSCSVC = :NUMLOTPRENOTFSCSVC
                                                                                AND CODCLI = :CODCLI
                                                                                AND ANOMESREF = :ANOMESREF
                                   ");

        return strBld.ToString();
    }


    public string EnviarFaturamentoServicoParcelasAvulso()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" INSERT INTO MRT.CADPRETITNOTFSCSVC (CODEMP, CODFILEMP, NUMLOTPRENOTFSCSVC, NUMPRENOTFSCSVC, NUMPCLTITNOTFSCSVC, DATVNCTIT, VLRPCLTIT, VLRJURMRA)
                                                                             SELECT 1 AS CODEMP
                                                                                   ,143 AS CODFILEMP
                                                                                   ,NUMLOTPRENOTFSCSVC AS NUMLOTPRENOTFSCSVC
                                                                                   ,1 AS NUMPRENOTFSCSVC
                                                                                   ,NUMPCLTITNOTFSCSVC AS NUMPCLTITNOTFSCSVC
                                                                                   ,DATVNCNOTFSCSVC AS DATVNCTIT
                                                                                   ,VLRPCLTIT AS VLRPCLTIT
                                                                                   ,0 VLRJURMRA
                                                                              FROM MRT.MOVFATCLISVCPCLTGV
                                                                             WHERE NUMLOTPRENOTFSCSVC = :NUMLOTPRENOTFSCSVC
                                                                               AND CODCLI = :CODCLI 
                                                                               AND ANOMESREF = :ANOMESREF
                                   ");

        return strBld.ToString();
    }


    public string ObterLotesFaturamentoPrincipalGrupoFat(FaturamentoApiModel.obterDemonstrativoFaturamentoApiModel _objPesquisar)
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@"     WITH TABPRINCIPAL AS ( SELECT M.ANOMESREF
                                                                        , M.INDFATSVC
                                                                        , M.CODCLI
                                                                        , S.CODSVCTGV
                                                                        , S.INDSTAATV                                                                                
                                                                        , S.VLRTOTSVC           
                                                                        , S.CODCLIGRPFAT
                                                                        , M.CODGRPEMPCLI
                                                                        , S.DATATVSVC
                                                                        , S.DATCNC
                                                                        , S.DATINISUSSVC
                                                                        , S.DATFIMSUSSVC
                                                                        , S.INDVLRFATVLD
                                                                    FROM MRT.MOVMESFATCLITGV  M
                                                                   INNER JOIN MRT.MOVMESFATCLISVCTGV S ON S.ANOMESREF = M.ANOMESREF AND S.CODCLI = M.CODCLI AND M.INDFATSVC = S.INDFATSVC
                                                                   INNER JOIN MRT.T0100043 CL ON(M.CODCLI = CL.CODCLI)
                                                                   WHERE S.INDSTAATV <> 1 
                                                                     AND M.INDFATSVC = 1
                                                                     AND S.CODCLIGRPFAT IS NOT NULL
                                                                     AND S.DATATVSVC IS NOT NULL
                                                                     AND (S.DATCNC IS NULL OR TO_CHAR(S.DATCNC, 'YYYYMM') >= :ANOMESVIGENTE) 
                                                                     AND M.ANOMESREF = :ANOMESREF
                                                                     AND M.CODCLI = :CODCLIMODFIL
                                                                     AND S.CODSVCTGV = :CODSVCTGVMODFIL
                                                                     AND CL.NUMCGCCLI = :NUMCGCCLIMODFIL
                                                                     AND CL.NOMCLI LIKE '%' || UPPER(:NOMCLIMODFIL) || '%'
                                                                   #INPUTOUTPUT
                                                               ), 
                                                TABPASSADO AS ( SELECT M.ANOMESREF
                                                                       , M.INDFATSVC
                                                                       , M.CODCLI
                                                                       , S.CODSVCTGV                                                                               
                                                                       , S.VLRTOTSVC  
                                                                       , S.INDSTAATV
                                                                       , S.DATATVSVC DATATVSVCPASS
                                                                       , S.DATCNC DATCNCPASS
                                                                       , S.DATINISUSSVC DATINISUSSVCPASS
                                                                       , S.DATFIMSUSSVC DATFIMSUSSVCPASS
                                                                       , S.INDVLRFATVLD INDVLRFATVLDPASS
                                                                  FROM MRT.MOVMESFATCLITGV  M
                                                                 INNER JOIN MRT.MOVMESFATCLISVCTGV S ON S.ANOMESREF = M.ANOMESREF AND S.CODCLI = M.CODCLI AND M.INDFATSVC = S.INDFATSVC
                                                                 INNER JOIN MRT.T0100043 CL ON(M.CODCLI = CL.CODCLI)
                                                                 WHERE S.INDSTAATV <> 1
                                                                   AND M.INDFATSVC = 1
                                                                   AND S.DATATVSVC IS NOT NULL
                                                                   AND S.CODCLIGRPFAT IS NOT NULL
                                                                   AND (S.DATCNC IS NULL OR TO_CHAR(S.DATCNC, 'YYYYMM') >= :ANOMESVIGENTEPASS) 
                                                                   AND M.ANOMESREF = :ANOMESREFPASS
                                                                   AND M.CODCLI = :CODCLIMODFIL 
                                                                   AND S.CODSVCTGV = :CODSVCTGVMODFIL
                                                                   AND CL.NUMCGCCLI = :NUMCGCCLIMODFIL
                                                                   AND CL.NOMCLI LIKE '%' || UPPER(:NOMCLIMODFIL) || '%'
                                                                   #INPUTOUTPUTPASS
                                                               )");



        strBld.Append(@" SELECT COALESCE(A.ANOMESREF,B.ANOMESREF) ANOMESREFP
                                                    , CLI.CODCLI
                                                    , CLI.NOMCLI
                                                    , CG.NOMGRPEMPCLI
                                                    , GE.NOMGRPEMPCLI AS NOMGRUPOCLI
                                                    , NVL(CG.CODGRPEMPCLI, 0) CODGRPEMPCLI
                                                    , B.ANOMESREF  ANOMESPASS
                                                    , SUM(B.VLRTOTSVC) VLRTOTSVCPASS
                                                    , A.ANOMESREF ANOMESREF  
                                                    , SUM(A.VLRTOTSVC) VLRTOTSVC ");


        strBld.Append(@", SV.DESSVCTGV DESSERPASS
                            , CASE WHEN B.INDSTAATV = 1 THEN 'EM IMPLANTAÇÃO'
                                WHEN B.INDSTAATV = 2 THEN 'ATIVO'
                                WHEN B.INDSTAATV = 3 THEN 'SUSPENSO'
                                WHEN B.INDSTAATV = 4 THEN 'CANCELADO'  END STATUSPASS 
                            , SV.CODSVCTGV CODSVCTGV
                            , SV.DESSVCTGV
                            , CASE WHEN A.INDSTAATV = 1 THEN 'EM IMPLANTAÇÃO'
                                WHEN A.INDSTAATV = 2 THEN 'ATIVO'
                                WHEN A.INDSTAATV = 3 THEN 'SUSPENSO'
                                WHEN A.INDSTAATV = 4 THEN 'CANCELADO'  END STATUS   
                            , TO_CHAR(A.DATATVSVC,'DD/MM/YYYY') DATATVSVC
                            , TO_CHAR(A.DATCNC,'DD/MM/YYYY') DATCNC
                            , TO_CHAR(A.DATINISUSSVC,'DD/MM/YYYY') DATINISUSSVC
                            , TO_CHAR(A.DATFIMSUSSVC,'DD/MM/YYYY') DATFIMSUSSVC
                            , TO_CHAR(B.DATATVSVCPASS,'DD/MM/YYYY') DATATVSVCPASS
                            , TO_CHAR(B.DATCNCPASS,'DD/MM/YYYY') DATCNCPASS
                            , TO_CHAR(B.DATINISUSSVCPASS,'DD/MM/YYYY') DATINISUSSVCPASS
                            , TO_CHAR(B.DATFIMSUSSVCPASS,'DD/MM/YYYY') DATFIMSUSSVCPASS
                            , 0 QTDATIVOSPASS
                            , 0 VALORPASSADO
                            , 0 QTDATIVOS
                            , 0 VALORTOTAL
                            , 0 MENSALIDADE 
                            , 0 INPUT                                 
                            , 0 OUTPUT
                            , 0 STATUSSEMDIVERGENCIA                                
                            , A.INDVLRFATVLD AS VALIDADADOS 
                        ");



        strBld.Append(@" FROM TABPRINCIPAL A
                             FULL JOIN TABPASSADO B ON A.CODCLI = B.CODCLI AND A.CODSVCTGV = B.CODSVCTGV
                            INNER JOIN MRT.T0100043 CLI ON CLI.CODCLI = COALESCE(A.CODCLI,B.CODCLI)   
                             LEFT JOIN MRT.T0100043 CLIFAT ON CLIFAT.CODCLI = A.CODCLIGRPFAT
                             LEFT JOIN MRT.CADGRPEMPTGV CG ON CG.CODCLIGRPFAT = A.CODCLIGRPFAT
                             LEFT JOIN MRT.CADGRPEMPTGV GE ON GE.CODGRPEMPCLI = A.CODGRPEMPCLI
                            INNER JOIN MRT.CADSVCTGV SV ON SV.CODSVCTGV = COALESCE(A.CODSVCTGV,B.CODSVCTGV) ");


        if (_objPesquisar.STATUS == 1)
            strBld.Append(" AND A.INDSTAATV = 2");
        else if (_objPesquisar.STATUS == 2)
            strBld.Append(" AND A.INDSTAATV = 3");
        else if (_objPesquisar.STATUS == 3)
            strBld.Append(" AND A.INDSTAATV = 4");


            strBld.Replace("#INPUTOUTPUTPASS", "");
            strBld.Replace("#INPUTOUTPUT", "");

        strBld.Append(@" GROUP BY A.ANOMESREF
                                     , B.ANOMESREF
                                     , CLI.CODCLI 
                                     , CLI.NOMCLI 
                                     , CG.NOMGRPEMPCLI
                                     , CG.CODGRPEMPCLI
                                     , GE.NOMGRPEMPCLI ");


        strBld.Append(@" , SV.CODSVCTGV 
                    , SV.DESSVCTGV
                    , B.ANOMESREF  
                    , B.INDSTAATV 
                    , A.INDSTAATV    
                    , A.DATATVSVC
                    , A.DATCNC
                    , A.DATINISUSSVC
                    , A.DATFIMSUSSVC
                    , B.DATATVSVCPASS
                    , B.DATCNCPASS
                    , B.DATINISUSSVCPASS
                    , B.DATFIMSUSSVCPASS
                    , A.INDVLRFATVLD
                    ");

        strBld.Append(@" ORDER BY CG.NOMGRPEMPCLI, CLI.NOMCLI, CLI.CODCLI ");


        return strBld.ToString();
    }


    public string ObterLotesFaturamentoPrincipalSemGrupoFat(FaturamentoApiModel.obterDemonstrativoFaturamentoApiModel _objPesquisar)
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@"     WITH TABPRINCIPAL AS ( SELECT M.ANOMESREF
                                                                        , M.INDFATSVC
                                                                        , M.CODCLI
                                                                        , S.CODSVCTGV
                                                                        , S.INDSTAATV                                                                                
                                                                        , S.VLRTOTSVC           
                                                                        , S.CODCLIGRPFAT
                                                                        , M.CODGRPEMPCLI
                                                                        , S.DATATVSVC
                                                                        , S.DATCNC
                                                                        , S.DATINISUSSVC
                                                                        , S.DATFIMSUSSVC
                                                                        , S.INDVLRFATVLD
                                                                    FROM MRT.MOVMESFATCLITGV  M
                                                                   INNER JOIN MRT.MOVMESFATCLISVCTGV S ON S.ANOMESREF = M.ANOMESREF AND S.CODCLI = M.CODCLI AND M.INDFATSVC = S.INDFATSVC
                                                                   INNER JOIN MRT.T0100043 CL ON(M.CODCLI = CL.CODCLI)
                                                                   WHERE S.INDSTAATV <> 1 
                                                                     AND M.INDFATSVC = 1
                                                                     AND S.CODCLIGRPFAT IS NULL
                                                                     AND S.DATATVSVC IS NOT NULL
                                                                     AND (S.DATCNC IS NULL OR TO_CHAR(S.DATCNC, 'YYYYMM') >= :ANOMESVIGENTE) 
                                                                     AND M.ANOMESREF = :ANOMESREF
                                                                     AND M.CODCLI = :CODCLIMODFIL
                                                                     AND S.CODSVCTGV = :CODSVCTGVMODFIL
                                                                     AND CL.NUMCGCCLI = :NUMCGCCLIMODFIL
                                                                     AND CL.NOMCLI LIKE '%' || UPPER(:NOMCLIMODFIL) || '%'
                                                                   #INPUTOUTPUT
                                                               ), 
                                                TABPASSADO AS ( SELECT M.ANOMESREF
                                                                       , M.INDFATSVC
                                                                       , M.CODCLI
                                                                       , S.CODSVCTGV                                                                               
                                                                       , S.VLRTOTSVC  
                                                                       , S.INDSTAATV
                                                                       , S.DATATVSVC DATATVSVCPASS
                                                                       , S.DATCNC DATCNCPASS
                                                                       , S.DATINISUSSVC DATINISUSSVCPASS
                                                                       , S.DATFIMSUSSVC DATFIMSUSSVCPASS
                                                                       , S.INDVLRFATVLD INDVLRFATVLDPASS
                                                                  FROM MRT.MOVMESFATCLITGV  M
                                                                 INNER JOIN MRT.MOVMESFATCLISVCTGV S ON S.ANOMESREF = M.ANOMESREF AND S.CODCLI = M.CODCLI AND M.INDFATSVC = S.INDFATSVC
                                                                 INNER JOIN MRT.T0100043 CL ON(M.CODCLI = CL.CODCLI)
                                                                 WHERE S.INDSTAATV <> 1
                                                                   AND M.INDFATSVC = 1
                                                                   AND S.DATATVSVC IS NOT NULL
                                                                   AND S.CODCLIGRPFAT IS NULL
                                                                   AND (S.DATCNC IS NULL OR TO_CHAR(S.DATCNC, 'YYYYMM') >= :ANOMESVIGENTEPASS) 
                                                                   AND M.ANOMESREF = :ANOMESREFPASS
                                                                   AND M.CODCLI = :CODCLIMODFIL 
                                                                   AND S.CODSVCTGV = :CODSVCTGVMODFIL
                                                                   AND CL.NUMCGCCLI = :NUMCGCCLIMODFIL
                                                                   AND CL.NOMCLI LIKE '%' || UPPER(:NOMCLIMODFIL) || '%'
                                                                   #INPUTOUTPUTPASS
                                                               )");



        strBld.Append(@" SELECT COALESCE(A.ANOMESREF,B.ANOMESREF) ANOMESREFP
                                                    , CLI.CODCLI
                                                    , CLI.NOMCLI
                                                    , CG.NOMGRPEMPCLI
                                                    , GE.NOMGRPEMPCLI AS NOMGRUPOCLI
                                                    , NVL(CG.CODGRPEMPCLI, 0) CODGRPEMPCLI
                                                    , B.ANOMESREF  ANOMESPASS
                                                    , SUM(B.VLRTOTSVC) VLRTOTSVCPASS
                                                    , A.ANOMESREF ANOMESREF  
                                                    , SUM(A.VLRTOTSVC) VLRTOTSVC ");


        strBld.Append(@", SV.DESSVCTGV DESSERPASS
                            , CASE WHEN B.INDSTAATV = 1 THEN 'EM IMPLANTAÇÃO'
                                WHEN B.INDSTAATV = 2 THEN 'ATIVO'
                                WHEN B.INDSTAATV = 3 THEN 'SUSPENSO'
                                WHEN B.INDSTAATV = 4 THEN 'CANCELADO'  END STATUSPASS 
                            , SV.CODSVCTGV CODSVCTGV
                            , SV.DESSVCTGV
                            , CASE WHEN A.INDSTAATV = 1 THEN 'EM IMPLANTAÇÃO'
                                WHEN A.INDSTAATV = 2 THEN 'ATIVO'
                                WHEN A.INDSTAATV = 3 THEN 'SUSPENSO'
                                WHEN A.INDSTAATV = 4 THEN 'CANCELADO'  END STATUS   
                            , TO_CHAR(A.DATATVSVC,'DD/MM/YYYY') DATATVSVC
                            , TO_CHAR(A.DATCNC,'DD/MM/YYYY') DATCNC
                            , TO_CHAR(A.DATINISUSSVC,'DD/MM/YYYY') DATINISUSSVC
                            , TO_CHAR(A.DATFIMSUSSVC,'DD/MM/YYYY') DATFIMSUSSVC
                            , TO_CHAR(B.DATATVSVCPASS,'DD/MM/YYYY') DATATVSVCPASS
                            , TO_CHAR(B.DATCNCPASS,'DD/MM/YYYY') DATCNCPASS
                            , TO_CHAR(B.DATINISUSSVCPASS,'DD/MM/YYYY') DATINISUSSVCPASS
                            , TO_CHAR(B.DATFIMSUSSVCPASS,'DD/MM/YYYY') DATFIMSUSSVCPASS
                            , 0 QTDATIVOSPASS
                            , 0 VALORPASSADO
                            , 0 QTDATIVOS
                            , 0 VALORTOTAL
                            , 0 MENSALIDADE 
                            , 0 INPUT                                 
                            , 0 OUTPUT
                            , 0 STATUSSEMDIVERGENCIA                                
                            , A.INDVLRFATVLD AS VALIDADADOS 
                        ");



        strBld.Append(@" FROM TABPRINCIPAL A
                             FULL JOIN TABPASSADO B ON A.CODCLI = B.CODCLI AND A.CODSVCTGV = B.CODSVCTGV
                            INNER JOIN MRT.T0100043 CLI ON CLI.CODCLI = COALESCE(A.CODCLI,B.CODCLI)   
                             LEFT JOIN MRT.T0100043 CLIFAT ON CLIFAT.CODCLI = A.CODCLIGRPFAT
                             LEFT JOIN MRT.CADGRPEMPTGV CG ON CG.CODCLIGRPFAT = A.CODCLIGRPFAT
                             LEFT JOIN MRT.CADGRPEMPTGV GE ON GE.CODGRPEMPCLI = A.CODGRPEMPCLI
                            INNER JOIN MRT.CADSVCTGV SV ON SV.CODSVCTGV = COALESCE(A.CODSVCTGV,B.CODSVCTGV) ");


        if (_objPesquisar.STATUS == 1)
            strBld.Append(" AND A.INDSTAATV = 2");
        else if (_objPesquisar.STATUS == 2)
            strBld.Append(" AND A.INDSTAATV = 3");
        else if (_objPesquisar.STATUS == 3)
            strBld.Append(" AND A.INDSTAATV = 4");


        strBld.Replace("#INPUTOUTPUTPASS", "");
        strBld.Replace("#INPUTOUTPUT", "");

        strBld.Append(@" GROUP BY A.ANOMESREF
                                     , B.ANOMESREF
                                     , CLI.CODCLI 
                                     , CLI.NOMCLI 
                                     , CG.NOMGRPEMPCLI
                                     , CG.CODGRPEMPCLI
                                     , GE.NOMGRPEMPCLI ");


        strBld.Append(@" , SV.CODSVCTGV 
                    , SV.DESSVCTGV
                    , B.ANOMESREF  
                    , B.INDSTAATV 
                    , A.INDSTAATV    
                    , A.DATATVSVC
                    , A.DATCNC
                    , A.DATINISUSSVC
                    , A.DATFIMSUSSVC
                    , B.DATATVSVCPASS
                    , B.DATCNCPASS
                    , B.DATINISUSSVCPASS
                    , B.DATFIMSUSSVCPASS
                    , A.INDVLRFATVLD
                    ");

        strBld.Append(@" ORDER BY CG.NOMGRPEMPCLI, CLI.NOMCLI, CLI.CODCLI ");


        return strBld.ToString();
    }

}