﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Transactions;

public class FaturamentoBLL
{
    public string GerarLoteFaturamento()
    {

        FaturamentoApiModel.ParametrosFaturamentoApiModel _objInserir = new FaturamentoApiModel.ParametrosFaturamentoApiModel();
        string _anomesref = "";
        int _numanomeslote = 0;
        var DAL = new FaturamentoDAL();
        bool _inseriucontas = false;
        bool _inseriudescontos = false;

        int _loteFaturado = 0;

        int _anolote = 0;
        int _meslote = 0;

        Int64 _numerolote = 0;

        try
        {
            //verifica se tem lote aberto e busca o proximo lote anomesref
            _anomesref = anomesref();

            _numanomeslote = int.Parse(_anomesref.Split(';')[1].Remove(4,1));
            
            _anolote = int.Parse(_numanomeslote.ToString().Substring(0, 4));
            _meslote = int.Parse(_numanomeslote.ToString().Substring(4, 2));
            if (_meslote == 1)
            {
                _anolote = _anolote - 1;
                _meslote = 12;

                _loteFaturado = int.Parse(_anolote.ToString() + _meslote.ToString().PadLeft(2, '0'));
            }
            else
                _loteFaturado = _numanomeslote - 1;


            _numerolote = DAL.obtemNumeroLoteFaturamento();


            _objInserir.ANOMESREF = _numanomeslote;
            _objInserir.ANOMESREFPASS = _loteFaturado.ToString();
            _objInserir.INDFATSVC = 1;
            _objInserir.CODFNCGRC = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
            _objInserir.NUMLOTPRENOTFSCSVC = _numerolote;

            var _inicio = DAL.InserirInicioLoteFat(_objInserir);

            using (TransactionScope Scope = new TransactionScope(TransactionScopeOption.Required)) 
            {
                if (_inicio)
                {
                    //percorre faturamento 
                    _inseriucontas = DAL.InserirMovimentoClientesContas(_objInserir);
                    if (_inseriucontas)
                    {
                        _inseriudescontos = DAL.InserirMovimnetoServicosDescontos(_objInserir);
                    }

                    if (_inseriucontas && _inseriudescontos)
                        Scope.Complete();
                    else
                        Scope.Dispose();
                }

            }

            DAL.FinalizaLoteFat(_objInserir);

            return "0;0";
        }
        catch (Exception ex)
        {
            DAL.deletetaLoteFat(_objInserir);
            Utilitario.CriaLogErro(ex, "ERRO AO GERAR LOTE DE FATURAMENTO.");
            Utilitario.InsereLog(ex, "GerarLoteFaturamento", "CadastroContatoApiModel.CadastroContatoApiModel", "GestaoContasBLL", "ERRO AO GERAR LOTE DE FATURAMENTO.");
            throw;
        }
    }

    public string anomesref ()
    {
        List<FaturamentoTO.verificaLotesEnviado> _listLotesEnviado = new List<FaturamentoTO.verificaLotesEnviado>();

        int _numanomeslote = 0;
        int _anolote = 0;
        int _meslote = 0;

        var DAL = new FaturamentoDAL();

        _listLotesEnviado = DAL.VerificaLoteEnviado();
        if (_listLotesEnviado != null)
        {
            if (_listLotesEnviado.Count() > 0)
            {
                foreach (FaturamentoTO.verificaLotesEnviado item in _listLotesEnviado)
                {
                    _numanomeslote = item.ANOMESREF;
                    if (item.DATFIMPCS.Trim() == "")
                    {
                            return "1;" + item.ANOMESREF.ToString().Substring(0, 4) + "/" + item.ANOMESREF.ToString().Substring(4, 2);
                    }

                    if (item.DATENVFAT.Trim() != "")
                    {
                        _anolote = int.Parse(_numanomeslote.ToString().Substring(0, 4));
                        _meslote = int.Parse(_numanomeslote.ToString().Substring(4, 2));
                        if (_meslote == 12)
                        {
                            _anolote = _anolote + 1;
                            _meslote = 01;

                            _numanomeslote = int.Parse(_anolote.ToString() + _meslote.ToString().PadLeft(2, '0'));
                        }
                        else
                            _numanomeslote = _numanomeslote + 1;


                        return "0;" + _numanomeslote.ToString().Substring(0, 4) + "/" + _numanomeslote.ToString().Substring(4, 2);
                    }
                    else
                    {
                        return "2;" + item.ANOMESREF.ToString().Substring(0, 4) + "/" + item.ANOMESREF.ToString().Substring(4, 2);
                    }

                }
            }
        }

        if (_numanomeslote == 0)
        {
            _anolote = DateTime.Now.Year;
            _meslote = int.Parse(DateTime.Now.Month.ToString().PadLeft(2, '0'));

            _numanomeslote = int.Parse(_anolote.ToString() + _meslote.ToString().PadLeft(2, '0'));
        }
        else
        {
            _anolote = int.Parse(_numanomeslote.ToString().Substring(0, 4));
            _meslote = int.Parse(_numanomeslote.ToString().Substring(4, 2));
            if (_meslote == 12)
            {
                _anolote = _anolote + 1;
                _meslote = 01;

                _numanomeslote = int.Parse(_anolote.ToString() + _meslote.ToString().PadLeft(2, '0'));
            }
        }

        return "0;" + _numanomeslote.ToString().Substring(0, 4) + "/" + _numanomeslote.ToString().Substring(4, 2);
    }

    public List<FaturamentoTO.obterLotesFaturamneto> ObterLotesFaturamento(FaturamentoApiModel.obterDemonstrativoFaturamentoApiModel objPesquisar)
    {
        var DAL = new FaturamentoDAL();
        List<FaturamentoTO.obterLotesFaturamneto> _listLoteFat = new List<FaturamentoTO.obterLotesFaturamneto>();
        List<FaturamentoTO.obterLotesFaturamneto> _listLoteFatInput = new List<FaturamentoTO.obterLotesFaturamneto>();

        try
        {
            if (objPesquisar.TIPOBUSCA != 0) {

                //Atribuição inicial do + filtros, caso alguma informação esteja preenchida, essa será a prioridade.
                if(objPesquisar.ANOMESREFMODFIL != "")
                {
                    objPesquisar.ANOMESREF = objPesquisar.ANOMESREFMODFIL;
                }

                if (objPesquisar.ANOMESREFPASSMODFIL != "")
                {
                    objPesquisar.ANOMESREFPASS = objPesquisar.ANOMESREFPASSMODFIL;
                }

                if (objPesquisar.VISAOMODFIL != 1)
                {
                    objPesquisar.VISAO = objPesquisar.VISAOMODFIL;
                }

                if (objPesquisar.STATUSMODFIL != 0)
                {
                    objPesquisar.STATUS = objPesquisar.STATUSMODFIL;
                }
                //-----------

                objPesquisar.ANOMESREF = objPesquisar.ANOMESREF.Remove(4,1);                

                if (objPesquisar.ANOMESREFPASS != null)
                {
                    if ((objPesquisar.ANOMESREFPASS.Trim() != ""))
                        objPesquisar.ANOMESREFPASS = objPesquisar.ANOMESREFPASS.Remove(4, 1);
                }


                if (objPesquisar.ANOMESREF != null)
                {
                    if (objPesquisar.ANOMESREF != "")
                    {
                        if (objPesquisar.ANOMESREF.Substring(4, 2) == "01")
                        {
                            string ano = (int.Parse(objPesquisar.ANOMESREF.Substring(0, 4)) - 1).ToString();
                            string mes = "12";
                            objPesquisar.ANOMESVIGENTE = int.Parse(ano + mes);
                        }
                        else
                            objPesquisar.ANOMESVIGENTE = int.Parse(objPesquisar.ANOMESREF) - 1;
                    }
                }

                if (objPesquisar.ANOMESREFPASS != null)
                {
                    if (objPesquisar.ANOMESREFPASS != "")
                    {
                        if (objPesquisar.ANOMESREFPASS.Substring(4, 2) == "01")
                        {
                            string ano = (int.Parse(objPesquisar.ANOMESREFPASS.Substring(0, 4)) - 1).ToString();
                            string mes = "12";
                            objPesquisar.ANOMESVIGENTEPASS = int.Parse(ano + mes);
                        }
                        else
                            objPesquisar.ANOMESVIGENTEPASS = int.Parse(objPesquisar.ANOMESREFPASS) - 1;
                    }
                }

                if (objPesquisar.VISAO == 1)
                {
                    if (objPesquisar.TIPOVISAO == 1)
                        _listLoteFat = DAL.ObterLotesFaturamentoPrincipal(objPesquisar).OrderBy(x => x.CODCLI).ToList();
                    else
                        _listLoteFat = DAL.obterLoteGrupoEconomicoAgrupadoPrincipal(objPesquisar).ToList();
                }
                else if (objPesquisar.VISAO == 2)
                {
                    if (objPesquisar.TIPOVISAO == 1)
                        _listLoteFat = DAL.ObterLotesFaturamentoPrincipalGrupoFat(objPesquisar).ToList();
                    else
                        _listLoteFat = DAL.obterLoteGrupoEconomicoAgrupadoGrupoFatPrincipal(objPesquisar).ToList();

                }
                else if (objPesquisar.VISAO == 3)
                {
                    if (objPesquisar.TIPOVISAO == 1)
                        _listLoteFat = DAL.ObterLotesFaturamentoPrincipalSemGrupoFat(objPesquisar).ToList();
                    else
                        _listLoteFat = DAL.obterLoteGrupoEconomicoAgrupadoSemGrupoFatPrincipal(objPesquisar).ToList();
                }


                if (objPesquisar.TIPODIVERGENCIA == 3)
                {
                    _listLoteFatInput = DAL.ObterLotesFaturamentoPrincipalImput(objPesquisar).OrderBy(x => x.CODCLI).ToList();

                    if (_listLoteFat != null)
                    {
                        _listLoteFat.AddRange(_listLoteFatInput);
                    }
                    else
                        _listLoteFat = _listLoteFatInput;
                }


                if (_listLoteFat != null)
                { 
                    if (_listLoteFat.Count > 0)
                    {
                        List<FaturamentoTO.obterTotaisConferencia> _totais = new List<FaturamentoTO.obterTotaisConferencia>();
                        _totais = DAL.obterTotaisConferencia(objPesquisar);

                        Decimal Valortotal = 0;
                        Int64 Qtdtotal = 0;
                        Decimal Valortotalpassado = 0;
                        Int64 Qtdtotalpassado = 0;


                        foreach (FaturamentoTO.obterTotaisConferencia _item in _totais)
                        {

                            if (_item.ANOMESREF == Int64.Parse(objPesquisar.ANOMESREF))
                            {
                                Valortotal = _item.VLRTOTSVC;
                                Qtdtotal = _item.QUANTICLI;
                            }

                            if (objPesquisar.ANOMESREFPASS != "")
                            {
                                if (_item.ANOMESREF == Int64.Parse(objPesquisar.ANOMESREFPASS))
                                {
                                    Valortotalpassado = _item.VLRTOTSVC;
                                    Qtdtotalpassado = _item.QUANTICLI;
                                }
                            }
                        }

                        foreach (var c in _listLoteFat)
                        {                            
                            c.QTDATIVOS = Qtdtotal;
                            c.QTDATIVOSPASS = Qtdtotalpassado;
                            c.VALORTOTAL = Valortotal.ToString();
                            c.VALORPASSADO = Valortotalpassado.ToString();
                            break;
                        }
                    }
                }

            }

            return _listLoteFat;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CONFERENCIA DE FATURAMENTO.");
            Utilitario.InsereLog(ex, "ObterLotesFaturamento", "FaturamentoApiModel.obterDemonstrativoFaturamentoApiModel objPesquisar", "FaturamentoBLL", "ERRO AO FAZER A CONSULTA DO CONFERENCIA DE FATURAMENTO.");
            throw;
        }
    }

    public List<FaturamentoTO.obterLotesFaturamneto>  obtemLotesAgrupadoGrupoEconomico(FaturamentoApiModel.obterDemonstrativoFaturamentoApiModel objPesquisar)
    {
        var DAL = new FaturamentoDAL();
        List<FaturamentoTO.obterLotesFaturamneto> _listLoteGrup = new List<FaturamentoTO.obterLotesFaturamneto>();
        List<FaturamentoTO.obterLotesFaturamneto> _listLoteGrupPrincipal = new List<FaturamentoTO.obterLotesFaturamneto>();
        List<FaturamentoTO.obterLotesFaturamneto> _listLoteGrupomparativo = new List<FaturamentoTO.obterLotesFaturamneto>();

        try
        {
            _listLoteGrupPrincipal = DAL.obterLoteGrupoEconomicoAgrupadoPrincipal(objPesquisar);


            return _listLoteGrupPrincipal;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CONFERENCIA DE FATURAMENTO.");
            Utilitario.InsereLog(ex, "obtemLotesAgrupadoGrupoEconomico", "FaturamentoApiModel.obterDemonstrativoFaturamentoApiModel objPesquisar", "FaturamentoBLL", "ERRO AO FAZER A CONSULTA DO CONFERENCIA DE FATURAMENTO.");
            throw;
        }


    }


    public string VerificaCancelarLote()
    {
        var DAL = new FaturamentoDAL();

        FaturamentoApiModel.cancelarLoteFaturamentoApiModel objCancelar = new FaturamentoApiModel.cancelarLoteFaturamentoApiModel();

        string _anomefrefd = "";

        try
        {
            //NUSCA PARA DESPROCESAR
            _anomefrefd = obterLoteParaDesprocessarEEnviar();

            if (_anomefrefd.Split(';')[0] == "1")
            {
                return _anomefrefd;
            }
            else if (_anomefrefd.Split(';')[0] == "2")
            {

                return _anomefrefd;
            }
            else
            {
                return _anomefrefd;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public string CancelarLote()
    {
        var DAL = new FaturamentoDAL();

        FaturamentoApiModel.cancelarLoteFaturamentoApiModel objCancelar = new FaturamentoApiModel.cancelarLoteFaturamentoApiModel();

        string _anomefrefd = "";

        try
        {
            //NUSCA PARA DESPROCESAR
            _anomefrefd = obterLoteParaDesprocessarEEnviar();

            objCancelar.ANOMESREF = _anomefrefd.Split(';')[1];
            using (TransactionScope Scope = new TransactionScope(TransactionScopeOption.Required))
            {
                DAL.CancelarParametroLote(objCancelar);
                DAL.CancelarClientesLote(objCancelar);
                DAL.CanclearDescontosLote(objCancelar);
                Scope.Complete();
            }

            return _anomefrefd;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public string VerificaEnviarLote()
    {
        var DAL = new FaturamentoDAL();

        FaturamentoApiModel.cancelarLoteFaturamentoApiModel objCancelar = new FaturamentoApiModel.cancelarLoteFaturamentoApiModel();

        string _anomefrefd = "";

        try
        {
            //BUSCA PARA DESPROCESAR
            _anomefrefd = obterLoteParaDesprocessarEEnviar();


            if (_anomefrefd.Split(';')[0] == "1")
            {
                return _anomefrefd;
            }
            else if (_anomefrefd.Split(';')[0] == "2")
            {

                return _anomefrefd;
            }
            else
            {
                List<FaturamentoTO.obterSomatoriaEmailFaturamento> _listsomatorio = new List<FaturamentoTO.obterSomatoriaEmailFaturamento>();
                _listsomatorio = DAL.obterSomatoriaFaturamentos(false);

                var _totalEvinar = _listsomatorio.Count();
                var _valortotalEvinar = _listsomatorio.Sum(x => x.VLRTOTSVC);
                var _valortotalSemEmail = _listsomatorio.Count(x => x.ENDCREETN == null);
                var _numeroLote = _listsomatorio.FirstOrDefault().NUMLOTPRENOTFSCSVC;

                return "0;" + _totalEvinar.ToString() + ";" + _valortotalEvinar.ToString() + ";" + _anomefrefd.Split(';')[1] + ";" + _valortotalSemEmail.ToString() + ";" + _numeroLote.ToString();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    public string obterLoteParaDesprocessarEEnviar()
    {
        List<FaturamentoTO.verificaLotesEnviado> _listLotesEnviado = new List<FaturamentoTO.verificaLotesEnviado>();

        string _numanomeslote = "";
        var DAL = new FaturamentoDAL();

        _listLotesEnviado = DAL.VerificaLoteEnviado();
        if (_listLotesEnviado != null)
        {
            if (_listLotesEnviado.Count() > 0)
            {
                foreach (FaturamentoTO.verificaLotesEnviado item in _listLotesEnviado)
                {
                    if (item.DATFIMPCS.Trim() == "")
                        _numanomeslote = "1;" + item.ANOMESREF.ToString();
                    else if (item.DATENVFAT.Trim() != "")
                        _numanomeslote = "2;" + item.ANOMESREF.ToString();
                    else
                        _numanomeslote = "0;" + item.ANOMESREF.ToString();
                }
            }
        }

        return _numanomeslote;

    }

    public string ObterLoteAtual()
    {
        string _anomesref = "";
        var DAL = new FaturamentoDAL();

        try
        { 
            _anomesref = anomesref();

            if (_anomesref.Split(';')[0] == "1")
                return _anomesref;
            else if (_anomesref.Split(';')[0] == "2")
                return _anomesref;
            else if (_anomesref.Split(';')[0] == "0")
                return _anomesref;


            return _anomesref;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


    public bool ValidarLoteProcessado(FaturamentoApiModel.validarLoteFaturamentoApiModel objValidar)
    {
        try
        {

            FaturamentoApiModel.validarLoteFaturamentoApiModel _objvalidados = new FaturamentoApiModel.validarLoteFaturamentoApiModel();

            List<string> _listvalidados = new List<string>();
            _listvalidados = objValidar.LISTVALIDADOS;

            var DAL = new FaturamentoDAL();
            using (TransactionScope Scope = new TransactionScope(TransactionScopeOption.Required))
            {

                foreach (string _item in _listvalidados)
                {
                    _objvalidados.INDVLRFATVLD = 1;
                    _objvalidados.ANOMESREF = int.Parse(_item.Split('/')[0]);
                    _objvalidados.CODCLI = int.Parse(_item.Split('/')[1]);
                    _objvalidados.CODSVCTGV = int.Parse(_item.Split('/')[2]);

                    DAL.atulizarValidados(_objvalidados);
                }
                Scope.Complete();
            }

            return true;

        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO INSERIR VALIDAÇÃO DO LOTE DE FATURAMENTO.");
            Utilitario.InsereLog(ex, "ValidarLoteProcessado", "FaturamentoApiModel.validarLoteFaturamentoApiModel objValidar", "FaturamentoBLL", "ERRO AO INSERIR VALIDAÇÃO DO LOTE DE FATURAMENTO.");
            throw;
        }

    }


    public List<FaturamentoTO.obterSomatoriaEmailFaturamento>  obterSomatoriaFaturamentos(FaturamentoApiModel.obterEmailFaturamento _objbusca)
    {
        try
        {
            var DAL = new FaturamentoDAL();
            List<FaturamentoTO.obterSomatoriaEmailFaturamento> _listemail = new List<FaturamentoTO.obterSomatoriaEmailFaturamento>();
           

            if (_objbusca.TIPOBUSCAEMAIL == 1)
                _listemail = DAL.obterSomatoriaFaturamentos(true).Where(X => X.ENDCREETN == null).ToList();


            return _listemail;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO OBTER SOMATORIO DE FATURAMENTO.");
            Utilitario.InsereLog(ex, "obterSomatoriaFaturamentos", " ", "FaturamentoBLL", "ERRO AO OBTER SOMATORIO DE FATURAMENTO.");
            throw;
        }

    }

    public string EviarLoteFaturamento(FaturamentoApiModel.enviarLoteparaFaturamento _objEnvLote)
    {
        //obtemNumeroLoteFaturamento
        var DAL = new FaturamentoDAL();
        Int64 _numerolote = 0;

        FaturamentoTO.obterTotaisFaturamento _totais = new FaturamentoTO.obterTotaisFaturamento();
        FaturamentoApiModel.ParametrosFaturamentoApiModel _atualizalotefat = new FaturamentoApiModel.ParametrosFaturamentoApiModel();

        try
        {
            using (TransactionScope Scope = new TransactionScope(TransactionScopeOption.Required))
            {
                _numerolote = _objEnvLote.NUMLOTPRENOTFSCSVC;

                _totais = DAL.obterTotaisFaturamento();

                //VERIFICA SE HOUVE DIVERGÊNCIAS
                if ((_objEnvLote.QTDTOTALCLI != _totais.QTDTOTAL) && (decimal.Parse(_objEnvLote.VALORTOTAL) != _totais.VLRTOTAL))
                {
                    return "1;" + _objEnvLote.QTDTOTALCLI.ToString() + "/" + _totais.QTDTOTAL.ToString() + "/" + _objEnvLote.VALORTOTAL + "/" + _totais.VLRTOTAL.ToString();
                }
                else if ((_objEnvLote.QTDTOTALCLI != _totais.QTDTOTAL))
                {
                    return "2;" + _objEnvLote.QTDTOTALCLI.ToString() + "/" + _totais.QTDTOTAL.ToString();
                }
                else if (decimal.Parse(_objEnvLote.VALORTOTAL) != _totais.VLRTOTAL)
                {
                    return "3;" + decimal.Parse(_objEnvLote.VALORTOTAL) + "/" + _totais.VLRTOTAL.ToString();
                }

                int _cofuncionario = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;


                DAL.InsereTabelaEnvioFaturamento(_numerolote, _cofuncionario);

                _atualizalotefat.ANOMESREF = _objEnvLote.ANOMESREF;
                _atualizalotefat.CODFNCFAT = _cofuncionario;
                _atualizalotefat.NUMLOTPRENOTFSCSVC = _numerolote;

                DAL.AtualizaStatusEnvioFaturamentoLote(_atualizalotefat);

                Scope.Complete();
            }

            return "0;" + _numerolote;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO ENVIAR PARA FATURAMENTO");
            Utilitario.InsereLog(ex, "EviarLoteFaturamento", "FaturamentoApiModel.enviarLoteparaFaturamento _objEnvLote", "FaturamentoBLL", "ERRO AO ENVIAR PARA FATURAMENTO");
            throw;
        }
    }


    public string EnvioServicoEventual(GestaoContasApiModel.GestaoContasContaInserirServicoAvulsoApiModel objInserir)
    {
        Int64 _numerolote = 0;

        try
        {
            Int64 _anomes = Int64.Parse(DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0'));
            objInserir.ANOMESREF = _anomes;

            objInserir.CODFNCGRC = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;

            FaturamentoDAL DAL = new FaturamentoDAL();
            _numerolote = DAL.obtemNumeroLoteFaturamento();
            objInserir.NUMLOTPRENOTFSCSVC = _numerolote;


            var _codigofunc = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
            var dalAprovador = new AprovacaoDAL();
            AprovacaoTO.obterEmailAprovador _aprovador;
            _aprovador = dalAprovador.obterEmailAprovador(_codigofunc);


            using (TransactionScope Scope = new TransactionScope(TransactionScopeOption.Required))
            {

                if (objInserir.DESJSTSLCSVC != null)
                {
                    if (objInserir.DESJSTSLCSVC.Trim() != "")
                    {
                        var DALANOTACAO = new CadastroAnotacaoDAL();
                        CadastroAnotacaoApiModel.CadastroAnotacaoApiModel _itemAnotacao = new CadastroAnotacaoApiModel.CadastroAnotacaoApiModel();

                        _itemAnotacao.CODCLI = int.Parse(objInserir.CODCLI.ToString());
                        _itemAnotacao.CODOPTVND = -1;

                        _itemAnotacao.DESOBS = "Serviço eventual faturado por " + _aprovador.NOMFNC.Trim() + " na data " + DateTime.Now.ToShortDateString() + ", dia " + DateTime.Now.ToShortDateString() + " às " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Hour.ToString() + " \n Justificativa: " + objInserir.DESJSTSLCSVC.Trim();
                        _itemAnotacao.CODMTVCTO = 21;


                        _itemAnotacao.TIPOBSCLI = "5";
                        _itemAnotacao.DATCAD = DateTime.Now.ToString();

                        DALANOTACAO.inserirCadastroAnotacaoGeral(_itemAnotacao);
                    }
                }


                DAL.InserirMovimentoParametroAvulso(objInserir);
                DAL.InserirMovimentoClientesContasServicoAvulso(objInserir);
                DAL.InserirMovimentoClientesServicoAvulso(objInserir);
                //parcelas.
                DAL.InserirMovimentoClientesParcelasServicoAvulso(objInserir);


                //tabelas de envio 
                DAL.EnviarFaturamentoClienteAvulso(objInserir);
                DAL.EnviarFaturamentoServicoAvulso(objInserir);
                //parcelas
                DAL.EnviarFaturamentoServicoParcelasAvulso(objInserir);
                Scope.Complete();
            }


            return "1;" + _numerolote.ToString();
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO ENVIAR FATURAMENTO AVULSO.");
            Utilitario.InsereLog(ex, "EnvioServicoEventual", "GestaoContasApiModel.GestaoContasContaInserirServicoAvulsoApiModel", "GestaoContasBLL", "ERRO AO ENVIAR FATURAMENTO AVULSO.");
            throw;
        }


    }


}