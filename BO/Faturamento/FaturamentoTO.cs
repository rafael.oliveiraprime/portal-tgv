﻿using System.Collections.Generic;
using System;

namespace FaturamentoTO
{
    public class obterParametrosFaturamento
    {
        public int ANOMESREF { get; set; }
        public int INDFATSVC { get; set; }
        public string DATINIPCS { get; set; }
        public string DATFIMPCS { get; set; }
        public int CODFNCGRC { get; set; }
        public string DATENVFAT { get; set; }
        public int CODFNCFAT { get; set; }
        public int NUMLOTPRENOTFSCSVC { get; set; }
    }


    public class verificaLotesAbertos
    {
        public int  ANOMESREF { get; set; }
        public string DATFIMPCS { get; set; }
    }
    
    public class verificaLotesEnviado
    {
        public int ANOMESREF { get; set; }
        public string DATFIMPCS { get; set; }
        public string DATENVFAT { get; set; }
    }

    public class obterLotesFaturamneto
    {
        public int ANOMESREFP { get; set; } 
        public int ANOMESREF { get; set; } 
        public int CODCLI { get; set; } 
        public string NOMCLI { get; set; } 
        public string NOMGRPEMPCLI { get; set; } 
        public int CODGRPEMPCLI { get; set; } 
        public int CODSVCTGV { get; set; } 
        public string DESSVCTGV { get; set; } 
        public string STATUS { get; set; } 
        public string VLRTOTSVC { get; set; } 
        public string DATATVSVC { get; set; } 
        public string DATCNC { get; set; } 
        public string DATINISUSSVC { get; set; } 
        public string DATFIMSUSSVC { get; set; }
        public string DATATVSVCPASS { get; set; }
        public string DATCNCPASS { get; set; }
        public string DATINISUSSVCPASS { get; set; }
        public string DATFIMSUSSVCPASS { get; set; }
        public int ANOMESPASS { get; set; } 
        public string DESSERPASS { get; set; } 
        public string STATUSPASS { get; set; } 
        public string VLRTOTSVCPASS { get; set; } 
        public Int64 QTDATIVOSPASS { get; set; } 
        public string VALORPASSADO { get; set; } 
        public Int64 QTDATIVOS { get; set; } 
        public string VALORTOTAL { get; set; }
        public int MENSALIDADE { get; set; }
        public int INPUT { get; set; }
        public int OUTPUT { get; set; }
        public int STATUSSEMDIVERGENCIA { get; set; }
        public int VALIDADADOS { get; set; }
        public string NOMGRUPOCLI { get; set; }
    }

    public class numerolotefaturamento
    {
        public int CADPRENOTFSCSVC_NUMLOTPRENOT { get; set; }

    }

    public class obterSomatoriaEmailFaturamento
    {
       public int ANOMESREF { get; set; }
       public int CODCLI { get; set; }
       public string NOMCLI { get; set; }
       public int INDBLTIPR { get; set; }
       public decimal VLRTOTSVC { get; set; }
       public int INDFATGRP { get; set; }
       public string ENDCREETN { get; set; }
       public Int64 NUMLOTPRENOTFSCSVC { get; set; }

    }

    public class obterTotaisFaturamento
    {
        public int QTDTOTAL { get; set; }
        public decimal VLRTOTAL { get; set; }
    }

    public class obterTotaisConferencia
    {
        public Int64 ANOMESREF { get; set; }
        public Int64 QUANTICLI { get; set; }
        public decimal VLRTOTSVC { get; set; }

    }




}