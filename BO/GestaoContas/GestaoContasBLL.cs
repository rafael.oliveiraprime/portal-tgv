﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Transactions;

public class GestaoContasBLL
{
    public List<GestaoContasTO.obterGestaoContas> obterGestaoContas(GestaoContasApiModel.GestaocontasApiModel objPesquisar)
    {
        try
        {
            var DAL = new GestaoContasDAL();

            if (objPesquisar.NUMCGCCLI != null)
            {
                objPesquisar.NUMCGCCLI = Regex.Replace(objPesquisar.NUMCGCCLI, "[/.-]", String.Empty);
            }


            if (objPesquisar.TIPOBUSCA != 0)
            {
                if (objPesquisar.TIPOBUSCA == 9)
                    objPesquisar.CODCLI = objPesquisar.CODCLIEXT;


                return DAL.obterGestaoContas(objPesquisar);
            }
            else
                return new List<GestaoContasTO.obterGestaoContas>(); 

        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DA GESTÃO DE CONTAS.");
            Utilitario.InsereLog(ex, "obterGestaoContas", "CadastroContatoApiModel.CadastroContatoApiModel", "GestaoContasBLL", "ERRO AO FAZER A CONSULTA DA GESTÃO DE CONTAS.");
            throw;
        }
    }

    public List<GestaoContasTO.obeterServicosConta> obeterServicosConta(GestaoContasApiModel.GestaocontasServicoApiModel objPesquisar)
    {
        try
        {
            var DAL = new GestaoContasDAL();
            return DAL.obeterServicosConta(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DOS SERVIÇOS DO GESTÃO DE CONTAS.");
            Utilitario.InsereLog(ex, "obeterServicosConta", "CadastroContatoApiModel.GestaocontasServicoApiModel", "GestaoContasBLL", "ERRO AO FAZER A CONSULTA DOS SERVIÇOS DO GESTÃO DE CONTAS.");
            throw;
        }
    }


    public string AcoesServicoGestaoContas(GestaoContasApiModel.AcoesServicoGestaoContasApiModel objAtualzar)
    {

        GestaoContasApiModel.AcoesServicoGestaoContasApiModel _atualuzar = new GestaoContasApiModel.AcoesServicoGestaoContasApiModel();
        List<GestaoContasTO.obeterStatusServicosConta> _listStatus = new List<GestaoContasTO.obeterStatusServicosConta>();
        GestaoContasApiModel.GestaocontasApiModel _gestaocaonta = new GestaoContasApiModel.GestaocontasApiModel();


        bool _statusimplantacao = false;
        bool _statusativo = false;
        bool _statussuspenso = false;
        bool _statuscancelado = false;
        int statusfinal = 1;

        try
        {
            var _codigofunc = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
            var dalAprovador = new AprovacaoDAL();
            AprovacaoTO.obterEmailAprovador _aprovador;
            _aprovador = dalAprovador.obterEmailAprovador(_codigofunc);

            var DAL = new GestaoContasDAL();
            foreach(long codservico in objAtualzar.LISTSERVICOS)
            {
                if (objAtualzar.TIPOACAO == 1)
                {
                    _atualuzar.CODCLI = objAtualzar.CODCLI;
                    _atualuzar.CODSVCTGV = int.Parse(codservico.ToString());

                    _atualuzar.INDSTAATV = "2";
                    _atualuzar.CODFNCATV = _codigofunc;

                    if (objAtualzar.DATATVSVC != null)
                    {
                        _atualuzar.DATATVSVC = DateTime.Parse(objAtualzar.DATATVSVC).ToShortDateString();

                        decimal _percentualprorata = buscaprorata(DateTime.Parse(objAtualzar.DATATVSVC));
                        _atualuzar.PERCOBMESATVSVC = _percentualprorata;
                        _atualuzar.PERCOBMESCNCSVC = 0;

                    }
                    else
                    {
                        _atualuzar.PERCOBMESATVSVC = 0;
                        _atualuzar.PERCOBMESCNCSVC = 0;
                    }

                    if (DAL.AcoesServicoGestaoContas(_atualuzar)) { 


                        if (objAtualzar.DATATVSVC != null)
                        {
                            GestaoContasApiModel.ServicoDescontosGestaodeContasApiModel _objbuscar = new GestaoContasApiModel.ServicoDescontosGestaodeContasApiModel();
                            _objbuscar.CODCLI = objAtualzar.CODCLI;
                            _objbuscar.CODSVCTGV = objAtualzar.CODSVCTGV;

                            AtualizaDataVigencia(_objbuscar, DateTime.Parse(objAtualzar.DATATVSVC));
                        }
                        else
                            objAtualzar.DATATVSVC = DateTime.Now.ToShortDateString();

                    }

                }
                else if (objAtualzar.TIPOACAO == 2)
                {
                    _atualuzar.CODCLI = objAtualzar.CODCLI;
                    _atualuzar.CODSVCTGV = int.Parse(codservico.ToString());
                    _atualuzar.DATINISUSSVC = objAtualzar.DATINISUSSVC;
                    _atualuzar.DATFIMSUSSVC = objAtualzar.DATFIMSUSSVC;
                    _atualuzar.INDSTAATV = "3";
                    _atualuzar.CODFNCSUS = _codigofunc;
                    _atualuzar.PERCOBMESATVSVC = 0;
                    _atualuzar.PERCOBMESCNCSVC = 0;

                    DAL.AcoesServicoGestaoContas(_atualuzar);
                }
                else if (objAtualzar.TIPOACAO == 3)
                {
                    _atualuzar.CODCLI = objAtualzar.CODCLI;
                    _atualuzar.CODSVCTGV = int.Parse(codservico.ToString());
                    _atualuzar.DATCNC = DateTime.Parse(objAtualzar.DATCNC).ToShortDateString();
                    _atualuzar.CODMTVCNC = objAtualzar.CODMTVCNC;
                    _atualuzar.INDSTAATV = "4";
                    _atualuzar.CODFNCCNC = _codigofunc;


                    decimal _percentualprorata = buscaprorata(DateTime.Parse(objAtualzar.DATCNC));
                    _atualuzar.PERCOBMESATVSVC = 0;
                    _atualuzar.PERCOBMESCNCSVC = _percentualprorata;

                    DAL.AcoesServicoGestaoContas(_atualuzar);
                }

                _atualuzar = new GestaoContasApiModel.AcoesServicoGestaoContasApiModel();
            }


            if (objAtualzar.DESOBS != null)
            {
                if (objAtualzar.DESOBS != "")
                {
                    var DALANOTACAO = new CadastroAnotacaoDAL();
                    CadastroAnotacaoApiModel.CadastroAnotacaoApiModel _itemAnotacao = new CadastroAnotacaoApiModel.CadastroAnotacaoApiModel();

                    _itemAnotacao.CODCLI = objAtualzar.CODCLI;
                    _itemAnotacao.CODOPTVND = -1;


                    if (objAtualzar.TIPOACAO == 1)
                    {
                        _itemAnotacao.DESOBS = "Serviço ativado por  " + _aprovador.NOMFNC.Trim() + " para da data " + DateTime.Parse(objAtualzar.DATATVSVC).ToShortDateString() + ", no dia " + DateTime.Now.ToShortDateString() + " às " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Hour.ToString() + " \n Justificativa: " + objAtualzar.DESOBS.Trim();
                        _itemAnotacao.CODMTVCTO = 11;
                    }
                    else if (objAtualzar.TIPOACAO == 2)
                    {
                        _itemAnotacao.DESOBS = "Serviço suspenso por " + _aprovador.NOMFNC.Trim() + ", dia " + DateTime.Now.ToShortDateString() + " às " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Hour.ToString() + " \n Justificativa: " + objAtualzar.DESOBS.Trim();
                        _itemAnotacao.CODMTVCTO = 13;
                    }
                    else if (objAtualzar.TIPOACAO == 3)
                    {
                        _itemAnotacao.DESOBS = "Serviço cancelado por " + _aprovador.NOMFNC.Trim() + " para data " + DateTime.Parse(objAtualzar.DATCNC).ToShortDateString() + ", dia " + DateTime.Now.ToShortDateString() + " às " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Hour.ToString() + " \n Justificativa: " + objAtualzar.DESOBS.Trim();
                        _itemAnotacao.CODMTVCTO = 12;
                    }


                    _itemAnotacao.TIPOBSCLI = "5";
                    _itemAnotacao.DATCAD = DateTime.Now.ToString();

                    DALANOTACAO.inserirCadastroAnotacaoGeral(_itemAnotacao);
                }
            }


            _listStatus = DAL.obeterStatusServicosConta(objAtualzar.CODCLI);
            foreach(GestaoContasTO.obeterStatusServicosConta _itemstatus in _listStatus)
            {
                if (_itemstatus.INDSTAATV == 1)
                    _statusimplantacao = true;
                else if (_itemstatus.INDSTAATV == 2)
                    _statusativo = true;
                else if (_itemstatus.INDSTAATV == 3)
                    _statussuspenso = true;
                else if (_itemstatus.INDSTAATV == 4)
                    _statuscancelado = true;
            }


            if (_statusativo)
                statusfinal = 2;
            else if (_statussuspenso)
                statusfinal = 3;
            else if (_statusimplantacao)
                statusfinal = 1;
            else if (_statuscancelado)
                statusfinal = 4;


            _gestaocaonta.CODCLI = objAtualzar.CODCLI;
            _gestaocaonta.CODSITCLI = statusfinal;
            DAL.AtualizaStatusGestaoContas(_gestaocaonta);

            return statusfinal.ToString(); 
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER ATAULIZAÇÃO DOS SERVIÇOS DO GESTÃO DE CONTAS.");
            Utilitario.InsereLog(ex, "AcoesServicoGestaoContas", "GestaoContasApiModel.AcoesServicoGestaoContasApiModel", "GestaoContasBLL", "ERRO AO FAZER ATAULIZAÇÃO DOS SERVIÇOS DO GESTÃO DE CONTAS.");
            throw;
        }
    }

    public bool AtualizaDataVigencia(GestaoContasApiModel.ServicoDescontosGestaodeContasApiModel _objbuscar, DateTime _dataativacao)
    {
        try
        {
            var DAL = new GestaoContasDAL();

            List<GestaoContasTO.ObterDescontosVigencia> _listdescontos = DAL.ObterDescontosVigencia(_objbuscar);
            GestaoContasApiModel.ServicoDescontosGestaodeContasApiModel _descontos = new GestaoContasApiModel.ServicoDescontosGestaodeContasApiModel();

            int _mesativacao = _dataativacao.Month;
            int _anoativacao = _dataativacao.Year;

            int _amomesfaturamnto = DAL.BuscaAnomeFaturamento();

            int _mesfaturamento = int.Parse(_amomesfaturamnto.ToString().Substring(4, 2));
            int _anofaturamento = int.Parse(_amomesfaturamnto.ToString().Substring(0, 4));

            if (_mesfaturamento == 13)
            {
                _mesfaturamento = 1;
                _anofaturamento = _anofaturamento + 1;
            }

            DateTime _datainicial = DateTime.Parse("01/" + _mesativacao.ToString().PadLeft(2,'0') + "/" + _anofaturamento);
            DateTime _dataFinal = DateTime.Parse("01/" + _mesativacao.ToString().PadLeft(2, '0') + "/" + _anofaturamento);


            int _identSomatorio = 0;
            int _anomeativacao = int.Parse(_anoativacao.ToString() + _mesativacao.ToString().PadLeft(2,'0'));

            if (_anomeativacao ==_amomesfaturamnto)
                _identSomatorio = 1;
            else if (_anomeativacao >= _amomesfaturamnto)
                _identSomatorio = 1;
            else if (_anomeativacao <= _amomesfaturamnto)
                _identSomatorio = 0;


            foreach (GestaoContasTO.ObterDescontosVigencia _itemdesc in _listdescontos)
            {                
                _descontos.CODCLI = _itemdesc.CODCLI;
                _descontos.CODSVCTGV = _itemdesc.CODSVCTGV;
                _descontos.CODCNDCMC = _itemdesc.CODCNDCMC;


                if (_itemdesc.NUMMESINIVLD != 0)
                {
                    _descontos.DATINIVLD = _datainicial.AddMonths(_itemdesc.NUMMESINIVLD - _identSomatorio).ToString();
                    _dataFinal = _datainicial;
                }

                if (_itemdesc.NUMMESFIMVLD != 0)
                {
                    _dataFinal = _dataFinal.AddMonths(_itemdesc.NUMMESFIMVLD - _identSomatorio);
                    _descontos.DATFIMVLD = DateTime.DaysInMonth(_dataFinal.Year, _dataFinal.Month).ToString().PadLeft(2,'0') + "/" + _dataFinal.Month.ToString().PadLeft(2,'0') + "/" + _dataFinal.Year.ToString().PadLeft(2,'0');                  
                }

                DAL.AtualizaDescontosVigenciaGestaoConta(_descontos);

                _descontos = new GestaoContasApiModel.ServicoDescontosGestaodeContasApiModel();
            }

            return true;
        }
        catch(Exception)
        {
            throw;
        }
    }

    public decimal buscaprorata(DateTime _dataativacao)
    {

        var DAL = new GestaoContasDAL();
        decimal _percentualprorata = 0;

        try
        {
            int _amomesfaturamnto = DAL.BuscaAnomeFaturamento();

            int _mesativacao = _dataativacao.Month;
            int _anoativacao = _dataativacao.Year;

            int _mesfaturamento = int.Parse(_amomesfaturamnto.ToString().Substring(4, 2));
            int _anofaturamento = int.Parse(_amomesfaturamnto.ToString().Substring(0, 4));

            int _anomeativacao = int.Parse(_anoativacao.ToString() + _mesativacao.ToString().PadLeft(2, '0'));

            decimal _diasnomes = DateTime.DaysInMonth(_dataativacao.Year, _dataativacao.Month);
            decimal  _dias = (_diasnomes - (_dataativacao.Day - 1));

            _percentualprorata = (_dias / _diasnomes);



            return _percentualprorata;
        }
        catch (Exception)
        {
            throw;
        }



    }

    public List<GestaoContasTO.obterServicoDescontoGestaoConta> ObterServicoDescontoGestaoConta(GestaoContasApiModel.ServicoDescontosGestaodeContasApiModel objPesquisar)
    {
        try
        {
            var DAL = new GestaoContasDAL();
            return DAL.ObterServicoDescontoGestaoConta(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DOS DESCONTOS DOS SERVIÇOS DO GESTÃO DE CONTAS.");
            Utilitario.InsereLog(ex, "obeterServicosConta", "CadastroContatoApiModel.GestaocontasServicoApiModel", "GestaoContasBLL", "ERRO AO FAZER A CONSULTA DOS DESCONTOS DOS SERVIÇOS DO GESTÃO DE CONTAS.");
            throw;
        }
    }

    public bool InserirCadastroCondicaoComercialServicoGestaoContas(GestaoContasApiModel.ServicoDescontosGestaodeContasApiModel objInserir)
    {
        try
        {
            var DAL = new GestaoContasDAL();


            if (objInserir.DSOBS != null)
            {
                if (objInserir.DSOBS != "")
                {
                    var DALANOTACAO = new CadastroAnotacaoDAL();
                    CadastroAnotacaoApiModel.CadastroAnotacaoApiModel _itemAnotacao = new CadastroAnotacaoApiModel.CadastroAnotacaoApiModel();

                    _itemAnotacao.CODCLI = objInserir.CODCLI;
                    _itemAnotacao.CODOPTVND = -1;
                    _itemAnotacao.DESOBS = objInserir.DSOBS;

                    _itemAnotacao.TIPOBSCLI = "5";
                    _itemAnotacao.CODMTVCTO = 10;
                    _itemAnotacao.DATCAD = DateTime.Now.ToString();

                    DALANOTACAO.inserirCadastroAnotacaoGeral(_itemAnotacao);
                }
            }

            return DAL.InserirCadastroCondicaoComercialServicoGestaoContas(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO INSERIR DESCONTOS DOS SERVIÇOS DO GESTÃO DE CONTAS.");
            Utilitario.InsereLog(ex, "InserirCadastroCondicaoComercialServicoGestaoContas", "CadastroContatoApiModel.GestaocontasServicoApiModel", "GestaoContasBLL", "ERRO AO INSERIR DESCONTOS DOS SERVIÇOS DO GESTÃO DE CONTAS.");
            throw;
        }
    }


    public bool DesativarCadastroCondicaoComercialServicoGestaoContas(GestaoContasApiModel.ServicoDescontosGestaodeContasApiModel objInserir)
    {
        var DAL = new GestaoContasDAL();
        GestaoContasApiModel.ServicoDescontosGestaodeContasApiModel _atulizar = new GestaoContasApiModel.ServicoDescontosGestaodeContasApiModel();

        try
        {

            if (objInserir.DSOBS != null)
            {
                if (objInserir.DSOBS != "")
                {
                    var DALANOTACAO = new CadastroAnotacaoDAL();
                    CadastroAnotacaoApiModel.CadastroAnotacaoApiModel _itemAnotacao = new CadastroAnotacaoApiModel.CadastroAnotacaoApiModel();

                    _itemAnotacao.CODCLI = objInserir.CODCLI;
                    _itemAnotacao.CODOPTVND = -1;
                    _itemAnotacao.DESOBS = objInserir.DSOBS;

                    _itemAnotacao.TIPOBSCLI = "5";
                    _itemAnotacao.CODMTVCTO = 10;
                    _itemAnotacao.DATCAD = DateTime.Now.ToString();

                    DALANOTACAO.inserirCadastroAnotacaoGeral(_itemAnotacao);
                }
            }

            foreach (long codcondicao in objInserir.LISTCONDICOES)
            {
                _atulizar.CODCLI = objInserir.CODCLI;
                _atulizar.CODSVCTGV = objInserir.CODSVCTGV;
                _atulizar.CODCNDCMC = int.Parse(codcondicao.ToString());

                DAL.DesativarCadastroCondicaoComercialServicoGestaoContas(_atulizar);
                _atulizar = new GestaoContasApiModel.ServicoDescontosGestaodeContasApiModel();
            }

            return true;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO DESATIVAR DESCONTOS DOS SERVIÇOS DO GESTÃO DE CONTAS.");
            Utilitario.InsereLog(ex, "DesativarCadastroCondicaoComercialServicoGestaoContas", "CadastroContatoApiModel.GestaocontasServicoApiModel", "GestaoContasBLL", "ERRO AO DESATIVAR  DESCONTOS DOS SERVIÇOS DO GESTÃO DE CONTAS.");
            throw;
        }
    }


    public string AtualizarContaCorrente(GestaoContasApiModel.GestaoContasContaCorrenteApiModel objAtualizar)
    {
        string _retorno = "";
        GestaoContasTO.ObterDadosGrupoCliPrincipal _cliprincipal = new GestaoContasTO.ObterDadosGrupoCliPrincipal();
        List<GestaoContasTO.ObterRelacaoCliGrupo> _listrealcao = new List<GestaoContasTO.ObterRelacaoCliGrupo>();
        GestaoContasApiModel.GestaoContasContaCorrenteApiModel _atulizardados = new GestaoContasApiModel.GestaoContasContaCorrenteApiModel();

        try
        {
            var DAL = new GestaoContasDAL();

            //varifcase ehgrupoS
            _cliprincipal = DAL.BuscaDadosGrupoCliPrincipal(objAtualizar.CODCLI, 0).FirstOrDefault();

            if (_cliprincipal != null)
            {
                if (_cliprincipal.INDFATGRP == 1)
                {
                    _listrealcao = DAL.BuscaRelacaoCliGrupo(0, _cliprincipal.CODGRPEMPCLI);
                    if (_listrealcao != null)
                    {
                        DAL.AtualizarContaCorrente(objAtualizar);

                        foreach (GestaoContasTO.ObterRelacaoCliGrupo _item in _listrealcao)
                        {
                            _atulizardados = objAtualizar;
                            _atulizardados.CODCLI = _item.CODCLI;

                            DAL.AtualizarContaCorrente(_atulizardados);

                            _atulizardados = new GestaoContasApiModel.GestaoContasContaCorrenteApiModel();
                        }

                        _retorno = "3:Sucesso!";
                    }
                }
                else
                {
                    DAL.AtualizarContaCorrente(_atulizardados);
                    _retorno = "1:Sucesso!";
                }
            }
            else
            {
                _listrealcao = DAL.BuscaRelacaoCliGrupo(objAtualizar.CODCLI, 0);
                if (_listrealcao != null)
                {
                    if (_listrealcao.Count > 0)
                    {
                        //varifcase ehgrupoS
                        int _codigogrupo = _listrealcao.FirstOrDefault().CODGRPEMPCLI;

                        _cliprincipal = DAL.BuscaDadosGrupoCliPrincipal(0, _codigogrupo).FirstOrDefault();

                        if (_cliprincipal != null)
                        {
                            if (_cliprincipal.INDFATGRP == 1)
                                _retorno = "2:" + _cliprincipal.NOMGRPEMPCLI.Trim();

                        }

                    }
                }

                if (_retorno == "") {
                    if (DAL.AtualizarContaCorrente(objAtualizar))
                        _retorno = "1:Sucesso!";
                }          

            }

            return _retorno; 

        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO ATUALIZAR DADOS DA CONTA CORRENTE DOS SERVIÇOS DO GESTÃO DE CONTAS.");
            Utilitario.InsereLog(ex, "AtualizarContaCorrente", "CadastroContatoApiModel.GestaoContasContaCorrenteApiModel", "GestaoContasBLL", "ERRO AO ATUALIZAR DADOS DA CONTA CORRENTE DOS SERVIÇOS DO GESTÃO DE CONTAS.");
            throw;
        }
    }

    public int VerificaServicoDescontoGestaoConta(GestaoContasApiModel.ServicoDescontosGestaodeContasApiModel objPesquisar)
    {
        try
        {
            var DAL = new GestaoContasDAL();
            return DAL.VerificaServicoDescontoGestaoConta(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A VERIFICAR CONDIÇÃO COMERCIAL DESCONTOS DOS SERVIÇOS DO GESTÃO DE CONTAS.");
            Utilitario.InsereLog(ex, "VerificaServicoDescontoGestaoConta", "CadastroContatoApiModel.GestaocontasServicoApiModel", "GestaoContasBLL", "ERRO AO FAZER A VERIFICAR CONDIÇÃO COMERCIAL DESCONTOS DOS SERVIÇOS DO GESTÃO DE CONTAS.");
            throw;
        }
    }

    public string AlteraQuantidade(GestaoContasApiModel.GestaoContasContaAlterarQuantidadeApiModel objAtualzar)
    {
        var DAL = new GestaoContasDAL();
        var dalAprovador = new AprovacaoDAL();

        GestaoContasApiModel.GestaoContasContaAlterarQuantidadeApiModel _atualizaqtd = new GestaoContasApiModel.GestaoContasContaAlterarQuantidadeApiModel();

        try
        { 

            var _codigofunc = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
            AprovacaoTO.obterEmailAprovador _aprovador;
            _aprovador = dalAprovador.obterEmailAprovador(_codigofunc);

            if (objAtualzar.DESOBS != null)
            {
                if (objAtualzar.DESOBS != "")
                {
                    var DALANOTACAO = new CadastroAnotacaoDAL();
                    CadastroAnotacaoApiModel.CadastroAnotacaoApiModel _itemAnotacao = new CadastroAnotacaoApiModel.CadastroAnotacaoApiModel();

                    _itemAnotacao.CODCLI = objAtualzar.CODCLI;
                    _itemAnotacao.CODOPTVND = -1;

                    _itemAnotacao.DESOBS = "Quantidade do Serviço alterado por  " + _aprovador.NOMFNC.Trim() + ", no dia " + DateTime.Now.ToShortDateString() + " às " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Hour.ToString() + " \n Justificativa: " + objAtualzar.DESOBS.Trim();
                    _itemAnotacao.CODMTVCTO = 20;
 

                    _itemAnotacao.TIPOBSCLI = "5";
                    _itemAnotacao.DATCAD = DateTime.Now.ToString();

                    DALANOTACAO.inserirCadastroAnotacaoGeral(_itemAnotacao);
                }
            }

            foreach (long codservico in objAtualzar.LISTSERVICOS)
            {
                _atualizaqtd.CODCLI = objAtualzar.CODCLI;
                _atualizaqtd.CODSVCTGV = int.Parse(codservico.ToString());
                _atualizaqtd.QDEPRDVND = objAtualzar.QDEPRDVND;

                DAL.AlteraQuantidade(_atualizaqtd);
                _atualizaqtd = new GestaoContasApiModel.GestaoContasContaAlterarQuantidadeApiModel();
            }

            return "1";

        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A VERIFICAR CONDIÇÃO COMERCIAL DESCONTOS DOS SERVIÇOS DO GESTÃO DE CONTAS.");
            Utilitario.InsereLog(ex, "VerificaServicoDescontoGestaoConta", "CadastroContatoApiModel.GestaocontasServicoApiModel", "GestaoContasBLL", "ERRO AO FAZER A VERIFICAR CONDIÇÃO COMERCIAL DESCONTOS DOS SERVIÇOS DO GESTÃO DE CONTAS.");
            throw;
        }


    }

    public bool AtualizaGrupoServico(GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel objAlterar)
    {
        try
        {
            return new GestaoContasDAL().AtualizaGrupoServico(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO ATUALIZAR GRUPO DE SERVICO.");
            Utilitario.InsereLog(ex, "AtualizaGrupoServico", "GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel", "GestaoContasBLL", "ERRO AO ATUALIZAR GRUPO DE SERVICO.");
            throw;
        }

    }

    public bool AtualizaGrupoGetaoServico(GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel objAlterar)
    {
        try
        {
            return new GestaoContasDAL().AtualizaGrupoGetaoServico(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO ATUALIZAR GRUPO DE SERVICO DE GETÃO DE CONTAS.");
            Utilitario.InsereLog(ex, "AtualizaGrupoGetaoServico", "GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel", "GestaoContasBLL", "ERRO AO ATUALIZAR GRUPO DE SERVICO DE GETÃO DE CONTAS.");
            throw;
        }

    }

    public bool AtualizaAllGrupoGetaoServico(GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel objAlterar)
    {
        try
        {
            return new GestaoContasDAL().AtualizaAllGrupoGetaoServico(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO ATUALIZAR TODOS GRUPO DE SERVICO DE GETÃO DE CONTAS.");
            Utilitario.InsereLog(ex, "AtualizaAllGrupoGetaoServico", "GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel", "GestaoContasBLL", "ERRO AO ATUALIZAR TODOS GRUPO DE SERVICO DE GETÃO DE CONTAS.");
            throw;
        }

    }

    public bool AtualizaAutomacao(GestaoContasApiModel.GestaoContasContaAlterarAutomacaoApiModel objAlterar)
    {
        try
        {
            return new GestaoContasDAL().AtualizaAutomacao(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO ATUALIZAR TODOS GRUPO DE SERVICO DE GETÃO DE CONTAS.");
            Utilitario.InsereLog(ex, "AtualizaAllGrupoGetaoServico", "GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel", "GestaoContasBLL", "ERRO AO ATUALIZAR TODOS GRUPO DE SERVICO DE GETÃO DE CONTAS.");
            throw;
        }

    }

}