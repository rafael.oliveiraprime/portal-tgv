﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrmApiModel;

public class GestaoContasDAL : DAL
{
    public List<GestaoContasTO.obterGestaoContas> obterGestaoContas(GestaoContasApiModel.GestaocontasApiModel objPesquisar)
    {
        var DALSQL = new GestaoContasDALSQL();
        string cmdSql = DALSQL.obterGestaoContas();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (objPesquisar.CODCLI == 0)
            objPesquisar.CODCLI = -1;

        if (objPesquisar.NUMCGCCLI == "")
            objPesquisar.NUMCGCCLI = null;

        if (objPesquisar.CODGRPEMPCLI == 0)
            objPesquisar.CODGRPEMPCLI = -1;

        if (objPesquisar.NOMCIDEXD == "")
            objPesquisar.NOMCIDEXD = null;

        if (objPesquisar.CodEstUni == "")
            objPesquisar.CodEstUni = null;

        if (objPesquisar.CODSITCLI == 0)
            objPesquisar.CODSITCLI = -1;


        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("NOMCLI", objPesquisar.NOMCLI);        
        dbCommand.AddWithValue("NUMCGCCLI", objPesquisar.NUMCGCCLI);
        dbCommand.AddWithValue("CODGRPEMPCLI", objPesquisar.CODGRPEMPCLI);
        dbCommand.AddWithValue("NOMCIDEXD", objPesquisar.NOMCIDEXD);
        dbCommand.AddWithValue("CodEstUni", objPesquisar.CodEstUni);
        dbCommand.AddWithValue("CODSITCLI", objPesquisar.CODSITCLI);
        //dbCommand.AddWithValue("CESTPROD", objPesquisar.CESTPROD);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<GestaoContasTO.obterGestaoContas>();
    }


    public List<GestaoContasTO.obeterServicosConta> obeterServicosConta(GestaoContasApiModel.GestaocontasServicoApiModel objPesquisar)
    {
        var DALSQL = new GestaoContasDALSQL();
        string cmdSql = DALSQL.obeterServicosConta();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (objPesquisar.CODCLI == 0)
            objPesquisar.CODCLI = -1;

        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<GestaoContasTO.obeterServicosConta>();

    }

    public bool AcoesServicoGestaoContas(GestaoContasApiModel.AcoesServicoGestaoContasApiModel objAtualizar)
    {
        var DALSQL = new GestaoContasDALSQL();
        var _datareativacao = 0;


        if (objAtualizar.DATATVSVC != null)
            _datareativacao = 1;


        string cmdSql = DALSQL.AcoesServicoGestaoContas(_datareativacao);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("INDSTAATV", objAtualizar.INDSTAATV);
        dbCommand.AddWithValue("CODCLI", objAtualizar.CODCLI);
        dbCommand.AddWithValue("CODSVCTGV", objAtualizar.CODSVCTGV);

        if (objAtualizar.DATATVSVC != null)
            dbCommand.AddWithValue("DATATVSVC", Convert.ToDateTime(objAtualizar.DATATVSVC));

        if (objAtualizar.DATINISUSSVC != null)
            dbCommand.AddWithValue("DATINISUSSVC", Convert.ToDateTime(objAtualizar.DATINISUSSVC));
        else
            dbCommand.AddWithValue("DATINISUSSVC", objAtualizar.DATINISUSSVC);

        if (objAtualizar.DATFIMSUSSVC != null)
            dbCommand.AddWithValue("DATFIMSUSSVC", Convert.ToDateTime(objAtualizar.DATFIMSUSSVC));
        else
            dbCommand.AddWithValue("DATFIMSUSSVC", objAtualizar.DATFIMSUSSVC);

        if (objAtualizar.DATCNC != null)
            dbCommand.AddWithValue("DATCNC", Convert.ToDateTime(objAtualizar.DATCNC));
        else
            dbCommand.AddWithValue("DATCNC", objAtualizar.DATCNC);


        if (objAtualizar.INDSTAATV == "2")
        {
            dbCommand.AddWithValue("CODFNCATV", objAtualizar.CODFNCATV);
            dbCommand.AddWithValue("CODFNCSUS", null);
            dbCommand.AddWithValue("CODFNCCNC", null);
        }
        else if (objAtualizar.INDSTAATV == "3")
        {
            dbCommand.AddWithValue("CODFNCATV", null);
            dbCommand.AddWithValue("CODFNCSUS", objAtualizar.CODFNCSUS);
            dbCommand.AddWithValue("CODFNCCNC", null);
        }
        else if (objAtualizar.INDSTAATV == "4")
        {
            dbCommand.AddWithValue("CODFNCATV", null);
            dbCommand.AddWithValue("CODFNCSUS", null);
            dbCommand.AddWithValue("CODFNCCNC", objAtualizar.CODFNCCNC);
        }

        if (objAtualizar.PERCOBMESATVSVC > 0)
            dbCommand.AddWithValue("PERCOBMESATVSVC", objAtualizar.PERCOBMESATVSVC);
        else
            dbCommand.AddWithValue("PERCOBMESATVSVC", null);

        
        if (objAtualizar.PERCOBMESCNCSVC > 0)
            dbCommand.AddWithValue("PERCOBMESCNCSVC", objAtualizar.PERCOBMESCNCSVC);
        else
            dbCommand.AddWithValue("PERCOBMESCNCSVC", null);


        dbCommand.AddWithValue("CODMTVCNC", objAtualizar.CODMTVCNC);
        //
        dbCommand.TrataDbCommandUniversal();

        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public List<GestaoContasTO.obeterStatusServicosConta> obeterStatusServicosConta(int _codcli)
    {
        var DALSQL = new GestaoContasDALSQL();
        string cmdSql = DALSQL.obeterStatusServicosConta();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", _codcli);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<GestaoContasTO.obeterStatusServicosConta>();

    }


    public bool AtualizaStatusGestaoContas(GestaoContasApiModel.GestaocontasApiModel objAtualizar)
    {
        var DALSQL = new GestaoContasDALSQL();
        string cmdSql = DALSQL.AtualizaStatusGestaoContas();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODSITCLI", objAtualizar.CODSITCLI);
        dbCommand.AddWithValue("CODCLI", objAtualizar.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal();

        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public List<GestaoContasTO.obterServicoDescontoGestaoConta> ObterServicoDescontoGestaoConta(GestaoContasApiModel.ServicoDescontosGestaodeContasApiModel objPesquisar)
    {
        var DALSQL = new GestaoContasDALSQL();
        string cmdSql = DALSQL.ObterServicoDescontoGestaoConta();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("CODSVC", objPesquisar.CODSVCTGV);

        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<GestaoContasTO.obterServicoDescontoGestaoConta>();

    }


    public bool InserirCadastroCondicaoComercialServicoGestaoContas(GestaoContasApiModel.ServicoDescontosGestaodeContasApiModel objInserir)
    {
        var _descmanual = false;
        if ((objInserir.CODCNDCMC == 0) || (objInserir.CODCNDCMC == -1))
        {
            if (objInserir.DESDSC == null)
            {
                objInserir.DESDSC = "Desc. Manual";
            }

            if (objInserir.DESDSC.Trim() == "")
            {
                objInserir.DESDSC = "Desc. Manual";
            }

            _descmanual = true;
        }

        if (objInserir.VLRDSC == null)
            objInserir.VLRDSC = "0";

        if (objInserir.PERDSC == null)
            objInserir.PERDSC = "0";


        var DALSQL = new GestaoContasDALSQL();
        string cmdSql = DALSQL.InserirCadastroCondicaoComercialServicoGestaoContas(_descmanual);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", objInserir.CODCLI);
        dbCommand.AddWithValue("CODSVCTGV", objInserir.CODSVCTGV);
        dbCommand.AddWithValue("CODCNDCMC", objInserir.CODCNDCMC);
        dbCommand.AddWithValue("DESDSC", objInserir.DESDSC);
        dbCommand.AddWithValue("VLRDSC", decimal.Parse(objInserir.VLRDSC));
        dbCommand.AddWithValue("PERDSC", decimal.Parse(objInserir.PERDSC));

        if (objInserir.DATINIVLD != null)
            dbCommand.AddWithValue("DATINIVLD", Convert.ToDateTime(objInserir.DATINIVLD));
        else
            dbCommand.AddWithValue("DATINIVLD", objInserir.DATINIVLD);

        if (objInserir.DATFIMVLD != null)
            dbCommand.AddWithValue("DATFIMVLD", Convert.ToDateTime(objInserir.DATFIMVLD));
        else
            dbCommand.AddWithValue("DATFIMVLD", objInserir.DATFIMVLD);


        var _codigofunc = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        dbCommand.AddWithValue("CODFNCCAD", _codigofunc);


        if ((objInserir.CODPRDCES == -1) || (objInserir.CODPRDCES == 0))
            dbCommand.AddWithValue("CODPRDCES", null);
        else
            dbCommand.AddWithValue("CODPRDCES", objInserir.CODPRDCES);


        //
        dbCommand.TrataDbCommandUniversal();

        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool DesativarCadastroCondicaoComercialServicoGestaoContas(GestaoContasApiModel.ServicoDescontosGestaodeContasApiModel objInserir)
    {

        var DALSQL = new GestaoContasDALSQL();
        string cmdSql = DALSQL.DesativarCadastroCondicaoComercialServicoGestaoContas();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", objInserir.CODCLI);
        dbCommand.AddWithValue("CODSVCTGV", objInserir.CODSVCTGV);
        dbCommand.AddWithValue("CODCNDCMC", objInserir.CODCNDCMC);

        var _codigofunc = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        dbCommand.AddWithValue("CODFNCDST", _codigofunc);

        //
        dbCommand.TrataDbCommandUniversal();

        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public bool AtualizarContaCorrente(GestaoContasApiModel.GestaoContasContaCorrenteApiModel objAtualizar)
    {

        var DALSQL = new GestaoContasDALSQL();
        string cmdSql = DALSQL.AtualizarContaCorrente(objAtualizar.INDDBTAUTPGT);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", objAtualizar.CODCLI);
        dbCommand.AddWithValue("INDDBTAUTPGT", objAtualizar.INDDBTAUTPGT);

        if (objAtualizar.CODBCO > 0)
            dbCommand.AddWithValue("CODBCO", objAtualizar.CODBCO);
        else
            dbCommand.AddWithValue("CODBCO", null);

        if ((objAtualizar.CODAGEBCO != null) && (objAtualizar.CODAGEBCO > 0))
            dbCommand.AddWithValue("CODAGEBCO", objAtualizar.CODAGEBCO);
        else
            dbCommand.AddWithValue("CODAGEBCO", null);

        if ((objAtualizar.NUMDIGVRFAGE != null) && (objAtualizar.NUMDIGVRFAGE != ""))
            dbCommand.AddWithValue("NUMDIGVRFAGE", objAtualizar.NUMDIGVRFAGE);
        else
            dbCommand.AddWithValue("NUMDIGVRFAGE", null);

        if ((objAtualizar.NUMCNTCRRCTT != null) && (objAtualizar.NUMCNTCRRCTT != ""))
            dbCommand.AddWithValue("NUMCNTCRRCTT", objAtualizar.NUMCNTCRRCTT);
        else 
            dbCommand.AddWithValue("NUMCNTCRRCTT", null);

        if ((objAtualizar.NUMDIGVRFCNTCRR != null) && (objAtualizar.NUMDIGVRFCNTCRR != ""))
            dbCommand.AddWithValue("NUMDIGVRFCNTCRR", objAtualizar.NUMDIGVRFCNTCRR);
        else
            dbCommand.AddWithValue("NUMDIGVRFCNTCRR", null);

        var _codigofunc = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        dbCommand.AddWithValue("CODFNCALT", _codigofunc);

        //
        dbCommand.TrataDbCommandUniversal();

        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    
    public int VerificaServicoDescontoGestaoConta(GestaoContasApiModel.ServicoDescontosGestaodeContasApiModel objPesquisar)
    {
        var DALSQL = new GestaoContasDALSQL();
        string cmdSql = DALSQL.VerificaServicoDescontoGestaoConta();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("CODSVCTGV", objPesquisar.CODSVCTGV);
        dbCommand.AddWithValue("CODCNDCMC", objPesquisar.CODCNDCMC);

        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return Convert.ToInt32(MRT001.ExecuteScalar(dbCommand));
    }

    
    public List<GestaoContasTO.ObterDescontosVigencia> ObterDescontosVigencia(GestaoContasApiModel.ServicoDescontosGestaodeContasApiModel objPesquisar)
    {
        var DALSQL = new GestaoContasDALSQL();
        string cmdSql = DALSQL.ObterDescontosVigencia();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("CODSVCTGV", objPesquisar.CODSVCTGV);

        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<GestaoContasTO.ObterDescontosVigencia>();

    }

    
    public bool AtualizaDescontosVigenciaGestaoConta(GestaoContasApiModel.ServicoDescontosGestaodeContasApiModel objAtualizar)
    {

        var DALSQL = new GestaoContasDALSQL();
        string cmdSql = DALSQL.AtualizaDescontosVigenciaGestaoConta();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", objAtualizar.CODCLI);
        dbCommand.AddWithValue("CODSVCTGV", objAtualizar.CODSVCTGV);
        dbCommand.AddWithValue("CODCNDCMC", objAtualizar.CODCNDCMC);

        if (objAtualizar.DATINIVLD != null)
            dbCommand.AddWithValue("DATINIVLD", DateTime.Parse(objAtualizar.DATINIVLD));
        else
            dbCommand.AddWithValue("DATINIVLD", null);


        if (objAtualizar.DATFIMVLD != null)
            dbCommand.AddWithValue("DATFIMVLD", DateTime.Parse(objAtualizar.DATFIMVLD));
        else
            dbCommand.AddWithValue("DATFIMVLD", null);

        //
        dbCommand.TrataDbCommandUniversal();

        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public int BuscaAnomeFaturamento()
    {
        var DALSQL = new GestaoContasDALSQL();
        string cmdSql = DALSQL.BuscaAnomeFaturamento();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToInt32(MRT001.ExecuteScalar(dbCommand));
    }


    public List<GestaoContasTO.ObterDadosGrupoCliPrincipal> BuscaDadosGrupoCliPrincipal(int _codcli, int _codgrup)
    {
        var DALSQL = new GestaoContasDALSQL();
        string cmdSql = DALSQL.BuscaDadosGrupoCliPrincipal();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (_codcli != 0)
            dbCommand.AddWithValue("CODCLIGRPFAT", _codcli);
        else
            dbCommand.AddWithValue("CODCLIGRPFAT", null);


        if (_codgrup != 0)
            dbCommand.AddWithValue("CODGRPEMPCLI", _codgrup);
        else
            dbCommand.AddWithValue("CODGRPEMPCLI", null);

        //
        dbCommand.TrataDbCommandUniversal(true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<GestaoContasTO.ObterDadosGrupoCliPrincipal>();
    }


    public List<GestaoContasTO.ObterRelacaoCliGrupo> BuscaRelacaoCliGrupo(int _codcli, int _codgrupo)
    {
        var DALSQL = new GestaoContasDALSQL();
        string cmdSql = DALSQL.BuscaRelacaoCliGrupo();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //

        if (_codcli != 0)
            dbCommand.AddWithValue("CODCLI", _codcli);
        else
            dbCommand.AddWithValue("CODCLI", null);


        if (_codgrupo != 0)
            dbCommand.AddWithValue("CODGRPEMPCLI", _codgrupo);
        else
            dbCommand.AddWithValue("CODGRPEMPCLI", null);


        //
        dbCommand.TrataDbCommandUniversal(true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<GestaoContasTO.ObterRelacaoCliGrupo>();
    }

    public bool AlteraQuantidade(GestaoContasApiModel.GestaoContasContaAlterarQuantidadeApiModel objAtualzar)
    {
        var DALSQL = new GestaoContasDALSQL();
        string cmdSql = DALSQL.AlteraQuantidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", objAtualzar.CODCLI);
        dbCommand.AddWithValue("CODSVCTGV", objAtualzar.CODSVCTGV);
        dbCommand.AddWithValue("QDEPRDVND", objAtualzar.QDEPRDVND);
        //
        dbCommand.TrataDbCommandUniversal();

        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }



    public bool AlteraTipoDebito(int INDDBTAUTPGT, long CODCLI)
    {
        var DALSQL = new GestaoContasDALSQL();
        string cmdSql = DALSQL.AlteraTipoDebito();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("INDDBTAUTPGT", INDDBTAUTPGT);
        dbCommand.AddWithValue("CODCLI", CODCLI);
        //
        dbCommand.TrataDbCommandUniversal();

        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool AtualizaGrupoServico(GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel objAlterar)
    {
        var DALSQL = new GestaoContasDALSQL();
        string cmdSql = DALSQL.AtualizaGrupoServico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", objAlterar.CODCLI);
        dbCommand.AddWithValue("CODSVCTGV", objAlterar.CODSVCTGV);
        dbCommand.AddWithValue("CODGRPEMPFAT", objAlterar.CODGRPEMPFAT);
        //
        dbCommand.TrataDbCommandUniversal();

        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public bool AtualizaGrupoGetaoServico(GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel objAlterar)
    {
        var DALSQL = new GestaoContasDALSQL();
        string cmdSql = DALSQL.AtualizaGrupoGetaoServico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", objAlterar.CODCLI);
        dbCommand.AddWithValue("CODGRPEMPFAT", objAlterar.CODGRPEMPFAT);
        //
        dbCommand.TrataDbCommandUniversal();

        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    
    public bool AtualizaAllGrupoGetaoServico(GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel objAlterar)
    {
        var DALSQL = new GestaoContasDALSQL();
        string cmdSql = DALSQL.AtualizaAllGrupoGetaoServico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODGRPEMPFAT", objAlterar.CODGRPEMPFAT);
        //
        dbCommand.TrataDbCommandUniversal();

        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool AtualizaApagaGrupoGetaoServico(GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel objAlterar)
    {
        var DALSQL = new GestaoContasDALSQL();
        string cmdSql = DALSQL.AtualizaApagaGrupoGetaoServico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", objAlterar.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal();

        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    
    public bool AtualizaAutomacao(GestaoContasApiModel.GestaoContasContaAlterarAutomacaoApiModel objAlterar)
    {
        var DALSQL = new GestaoContasDALSQL();
        string cmdSql = DALSQL.AtualizaAutomacao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", objAlterar.CODCLI);
        dbCommand.AddWithValue("CODEMPAUT", objAlterar.CODEMPAUT);
        //
        dbCommand.TrataDbCommandUniversal();

        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


}