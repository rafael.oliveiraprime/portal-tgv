﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class GestaoContasDALSQL
{
    public string obterGestaoContas()
    {
        StringBuilder strBld = new StringBuilder(@"  SELECT GC.CODCLI, 
                                                            GC.CODCLI CODCLICHECK,
                                                            CLI.NOMCLI,
                                                            CASE CLI.NUMCGCCLI WHEN CLI.NUMCGCCLI THEN (SUBSTR(CLI.NUMCGCCLI, 1, 2) || '.' || SUBSTR(CLI.NUMCGCCLI, 3, 3) || '.' || SUBSTR(CLI.NUMCGCCLI, 6, 3) || '/' || SUBSTR(CLI.NUMCGCCLI, 9,4) || '-' || SUBSTR(CLI.NUMCGCCLI, 13,2) ) ELSE NULL END NUMCGCCLI,       
                                                            CG.NOMGRPEMPCLI,
                                                            CASE WHEN GC.CODSITCLI = 1 THEN 'EM IMPLANTAÇÃO'
                                                                 WHEN GC.CODSITCLI = 2 THEN 'ATIVO'
                                                                 WHEN GC.CODSITCLI = 3 THEN 'SUSPENSO'
                                                                 WHEN GC.CODSITCLI = 4 THEN 'CANCELADO'  END STATUS,            
                                                            CID.NOMCIDEXD AS NOMCID, 
                                                            CID.CodEstUni AS UF,
                                                            GC.INDDBTAUTPGT,
                                                            GC.CODBCO,
                                                            GC.CODAGEBCO,
                                                            GC.NUMDIGVRFAGE,
                                                            GC.NUMCNTCRRCTT,
                                                            GC.NUMDIGVRFCNTCRR
                                                            ,(SELECT WM_CONCAT(S.DESSVCTGV) FROM MRT.CADCLISVCTGV CS INNER JOIN MRT.CADSVCTGV S ON S.CODSVCTGV = CS.CODSVCTGV WHERE CS.CODCLI = CLI.CODCLI AND CS.INDSTAATV <> 4 ) AS SERVICOS
                                                            ,CG.INDFATGRP
                                                            ,CG.CODGRPEMPCLI
                                                            ,GC.CODEMPAUT
                                                            ,GC.CODEMPAUT
                                                            ,AU.RAZSOCEMP
                                                       FROM MRT.CADCLITGV GC
                                                      INNER JOIN MRT.T0100043 CLI ON CLI.CODCLI = GC.CODCLI   
                                                       LEFT JOIN MRT.RLCCLIGRPEMPTGV GP ON GP.CODCLI = GC.CODCLI
                                                       LEFT JOIN MRT.CADGRPEMPTGV CG ON CG.CODGRPEMPCLI = GP.CODGRPEMPCLI 
                                                       LEFT JOIN MRT.T0138938 END ON CLI.CodCli = END.Codcli AND END.TIPENDCLI = 0
                                                       LEFT JOIN MRT.T0100027 BAI ON END.CodBai = BAI.Codbai
                                                       LEFT JOIN MRT.T0100035 CID ON BAI.CodCid = CID.Codcid
                                                       LEFT JOIN MRT.CADEMPAUTTGV AU ON GC.CODEMPAUT = AU.CODEMPAUT
                                                      WHERE 1 = 1
                                                        AND GC.CODCLI = :CODCLI
                                                        AND CLI.NOMCLI LIKE '%' || UPPER(:NOMCLI) || '%'
                                                        AND CLI.NUMCGCCLI = :NUMCGCCLI
                                                        AND CG.CODGRPEMPCLI = :CODGRPEMPCLI
                                                        AND CID.NOMCIDEXD LIKE '%' || UPPER(:NOMCIDEXD) || '%'
                                                        AND UPPER(CID.CodEstUni) = UPPER(:CodEstUni)
                                                        AND GC.CODSITCLI = :CODSITCLI
                                                ");

        //

        return strBld.ToString();
    }


    public string obeterServicosConta()
    {
        StringBuilder strBld = new StringBuilder(@"  SELECT GCS.CODSVCTGV,
                                                            SR.DESSVCTGV,
                                                            GCS.VLRSVC,
                                                            0 AS VLRTOTSVC,
                                                            CASE WHEN GCS.INDSTAATV = 1 THEN 'EM IMPLANTAÇÃO'
                                                                WHEN GCS.INDSTAATV = 2 THEN 'ATIVO'
                                                                WHEN GCS.INDSTAATV = 3 THEN 'SUSPENSO'
                                                                WHEN GCS.INDSTAATV = 4 THEN 'CANCELADO'  END STATUS,            
                                                            CASE GCS.DATATVSVC WHEN GCS.DATATVSVC THEN TO_CHAR(GCS.DATATVSVC,'DD/MM/YYYY') ELSE NULL END DATATVSVC,
                                                            CASE GCS.DATINISUSSVC WHEN GCS.DATINISUSSVC THEN TO_CHAR(GCS.DATINISUSSVC,'DD/MM/YYYY') ELSE NULL END DATINISUSSVC,       
                                                            CASE GCS.DATFIMSUSSVC WHEN GCS.DATFIMSUSSVC THEN TO_CHAR(GCS.DATFIMSUSSVC,'DD/MM/YYYY') ELSE NULL END DATFIMSUSSVC,
                                                            CASE GCS.DATCNC WHEN GCS.DATCNC THEN TO_CHAR(GCS.DATCNC,'DD/MM/YYYY') ELSE NULL END DATCNC,
                                                            GCS.CODMTVCNC,
                                                            NVL(CNC.DESMTVCNC, ' ') AS MOTIVACANC,
                                                            NVL(QDEPRDVND, 1) AS QDEPRDVND
                                                            ,GCS.CODGRPEMPFAT
                                                            ,CADGRUP.NOMGRPEMPCLI 
                                                       FROM MRT.CADCLISVCTGV GCS
                                                      INNER JOIN MRT.CADSVCTGV SR ON (GCS.CODSVCTGV = SR.CODSVCTGV)
                                                       LEFT JOIN MRT.CADMTVCNCSVCTGV CNC ON (CNC.CODMTVCNC = GCS.CODMTVCNC)
                                                       LEFT JOIN MRT.CADGRPEMPTGV CADGRUP ON(GCS.CODGRPEMPFAT = CADGRUP.CODGRPEMPCLI)
                                                      WHERE 1 = 1
                                                        AND GCS.CODCLI = :CODCLI  
                                                 ");
       
        return strBld.ToString();

    }


    public string AcoesServicoGestaoContas(int _datareativacao) {


        StringBuilder strBld;

        if (_datareativacao == 1)
        {
            strBld = new StringBuilder(@"  UPDATE MRT.CADCLISVCTGV SET INDSTAATV = :INDSTAATV,
                                                                                 DATATVSVC = :DATATVSVC,
                                                                                 CODFNCATV = :CODFNCATV,
                                                                                 DATINISUSSVC = :DATINISUSSVC,
                                                                                 DATFIMSUSSVC = :DATFIMSUSSVC,
                                                                                 CODFNCSUS = :CODFNCSUS,
                                                                                 DATCNC = :DATCNC,
                                                                                 CODFNCCNC = :CODFNCCNC,
                                                                                 CODMTVCNC = :CODMTVCNC,
                                                                                 PERCOBMESATVSVC = :PERCOBMESATVSVC,
                                                                                 PERCOBMESCNCSVC = :PERCOBMESCNCSVC
                                                      WHERE CODCLI = :CODCLI
                                                        AND CODSVCTGV = :CODSVCTGV

                                                 ");
        }
        else
        {
            strBld = new StringBuilder(@"  UPDATE MRT.CADCLISVCTGV SET INDSTAATV = :INDSTAATV,
                                                                                 CODFNCATV = :CODFNCATV,
                                                                                 DATINISUSSVC = :DATINISUSSVC,
                                                                                 DATFIMSUSSVC = :DATFIMSUSSVC,
                                                                                 CODFNCSUS = :CODFNCSUS,
                                                                                 DATCNC = :DATCNC,
                                                                                 CODFNCCNC = :CODFNCCNC,
                                                                                 CODMTVCNC = :CODMTVCNC,
                                                                                 PERCOBMESATVSVC = :PERCOBMESATVSVC,
                                                                                 PERCOBMESCNCSVC = :PERCOBMESCNCSVC
                                                      WHERE CODCLI = :CODCLI
                                                        AND CODSVCTGV = :CODSVCTGV

                                                 ");
        }


        return strBld.ToString();

    }

    public string obeterStatusServicosConta()
    {
        StringBuilder strBld = new StringBuilder(@"  SELECT CODSVCTGV,
                                                            INDSTAATV            
                                                       FROM MRT.CADCLISVCTGV 
                                                      WHERE CODCLI = :CODCLI  
                                                 ");

        return strBld.ToString();
    }

    public string AtualizaStatusGestaoContas()
    {

        StringBuilder strBld = new StringBuilder(@"  UPDATE MRT.CADCLITGV SET CODSITCLI = :CODSITCLI
                                                      WHERE CODCLI = :CODCLI

                                                 ");

        return strBld.ToString();

    }

    public string ObterServicoDescontoGestaoConta()
    {
        StringBuilder strBld = new StringBuilder(@"  SELECT DSC.CODCNDCMC, 
                                                            DSC.DESDSC AS DESCNDCMC,      
                                                            CSP.DESPRDCES AS CESTPROD,
                                                            DSC.PERDSC,
                                                            DSC.VLRDSC,
                                                            CASE DSC.DATINIVLD WHEN DSC.DATINIVLD THEN TO_CHAR(DSC.DATINIVLD,'DD/MM/YYYY') ELSE NULL END DATINIVLD,
                                                            CASE DSC.DATFIMVLD WHEN DSC.DATFIMVLD THEN TO_CHAR(DSC.DATFIMVLD,'DD/MM/YYYY') ELSE NULL END DATFIMVLD,
                                                            DECODE(DSC.NUMMESINIVLD, NULL, NULL, DSC.NUMMESINIVLD || '°' ) NUMMESINIVLD,
                                                            DECODE(DSC.NUMMESFIMVLD, NULL, NULL, DSC.NUMMESFIMVLD || '°') NUMMESFIMVLD,
                                                            CASE WHEN DSC.DATDST IS NULL THEN 'ATIVO' ELSE 'DESATIVADO' END AS STATUS,
                                                            CASE WHEN DSC.DATDST IS NULL THEN TO_CHAR(DSC.DATCAD,'DD/MM/YYYY') ELSE TO_CHAR(DSC.DATDST,'DD/MM/YYYY') END AS DATAALTERACAO
                                                       FROM MRT.CADCLISVCDSCTGV DSC
                                                       LEFT JOIN MRT.CADCESPRDTGV CSP ON DSC.CODPRDCES = CSP.CODPRDCES                                                      
                                                      WHERE DSC.CODCLI = :CODCLI
                                                        AND DSC.CODSVCTGV = :CODSVC
                                                      ORDER BY CASE WHEN DSC.DATDST IS NULL THEN TO_CHAR(DSC.DATCAD,'DD/MM/YYYY') ELSE TO_CHAR(DSC.DATDST,'DD/MM/YYYY') END DESC
                                                 ");

        return strBld.ToString();
    }


    public string InserirCadastroCondicaoComercialServicoGestaoContas(bool _descmanual)
    {
        StringBuilder strBld;

        if (_descmanual)
        {
            strBld = new StringBuilder(@" INSERT INTO MRT.CADCLISVCDSCTGV (CODCLI, CODSVCTGV, CODCNDCMC, DESDSC, PERDSC, VLRDSC, DATINIVLD, DATFIMVLD, DATCAD, CODFNCCAD, CODPRDCES, INDSTADSC)
                                                                   VALUES (:CODCLI, 
                                                                            :CODSVCTGV, 
                                                                            (SELECT (NVL(MAX(CODCNDCMC), 8999)) + 1  FROM MRT.CADCLISVCDSCTGV WHERE CODCLI = :CODCLI AND CODSVCTGV = :CODSVCTGV AND CODCNDCMC >= 9000), 
                                                                            UPPER(:DESDSC), 
                                                                            :PERDSC, 
                                                                            :VLRDSC, 
                                                                            :DATINIVLD, 
                                                                            :DATFIMVLD, 
                                                                            SYSDATE, 
                                                                            :CODFNCCAD,
                                                                            :CODPRDCES,
                                                                            1)
                                                 ");
        }
        else
        {
            strBld = new StringBuilder(@" INSERT INTO MRT.CADCLISVCDSCTGV (CODCLI, CODSVCTGV, CODCNDCMC, DESDSC, PERDSC, VLRDSC, DATINIVLD, DATFIMVLD, DATCAD, CODFNCCAD, CODPRDCES, INDSTADSC)
                                                                   VALUES (:CODCLI, :CODSVCTGV, :CODCNDCMC, UPPER(:DESDSC), :PERDSC, :VLRDSC, :DATINIVLD, :DATFIMVLD, SYSDATE, :CODFNCCAD, :CODPRDCES, 1)
                                                 ");
        }

        return strBld.ToString();

    }


    public string DesativarCadastroCondicaoComercialServicoGestaoContas()
    {
        StringBuilder strBld = new StringBuilder(@"  UPDATE MRT.CADCLISVCDSCTGV SET DATDST = SYSDATE,
                                                                                    CODFNCDST = :CODFNCDST,
                                                                                    INDSTADSC = 0
                                                      WHERE DATDST IS NULL
                                                        AND CODCLI = :CODCLI
                                                        AND CODSVCTGV = :CODSVCTGV
                                                        AND CODCNDCMC = :CODCNDCMC                                                                
                                                 ");

        return strBld.ToString();
    }

    public string AtualizarContaCorrente(int _tipocobranca)
    {
        StringBuilder strBld;

            strBld = new StringBuilder(@"  UPDATE MRT.CADCLITGV SET INDDBTAUTPGT = :INDDBTAUTPGT,
                                                                    CODBCO = :CODBCO,
                                                                    CODAGEBCO = :CODAGEBCO,
                                                                    NUMDIGVRFAGE = :NUMDIGVRFAGE,
                                                                    NUMCNTCRRCTT = :NUMCNTCRRCTT,
                                                                    NUMDIGVRFCNTCRR = :NUMDIGVRFCNTCRR,
                                                                    DATALT = SYSDATE,
                                                                    CODFNCALT = :CODFNCALT
                                                      WHERE CODCLI = :CODCLI
                                                 ");

        return strBld.ToString();
    }

    public string VerificaServicoDescontoGestaoConta()
    {
        StringBuilder strBld = new StringBuilder(@"  SELECT COUNT(DSC.CODCNDCMC) 
                                                       FROM MRT.CADCLISVCDSCTGV DSC
                                                      WHERE DSC.CODCLI = :CODCLI
                                                        AND DSC.CODSVCTGV = :CODSVCTGV
                                                        AND DSC.CODCNDCMC = :CODCNDCMC
                                                        AND DATDST IS NULL
                                                 ");

        return strBld.ToString();
    }

    public string ObterDescontosVigencia()
    {
        StringBuilder strBld = new StringBuilder(@"  SELECT CODCLI, 
                                                            CODSVCTGV, 
                                                            CODCNDCMC, 
                                                            NUMMESINIVLD,
                                                            NUMMESFIMVLD
                                                       FROM MRT.CADCLISVCDSCTGV 
                                                      WHERE ((NUMMESINIVLD IS NOT NULL) OR (NUMMESFIMVLD IS NOT NULL)) 
                                                        AND CODCLI = :CODCLI
                                                        AND CODSVCTGV = :CODSVCTGV                                                         
                                                 ");

        return strBld.ToString();
    }



    public string AtualizaDescontosVigenciaGestaoConta()
    {
        StringBuilder strBld = new StringBuilder(@"  UPDATE MRT.CADCLISVCDSCTGV SET DATINIVLD = :DATINIVLD,
                                                                                    DATFIMVLD = :DATFIMVLD
                                                      WHERE CODCLI = :CODCLI
                                                        AND CODSVCTGV = :CODSVCTGV
                                                        AND CODCNDCMC = :CODCNDCMC                                                         
                                                 ");

        return strBld.ToString();
    }


    public string BuscaAnomeFaturamento()
    {
        StringBuilder strBld = new StringBuilder(@" SELECT NVL(MAX(ANOMESREF), 0) AS ANOMESREF
                                                      FROM MRT.MOVMESFATRSMTGV
                                                      WHERE DATENVFAT IS NOT NULL
                                                 ");

        return strBld.ToString();
    }

    public string BuscaDadosGrupoCliPrincipal()
    {
        StringBuilder strBld = new StringBuilder(@" SELECT CODGRPEMPCLI, NOMGRPEMPCLI, INDFATGRP, CODCLIGRPFAT
                                                      FROM MRT.CADGRPEMPTGV
                                                     WHERE 1 = 1 
                                                       AND CODCLIGRPFAT = :CODCLIGRPFAT
                                                       AND CODGRPEMPCLI = :CODGRPEMPCLI
                                                 ");

        return strBld.ToString();
    }

    public string BuscaRelacaoCliGrupo()
    {
        StringBuilder strBld = new StringBuilder(@" SELECT CODGRPEMPCLI, CODCLI
                                                      FROM MRT.RLCCLIGRPEMPTGV 
                                                     WHERE 1 = 1
                                                       AND CODCLI = :CODCLI
                                                       AND CODGRPEMPCLI = :CODGRPEMPCLI
                                                 ");

        return strBld.ToString();
    }

    public string AlteraQuantidade()
    {
        StringBuilder strBld;

            strBld = new StringBuilder(@"  UPDATE MRT.CADCLISVCTGV SET QDEPRDVND = :QDEPRDVND
                                            WHERE CODCLI = :CODCLI
                                              AND CODSVCTGV = :CODSVCTGV
                                       ");

        return strBld.ToString();
    }


    public string AlteraTipoDebito()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@"  UPDATE MRT.CADCLITGV SET INDDBTAUTPGT = :INDDBTAUTPGT
                                        WHERE CODCLI = :CODCLI 
                                   ");

        return strBld.ToString();
    }

    
    public string AtualizaGrupoServico()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@"  UPDATE MRT.CADCLISVCTGV SET CODGRPEMPFAT = :CODGRPEMPFAT
                                            WHERE CODCLI = :CODCLI
                                              AND CODSVCTGV = :CODSVCTGV
                                       ");

        return strBld.ToString();
    }


    public string AtualizaGrupoGetaoServico()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" UPDATE MRT.CADCLISVCTGV SET CODGRPEMPFAT = :CODGRPEMPFAT
                                       WHERE CODCLI = :CODCLI
                                   ");

        return strBld.ToString();
    }


    public string AtualizaAllGrupoGetaoServico()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" UPDATE MRT.CADCLISVCTGV SET CODGRPEMPFAT = null
                                       WHERE CODGRPEMPFAT = :CODGRPEMPFAT
                                   ");

        return strBld.ToString();
    }


    public string AtualizaApagaGrupoGetaoServico()
    {

        StringBuilder strBld;

        strBld = new StringBuilder(@" UPDATE MRT.CADCLISVCTGV SET CODGRPEMPFAT = null
                                       WHERE CODCLI = :CODCLI
                                   ");

        return strBld.ToString();
    }


    public string AtualizaAutomacao()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" UPDATE MRT.CADCLITGV SET CODEMPAUT = :CODEMPAUT
                                       WHERE CODCLI = :CODCLI
                                   ");

        return strBld.ToString();
    }


}