﻿using System.Collections.Generic;

namespace GestaoContasTO
{
    public class obterGestaoContas
    {
        public int CODCLI { get; set; }
        public int CODCLICHECK { get; set; }
        public string NOMCLI { get; set; }
        public string NUMCGCCLI { get; set; }
        public string NOMGRPEMPCLI { get; set; }
        public string STATUS { get; set; }
        public string NOMCID { get; set; }
        public string UF { get; set; }
        public int INDDBTAUTPGT { get; set; }
        public int CODBCO { get; set; }
        public int CODAGEBCO { get; set; }
        public string NUMDIGVRFAGE { get; set; }
        public string NUMCNTCRRCTT { get; set; }
        public string NUMDIGVRFCNTCRR { get; set; }
        public string SERVICOS { get; set; }
        public int INDFATGRP { get; set; }
        public int CODGRPEMPCLI { get; set; }
        public int CODEMPAUT { get; set; }
        public string RAZSOCEMP { get; set; }
    }

    public class obeterServicosConta
    {
        public int CODSVCTGV { get; set; }
        public string DESSVCTGV { get; set; }
        public decimal VLRSVC { get; set; }
        public decimal VLRTOTSVC { get; set; }
        public string STATUS { get; set; }
        public string DATATVSVC { get; set; }
        public string DATINISUSSVC { get; set; }
        public string DATFIMSUSSVC { get; set; }
        public string DATCNC { get; set; }
        public int CODMTVCNC { get; set; }
        public string MOTIVACANC { get; set; }
        public int QDEPRDVND { get; set; }
        public int CODGRPEMPFAT { get; set; }
        public string NOMGRPEMPCLI { get; set; }
    }

    public class obterServicosCliente
    {
        public string DESSVCTGV { get; set; }
    }

    public class AcoesServicosConta
    {
        public int CODSVCTGV { get; set; }
        public int CODCLI { get; set; }
    }

    public class obeterStatusServicosConta
    {
        public int CODSVCTGV { get; set; }
        public int INDSTAATV { get; set; }
    }

    public class obterServicoDescontoGestaoConta
    {
        public int CODCNDCMC { get; set; }
        public string DESCNDCMC { get; set; }
        public string CESTPROD { get; set; }
        public decimal PERDSC { get; set; }
        public decimal VLRDSC { get; set; }
        public string DATINIVLD { get; set; }
        public string DATFIMVLD { get; set; }
        public string NUMMESINIVLD { get; set; }
        public string NUMMESFIMVLD { get; set; }
        public string STATUS { get; set; }
        public string DATAALTERACAO { get; set; }
    }

    public class ObterDescontosVigencia
    {
        public int CODCLI { get; set; }
        public int CODSVCTGV { get; set; }
        public int CODCNDCMC { get; set; }
        public int NUMMESINIVLD { get; set; }
        public int NUMMESFIMVLD { get; set; }
    }

    public class ObterDadosGrupoCliPrincipal
    {
        public int CODGRPEMPCLI { get; set; }
        public string NOMGRPEMPCLI { get; set; }
        public int INDFATGRP { get; set; }
        public int CODCLIGRPFAT { get; set; }
    }

    public class ObterRelacaoCliGrupo
    {
        public int CODGRPEMPCLI { get; set; }
        public int CODCLI { get; set; }
    }

}