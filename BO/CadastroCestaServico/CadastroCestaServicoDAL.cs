﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CadastroCestaServicoDAL : DAL
{
    public List<CadastroCestaServicoTO.obterCadastroCestaServico> ObterCadastroCestaServico(CadastroCestaServicoApiModel.CadastroCestaServicoApiModel objPesquisar)
    {
        var DALSQL = new CadastroCestaServicoDALSQL();
        string cmdSql = DALSQL.obterCadastroCestaServico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (objPesquisar.CODPRDCES == 0)
        {
            objPesquisar.CODPRDCES = -1;
        }
        dbCommand.AddWithValue("CODPRDCES", objPesquisar.CODPRDCES);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroCestaServicoTO.obterCadastroCestaServico>();
    }

    public bool InserirCadastroCestaServico(CadastroCestaServicoApiModel.CadastroCestaServicoApiModel objInserir)
    {
        var DALSQL = new CadastroCestaServicoDALSQL();
        string cmdSql = DALSQL.inserirCadastroCestaServico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objInserir.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        objInserir.CODFNCDST = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //
        if (objInserir.FLGIPDSVC == -1)
        {
            objInserir.FLGIPDSVC = 1;
        }
        //
        dbCommand.AddWithValue("DESPRDCES", objInserir.DESPRDCES);
        dbCommand.AddWithValue("NUMSEQ", objInserir.NUMSEQ);
        dbCommand.AddWithValue("TIPDDOATR", objInserir.TIPDDOATR);
        dbCommand.AddWithValue("FLGIPDSVC", objInserir.FLGIPDSVC);
        dbCommand.AddWithValue("CODFNCALT", objInserir.CODFNCALT);
        dbCommand.AddWithValue("CODFNCDST", objInserir.CODFNCDST);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool alterarCadastroCestaServico(CadastroCestaServicoApiModel.CadastroCestaServicoApiModel objAlterar)
    {
        var DALSQL = new CadastroCestaServicoDALSQL();
        string cmdSql = DALSQL.alterarCadastroCestaServico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //
        dbCommand.AddWithValue("CODPRDCES", objAlterar.CODPRDCES);
        dbCommand.AddWithValue("DESPRDCES", objAlterar.DESPRDCES);
        dbCommand.AddWithValue("NUMSEQ", objAlterar.NUMSEQ);
        dbCommand.AddWithValue("TIPDDOATR", objAlterar.TIPDDOATR);
        dbCommand.AddWithValue("FLGIPDSVC", objAlterar.FLGIPDSVC);
        dbCommand.AddWithValue("CODFNCALT", objAlterar.CODFNCALT);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool ativarCadastroCestaServico(CadastroCestaServicoApiModel.CadastroCestaServicoApiModel objAlterar)
    {
        var DALSQL = new CadastroCestaServicoDALSQL();
        string cmdSql = DALSQL.ativarCadastroCestaServico(objAlterar.LISTCODPRDCES);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool desativarCadastroCestaServico(CadastroCestaServicoApiModel.CadastroCestaServicoApiModel objAlterar)
    {
        var DALSQL = new CadastroCestaServicoDALSQL();
        string cmdSql = DALSQL.desativarCadastroCestaServico(objAlterar.LISTCODPRDCES);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCDST = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //                     
        dbCommand.AddWithValue("CODFNCDST", objAlterar.CODFNCDST);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }
}