﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CadastroCestaServicoBLL
{
    public List<CadastroCestaServicoTO.obterCadastroCestaServico> ObterCadastroCestaServico(CadastroCestaServicoApiModel.CadastroCestaServicoApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroCestaServicoDAL();
            return DAL.ObterCadastroCestaServico(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO CONDICAO COMERCIAL.");
            Utilitario.InsereLog(ex, "alterarCadastroCondicao", "CadastroCondicaoApiModel.CadastroCondicaoApiModel", "CadastroCondicaoBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO CONDICAO COMERCIAL.");
            throw;
        }
    }

    public bool InserirCadastroCestaServico(CadastroCestaServicoApiModel.CadastroCestaServicoApiModel objInserir)
    {
        try
        {
            var DAL = new CadastroCestaServicoDAL();
            return DAL.InserirCadastroCestaServico(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO CADASTRO CONDICAO COMERCIAL.");
            Utilitario.InsereLog(ex, "inserirCadastroCondicao", "CadastroCondicaoApiModel.CadastroCondicaoApiModel", "CadastroCondicaoBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO CONDICAO COMERCIAL.");
            throw;
        }
    }

    public bool alterarCadastroCestaServico(CadastroCestaServicoApiModel.CadastroCestaServicoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroCestaServicoDAL();
            return DAL.alterarCadastroCestaServico(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO CADASTRO CONDICAO COMERCIAL.");
            Utilitario.InsereLog(ex, "alterarCadastroCondicao", "CadastroCondicaoApiModel.CadastroCondicaoApiModel", "CadastroCondicaoBLL", "ERRO AO FAZER A ALTERACAO DO CADASTRO CONDICAO COMERCIAL.");
            throw;
        }
    }

    public bool ativarCadastroCestaServico(CadastroCestaServicoApiModel.CadastroCestaServicoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroCestaServicoDAL();
            return DAL.ativarCadastroCestaServico(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATIVACAO DO CADASTRO CESTA SERVICO.");
            Utilitario.InsereLog(ex, "ativarCadastroCestaServico", "CadastroCestaServicoApiModel.CadastroCestaServicoApiModel", "CadastroCestaServicoBLL", "ERRO AO FAZER A ATIVACAO DO CADASTRO CESTA SERVICO.");
            throw;
        }
    }

    public bool desativarCadastroCestaServico(CadastroCestaServicoApiModel.CadastroCestaServicoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroCestaServicoDAL();
            return DAL.desativarCadastroCestaServico(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATIVACAO DO CADASTRO CESTA SERVICO.");
            Utilitario.InsereLog(ex, "ativarCadastroCestaServico", "CadastroCestaServicoApiModel.CadastroCestaServicoApiModel", "CadastroCestaServicoBLL", "ERRO AO FAZER A ATIVACAO DO CADASTRO CESTA SERVICO.");
            throw;
        }
    }
}