﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class CadastroCestaServicoDALSQL
{
    public string obterCadastroCestaServico()
    {
        StringBuilder strBld = new StringBuilder(@"
                                   SELECT CES.CODPRDCES
                                        , CES.CODPRDCES CODPRDCESCHECK
                                        , CES.DESPRDCES
                                        , CES.NUMSEQ
                                        , CASE WHEN CES.TIPDDOATR = 1 
                                            THEN 'NUMERO' WHEN CES.TIPDDOATR = 2 
                                            THEN 'TEXTO' 
                                          END NOMTIPDDOATR
                                        , CES.TIPDDOATR
                                        , CES.FLGIPDSVC
                                        , CES.DATALT
                                        , CES.CODFNCALT
                                        , CES.DATDST
                                        , CES.CODFNCDST
                                        , CASE CES.CODFNCDST 
                                            WHEN CES.CODFNCDST THEN 'DESATIVADO' 
                                            ELSE 'ATIVO' 
                                          END STATUS
                                        , CASE CES.FLGIPDSVC 
                                            WHEN 0 THEN 'NÃO'
                                            WHEN 1 THEN 'SIM'
                                            WHEN -1 THEN 'NÃO'
                                          END IMPORTA
                                        , TRIM(FNC_ALT.NOMFNC) FNCALT
                                        , TO_CHAR(CES.DATALT,'DD/MM/YYYY') DATFNCALT
                                        , CASE CES.DATDST
                                            WHEN CES.DATDST THEN TRIM(FNC_DST.NOMFNC) 
                                            ELSE NULL
                                          END FNCDST
                                        , CASE CES.DATDST WHEN 
                                            CES.DATDST THEN TO_CHAR(CES.DATDST,'DD/MM/YYYY') 
                                            ELSE NULL 
                                          END DATFNCDST                 
                                FROM MRT.CADCESPRDTGV CES 
                                LEFT JOIN MRT.T0100361 FNC_ALT ON FNC_ALT.CODFNC = CES.CODFNCALT
                                LEFT JOIN MRT.T0100361 FNC_DST ON FNC_DST.CODFNC = CES.CODFNCDST        
                                WHERE CODPRDCES = :CODPRDCES
                                    ORDER BY STATUS, CODPRDCES
                                                  ");

        return strBld.ToString();
    }

    public string inserirCadastroCestaServico()
    {
        return @" INSERT INTO MRT.CADCESPRDTGV (CODPRDCES, DESPRDCES, NUMSEQ, TIPDDOATR, FLGIPDSVC, CODFNCALT, DATALT)
                                                   VALUES ((SELECT COALESCE(MAX(CODPRDCES),0)+1 FROM MRT.CADCESPRDTGV),
                                                   UPPER(:DESPRDCES), :NUMSEQ, :TIPDDOATR, :FLGIPDSVC, :CODFNCALT, SYSDATE) ";
    }

    public string alterarCadastroCestaServico()
    {
        return @"
                 UPDATE MRT.CADCESPRDTGV SET
                 DESPRDCES = UPPER(:DESPRDCES), 
                 NUMSEQ = :NUMSEQ, 
                 TIPDDOATR = :TIPDDOATR, 
                 FLGIPDSVC = :FLGIPDSVC,
                 CODFNCALT = :CODFNCALT,                            
                 DATALT = SYSDATE                       
                 WHERE  CODPRDCES = :CODPRDCES
                ";
    }

    public string ativarCadastroCestaServico(List<long> LISTCODPRDCES)
    {

        return @"
                 UPDATE MRT.CADCESPRDTGV SET                 
                 CODFNCDST = NULL,
                 DATDST = NULL
                 WHERE CODPRDCES IN(#LISTCODPRDCES)
                ".Replace("#LISTCODPRDCES", Utilitario.ToListStr(LISTCODPRDCES));
    }

    public string desativarCadastroCestaServico(List<long> LISTCODPRDCES)
    {
        return @"
                 UPDATE MRT.CADCESPRDTGV SET                                 
                 CODFNCDST = :CODFNCDST,
                 DATDST = SYSDATE
                 WHERE CODPRDCES IN(#LISTCODPRDCES)
                ".Replace("#LISTCODPRDCES", Utilitario.ToListStr(LISTCODPRDCES));
    }
}