﻿using System.Collections.Generic;

namespace CadastroCestaServicoTO
{
    public class obterCadastroCestaServico
    {
        public int CODPRDCES { get; set; }

        public int CODPRDCESCHECK { get; set; }

        public string DESPRDCES { get; set; }

        public int NUMSEQ { get; set; }

        public string TIPDDOATR { get; set; }

        public string NOMTIPDDOATR { get; set; }

        public int FLGIPDSVC { get; set; }

        public string DATALT { get; set; }

        public string DATDST { get; set; }

        public string IMPORTA { get; set; }

        public string STATUS { get; set; }

        public string FNCALT { get; set; }

        public string DATFNCALT { get; set; }

        public string FNCDST { get; set; }

        public string DATFNCDST { get; set; }
    }
}

