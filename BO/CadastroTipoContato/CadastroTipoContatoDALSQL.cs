﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class CadastroTipoContatoDALSQL
{
    public string obterCadastroTipoContato(int flgAtivo, int codTipCto)
    {
        StringBuilder strBld = new StringBuilder(@"                 
                 SELECT TIP.CODTIPCTO, TIP.CODTIPCTO CODTIPCTOCHECK, TIP.DESTIPCTO,                   
                 CASE TIP.CODFNCDST WHEN TIP.CODFNCDST THEN 'DESATIVADO' ELSE 'ATIVADO' END STATUS,                              
                 TRIM(FNC_ALT.NOMFNC) FNCALT,
                 TO_CHAR(TIP.DATALT,'DD/MM/YYYY') DATFNCALT,
                 CASE TIP.DATDST WHEN TIP.DATDST THEN TRIM(FNC_DST.NOMFNC) ELSE NULL END FNCDST,
                 CASE TIP.DATDST WHEN TIP.DATDST THEN TO_CHAR(TIP.DATDST,'DD/MM/YYYY') ELSE NULL END DATFNCDST
                 FROM MRT.CADTIPCTOTGV TIP
                 LEFT JOIN MRT.T0100361 FNC_ALT ON FNC_ALT.CODFNC = TIP.CODFNCALT
                 LEFT JOIN MRT.T0100361 FNC_DST ON FNC_DST.CODFNC = TIP.CODFNCDST 
                 WHERE TIP.CODTIPCTO = :CODTIPCTO
                ");

        if (flgAtivo == 1)
        {
            if (codTipCto == -1)
            {
                strBld.AppendLine(" WHERE DATDST IS NULL ");
            }
            else
            {
                strBld.AppendLine(" AND DATDST IS NULL ");
            }
        }
        if (flgAtivo == 2)
        {
            if (codTipCto == -1)
            {
                strBld.AppendLine(" WHERE DATDST IS NOT NULL ");
            }
            else
            {
                strBld.AppendLine(" AND DATDST IS NOT NULL ");
            }
        }

        strBld.AppendLine("ORDER BY STATUS, CODTIPCTO");

        return strBld.ToString();
    }

    public string inserirCadastroTipoContato()
    {
        return @"
                 INSERT INTO MRT.CADTIPCTOTGV (CODTIPCTO, DESTIPCTO, CODFNCALT, DATALT)
                 VALUES ((SELECT COALESCE(MAX(CODTIPCTO),0)+1 FROM MRT.CADTIPCTOTGV),
                 UPPER(:DESTIPCTO), :CODFNCALT, SYSDATE)
                ";
    }

    public string alterarCadastroTipoContato()
    {
        return @"
                 UPDATE MRT.CADTIPCTOTGV SET
                 DESTIPCTO = UPPER(:DESTIPCTO),                                 
                 CODFNCALT = :CODFNCALT,
                 DATALT = SYSDATE                
                 WHERE CODTIPCTO = :CODTIPCTO
                ";

    }

    public string ativarCadastroTipoContato(List<long> LISTCODTIPCTO)
    {
        return @"
                 UPDATE MRT.CADTIPCTOTGV SET                 
                 CODFNCDST = NULL,
                 DATDST = NULL
                 WHERE CODTIPCTO IN(#LISTCODTIPCTO)
                ".Replace("#LISTCODTIPCTO", Utilitario.ToListStr(LISTCODTIPCTO));
    }

    public string desativarCadastroTipoContato(List<long> LISTCODTIPCTO)
    {
        return @"
                 UPDATE MRT.CADTIPCTOTGV SET
                 CODFNCDST = :CODFNCDST,
                 DATDST = SYSDATE
                 WHERE CODTIPCTO IN(#LISTCODTIPCTO)
                ".Replace("#LISTCODTIPCTO", Utilitario.ToListStr(LISTCODTIPCTO));
    }
}