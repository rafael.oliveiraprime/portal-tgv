﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CadastroTipoContatoBLL
{
    public List<CadastroTipoContatoTO.obterCadastroTipoContato> obterCadastroTipoContato(CadastroTipoContatoApiModel.CadastroTipoContatoApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroTipoContatoDAL();
            return DAL.obterCadastroTipoContato(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO DE.");
            Utilitario.InsereLog(ex, "obterCadastroTipoContato", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroTipoContatoBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO DEPARTAMENTO.");
            throw;
        }
    }

    public bool inserirCadastroTipoContato(CadastroTipoContatoApiModel.CadastroTipoContatoApiModel objInserir)
    {
        try
        {
            var DAL = new CadastroTipoContatoDAL();
            return DAL.inserirCadastroTipoContato(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO CADASTRO TIPO CONTATO.");
            Utilitario.InsereLog(ex, "inserirCadastroTipoContato", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroTipoContatoBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO DEPARTAMENTO.");
            throw;
        }
    }

    public bool alterarCadastroTipoContato(CadastroTipoContatoApiModel.CadastroTipoContatoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroTipoContatoDAL();
            return DAL.alterarCadastroTipoContato(objAlterar);
        }

        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO CADASTRO TIPO CONTATO.");
            Utilitario.InsereLog(ex, "inserirCadastroTipoContato", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroTipoContatoBLL", "ERRO AO FAZER A ALTERACAO DO CADASTRO DEPARTAMENTO.");
            throw;
        }
    }

    public bool ativarCadastroTipoContato(CadastroTipoContatoApiModel.CadastroTipoContatoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroTipoContatoDAL();
            return DAL.ativarCadastroTipoContato(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATIVACAO DO CADASTRO MOTIVO CONTATO.");
            Utilitario.InsereLog(ex, "ativarCadastroTipoContato", "CadastroTipoContatoApiModel.CadastroTipoContatoApiModel", "CadastroTipoContatoBLL", "ERRO AO FAZER A ATIVACAO DO CADASTRO TIPO CONTATO.");
            throw;
        }
    }

    public bool desativarCadastroTipoContato(CadastroTipoContatoApiModel.CadastroTipoContatoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroTipoContatoDAL();
            return DAL.desativarCadastroTipoContato(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A DESATIVACAO DO CADASTRO MOTIVO CONTATO.");
            Utilitario.InsereLog(ex, "desativarCadastroTipoContato", "CadastroTipoContatoApiModel.CadastroTipoContatoApiModel", "CadastroTipoContatoBLL", "ERRO AO FAZER A DESATIVACAO DO CADASTRO TIPO CONTATO.");
            throw;
        }
    }
}
