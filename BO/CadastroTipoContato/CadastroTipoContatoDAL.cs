﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CadastroTipoContatoDAL
/// </summary>
public class CadastroTipoContatoDAL : DAL
{
    public List<CadastroTipoContatoTO.obterCadastroTipoContato> obterCadastroTipoContato(CadastroTipoContatoApiModel.CadastroTipoContatoApiModel objPesquisar)
    {
        var DALSQL = new CadastroTipoContatoDALSQL();
        string cmdSql = DALSQL.obterCadastroTipoContato(objPesquisar.TIPCTOATI, objPesquisar.CODTIPCTO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("CODTIPCTO", objPesquisar.CODTIPCTO);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroTipoContatoTO.obterCadastroTipoContato>();
    }

    public bool inserirCadastroTipoContato(CadastroTipoContatoApiModel.CadastroTipoContatoApiModel objInserir)
    {
        var DALSQL = new CadastroTipoContatoDALSQL();
        string cmdSql = DALSQL.inserirCadastroTipoContato();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        objInserir.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //        
        dbCommand.AddWithValue("DESTIPCTO", objInserir.DESTIPCTO);
        dbCommand.AddWithValue("CODFNCALT", objInserir.CODFNCALT);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool alterarCadastroTipoContato(CadastroTipoContatoApiModel.CadastroTipoContatoApiModel objAlterar)
    {
        var DALSQL = new CadastroTipoContatoDALSQL();
        string cmdSql = DALSQL.alterarCadastroTipoContato();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //              
        dbCommand.AddWithValue("CODTIPCTO", objAlterar.CODTIPCTO);
        dbCommand.AddWithValue("DESTIPCTO", objAlterar.DESTIPCTO);
        dbCommand.AddWithValue("CODFNCALT", objAlterar.CODFNCALT);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool ativarCadastroTipoContato(CadastroTipoContatoApiModel.CadastroTipoContatoApiModel objAlterar)
    {
        var DALSQL = new CadastroTipoContatoDALSQL();
        string cmdSql = DALSQL.ativarCadastroTipoContato(objAlterar.LISTCODTIPCTO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool desativarCadastroTipoContato(CadastroTipoContatoApiModel.CadastroTipoContatoApiModel objAlterar)
    {
        var DALSQL = new CadastroTipoContatoDALSQL();
        string cmdSql = DALSQL.desativarCadastroTipoContato(objAlterar.LISTCODTIPCTO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCDST = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //              
        dbCommand.AddWithValue("CODFNCDST", objAlterar.CODFNCDST);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }
}