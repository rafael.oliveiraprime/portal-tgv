﻿using System.Collections.Generic;

namespace CadastroTipoContatoTO
{
    public class obterCadastroTipoContato
    {
        public int CODTIPCTO { get; set; }

        public int CODTIPCTOCHECK { get; set; }

        public string DESTIPCTO { get; set; }

        public string DATFNCALT { get; set; }

        public string FNCALT { get; set; }

        public string DATFNCDST { get; set; }

        public string FNCDST { get; set; }

        public string STATUS { get; set; }
    }
}