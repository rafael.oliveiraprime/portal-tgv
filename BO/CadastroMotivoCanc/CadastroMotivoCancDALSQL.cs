﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class CadastroMotivoCancDALSQL
{
    public string obterCadastroMotivoCanc(int flgAtivo, int CODMTVCNC)
    {
        StringBuilder strBld = new StringBuilder(@"
                SELECT MTV.CODMTVCNC
                       ,MTV.CODMTVCNC CODMTVCNCCHECK
                       ,MTV.DESMTVCNC                   
                       ,CASE MTV.CODFNCDST WHEN MTV.CODFNCDST THEN 'DESATIVADO' ELSE 'ATIVADO' END STATUS                             
                       ,TRIM(FNC_ALT.NOMFNC) FNCALT
                       ,TO_CHAR(MTV.DATALT,'DD/MM/YYYY') DATFNCALT
                       ,CASE MTV.DATDST WHEN MTV.DATDST THEN TRIM(FNC_DST.NOMFNC) ELSE NULL END FNCDST
                       ,CASE MTV.DATDST WHEN MTV.DATDST THEN TO_CHAR(MTV.DATDST,'DD/MM/YYYY') ELSE NULL END DATFNCDST
                       ,MTV.INDCNCATI
                       ,CASE WHEN INDCNCATI = 1 THEN 'NEGOCIAÇÃO' ELSE 'GESTÃO DE CONTAS' END AS DESCTIPO                 
                 FROM MRT.CADMTVCNCSVCTGV MTV
                 LEFT JOIN MRT.T0100361 FNC_ALT ON FNC_ALT.CODFNC = MTV.CODFNCALT
                 LEFT JOIN MRT.T0100361 FNC_DST ON FNC_DST.CODFNC = MTV.CODFNCDST 
                WHERE 1 = 1
                  AND MTV.CODMTVCNC = :CODMTVCNC
                  AND MTV.INDCNCATI = :INDCNCATI
                ");

        if (flgAtivo == 1)
        {
            strBld.AppendLine(" AND MTV.DATDST IS NULL ");
        }
        if (flgAtivo == 2)
        {
            strBld.AppendLine(" AND MTV.DATDST IS NOT NULL ");
        }

        strBld.AppendLine("ORDER BY STATUS, MTV.CODMTVCNC");

        return strBld.ToString();
    }

    public string inserirCadastroMotivoCanc()
    {
        return @"
                 INSERT INTO MRT.CADMTVCNCSVCTGV (CODMTVCNC, DESMTVCNC, CODFNCALT, DATALT, INDCNCATI)
                 VALUES ((SELECT COALESCE(MAX(CODMTVCNC),0)+1 FROM MRT.CADMTVCNCSVCTGV),
                 UPPER(:DESMTVCNC), :CODFNCALT, SYSDATE, :INDCNCATI)
                ";
    }

    public string alterarCadastroMotivoCanc()
    {
        return @"
                 UPDATE MRT.CADMTVCNCSVCTGV SET
                 DESMTVCNC = UPPER(:DESMTVCNC),
                 CODFNCALT = :CODFNCALT,
                 DATALT = SYSDATE,
                 INDCNCATI = :INDCNCATI
                 WHERE CODMTVCNC = :CODMTVCNC
                ";
    }

    public string ativarCadastroMotivoCanc(List<long> LISTMTVCNC)
    {
        return @"
                 UPDATE MRT.CADMTVCNCSVCTGV SET                 
                 CODFNCDST = NULL,
                 DATDST = NULL
                 WHERE CODMTVCNC IN(#LISTMTVCNC)
                ".Replace("#LISTMTVCNC", Utilitario.ToListStr(LISTMTVCNC));
    }

    public string desativarCadastroMotivoCanc(List<long> LISTMTVCNC)
    {
        return @"
                 UPDATE MRT.CADMTVCNCSVCTGV SET
                 CODFNCDST = :CODFNCDST,
                 DATDST = SYSDATE
                 WHERE CODMTVCNC IN(#LISTMTVCNC)
                ".Replace("#LISTMTVCNC", Utilitario.ToListStr(LISTMTVCNC));
    }
}