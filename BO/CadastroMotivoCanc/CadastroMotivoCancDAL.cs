﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CadastroMotivoCancDAL
/// </summary>
public class CadastroMotivoCancDAL : DAL
{

    public List<CadastroMotivoCancTO.obterCadastroMotivoCanc> obterCadastroMotivoCanc(CadastroMotivoCancApiModel.CadastroMotivoCancApiModel objPesquisar)
    {
        var DALSQL = new CadastroMotivoCancDALSQL();
        string cmdSql = DALSQL.obterCadastroMotivoCanc(objPesquisar.MTVCNCATI, objPesquisar.CODMTVCNC);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("CODMTVCNC", objPesquisar.CODMTVCNC);
        dbCommand.AddWithValue("INDCNCATI", objPesquisar.INDCNCATI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroMotivoCancTO.obterCadastroMotivoCanc>();
    }

    public bool inserirCadastroMotivoCanc(CadastroMotivoCancApiModel.CadastroMotivoCancApiModel objInserir)
    {
        var DALSQL = new CadastroMotivoCancDALSQL();
        string cmdSql = DALSQL.inserirCadastroMotivoCanc();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        objInserir.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //        
        dbCommand.AddWithValue("DESMTVCNC", objInserir.DESMTVCNC);
        dbCommand.AddWithValue("CODFNCALT", objInserir.CODFNCALT);
        dbCommand.AddWithValue("INDCNCATI", objInserir.INDCNCATI);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool alterarCadastroMotivoCanc(CadastroMotivoCancApiModel.CadastroMotivoCancApiModel objAlterar)
    {
        var DALSQL = new CadastroMotivoCancDALSQL();
        string cmdSql = DALSQL.alterarCadastroMotivoCanc();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //              
        dbCommand.AddWithValue("CODMTVCNC", objAlterar.CODMTVCNC);
        dbCommand.AddWithValue("DESMTVCNC", objAlterar.DESMTVCNC);        
        dbCommand.AddWithValue("INDCNCATI", objAlterar.INDCNCATI);
        dbCommand.AddWithValue("CODFNCALT", objAlterar.CODFNCALT);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool ativarCadastroMotivoCanc(CadastroMotivoCancApiModel.CadastroMotivoCancApiModel objAlterar)
    {
        var DALSQL = new CadastroMotivoCancDALSQL();
        string cmdSql = DALSQL.ativarCadastroMotivoCanc(objAlterar.LISTMTVCNC);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool desativarCadastroMotivoCanc(CadastroMotivoCancApiModel.CadastroMotivoCancApiModel objAlterar)
    {
        var DALSQL = new CadastroMotivoCancDALSQL();
        string cmdSql = DALSQL.desativarCadastroMotivoCanc(objAlterar.LISTMTVCNC);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCDST = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //              
        dbCommand.AddWithValue("CODFNCDST", objAlterar.CODFNCDST);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }
}