﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CadastroMotivoCancBLL
{
    public List<CadastroMotivoCancTO.obterCadastroMotivoCanc> obterCadastroMotivoCanc(CadastroMotivoCancApiModel.CadastroMotivoCancApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroMotivoCancDAL();
            return DAL.obterCadastroMotivoCanc(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A PESQUISA DO MOTIVO Cancf.");
            Utilitario.InsereLog(ex, "obterCadastroMotivoCanc", "CadastroMotivoCancApiModel.CadastroMotivoCancApiModel", "CadastroMotivoCancBLL", "ERRO AO FAZER A PESQUISA DO MOTIVO Canc.");
            throw;
        }
    }

    public bool inserirCadastroMotivoCanc(CadastroMotivoCancApiModel.CadastroMotivoCancApiModel objInserir)
    {

        try
        {
            var DAL = new CadastroMotivoCancDAL();
            return DAL.inserirCadastroMotivoCanc(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO MOTIVO Canc.");
            Utilitario.InsereLog(ex, "inserirCadastroMotivoCanc", "CadastroMotivoCancApiModel.CadastroMotivoCancApiModel", "CadastroMotivoCancBLL", "ERRO AO FAZER A INSERCAO DO MOTIVO Canc.");
            throw;
        }
    }

    public bool alterarCadastroMotivoCanc(CadastroMotivoCancApiModel.CadastroMotivoCancApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroMotivoCancDAL();
            return DAL.alterarCadastroMotivoCanc(objAlterar);
        }

        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, " ERRO AO FAZER A ALTERACAO DO MOTIVO Canc.");
            Utilitario.InsereLog(ex, "alterarCadastroMotivoCanc", "CadastroMotivoCancApiModel.CadastroMotivoCancApiModel", "CadastroMotivoCancBLL", " ERRO AO FAZER A ALTERACAO DO MOTIVO Canc.");
            throw;
        }
    }

    public bool ativarCadastroMotivoCanc(CadastroMotivoCancApiModel.CadastroMotivoCancApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroMotivoCancDAL();
            return DAL.ativarCadastroMotivoCanc(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATIVACAO DO CADASTRO MOTIVO Canc.");
            Utilitario.InsereLog(ex, "ativarCadastroMotivoCanc", "CadastroMotivoCancApiModel.CadastroMotivoCancApiModel", "CadastroMotivoCancBLL", "ERRO AO FAZER A ATIVACAO DO CADASTRO MOTIVO Canc.");
            throw;
        }
    }

    public bool desativarCadastroMotivoCanc(CadastroMotivoCancApiModel.CadastroMotivoCancApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroMotivoCancDAL();
            return DAL.desativarCadastroMotivoCanc(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A DESATIVACAO DO CADASTRO MOTIVO Canc.");
            Utilitario.InsereLog(ex, "desativarCadastroMotivoCanc", "CadastroMotivoCancApiModel.CadastroMotivoCancApiModel", "CadastroMotivoCancBLL", "ERRO AO FAZER A DESATIVACAO DO CADASTRO MOTIVO Canc.");
            throw;
        }
    }
}