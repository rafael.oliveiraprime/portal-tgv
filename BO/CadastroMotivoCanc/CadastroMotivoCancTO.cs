﻿using System.Collections.Generic;

namespace CadastroMotivoCancTO
{
    public class obterCadastroMotivoCanc
    {
        public int CODMTVCNC { get; set; }

        public int CODMTVCNCCHECK { get; set; }

        public string DESMTVCNC { get; set; }

        public string FNCALT { get; set; }

        public string DATFNCALT { get; set; }

        public string FNCDST { get; set; }

        public string DATFNCDST { get; set; }

        public string STATUS { get; set; }

        public int INDCNCATI { get; set; }

        public string DESCTIPO { get; set; }
    }
}