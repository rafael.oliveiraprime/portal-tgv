﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CadastroConsultorBLL
{
    public List<CadastroConsultorTO.obterCadastroConsultor> obterCadastroConsultor(CadastroConsultorApiModel.CadastroConsultorApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroConsultorDAL();
            return DAL.obterCadastroConsultor(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO CONSULTOR.");
            Utilitario.InsereLog(ex, "obterCadastroConsultor", "CadastroConsultorApiModel.CadastroConsultorApiModel", "CadastroConsultorBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO CONSULTOR.");
            throw;
        }
    }

    public List<CadastroConsultorTO.obterConsultorVendasAtivo> obterConsultorVendasAtivo(CadastroConsultorApiModel.CadastroConsultorApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroConsultorDAL();
            return DAL.obterConsultorVendasAtivo(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO CONSULTOR ATIVO VENDEDOR.");
            Utilitario.InsereLog(ex, "obterConsultorVendasAtivo", "CadastroConsultorApiModel.CadastroConsultorApiModel", "CadastroConsultorBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO CONSULTOR ATIVO VENDEDOR.");
            throw;
        }
    }

    public bool inserirCadastroConsultor(CadastroConsultorApiModel.CadastroConsultorApiModel objInserir)
    {
        try
        {
            var DAL = new CadastroConsultorDAL();
            return DAL.inserirCadastroConsultor(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO CADASTRO CONSULTOR.");
            Utilitario.InsereLog(ex, "inserirCadastroConsultor", "CadastroConsultorApiModel.CadastroConsultorApiModel", "CadastroConsultorBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO CONSULTOR.");
            throw;
        }
    }

    public bool alterarCadastroConsultor(CadastroConsultorApiModel.CadastroConsultorApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroConsultorDAL();
            return DAL.alterarCadastroConsultor(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO CADASTRO CONSULTOR.");
            Utilitario.InsereLog(ex, "alterarCadastroConsultor", "CadastroConsultorApiModel.CadastroConsultorApiModel", "CadastroConsultorBLL", "ERRO AO FAZER A ALTERACAO DO CADASTRO CONSULTOR.");
            throw;
        }
    }

    public bool ativarCadastroConsultor(CadastroConsultorApiModel.CadastroConsultorApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroConsultorDAL();
            return DAL.ativarCadastroConsultor(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATIVACAO DO CADASTRO CONSULTOR.");
            Utilitario.InsereLog(ex, "ativarCadastroConsultor", "CadastroConsultorApiModel.CadastroConsultorApiModel", "CadastroConsultorBLL", "ERRO AO FAZER A ATIVACAO DO CADASTRO CONSULTOR.");
            throw;
        }
    }

    public string desativarCadastroConsultor(CadastroConsultorApiModel.CadastroConsultorApiModel objAlterar)
    {
        List<CadastroConsultorTO.obterConsultorVendasCobranca> _ConCob = new List<CadastroConsultorTO.obterConsultorVendasCobranca>();

        try
        {
            var DAL = new CadastroConsultorDAL();
            //_ConCob = DAL.VerificaConsultorCobranca(objAlterar);

            //if (_ConCob != null)
            //{
            //    foreach(CadastroConsultorTO.obterConsultorVendasCobranca _item in _ConCob)
            //    {
            //        if (_item.TOTAL > 0)
            //        {
            //            _result = _item.NOMFNC + ";" + _result;
            //        }
            //    }

            //    if (_result != "")
            //        return "2," + _result;

            //}

            if (DAL.desativarCadastroConsultor(objAlterar))
                DAL.AtaulizaResponsavel(objAlterar);


            return "1,0";

        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATIVACAO DO CADASTRO CONSULTOR.");
            Utilitario.InsereLog(ex, "desativarCadastroConsultor", "CadastroConsultorApiModel.CadastroConsultorApiModel", "CadastroConsultorBLL", "ERRO AO FAZER A ATIVACAO DO CADASTRO CONSULTOR.");
            throw;
        }
    }

    public int verificaFuncionario(int CODFNC)
    {
        try
        {
            var DAL = new CadastroConsultorDAL();
            return DAL.verificaFuncionario(CODFNC);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A VERIFICACAO DE CODFNC NO CADASTRO CONSULTOR.");
            Utilitario.InsereLog(ex, "VerificaFuncionario", "CODFNC", "CadastroConsultorBLL", "ERRO AO FAZER A VERIFICACAO DE CODFNC NO CADASTRO CONSULTOR.");
            throw;
        }
    }

    public List<CadastroConsultorTO.obterConsultorVendasAtivo> obterConsultorAtivo(CadastroConsultorApiModel.CadastroConsultorApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroConsultorDAL();
            return DAL.obterConsultorAtivo(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO CONSULTOR ATIVO.");
            Utilitario.InsereLog(ex, "obterConsultorVendasAtivo", "CadastroConsultorApiModel.CadastroConsultorApiModel", "CadastroConsultorBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO CONSULTOR ATIVO.");
            throw;
        }
    }

}