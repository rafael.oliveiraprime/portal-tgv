﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CadastroConsultorDAL : DAL
{
    public List<CadastroConsultorTO.obterCadastroConsultor> obterCadastroConsultor(CadastroConsultorApiModel.CadastroConsultorApiModel objPesquisar)
    {
        var DALSQL = new CadastroConsultorDALSQL();
        string cmdSql = DALSQL.obterCadastroConsultor(objPesquisar.CONATIFIL);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("CODFNCFIL", objPesquisar.CODFNCFIL);        
        dbCommand.AddWithValue("CODPFLACS", objPesquisar.CODPFLACS);

        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroConsultorTO.obterCadastroConsultor>();
    }

    public List<CadastroConsultorTO.obterConsultorVendasAtivo> obterConsultorVendasAtivo(CadastroConsultorApiModel.CadastroConsultorApiModel objPesquisar)
    {
        var DALSQL = new CadastroConsultorDALSQL();
        string cmdSql = DALSQL.obterConsultorVendasAtivo();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //               
        dbCommand.AddWithValue("CODFNC", objPesquisar.CODFNC);
        dbCommand.AddWithValue("NOMFNC", "%" + objPesquisar.NOMFNC + "%");
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroConsultorTO.obterConsultorVendasAtivo>();
    }

    public bool inserirCadastroConsultor(CadastroConsultorApiModel.CadastroConsultorApiModel objInserir)
    {
        var DALSQL = new CadastroConsultorDALSQL();
        string cmdSql = DALSQL.inserirCadastroConsultor();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objInserir.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        objInserir.CODFNCCAD = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //
        dbCommand.AddWithValue("CODFNC", objInserir.CODFNC);
        dbCommand.AddWithValue("CODPFLACS", objInserir.CODPFLACS);        
        dbCommand.AddWithValue("CODFNCCAD", objInserir.CODFNCCAD);
        dbCommand.AddWithValue("CODFNCALT", objInserir.CODFNCALT);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool alterarCadastroConsultor(CadastroConsultorApiModel.CadastroConsultorApiModel objAlterar)
    {
        var DALSQL = new CadastroConsultorDALSQL();
        string cmdSql = DALSQL.alterarCadastroConsultor();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //              
        dbCommand.AddWithValue("CODFNC", objAlterar.CODFNC);
        dbCommand.AddWithValue("CODPFLACS", objAlterar.CODPFLACS);        
        dbCommand.AddWithValue("CODFNCALT", objAlterar.CODFNCALT);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool ativarCadastroConsultor(CadastroConsultorApiModel.CadastroConsultorApiModel objAlterar)
    {
        var DALSQL = new CadastroConsultorDALSQL();
        string cmdSql = DALSQL.ativarCadastroConsultor(objAlterar.LISTCODFNC);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool desativarCadastroConsultor(CadastroConsultorApiModel.CadastroConsultorApiModel objAlterar)
    {
        var DALSQL = new CadastroConsultorDALSQL();
        string cmdSql = DALSQL.desativarCadastroConsultor(objAlterar.LISTCODFNC);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCDST = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //                     
        dbCommand.AddWithValue("CODFNCDST", objAlterar.CODFNCDST);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public int verificaFuncionario(int CODFNC)
    {
        var DALSQL = new CadastroConsultorDALSQL();
        string cmdSql = DALSQL.verificaFuncionario();
        //       
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        dbCommand.AddWithValue("CODFNC", CODFNC);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return Convert.ToInt32(MRT001.ExecuteScalar(dbCommand));
    }


    
    public List<CadastroConsultorTO.obterConsultorVendasCobranca> VerificaConsultorCobranca(CadastroConsultorApiModel.CadastroConsultorApiModel objAlterar)
    {
        var DALSQL = new CadastroConsultorDALSQL();
        string cmdSql = DALSQL.VerificaConsultorCobranca(objAlterar.LISTCODFNC);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroConsultorTO.obterConsultorVendasCobranca>();

    }

    public List<CadastroConsultorTO.obterConsultorVendasAtivo> obterConsultorAtivo(CadastroConsultorApiModel.CadastroConsultorApiModel objPesquisar)
    {
        var DALSQL = new CadastroConsultorDALSQL();
        string cmdSql = DALSQL.obterConsultorAtivo();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //               
        dbCommand.AddWithValue("CODFNC", objPesquisar.CODFNC);
        dbCommand.AddWithValue("NOMFNC", "%" + objPesquisar.NOMFNC + "%");
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroConsultorTO.obterConsultorVendasAtivo>();
    }

    public bool AtaulizaResponsavel(CadastroConsultorApiModel.CadastroConsultorApiModel objAlterar)
    {
        var DALSQL = new CadastroConsultorDALSQL();
        string cmdSql = DALSQL.AtaulizaResponsavel(objAlterar.LISTCODFNC);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }



}