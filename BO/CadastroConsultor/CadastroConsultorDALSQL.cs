﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class CadastroConsultorDALSQL
{
    public string obterCadastroConsultor(int flgAtivo)
    {
        StringBuilder strBld = new StringBuilder(@"
                 SELECT CON.CODFNC, CON.CODFNC CODFNCCHECK, FNC.NOMFNC, CON.CODPFLACS,
                 CASE WHEN CON.CODPFLACS = 1 THEN 'CONSULTOR VENDAS' 
                      WHEN CON.CODPFLACS = 2 THEN 'MONITOR VENDAS' 
                      WHEN CON.CODPFLACS = 3 THEN 'COBRANÇA' 
                      WHEN CON.CODPFLACS = 4 THEN 'ADMINISTRATIVO' END NOMCODPFLACS,
                 TRIM(FNC_CAD.NOMFNC) FNCCAD,
                 TO_CHAR(CON.DATCAD,'DD/MM/YYYY') DATFNCCAD,
                 TRIM(FNC_ALT.NOMFNC) FNCALT,
                 TO_CHAR(CON.DATALT,'DD/MM/YYYY') DATFNCALT,
                 CASE CON.DATDST WHEN CON.DATDST THEN TRIM(FNC_DST.NOMFNC) ELSE NULL END FNCDST,
                 CASE CON.DATDST WHEN CON.DATDST THEN TO_CHAR(CON.DATDST,'DD/MM/YYYY') ELSE NULL END DATFNCDST,
                 CASE CON.CODFNCDST WHEN CON.CODFNCDST THEN 'DESATIVADO' ELSE 'ATIVADO' END STATUS 
                 FROM MRT.CADFNCSISTGV CON
                 LEFT JOIN MRT.T0100361 FNC_CAD ON FNC_CAD.CODFNC = CON.CODFNCCAD
                 LEFT JOIN MRT.T0100361 FNC_ALT ON FNC_ALT.CODFNC = CON.CODFNCALT
                 LEFT JOIN MRT.T0100361 FNC_DST ON FNC_DST.CODFNC = CON.CODFNCDST 
                 LEFT JOIN MRT.T0100361 FNC ON FNC.CODFNC = CON.CODFNC 
                 WHERE 1 = 1
                 AND CON.CODFNC = :CODFNCFIL
                 AND CON.CODPFLACS = :CODPFLACS
                ");

        if (flgAtivo == 1)
        {
            strBld.AppendLine(" AND DATDST IS NULL ");

        }
        if (flgAtivo == 2)
        {
            strBld.AppendLine(" AND DATDST IS NOT NULL ");
        }

        strBld.AppendLine("ORDER BY STATUS, CON.CODFNC");

        return strBld.ToString();
    }

    public string obterConsultorVendasAtivo()
    {
        return @"
                 SELECT CON.CODFNC,
                 TRIM(FNC.NOMFNC) AS NOMFNC
                 FROM MRT.CADFNCSISTGV CON 
                 LEFT JOIN MRT.T0100361 FNC ON FNC.CODFNC = CON.CODFNC
                 WHERE DATDST IS NULL
                 AND CODPFLACS = 1
                 AND CON.CODFNC = :CODFNC
                 AND FNC.NOMFNC LIKE UPPER(:NOMFNC) 
                ";
    }

    public string inserirCadastroConsultor()
    {
        return @"
                 INSERT INTO MRT.CADFNCSISTGV (CODFNC, CODPFLACS, CODFNCCAD, DATCAD, CODFNCALT, DATALT)
                 VALUES (:CODFNC, :CODPFLACS, :CODFNCCAD, SYSDATE, :CODFNCALT, SYSDATE)
                ";
    }    

    public string alterarCadastroConsultor()
    {
        return @"
                 UPDATE MRT.CADFNCSISTGV SET
                 CODPFLACS = :CODPFLACS,   
                 CODFNCALT = :CODFNCALT,
                 DATALT = SYSDATE
                 WHERE CODFNC = :CODFNC
                ";
    }

    public string ativarCadastroConsultor(List<long> LISTCODFNC)
    {

        return @"
                 UPDATE MRT.CADFNCSISTGV SET                 
                 CODFNCDST = NULL,
                 DATDST = NULL
                 WHERE CODFNC IN(#LISTCODFNC)
                ".Replace("#LISTCODFNC", Utilitario.ToListStr(LISTCODFNC));
    }

    public string desativarCadastroConsultor(List<long> LISTCODFNC)
    {
        return @"
                 UPDATE MRT.CADFNCSISTGV SET                                 
                 CODFNCDST = :CODFNCDST,
                 DATDST = SYSDATE
                 WHERE CODFNC IN(#LISTCODFNC)
                ".Replace("#LISTCODFNC", Utilitario.ToListStr(LISTCODFNC));
    }

    public string verificaFuncionario()
    {
        return @"
                 SELECT COUNT(CODFNC) 
                 FROM MRT.CADFNCSISTGV 
                 WHERE CODFNC = :CODFNC
                ";

    }


    public string VerificaConsultorCobranca(List<long> LISTCODFNC)
    {
        return @"  SELECT CON.CODFNC
                          ,TRIM(FNC.NOMFNC) AS NOMFNC
                          ,COUNT(CT.CODRPN) AS TOTAL
                     FROM MRT.CADFNCSISTGV CON 
                     LEFT JOIN MRT.T0100361 FNC ON FNC.CODFNC = CON.CODFNC
                    INNER JOIN MRT.CADCLITGV CT ON CON.CODFNC = CT.CODRPN
                    WHERE CON.DATDST IS NULL
                      AND CON.CODFNC IN(#LISTCODFNC)
                    GROUP BY CON.CODFNC, FNC.NOMFNC
                    ".Replace("#LISTCODFNC", Utilitario.ToListStr(LISTCODFNC));
    }


    public string obterConsultorAtivo()
    {
        return @"
                 SELECT CON.CODFNC,
                 TRIM(FNC.NOMFNC) AS NOMFNC
                 FROM MRT.CADFNCSISTGV CON 
                 LEFT JOIN MRT.T0100361 FNC ON FNC.CODFNC = CON.CODFNC
                 WHERE DATDST IS NULL
                 AND CON.CODFNC = :CODFNC
                 AND FNC.NOMFNC LIKE UPPER(:NOMFNC) 
                ";
    }

    public string AtaulizaResponsavel(List<long> LISTCODFNC)
    {
        return @" UPDATE MRT.CADCLITGV SET CODRPN = NULL
                   WHERE CODRPN IN(#LISTCODFNC) ".Replace("#LISTCODFNC", Utilitario.ToListStr(LISTCODFNC));

    }




}