﻿using System.Collections.Generic;
using System;

namespace CadastroConsultorTO
{
    public class obterCadastroConsultor
    {        
        public int CODFNC { get; set; }

        public int CODFNCCHECK { get; set; }

        public string NOMFNC { get; set; }

        public int CODPFLACS { get; set; }

        public string NOMCODPFLACS { get; set; }

        public string FNCCAD { get; set; }

        public string DATFNCCAD { get; set; }

        public string FNCALT { get; set; }

        public string DATFNCALT { get; set; }

        public string FNCDST { get; set; }

        public string DATFNCDST { get; set; }

        public string STATUS { get; set; }
    }

    public class obterConsultorVendasAtivo
    {
        public int CODFNC { get; set; }

        public string NOMFNC { get; set; }        
    }

    public class obterConsultorVendasCobranca
    {
        public int CODFNC { get; set; }
        public string NOMFNC { get; set; }
        public Int64 TOTAL{ get; set; }

    }

}