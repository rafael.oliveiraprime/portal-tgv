﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class CadastroDocConciliacaoDALSQL
{
    public string obterCadastroDocConciliacao(int flgAtivo)
    {
        StringBuilder strBld = new StringBuilder(@"
                    SELECT CAD_DOC.CODDOCARZ AS CODDOCARZ,
                       CAD_DOC.DESDOCARZ AS DESDOCARZ,
                            CASE WHEN CAD_DOC.TIPDOC = 1 THEN 'DOC. BANDEIRA'
                                 WHEN CAD_DOC.TIPDOC = 2 THEN 'DOC. BANCÁRIA'
                            END AS TIPDOC,
                            CASE WHEN CAD_DOC.INDENV = 1 THEN 'SIM'
                                 WHEN CAD_DOC.INDENV = 0 THEN 'NÃO' 
                            END AS INDENV,                      
                       TRIM(SUBSTR(CAD_DOC.CDOANXDOCARZ, 1, INSTR(CAD_DOC.CDOANXDOCARZ, ';')-1)) AS CDOANXDOCARZ,
                       CAD_DOC.DATCAD DATCAD,
                       FNC_CAD.CODFNC AS CODFNCCAD,
                       FNC_CAD.NOMFNC AS NOMFNCCAD,
                       CAD_DOC.DATALT DATALT,
                       FNC_ALT.CODFNC AS CODFNCALT,
                       FNC_ALT.NOMFNC AS NOMFNCALT,
                       CAD_DOC.DATDST DATDST,
                       TO_CHAR(CAD_DOC.DATDST,'DD/MM/YYYY') DATDSTRES,
                            CASE WHEN CAD_DOC.CODFNCDST IS NOT NULL THEN CAD_DOC.CODFNCDST
                            END AS CODFNCDST,
                            CASE WHEN CAD_DOC.CODFNCDST IS NOT NULL THEN FNC_DES.NOMFNC
                            END AS NOMFNCDES
                    FROM MRT.CADDOCARZCLITGV CAD_DOC
                    LEFT JOIN MRT.T0100361 FNC_CAD ON FNC_CAD.CODFNC = CAD_DOC.CODFNCCAD
                    LEFT JOIN MRT.T0100361 FNC_ALT ON FNC_ALT.CODFNC = CAD_DOC.CODFNCALT
                    LEFT JOIN MRT.T0100361 FNC_DES ON FNC_DES.CODFNC = CAD_DOC.CODFNCDST
                    WHERE 1 = 1 
                    AND CAD_DOC.CODDOCARZ = :CODDOCARZ
                    AND CAD_DOC.TIPDOC = :TIPDOC
                    AND CAD_DOC.INDENV = :INDENV
                ");

        if (flgAtivo == 1)
        {    
                strBld.AppendLine(" AND DATDST IS NULL ");
        }
        if (flgAtivo == 2)
        {            
                strBld.AppendLine(" AND DATDST IS NOT NULL ");
        }

        strBld.AppendLine("ORDER BY CODDOCARZ");

        return strBld.ToString();
    }    

    public string inserirCadastroDocConciliacao()
    {
        return @"
                 INSERT INTO MRT.CADDOCARZCLITGV (CODDOCARZ, DESDOCARZ, INDENV, TIPDOC, CDOANXDOCARZ, CODFNCCAD, DATCAD, CODFNCALT, DATALT)
                 VALUES ((SELECT COALESCE(MAX(CODDOCARZ),0)+1 FROM MRT.CADDOCARZCLITGV),
                 UPPER(:DESDOCARZ),
                 :INDENV,
                 :TIPDOC,
                 :CDOANXDOCARZ,
                 :CODFNCCAD,
                 SYSDATE, 
                 :CODFNCALT,
                 SYSDATE)
                ";
    }

    public string alterarCadastroDocConciliacao()
    {
        return @"
                 UPDATE MRT.CADDOCARZCLITGV SET
                 DESDOCARZ = UPPER(:DESDOCARZ),
                 TIPDOC = :TIPDOC,
                 INDENV = :INDENV,
                 CODFNCALT = :CODFNCALT,                 
                 CDOANXDOCARZ = :CDOANXDOCARZ,
                 DATALT = SYSDATE
                 WHERE CODDOCARZ = :CODDOCARZ
                ";                  
    }

    public string ativarCadastroDocConciliacao(List<long> LISTCODDOC)
    {   
        
        return @"
                 UPDATE MRT.CADDOCARZCLITGV SET                 
                 CODFNCDST = NULL,
                 DATDST = NULL
                 WHERE CODDOCARZ IN(#LISTCODDOC)
                ".Replace("#LISTCODDOC", Utilitario.ToListStr(LISTCODDOC));            
    }

    public string desativarCadastroDocConciliacao(List<long> LISTCODDOC)
    {     
        return @"
                 UPDATE MRT.CADDOCARZCLITGV SET                                 
                 CODFNCDST = :CODFNCDST,
                 DATDST = SYSDATE
                 WHERE CODDOCARZ IN(#LISTCODDOC)
                ".Replace("#LISTCODDOC", Utilitario.ToListStr(LISTCODDOC));
    }


    public string VisualizarTemplate()
    {
        StringBuilder strBld = new StringBuilder(@" SELECT CODDOCARZ, NVL(CDOANXDOCARZ, ' ') AS CDOANXDOCARZ FROM MRT.CADDOCARZCLITGV WHERE CODDOCARZ = :CODDOCARZ ");

        return strBld.ToString();
    }


    public string obterDocRelacaoServico()
    {
        StringBuilder strBld = new StringBuilder(@" SELECT DOC.CODDOCARZ
                                                           ,DOC.DESDOCARZ 
                                                      FROM MRT.CADDOCARZCLITGV DOC 
                                                     INNER JOIN MRT.RLCSVCDOCTGV RL ON (DOC.CODDOCARZ = RL.CODDOCARZ) 
                                                     WHERE 1 = 1
                                                       AND DOC.INDENV = 1
                                                       AND RL.CODSVCTGV = :CODSVCTGV
                                                       ORDER BY DOC.DESDOCARZ
                                                ");

        return strBld.ToString();

    }


}