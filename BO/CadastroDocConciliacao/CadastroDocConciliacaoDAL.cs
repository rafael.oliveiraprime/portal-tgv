﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CadastroDocConciliacaoDAL : DAL
{
    public List<CadastroDocConciliacaoTO.obterCadastroDocConciliacao> obterCadastroDocConciliacao(CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel objPesquisar)
    {
        var DALSQL = new CadastroDocConciliacaoDALSQL();
        string cmdSql = DALSQL.obterCadastroDocConciliacao(objPesquisar.ATIVADO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODDOCARZ", objPesquisar.CODDOCARZ);
        dbCommand.AddWithValue("TIPDOC", objPesquisar.TIPDOC);
        dbCommand.AddWithValue("INDENV", objPesquisar.INDENV);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroDocConciliacaoTO.obterCadastroDocConciliacao>();
    }

    public bool inserirCadastroDocConciliacao(CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel objInserir)
    {
        var DALSQL = new CadastroDocConciliacaoDALSQL();
        string cmdSql = DALSQL.inserirCadastroDocConciliacao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objInserir.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        objInserir.CODFNCCAD = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //
        dbCommand.AddWithValue("DESDOCARZ", objInserir.DESDOCARZ);
        dbCommand.AddWithValue("INDENV", objInserir.INDENV);
        dbCommand.AddWithValue("TIPDOC", objInserir.TIPDOC);
        if (objInserir.CDOANXDOCARZ==null)
        {
            objInserir.CDOANXDOCARZ = " ";
        }
        dbCommand.AddWithValue("CDOANXDOCARZ", objInserir.CDOANXDOCARZ);
        dbCommand.AddWithValue("CODFNCCAD", objInserir.CODFNCCAD);  
        dbCommand.AddWithValue("CODFNCALT", objInserir.CODFNCALT);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool alterarCadastroDocConciliacao(CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel objAlterar)
    {
        var DALSQL = new CadastroDocConciliacaoDALSQL();
        string cmdSql = DALSQL.alterarCadastroDocConciliacao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //              
        dbCommand.AddWithValue("CODDOCARZ", objAlterar.CODDOCARZ); 
        dbCommand.AddWithValue("DESDOCARZ", objAlterar.DESDOCARZ);
        dbCommand.AddWithValue("TIPDOC", objAlterar.TIPDOC);
        dbCommand.AddWithValue("INDENV", objAlterar.INDENV);
        dbCommand.AddWithValue("CODFNCALT", objAlterar.CODFNCALT);
        dbCommand.AddWithValue("CDOANXDOCARZ", objAlterar.CDOANXDOCARZ);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool ativarCadastroDocConciliacao(CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel objAlterar)
    {
        var DALSQL = new CadastroDocConciliacaoDALSQL();
        string cmdSql = DALSQL.ativarCadastroDocConciliacao(objAlterar.LISTCODDOC);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool desativarCadastroDocConciliacao(CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel objAlterar)
    {
        var DALSQL = new CadastroDocConciliacaoDALSQL();
        string cmdSql = DALSQL.desativarCadastroDocConciliacao(objAlterar.LISTCODDOC);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCDST = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //                     
        dbCommand.AddWithValue("CODFNCDST", objAlterar.CODFNCDST);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public CadastroDocConciliacaoTO.obterCadastroDocConciliacaoTemplate VisualizarTemplate(CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel objPesquisar)
    {
        var DALSQL = new CadastroDocConciliacaoDALSQL();
        string cmdSql = DALSQL.VisualizarTemplate();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (objPesquisar.CODDOCARZ == 0)
        {
            objPesquisar.CODDOCARZ = -1;
        }
        dbCommand.AddWithValue("CODDOCARZ", objPesquisar.CODDOCARZ);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroDocConciliacaoTO.obterCadastroDocConciliacaoTemplate>().FirstOrDefault();
    }

    public List<CadastroDocConciliacaoTO.obterDocRelacaoServico> obterDocRelacaoServico(CadastroDocConciliacaoApiModel.obterDocRelacaoServicoApiModel objPesquisar)
    {
        var DALSQL = new CadastroDocConciliacaoDALSQL();
        string cmdSql = DALSQL.obterDocRelacaoServico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODSVCTGV", objPesquisar.CODSVCTGV);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroDocConciliacaoTO.obterDocRelacaoServico>();
    }

}