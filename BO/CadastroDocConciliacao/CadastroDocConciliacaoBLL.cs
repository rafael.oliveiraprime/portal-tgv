﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CadastroDocConciliacaoBLL
{
    public List<CadastroDocConciliacaoTO.obterCadastroDocConciliacao> obterCadastroDocConciliacao(CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroDocConciliacaoDAL();
            var a =  DAL.obterCadastroDocConciliacao(objPesquisar);
            return a;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO SERVIÇO.");
            Utilitario.InsereLog(ex, "obterCadastroDocConciliacao", "CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel", "CadastroDocConciliacaoBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO SERVIÇO.");
            throw;
        }
    }

    public bool inserirCadastroDocConciliacao(CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel objInserir)
    {
        try
        {
            
            var DAL = new CadastroDocConciliacaoDAL();
            return DAL.inserirCadastroDocConciliacao(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO CADASTRO SERVICO.");
            Utilitario.InsereLog(ex, "inserirCadastroDocConciliacao", "CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel", "CadastroDocConciliacaoBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO SERVICO.");
            throw;
        }
    }

    public bool alterarCadastroDocConciliacao(CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroDocConciliacaoDAL();
            return DAL.alterarCadastroDocConciliacao(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO CADASTRO SERVICO.");
            Utilitario.InsereLog(ex, "alterarCadastroDocConciliacao", "CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel", "CadastroDocConciliacaoBLL", "ERRO AO FAZER A ALTERACAO DO CADASTRO SERVICO.");
            throw;
        }
    }

    public bool ativarCadastroDocConciliacao(CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroDocConciliacaoDAL();
            return DAL.ativarCadastroDocConciliacao(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATIVACAO DO CADASTRO SERVICO.");
            Utilitario.InsereLog(ex, "ativarCadastroDocConciliacao", "CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel", "CadastroDocConciliacaoBLL", "ERRO AO FAZER A ATIVACAO DO CADASTRO SERVICO.");
            throw;
        }
    }

    public bool desativarCadastroDocConciliacao(CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroDocConciliacaoDAL();
            return DAL.desativarCadastroDocConciliacao(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATIVACAO DO CADASTRO SERVICO.");
            Utilitario.InsereLog(ex, "desativarCadastroDocConciliacao", "CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel", "CadastroDocConciliacaoBLL", "ERRO AO FAZER A DESATIVACAO DO CADASTRO SERVICO.");
            throw;
        }
    }

    public string VisualizarTemplate(CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel objPesquisar)
    {

        CadastroDocConciliacaoTO.obterCadastroDocConciliacaoTemplate _item = new CadastroDocConciliacaoTO.obterCadastroDocConciliacaoTemplate();
        string _arquivo = "";

        try
        {
            var DAL = new CadastroDocConciliacaoDAL();
            _item = DAL.VisualizarTemplate(objPesquisar);

            if (_item != null)
            {
                if (_item.CDOANXDOCARZ != null)
                    _arquivo = _item.CDOANXDOCARZ;
            }

            return _arquivo;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO VISUALIZAR O TEMPLATE DE CADASTRO SERVICO.");
            Utilitario.InsereLog(ex, "VisualizarTemplate", "CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel objPesquisarl", "CadastroDocConciliacaoBLL", "ERRO AO VISUALIZAR O TEMPLATE DE CADASTRO SERVICO.");
            throw;
        }
    }

}