﻿using System.Collections.Generic;

namespace CadastroDocConciliacaoTO
{
    public class obterCadastroDocConciliacao
    {
        public int CODDOCARZ { get; set; }

        public string DESDOCARZ { get; set; }

        public string TIPDOC { get; set; }

        public string INDENV { get; set; }

        public string CDOANXDOCARZ { get; set; }

        public string DATCAD { get; set; }

        public int CODFNCCAD { get; set; }

        public string DATALT { get; set; }

        public int CODFNCALT { get; set; }

        public string DATDST { get; set; }

        public int CODFNCDST { get; set; }

        public string NOMFNCCAD { get; set; }

        public string NOMFNCALT { get; set; }

        public string NOMFNCDES { get; set; }

        public string DATDSTRES { get; set; }

    }

    public class obterCadastroDocConciliacaoTemplate
    {
        public int CODDOCARZ { get; set; }

        public string CDOANXDOCARZ { get; set; }

    }


    public class obterDocRelacaoServico
    {

        public int CODDOCARZ { get; set; }

        public string DESDOCARZ { get; set; }

    }


}