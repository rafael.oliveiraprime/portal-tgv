﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class RelCobrancaDAL : DAL
{
    public List<RelPontuacaoTO.obterPontuacao> obterPontuacao(RelPontuacaoApiModel.obterPontuacaoApiModel objPesquisar)
    {
        var DALSQL = new RelPontuacaoDALSQL();
        string cmdSql = DALSQL.obterPontuacao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //    
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("NUMCGCCLI", Regex.Replace(objPesquisar.NUMCGCCLI, "[/.-]", String.Empty));
        dbCommand.AddWithValue("CODSVCTGV", objPesquisar.CODSVCTGV);
        dbCommand.AddWithValue("STATUS", objPesquisar.STATUS);
        dbCommand.AddWithValue("DATAINICIO", objPesquisar.DATAINICIO);
        dbCommand.AddWithValue("DATAFIM", objPesquisar.DATAFIM);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<RelPontuacaoTO.obterPontuacao>();
    }

    public List<RelPontuacaoTO.ObterUnica> ObterUnica(RelPontuacaoApiModel.ObterUnicaApiModel objPesquisar)
    {
        var DALSQL = new RelPontuacaoDALSQL();
        string cmdSql = DALSQL.ObterUnica();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //    
        dbCommand.AddWithValue("MESANO", objPesquisar.MESANO);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<RelPontuacaoTO.ObterUnica>();
    }


}