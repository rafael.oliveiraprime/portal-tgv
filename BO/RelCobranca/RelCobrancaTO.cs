﻿
namespace RelCobrancaTO
{
    public class obterPontuacao
    {
        public int CODOPTVND { get; set; }
        public int CODCLI { get; set; }
        public string NOMCLI { get; set; }
        public string NUMCGCCLI { get; set; }
        public int CODCNIVNDTGV { get; set; }
        public int CODSVCTGV { get; set; }
        public string DESSVCTGV { get; set; }
        public string NOMCON { get; set; }
        public string NOMINDSTANGC { get; set; }
        public string DATACEDOC { get; set; }
        public decimal VLRSVC { get; set; }
        public decimal VLRDSC { get; set; }
        public decimal PERDSC { get; set; }
        public decimal VLRTOTSVC { get; set; }
        public string NOMPESCTOPES { get; set; }
        public string NOMCNLORICLI { get; set; }
        public string DATFIMNGCOPT { get; set; }
    }
    
    public class ObterUnica
    {
        public string GF_CNPJ { get; set; }
        public string GF_CODIGO_MARTINS { get; set; }
        public string MES_ANO { get; set; }
        public string CNPJ { get; set; }
        public string SOLUCAO { get; set; }
        public string TOTAL { get; set; }
        public string DATA_ATIVACAO { get; set; }
        public string DATA_CANCELAMENTO { get; set; }
    }

}