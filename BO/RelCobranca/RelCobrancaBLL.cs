﻿using System;
using System.Collections.Generic;

public class RelcobrancaBLL
{
    public List<RelPontuacaoTO.obterPontuacao> obterPontuacao(RelPontuacaoApiModel.obterPontuacaoApiModel objPesquisar)
    {
        try
        {
            var DAL = new RelPontuacaoDAL();
            return DAL.obterPontuacao(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO RELATORIO DE PONTUACAO.");
            Utilitario.InsereLog(ex, "obterIndicacoes", "RelPontuacaoApiModel.obterPontuacaoApiModel", "RelPontuacaoBLL", "ERRO AO FAZER A CONSULTA DO RELATORIO DE PONTUACAO.");
            throw;
        }
    }

    public List<RelPontuacaoTO.ObterUnica> ObterUnica(RelPontuacaoApiModel.ObterUnicaApiModel objPesquisar)
    {
        try
        {
            var DAL = new RelPontuacaoDAL();

            if (objPesquisar.MESANO != null)
            {
                if (objPesquisar.MESANO.Trim() != "")
                    objPesquisar.MESANO = objPesquisar.MESANO.Replace(@"/","");
            }

            return DAL.ObterUnica(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO RELATORIO UNICA.");
            Utilitario.InsereLog(ex, "ObterUnica", "RelPontuacaoApiModel.ObterUnicaApiModel", "RelPontuacaoBLL", "ERRO AO FAZER A CONSULTA DO RELATORIO UNICA.");
            throw;
        }
    }


}