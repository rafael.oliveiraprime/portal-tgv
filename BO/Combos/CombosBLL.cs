﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CombosBLL
{
    public List<CombosTO.obterCodigosServicos> obterCodigosServicos(ServicosApiModel.ServicosApiModel objPesquisa)
    {


        try
        {
            var DAL = new CombosDAL();
            return DAL.obterCodigosServicos(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CODIGO DOS SERVICOS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterCodigosServicos", "ServicosApiModel.ServicosApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DO CODIGO DOS SERVICOS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterDescricoesServicos> obterDescricoesServicos(ServicosApiModel.ServicosApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesServicos(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DA DESCRICAO DOS SERVICOS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesServicos", "ServicosApiModel.ServicosApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DA DESCRICAO DOS SERVICOS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterDescricoesSistemas> obterDescricoesSistemas(SistemasApiModel.SistemasApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesSistemas(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DA DESCRICAO DOS SISTEMAS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesSistemas", "SistemasApiModel.SistemasApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DA DESCRICAO DOS SISTEMAS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterCodigosParcerias> obterCodigosParcerias(ParceriasApiModel.ParceriasApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterCodigosParcerias(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CODIGO DAS PARCERIAS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterCodigosParcerias", "ParceriasApiModel.ParceriasApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DO CODIGO DAS PARCERIAS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterDescricoesParcerias> obterDescricoesParcerias(ParceriasApiModel.ParceriasApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesParcerias(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DA DESCRICAO DAS PARCERIAS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesParcerias", "ParceriasApiModel.ParceriasApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DA DESCRICAO DAS PARCERIAS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterCodigosSegmentos> obterCodigosSegmentos(SegmentosApiModel.SegmentosApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterCodigosSegmentos(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CODIGO DOS SEGMENTOS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterCodigosSegmentos", "SegmentosApiModel.SegmentosApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DO CODIGO DOS SEGMENTOS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterDescricoesSegmentos> obterDescricoesSegmentos(SegmentosApiModel.SegmentosApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesSegmentos(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DA DESCRICAO DOS SEGMENTOS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesSegmentos", "SegmentosApiModel.SegmentosApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DA DESCRICAO DOS SEGMENTOS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterCodigosAutomacoes> obterCodigosAutomacoes(AutomacoesApiModel.AutomacoesApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterCodigosAutomacoes(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CODIGO DAS AUTOMACOES DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterCodigosAutomacoes", "AutomacoesApiModel.AutomacoesApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DO CODIGO DAS AUTOMACOES DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterDescricoesAutomacoes> obterDescricoesAutomacoes(AutomacoesApiModel.AutomacoesApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesAutomacoes(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DAS DESCRICOES DAS AUTOMACOES DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesAutomacoes", "AutomacoesApiModel.AutomacoesApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DAS DESCRICOES DAS AUTOMACOES DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterSistemasCodigosAutomacoes> obterSistemasCodigosAutomacoes(SistemasCodigosAutomacoesApiModel.SistemasCodigosAutomacoesApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterSistemasCodigosAutomacoes(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CODIGO DOS SISTEMA DE DETERMINADA AUTOMACAO.");
            Utilitario.InsereLog(ex, "obterSistemasCodigosAutomacoes", "SistemasCodigosAutomacoesApiModel.SistemasCodigosAutomacoesApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DO CODIGO DOS SISTEMA DE DETERMINADA AUTOMACAO.");
            throw;
        }
    }

    public List<CombosTO.obterCodigosMotivosContatos> obterCodigosMotivosContatos(MotivosContatosApiModel.MotivosContatosApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterCodigosMotivosContatos(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CODIGO DOS MOTIVOS CONTATO DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterCodigosMotivosContatos", "MotivosContatosApiModel.MotivosContatosApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DO CODIGO DOS MOTIVOS CONTATO DO COMPONENTE SELECT2.");
            throw;
        }
    }
    public List<CombosTO.obterDescricoesMotivosContatos> obterDescricoesMotivosContatos(MotivosContatosApiModel.MotivosContatosApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesMotivosContatos(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DAS DESCRICOES DOS MOTIVOS CONTATO DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesMotivosContatos", "MotivosContatosApiModel.MotivosContatosApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DAS DESCRICOES DOS MOTIVOS CONTATO DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterDescricoesMotivosContatos> obterDescricoesMotivosContatosNeg(MotivosContatosApiModel.MotivosContatosApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesMotivosContatosNeg(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DAS DESCRICOES DOS MOTIVOS CONTATO DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesMotivosContatos", "MotivosContatosApiModel.MotivosContatosApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DAS DESCRICOES DOS MOTIVOS CONTATO DO COMPONENTE SELECT2.");
            throw;
        }
    }

    //ObterDescricoesMotivosContatosNegEnviado


    public List<CombosTO.obterCodigosTiposContatos> obterCodigosTiposContatos(TiposContatosApiModel.TiposContatosApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterCodigosTiposContatos(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CODIGO DOS DEPARTAMENTOS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterCodigosTiposContatos", "TiposContatosApiModel.TiposContatosApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DO CODIGO DOS DEPARTAMENTOS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterDescricoesTiposContatos> obterDescricoesTiposContatos(TiposContatosApiModel.TiposContatosApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesTiposContatos(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DAS DESCRICOES DOS DEPARTAMENTOS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesTiposContatos", "TiposContatosApiModel.TiposContatosApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DAS DESCRICOES DOS DEPARTAMENTOS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterCodigosPessoasIndicacoes> obterCodigosPessoasIndicacoes(PessoasIndicacoesApiModel.PessoasIndicacoesApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterCodigosPessoasIndicacoes(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CODIGO DAS PESSOAS INDICACOES DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterCodigosPessoasIndicacoes", "PessoasIndicacoesApiModel.PessoasIndicacoesApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DO CODIGO DAS PESSOAS INDICACOES DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterDescricoesPessoasIndicacoes> obterDescricoesPessoasIndicacoes(PessoasIndicacoesApiModel.PessoasIndicacoesApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesPessoasIndicacoes(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DAS DESCRICOES DAS PESSOAS INDICACOES DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesPessoasIndicacoes", "PessoasIndicacoesApiModel.PessoasIndicacoesApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DAS DESCRICOES DAS PESSOAS INDICACOES DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterCodigosContatos> obterCodigosContatos(ContatosApiModel.ContatosApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterCodigosContatos(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CODIGO DOS CONTATOS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterCodigosContatos", "ContatosApiModel.ContatosApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DO CODIGO DOS CONTATOS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterDescricoesContatos> obterDescricoesContatos(ContatosApiModel.ContatosApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesContatos(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DAS DESCRICOES DOS CONTATOS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesContatos", "ContatosApiModel.ContatosApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DAS DESCRICOES DOS CONTATOS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterCodigosClientes> obterCodigosClientes(ClientesApiModel.ClientesApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterCodigosClientes(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CODIGO DOS CLIENTES DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterCodigosClientes", "ClientesApiModel.ClientesApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DO CODIGO DOS CLIENTES DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterDescricoesClientes> obterDescricoesClientes(ClientesApiModel.ClientesApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesClientes(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DAS DESCRICOES DOS CLIENTES DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesClientes", "ClientesApiModel.ClientesApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DAS DESCRICOES DOS CLIENTES DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterCodigosFuncionarios> obterCodigosFuncionarios(FuncionariosApiModel.FuncionariosApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterCodigosFuncionarios(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CODIGO DOS FUNCIONARIOS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterCodigosFuncionarios", "FuncionariosApiModel.FuncionariosApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DO CODIGO DOS FUNCIONARIOS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterDescricoesFuncionarios> obterDescricoesFuncionarios(FuncionariosApiModel.FuncionariosApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesFuncionarios(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DAS DESCRICOES DOS FUNCIONARIOS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterCodigosFuncionarios", "FuncionariosApiModel.FuncionariosApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DO CODIGO DOS FUNCIONARIOS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterCodigosRepresentantes> obterCodigosRepresentantes(RepresentantesApiModel.RepresentantesApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterCodigosRepresentantes(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CODIGO DOS REPRESENTANTES DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterCodigosRepresentantes", "RepresentantesApiModel.RepresentantesApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DO CODIGO DOS REPRESENTANTES DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterDescricoesRepresentantes> obterDescricoesRepresentantes(RepresentantesApiModel.RepresentantesApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesRepresentantes(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DAS DESCRICOES DOS REPRESENTANTES DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesRepresentantes", "RepresentantesApiModel.RepresentantesApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DAS DESCRICOES DOS REPRESENTANTES DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterDescricoesCondicoesComerciais> obterDescricoesCondicoesComerciais(CondicoesComerciasApiModel.CondicoesComerciasApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesCondicoesComerciais(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DAS DESCRICOES DAS CONDICOES COMERCIAIS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesCondicoesComerciais", "CondicoesComerciasApiModel.CondicoesComerciasApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DAS DESCRICOES DAS CONDICOES COMERCIAIS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterCondicoesComerciaisVerificacao> obterCondicoesComerciaisVerificacao(CondicoesComerciasApiModel.CondicoesComerciasApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterCondicoesComerciaisVerificacao(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DAS DESCRICOES DAS CONDICOES COMERCIAIS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterCondicoesComerciaisVerificacao", "CondicoesComerciasApiModel.CondicoesComerciasApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DAS DESCRICOES DAS CONDICOES COMERCIAIS COM VERIFICACAO DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.ObterCondicao> ObterCondicao(CondicaoApiModel.CondicaoApiModel objPesquisa)
    {


        try
        {
            var DAL = new CombosDAL();
            return DAL.ObterCondicao(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DAS CONDIÇÕES COMERCIAIS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "ObterCondicao", "CondicaoApiModel.CondicaoApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DAS CONDIÇÕES COMERCIAIS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterCodigosConsultores> obterCodigosConsultores(ConsultoresApiModel.ConsultoresApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterCodigosConsultores(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DOS CONSULTORES DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterCodigosConsultores", "ConsultoresApiModel.ConsultoresApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DOS CONSULTORES DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterDescricoesConsultores> obterDescricoesConsultores(ConsultoresApiModel.ConsultoresApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesConsultores(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DOS CONSULTORES DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesConsultores", "ConsultoresApiModel.ConsultoresApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DOS CONSULTORES DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterCodigosConsultores> obterCodigosConsultoresAtivosVend(ConsultoresApiModel.ConsultoresApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterCodigosConsultoresAtivosVend(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DOS CONSULTORES DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterCodigosConsultores", "ConsultoresApiModel.ConsultoresApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DOS CONSULTORES DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterDescricoesConsultores> obterDescricoesConsultoresAtivosVend(ConsultoresApiModel.ConsultoresApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesConsultoresAtivosVend(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DOS CONSULTORES DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesConsultores", "ConsultoresApiModel.ConsultoresApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DOS CONSULTORES DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterCodigosServicosCesta> obterCodigosServicosCesta(CestaServicosApiModel.CestaServicosApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterCodigosServicosCesta(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CODIGO DOS SERVICOS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterCodigosServicos", "ServicosApiModel.ServicosApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DO CODIGO DOS SERVICOS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterDescricoesServicosCesta> obterDescricoesServicosCesta(CestaServicosApiModel.CestaServicosApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesServicosCesta(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DA DESCRICAO DOS SERVICOS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesServicos", "ServicosApiModel.ServicosApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DA DESCRICAO DOS SERVICOS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterCodigosGrupoEconomico> obterCodigosGrupoEconomico(GrupoEconomicoApiModel.GrupoEconomicoApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterCodigosGrupoEconomico(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CODIGO DOS GRUPOS ECONOMICOS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterCodigosGrupoEconomico", "GrupoEconomicoApiModel.GrupoEconomicoApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DO CODIGO DOS GRUPOS ECONOMICOS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterDescricoesGrupoEconomico> obterDescricoesGrupoEconomico(GrupoEconomicoApiModel.GrupoEconomicoApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesGrupoEconomico(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO NOME DOS GRUPOS ECONOMICOS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesGrupoEconomico", "GrupoEconomicoApiModel.GrupoEconomicoApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DO NOME DOS GRUPOS ECONOMICOS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    
    public List<CombosTO.obterDescricoesMotivosCancelamentoGestao> obterDescricoesMotivosCancelamentoGestao(MotivosCancelamentoGestaoApiModel.MotivosCancelamentoGestaoApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesMotivosCancelamentoGestao(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DAS DESCRICOES DOS MOTIVOS DE CANCELAMENTO DE GESTÃO DE CONTAS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesMotivosCancelamentoGestao", "MotivosCancelamentoGestaoApiModel.MotivosCancelamentoGestaoApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DAS DESCRICOES DOS MOTIVOS DE CANCELAMENTO DE GESTÃO DE CONTAS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    
    public List<CombosTO.obterDescricoesMotivosCancelamentoGestao> obterDescricoesMotivosCancelamentoNegociacao(MotivosCancelamentoGestaoApiModel.MotivosCancelamentoGestaoApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesMotivosCancelamentoNegociacao(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DAS DESCRICOES DOS MOTIVOS DE CANCELAMENTO DE NEGOCIACAO DE CONTAS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesMotivosCancelamentoGestao", "MotivosCancelamentoGestaoApiModel.MotivosCancelamentoGestaoApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DAS DESCRICOES DOS MOTIVOS DE CANCELAMENTO DE NEGOCIACAO DE CONTAS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterDescricoesConsultores> obterDescricoesConsultoresAtivosCob(ConsultoresApiModel.ConsultoresApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesConsultoresAtivosCob(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DOS CONSULTORES DE COBRANÇA DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesConsultoresAtivosCob", "ConsultoresApiModel.ConsultoresApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DOS CONSULTORES DE COBRANÇA DO COMPONENTE SELECT2.");
            throw;
        }
    }


    public List<CombosTO.obterCodigosConsultores> obterCodigosConsultoresAtivosCob(ConsultoresApiModel.ConsultoresApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterCodigosConsultoresAtivosCob(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DOS CONSULTORES DE COBRANÇA DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterCodigosConsultoresAtivosCob", "ConsultoresApiModel.ConsultoresApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DOS CONSULTORES DE COBRANÇA DO COMPONENTE SELECT2.");
            throw;
        }
    }

    
    public List<CombosTO.obterDescricoesGrupoEconomico> ObterDescricoesGrupoEconomicoFat(GrupoEconomicoApiModel.GrupoEconomicoApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.ObterDescricoesGrupoEconomicoFat(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO NOME DOS GRUPOS ECONOMICOS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesGrupoEconomico", "GrupoEconomicoApiModel.GrupoEconomicoApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DO NOME DOS GRUPOS ECONOMICOS DO COMPONENTE SELECT2.");
            throw;
        }
    }


    public List<CombosTO.ObterDescricoesEmpresas> ObterDescricoesEmpresas(EmpresasApiModel.Empresas objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.ObterDescricoesEmpresas(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO NOME DAS EMPRESAS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "ObterDescricoesEmpresas", "EmpresasApiModel.Empresas", "CombosBLL", "ERRO AO FAZER A CONSULTA DO NOME DAS EMPRESAS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterDocConciliacao> obterDocConciliacao(DocConciliacaoApiModel.DocConciliacaoApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.ObterDocConciliacao(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO NOME DOS GRUPOS ECONOMICOS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDocConciliacao", "DocConciliacaoApiModel.DocConciliacaoApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DO NOME DOS GRUPOS ECONOMICOS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterDocConciliacao> obterDescricoesDocConciliacao(DocConciliacaoApiModel.DocConciliacaoApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesDocConciliacao(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO NOME DOS GRUPOS ECONOMICOS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesDocConciliacao", "DocConciliacaoApiModel.DocConciliacaoApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DO NOME DOS GRUPOS ECONOMICOS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterCadastroDocServicos> obterCadastroDocServicos(CadastroDocServicosApiModel.CadastroDocServicosApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterCadastroDocServicos(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO NOME DOS GRUPOS ECONOMICOS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDocConciliacao", "DocConciliacaoApiModel.DocConciliacaoApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DO NOME DOS GRUPOS ECONOMICOS DO COMPONENTE SELECT2.");
            throw;
        }
    }

    public List<CombosTO.obterCadastroDocServicos> obterDescricoesCadastroDocServicos(CadastroDocServicosApiModel.CadastroDocServicosApiModel objPesquisa)
    {
        try
        {
            var DAL = new CombosDAL();
            return DAL.obterDescricoesCadastroDocServicos(objPesquisa);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO NOME DOS GRUPOS ECONOMICOS DO COMPONENTE SELECT2.");
            Utilitario.InsereLog(ex, "obterDescricoesCadastroDocServicos", "CadastroDocServicosApiModel.CadastroDocServicosApiModel", "CombosBLL", "ERRO AO FAZER A CONSULTA DO NOME DOS GRUPOS ECONOMICOS DO COMPONENTE SELECT2.");
            throw;
        }
    }

}