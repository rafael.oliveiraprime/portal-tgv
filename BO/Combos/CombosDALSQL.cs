﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

//Consultas feitas nos nos dropdownlist "Select2".

public class CombosDALSQL
{
    public string obterCodigosServicos(int ativo)
    {
        StringBuilder strBld = new StringBuilder(@"         
                  SELECT CODSVCTGV,
                  TRIM(DESSVCTGV) DESSVCTGV  
                  FROM MRT.CADSVCTGV 
                  WHERE CODSVCTGV = :CODSVCTGV                 
                ");

        if (ativo == 1)
        {
            strBld.AppendLine("AND DATDST IS NULL");
        }

        strBld.AppendLine("ORDER BY DESSVCTGV");


        return strBld.ToString();
    }

    public string obterDescricoesServicos(int ativo)
    {
        StringBuilder strBld = new StringBuilder(@"         
                  SELECT CODSVCTGV,
                  TRIM(DESSVCTGV) DESSVCTGV  
                  FROM MRT.CADSVCTGV 
                  WHERE DESSVCTGV LIKE UPPER(:DESSVCTGV)                                
                ");

        if (ativo == 1)
        {
            strBld.AppendLine("AND DATDST IS NULL");
        }

        strBld.AppendLine("ORDER BY DESSVCTGV ");

        return strBld.ToString();
    }

    //Consulta especifica para o sistemas de uma determinada automação.
    public string obterSistemasCodigosAutomacoes()
    {
        return @"
                  SELECT CODSISINF, TRIM(NOMSIS) NOMSIS
                  FROM MRT.CADSISEMPAUTTGV 
                  WHERE 
                  CODEMPAUT = :CODEMPAUT AND
                  NOMSIS LIKE UPPER(:NOMSIS)                 
                  ORDER BY NOMSIS
                ";
    }

    public string obterCodigosParcerias()
    {
        return @"
                  SELECT CODPAC,
                  TRIM(NOMPAC) NOMPAC  
                  FROM MRT.CADTIPPACTGV 
                  WHERE CODPAC = :CODPAC
                  AND DATDST IS NULL
                  ORDER BY NOMPAC
                ";
    }

    public string obterDescricoesParcerias(int ativo)
    {
        StringBuilder strBld = new StringBuilder(@"         
                  SELECT CODPAC,
                  TRIM(NOMPAC) NOMPAC  
                  FROM MRT.CADTIPPACTGV 
                  WHERE NOMPAC LIKE UPPER(:NOMPAC)                  
                ");

        if (ativo == 1)
        {
            strBld.AppendLine("AND DATDST IS NULL");
        }

        strBld.AppendLine("ORDER BY NOMPAC");


        return strBld.ToString();
    }

    public string obterCodigosSegmentos()
    {
        return @"
                  SELECT CODSGM,
                  TRIM(DESSGM) DESSGM  
                  FROM MRT.CADSGMSISTGV  
                  WHERE CODSGM = :CODSGM
                  AND DATDST IS NULL
                  ORDER BY DESSGM
                ";
    }

    public string obterDescricoesSegmentos(int ativo)
    {
        StringBuilder strBld = new StringBuilder(@"         
                  SELECT CODSGM,
                  TRIM(DESSGM) DESSGM  
                  FROM MRT.CADSGMSISTGV 
                  WHERE DESSGM LIKE UPPER(:DESSGM)                 
                ");

        if (ativo == 1)
        {
            strBld.AppendLine("AND DATDST IS NULL");
        }

        strBld.AppendLine("ORDER BY DESSGM");


        return strBld.ToString();
    }

    public string obterCodigosAutomacoes()
    {
        return @"
                  SELECT CODEMPAUT,
                  TRIM(NOMFNT) NOMFNT  
                  FROM MRT.CADEMPAUTTGV 
                  WHERE CODEMPAUT = :CODEMPAUT
                  AND DATDST IS NULL
                  ORDER BY NOMFNT
                ";
    }

    public string obterDescricoesAutomacoes()
    {
        return @"
                  SELECT CODEMPAUT,
                  TRIM(NOMFNT) NOMFNT  
                  FROM MRT.CADEMPAUTTGV 
                  WHERE NOMFNT LIKE UPPER(:NOMFNT)
                  ORDER BY NOMFNT
                ";
    }

    public string obterDescricoesSistemas()
    {
        return @"
                  SELECT CODEMPAUT, TRIM(NOMSIS) NOMSIS
                  FROM MRT.CADSISEMPAUTTGV 
                  WHERE NOMSIS LIKE UPPER(:NOMSIS) 
                  ORDER BY NOMSIS
                ";
    }

    public string obterCodigosMotivosContatos(int ativo)
    {
        StringBuilder strBld = new StringBuilder(@"         
                  SELECT CODMTVCTO,
                  TRIM(DESMTVCTO) DESMTVCTO  
                  FROM MRT.CADMTVCTOTGV
                  WHERE CODMTVCTO = :CODMTVCTO
                  AND CODMTVCTO > 1           
                ");

        if (ativo == 1)
        {
            strBld.AppendLine("AND DATDST IS NULL");
        }

        strBld.AppendLine("ORDER BY DESMTVCTO");

        return strBld.ToString();
    }

    public string obterDescricoesMotivosContatos(int ativo)
    {
        StringBuilder strBld = new StringBuilder(@"         
                  SELECT CODMTVCTO,
                  TRIM(DESMTVCTO) DESMTVCTO  
                  FROM MRT.CADMTVCTOTGV                  
                  WHERE DESMTVCTO LIKE UPPER(:DESMTVCTO)
                ");

        if (ativo == 1)
        {
            strBld.AppendLine("AND DATDST IS NULL");
        }

        strBld.AppendLine("ORDER BY DESMTVCTO");

        return strBld.ToString();
    }

    public string obterDescricoesMotivosContatosNeg(int ativo)
    {
        StringBuilder strBld = new StringBuilder(@"         
                  SELECT CODMTVCTO,
                  TRIM(DESMTVCTO) DESMTVCTO  
                  FROM MRT.CADMTVCTOTGV                  
                  WHERE DESMTVCTO LIKE UPPER(:DESMTVCTO)
                  AND CODMTVCTO > 0 
                ");

        if (ativo == 1)
        {
            strBld.AppendLine("AND DATDST IS NULL");
        }

        strBld.AppendLine("ORDER BY DESMTVCTO");

        return strBld.ToString();
    }

    public string obterCodigosTiposContatos(int ativo)
    {
        StringBuilder strBld = new StringBuilder(@"         
                  SELECT CODTIPCTO,
                  TRIM(DESTIPCTO) DESTIPCTO  
                  FROM MRT.CADTIPCTOTGV		 
                  WHERE CODTIPCTO = :CODTIPCTO                 
                ");

        if (ativo == 1)
        {
            strBld.AppendLine("AND DATDST IS NULL");
        }

        strBld.AppendLine("ORDER BY DESTIPCTO");

        return strBld.ToString();
    }

    public string obterDescricoesTiposContatos(int ativo)
    {
        StringBuilder strBld = new StringBuilder(@"         
                  SELECT CODTIPCTO,
                  TRIM(DESTIPCTO) DESTIPCTO  
                  FROM MRT.CADTIPCTOTGV		                  
                  WHERE DESTIPCTO LIKE UPPER(:DESTIPCTO)                  
                ");

        if (ativo == 1)
        {
            strBld.AppendLine("AND DATDST IS NULL");
        }

        strBld.AppendLine("ORDER BY DESTIPCTO");

        return strBld.ToString();
    }

    public string obterCodigosPessoasIndicacoes(int ativo)
    {
        StringBuilder strBld = new StringBuilder(@"         
                  SELECT NUMSEQPESIND,
                  TRIM(NOMPESCTO) NOMPESCTO  
                  FROM MRT.CADDDOPESINDTGV
                  WHERE NUMSEQPESIND = :NUMSEQPESIND                
                ");

        if (ativo == 1)
        {
            strBld.AppendLine("AND DATDST IS NULL");
        }

        strBld.AppendLine("ORDER BY NOMPESCTO");

        return strBld.ToString();
    }

    public string obterDescricoesPessoasIndicacoes(int ativo)
    {
        StringBuilder strBld = new StringBuilder(@"         
                  SELECT NUMSEQPESIND,
                  TRIM(NOMPESCTO) NOMPESCTO  
                  FROM MRT.CADDDOPESINDTGV		                  
                  WHERE NOMPESCTO LIKE UPPER(:NOMPESCTO)              
                ");

        if (ativo == 1)
        {
            strBld.AppendLine("AND DATDST IS NULL");
        }

        strBld.AppendLine("ORDER BY NOMPESCTO");

        return strBld.ToString();
    }

    public string obterCodigosContatos()
    {
        return @"
                  SELECT CODCTO,
                  TRIM(NOMCTO) NOMCTO  
                  FROM MRT.CADCTOTGV 
                  WHERE CODCTO = :CODCTO
                  AND DATDST IS NULL
                  ORDER BY NOMCTO
                ";
    }

    public string obterDescricoesContatos()
    {
        return @"
                  SELECT CODCTO,
                  TRIM(NOMCTO) NOMCTO  
                  FROM MRT.CADCTOTGV		                  
                  WHERE NOMCTO LIKE UPPER(:NOMCTO)
                  ORDER BY NOMCTO
                ";
    }

    public string obterCodigosClientes()
    {
        return @"
                  SELECT CODCLI,
                  TRIM(NOMCLI) AS NOMCLI
                  FROM MRT.T0100043			                  
                  WHERE 1=1                   
                  AND CODCLI = :CODCLI
                  ORDER BY NOMCLI
                ";
    }

    public string obterDescricoesClientes()
    {
        return @"
                  SELECT CODCLI,
                  TRIM(NOMCLI) AS NOMCLI
                  FROM MRT.T0100043			                  
                  WHERE 1=1  
                  AND NOMCLI LIKE UPPER(:NOMCLI)
                  ORDER BY NOMCLI
                ";
    }

    public string obterCodigosFuncionarios()
    {
        return @"
                  SELECT CODFNC,
                  TRIM(NOMFNC) AS NOMFNC
                  FROM MRT.T0100361			                  
                  WHERE 1=1    
                  AND CODFNC = :CODFNC
                  ORDER BY NOMFNC
                ";
    }

    public string obterDescricoesFuncionarios()
    {
        return @"
                  SELECT CODFNC,
                  TRIM(NOMFNC) AS NOMFNC
                  FROM MRT.T0100361			                  
                  WHERE 1=1
                  AND NOMFNC LIKE UPPER(:NOMFNC) 
                  ORDER BY NOMFNC
                ";
    }

    public string obterCodigosRepresentantes()
    {
        return @"
                  SELECT CODREP,
                  TRIM(NOMREP) AS NOMREP
                  FROM MRT.T0100116			                  
                  WHERE 1=1   
                  AND CODREP = :CODREP
                  ORDER BY NOMREP
                ";
    }

    public string obterDescricoesRepresentantes()
    {
        return @"
                  SELECT CODREP,
                  TRIM(NOMREP) AS NOMREP
                  FROM MRT.T0100116			                  
                  WHERE 1=1
                  AND NOMREP LIKE UPPER(:NOMREP) 
                  ORDER BY NOMREP
                ";
    }

    public string obterDescricoesCondicoesComerciais(int ativo)
    {
        StringBuilder strBld = new StringBuilder(@"         
                  SELECT CODCNDCMC,
                         TRIM(DESCNDCMC) AS DESCNDCMC
                  FROM MRT.CADCNDCMCTGV			                  
                  WHERE 1=1
                  AND DESCNDCMC LIKE UPPER(:DESCNDCMC)                 
                ");

        if (ativo == 1)
        {
            strBld.AppendLine("AND DATDST IS NULL");
        }

        strBld.AppendLine("ORDER BY DESCNDCMC");

        return strBld.ToString();

    }

    public string obterCondicoesComerciaisVerificacao(int tipo)
    {
        if (tipo == 1)
        {
            return @"         
                                  SELECT  C.CODCNDCMC,    TRIM(C.DESCNDCMC) DESCNDCMC
                   , C.CODSVCTGV, c.codgrpempcli
                   , V.CODSVCTGV, G.codgrpempcli
                   , O.CODCLI
                FROM MRT.CADCNDCMCTGV C	
                LEFT JOIN MRT.CADOPTVNDCLITGV O ON O.CODOPTVND = :CODOPTVND
                LEFT JOIN MRT.RLCOPTVNDSVCTGV V ON v.codoptvnd = o.codoptvnd AND V.CODSVCTGV = C.CODSVCTGV
                LEFT JOIN MRT.RLCCLIGRPEMPTGV G ON G.CODGRPEMPCLI = C.CODGRPEMPCLI AND G.CODCLI = O.CODCLI
                WHERE TRUNC(SYSDATE) BETWEEN COALESCE(C.DATINIVLD,TRUNC(SYSDATE)) AND COALESCE(C.DATFIMVLD,TRUNC(SYSDATE))
                  AND C.DATDST IS NULL
                  AND C.DESCNDCMC LIKE UPPER(:DESCNDCMC)
                  AND (COALESCE(C.CODSVCTGV,0) = 0  OR C.CODSVCTGV = :CODSVCTGV)
                  AND (COALESCE(C.CODGRPEMPCLI,0) = 0 OR C.CODGRPEMPCLI = G.CODGRPEMPCLI)  
                  ORDER BY DESCNDCMC
                ";
        }
        else if (tipo == 2)
        {
            return @"         
                                  SELECT  C.CODCNDCMC,    TRIM(C.DESCNDCMC) DESCNDCMC
                   , C.CODSVCTGV, c.codgrpempcli
                   , V.CODSVCTGV, G.codgrpempcli
                   , O.CODCLI
                FROM MRT.CADCNDCMCTGV C	
                LEFT JOIN MRT.CADCLITGV O ON O.CODCLI = :CODCLI
                LEFT JOIN MRT.CADCLISVCTGV V ON v.CODCLI = o.CODCLI AND V.CODSVCTGV = C.CODSVCTGV
                LEFT JOIN MRT.RLCCLIGRPEMPTGV G ON G.CODGRPEMPCLI = C.CODGRPEMPCLI AND G.CODCLI = O.CODCLI
                WHERE TRUNC(SYSDATE) BETWEEN COALESCE(C.DATINIVLD,TRUNC(SYSDATE)) AND COALESCE(C.DATFIMVLD,TRUNC(SYSDATE))
                  AND C.DATDST IS NULL
                  AND C.DESCNDCMC LIKE UPPER(:DESCNDCMC)
                  AND (COALESCE(C.CODSVCTGV,0) = 0  OR C.CODSVCTGV = :CODSVCTGV)
                  AND (COALESCE(C.CODGRPEMPCLI,0) = 0 OR C.CODGRPEMPCLI = G.CODGRPEMPCLI)  
                  ORDER BY DESCNDCMC
                ";
        }
        else
        {
            return @"SELECT  C.CODCNDCMC,    TRIM(C.DESCNDCMC) DESCNDCMC
                   , C.CODSVCTGV, c.codgrpempcli
                   , V.CODSVCTGV, G.codgrpempcli
                   , O.CODCLI
                FROM MRT.CADCNDCMCTGV C
                LEFT JOIN MRT.CADOPTVNDCLITGV O ON O.CODOPTVND = :CODOPTVND
                LEFT JOIN MRT.RLCOPTVNDSVCTGV V ON v.codoptvnd = o.codoptvnd AND V.CODSVCTGV = C.CODSVCTGV
                LEFT JOIN MRT.RLCCLIGRPEMPTGV G ON G.CODGRPEMPCLI = C.CODGRPEMPCLI AND G.CODCLI = O.CODCLI
                WHERE TRUNC(SYSDATE) BETWEEN COALESCE(C.DATINIVLD, TRUNC(SYSDATE)) AND COALESCE(C.DATFIMVLD, TRUNC(SYSDATE))
                  AND C.DATDST IS NULL
                  AND C.DESCNDCMC LIKE UPPER(:DESCNDCMC)
                  AND(COALESCE(C.CODSVCTGV, 0) = 0  OR C.CODSVCTGV = :CODSVCTGV)
                  AND(COALESCE(C.CODGRPEMPCLI, 0) = 0 OR C.CODGRPEMPCLI = G.CODGRPEMPCLI)
                  ORDER BY DESCNDCMC";
        }
    }

    public string ObterCondicao(string DesCndCmc)
    {
        return @"
                  SELECT CODCNDCMC,
                  TRIM(DESCNDCMC) DESCNDCMC  
                  FROM MRT.CADCNDCMCTGV 
                  WHERE 1=1 
                  AND DESCNDCMC LIKE UPPER(:DESCNDCMC)
                  ORDER BY DESCNDCMC
                ";

    }

    public string obterCodigosConsultores(int ativo)
    {
        StringBuilder strBld = new StringBuilder(@"         
                  SELECT CON.CODFNC,
                  TRIM(FNC.NOMFNC) AS NOMFNC
                  FROM MRT.CADFNCSISTGV	CON	
                  LEFT JOIN MRT.T0100361 FNC ON FNC.CODFNC = CON.CODFNC
                  WHERE 1=1                  
                  AND CON.CODFNC = :CODFNC               
                ");

        if (ativo == 1)
        {
            strBld.AppendLine("AND DATDST IS NULL");
        }

        strBld.AppendLine("ORDER BY FNC.NOMFNC");

        return strBld.ToString();
    }


    public string obterDescricoesConsultores(int ativo)
    {
        StringBuilder strBld = new StringBuilder(@"         
                  SELECT CON.CODFNC,
                  TRIM(FNC.NOMFNC) AS NOMFNC
                  FROM MRT.CADFNCSISTGV CON	
                  LEFT JOIN MRT.T0100361 FNC ON FNC.CODFNC = CON.CODFNC
                  WHERE 1=1
                  AND FNC.NOMFNC LIKE UPPER(:NOMFNC)                 
                ");

        if (ativo == 1)
        {
            strBld.AppendLine("AND DATDST IS NULL");
        }

        strBld.AppendLine("ORDER BY FNC.NOMFNC");

        return strBld.ToString();
    }

    public string obterCodigosConsultoresAtivosVend(int ativo)
    {
        StringBuilder strBld = new StringBuilder(@"         
                  SELECT CON.CODFNC,
                  TRIM(FNC.NOMFNC) AS NOMFNC
                  FROM MRT.CADFNCSISTGV	CON	
                  LEFT JOIN MRT.T0100361 FNC ON FNC.CODFNC = CON.CODFNC
                  WHERE DATDST IS NULL
                  AND CODPFLACS = 1
                  AND CON.CODFNC = :CODFNC               
                ");

        if (ativo == 1)
        {
            strBld.AppendLine("AND DATDST IS NULL");
        }

        strBld.AppendLine("ORDER BY FNC.NOMFNC");

        return strBld.ToString();
    }


    public string obterDescricoesConsultoresAtivosVend(int ativo)
    {
        StringBuilder strBld = new StringBuilder(@"         
                  SELECT CON.CODFNC,
                  TRIM(FNC.NOMFNC) AS NOMFNC
                  FROM MRT.CADFNCSISTGV CON	
                  LEFT JOIN MRT.T0100361 FNC ON FNC.CODFNC = CON.CODFNC
                  WHERE DATDST IS NULL
                  --AND CODPFLACS = 1
                  AND FNC.NOMFNC LIKE UPPER(:NOMFNC)                 
                ");

        if (ativo == 1)
        {
            strBld.AppendLine("AND DATDST IS NULL");
        }

        strBld.AppendLine("ORDER BY FNC.NOMFNC");

        return strBld.ToString();
    }

    public string obterCodigosServicosCesta()
    {
        return @"
                    SELECT CODPRDCES,
                           TRIM(DESPRDCES) AS DESPRDCES   
                      FROM MRT.CADCESPRDTGV		 
                     WHERE CODPRDCES = :CODPRDCES 
                     AND DATDST IS NULL
                     ORDER BY DESPRDCES
                ";
    }

    public string obterDescricoesServicosCesta()
    {
        return @"
                    SELECT CODPRDCES,
                           TRIM(DESPRDCES) AS DESPRDCES   
                      FROM MRT.CADCESPRDTGV	 
                     WHERE DESPRDCES LIKE UPPER(:DESPRDCES)                   
                     ORDER BY DESPRDCES
                ";
    }

    public string obterCodigosGrupoEconomico()
    {
        return @"  
                  SELECT GP.CODGRPEMPCLI,
                  TRIM(GP.NOMGRPEMPCLI) AS NOMGRPEMPCLI
                  FROM MRT.CADGRPEMPTGV	GP	
                  WHERE 1=1
                  AND CON.CODGRPEMPCLI = :CODGRPEMPCLI 
                  ORDER BY GP.NOMGRPEMPCLI
                ";
    }


    public string obterDescricoesGrupoEconomico()
    {
        return @" 
                  SELECT CODGRPEMPCLI,
                  TRIM(NOMGRPEMPCLI) AS NOMGRPEMPCLI
                  FROM MRT.CADGRPEMPTGV			                  
                  WHERE 1=1
                  AND NOMGRPEMPCLI LIKE UPPER(:NOMGRPEMPCLI)   
                  ORDER BY NOMGRPEMPCLI
                ";
    }


    public string obterDescricoesMotivosCancelamentoGestao(int ativo)
    {
        StringBuilder strBld = new StringBuilder(@"         
                                                    SELECT CODMTVCNC,
                                                           TRIM(DESMTVCNC) AS DESMTVCNC  
                                                      FROM MRT.CADMTVCNCSVCTGV                                          
                                                     WHERE INDCNCATI = 2
                                                       AND DESMTVCNC LIKE UPPER(:DESMTVCNC)     
                                                 ");

        if (ativo == 1)
        {
            strBld.AppendLine("AND DATDST IS NULL");
        }

        strBld.AppendLine("ORDER BY DESMTVCNC");

        return strBld.ToString();
    }


    public string obterDescricoesMotivosCancelamentoNegociacao(int ativo)
    {
        StringBuilder strBld = new StringBuilder(@"         
                                                    SELECT CODMTVCNC,
                                                           TRIM(DESMTVCNC) AS DESMTVCNC  
                                                      FROM MRT.CADMTVCNCSVCTGV                  
                                                     WHERE INDCNCATI = 1
                                                       AND DESMTVCNC LIKE UPPER(:DESMTVCNC)     
                                                 ");

        if (ativo == 1)
        {
            strBld.AppendLine("AND DATDST IS NULL");
        }

        strBld.AppendLine("ORDER BY DESMTVCNC");

        return strBld.ToString();
    }

    public string obterDescricoesConsultoresAtivosCob(int ativo)
    {
        StringBuilder strBld = new StringBuilder(@"         
                  SELECT CON.CODFNC,
                  TRIM(FNC.NOMFNC) AS NOMFNC
                  FROM MRT.CADFNCSISTGV CON	
                  LEFT JOIN MRT.T0100361 FNC ON FNC.CODFNC = CON.CODFNC
                  WHERE DATDST IS NULL
                  AND CODPFLACS = 3
                  AND FNC.NOMFNC LIKE UPPER(:NOMFNC)                 
                ");

        if (ativo == 1)
        {
            strBld.AppendLine("AND DATDST IS NULL");
        }

        strBld.AppendLine("ORDER BY FNC.NOMFNC");

        return strBld.ToString();
    }

    public string obterCodigosConsultoresAtivosCob(int ativo)
    {
        StringBuilder strBld = new StringBuilder(@"         
                  SELECT CON.CODFNC,
                  TRIM(FNC.NOMFNC) AS NOMFNC
                  FROM MRT.CADFNCSISTGV	CON	
                  LEFT JOIN MRT.T0100361 FNC ON FNC.CODFNC = CON.CODFNC
                  WHERE DATDST IS NULL
                  AND CODPFLACS = 3
                  AND CON.CODFNC = :CODFNC               
                ");

        if (ativo == 1)
        {
            strBld.AppendLine("AND DATDST IS NULL");
        }

        strBld.AppendLine("ORDER BY FNC.NOMFNC");

        return strBld.ToString();
    }

    public string ObterDescricoesGrupoEconomicoFat()
    {
        return @" 
                  SELECT CODGRPEMPCLI,
                  TRIM(NOMGRPEMPCLI) AS NOMGRPEMPCLI
                  FROM MRT.CADGRPEMPTGV			                  
                  WHERE 1=1
                  AND NOMGRPEMPCLI LIKE UPPER(:NOMGRPEMPCLI)   
                  ORDER BY NOMGRPEMPCLI
                ";
    }


    public string ObterDescricoesEmpresas()
    {
        return @" 
                    SELECT CODFILEMP
                           ,(CODFILEMP || ' - ' || NOMFILEMP) NOMFILEMP    
                      FROM MRT.T0112963
                     WHERE 1 = 1 
                       AND NOMFILEMP LIKE '%' || UPPER(:NOMFILEMP) || '%'
                     ORDER BY NOMFILEMP
                ";
    }

    public string ObterDocConciliacao()
    {
        return @" 
                  SELECT CODDOCARZ,
                  TRIM(DESDOCARZ) AS DESDOCARZ
                  FROM MRT.CADDOCARZCLITGV			                  
                  WHERE 1=1
                  AND CODDOCARZ = :CODDOCARZ   
                  ORDER BY CODDOCARZ
                ";
    }

    public string obterDescricoesDocConciliacao()
    {
        return @" 
                  SELECT CODDOCARZ,
                  TRIM(DESDOCARZ) AS DESDOCARZ
                  FROM MRT.CADDOCARZCLITGV			                  
                  WHERE 1=1
                  AND DESDOCARZ LIKE UPPER(:DESDOCARZ)   
                  ORDER BY DESDOCARZ
                ";
    }

    public string obterCadastroDocServicos()
    {
        return @" 
                  SELECT CODDOCARZ,
                  TRIM(DESDOCARZ) AS DESDOCARZ
                  FROM MRT.CADDOCARZCLITGV
                  WHERE 1=1
                  AND CODDOCARZ = :CODDOCARZ
                  ORDER BY CODDOCARZ
                ";
    }

    public string obterDescricoesCadastroDocServicos()
    {
        return @" 
                  SELECT CODDOCARZ,
                  TRIM(DESDOCARZ) AS DESDOCARZ
                  FROM MRT.CADDOCARZCLITGV			                  
                  WHERE 1=1
                  AND DESDOCARZ LIKE UPPER(:DESDOCARZ)   
                  ORDER BY DESDOCARZ
                ";
    }
}
