﻿using System;

namespace CombosTO
{
    public class obterCodigosServicos
    {
        public int CODSVCTGV { get; set; }

        public string DESSVCTGV { get; set; }
    }

    public class obterDescricoesServicos
    {
        public int CODSVCTGV { get; set; }

        public string DESSVCTGV { get; set; }
    }

    public class obterSistemasCodigosAutomacoes
    {
        public int CODSISINF { get; set; }

        public string NOMSIS { get; set; }
    }

    public class obterCodigosParcerias
    {
        public int CODPAC { get; set; }

        public string NOMPAC { get; set; }
    }

    public class obterDescricoesParcerias
    {
        public int CODPAC { get; set; }

        public string NOMPAC { get; set; }
    }

    public class obterCodigosSegmentos
    {
        public int CODSGM { get; set; }

        public string DESSGM { get; set; }
    }

    public class obterDescricoesSegmentos
    {
        public int CODSGM { get; set; }

        public string DESSGM { get; set; }
    }

    public class obterCodigosAutomacoes
    {
        public int CODEMPAUT { get; set; }

        public string NOMFNT { get; set; }
    }

    public class obterDescricoesAutomacoes
    {
        public int CODEMPAUT { get; set; }

        public string NOMFNT { get; set; }
    }

    public class obterDescricoesSistemas
    {
        public int CODEMPAUT { get; set; }

        public string NOMSIS { get; set; }
    }

    public class obterCodigosMotivosContatos
    {
        public int CODMTVCTO { get; set; }

        public string DESMTVCTO { get; set; }
    }

    public class obterDescricoesMotivosContatos
    {
        public int CODMTVCTO { get; set; }

        public string DESMTVCTO { get; set; }
    }

    public class obterCodigosTiposContatos
    {
        public int CODTIPCTO { get; set; }

        public string DESTIPCTO { get; set; }
    }

    public class obterDescricoesTiposContatos
    {
        public int CODTIPCTO { get; set; }

        public string DESTIPCTO { get; set; }
    }

    public class obterCodigosPessoasIndicacoes
    {
        public Int64 NUMSEQPESIND { get; set; }

        public string NOMPESCTO { get; set; }
    }

    public class obterDescricoesPessoasIndicacoes
    {
        public Int64 NUMSEQPESIND { get; set; }

        public string NOMPESCTO { get; set; }
    }

    public class obterCodigosContatos
    {
        public int CODCTO { get; set; }

        public string NOMCTO { get; set; }
    }

    public class obterDescricoesContatos
    {
        public int CODCTO { get; set; }

        public string NOMCTO { get; set; }
    }

    public class obterCodigosClientes
    {
        public int CODCLI { get; set; }

        public string NOMCLI { get; set; }
    }

    public class obterDescricoesClientes
    {
        public int CODCLI { get; set; }

        public string NOMCLI { get; set; }
    }

    public class obterCodigosFuncionarios
    {
        public int CODFNC { get; set; }

        public string NOMFNC { get; set; }
    }

    public class obterDescricoesFuncionarios
    {
        public int CODFNC { get; set; }

        public string NOMFNC { get; set; }
    }

    public class obterCodigosRepresentantes
    {
        public int CODREP { get; set; }

        public string NOMREP { get; set; }
    }

    public class obterDescricoesRepresentantes
    {
        public int CODREP { get; set; }

        public string NOMREP { get; set; }
    }

    public class obterDescricoesCondicoesComerciais
    {
        public int CODCNDCMC { get; set; }

        public string DESCNDCMC { get; set; }        
    }

    public class obterCondicoesComerciaisVerificacao
    {
        public int CODCNDCMC { get; set; }

        public string DESCNDCMC { get; set; }
    }

    public class ObterCondicao
    {
        public int CODCNDCMC { get; set; }

        public string DESCNDCMC { get; set; }
    }

    public class obterCodigosConsultores
    {
        public int CODFNC { get; set; }

        public string NOMFNC { get; set; }
    }

    public class obterDescricoesConsultores
    {
        public int CODFNC { get; set; }

        public string NOMFNC { get; set; }
    }

    public class obterCodigosServicosCesta
    {
        public int CODPRDCES { get; set; }

        public string DESPRDCES { get; set; }
    }

    public class obterDescricoesServicosCesta
    {
        public int CODPRDCES { get; set; }

        public string DESPRDCES { get; set; }
    }

    public class obterCodigosGrupoEconomico
    {
        public int CODGRPEMPCLI { get; set; }

        public string NOMGRPEMPCLI { get; set; }
    }

    public class obterDescricoesGrupoEconomico
    {
        public int CODGRPEMPCLI { get; set; }

        public string NOMGRPEMPCLI { get; set; }
    }


    public class obterDescricoesMotivosCancelamentoGestao
    {
        public int CODMTVCNC { get; set; }

        public string DESMTVCNC { get; set; }
    }

    public class ObterDescricoesEmpresas
    {
        public int CODFILEMP { get; set; }

        public string NOMFILEMP { get; set; }
    }
    public class obterDocConciliacao
    {
        public int CODDOCARZ { get; set; }

        public string DESDOCARZ { get; set; }
    }

    public class obterCadastroDocServicos
    {
        public int CODDOCARZ { get; set; }
        public string DESDOCARZ { get; set; }
    }

}