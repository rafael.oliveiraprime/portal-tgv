﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CombosDAL : DAL
{
    public List<CombosTO.obterCodigosServicos> obterCodigosServicos(ServicosApiModel.ServicosApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterCodigosServicos(objPesquisa.ATIVO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("CODSVCTGV", objPesquisa.CODSVCTGV);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterCodigosServicos>();
    }

    public List<CombosTO.obterDescricoesServicos> obterDescricoesServicos(ServicosApiModel.ServicosApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesServicos(objPesquisa.ATIVO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("DESSVCTGV", "%" + objPesquisa.DESSVCTGV + "%");
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDescricoesServicos>();
    }

    public List<CombosTO.obterSistemasCodigosAutomacoes> obterSistemasCodigosAutomacoes(SistemasCodigosAutomacoesApiModel.SistemasCodigosAutomacoesApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterSistemasCodigosAutomacoes();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODEMPAUT", objPesquisa.CODEMPAUT);
        //
        dbCommand.AddWithValue("NOMSIS", "%" + objPesquisa.NOMSIS + "%");
        //                                  
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterSistemasCodigosAutomacoes>();
    }

    public List<CombosTO.obterCodigosParcerias> obterCodigosParcerias(ParceriasApiModel.ParceriasApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterCodigosParcerias();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODPAC", objPesquisa.CODPAC);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterCodigosParcerias>();
    }

    public List<CombosTO.obterDescricoesParcerias> obterDescricoesParcerias(ParceriasApiModel.ParceriasApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesParcerias(objPesquisa.ATIVO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("NOMPAC", "%" + objPesquisa.NOMPAC + "%");
        //       
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDescricoesParcerias>();
    }

    public List<CombosTO.obterCodigosSegmentos> obterCodigosSegmentos(SegmentosApiModel.SegmentosApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterCodigosSegmentos();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODSGM", objPesquisa.CODSGM);
        //                
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterCodigosSegmentos>();
    }

    public List<CombosTO.obterDescricoesSegmentos> obterDescricoesSegmentos(SegmentosApiModel.SegmentosApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesSegmentos(objPesquisa.ATIVO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("DESSGM", "%" + objPesquisa.DESSGM + "%");
        //                
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDescricoesSegmentos>();
    }

    public List<CombosTO.obterCodigosAutomacoes> obterCodigosAutomacoes(AutomacoesApiModel.AutomacoesApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterCodigosAutomacoes();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //  
        dbCommand.AddWithValue("CODEMPAUT", objPesquisa.CODEMPAUT);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterCodigosAutomacoes>();
    }

    public List<CombosTO.obterDescricoesAutomacoes> obterDescricoesAutomacoes(AutomacoesApiModel.AutomacoesApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesAutomacoes();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("NOMFNT", "%" + objPesquisa.NOMFNT + "%");
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDescricoesAutomacoes>();
    }

    public List<CombosTO.obterDescricoesSistemas> obterDescricoesSistemas(SistemasApiModel.SistemasApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesSistemas();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        dbCommand.AddWithValue("NOMSIS", "%" + objPesquisa.NOMSIS + "%");
        //       
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDescricoesSistemas>();
    }

    public List<CombosTO.obterCodigosMotivosContatos> obterCodigosMotivosContatos(MotivosContatosApiModel.MotivosContatosApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterCodigosMotivosContatos(objPesquisa.ATIVO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //  
        dbCommand.AddWithValue("CODMTVCTO", objPesquisa.CODMTVCTO);
        //       
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterCodigosMotivosContatos>();
    }

    public List<CombosTO.obterDescricoesMotivosContatos> obterDescricoesMotivosContatos(MotivosContatosApiModel.MotivosContatosApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesMotivosContatos(objPesquisa.ATIVO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("DESMTVCTO", "%" + objPesquisa.DESMTVCTO + "%");
        //       
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDescricoesMotivosContatos>();
    }

    public List<CombosTO.obterDescricoesMotivosContatos> obterDescricoesMotivosContatosNeg(MotivosContatosApiModel.MotivosContatosApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesMotivosContatosNeg(objPesquisa.ATIVO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("DESMTVCTO", "%" + objPesquisa.DESMTVCTO + "%");
        //       
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDescricoesMotivosContatos>();
    }

    public List<CombosTO.obterCodigosTiposContatos> obterCodigosTiposContatos(TiposContatosApiModel.TiposContatosApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterCodigosTiposContatos(objPesquisa.ATIVO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //  
        dbCommand.AddWithValue("CODTIPCTO", objPesquisa.CODTIPCTO);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterCodigosTiposContatos>();
    }

    public List<CombosTO.obterDescricoesTiposContatos> obterDescricoesTiposContatos(TiposContatosApiModel.TiposContatosApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesTiposContatos(objPesquisa.ATIVO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("DESTIPCTO", "%" + objPesquisa.DESTIPCTO + "%");
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDescricoesTiposContatos>();
    }

    public List<CombosTO.obterCodigosPessoasIndicacoes> obterCodigosPessoasIndicacoes(PessoasIndicacoesApiModel.PessoasIndicacoesApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterCodigosPessoasIndicacoes(objPesquisa.ATIVO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("NUMSEQPESIND", objPesquisa.NUMSEQPESIND);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterCodigosPessoasIndicacoes>();
    }

    public List<CombosTO.obterDescricoesPessoasIndicacoes> obterDescricoesPessoasIndicacoes(PessoasIndicacoesApiModel.PessoasIndicacoesApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesPessoasIndicacoes(objPesquisa.ATIVO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("NOMPESCTO", "%" + objPesquisa.NOMPESCTO + "%");
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDescricoesPessoasIndicacoes>();
    }

    public List<CombosTO.obterCodigosContatos> obterCodigosContatos(ContatosApiModel.ContatosApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterCodigosContatos();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //               
        dbCommand.AddWithValue("CODCTO", objPesquisa.CODCTO);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterCodigosContatos>();
    }

    public List<CombosTO.obterDescricoesContatos> obterDescricoesContatos(ContatosApiModel.ContatosApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesContatos();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("NOMCTO", "%" + objPesquisa.NOMCTO + "%");
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDescricoesContatos>();
    }

    public List<CombosTO.obterCodigosClientes> obterCodigosClientes(ClientesApiModel.ClientesApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterCodigosClientes();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        dbCommand.AddWithValue("CODCLI", objPesquisa.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterCodigosClientes>();
    }

    public List<CombosTO.obterDescricoesClientes> obterDescricoesClientes(ClientesApiModel.ClientesApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesClientes();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("NOMCLI", "%" + objPesquisa.NOMCLI + "%");
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDescricoesClientes>();
    }

    public List<CombosTO.obterCodigosFuncionarios> obterCodigosFuncionarios(FuncionariosApiModel.FuncionariosApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterCodigosFuncionarios();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //          
        dbCommand.AddWithValue("CODFNC", objPesquisa.CODFNC);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterCodigosFuncionarios>();
    }

    public List<CombosTO.obterDescricoesFuncionarios> obterDescricoesFuncionarios(FuncionariosApiModel.FuncionariosApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesFuncionarios();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("NOMFNC", "%" + objPesquisa.NOMFNC + "%");
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDescricoesFuncionarios>();
    }

    public List<CombosTO.obterCodigosRepresentantes> obterCodigosRepresentantes(RepresentantesApiModel.RepresentantesApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterCodigosRepresentantes();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        // 
        dbCommand.AddWithValue("CODREP", objPesquisa.CODREP);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterCodigosRepresentantes>();
    }

    public List<CombosTO.obterDescricoesRepresentantes> obterDescricoesRepresentantes(RepresentantesApiModel.RepresentantesApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesRepresentantes();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("NOMREP", "%" + objPesquisa.NOMREP + "%");
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDescricoesRepresentantes>();
    }

    public List<CombosTO.obterDescricoesCondicoesComerciais> obterDescricoesCondicoesComerciais(CondicoesComerciasApiModel.CondicoesComerciasApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesCondicoesComerciais(objPesquisa.ATIVO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("DESCNDCMC", "%" + objPesquisa.DESCNDCMC + "%");
        //      
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDescricoesCondicoesComerciais>();
    }

    public List<CombosTO.obterCondicoesComerciaisVerificacao> obterCondicoesComerciaisVerificacao(CondicoesComerciasApiModel.CondicoesComerciasApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterCondicoesComerciaisVerificacao(objPesquisa.TIPOCONSULTA);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("DESCNDCMC", "%" + objPesquisa.DESCNDCMC + "%");
        dbCommand.AddWithValue("CODSVCTGV", objPesquisa.CODSVCTGV);
        //
        if (objPesquisa.TIPOCONSULTA == 1)
        {
            dbCommand.AddWithValue("CODOPTVND", objPesquisa.CODOPTVND);
        }
        else if (objPesquisa.TIPOCONSULTA == 2)
        {
            dbCommand.AddWithValue("CODCLI", objPesquisa.CODCLI);
        }
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterCondicoesComerciaisVerificacao>();
    }

    public List<CombosTO.ObterCondicao> ObterCondicao(CondicaoApiModel.CondicaoApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.ObterCondicao(objPesquisa.DESCNDCMC);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (objPesquisa.CODCNDCMC == "-1")
        {
            objPesquisa.CODCNDCMC = null;
        }
        dbCommand.AddWithValue("CODCNDCMC", objPesquisa.CODCNDCMC);
        //
        dbCommand.AddWithValue("DESCNDCMC", "%" + objPesquisa.DESCNDCMC + "%");
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.ObterCondicao>();
    }

    public List<CombosTO.obterCodigosConsultores> obterCodigosConsultores(ConsultoresApiModel.ConsultoresApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterCodigosConsultores(objPesquisa.ATIVO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        // 
        dbCommand.AddWithValue("CODFNC", objPesquisa.CODFNC);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterCodigosConsultores>();
    }

    public List<CombosTO.obterDescricoesConsultores> obterDescricoesConsultores(ConsultoresApiModel.ConsultoresApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesConsultores(objPesquisa.ATIVO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("NOMFNC", "%" + objPesquisa.NOMFNC + "%");
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDescricoesConsultores>();
    }

    public List<CombosTO.obterCodigosConsultores> obterCodigosConsultoresAtivosVend(ConsultoresApiModel.ConsultoresApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterCodigosConsultoresAtivosVend(objPesquisa.ATIVO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        // 
        dbCommand.AddWithValue("CODFNC", objPesquisa.CODFNC);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterCodigosConsultores>();
    }

    public List<CombosTO.obterDescricoesConsultores> obterDescricoesConsultoresAtivosVend(ConsultoresApiModel.ConsultoresApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesConsultoresAtivosVend(objPesquisa.ATIVO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("NOMFNC", "%" + objPesquisa.NOMFNC + "%");
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDescricoesConsultores>();
    }

    public List<CombosTO.obterCodigosServicosCesta> obterCodigosServicosCesta(CestaServicosApiModel.CestaServicosApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterCodigosServicosCesta();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("CODPRDCES", objPesquisa.CODPRDCES);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterCodigosServicosCesta>();
    }

    public List<CombosTO.obterDescricoesServicosCesta> obterDescricoesServicosCesta(CestaServicosApiModel.CestaServicosApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesServicosCesta();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("DESPRDCES", "%" + objPesquisa.DESPRDCES + "%");
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDescricoesServicosCesta>();
    }

    public List<CombosTO.obterCodigosGrupoEconomico> obterCodigosGrupoEconomico(GrupoEconomicoApiModel.GrupoEconomicoApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterCodigosGrupoEconomico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("CODGRPEMPCLI", objPesquisa.CODGRPEMPCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterCodigosGrupoEconomico>();
    }

    public List<CombosTO.obterDescricoesGrupoEconomico> obterDescricoesGrupoEconomico(GrupoEconomicoApiModel.GrupoEconomicoApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesGrupoEconomico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("NOMGRPEMPCLI", "%" + objPesquisa.NOMGRPEMPCLI + "%");
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDescricoesGrupoEconomico>();
    }

    public List<CombosTO.obterDescricoesMotivosCancelamentoGestao> obterDescricoesMotivosCancelamentoGestao(MotivosCancelamentoGestaoApiModel.MotivosCancelamentoGestaoApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesMotivosCancelamentoGestao(objPesquisa.ATIVO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("DESMTVCNC", "%" + objPesquisa.DESMTVCNC + "%");
        //       
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDescricoesMotivosCancelamentoGestao>();
    }

    
    public List<CombosTO.obterDescricoesMotivosCancelamentoGestao> obterDescricoesMotivosCancelamentoNegociacao(MotivosCancelamentoGestaoApiModel.MotivosCancelamentoGestaoApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesMotivosCancelamentoNegociacao(objPesquisa.ATIVO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("DESMTVCNC", "%" + objPesquisa.DESMTVCNC + "%");
        //       
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDescricoesMotivosCancelamentoGestao>();
    }

    public List<CombosTO.obterDescricoesConsultores> obterDescricoesConsultoresAtivosCob(ConsultoresApiModel.ConsultoresApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesConsultoresAtivosCob(objPesquisa.ATIVO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("NOMFNC", "%" + objPesquisa.NOMFNC + "%");
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDescricoesConsultores>();
    }


    public List<CombosTO.obterCodigosConsultores> obterCodigosConsultoresAtivosCob(ConsultoresApiModel.ConsultoresApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterCodigosConsultoresAtivosCob(objPesquisa.ATIVO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        // 
        dbCommand.AddWithValue("CODFNC", objPesquisa.CODFNC);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterCodigosConsultores>();
    }

    public List<CombosTO.obterDescricoesGrupoEconomico> ObterDescricoesGrupoEconomicoFat(GrupoEconomicoApiModel.GrupoEconomicoApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.ObterDescricoesGrupoEconomicoFat();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("NOMGRPEMPCLI", "%" + objPesquisa.NOMGRPEMPCLI + "%");
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDescricoesGrupoEconomico>();
    }

    public List<CombosTO.ObterDescricoesEmpresas> ObterDescricoesEmpresas(EmpresasApiModel.Empresas objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.ObterDescricoesEmpresas();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("NOMFILEMP", objPesquisa.NOMFILEMP);
        //                                  
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.ObterDescricoesEmpresas>();
    }

    public List<CombosTO.obterDocConciliacao> ObterDocConciliacao(DocConciliacaoApiModel.DocConciliacaoApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.ObterDocConciliacao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODDOCARZ", objPesquisa.CODDOCARZ);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDocConciliacao>();
    }

    public List<CombosTO.obterDocConciliacao> obterDescricoesDocConciliacao(DocConciliacaoApiModel.DocConciliacaoApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesDocConciliacao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("DESDOCARZ", "%" + objPesquisa.DESDOCARZ + "%");
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterDocConciliacao>();
    }

    public List<CombosTO.obterCadastroDocServicos> obterCadastroDocServicos(CadastroDocServicosApiModel.CadastroDocServicosApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterCadastroDocServicos();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODDOCARZ", objPesquisa.CODDOCARZ);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterCadastroDocServicos>();
    }

    public List<CombosTO.obterCadastroDocServicos> obterDescricoesCadastroDocServicos(CadastroDocServicosApiModel.CadastroDocServicosApiModel objPesquisa)
    {
        var DALSQL = new CombosDALSQL();
        string cmdSql = DALSQL.obterDescricoesCadastroDocServicos();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("DESDOCARZ", "%" + objPesquisa.DESDOCARZ + "%");
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CombosTO.obterCadastroDocServicos>();
    }

}