﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

public class OportunidadeContatoDAL : DAL
{
    public List<OportunidadeContatoTO.OportunidadeContato> obterOportunidadeContato(int CODOPT)
    {
        var DALSQL = new OportunidadeContatoDALSQL();
        string cmdSql = DALSQL.obterOportunidadeContato();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODOPTVND", CODOPT);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<OportunidadeContatoTO.OportunidadeContato>();
    }


    public List<OportunidadeContatoTO.obterDadosBasicos> obterDadoBasicos(long CODCLI)
    {
        var DALSQL = new OportunidadeContatoDALSQL ();
        string cmdSql = DALSQL.obterDadosBasicos();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", CODCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<OportunidadeContatoTO.obterDadosBasicos>();
    }

    public List<OportunidadeContatoTO.obterServico> obterServicos(int CODOPT)
    {
        var DALSQL = new OportunidadeContatoDALSQL();
        string cmdSql = DALSQL.obterServicos();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODOPTVND", CODOPT);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<OportunidadeContatoTO.obterServico>();
    }

    public List<OportunidadeContatoTO.obterServico> obterServico(int CODOPT , int COSVC)
    {
        var DALSQL = new OportunidadeContatoDALSQL();
        string cmdSql = DALSQL.obterServico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODOPTVND", CODOPT);
        dbCommand.AddWithValue("CODSVCTGV", COSVC);        
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<OportunidadeContatoTO.obterServico>();
    }
}