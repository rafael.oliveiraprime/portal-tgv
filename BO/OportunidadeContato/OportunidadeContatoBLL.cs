﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class OportunidadeContatoBLL
{
    public List<OportunidadeContatoTO.OportunidadeContato> obterOportunidadeContato(int CODOPT)
    {
        try
        {
            var DAL = new OportunidadeContatoDAL();
            return DAL.obterOportunidadeContato(CODOPT);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO CONTATO.");
            Utilitario.InsereLog(ex, "obterCadastroContato", "CadastroContatoApiModel.CadastroContatoApiModel", "CadastroContatoBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO CONTATO.");
            throw;
        }
    }


    public List<OportunidadeContatoTO.obterDadosBasicos> obterDadosBasicos(int CODCLI)
    {
        try
        {
            var DAL = new OportunidadeContatoDAL();
            return DAL.obterDadoBasicos(CODCLI);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO CONTATO.");
            Utilitario.InsereLog(ex, "obterCadastroContato", "CadastroContatoApiModel.CadastroContatoApiModel", "CadastroContatoBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO CONTATO.");
            throw;
        }
    }

    public List<OportunidadeContatoTO.obterServico> obterServicos(int CODOPT)
    {
        try
        {
            var DAL = new OportunidadeContatoDAL();
            return DAL.obterServicos(CODOPT);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO CONTATO.");
            Utilitario.InsereLog(ex, "obterCadastroContato", "CadastroContatoApiModel.CadastroContatoApiModel", "CadastroContatoBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO CONTATO.");
            throw;
        }
    }

    public List<OportunidadeContatoTO.obterServico> obterServico(int CODOPT, int COSVC)
    {
        try
        {
            var DAL = new OportunidadeContatoDAL();
            return DAL.obterServico(CODOPT, COSVC);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO CONTATO.");
            Utilitario.InsereLog(ex, "obterCadastroContato", "CadastroContatoApiModel.CadastroContatoApiModel", "CadastroContatoBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO CONTATO.");
            throw;
        }
    }

}

