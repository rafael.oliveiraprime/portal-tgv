﻿using System.Collections.Generic;

namespace OportunidadeContatoTO
{
    public class OportunidadeContato
    {
        public int CODOPTVND { get; set; }

        public string DESENDETNCTO { get; set; }

        public string NOMCTO { get; set; }

        public string DESTIPCTO { get; set; }

    }

    public class obterDadosBasicos
    {
        public int CODCLI { get; set; }

        public string NOMCLI { get; set; }

        public string NUMCGCCLI { get; set; }
        
        public string ENDERECO { get; set; }

        public string COMPLEMENTO { get; set; }

        public string CODCEP { get; set; }

        public string NOMBAI { get; set; }

        public string NOMCID { get; set; }

        public string CODESTUNI { get; set; }

        public int CODGRPEMPCLI { get; set; }

        public string NOMFNTCLI { get; set; }

    }


    public class obterServico
    {
        public int CODOPTVND { get; set; }

        public int CODSVCTGV { get; set; }

        public string DESENDETN { get; set; }

        public string DATENVDOC { get; set; }

        public string DESSVCTGV { get; set; }

        public int INDSTANGC { get; set; }

        public string TEMPLATESERVICO { get; set; }

        public int INDSVCVND { get; set; }

        public string NOMARQORI { get; set; }

    }


}