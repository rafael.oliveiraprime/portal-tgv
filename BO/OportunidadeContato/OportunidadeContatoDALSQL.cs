﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class OportunidadeContatoDALSQL
{
    public string obterOportunidadeContato()
    {
        StringBuilder strBld = new StringBuilder(@"
                                                      SELECT DISTINCT OPT.CODOPTVND, 
                                                                      CTO.DESENDETNCTO,
                                                                      TRIM(TIP.DESTIPCTO) AS DESTIPCTO, 
                                                                      TRIM(CTO.NOMCTO) AS NOMCTO                  
                                                          FROM MRT.CADOPTVNDCLITGV  OPT
                                                         INNER JOIN MRT.CADCTOTGV CTO ON (OPT.CODCLI = CTO.CODEDE)
                                                         INNER JOIN MRT.CADTIPCTOTGV TIP ON TIP.CODTIPCTO = CTO.CODTIPCTO      
                                                         WHERE OPT.CODOPTVND = :CODOPTVND 
                                                           AND CTO.CODTIPCTO IN (1, 2)
                                                ");


        //SELECT DISTINCT OPT.CODOPTVND, 
        //                SOPT.CODSVCTGV,
        //                CTO.DESENDETNCTO
        //            FROM MRT.CADOPTVNDCLITGV OPT
        //            LEFT JOIN MRT.RLCOPTVNDSVCTGV SOPT ON(OPT.CODOPTVND = SOPT.CODOPTVND)
        //            LEFT JOIN MRT.CADCTOTGV CTO ON(OPT.CODCLI = CTO.CODEDE)
        //            WHERE OPT.CODOPTVND = :CODOPTVND
        //            AND CTO.CODTIPCTO = 2
        //            AND SOPT.DESENDETN IS NULL
        //            ORDER BY SOPT.CODSVCTGV



        return strBld.ToString();
    }


    public string obterDadosBasicos()
    {
        StringBuilder strBld = new StringBuilder(@"
                                                     SELECT C.CODCLI, 
                                                            C.NOMCLI, 
                                                            C.NUMCGCCLI 
                                                            ,TRIM(LGR.DesTipLgr) || ' ' || TRIM(END.DesLgr) || ', ' || END.NumEndLgr ENDERECO
                                                            ,END.DESCPLEND COMPLEMENTO
                                                            ,END.CODCEP
                                                            ,BAI.NOMBAIEXD NOMBAI
                                                            ,CID.NOMCIDEXD NOMCID 
                                                            ,CID.CodEstUni
                                                            ,GRP.CODGRPEMPCLI
                                                            ,NVL(C.NOMFNTCLI, ' ') AS NOMFNTCLI
                                                       FROM MRT.T0100043 C       
                                                       LEFT JOIN MRT.T0138938 END ON C.CodCli = END.Codcli
                                                       LEFT JOIN MRT.T0100027 BAI ON END.CodBai = BAI.Codbai
                                                       LEFT JOIN MRT.T0100035 CID ON BAI.CodCid = CID.Codcid
                                                       LEFT JOIN MRT.T0103905 CPL ON END.Codcplbai = CPL.CodCplBai
                                                       LEFT JOIN MRT.T0138946 LGR ON END.TipLgr = LGR.TipLgr
                                                       LEFT JOIN MRT.RLCCLIGRPEMPTGV GRP ON C.CODCLI = GRP.CODCLI
                                                      WHERE c.CodCli  = :CODCLI
                                                ");

        return strBld.ToString();
    }


    public string obterServicos()
    {
        StringBuilder strBld = new StringBuilder(@"
                                                     SELECT SER.CODOPTVND,
                                                            SER.CODSVCTGV,
                                                            SER.DESENDETN,
                                                            SER.DATENVDOC,
                                                            CSV.DESSVCTGV,
                                                            INDSTANGC,
                                                            CSV.CDOANXCTTCLI AS TEMPLATESERVICO,
                                                            SER.INDSVCVND,
                                                            SER.NOMARQORI
                                                       FROM MRT.RLCOPTVNDSVCTGV SER
                                                      INNER JOIN MRT.CADSVCTGV CSV ON(SER.CODSVCTGV = CSV.CODSVCTGV)   
                                                      WHERE CODOPTVND = :CODOPTVND
                                                        AND INDSTANGC IN (1, 2, 5, 6, 7)
                                                ");

        return strBld.ToString();
    }


    public string obterServico()
    {
        StringBuilder strBld = new StringBuilder(@"
                                                     SELECT SER.CODOPTVND,
                                                            SER.CODSVCTGV,
                                                            SER.DESENDETN,
                                                            SER.DATENVDOC,
                                                            CSV.DESSVCTGV,
                                                            INDSTANGC,
                                                            SER.CDOANXCTTCLI AS TEMPLATESERVICO,
                                                            SER.INDSVCVND,
                                                            SER.NOMARQORI
                                                       FROM MRT.RLCOPTVNDSVCTGV SER
                                                      INNER JOIN MRT.CADSVCTGV CSV ON(SER.CODSVCTGV = CSV.CODSVCTGV)   
                                                      WHERE SER.CODOPTVND = :CODOPTVND
                                                        AND SER.CODSVCTGV = :CODSVCTGV
                                                ");

        return strBld.ToString();
    }

}
