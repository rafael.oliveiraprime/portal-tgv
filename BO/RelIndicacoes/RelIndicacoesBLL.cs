﻿using System;
using System.Collections.Generic;

public class RelIndicacoesBLL
{
    public List<RelIndicacoesTO.obterIndicacoes> obterIndicacoes(RelIndicacoesApiModel.obterIndicacoesApiModel objPesquisar)
    {
        try
        {
            var DAL = new RelIndicacoesDAL();
            return DAL.obterIndicacoes(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO RELATORIO DE INDICACOES.");
            Utilitario.InsereLog(ex, "obterIndicacoes", "RelIndicacoesApiModel.obterIndicacoesApiModel", "RelIndicoesBLL", "ERRO AO FAZER A CONSULTA DO RELATORIO DE INDICACOES.");
            throw;
        }
    }
}