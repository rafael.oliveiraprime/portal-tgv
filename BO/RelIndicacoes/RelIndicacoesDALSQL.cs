﻿
public class RelIndicacoesDALSQL
{
    public string obterIndicacoes()
    {
        return @"
             WITH TABDPT AS ( SELECT CODDPT, NOMFNC
                            FROM MRT.T0100361 SUP 
                            WHERE SUP.codsec = 0 
                            AND SUP.DATDEMFNC IS NULL
                            AND SUP.TIPFNC = 1
                            AND SUP.CODDPT IS NOT NULL
                        )  
                     SELECT A.CODOPTVND
                        ,CASE WHEN  A.CODCNLORICLI=1 THEN 'SITE (PORTAL)' 
                              WHEN A.CODCNLORICLI=2 THEN 'TELEFONE' 
                              WHEN A.CODCNLORICLI=3 THEN 'E-MAIL' 
                              WHEN A.CODCNLORICLI=4 THEN 'WHATSAPP' 
                              WHEN A.CODCNLORICLI=5 THEN 'PARCERIA' 
                              WHEN A.CODCNLORICLI=6 THEN 'CAMPANHA' 
                              WHEN A.CODCNLORICLI=7 THEN 'OUTRO' 
                              WHEN A.CODCNLORICLI=8 THEN 'PROSPECT'
                              WHEN A.CODCNLORICLI=9 THEN 'INDICAÇÃO PRÓPRIA' 
                              WHEN A.CODCNLORICLI=10 THEN 'INDICAÇÃO GERENTE TRIBANCO' 
                              WHEN A.CODCNLORICLI=11 THEN 'INDICAÇÃO GERENTE MARTINS' 
                              WHEN A.CODCNLORICLI=12 THEN 'RCA' 
                              WHEN A.CODCNLORICLI=13 THEN 'EX-SMART' 
                              WHEN A.CODCNLORICLI=14 THEN 'INDICAÇÃO TRIBANCO - GERENCIADOR' 
                              WHEN A.CODCNLORICLI=15 THEN 'MIGRAÇÃO SERVIÇO' 
                              WHEN A.CODCNLORICLI=16 THEN 'INDICAÇÃO TGV - GERENCIADOR' 
                              WHEN A.CODCNLORICLI=17 THEN 'INDICAÇÃO PARCERIA' 
                              WHEN A.CODCNLORICLI=18 THEN 'IMPORT' 
                          END AS TIPOINDICACAO
                         --, DECODE(P.TIPFNCRPN,'M' , 'Funcionário Martins','R' ,'RCA autônomo','T','Gerente Tribanco', 'O' , 'Outro','') TIPOINDICACAO 
                         , I.CODFNC CODQUEMINDICOU, I.NOMFNC QUEMINDICOU, F.CODFNC CODCONSULTOR, F.NOMFNC CONSULTOR
                         , A.CODCLI, C.NOMCLI, A.NOMCTORPNCLI, A.DESENDETNRPNCLI
                         , P.DESENDETN
                         , CASE C.NUMCGCCLI WHEN C.NUMCGCCLI THEN (SUBSTR(C.NUMCGCCLI, 1, 2) || '.' || 
                            SUBSTR(C.NUMCGCCLI, 3, 3) || '.' ||
                            SUBSTR(C.NUMCGCCLI, 6, 3) || '/' || SUBSTR(C.NUMCGCCLI, 9,4) || '-' ||
                            SUBSTR(C.NUMCGCCLI, 13,2) ) ELSE NULL END NUMCGCCLI 
                         , DECODE(A.INDSTAOPTVND,1,'NEGOCIAÇÃO',2,'CONTRATO ENVIADO', 3,'VENDA GANHA', 4,'VENDA PERDIDA','') STATUSOP
                         , S.CODSVCTGV, T.DESSVCTGV, DECODE(S.INDSTANGC,1,'NEGOCIAÇÃO', 2, 'VENDA GANHA', 3, 'VENDA PERDIDA') STATUSSERVICO 
                         , TO_CHAR(S.DATENVDOC,'DD/MM/YYYY') DATENVDOC, TO_CHAR(S.DATACEDOC,'DD/MM/YYYY') DATACEDOC, S.VLRSVC, S.VLRDSC DESCONTOSERVICO
                         , D.CODCNDCMC, M.DESCNDCMC, D.PERDSC, D.VLRDSC DESCONTOCONDICAO
                         ,  TO_CHAR(D.DATINIVLD,'DD/MM/YYYY') DATINIVLD
                         ,  TO_CHAR(D.DATFIMVLD,'DD/MM/YYYY') DATFIMVLD
                         , DECODE(D.NUMMESINIVLD, 0, '', D.NUMMESINIVLD) NUMMESINIVLD, DECODE(D.NUMMESFIMVLD, 0, '', D.NUMMESFIMVLD) NUMMESFIMVLD
                         , CASE A.DATCAD WHEN A.DATCAD THEN TO_CHAR(A.DATCAD,'DD/MM/YYYY') ELSE NULL END DATCAD
                         ,(SELECT CASE WHEN CO.CODSITCLI = 1 THEN 'EM IMPLANTAÇÃO'
                                       WHEN CO.CODSITCLI = 2 THEN 'ATIVO'
                                       WHEN CO.CODSITCLI = 3 THEN 'SUSPENSO'
                                       WHEN CO.CODSITCLI = 4 THEN 'CANCELADO'  END STATUS from MRT.CADCLITGV CO where co.codcli = a.codcli) AS STATUSCONTA 
                        ,Decode(to_char(A.DATCAD,'d'),1,'Domingo',
                                        2,'Segunda-Feira',
                                        3,'Terça-Feira',
                                        4,'Quarta-Feira',
                                        5,'Quinta-Feira',
                                        6,'Sexta-Feira',
                                        7,'Sábado') AS DIA_SEMANA 
                        ,CASE WHEN LENGTH(TRIM(A.NUMTLFCTO)) =11 THEN ( '(' || SUBSTR(A.NUMTLFCTO, 1, 2) || ')' || SUBSTR(A.NUMTLFCTO, 3, 5) || '-' ||  SUBSTR(A.NUMTLFCTO, 8, 4)) WHEN LENGTH(TRIM(A.NUMTLFCTO)) =10 THEN '(' || SUBSTR(A.NUMTLFCTO, 1, 2) || ') ' || SUBSTR(A.NUMTLFCTO, 3, 4) || '-' || SUBSTR(A.NUMTLFCTO, 7, 4) END NUMTLFCTO
                        ,CRG.DESCGRFNC AS CARGO
                        --,(SELECT SUP.NOMFNC FROM MRT.T0100361 SUP WHERE SUP.CODDPT = I.CODDPT AND SUP.codsec = 0 AND SUP.DATDEMFNC IS NULL AND SUP.TIPFNC = 1) AS SUP_IMEDIATO
                       --, I.CODDPT
                       , X.NOMFNC SUP_IMEDIATO
                   FROM MRT.CADOPTVNDCLITGV A
                  INNER JOIN MRT.RLCOPTVNDSVCTGV S ON S.CODOPTVND = A.CODOPTVND
                   LEFT JOIN MRT.RLCOPTCNDCMCTGV D ON S.CODOPTVND = D.CODOPTVND AND S.CODSVCTGV = D.CODSVCTGV
                  INNER JOIN MRT.T0100043 C ON C.CODCLI = A.CODCLI
                  INNER JOIN MRT.CADSVCTGV T ON T.CODSVCTGV = S.CODSVCTGV
                   LEFT JOIN MRT.CADCNDCMCTGV M ON M.CODCNDCMC = D.CODCNDCMC
                  INNER JOIN MRT.T0100361 F ON F.CODFNC = A.CODCNIVNDTGV
                  INNER JOIN MRT.CADDDOPESINDTGV P ON P.NUMSEQPESIND = A.NUMSEQPESIND
                   LEFT JOIN MRT.T0100361 I ON I.CODFNC = P.CODFNC   
                   LEFT JOIN TABDPT X ON X.CODDPT = I.CODDPT
                   LEFT JOIN MRT.T0120931 CRG ON I.CODCGRFNC = CRG.CODCGRFNC
                  WHERE 0 = 0
                    AND A.CODCLI = :CODCLI 
                    AND C.NUMCGCCLI = :NUMCGCCLI 
                    AND S.CODSVCTGV = :CODSVCTGV 
                    AND A.NUMSEQPESIND = :NUMSEQPESIND 
                    AND A.INDSTAOPTVND = :STATUS 
                    AND TRUNC(A.DATCAD) BETWEEN TRUNC(TO_DATE(:DATCADINI,'DD/MM/YYYY')) AND TRUNC(TO_DATE(:DATCADFIM,'DD/MM/YYYY'))
                  ORDER BY  A.CODCLI, S.CODSVCTGV
                ";
    }

}