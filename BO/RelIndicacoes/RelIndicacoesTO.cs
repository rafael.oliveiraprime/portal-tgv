﻿
namespace RelIndicacoesTO
{
    public class obterIndicacoes
    {
        public string DATCAD { get; set; }
        public int CODOPTVND { get; set; }
        public string TIPOINDICACAO { get; set; }
        public string NUMCGCCLI { get; set; }
        public string NOMCTORPNCLI { get; set; }
        public string DESENDETNRPNCLI { get; set; }
        public string DESENDETN { get; set; }
        public int CODQUEMINDICOU { get; set; }
        public string QUEMINDICOU { get; set; }
        public int CODCONSULTOR { get; set; }
        public string CONSULTOR { get; set; }
        public int CODCLI { get; set; }
        public string NOMCLI { get; set; }
        public string STATUSOP { get; set; }
        public int CODSVCTGV { get; set; }
        public string DESSVCTGV { get; set; }
        public string STATUSSERVICO { get; set; }
        public string DATENVDOC { get; set; }
        public string DATACEDOC { get; set; }
        public decimal VLRSVC { get; set; }
        public decimal DESCONTOSERVICO { get; set; }
        public int CODCNDCMC { get; set; }
        public string DESCNDCMC { get; set; }
        public decimal PERDSC { get; set; }
        public decimal DESCONTOCONDICAO { get; set; }
        public string DATINIVLD { get; set; }
        public string DATFIMVLD { get; set; }
        public string NUMMESINIVLD { get; set; }
        public string NUMMESFIMVLD { get; set; }
        public string STATUSCONTA { get; set; }
        public string DIA_SEMANA { get; set; }
        public string NUMTLFCTO { get; set; }
        public string CARGO { get; set; }
        public string SUP_IMEDIATO { get; set; }
    }
}