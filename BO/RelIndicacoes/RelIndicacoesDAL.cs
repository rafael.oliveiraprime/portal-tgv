﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class RelIndicacoesDAL : DAL
{
    public List<RelIndicacoesTO.obterIndicacoes> obterIndicacoes(RelIndicacoesApiModel.obterIndicacoesApiModel objPesquisar)
    {
        var DALSQL = new RelIndicacoesDALSQL();
        string cmdSql = DALSQL.obterIndicacoes();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //    
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("NUMCGCCLI", Regex.Replace(objPesquisar.NUMCGCCLI, "[/.-]", String.Empty));
        dbCommand.AddWithValue("CODSVCTGV", objPesquisar.CODSVCTGV);
        dbCommand.AddWithValue("NUMSEQPESIND", objPesquisar.NUMSEQPESIND);
        dbCommand.AddWithValue("STATUS", objPesquisar.STATUS);
        dbCommand.AddWithValue("DATCADINI", objPesquisar.DATCADINI);
        dbCommand.AddWithValue("DATCADFIM", objPesquisar.DATCADFIM);
        
          
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<RelIndicacoesTO.obterIndicacoes>();
    }
}