﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class NegociacaoOportunidadeDALSQL
{
    public string obterNegociacaoOportunidade()
    {
        return @"
                 SELECT OPT.CODCLI, CLI.NOMCLI,
                 CASE CLI.NUMCGCCLI WHEN CLI.NUMCGCCLI THEN (SUBSTR(CLI.NUMCGCCLI, 1, 2) || '.' || 
                 SUBSTR(CLI.NUMCGCCLI, 3, 3) || '.' ||
                 SUBSTR(CLI.NUMCGCCLI, 6, 3) || '/' || SUBSTR(CLI.NUMCGCCLI, 9,4) || '-' ||
                 SUBSTR(CLI.NUMCGCCLI, 13,2) ) ELSE NULL END NUMCGCCLI, 
                 OPT.CODOPTVND, OPT.CODCNIVNDTGV,
                 OPT.NUMSEQPESIND, 
                 CASE WHEN INDSTAOPTVND =1 THEN 'EM NEGOCIAÇÃO' END STATUS,  
                 TRIM(FNC_CAD.NOMFNC) || ' em ' || OPT.DATCAD DATFNCCAD,
                 TRIM(CPI.NOMPESCTO) AS NOMPESCTOPES,                  
                 CASE WHEN CPI.TIPFNCRPN = 'M' THEN 'FUNCIONARIO MARTINS' WHEN CPI.TIPFNCRPN = 'R' THEN 'RCA AUTÔNOMO' WHEN CPI.TIPFNCRPN = 'T' THEN 'GERENTE TRIBANCO' WHEN CPI.TIPFNCRPN = 'O' THEN 'OUTROS' END TIPFNCRPNPES,
                 CPI.NUMSEQPESIND AS NUMSEQPESINDPES,
                 CASE WHEN CPI.CODFNC != 0 THEN CPI.CODFNC WHEN CPI.CODREP != 0 THEN CPI.CODREP ELSE NULL END MATRICULAPES,
                 CASE CPI.NUMCPF WHEN CPI.NUMCPF THEN (SUBSTR(CPI.NUMCPF, 1, 3) || '.' || SUBSTR(CPI.NUMCPF, 3, 3) || '.' ||
                 SUBSTR(CPI.NUMCPF, 6, 3) || '-' || SUBSTR(CPI.NUMCPF, 9,2)) ELSE NULL END NUMCPFPES,
                 TRIM(CPI.DESENDETN) AS DESENDETNPES,
                 CASE WHEN LENGTH(TRIM(CPI.NUMTLF)) =11 THEN ( '(' || SUBSTR(CPI.NUMTLF, 1, 2) || ')' || 
                 SUBSTR(CPI.NUMTLF, 3, 5) || '-' ||
                 SUBSTR(CPI.NUMTLF, 7, 4)) WHEN LENGTH(TRIM(CPI.NUMTLF)) =10 THEN 
                 '(' || SUBSTR(CPI.NUMTLF, 1, 2) || ') ' || 
                 SUBSTR(CPI.NUMTLF, 3, 4) || '-' ||
                 SUBSTR(CPI.NUMTLF, 6, 4)
                 END NUMTLFPES,
                 CASE WHEN LENGTH(TRIM(CPI.NUMTLFCEL)) =11 THEN ( '(' || SUBSTR(CPI.NUMTLFCEL, 1, 2) || ')' || 
                 SUBSTR(CPI.NUMTLFCEL, 3, 5) || '-' ||
                 SUBSTR(CPI.NUMTLFCEL, 7, 4)) WHEN LENGTH(TRIM(CPI.NUMTLFCEL)) =10 THEN 
                 '(' || SUBSTR(CPI.NUMTLFCEL, 1, 2) || ') ' || 
                 SUBSTR(CPI.NUMTLFCEL, 3, 4) || '-' ||
                 SUBSTR(CPI.NUMTLFCEL, 6, 4)
                 END NUMTLFCELPES
                 FROM MRT.CADOPTVNDCLITGV OPT
                 LEFT JOIN MRT.T0100361 FNC_CAD ON FNC_CAD.CODFNC = OPT.CODFNCCAD
                 LEFT JOIN MRT.CADDDOPESINDTGV CPI ON CPI.NUMSEQPESIND = OPT.NUMSEQPESIND
                 LEFT JOIN MRT.T0100043 CLI ON CLI.CODCLI = OPT.CODCLI
                 WHERE OPT.INDSTAOPTVND = 1
                 AND CLI.NUMCGCCLI = :NUMCGCCLI
                 AND OPT.CODOPTVND = :CODOPTVND
                 AND OPT.CODCLI = :CODCLI
                 AND OPT.CODCLI = :CODCLIFIL
                 AND OPT.NUMCGCCLI = :NUMCGCCLIFIL
                ";
    }    

    public string obterEstruturaBasica()
    {
        return @"
                 WITH TABESTRUTURA AS (
                 SELECT '1REP' TIPO FROM DUAL 
                 UNION SELECT '2GM' TIPO FROM DUAL
                 UNION SELECT '3GV' TIPO FROM DUAL)
                 SELECT T.TIPO ORDEM
                 , 'MARTINS - FV' EMPRESA
                 , CASE WHEN T.TIPO = '1REP' THEN 'REPRESENTANTE' 
                 WHEN T.TIPO = '2GM'  THEN 'GERENTE MERCADO' 
                 WHEN T.TIPO = '3GV' THEN 'GERENTE VENDAS' 
                 ELSE NULL 
                 END FUNCAO 
                 , CASE WHEN T.TIPO = '1REP' THEN RCA.CODREP
                 WHEN T.TIPO = '2GM'  THEN GM.CODSUP 
                 WHEN T.TIPO = '3GV' THEN GV.CODGER
                 ELSE NULL 
                 END MATRICULA 
                 , CASE WHEN T.TIPO = '1REP' THEN TRIM(RCA.NOMREP)
                 WHEN T.TIPO = '2GM'  THEN GM.NOMSUP
                 WHEN T.TIPO = '3GV' THEN GV.NOMGER
                 ELSE NULL 
                 END NOME
                 , CASE WHEN T.TIPO = '1REP' THEN '-' 
                 WHEN T.TIPO = '2GM'  THEN LOWER(GMS.DESUSRRDEMRT) || '@martins.com.br' 
                 WHEN T.TIPO = '3GV' THEN LOWER(GMS.DESUSRRDEMRT) || '@martins.com.br'
                 ELSE NULL 
                 END EMAIL      
                 , CASE WHEN T.TIPO = '1REP' THEN GM.NOMSUP  
                 WHEN T.TIPO = '2GM'  THEN GV.NOMGER
                 WHEN T.TIPO = '3GV' THEN '-'
                 ELSE NULL 
                 END SUPERIOR      
                 FROM TABESTRUTURA T
                 INNER JOIN mrt.t0100043 cli ON 1 = 1
                 INNER JOIN mrt.t0133774 tet ON cli.codcli = tet.codcli 
                 INNER JOIN mrt.t0133715 vnd ON tet.codtetvnd = vnd.codtetvnd 
                 INNER JOIN mrt.t0100116 rca ON vnd.codrep = rca.codrep and rca.CODUNDNGC = 1 
                 INNER JOIN mrt.t0100124 gm ON rca.codsup = gm.codsup 
                 INNER JOIN mrt.t0100051 gv ON gm.codger = gv.codger 
                 INNER JOIN MRT.CADUSRRDE GMS ON GMS.CODFNC = GM.CODFNCSUP
                 INNER JOIN MRT.CADUSRRDE GVS ON GVS.CODFNC = GV.CODFNCGER
                 INNER JOIN mrt.t0100116 REPGM ON REPGM.CODREP = GM.CODSUP
                 WHERE cli.codcli = :CODCLI
                 AND rca.datdstrep IS NULL 
                 AND gv.datdstger IS NULL 
                 AND vnd.datdsttetvnd IS NULL 
                 AND gm.datdstsup IS NULL                 
                 UNION
                 SELECT '4SMART' ORDEM  
                 , 'SMART' EMPRESA
                 , 'SUPERVISOR' FUNCAO
                 , F.CODFNC MATRICULA
                 , F.NOMFNC NOME
                 , R.DESUSRRDEMRT || '@martins.com.br' EMAIL                    
                 , S.NOMFNC SUPERIOR
                 FROM MRT.RLCCLIAREATDSUPSMA A
                 INNER JOIN MRT.T0100361 F ON F.CODFNC = A.CODFNCSUPRPN
                 INNER JOIN MRT.CADUSRRDE R ON R.CODFNC = F.CODFNC
                 INNER JOIN MRT.T0100361 S ON S.CODFNC = R.CODFNCRSP
                 WHERE A.CODCLI = :CODCLI
                 ORDER BY 1
                ";
    }

    public string obterDadosBasicos()
    {
        return @"
                 SELECT C.CODCLI, C.NOMCLI, C.NUMCGCCLI 
                 , C.TAMAREFISVNDCLI, C.TAMAREFISDPSCLI    
                 , TRIM(LGR.DesTipLgr) || ' ' || TRIM(END.DesLgr) || ', ' || END.NumEndLgr ENDERECO
                 , END.DESCPLEND COMPLEMENTO
                 , END.CODCEP
                 , BAI.NOMBAIEXD NOMBAI
                 , CID.NOMCIDEXD NOMCID 
                 , TRIM(CID.NOMCIDEXD) || ' - ' || TRIM(CID.CodEstUni) AS NOMCIDEST 
                 , CID.CodEstUni
                 , CNL.DESCNL CANAL -- INCLUIR NA TELA
                 , SGM.DESEQINGC SEGMENTOMERCADO -- INCLUIR NA TELA
                 , UND.DESUNDESRNGC UNIDADENEGOCIO -- INCLUIR NA TELA
                 , C.NUMFAXCLI NUMTEL 
                 , C.NUMTLFCELCLI NUMTELCEL 
                 FROM MRT.T0100043 C       
                 INNER JOIN MRT.T0138938 END ON C.CodCli = END.Codcli
                 INNER JOIN MRT.T0100027 BAI ON END.CodBai    = BAI.Codbai
                 INNER JOIN MRT.T0100035 CID ON BAI.CodCid    = CID.Codcid
                 LEFT  JOIN MRT.T0103905 CPL   -- Complemento Bairro
                 On END.Codcplbai = CPL.CodCplBai
                 LEFT  JOIN MRT.T0138946 LGR                 -- Tipo De Logradouro
                 On END.TipLgr    = LGR.TipLgr
                 INNER JOIN MRT.T0114850 CNL ON CNL.CODCNL = C.CODCNL
                 INNER JOIN MRT.T0114893 SGM ON SGM.CODEQINGC = CNL.CODEQINGC
                 INNER JOIN MRT.T0131275 UND ON UND.CODUNDESRNGC = SGM.CODUNDESRNGC
                 WHERE END.CodCli  = :CODCLI  
                 AND END.TipEndCli = :TIPENDCLI
                ";
    }

    public string inserirAnotacao()
    {
        return @"
                 INSERT INTO MRT.CADOBSCLITGV (CODCLI, NUMSEQOBS, TIPOBSCLI, CODMTVCTO, DESOBS, NUMNIVGARNGC, DATAGDCTO, CODFNCCAD, DATCAD)
                 VALUES (:CODCLI,
                 (SELECT COALESCE(MAX(NUMSEQOBS),0)+1 FROM MRT.CADOBSCLITGV),
                 :TIPOBSCLI, :CODMTVCTO, UPPER(:DESOBS), :NUMNIVGARNGC, :DATAGDCTO, :CODFNCCAD, 
                 SYSDATE)
                ";
    }    

    public string obterContatoClienteNegociacao()
    {
        return @"
                 WITH RESCONT AS (SELECT CTO.CODCTO, CTO.TIPEDE, CTO.CODEDE, CTO.CODTIPCTO, TRIM(TIP.DESTIPCTO) AS DESTIPCTO, 
                 TRIM(CTO.NOMCTO) AS NOMCTO, 
                 TRIM(CTO.DESENDETNCTO) AS DESENDETNCTO,
                 CASE WHEN LENGTH(TRIM(CTO.NUMTLFCEL)) =11 THEN ( '(' || SUBSTR(CTO.NUMTLFCEL, 1, 2) || ')' || SUBSTR(CTO.NUMTLFCEL, 3, 5) || '-' ||  SUBSTR(CTO.NUMTLFCEL, 8, 4)) WHEN LENGTH(TRIM(CTO.NUMTLFCEL)) =10 THEN '(' || SUBSTR(CTO.NUMTLFCEL, 1, 2) || ') ' || SUBSTR(CTO.NUMTLFCEL, 3, 4) || '-' || SUBSTR(CTO.NUMTLFCEL, 7, 4) END NUMTLFCEL,                 
                 CASE WHEN LENGTH(TRIM(CTO.NUMTLFCTO)) =11 THEN ( '(' || SUBSTR(CTO.NUMTLFCTO, 1, 2) || ')' || SUBSTR(CTO.NUMTLFCTO, 3, 5) || '-' ||  SUBSTR(CTO.NUMTLFCTO, 8, 4)) WHEN LENGTH(TRIM(CTO.NUMTLFCTO)) =10 THEN '(' || SUBSTR(CTO.NUMTLFCTO, 1, 2) || ') ' || SUBSTR(CTO.NUMTLFCTO, 3, 4) || '-' || SUBSTR(CTO.NUMTLFCTO, 7, 4) END NUMTLFCTO,                 
                 CASE WHEN LENGTH(TRIM(CTO.NUMTLFCTOSCD)) =11 THEN ( '(' || SUBSTR(CTO.NUMTLFCTOSCD, 1, 2) || ')' || SUBSTR(CTO.NUMTLFCTOSCD, 3, 5) || '-' ||  SUBSTR(CTO.NUMTLFCTOSCD, 8, 4)) WHEN LENGTH(TRIM(CTO.NUMTLFCTOSCD)) =10 THEN '(' || SUBSTR(CTO.NUMTLFCTOSCD, 1, 2) || ') ' || SUBSTR(CTO.NUMTLFCTOSCD, 3, 4) || '-' || SUBSTR(CTO.NUMTLFCTOSCD, 7, 4) END NUMTLFCTOSCD,                 
                 CASE WHEN LENGTH(TRIM(CTO.NUMTLFCTOTCR)) =11 THEN ( '(' || SUBSTR(CTO.NUMTLFCTOTCR, 1, 2) || ')' || SUBSTR(CTO.NUMTLFCTOTCR, 3, 5) || '-' ||  SUBSTR(CTO.NUMTLFCTOTCR, 8, 4)) WHEN LENGTH(TRIM(CTO.NUMTLFCTOTCR)) =10 THEN '(' || SUBSTR(CTO.NUMTLFCTOTCR, 1, 2) || ') ' || SUBSTR(CTO.NUMTLFCTOTCR, 3, 4) || '-' || SUBSTR(CTO.NUMTLFCTOTCR, 7, 4) END NUMTLFCTOTCR,                 
                 CASE WHEN LENGTH(TRIM(CTO.NUMTLFCTOQRT)) =11 THEN ( '(' || SUBSTR(CTO.NUMTLFCTOQRT, 1, 2) || ')' || SUBSTR(CTO.NUMTLFCTOQRT, 3, 5) || '-' ||  SUBSTR(CTO.NUMTLFCTOQRT, 8, 4)) WHEN LENGTH(TRIM(CTO.NUMTLFCTOQRT)) =10 THEN '(' || SUBSTR(CTO.NUMTLFCTOQRT, 1, 2) || ') ' || SUBSTR(CTO.NUMTLFCTOQRT, 3, 4) || '-' || SUBSTR(CTO.NUMTLFCTOQRT, 7, 4) END NUMTLFCTOQRT,                 
                 CASE WHEN LENGTH(TRIM(CTO.NUMTLFCTOQNT)) =11 THEN ( '(' || SUBSTR(CTO.NUMTLFCTOQNT, 1, 2) || ')' || SUBSTR(CTO.NUMTLFCTOQNT, 3, 5) || '-' ||  SUBSTR(CTO.NUMTLFCTOQNT, 8, 4)) WHEN LENGTH(TRIM(CTO.NUMTLFCTOQNT)) =10 THEN '(' || SUBSTR(CTO.NUMTLFCTOQNT, 1, 2) || ') ' || SUBSTR(CTO.NUMTLFCTOQNT, 3, 4) || '-' || SUBSTR(CTO.NUMTLFCTOQNT, 7, 4) END NUMTLFCTOQNT,                 
                 CASE CTO.CODFNCDST WHEN CTO.CODFNCDST THEN 'DESATIVADO' ELSE 'ATIVADO' END STATUS,                 
                 TRIM(FNC_CAD.NOMFNC) FNCCAD,
                 TO_CHAR(CTO.DATCAD,'DD/MM/YYYY') DATFNCCAD,
                 TRIM(FNC_ALT.NOMFNC) FNCALT,
                 TO_CHAR(CTO.DATALT,'DD/MM/YYYY') DATFNCALT,
                 CASE CTO.DATDST WHEN CTO.DATDST THEN TRIM(FNC_DST.NOMFNC) ELSE NULL END FNCDST,
                 CASE CTO.DATDST WHEN CTO.DATDST THEN TO_CHAR(CTO.DATDST,'DD/MM/YYYY') ELSE NULL END DATFNCDST
                 FROM MRT.CADCTOTGV CTO 
                 LEFT JOIN MRT.CADTIPCTOTGV TIP ON TIP.CODTIPCTO = CTO.CODTIPCTO
                 LEFT JOIN MRT.T0100361 FNC_CAD ON FNC_CAD.CODFNC = CTO.CODFNCCAD
                 LEFT JOIN MRT.T0100361 FNC_ALT ON FNC_ALT.CODFNC = CTO.CODFNCALT
                 LEFT JOIN MRT.T0100361 FNC_DST ON FNC_DST.CODFNC = CTO.CODFNCDST     
                 WHERE CTO.CODEDE = :CODEDE
                 UNION ALL           
                 SELECT DISTINCT 0 , 0 , A.CODCLI, 0 , 'CAD. MARTINS' DESTIPCTO , A.NOMCTOCLI, X.IDTENDRDEGLB, A.NUMTLFCELCLI, A.NUMTLFCLI, NULL,NULL,NULL,NULL, 'MARTINS', NULL,NULL,NULL,NULL,NULL,NULL
                 from MRT.T0100043 A
                 INNER JOIN MRT.t0127006 X ON A.CODCLI = X.CODCLI
                 WHERE A.CODCLI = :CODEDE
                 AND X.TIPRCSRDEGLB = 1
                 ORDER BY STATUS DESC, 2)
                 SELECT * FROM RESCONT RES
                 WHERE 1=1 
                 AND RES.CODTIPCTO = :CODTIPCTO";
    }

    public string obterServicosClientes()
    {
        return @"
                 SELECT SVCOPT.CODOPTVND, SVCOPT.CODSVCTGV,
                 SVCOPT.VLRSVC,
                 SVCOPT.VLRDSC,
                 SVC.DESSVCTGV, 
                 CASE WHEN SVC.TIPSVC = 1 THEN 'MENSAL' WHEN SVC.TIPSVC = 2 THEN 'RECORRENTE' WHEN SVC.TIPSVC = 3 THEN 'REMANEJAMENTO' END DESTIPSVC, 
                 SVCOPT.INDSTADSC, 
                 CASE WHEN SVCOPT.INDSTANGC = 2 THEN 'VENDA GANHA' WHEN SVCOPT.INDSTANGC = 3 THEN 'VENDA PERDIDA' END NOMINDSTANGC
                 FROM MRT.RLCOPTVNDSVCTGV SVCOPT
                 LEFT JOIN MRT.CADSVCTGV SVC ON SVC.CODSVCTGV = SVCOPT.CODSVCTGV
                 LEFT JOIN MRT.CADOPTVNDCLITGV OPT ON OPT.CODOPTVND = SVCOPT.CODOPTVND
                 WHERE OPT.CODCLI = :CODCLI
                 AND SVCOPT.INDSTANGC > 1
                ";
    }


    public string obterValoresCondicaoComercial()
    {
        return @"
                  SELECT (CASE WHEN VLRDSC = 0 THEN PERDSC ELSE VLRDSC END) AS VLRDSC,
                         (CASE WHEN VLRDSC = 0 THEN 'P' ELSE 'V' END) AS IDDESC,
                         NVL(CA.CODPRDCES, 0) AS CODPRDCES,
                         NVL(CES.DESPRDCES, ' ') AS DESPRDCES
                    FROM MRT.CADCNDCMCTGV CA
                    LEFT JOIN MRT.CADCESPRDTGV CES ON (CA.CODPRDCES = CES.CODPRDCES)
                   WHERE CODCNDCMC = :CODCNDCMC 
                ";
    }


}