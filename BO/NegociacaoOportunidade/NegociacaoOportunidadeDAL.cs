﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

public class NegociacaoOportunidadeDAL : DAL
{
    public List<NegociacaoOportunidadeTO.obterNegociacaoOportunidade> obterNegociacaoOportunidade(NegociacaoOportunidadeApiModel.NegociacaoOportunidadeApiModel objPesquisar)
    {
        var DALSQL = new NegociacaoOportunidadeDALSQL();
        string cmdSql = DALSQL.obterNegociacaoOportunidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (objPesquisar.CODOPTVND == 0)
        {
            objPesquisar.CODOPTVND = -1;
        }
        //
        if (objPesquisar.NUMCGCCLI == "-1")
        {
            objPesquisar.NUMCGCCLI = null;
        }
        string NUMCGCCLI;

        if (objPesquisar.NUMCGCCLI != null)
        {
            NUMCGCCLI = Regex.Replace(objPesquisar.NUMCGCCLI, "[/.-]", String.Empty);
        }
        else
        {
            NUMCGCCLI = null;
        }
        //
        if (objPesquisar.NUMCGCCLI == "-1")
        {
            objPesquisar.NUMCGCCLI = null;
        }
        string NUMCGCCLIFIL;

        if (objPesquisar.NUMCGCCLIFIL != null)
        {
            NUMCGCCLIFIL = Regex.Replace(objPesquisar.NUMCGCCLIFIL, "[/.-]", String.Empty);
        }
        else
        {
            NUMCGCCLIFIL = null;
        }
        //
        dbCommand.AddWithValue("CODOPTVND", objPesquisar.CODOPTVND);
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("NUMCGCCLI", NUMCGCCLI);
        dbCommand.AddWithValue("CODCLIFIL", objPesquisar.CODCLIFIL);
        dbCommand.AddWithValue("NUMCGCCLIFIL", NUMCGCCLIFIL);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<NegociacaoOportunidadeTO.obterNegociacaoOportunidade>();
    }

    public List<NegociacaoOportunidadeTO.obterEstruturaBasica> obterEstruturaBasica(NegociacaoOportunidadeApiModel.NegociacaoOportunidadeApiModel objPesquisar)
    {
        var DALSQL = new NegociacaoOportunidadeDALSQL();
        string cmdSql = DALSQL.obterEstruturaBasica();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //      
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<NegociacaoOportunidadeTO.obterEstruturaBasica>();
    }   

    public NegociacaoOportunidadeTO.obterDadosBasicos obterDadosBasicos(NegociacaoOportunidadeApiModel.NegociacaoOportunidadeApiModel objPesquisar)
    {
        var DALSQL = new NegociacaoOportunidadeDALSQL();
        string cmdSql = DALSQL.obterDadosBasicos();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("TIPENDCLI", objPesquisar.TIPENDCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        var retorno = MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<NegociacaoOportunidadeTO.obterDadosBasicos>();
        if (retorno.Count == 1)
        {
            return retorno.FirstOrDefault();
        }
        else
        {
            return new NegociacaoOportunidadeTO.obterDadosBasicos();
        }
    }

    public List<NegociacaoOportunidadeTO.obterContatoClienteNegociacao> obterContatoClienteNegociacao(NegociacaoOportunidadeApiModel.NegociacaoOportunidadeApiModel objPesquisar)
    {
        var DALSQL = new NegociacaoOportunidadeDALSQL();
        string cmdSql = DALSQL.obterContatoClienteNegociacao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //   
        if(objPesquisar.CODEDE == 0)
        {
            objPesquisar.CODEDE = -1;
        }
        if (objPesquisar.CODTIPCTO == 0)
        {
            objPesquisar.CODTIPCTO = -1;
        }
        //
        dbCommand.AddWithValue("CODEDE", objPesquisar.CODEDE);

        dbCommand.AddWithValue("CODTIPCTO", objPesquisar.CODTIPCTO);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<NegociacaoOportunidadeTO.obterContatoClienteNegociacao>();
    }

    public List<NegociacaoOportunidadeTO.obterServicosClientes> obterServicosClientes(NegociacaoOportunidadeApiModel.NegociacaoOportunidadeApiModel objPesquisar)
    {
        var DALSQL = new NegociacaoOportunidadeDALSQL();
        string cmdSql = DALSQL.obterServicosClientes();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        // 
        if(objPesquisar.CODCLI == -1)
        {
            objPesquisar.CODCLI = 0;
        }
        //
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<NegociacaoOportunidadeTO.obterServicosClientes>();
    }

    
    public NegociacaoOportunidadeTO.obterValoresCondicaoComercial obterValoresCondicaoComercial(NegociacaoOportunidadeApiModel.NegociacaoOportunidadeApiModel objPesquisar)
    {
        var DALSQL = new NegociacaoOportunidadeDALSQL();
        string cmdSql = DALSQL.obterValoresCondicaoComercial();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCNDCMC", objPesquisar.CODCNDCMC);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<NegociacaoOportunidadeTO.obterValoresCondicaoComercial>().FirstOrDefault();
    }






}