﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class NegociacaoOportunidadeBLL
{
    public List<NegociacaoOportunidadeTO.obterNegociacaoOportunidade> obterNegociacaoOportunidade(NegociacaoOportunidadeApiModel.NegociacaoOportunidadeApiModel objPesquisar)
    {
        try
        {
            var DAL = new NegociacaoOportunidadeDAL();
            return DAL.obterNegociacaoOportunidade(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DA NEGOCIACAO OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "obterNegociacaoOportunidade", "CadastroServicoApiModel.CadastroServicoApiModel", "NegociacaoOportunidadeBLL", "ERRO AO FAZER A CONSULTA DA NEGOCIACAO OPORTUNIDADE.");
            throw;
        }
    }

    public List<NegociacaoOportunidadeTO.obterEstruturaBasica> obterEstruturaBasica(NegociacaoOportunidadeApiModel.NegociacaoOportunidadeApiModel objPesquisar)
    {
        try
        {
            var DAL = new NegociacaoOportunidadeDAL();
            return DAL.obterEstruturaBasica(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DA ESTRUTURA BASICA.");
            Utilitario.InsereLog(ex, "obterEstruturaBasica", "CadastroServicoApiModel.CadastroServicoApiModel", "NegociacaoOportunidadeBLL", "ERRO AO FAZER A CONSULTA DA ESTRUTURA BASICA.");
            throw;
        }
    }

    public NegociacaoOportunidadeTO.obterDadosBasicos obterDadosBasicos(NegociacaoOportunidadeApiModel.NegociacaoOportunidadeApiModel objPesquisar)
    {
        try
        {
            var DAL = new NegociacaoOportunidadeDAL();
            return DAL.obterDadosBasicos(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DOS DADOS BASICOS CLIENTE.");
            Utilitario.InsereLog(ex, "obterDadosBasicos", "CadastroServicoApiModel.CadastroServicoApiModel", "NegociacaoOportunidadeBLL", "ERRO AO FAZER A CONSULTA DOS DADOS BASICOS CLIENTE.");
            throw;
        }
    }

    public List<NegociacaoOportunidadeTO.obterContatoClienteNegociacao> obterContatoClienteNegociacao(NegociacaoOportunidadeApiModel.NegociacaoOportunidadeApiModel objPesquisar)
    {
        try
        {
            var DAL = new NegociacaoOportunidadeDAL();
            return DAL.obterContatoClienteNegociacao(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CONTATO DO CLIENTE DA NEGOCIACAO.");
            Utilitario.InsereLog(ex, "obterDadosBasicos", "CadastroServicoApiModel.CadastroServicoApiModel", "NegociacaoOportunidadeBLL", "ERRO AO FAZER A CONSULTA DOS DADOS BASICOS CLIENTE.");
            throw;
        }
    }

    public List<NegociacaoOportunidadeTO.obterServicosClientes> obterServicosClientes(NegociacaoOportunidadeApiModel.NegociacaoOportunidadeApiModel objPesquisar)
    {
        try
        {
            var DAL = new NegociacaoOportunidadeDAL();
            return DAL.obterServicosClientes(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DOS SERVICOS DO CLIENTE.");
            Utilitario.InsereLog(ex, "obterDadosBasicos", "CadastroServicoApiModel.CadastroServicoApiModel", "NegociacaoOportunidadeBLL", "ERRO AO FAZER A CONSULTA DOS DADOS BASICOS CLIENTE.");
            throw;
        }
    }

    
    public NegociacaoOportunidadeTO.obterValoresCondicaoComercial obterValoresCondicaoComercial(NegociacaoOportunidadeApiModel.NegociacaoOportunidadeApiModel objPesquisar)
    {
        
        try
        {
            NegociacaoOportunidadeTO.obterValoresCondicaoComercial _item = new NegociacaoOportunidadeTO.obterValoresCondicaoComercial();

            if (objPesquisar != null)
            {
                var DAL = new NegociacaoOportunidadeDAL();
                _item = DAL.obterValoresCondicaoComercial(objPesquisar);
                if (_item.VLRDSC.IndexOf(',') == -1)
                    _item.VLRDSC = _item.VLRDSC + ",00";

                return _item;
            }
            else
                return new NegociacaoOportunidadeTO.obterValoresCondicaoComercial();
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DOS SERVICOS DO CLIENTE.");
            Utilitario.InsereLog(ex, "obterDadosBasicos", "CadastroServicoApiModel.CadastroServicoApiModel", "NegociacaoOportunidadeBLL", "ERRO AO FAZER A CONSULTA DOS DADOS BASICOS CLIENTE.");
            throw;
        }
    }

    public bool CancelarOportunidade(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objPesquisar)
    {





        return true;
    }

}