﻿using System.Collections.Generic;
using System;

namespace NegociacaoOportunidadeTO
{
    public class obterNegociacaoOportunidade
    {
        public int CODOPTVND { get; set; }

        public int CODCLI { get; set; }

        public string NOMCLI { get; set; }

        public string NUMCGCCLI { get; set; }

        public int CODCNIVNDTGV { get; set; }        

        public Int64 NUMSEQPESIND { get; set; }

        public string STATUS { get; set; }

        public string DATFNCCAD { get; set; }


        public Int64 NUMSEQPESINDPES { get; set; }

        public string TIPFNCRPNPES { get; set; }

        public int MATRICULAPES { get; set; }

        public string NOMPESCTOPES { get; set; }

        public string NUMCPFPES { get; set; }

        public string NUMTLFPES { get; set; }

        public string NUMTLFCELPES { get; set; }

        public string DESENDETNPES { get; set; }        
    }

    public class obterEstruturaBasica
    {
        public string ORDEM { get; set; }

        public string EMPRESA { get; set; }

        public string FUNCAO { get; set; }

        public string MATRICULA { get; set; }

        public string NOME { get; set; }

        public string EMAIL { get; set; }

        public string SUPERIOR { get; set; }
    }

    public class obterDadosBasicos
    {
        public int CODCLI { get; set; }

        public string NOMCLI { get; set; }

        public string NUMCGCCLI { get; set; }

        public string TAMAREFISVNDCLI { get; set; }

        public string TAMAREFISDPSCLI { get; set; }

        public string ENDERECO { get; set; }

        public string COMPLEMENTO { get; set; }

        public string CODCEP { get; set; }

        public string NOMBAI { get; set; }

        public string NOMCID { get; set; }

        public string NOMCIDEST { get; set; }

        public string CODESTUNI { get; set; }

        public string CANAL { get; set; }

        public string SEGMENTOMERCADO { get; set; }

        public string UNIDADENEGOCIO { get; set; }

        public string NUMTEL { get; set; }

        public string NUMTELCEL { get; set; }
    }

    public class obterContatoClienteNegociacao
    {
        public int CODCTO { get; set; }        

        public int TIPEDE { get; set; }

        public Int64 CODEDE { get; set; }

        public int CODTIPCTO { get; set; }

        public string DESTIPCTO { get; set; }

        public string NOMCTO { get; set; }

        public string DESENDETNCTO { get; set; }

        public string NUMTLFCEL { get; set; }

        public string NUMTLFCTO { get; set; }

        public string NUMTLFCTOSCD { get; set; }

        public string NUMTLFCTOTCR { get; set; }

        public string NUMTLFCTOQRT { get; set; }

        public string NUMTLFCTOQNT { get; set; }

        public string FNCCAD { get; set; }

        public string DATFNCCAD { get; set; }

        public string FNCALT { get; set; }

        public string DATFNCALT { get; set; }

        public string FNCDST { get; set; }

        public string DATFNCDST { get; set; }

        public string STATUS { get; set; }
    }

    public class obterServicosClientes
    {
        public int CODOPTVND { get; set; }
        
        public int CODSVCTGV { get; set; }

        public string DESSVCTGV { get; set; }

        public string DESTIPSVC { get; set; }

        public decimal VLRSVC { get; set; }

        public decimal VLRDSC { get; set; }

        public string NOMINDSTANGC { get; set; }
    }

    public class obterValoresCondicaoComercial
    {
        public string VLRDSC { get; set; }
        public string IDDESC { get; set; }
        public int CODPRDCES { get; set; }
        public string DESPRDCES { get; set; }
    }


}