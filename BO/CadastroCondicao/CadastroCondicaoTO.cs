﻿using System.Collections.Generic;

namespace CadastroCondicaoTO
{
    public class obterCadastroCondicao
    {
        public int CODCNDCMC { get; set; }

        public int CODCNDCMCCHECK { get; set; }

        public string DESCNDCMC { get; set; }

        public decimal VLRDSC { get; set; }

        public decimal PERDSC { get; set; }

        public int TIPDSC { get; set; }

        public int CODPRDCES { get; set; }

        public string DESPRDCES { get; set; }

        public int CODGRPEMPCLI { get; set; }

        public string NOMGRPEMPCLI { get; set; }

        //public int CODCLI { get; set; }

        public int CODSVCTGV { get; set; }

        public string DESSVCTGV { get; set; }

        public int INDTIPVGR { get; set; }

        public string DATINIVLD { get; set; }

        public string DATFIMVLD { get; set; }

        public int NUMMESINIVLD { get; set; }

        public int NUMMESFIMVLD { get; set; }

        public string STATUS { get; set; }

        public string VLRPRDCES { get; set; }

        public string DESOBSCNDCMC { get; set; }

        public string FNCCAD { get; set; }

        public string DATFNCCAD { get; set; }

        public string FNCALT { get; set; }

        public string DATFNCALT { get; set; }

        public string FNCDST { get; set; }

        public string DATFNCDST { get; set; }
    }
}

