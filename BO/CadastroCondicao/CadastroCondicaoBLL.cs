﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CadastroCondicaoBLL
{
    public List<CadastroCondicaoTO.obterCadastroCondicao> obterCadastroCondicao(CadastroCondicaoApiModel.CadastroCondicaoApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroCondicaoDAL();
            return DAL.obterCadastroCondicao(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO CONDICAO COMERCIAL.");
            Utilitario.InsereLog(ex, "alterarCadastroCondicao", "CadastroCondicaoApiModel.CadastroCondicaoApiModel", "CadastroCondicaoBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO CONDICAO COMERCIAL.");
            throw;
        }
    }   

    public bool inserirCadastroCondicao(CadastroCondicaoApiModel.inserirCadastroCondicao objInserir)
    {
        try
        {
            var DAL = new CadastroCondicaoDAL();
            return DAL.inserirCadastroCondicao(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO CADASTRO CONDICAO COMERCIAL.");
            Utilitario.InsereLog(ex, "inserirCadastroCondicao", "CadastroCondicaoApiModel.CadastroCondicaoApiModel", "CadastroCondicaoBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO CONDICAO COMERCIAL.");
            throw;
        }
    }    

    public bool alterarCadastroCondicao(CadastroCondicaoApiModel.alterarCadastroCondicao objAlterar)
    {
        try
        {
            var DAL = new CadastroCondicaoDAL();
            return DAL.alterarCadastroCondicao(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO CADASTRO CONDICAO COMERCIAL.");
            Utilitario.InsereLog(ex, "alterarCadastroCondicao", "CadastroCondicaoApiModel.CadastroCondicaoApiModel", "CadastroCondicaoBLL", "ERRO AO FAZER A ALTERACAO DO CADASTRO CONDICAO COMERCIAL.");
            throw;
        }
    }

    public bool ativarCadastroCondicao(CadastroCondicaoApiModel.CadastroCondicaoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroCondicaoDAL();
            return DAL.ativarCadastroCondicao(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATIVACAO DO CADASTRO CONDICAO.");
            Utilitario.InsereLog(ex, "ativarCadastroCondicao", "CadastroCondicaoApiModel.CadastroCondicaoApiModel", "CadastroCondicaoBLL", "ERRO AO FAZER A ATIVACAO DO CADASTRO CONDICAO.");
            throw;
        }
    }

    public bool desativarCadastroCondicao(CadastroCondicaoApiModel.CadastroCondicaoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroCondicaoDAL();
            return DAL.desativarCadastroCondicao(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATIVACAO DO CADASTRO CONDICAO.");
            Utilitario.InsereLog(ex, "desativarCadastroCondicao", "CadastroCondicaoApiModel.CadastroCondicaoApiModel", "CadastroCondicaoBLL", "ERRO AO FAZER A DESATIVACAO DO CADASTRO CONDICAO.");
            throw;
        }
    }
}