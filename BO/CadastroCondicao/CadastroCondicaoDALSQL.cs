﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class CadastroCondicaoDALSQL
{
    public string obterCadastroCondicao(int flgAtivo)
    {
        StringBuilder strBld = new StringBuilder(@"
                                           SELECT CON.CODCNDCMC, CON.CODCNDCMC CODCNDCMCCHECK, TRIM(CON.DESCNDCMC) DESCNDCMC
                                , CON.VLRDSC      
                                , CON.PERDSC
                                , CASE WHEN CON.VLRDSC > 0 THEN 1 
                                      ELSE 2
                                  END TIPDSC
                                , CON.CODPRDCES
                                , CES.DESPRDCES
                                , CON.CODGRPEMPCLI
                                , GRP.NOMGRPEMPCLI
                                , CON.CODSVCTGV
                                , SV.DESSVCTGV
                                , CON.INDTIPVGR
                                , TO_CHAR(CON.DATINIVLD,'DD/MM/YYYY') DATINIVLD
                                , TO_CHAR(CON.DATFIMVLD,'DD/MM/YYYY') DATFIMVLD
                                , CON.NUMMESINIVLD 
                                , CON.NUMMESFIMVLD 
                                , CASE CON.CODFNCDST
                                      WHEN CON.CODFNCDST THEN 'DESATIVADO' 
                                      ELSE 'ATIVADO' 
                                  END STATUS                 
                                , TRIM(FNC_CAD.NOMFNC) FNCCAD
                                , TO_CHAR(CON.DATCAD,'DD/MM/YYYY') DATFNCCAD
                                , TRIM(FNC_ALT.NOMFNC) FNCALT
                                , TO_CHAR(CON.DATALT,'DD/MM/YYYY') DATFNCALT
                                , CASE CON.DATDST
                                      WHEN CON.DATDST THEN TRIM(FNC_DST.NOMFNC) 
                                      ELSE NULL
                                  END FNCDST
                                , CASE CON.DATDST WHEN CON.DATDST 
                                      THEN TO_CHAR(CON.DATDST,'DD/MM/YYYY') 
                                      ELSE NULL 
                                  END DATFNCDST,
                                  TRIM(CON.VLRPRDCES) VLRPRDCES,
                                  TRIM(CON.DESOBSCNDCMC) DESOBSCNDCMC
                         FROM MRT.CADCNDCMCTGV CON 
                         LEFT JOIN MRT.T0100361 FNC_CAD ON FNC_CAD.CODFNC = CON.CODFNCCAD
                         LEFT JOIN MRT.T0100361 FNC_ALT ON FNC_ALT.CODFNC = CON.CODFNCALT
                         LEFT JOIN MRT.T0100361 FNC_DST ON FNC_DST.CODFNC = CON.CODFNCDST
                         LEFT JOIN MRT.CADSVCTGV SV ON SV.CODSVCTGV = CON.CODSVCTGV
                         LEFT JOIN MRT.CADCESPRDTGV CES ON CES.CODPRDCES = CON.CODPRDCES   
                         LEFT JOIN MRT.CADGRPEMPTGV GRP ON GRP.CODGRPEMPCLI = CON.CODGRPEMPCLI   
                         WHERE 1=1
                         AND CON.CODCNDCMC = :CODCNDCMC
                ");

        if (flgAtivo == 1)
        {
            strBld.AppendLine(" AND CON.DATDST IS NULL ");
        }
        if (flgAtivo == 2)
        {
            strBld.AppendLine(" AND CON.DATDST IS NOT NULL ");
        }

        strBld.AppendLine("ORDER BY STATUS, CON.CODCNDCMC");

        return strBld.ToString();
    }
   
    public string inserirCadastroCondicao()
    {
        return @"
                   INSERT INTO MRT.CADCNDCMCTGV (CODCNDCMC, DESCNDCMC, VLRDSC, PERDSC, CODPRDCES, CODGRPEMPCLI, CODSVCTGV, INDTIPVGR, DATINIVLD, DATFIMVLD, NUMMESINIVLD, NUMMESFIMVLD, DESOBSCNDCMC, VLRPRDCES, CODFNCCAD, DATCAD)
                   VALUES ((SELECT COALESCE(MAX(CODCNDCMC),0)+1 FROM MRT.CADCNDCMCTGV),
                   UPPER(:DESCNDCMC), :VLRDSC, :PERDSC, :CODPRDCES, :CODGRPEMPCLI, :CODSVCTGV, :INDTIPVGR, :DATINIVLD, :DATFIMVLD, :NUMMESINIVLD, :NUMMESFIMVLD, :DESOBSCNDCMC, :VLRPRDCES, :CODFNCCAD, SYSDATE)
                ";
    }    

    public string alterarCadastroCondicao()
    {
        return @"
                 UPDATE MRT.CADCNDCMCTGV SET
                 DESCNDCMC = UPPER(:DESCNDCMC),
                 VLRDSC = :VLRDSC,
                 PERDSC = :PERDSC,
                 CODPRDCES = :CODPRDCES,
                 CODGRPEMPCLI = :CODGRPEMPCLI,
                 CODSVCTGV = :CODSVCTGV,
                 INDTIPVGR = :INDTIPVGR,
                 DATINIVLD = :DATINIVLD,
                 DATFIMVLD = :DATFIMVLD,
                 NUMMESINIVLD = :NUMMESINIVLD,                 
                 NUMMESFIMVLD = :NUMMESFIMVLD,                 
                 DESOBSCNDCMC = :DESOBSCNDCMC,                 
                 VLRPRDCES = :VLRPRDCES,                 
                 CODFNCALT = :CODFNCALT,
                 DATALT = SYSDATE                 
                 WHERE CODCNDCMC = :CODCNDCMC
                ";
    }

    public string ativarCadastroCondicao(List<long> LISTCODCNDCMC)
    {

        return @"
                 UPDATE MRT.CADCNDCMCTGV SET                 
                 CODFNCDST = NULL,
                 DATDST = NULL
                 WHERE CODCNDCMC IN(#LISTCODCNDCMC)
                ".Replace("#LISTCODCNDCMC", Utilitario.ToListStr(LISTCODCNDCMC));
    }

    public string desativarCadastroCondicao(List<long> LISTCODCNDCMC)
    {
        return @"
                 UPDATE MRT.CADCNDCMCTGV SET                                 
                 CODFNCDST = :CODFNCDST,
                 DATDST = SYSDATE
                 WHERE CODCNDCMC IN(#LISTCODCNDCMC)
                ".Replace("#LISTCODCNDCMC", Utilitario.ToListStr(LISTCODCNDCMC));
    }
}