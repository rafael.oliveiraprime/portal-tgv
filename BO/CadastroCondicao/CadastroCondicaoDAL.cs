﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CadastroCondicaoDAL : DAL
{
    public List<CadastroCondicaoTO.obterCadastroCondicao> obterCadastroCondicao(CadastroCondicaoApiModel.CadastroCondicaoApiModel objPesquisar)
    {
        var DALSQL = new CadastroCondicaoDALSQL();
        string cmdSql = DALSQL.obterCadastroCondicao(objPesquisar.PRDATI);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (objPesquisar.CODCNDCMC == 0)
        {
            objPesquisar.CODCNDCMC = -1;
        }
        dbCommand.AddWithValue("CODCNDCMC", objPesquisar.CODCNDCMC);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroCondicaoTO.obterCadastroCondicao>();
    }   

    public bool inserirCadastroCondicao(CadastroCondicaoApiModel.inserirCadastroCondicao objInserir)
    {
        var DALSQL = new CadastroCondicaoDALSQL();
        string cmdSql = DALSQL.inserirCadastroCondicao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objInserir.CODFNCCAD = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //
        if (objInserir.TIPDSCPER == 1)
        {
            dbCommand.AddWithValue("VLRDSC", decimal.Parse(objInserir.VLRDSCPER));
            dbCommand.AddWithValue("PERDSC", 0);
        }
        else
        {
            dbCommand.AddWithValue("PERDSC", decimal.Parse(objInserir.VLRDSCPER));
            dbCommand.AddWithValue("VLRDSC", 0);
        }

        dbCommand.AddWithValue("DESCNDCMC", objInserir.DESCNDCMC);

        if (objInserir.CODPRDCES != -1)
            dbCommand.AddWithValue("CODPRDCES", objInserir.CODPRDCES);
        else
            dbCommand.AddWithValue("CODPRDCES", null);

        if (objInserir.CODGRPEMPCLI != -1)
            dbCommand.AddWithValue("CODGRPEMPCLI", objInserir.CODGRPEMPCLI);
        else
            dbCommand.AddWithValue("CODGRPEMPCLI", null);

        if (objInserir.CODSVCTGV != -1)
            dbCommand.AddWithValue("CODSVCTGV", objInserir.CODSVCTGV);
        else
            dbCommand.AddWithValue("CODSVCTGV", null);

        if (objInserir.NUMMESINIVLD != 0)
            dbCommand.AddWithValue("NUMMESINIVLD", objInserir.NUMMESINIVLD);
        else
            dbCommand.AddWithValue("NUMMESINIVLD", null);

        if (objInserir.NUMMESFIMVLD != 0)
            dbCommand.AddWithValue("NUMMESFIMVLD", objInserir.NUMMESFIMVLD);
        else
            dbCommand.AddWithValue("NUMMESFIMVLD", null);

        if (objInserir.DESOBSCNDCMC != "")
            dbCommand.AddWithValue("DESOBSCNDCMC", objInserir.DESOBSCNDCMC);
        else
            dbCommand.AddWithValue("DESOBSCNDCMC", null);

        if (objInserir.VLRPRDCES != "")
            dbCommand.AddWithValue("VLRPRDCES", objInserir.VLRPRDCES);
        else
            dbCommand.AddWithValue("VLRPRDCES", null);

        dbCommand.AddWithValue("INDTIPVGR", objInserir.INDTIPVGR);


        if (objInserir.DATINIVLD != null)
            dbCommand.AddWithValue("DATINIVLD", Convert.ToDateTime(objInserir.DATINIVLD));
        else
            dbCommand.AddWithValue("DATINIVLD", objInserir.DATINIVLD);

        if (objInserir.DATFIMVLD != null)
            dbCommand.AddWithValue("DATFIMVLD", Convert.ToDateTime(objInserir.DATFIMVLD));
        else
            dbCommand.AddWithValue("DATFIMVLD", objInserir.DATFIMVLD);

        dbCommand.AddWithValue("CODFNCCAD", objInserir.CODFNCCAD);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }
    
    public bool alterarCadastroCondicao(CadastroCondicaoApiModel.alterarCadastroCondicao objAlterar)
    {
        var DALSQL = new CadastroCondicaoDALSQL();
        string cmdSql = DALSQL.alterarCadastroCondicao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //  
        if (objAlterar.TIPDSCPER == 1)
        {
            dbCommand.AddWithValue("VLRDSC", decimal.Parse(objAlterar.VLRDSCPER));
            dbCommand.AddWithValue("PERDSC", 0);
        }
        else
        {
            dbCommand.AddWithValue("PERDSC", decimal.Parse(objAlterar.VLRDSCPER));
            dbCommand.AddWithValue("VLRDSC", 0);
        }
        //      
        dbCommand.AddWithValue("CODCNDCMC", objAlterar.CODCNDCMC);
        dbCommand.AddWithValue("DESCNDCMC", objAlterar.DESCNDCMC);

        if (objAlterar.CODPRDCES != -1)
            dbCommand.AddWithValue("CODPRDCES", objAlterar.CODPRDCES);
        else
            dbCommand.AddWithValue("CODPRDCES", null);

        if (objAlterar.CODGRPEMPCLI != -1)
            dbCommand.AddWithValue("CODGRPEMPCLI", objAlterar.CODGRPEMPCLI);
        else
            dbCommand.AddWithValue("CODGRPEMPCLI", null);

        if (objAlterar.CODSVCTGV != -1)
            dbCommand.AddWithValue("CODSVCTGV", objAlterar.CODSVCTGV);
        else
            dbCommand.AddWithValue("CODSVCTGV", null);

        if (objAlterar.NUMMESINIVLD != 0)
            dbCommand.AddWithValue("NUMMESINIVLD", objAlterar.NUMMESINIVLD);
        else
            dbCommand.AddWithValue("NUMMESINIVLD", null);

        if (objAlterar.NUMMESFIMVLD != 0)
            dbCommand.AddWithValue("NUMMESFIMVLD", objAlterar.NUMMESFIMVLD);
        else
            dbCommand.AddWithValue("NUMMESFIMVLD", null);

        if (objAlterar.DESOBSCNDCMC != "")
            dbCommand.AddWithValue("DESOBSCNDCMC", objAlterar.DESOBSCNDCMC);
        else
            dbCommand.AddWithValue("DESOBSCNDCMC", null);

        if (objAlterar.VLRPRDCES != "")
            dbCommand.AddWithValue("VLRPRDCES", objAlterar.VLRPRDCES);
        else
            dbCommand.AddWithValue("VLRPRDCES", null);

        dbCommand.AddWithValue("INDTIPVGR", objAlterar.INDTIPVGR);


        if (objAlterar.DATINIVLD != null)
            dbCommand.AddWithValue("DATINIVLD", Convert.ToDateTime(objAlterar.DATINIVLD));
        else
            dbCommand.AddWithValue("DATINIVLD", objAlterar.DATINIVLD);

        if (objAlterar.DATFIMVLD != null)
            dbCommand.AddWithValue("DATFIMVLD", Convert.ToDateTime(objAlterar.DATFIMVLD));
        else
            dbCommand.AddWithValue("DATFIMVLD", objAlterar.DATFIMVLD);


        dbCommand.AddWithValue("CODFNCALT", objAlterar.CODFNCALT);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool ativarCadastroCondicao(CadastroCondicaoApiModel.CadastroCondicaoApiModel objAlterar)
    {
        var DALSQL = new CadastroCondicaoDALSQL();
        string cmdSql = DALSQL.ativarCadastroCondicao(objAlterar.LISTCODCNDCMC);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool desativarCadastroCondicao(CadastroCondicaoApiModel.CadastroCondicaoApiModel objAlterar)
    {
        var DALSQL = new CadastroCondicaoDALSQL();
        string cmdSql = DALSQL.desativarCadastroCondicao(objAlterar.LISTCODCNDCMC);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCDST = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //                     
        dbCommand.AddWithValue("CODFNCDST", objAlterar.CODFNCDST);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }
}