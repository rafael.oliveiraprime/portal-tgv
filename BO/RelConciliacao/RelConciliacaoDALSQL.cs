﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class RelConciliacaoDALSQL
{
    public string obterConciliacao(string fildat)
    {
        StringBuilder strBld;
        strBld = new StringBuilder(@"                     SELECT RLC.CODCLI
                                            ,INITCAP(CLI.NOMCLI) NOMCLI
                                            ,(CASE CLI.NUMCGCCLI WHEN CLI.NUMCGCCLI THEN (SUBSTR(CLI.NUMCGCCLI, 1, 2) || '.' || SUBSTR(CLI.NUMCGCCLI, 3, 3) || '.' || SUBSTR(CLI.NUMCGCCLI, 6, 3) || '/' || SUBSTR(CLI.NUMCGCCLI, 9,4) || '-' || SUBSTR(CLI.NUMCGCCLI, 13,2) ) ELSE '' END) AS NUMCGCCLI
                                            ,RLC.CODOPTVND
                                            ,RLC.CODSVCTGV
                                            ,INITCAP(SVC.DESSVCTGV) DESSVCTGV
                                            ,CASE WHEN OPT.INDSTANGC = 1 THEN 'Em Negociação' 
                                                WHEN OPT.INDSTANGC = 2 THEN 'Venda Ganha' 
                                                WHEN OPT.INDSTANGC = 3 THEN 'Venda Perdida' 
                                                WHEN OPT.INDSTANGC = 4 THEN 'Abrir Ticket' 
                                                WHEN OPT.INDSTANGC = 5 THEN 'Aguardando Aprovação De Desconto' 
                                                WHEN OPT.INDSTANGC = 6 THEN 'Aguardando Aceite De Contrato' 
                                            END INDSTANGC
                                            ,RLC.CODDOCARZ
                                            ,INITCAP(CAD.DESDOCARZ) DESDOCARZ 
                                            ,NVL(TO_CHAR(RLC.DATENVDOC,'DD/MM/YYYY'),' ') DATENVDOC
                                            ,NVL(TO_CHAR(RLC.DATACEDOC,'DD/MM/YYYY'),' ') DATACEDOC
                                            ,NVL(TO_CHAR(TRUNC(RLC.DATACEDOC - RLC.DATENVDOC)),'Em Aberto') QNT_DIAS
                                            ,NVL(TO_CHAR(OPT.DATACEDOC,'DD/MM/YYYY'),' ') ACEITE
                                            ,CASE WHEN RLC.INDSTAAPV = 1 THEN 'Aguardando Assinatura'
                                                WHEN RLC.INDSTAAPV = 2 THEN 'Aguardando Aprovação'
                                                WHEN RLC.INDSTAAPV = 3 THEN 'Aprovado'
                                                WHEN RLC.INDSTAAPV = 4 THEN 'Rejeitado'
                                            END INDSTAAPV
                                            ,VND.CODCNIVNDTGV CODCNIVNDTGV
                                            ,INITCAP(USU.NOMFNC) AS NOMFNC
                                      FROM MRT.RLCSVCDOCARZTGV RLC
                                      INNER JOIN MRT.CADDOCARZCLITGV CAD ON CAD.CODDOCARZ = RLC.CODDOCARZ
                                      INNER JOIN MRT.CADSVCTGV SVC ON SVC.CODSVCTGV = RLC.CODSVCTGV
                                      INNER JOIN MRT.RLCOPTVNDSVCTGV OPT ON OPT.CODOPTVND = RLC.CODOPTVND AND RLC.CODSVCTGV = OPT.CODSVCTGV
                                      INNER JOIN MRT.CADOPTVNDCLITGV VND ON VND.CODCLI = RLC.CODCLI
                                      INNER JOIN MRT.T0100361 USU ON USU.CODFNC = VND.CODCNIVNDTGV
                                      INNER JOIN MRT.T0100043 CLI ON CLI.CODCLI = RLC.CODCLI
                                      WHERE 1=1
                                      AND RLC.CODCLI = :CODCLI
                                      AND CLI.NOMCLI LIKE '%' || UPPER(:NOMCLI) || '%'
                                      AND CLI.NUMCGCCLI = :NUMCGCCLI
                                      AND RLC.INDSTAAPV = :INDSTAAPV
                                       ");

        if (fildat == "0")
        {
            strBld.AppendLine(" AND TO_CHAR(RLC.DATENVDOC,'DD/MM/YYYY') BETWEEN :DATENVDOCINI AND :DATENVDOCFIM");
        }
        else if (fildat == "1")
        {
            strBld.AppendLine(" AND TO_CHAR(RLC.DATACEDOC,'DD/MM/YYYY') BETWEEN :DATENVDOCINI AND :DATENVDOCFIM");

        }

        strBld.AppendLine("GROUP BY RLC.CODCLI, CLI.NOMCLI, CLI.NUMCGCCLI, RLC.CODOPTVND, RLC.CODSVCTGV, SVC.DESSVCTGV, OPT.INDSTANGC, RLC.CODDOCARZ, CAD.DESDOCARZ, RLC.DATENVDOC, RLC.DATACEDOC, RLC.INDSTAAPV, VND.CODCNIVNDTGV, USU.NOMFNC, OPT.DATACEDOC ORDER BY CLI.NOMCLI, RLC.CODCLI, RLC.CODOPTVND, RLC.CODSVCTGV, CAD.DESDOCARZ");


        return strBld.ToString(); 
    }

    
}