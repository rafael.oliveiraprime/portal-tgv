﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class RelConciliacaoDAL : DAL
{
    public List<RelConciliacaoTO.obterConciliacao> obterConciliacao(RelConciliacaoApiModel.obterRelConciliacaoApiModel objPesquisar)
    {
        var DALSQL = new RelConciliacaoDALSQL();
        string cmdSql = DALSQL.obterConciliacao(objPesquisar.FILDAT);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //    
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("NOMCLI", objPesquisar.NOMCLI);
        dbCommand.AddWithValue("NUMCGCCLI", Regex.Replace(objPesquisar.NUMCGCCLI, "[/.-]", String.Empty));
        dbCommand.AddWithValue("INDSTAAPV", objPesquisar.INDSTAAPV);

        if (objPesquisar.DATENVDOCINI != "")
        {       
            dbCommand.AddWithValue("DATENVDOCINI", objPesquisar.DATENVDOCINI);

            if (objPesquisar.DATENVDOCFIM != "")
                dbCommand.AddWithValue("DATENVDOCFIM", objPesquisar.DATENVDOCFIM);
            else
                dbCommand.AddWithValue("DATENVDOCFIM", "");
        }
        else
        {
            dbCommand.AddWithValue("DATENVDOCINI", "");
            dbCommand.AddWithValue("DATENVDOCFIM", "");
        }


        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<RelConciliacaoTO.obterConciliacao>();
    }


}