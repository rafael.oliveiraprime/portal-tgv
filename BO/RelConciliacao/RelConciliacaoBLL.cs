﻿using System;
using System.Collections.Generic;

public class RelConciliacaoBLL
{
    public List<RelConciliacaoTO.obterConciliacao> obterConciliacao(RelConciliacaoApiModel.obterRelConciliacaoApiModel objPesquisar)
    {
        try
        {
            var DAL = new RelConciliacaoDAL();
            return DAL.obterConciliacao(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO RELATORIO DE OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "obterConciliacao", "RelConciliacaoApiModel.obterConciliacaoApiModel", "RelConciliacaoBLL", "ERRO AO FAZER A CONSULTA DO RELATORIO DE CONCILIAÇÃO.");
            throw;
        }
    }


}