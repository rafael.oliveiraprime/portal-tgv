﻿
namespace RelConciliacaoTO
{
    public class obterConciliacao
    {
        public int CODCLI { get; set; }
        public string NOMCLI { get; set; }
        public string NUMCGCCLI { get; set; }
        public int CODOPTVND { get; set; }
        public int CODSVCTGV { get; set; }
        public string DESSVCTGV { get; set; }
        public string INDSTANGC { get; set; }
        public int CODDOCARZ { get; set; }
        public string DESDOCARZ { get; set; }
        public string DATENVDOC { get; set; }
        public string DATACEDOC { get; set; }
        public string QNT_DIAS { get; set; }
        public string ACEITE { get; set; }
        public string INDSTAAPV { get; set; }
        public int CODCNIVNDTGV { get; set; }
        public string NOMFNC { get; set; }
    }
    
}