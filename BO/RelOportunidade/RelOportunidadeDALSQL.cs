﻿
public class RelOportunidadeDALSQL
{
    public string obterOportunidade()
    {
        return @"     SELECT S.CODOPTVND
                            ,C.CODCLI
                            ,CLI.NOMCLI
                            ,CLI.NUMCGCCLI
                            ,C.CODCNIVNDTGV
                            ,S.CODSVCTGV              
                            ,SVC.DESSVCTGV
                            ,SUBSTR(FNC.NOMFNC,1,25) AS NOMCON
                            ,CASE WHEN S.INDSTANGC = 1 THEN 'EM NEGOCIAÇÃO' 
                                WHEN S.INDSTANGC = 2 THEN 'VENDA GANHA' 
                                WHEN S.INDSTANGC = 3 THEN 'VENDA PERDIDA' 
                                WHEN S.INDSTANGC = 4 THEN 'ABRIR SITE SURVEY' 
                                WHEN S.INDSTANGC = 5 THEN 'AGUARDANDO APROVAÇÃO DE DESCONTO' 
                                WHEN S.INDSTANGC = 6 THEN 'AGUARDANDO ACEITE DE CONTRATO'                          
                                WHEN S.INDSTANGC = 7 THEN 'AGUARDANDO ASS. DE DOCUMENTO' END NOMINDSTANGC                              
                            ,S.DATACEDOC
                            ,S.VLRSVC * NVL(S.QDEPRDVND,1) AS VLRSVC
                            ,SUM(NVL(D.VLRDSC,0)) AS VLRDSC
                            ,SUM(NVL(D.PERDSC,0)) AS PERDSC
                            ,ROUND(( (S.VLRSVC * NVL(S.QDEPRDVND,1)) - SUM(NVL(D.VLRDSC,0))) * DECODE(SUM(NVL(D.PERDSC,0)),0,1,1-(SUM(D.PERDSC)/100)),2) VLRTOTSVC
                            ,TRIM(CPI.NOMPESCTO) AS NOMPESCTOPES
                            ,CASE WHEN C.CODCNLORICLI=1 THEN 'SITE (PORTAL)'
                                WHEN C.CODCNLORICLI=2 THEN 'TELEFONE'
                                WHEN C.CODCNLORICLI=3 THEN 'E-MAIL'
                                WHEN C.CODCNLORICLI=4 THEN 'WHATSAPP'
                                WHEN C.CODCNLORICLI=5 THEN 'PARCERIA' 
                                WHEN C.CODCNLORICLI=6 THEN 'CAMPANHA'
                                WHEN C.CODCNLORICLI=7 THEN 'OUTRO' 
                                WHEN C.CODCNLORICLI=8 THEN 'PROSPECT' 
                                WHEN C.CODCNLORICLI=9 THEN 'PRÓPRIA' 
                                WHEN C.CODCNLORICLI=10 THEN 'GERENTE TRIBANCO' 
                                WHEN C.CODCNLORICLI=11 THEN 'GERENTE MARTINS'
                                WHEN C.CODCNLORICLI=12 THEN 'RCA'
                                WHEN C.CODCNLORICLI=13 THEN 'EX-SMART'
                                WHEN C.CODCNLORICLI=14 THEN 'TRIBANCO - GERENCIADOR' 
                                WHEN C.CODCNLORICLI=15 THEN 'MIGRAÇÃO SERVIÇO' 
                                WHEN C.CODCNLORICLI=16 THEN 'TGV - GERENCIADOR' 
                                END NOMCNLORICLI
                            ,TO_CHAR(TRUNC(C.DATFIMNGCOPT), 'DD/MM/YYYY') AS DATFIMNGCOPT
                            ,NVL(TO_CHAR(TRUNC(C.DATFIMNGCOPT - C.DATCAD)), 'EM ABERTO') AS SLA
                            ,TO_CHAR(TRUNC(C.DATCAD), 'DD/MM/YYYY') AS DATCAD
                       FROM MRT.CADOPTVNDCLITGV C
                      INNER JOIN MRT.T0100043 CLI ON CLI.CODCLI = C.CODCLI                          
                      INNER JOIN MRT.RLCOPTVNDSVCTGV S ON C.CODOPTVND = S.CODOPTVND-- AND T.CODSVCTGV = S.CODSVCTGV
                       LEFT JOIN MRT.CADSVCTGV SVC ON SVC.CODSVCTGV = S.CODSVCTGV                         
                      INNER JOIN MRT.T0100361 FNC ON FNC.CODFNC = C.CODCNIVNDTGV
                       LEFT JOIN MRT.RLCOPTCNDCMCTGV D ON  S.CODOPTVND = D.CODOPTVND AND S.CODSVCTGV = D.CODSVCTGV 
                       LEFT JOIN MRT.CADCNDCMCTGV M ON M.CODCNDCMC = D.CODCNDCMC                          
                       LEFT JOIN MRT.CADDDOPESINDTGV CPI ON C.NUMSEQPESIND = CPI.NUMSEQPESIND
                      WHERE 1 = 1
                        AND C.CODCLI = :CODCLI 
                        AND CLI.NUMCGCCLI = :NUMCGCCLI 
                        AND S.CODSVCTGV = :CODSVCTGV 
                        AND S.INDSTANGC = :STATUS 
                        AND TRUNC(C.DATCAD) BETWEEN TRUNC(TO_DATE(:DATAINICIO,'DD/MM/YYYY')) AND TRUNC(TO_DATE(:DATAFIM,'DD/MM/YYYY'))
                        AND C.CODCNIVNDTGV = :CODCNIVNDTGV
                        AND TO_CHAR(C.DATFIMNGCOPT, 'YYYYMM' ) = :MESANO 
                      GROUP BY S.CODOPTVND
                               ,C.CODCLI
                               ,CLI.NOMCLI                               
                               ,CLI.NUMCGCCLI
                               ,C.CODCNIVNDTGV
                               ,S.CODSVCTGV
                               ,SVC.DESSVCTGV
                               ,S.DATACEDOC                               
                               ,S.INDSTANGC
                               ,S.VLRSVC
                               ,S.QDEPRDVND
                               ,FNC.NOMFNC
                               ,CPI.NOMPESCTO
                               ,C.CODCNLORICLI
                               ,C.DATFIMNGCOPT
                               ,C.DATCAD
                     ORDER BY c.codcli, CLI.NOMCLI
                ";
    }





}