﻿using System;
using System.Collections.Generic;

public class RelOportunidadeBLL
{
    public List<RelOportunidadeTO.obterOportunidade> obterOportunidade(RelOportunidadeApiModel.obterRelOportunidadeApiModel objPesquisar)
    {
        try
        {
            var DAL = new RelOportunidadeDAL();

            if (objPesquisar.MESANO != null)
            {
                if (objPesquisar.MESANO.Trim() != "")
                    objPesquisar.MESANO = objPesquisar.MESANO.Replace(@"/","");
            }
            return DAL.obterOportunidade(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO RELATORIO DE OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "obterIndicacoes", "RelOportunidadeApiModel.obterOportunidadeApiModel", "RelOportunidadeBLL", "ERRO AO FAZER A CONSULTA DO RELATORIO DE OPORTUNIDADE.");
            throw;
        }
    }


}