﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class RelOportunidadeDAL : DAL
{
    public List<RelOportunidadeTO.obterOportunidade> obterOportunidade(RelOportunidadeApiModel.obterRelOportunidadeApiModel objPesquisar)
    {
        var DALSQL = new RelOportunidadeDALSQL();
        string cmdSql = DALSQL.obterOportunidade();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //    
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("NUMCGCCLI", Regex.Replace(objPesquisar.NUMCGCCLI, "[/.-]", String.Empty));
        dbCommand.AddWithValue("CODSVCTGV", objPesquisar.CODSVCTGV);
        dbCommand.AddWithValue("STATUS", objPesquisar.STATUS);
        dbCommand.AddWithValue("DATAINICIO", objPesquisar.DATAINICIO);
        dbCommand.AddWithValue("DATAFIM", objPesquisar.DATAFIM);
        dbCommand.AddWithValue("CODCNIVNDTGV", objPesquisar.CODCNIVNDTGV);
        dbCommand.AddWithValue("MESANO", objPesquisar.MESANO);
        
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<RelOportunidadeTO.obterOportunidade>();
    }


}