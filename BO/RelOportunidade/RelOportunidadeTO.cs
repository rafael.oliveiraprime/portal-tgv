﻿
namespace RelOportunidadeTO
{
    public class obterOportunidade
    {
        public int CODOPTVND { get; set; }
        public int CODCLI { get; set; }
        public string NOMCLI { get; set; }
        public string NUMCGCCLI { get; set; }
        public int CODCNIVNDTGV { get; set; }
        public int CODSVCTGV { get; set; }
        public string DESSVCTGV { get; set; }
        public string NOMCON { get; set; }
        public string NOMINDSTANGC { get; set; }
        public string DATACEDOC { get; set; }
        public decimal VLRSVC { get; set; }
        public decimal VLRDSC { get; set; }
        public decimal PERDSC { get; set; }
        public decimal VLRTOTSVC { get; set; }
        public string NOMPESCTOPES { get; set; }
        public string NOMCNLORICLI { get; set; }
        public string DATFIMNGCOPT { get; set; }
        public string SLA { get; set; }
        public string DATCAD { get; set; }

    }
    
}