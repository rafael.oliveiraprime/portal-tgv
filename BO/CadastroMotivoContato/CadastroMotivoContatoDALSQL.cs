﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class CadastroMotivoContatoDALSQL
{
    public string obterCadastroMotivoContato(int flgAtivo, int codMtvCto)
    {
        StringBuilder strBld = new StringBuilder(@"
                 SELECT MTV.CODMTVCTO, MTV.CODMTVCTO CODMTVCTOCHECK, MTV.DESMTVCTO,                   
                 CASE MTV.CODFNCDST WHEN MTV.CODFNCDST THEN 'DESATIVADO' ELSE 'ATIVADO' END STATUS,                              
                 TRIM(FNC_ALT.NOMFNC) FNCALT,
                 TO_CHAR(MTV.DATALT,'DD/MM/YYYY') DATFNCALT,
                 CASE MTV.DATDST WHEN MTV.DATDST THEN TRIM(FNC_DST.NOMFNC) ELSE NULL END FNCDST,
                 CASE MTV.DATDST WHEN MTV.DATDST THEN TO_CHAR(MTV.DATDST,'DD/MM/YYYY') ELSE NULL END DATFNCDST
                 FROM MRT.CADMTVCTOTGV MTV
                 LEFT JOIN MRT.T0100361 FNC_ALT ON FNC_ALT.CODFNC = MTV.CODFNCALT
                 LEFT JOIN MRT.T0100361 FNC_DST ON FNC_DST.CODFNC = MTV.CODFNCDST 
                 WHERE MTV.CODMTVCTO = :CODMTVCTO
                ");

        if (flgAtivo == 1)
        {
            if (codMtvCto == -1)
            {
                strBld.AppendLine(" WHERE DATDST IS NULL ");
            }
            else
            {
                strBld.AppendLine(" AND DATDST IS NULL ");
            }
        }
        if (flgAtivo == 2)
        {
            if (codMtvCto == -1)
            {
                strBld.AppendLine(" WHERE DATDST IS NOT NULL ");
            }
            else
            {
                strBld.AppendLine(" AND DATDST IS NOT NULL ");
            }
        }

        strBld.AppendLine("ORDER BY STATUS, CODMTVCTO");

        return strBld.ToString();
    }

    public string inserirCadastroMotivoContato()
    {
        return @"
                 INSERT INTO MRT.CADMTVCTOTGV (CODMTVCTO, DESMTVCTO, CODFNCALT, DATALT)
                 VALUES ((SELECT COALESCE(MAX(CODMTVCTO),0)+1 FROM MRT.CADMTVCTOTGV),
                 UPPER(:DESMTVCTO), :CODFNCALT, SYSDATE)
                ";
    }

    public string alterarCadastroMotivoContato()
    {
        return @"
                 UPDATE MRT.CADMTVCTOTGV SET
                 DESMTVCTO = UPPER(:DESMTVCTO),
                 CODFNCALT = :CODFNCALT,
                 DATALT = SYSDATE
                 WHERE CODMTVCTO = :CODMTVCTO
                ";       
    }

    public string ativarCadastroMotivoContato(List<long> LISTMTVCTO)
    {
        return @"
                 UPDATE MRT.CADMTVCTOTGV SET                 
                 CODFNCDST = NULL,
                 DATDST = NULL
                 WHERE CODMTVCTO IN(#LISTMTVCTO)
                ".Replace("#LISTMTVCTO", Utilitario.ToListStr(LISTMTVCTO));
    }

    public string desativarCadastroMotivoContato(List<long> LISTMTVCTO)
    {
        return @"
                 UPDATE MRT.CADMTVCTOTGV SET
                 CODFNCDST = :CODFNCDST,
                 DATDST = SYSDATE
                 WHERE CODMTVCTO IN(#LISTMTVCTO)
                ".Replace("#LISTMTVCTO", Utilitario.ToListStr(LISTMTVCTO));
    }
}