﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CadastroMotivoContatoDAL
/// </summary>
public class CadastroMotivoContatoDAL : DAL
{

    public List<CadastroMotivoContatoTO.obterCadastroMotivoContato> obterCadastroMotivoContato(CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel objPesquisar)
    {
        var DALSQL = new CadastroMotivoContatoDALSQL();
        string cmdSql = DALSQL.obterCadastroMotivoContato(objPesquisar.MTVCTOATI, objPesquisar.CODMTVCTO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //        
        dbCommand.AddWithValue("CODMTVCTO", objPesquisar.CODMTVCTO);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroMotivoContatoTO.obterCadastroMotivoContato>();
    }

    public bool inserirCadastroMotivoContato(CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel objInserir)
    {
        var DALSQL = new CadastroMotivoContatoDALSQL();
        string cmdSql = DALSQL.inserirCadastroMotivoContato();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        objInserir.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //        
        dbCommand.AddWithValue("DESMTVCTO", objInserir.DESMTVCTO);
        dbCommand.AddWithValue("CODFNCALT", objInserir.CODFNCALT);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool alterarCadastroMotivoContato(CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel objAlterar)
    {
        var DALSQL = new CadastroMotivoContatoDALSQL();
        string cmdSql = DALSQL.alterarCadastroMotivoContato();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //              
        dbCommand.AddWithValue("CODMTVCTO", objAlterar.CODMTVCTO);
        dbCommand.AddWithValue("DESMTVCTO", objAlterar.DESMTVCTO);
        dbCommand.AddWithValue("CODFNCALT", objAlterar.CODFNCALT);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool ativarCadastroMotivoContato(CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel objAlterar)
    {
        var DALSQL = new CadastroMotivoContatoDALSQL();
        string cmdSql = DALSQL.ativarCadastroMotivoContato(objAlterar.LISTMTVCTO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool desativarCadastroMotivoContato(CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel objAlterar)
    {
        var DALSQL = new CadastroMotivoContatoDALSQL();
        string cmdSql = DALSQL.desativarCadastroMotivoContato(objAlterar.LISTMTVCTO);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCDST = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //              
        dbCommand.AddWithValue("CODFNCDST", objAlterar.CODFNCDST);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }
}