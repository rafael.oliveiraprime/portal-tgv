﻿using System.Collections.Generic;

namespace CadastroMotivoContatoTO
{
    public class obterCadastroMotivoContato
    {
        public int CODMTVCTO { get; set; }

        public int CODMTVCTOCHECK { get; set; }

        public string DESMTVCTO { get; set; }

        public string FNCALT { get; set; }

        public string DATFNCALT { get; set; }

        public string FNCDST { get; set; }

        public string DATFNCDST { get; set; }

        public string STATUS { get; set; }
    }
}