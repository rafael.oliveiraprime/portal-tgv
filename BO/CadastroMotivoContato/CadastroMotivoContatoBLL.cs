﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CadastroMotivoContatoBLL
{
    public List<CadastroMotivoContatoTO.obterCadastroMotivoContato> obterCadastroMotivoContato(CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroMotivoContatoDAL();
            return DAL.obterCadastroMotivoContato(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A PESQUISA DO MOTIVO CONTATO.");
            Utilitario.InsereLog(ex, "obterCadastroMotivoContato", "CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel", "CadastroMotivoContatoBLL", "ERRO AO FAZER A PESQUISA DO MOTIVO CONTATO.");
            throw;
        }
    }

    public bool inserirCadastroMotivoContato(CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel objInserir)
    {

        try
        {
            var DAL = new CadastroMotivoContatoDAL();
            return DAL.inserirCadastroMotivoContato(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO MOTIVO CONTATO.");
            Utilitario.InsereLog(ex, "inserirCadastroMotivoContato", "CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel", "CadastroMotivoContatoBLL", "ERRO AO FAZER A INSERCAO DO MOTIVO CONTATO.");
            throw;
        }
    }

    public bool alterarCadastroMotivoContato(CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel objAlterar)
    {
        try { 
        var DAL = new CadastroMotivoContatoDAL();

        return DAL.alterarCadastroMotivoContato(objAlterar);
        }

        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, " ERRO AO FAZER A ALTERACAO DO MOTIVO CONTATO.");
            Utilitario.InsereLog(ex, "alterarCadastroMotivoContato", "CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel", "CadastroMotivoContatoBLL", " ERRO AO FAZER A ALTERACAO DO MOTIVO CONTATO.");
            throw;
        }
    }

    public bool ativarCadastroMotivoContato(CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroMotivoContatoDAL();
            return DAL.ativarCadastroMotivoContato(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATIVACAO DO CADASTRO MOTIVO CONTATO.");
            Utilitario.InsereLog(ex, "ativarCadastroMotivoContato", "CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel", "CadastroMotivoContatoBLL", "ERRO AO FAZER A ATIVACAO DO CADASTRO MOTIVO CONTATO.");
            throw;
        }
    }

    public bool desativarCadastroMotivoContato(CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroMotivoContatoDAL();
            return DAL.desativarCadastroMotivoContato(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A DESATIVACAO DO CADASTRO MOTIVO CONTATO.");
            Utilitario.InsereLog(ex, "desativarCadastroMotivoContato", "CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel", "CadastroMotivoContatoBLL", "ERRO AO FAZER A DESATIVACAO DO CADASTRO MOTIVO CONTATO.");
            throw;
        }
    }
}