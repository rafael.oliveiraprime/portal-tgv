﻿using System.Collections.Generic;

namespace CadastroGrupoEconomicoTO
{
    public class obterCadastroGrupoEconomico
    {
        public int CODGRPEMPCLI { get; set; }
        public int CODGRPEMPCLICHECK { get; set; }
        public string NOMGRPEMPCLI { get; set; }
        public int INDFATGRP { get; set; }
        public string NOMINDFATGRP { get; set; }
        public string CODCLIGRPFAT { get; set; }
        public string CNPJ { get; set; }
        public string RAZAOSOCIAL { get; set; }
        public string FNCCAD { get; set; }
        public string DATFNCCAD { get; set; }
        public string FNCALT { get; set; }
        public string DATFNCALT { get; set; }
        public string FNCDST { get; set; }
        public string DATFNCDST { get; set; }
        public string STATUS { get; set; }

        public int INDDBTAUTPGT { get; set; }
        public int CODBCO { get; set; }
        public int CODAGEBCO { get; set; }
        public string NUMDIGVRFAGE { get; set; }
        public int NUMCNTCRRCTT { get; set; }
        public string NUMDIGVRFCNTCRR { get; set; }

    }

    public class obterCadastroGrupoEconomicoMartins
    {
        public int CODCLI { get; set; }
        public string NOMCLI { get; set; }
        public string NUMCGCCLI { get; set; }
        public string DESGRPEMPCLI { get; set; }
    }

    public class obterCadastroGrupoEconomicoXClientesExistentes
    {
        public int CODCLICHECK { get; set; }
        public int CODCLI { get; set; }
        public string NOMCLI { get; set; }
        public string NUMCGCCLI { get; set; }
        public int CODGRPEMPCLI { get; set; }
        public string NOMGRPEMPCLI { get; set; }
        public string DESGRPEMPCLI { get; set; }
        public string STATUS { get; set; }
        public string SERVICOS { get; set; }
        public string NOMCID { get; set; }
        public string CODESTUNI { get; set; }
    }

    public class obterMaximoGrupoEconomico
    {
        public int CODGRPEMPCLI { get; set; }
    }

    public class BuscaGrupoEconomico
    {
        public int CODGRPEMPCLI { get; set; }
        public string NOMGRPEMPCLI { get; set; }
        public int INDFATGRP { get; set; }
        public int CODCLIGRPFAT { get; set; }
    }

    public class ExisteGrupoCli
    {
        public int EXISTE { get; set; }
    }

    public class VerificaSeEDebito
    {
        public int CODGRPEMPCLI { get; set; }
        public int INDDBTAUTPGT { get; set; }
    }

    public class RelacaoGrupoCliente
    {
        public int CODGRPEMPCLI { get; set; }
        public int CODCLI { get; set; }
    }

    public class obterCadastroGrupoXClientesExistentes
    {
        public int CODCLI { get; set; }
        public string NOMCLI { get; set; }
        public string NUMCGCCLI { get; set; }
        public int CODGRPEMPCLI { get; set; }
        public string NOMGRPEMPCLI { get; set; }
        public string DESGRPEMPCLI { get; set; }
    }

    public class obterCadastroGrupoFaturamentoServico
    {
        public int CODCLI { get; set; }
        public string NUMCGCCLI { get; set; }
        public string NOMCLI { get; set; }
        public int CODSVCTGV { get; set; }
        public string DESSVCTGV { get; set; }
        public string STATUS { get; set; }
    }



}