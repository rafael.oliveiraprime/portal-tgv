﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrmApiModel;

public class CadastroGrupoEconomicoDAL : DAL
{
    public List<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomico> obterCadastroGrupoEconomico(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel objPesquisar)
    {
        var DALSQL = new CadastroGrupoEconomicoDALSQL();
        string cmdSql = DALSQL.obterCadastroGrupoEconomico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (objPesquisar.CODGRPEMPCLI == 0)
            objPesquisar.CODGRPEMPCLI = -1;

        if (objPesquisar.INDFATGRP == 0)
            objPesquisar.INDFATGRP = -1;


        if (objPesquisar.NOMGRPEMPCLI == "-1")
            objPesquisar.NOMGRPEMPCLI = "";


        dbCommand.AddWithValue("CODGRPEMPCLI", objPesquisar.CODGRPEMPCLI);
        dbCommand.AddWithValue("NOMGRPEMPCLI", objPesquisar.NOMGRPEMPCLI);
        dbCommand.AddWithValue("INDFATGRP", objPesquisar.INDFATGRP);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomico>();
    }

    public bool inserirCadastroGrupoEconomico(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel objInserir)
    {
        var DALSQL = new CadastroGrupoEconomicoDALSQL();
        string cmdSql = DALSQL.inserirCadastroGrupoEconomico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        // 
        objInserir.CODFNC = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //
        if (objInserir.NOMGRPEMPCLI == null)
        {
            objInserir.NOMGRPEMPCLI = " ";
        }

        dbCommand.AddWithValue("NOMGRPEMPCLI", objInserir.NOMGRPEMPCLI);
        dbCommand.AddWithValue("INDFATGRP", objInserir.INDFATGRP);
        dbCommand.AddWithValue("CODFNC", objInserir.CODFNC);
        //
        if (objInserir.CODCLIGRPFAT == -1)
        {
            dbCommand.AddWithValue("CODCLIGRPFAT", null);
        }
        else
        {
            dbCommand.AddWithValue("CODCLIGRPFAT", objInserir.CODCLIGRPFAT);
        }
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public bool alterarCadastroGrupoEconomico(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel objAlterar)
    {
        var DALSQL = new CadastroGrupoEconomicoDALSQL();
        string cmdSql = DALSQL.alterarCadastroGrupoEconomico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        objAlterar.CODFNC = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //
        dbCommand.AddWithValue("NOMGRPEMPCLI", objAlterar.NOMGRPEMPCLI);
        dbCommand.AddWithValue("INDFATGRP", objAlterar.INDFATGRP);
        dbCommand.AddWithValue("CODFNC", objAlterar.CODFNC);
        //
        if (objAlterar.CODCLIGRPFAT == -1)
        {
            dbCommand.AddWithValue("CODCLIGRPFAT", null);
        }
        else
        {
            dbCommand.AddWithValue("CODCLIGRPFAT", objAlterar.CODCLIGRPFAT);
        }
        //
        dbCommand.AddWithValue("CODGRPEMPCLI", objAlterar.CODGRPEMPCLI);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public List<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoMartins> obterCadastroGrupoEconomicoMartins(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoMartinsApiModel objPesquisar)
    {
        var DALSQL = new CadastroGrupoEconomicoDALSQL();
        string cmdSql = DALSQL.obterCadastroGrupoEconomicoMartins(objPesquisar.GRUPREDE);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (objPesquisar.CODCLI == 0)
            objPesquisar.CODCLI = -1;
        //
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("NOMCLI", objPesquisar.NOMCLI);
        dbCommand.AddWithValue("CNPJ", objPesquisar.CNPJ);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoMartins>();
    }


    public bool inserirCadastroGrupoEconomicoXClientes(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel objInserir)
    {
        var DALSQL = new CadastroGrupoEconomicoDALSQL();
        string cmdSql = DALSQL.inserirCadastroGrupoEconomicoXClientes();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODGRPEMPCLI", objInserir.CODGRPEMPCLI);
        dbCommand.AddWithValue("CODCLI", objInserir.CODCLI);
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public List<CadastroGrupoEconomicoTO.obterCadastroGrupoXClientesExistentes> BuscaClientesExistentes(List<long> _listclientes, int _codgrupo)
    {

        var DALSQL = new CadastroGrupoEconomicoDALSQL();
        string cmdSql = DALSQL.buscaClientesExistentes(_listclientes);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        //if (_codgrupo != -1)
        dbCommand.AddWithValue("CODGRPEMPCLI", _codgrupo);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroGrupoEconomicoTO.obterCadastroGrupoXClientesExistentes>();
    }


    public List<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes> obterCadastroGrupoEconomicoXClientesExistentes(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel objPesquisar)
    {

        var DALSQL = new CadastroGrupoEconomicoDALSQL();
        string cmdSql = DALSQL.obterCadastroGrupoEconomicoXClientesExistentes();
        //
        if (objPesquisar.CODGRPEMPCLI == -1)
            objPesquisar.CODGRPEMPCLI = 0;

        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODGRPEMPCLI", objPesquisar.CODGRPEMPCLI);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes>();

    }

    public List<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes> obterClientesExistentesSession(List<long> _ListClientes)
    {
        var DALSQL = new CadastroGrupoEconomicoDALSQL();
        string cmdSql = DALSQL.buscaClientesExistentesSession(_ListClientes);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes>();
    }


    public bool apagarCadastroGrupoEconomicoXClientes(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel objInserir)
    {
        var DALSQL = new CadastroGrupoEconomicoDALSQL();
        string cmdSql = DALSQL.apagarCadastroGrupoEconomicoXClientes(objInserir.CODCLIENTES);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODGRPEMPCLI", objInserir.CODGRPEMPCLI);
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public CadastroGrupoEconomicoTO.obterMaximoGrupoEconomico obterMaximoGrupoEconomico()
    {
        var DALSQL = new CadastroGrupoEconomicoDALSQL();
        string cmdSql = DALSQL.obterMaximoGrupoEconomico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        var retorno = MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroGrupoEconomicoTO.obterMaximoGrupoEconomico>();
        if (retorno.Count > 0)
        {
            return retorno.FirstOrDefault();
        }
        else
        {
            return new CadastroGrupoEconomicoTO.obterMaximoGrupoEconomico();
        }
    }

    public bool ativarCadastroGrupoEconomico(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel objAlterar)
    {
        var DALSQL = new CadastroGrupoEconomicoDALSQL();
        string cmdSql = DALSQL.ativarCadastroGrupoEconomico(objAlterar.LISTCODGRPEMPCLI);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool desativarCadastroGrupoEconomico(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel objAlterar)
    {
        var DALSQL = new CadastroGrupoEconomicoDALSQL();
        string cmdSql = DALSQL.desativarCadastroGrupoEconomico(objAlterar.LISTCODGRPEMPCLI);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNC = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //                     
        dbCommand.AddWithValue("CODFNC", objAlterar.CODFNC);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    
    public CadastroGrupoEconomicoTO.BuscaGrupoEconomico BuscaGrupoEconomico(int CODGRPEMPCLI)
    {
        var DALSQL = new CadastroGrupoEconomicoDALSQL();
        string cmdSql = DALSQL.BuscaGrupoEconomico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODGRPEMPCLI", CODGRPEMPCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroGrupoEconomicoTO.BuscaGrupoEconomico>().FirstOrDefault();
    }

    public CadastroGrupoEconomicoTO.BuscaGrupoEconomico BuscaGrupoEconomicoAtivo(long CODGRPEMPCLI)
    {
        var DALSQL = new CadastroGrupoEconomicoDALSQL();
        string cmdSql = DALSQL.BuscaGrupoEconomico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODGRPEMPCLI", CODGRPEMPCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroGrupoEconomicoTO.BuscaGrupoEconomico>().FirstOrDefault();
    }

    public CadastroGrupoEconomicoTO.ExisteGrupoCli ExisteGrupoCli(long CODCLI)
    {
        var DALSQL = new CadastroGrupoEconomicoDALSQL();
        string cmdSql = DALSQL.ExisteGrupoCli();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", CODCLI);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroGrupoEconomicoTO.ExisteGrupoCli>().FirstOrDefault();
    }

    

    public CadastroGrupoEconomicoTO.VerificaSeEDebito VerificaSeEDebito(int CODGRPEMPCLI)
    {
        var DALSQL = new CadastroGrupoEconomicoDALSQL();
        string cmdSql = DALSQL.VerificaSeEDebito();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODGRPEMPCLI", CODGRPEMPCLI);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroGrupoEconomicoTO.VerificaSeEDebito>().FirstOrDefault();
    }

    public List<CadastroGrupoEconomicoTO.RelacaoGrupoCliente> BuscaRelacaoGrupoCliente(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel objPesquisar)
    {
        var DALSQL = new CadastroGrupoEconomicoDALSQL();
        string cmdSql = DALSQL.BuscaRelacaoGrupoCliente();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODGRPEMPCLI", objPesquisar.CODGRPEMPCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroGrupoEconomicoTO.RelacaoGrupoCliente>();
    }


    public CadastroGrupoEconomicoTO.ExisteGrupoCli ExisteLojaCentralizadora(int CODGRPEMPCLI, int CODCLIGRPFAT)
    {
        var DALSQL = new CadastroGrupoEconomicoDALSQL();
        string cmdSql = DALSQL.ExisteLojaCentralizadora();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODGRPEMPCLI", CODGRPEMPCLI);
        dbCommand.AddWithValue("CODCLIGRPFAT", CODCLIGRPFAT);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroGrupoEconomicoTO.ExisteGrupoCli>().FirstOrDefault();
    }

    
    public CadastroGrupoEconomicoTO.ExisteGrupoCli ExisteCentralizadoraGrupoCli(int CODGRPEMPCLI, int CODCLI)
    {
        var DALSQL = new CadastroGrupoEconomicoDALSQL();
        string cmdSql = DALSQL.ExisteCentralizadoraGrupoCli();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODGRPEMPCLI", CODGRPEMPCLI);
        dbCommand.AddWithValue("CODCLI", CODCLI);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroGrupoEconomicoTO.ExisteGrupoCli>().FirstOrDefault();
    }

    public List<CadastroGrupoEconomicoTO.obterCadastroGrupoFaturamentoServico> obterCadastroGrupoFaturamentoServico(GrupoFaturamentoApiModel.GrupoFaturamentoApiModel objPesquisar)
    {
        var DALSQL = new CadastroGrupoEconomicoDALSQL();
        string cmdSql = DALSQL.obterCadastroGrupoFaturamentoServico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODGRPEMPCLI", objPesquisar.CODGRPEMPCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroGrupoEconomicoTO.obterCadastroGrupoFaturamentoServico>();
    }

    public bool AtualizarContaCorrente(GrupoFaturamentoApiModel.GrupoFaturamentoContaCorrenteApiModel objAtualizar)
    {

        var DALSQL = new CadastroGrupoEconomicoDALSQL();
        string cmdSql = DALSQL.AtualizarContaCorrente();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODGRPEMPCLI", objAtualizar.CODGRPEMPCLI);
        dbCommand.AddWithValue("INDDBTAUTPGT", objAtualizar.INDDBTAUTPGT);

        if (objAtualizar.CODBCO > 0)
            dbCommand.AddWithValue("CODBCO", objAtualizar.CODBCO);
        else
            dbCommand.AddWithValue("CODBCO", null);

        if (objAtualizar.CODAGEBCO > 0)
            dbCommand.AddWithValue("CODAGEBCO", objAtualizar.CODAGEBCO);
        else
            dbCommand.AddWithValue("CODAGEBCO", null);

        if ((objAtualizar.NUMDIGVRFAGE != null) && (objAtualizar.NUMDIGVRFAGE != ""))
            dbCommand.AddWithValue("NUMDIGVRFAGE", objAtualizar.NUMDIGVRFAGE);
        else
            dbCommand.AddWithValue("NUMDIGVRFAGE", null);

        if ((objAtualizar.NUMCNTCRRCTT != null) && (objAtualizar.NUMCNTCRRCTT != ""))
            dbCommand.AddWithValue("NUMCNTCRRCTT", objAtualizar.NUMCNTCRRCTT);
        else
            dbCommand.AddWithValue("NUMCNTCRRCTT", null);

        if ((objAtualizar.NUMDIGVRFCNTCRR != null) && (objAtualizar.NUMDIGVRFCNTCRR != ""))
            dbCommand.AddWithValue("NUMDIGVRFCNTCRR", objAtualizar.NUMDIGVRFCNTCRR);
        else
            dbCommand.AddWithValue("NUMDIGVRFCNTCRR", null);

        //var _codigofunc = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //dbCommand.AddWithValue("CODFNCALT", _codigofunc);

        //
        dbCommand.TrataDbCommandUniversal();

        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }




}