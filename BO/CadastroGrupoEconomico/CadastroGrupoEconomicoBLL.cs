﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Transactions;


public class CadastroGrupoEconomicoBLL
{
    public List<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomico> obterCadastroGrupoEconomico(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroGrupoEconomicoDAL();
            return DAL.obterCadastroGrupoEconomico(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO GRUPO ECONOMICO.");
            Utilitario.InsereLog(ex, "obterCadastroGrupoEconomico", "CadastroContatoApiModel.CadastroContatoApiModel", "CadastroGrupoEconomicoBLL", "ERRO AO FAZER A CONSULTA DO GRUPO ECONOMICO.");
            throw;
        }
    }

    public bool inserirCadastroGrupoEconomico(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel objInserir)
    {
        bool _isertgrupo = false;
        CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel _itemcliente = new CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel();
        CadastroOportunidadeApiModel.CadastroOportunidadeApiModel _negociacao = new CadastroOportunidadeApiModel.CadastroOportunidadeApiModel();
        GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel _gestaocontas = new GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel();


        try
        {

            using (TransactionScope Scope = new TransactionScope(TransactionScopeOption.Required, (new TimeSpan(0, 3, 0)))) // DE ATÉ 3 MINUTOS.
            {
                var DAL = new CadastroGrupoEconomicoDAL();
                var DALNEG = new CadastroOportunidadeDAL();
                var DALGESTAO = new GestaoContasDAL();


                if (_isertgrupo = DAL.inserirCadastroGrupoEconomico(objInserir) == false)
                {
                    Scope.Dispose();
                    return false;
                }

                if (_isertgrupo)
                {
                    objInserir.CODGRPEMPCLI = DAL.obterMaximoGrupoEconomico().CODGRPEMPCLI;
                    if (GrupoEconomicoSession.Grupo != null)
                    {
                        foreach (CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes item in GrupoEconomicoSession.Grupo)
                        {
                            _itemcliente.CODCLI = item.CODCLI;
                            _itemcliente.CODGRPEMPCLI = objInserir.CODGRPEMPCLI;

                            if (DAL.inserirCadastroGrupoEconomicoXClientes(_itemcliente) == false)
                            {
                                Scope.Dispose();
                                return false;
                            }
                            _itemcliente = new CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel();


                            if (objInserir.INDFATGRP == 1)
                            {
                                //verifica e insere negociação 
                                _negociacao.CODCLI = item.CODCLI;
                                _negociacao.CODGRPEMPFAT = objInserir.CODGRPEMPCLI;

                                DALNEG.AtualizaGrupoNegocicaoServico(_negociacao);
                                _negociacao = new CadastroOportunidadeApiModel.CadastroOportunidadeApiModel();


                                //verifica e insere gstão de contas
                                _gestaocontas.CODCLI = item.CODCLI;
                                _gestaocontas.CODGRPEMPFAT = objInserir.CODGRPEMPCLI;

                                DALGESTAO.AtualizaGrupoGetaoServico(_gestaocontas);
                                _gestaocontas = new GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel();
                            }

                        }
                    }
                }

                Scope.Complete();
                return _isertgrupo;
            }
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO GRUPO ECONOMICO.");
            Utilitario.InsereLog(ex, "inserirCadastroContato", "CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel", "CadastroGrupoEconomicoBLL", "ERRO AO FAZER A INSERCAO DO GRUPO ECONOMICO.");
            throw;
        }
    }


    public string alterarCadastroGrupoEconomico(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel objInserir)
    {

        bool _alteragrupo = false;
        CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel _itemcliente = new CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel();
        CadastroOportunidadeApiModel.CadastroOportunidadeApiModel _negociacao = new CadastroOportunidadeApiModel.CadastroOportunidadeApiModel();
        GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel _gestaocontas = new GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel();
        List<CadastroGrupoEconomicoTO.RelacaoGrupoCliente> _realcaogrupocliente = new List<CadastroGrupoEconomicoTO.RelacaoGrupoCliente>();

        try
        {
            var DAL = new CadastroGrupoEconomicoDAL();
            var DALNEG = new CadastroOportunidadeDAL();
            var DALGESTAO = new GestaoContasDAL();

            if (objInserir.INDFATGRP != 2)
            {
                if (DAL.ExisteLojaCentralizadora(objInserir.CODGRPEMPCLI, objInserir.CODCLIGRPFAT).EXISTE > 0)
                    return "2";
            }


            if (objInserir.CODGRPEMPCLI != -1)
            {
                _alteragrupo = DAL.alterarCadastroGrupoEconomico(objInserir);

                if (_alteragrupo)
                {
                    if (objInserir.INDFATGRP == 2)
                    {
                        _negociacao.CODGRPEMPFAT = objInserir.CODGRPEMPCLI;
                        DALNEG.AtualizaAllGrupoNegocicaoServico(_negociacao);

                        _gestaocontas.CODGRPEMPFAT = objInserir.CODGRPEMPCLI;
                        DALGESTAO.AtualizaAllGrupoGetaoServico(_gestaocontas);

                        _gestaocontas = new GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel();
                        _negociacao = new CadastroOportunidadeApiModel.CadastroOportunidadeApiModel();
                    }
                    else
                    {
                        _realcaogrupocliente = DAL.BuscaRelacaoGrupoCliente(objInserir);

                        if (_realcaogrupocliente.Count > 0)
                        {
                            foreach (CadastroGrupoEconomicoTO.RelacaoGrupoCliente _itemgrp in _realcaogrupocliente)
                            {

                                //verifica e insere negociação 
                                _negociacao.CODCLI = _itemgrp.CODCLI;
                                _negociacao.CODGRPEMPFAT = objInserir.CODGRPEMPCLI;

                                DALNEG.AtualizaGrupoNegocicaoServico(_negociacao);
                                _negociacao = new CadastroOportunidadeApiModel.CadastroOportunidadeApiModel();


                                //verifica e insere gstão de contas
                                _gestaocontas.CODCLI = _itemgrp.CODCLI;
                                _gestaocontas.CODGRPEMPFAT = objInserir.CODGRPEMPCLI;

                                DALGESTAO.AtualizaGrupoGetaoServico(_gestaocontas);
                                _gestaocontas = new GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel();

                            }
                        }
                    }

                }

            }
            else
                _alteragrupo = DAL.inserirCadastroGrupoEconomico(objInserir);




            if (_alteragrupo)
            {
                if (objInserir.CODGRPEMPCLI == -1)
                    objInserir.CODGRPEMPCLI = DAL.obterMaximoGrupoEconomico().CODGRPEMPCLI;

                if (GrupoEconomicoSession.Grupo != null)
                {
                    foreach (CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes item in GrupoEconomicoSession.Grupo)
                    {
                        _itemcliente.CODCLI = item.CODCLI;
                        _itemcliente.CODGRPEMPCLI = objInserir.CODGRPEMPCLI;

                        DAL.inserirCadastroGrupoEconomicoXClientes(_itemcliente);
                        _itemcliente = new CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel();


                        if (objInserir.INDFATGRP != 2)
                        {
                            //verifica e insere negociação 
                            _negociacao.CODCLI = item.CODCLI;
                            _negociacao.CODGRPEMPFAT = objInserir.CODGRPEMPCLI;

                            DALNEG.AtualizaGrupoNegocicaoServico(_negociacao);
                            _negociacao = new CadastroOportunidadeApiModel.CadastroOportunidadeApiModel();


                            //verifica e insere gstão de contas
                            _gestaocontas.CODCLI = item.CODCLI;
                            _gestaocontas.CODGRPEMPFAT = objInserir.CODGRPEMPCLI;

                            DALGESTAO.AtualizaGrupoGetaoServico(_gestaocontas);
                            _gestaocontas = new GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel();
                        }
                    }
                }
            }

            GrupoEconomicoSession.ClearSession();

            if (objInserir.INDFATGRP != 2)
            {
                if (DAL.ExisteGrupoCli(objInserir.CODCLIGRPFAT).EXISTE == 0)
                    return "3";
            }


            return "1";
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER AO ALTERAR O GRUPO ECONOMICO.");
            Utilitario.InsereLog(ex, "inserirCadastroContato", "CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel", "CadastroGrupoEconomicoBLL", "ERRO AO FAZER AO ALTERAR O GRUPO ECONOMICO.");
            throw;
        }
    }

    public List<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoMartins> obterCadastroGrupoEconomicoMartins(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoMartinsApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroGrupoEconomicoDAL();

            if (objPesquisar.CNPJ != "")
                objPesquisar.CNPJ = Regex.Replace(objPesquisar.CNPJ, "[/.-]", String.Empty);


            if (objPesquisar.TIPOBUSCAMODAL == 1)
                return DAL.obterCadastroGrupoEconomicoMartins(objPesquisar);
            else
                return new List<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoMartins>();

        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO GRUPO ECONOMICO.");
            Utilitario.InsereLog(ex, "obterCadastroGrupoEconomico", "CadastroContatoApiModel.CadastroContatoApiModel", "CadastroGrupoEconomicoBLL", "ERRO AO FAZER A CONSULTA DO GRUPO ECONOMICO.");
            throw;
        }
    }


    public string inserirCadastroGrupoEconomicoXClientes(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel objInserir)
    {
        List<CadastroGrupoEconomicoTO.obterCadastroGrupoXClientesExistentes> _clientesexistetentes = new List<CadastroGrupoEconomicoTO.obterCadastroGrupoXClientesExistentes>();
        string ClietesExistentes = "";

        if (GrupoEconomicoSession.Grupo != null)
            GrupoEconomicoSession.Grupo = new List<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes>();


        CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes itemSession = new CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes();

        try
        {
            var DAL = new CadastroGrupoEconomicoDAL();
            _clientesexistetentes = DAL.BuscaClientesExistentes(objInserir.CODCLIENTES, objInserir.CODGRPEMPCLI);
            if (_clientesexistetentes != null)
            {
                if (_clientesexistetentes.Count > 0)
                {
                    foreach (CadastroGrupoEconomicoTO.obterCadastroGrupoXClientesExistentes item in _clientesexistetentes)
                    {
                        ClietesExistentes = ClietesExistentes += item.NOMCLI.ToString() + ", \n";
                    }
                }
            }

            if (ClietesExistentes.Trim() != "")
                return ClietesExistentes;


            foreach (int cod in objInserir.CODCLIENTES)
            {
                if (objInserir.CODCLI != cod)
                {
                    itemSession.CODCLI = cod;
                    GrupoEconomicoSession.Grupo.Add(itemSession);
                    itemSession = new CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes();
                    // DAL.InserirCadastroGrupoEconomicoXClientes(objInserir);
                }
            }

            return "";
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO GRUPO ECONOMICO.");
            Utilitario.InsereLog(ex, "inserirCadastroContato", "CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel", "CadastroGrupoEconomicoBLL", "ERRO AO FAZER A INSERCAO DO GRUPO ECONOMICO.");
            throw;
        }
    }

    public List<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes> obterCadastroGrupoEconomicoXClientesExistentes(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel objPesquisar)
    {

        var DAL = new CadastroGrupoEconomicoDAL();
        List<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes> _listGravado = new List<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes>();
        CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes _item = new CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes();
        List<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes> _ListSession = new List<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes>();

        List<long> ListCosdigos = new List<long>();

        try
        {
            _listGravado = DAL.obterCadastroGrupoEconomicoXClientesExistentes(objPesquisar);

            if (GrupoEconomicoSession.Grupo != null)
            {
                if (GrupoEconomicoSession.Grupo.Count > 0)
                {
                    foreach (CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes _itemsession in GrupoEconomicoSession.Grupo)
                    {
                        ListCosdigos.Add(_itemsession.CODCLI);
                    }

                    _ListSession = DAL.obterClientesExistentesSession(ListCosdigos);
                    if (_ListSession != null)
                    {
                        if (_ListSession.Count > 0)
                        {
                            foreach (CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes _itemcliente in _ListSession)
                            {
                                _item.CODCLI = _itemcliente.CODCLI;
                                _item.NOMCLI = _itemcliente.NOMCLI;
                                _item.NUMCGCCLI = _itemcliente.NUMCGCCLI;
                                _item.CODGRPEMPCLI = _itemcliente.CODGRPEMPCLI;
                                _item.NOMGRPEMPCLI = _itemcliente.NOMGRPEMPCLI;
                                _item.STATUS = _itemcliente.STATUS;
                                _item.SERVICOS = _itemcliente.SERVICOS;
                                _item.NOMCID = _itemcliente.NOMCID;
                                _item.CODESTUNI = _itemcliente.CODESTUNI;

                                _listGravado.Add(_item);
                                _item = new CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes();
                            }
                        }
                    }
                }
            }

            return _listGravado;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO GRUPO ECONOMICO.");
            Utilitario.InsereLog(ex, "obterCadastroGrupoEconomico", "CadastroContatoApiModel.CadastroContatoApiModel", "CadastroGrupoEconomicoBLL", "ERRO AO FAZER A CONSULTA DO GRUPO ECONOMICO.");
            throw;
        }
    }

    public bool ApagarCadastroGrupoEconomicoXClientes(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel objInserir)
    {
        CadastroOportunidadeApiModel.CadastroOportunidadeApiModel _negociacao = new CadastroOportunidadeApiModel.CadastroOportunidadeApiModel();
        GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel _gestaocontas = new GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel();



        try
        {
            var DAL = new CadastroGrupoEconomicoDAL();
            List<long> ListCosdigos = new List<long>();
            var DALNEG = new CadastroOportunidadeDAL();
            var DALGESTAO = new GestaoContasDAL();


            if (GrupoEconomicoSession.Grupo != null)
            {
                if (GrupoEconomicoSession.Grupo.Count > 0)
                {
                    //Alteração feita pelo Marcus Caixeta dia 19/06/2018, foi inserido o ToList() no GrupoEconomicoSession.Grupo,
                    //e tirado o break para que todos itens da lista possam ser removidos. Logo depois foi criado um return
                    //para sair do metodo após a conclusão do for each.
                    foreach (CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes _itemsession in GrupoEconomicoSession.Grupo.ToList())
                    {
                        if (objInserir.CODCLIENTES.Exists(x => x == _itemsession.CODCLI))
                        {
                            GrupoEconomicoSession.Grupo.Remove(_itemsession);
                            //break;
                        }
                    }
                    return true;
                }
            }

            foreach (long cod in objInserir.CODCLIENTES)
            {
                //verifica e insere negociação 
                _negociacao.CODCLI = int.Parse(cod.ToString());

                DALNEG.AtualizaApagaGrupoNegocicaoServico(_negociacao);
                _negociacao = new CadastroOportunidadeApiModel.CadastroOportunidadeApiModel();


                //verifica e insere gstão de contas
                _gestaocontas.CODCLI = int.Parse(cod.ToString());

                DALGESTAO.AtualizaApagaGrupoGetaoServico(_gestaocontas);
                _gestaocontas = new GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel();

            }

            return DAL.apagarCadastroGrupoEconomicoXClientes(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO GRUPO ECONOMICO.");
            Utilitario.InsereLog(ex, "inserirCadastroContato", "CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel", "CadastroGrupoEconomicoBLL", "ERRO AO FAZER A INSERCAO DO GRUPO ECONOMICO.");
            throw;
        }
    }

    //Metodo para inserir o grupo economico, e os clientes ao mesmo tempo sem ter o codigo.
    public string inserirGrupoCliente(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel objInserir)
    {
        List<CadastroGrupoEconomicoTO.obterCadastroGrupoXClientesExistentes> _clientesexistetentes = new List<CadastroGrupoEconomicoTO.obterCadastroGrupoXClientesExistentes>();
        string ClientesExistentes = "";

        CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes _itemClietnes = new CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes();

        try
        {
            var DAL = new CadastroGrupoEconomicoDAL();

            using (TransactionScope Scope = new TransactionScope(TransactionScopeOption.Required, (new TimeSpan(0, 3, 0)))) // DE ATÉ 3 MINUTOS.
            {
                //Inserção Clientes Grupo Economico                
                _clientesexistetentes = DAL.BuscaClientesExistentes(objInserir.CODCLIENTES, objInserir.CODGRPEMPCLI);
                if (_clientesexistetentes != null)
                {
                    if (_clientesexistetentes.Count > 0)
                    {
                        foreach (CadastroGrupoEconomicoTO.obterCadastroGrupoXClientesExistentes item in _clientesexistetentes)
                        {
                            ClientesExistentes = ClientesExistentes += item.NOMCLI.ToString() + ", \n";
                        }
                    }
                }

                if (ClientesExistentes.Trim() != "")
                    return ClientesExistentes + ";" + objInserir.CODGRPEMPCLI;



                _clientesexistetentes = new List<CadastroGrupoEconomicoTO.obterCadastroGrupoXClientesExistentes>();
                _clientesexistetentes = DAL.BuscaClientesExistentes(objInserir.CODCLIENTES, -1);
                if (_clientesexistetentes != null)
                {
                    if (_clientesexistetentes.Count > 0)
                    {
                        foreach (CadastroGrupoEconomicoTO.obterCadastroGrupoXClientesExistentes item in _clientesexistetentes)
                        {
                            ClientesExistentes = ClientesExistentes += item.NOMCLI.ToString() + ", \n";
                        }
                    }
                }

                if (ClientesExistentes.Trim() != "")
                    return ClientesExistentes + ";" + objInserir.CODGRPEMPCLI;



                var LISTCODCLIREPETIDOS = new List<int>();
                var hashSetCliente = new HashSet<long>(objInserir.CODCLIENTES);

                if (GrupoEconomicoSession.Grupo == null)
                    GrupoEconomicoSession.Grupo = new List<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes>();


                if (GrupoEconomicoSession.Grupo != null)
                {
                    foreach (int cod in hashSetCliente)
                    {
                        _itemClietnes.CODCLI = cod;
                        _itemClietnes.CODGRPEMPCLI = objInserir.CODGRPEMPCLI;

                        GrupoEconomicoSession.Grupo.Add(_itemClietnes);
                        _itemClietnes = new CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes();
                    }
                }

                //foreach (int cod in hashSetCliente)
                //{
                //    objInserir.CODCLI = cod;
                //    DAL.InserirCadastroGrupoEconomicoXClientes(objInserir);

                //}

                Scope.Complete();
            }

        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO GRUPO CLIENTE.");
            Utilitario.InsereLog(ex, "inserirGrupoCliente", "CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel", "CadastroGrupoEconomicoBLL", "ERRO AO FAZER A INSERCAO DO GRUPO CLIENTE.");
            throw;
        }

        return ";" + objInserir.CODGRPEMPCLI;
    }

    public bool ativarCadastroGrupoEconomico(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroGrupoEconomicoDAL();
            return DAL.ativarCadastroGrupoEconomico(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATIVACAO DO CADASTRO GRUPO ECONOMICO.");
            Utilitario.InsereLog(ex, "ativarCadastroConsultor", "CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel", "CadastroGrupoEconomicoBLL", "ERRO AO FAZER A ATIVACAO DO CADASTRO GRUPO ECONOMICO.");
            throw;
        }
    }

    public bool desativarCadastroGrupoEconomico(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroGrupoEconomicoDAL();
            return DAL.desativarCadastroGrupoEconomico(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATIVACAO DO CADASTRO GRUPO ECONOMICO.");
            Utilitario.InsereLog(ex, "desativarCadastroGrupoEconomico", "CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel", "CadastroGrupoEconomicoBLL", "ERRO AO FAZER A ATIVACAO DO CADASTRO GRUPO ECONOMICO.");
            throw;
        }
    }


    public List<CadastroGrupoEconomicoTO.obterCadastroGrupoFaturamentoServico> obterCadastroGrupoFaturamentoServico(GrupoFaturamentoApiModel.GrupoFaturamentoApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroGrupoEconomicoDAL();
            return DAL.obterCadastroGrupoFaturamentoServico(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO GRUPO ECONOMICO.");
            Utilitario.InsereLog(ex, "obterCadastroGrupoEconomico", "CadastroContatoApiModel.CadastroContatoApiModel", "CadastroGrupoEconomicoBLL", "ERRO AO FAZER A CONSULTA DO GRUPO ECONOMICO.");
            throw;
        }
    }

    public string AtualizarContaCorrente(GrupoFaturamentoApiModel.GrupoFaturamentoContaCorrenteApiModel objAtualizar)
    {
        try
        {
            string _result = "";

            var DAL = new CadastroGrupoEconomicoDAL();

            if (DAL.AtualizarContaCorrente(objAtualizar))
                _result = "1";
            else
                _result = "2";

            return _result;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO GRAVAR CONTA CORRENTE GRUPO FATURAMENTO.");
            Utilitario.InsereLog(ex, "AtualizarContaCorrente", "GrupoFaturamentoApiModel.GrupoFaturamentoContaCorrenteApiModel", "CadastroGrupoEconomicoBLL", "ERRO AO FAZER A CONSULTA DO GRUPO ECONOMICO.");
            throw;
        }

    }
}