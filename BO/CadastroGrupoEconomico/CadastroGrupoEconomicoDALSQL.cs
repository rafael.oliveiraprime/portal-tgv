﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class CadastroGrupoEconomicoDALSQL
{
    public string obterCadastroGrupoEconomico()
    {
        StringBuilder strBld = new StringBuilder(@"  SELECT GP.CODGRPEMPCLI,
                                                            GP.CODGRPEMPCLI CODGRPEMPCLICHECK,
                                                            GP.NOMGRPEMPCLI,
                                                            GP.INDFATGRP,
                                                            DECODE(GP.INDFATGRP, 1, 'SIM', 2, 'NÃO') NOMINDFATGRP,
                                                            GP.CODCLIGRPFAT,
                                                            (CASE CL.NUMCGCCLI WHEN CL.NUMCGCCLI THEN (SUBSTR(NUMCGCCLI, 1, 2) || '.' || SUBSTR(NUMCGCCLI, 3, 3) || '.' || SUBSTR(NUMCGCCLI, 6, 3) || '/' || SUBSTR(NUMCGCCLI, 9,4) || '-' || SUBSTR(NUMCGCCLI, 13,2) ) ELSE '' END) AS CNPJ,    
                                                            CL.NOMCLI AS RAZAOSOCIAL,
                                                            TRIM(FNC_CAD.NOMFNC) FNCCAD,
                                                            TO_CHAR(GP.DATCAD,'DD/MM/YYYY') DATFNCCAD,
                                                            TRIM(FNC_ALT.NOMFNC) FNCALT,
                                                            TO_CHAR(GP.DATALT,'DD/MM/YYYY') DATFNCALT,
                                                            CASE GP.DATDST WHEN GP.DATDST THEN TRIM(FNC_DST.NOMFNC) ELSE NULL END FNCDST,
                                                            CASE GP.DATDST WHEN GP.DATDST THEN TO_CHAR(GP.DATDST,'DD/MM/YYYY') ELSE NULL END DATFNCDST,
                                                            CASE GP.CODFNCDST WHEN GP.CODFNCDST THEN 'DESATIVADO' ELSE 'ATIVADO' END STATUS 
                                                            ,GP.INDDBTAUTPGT
                                                            ,GP.CODBCO
                                                            ,GP.CODAGEBCO
                                                            ,GP.NUMDIGVRFAGE
                                                            ,GP.NUMCNTCRRCTT
                                                            ,GP.NUMDIGVRFCNTCRR
                                                       FROM MRT.CADGRPEMPTGV GP
                                                       LEFT JOIN MRT.T0100361 FNC_CAD ON FNC_CAD.CODFNC = GP.CODFNCCAD
                                                       LEFT JOIN MRT.T0100361 FNC_ALT ON FNC_ALT.CODFNC = GP.CODFNCALT
                                                       LEFT JOIN MRT.T0100361 FNC_DST ON FNC_DST.CODFNC = GP.CODFNCDST 
                                                       LEFT JOIN MRT.T0100043 CL ON(GP.CODCLIGRPFAT = CL.CODCLI)
                                                      WHERE 1 = 1
                                                        AND CODGRPEMPCLI = :CODGRPEMPCLI
                                                        AND NOMGRPEMPCLI LIKE '%' || UPPER(:NOMGRPEMPCLI) || '%'
                                                        AND INDFATGRP = :INDFATGRP
                                                ");
        //
        return strBld.ToString();
    }

    public string inserirCadastroGrupoEconomico()
    {

        StringBuilder strBld = new StringBuilder(@"
                                                     INSERT INTO MRT.CADGRPEMPTGV (CODGRPEMPCLI, NOMGRPEMPCLI, INDFATGRP, CODCLIGRPFAT, CODFNCCAD, DATCAD, CODFNCALT, DATALT)
                                                                           VALUES ((SELECT COALESCE(MAX(CODGRPEMPCLI),0)+1 FROM MRT.CADGRPEMPTGV), 
                                                                                    UPPER(:NOMGRPEMPCLI), 
                                                                                    :INDFATGRP, 
                                                                                    :CODCLIGRPFAT, 
                                                                                    :CODFNC, 
                                                                                    SYSDATE,
                                                                                    :CODFNC,
                                                                                    SYSDATE)
        ");

        return strBld.ToString();
    }

    public string alterarCadastroGrupoEconomico()
    {

        StringBuilder strBld = new StringBuilder(@"
                                                        UPDATE MRT.CADGRPEMPTGV SET NOMGRPEMPCLI = UPPER(:NOMGRPEMPCLI),
                                                                                    INDFATGRP = :INDFATGRP,
                                                                                    CODCLIGRPFAT = :CODCLIGRPFAT,
                                                                                    CODFNCALT = :CODFNC,
                                                                                    DATALT = SYSDATE                                                                                    
                                                        WHERE CODGRPEMPCLI = :CODGRPEMPCLI
        ");

        return strBld.ToString();

    }


    public string obterCadastroGrupoEconomicoMartins(int _gruporede)
    {
        StringBuilder strBld = new StringBuilder(@"  SELECT C.CODCLI, 
                                                            C.NOMCLI, 
                                                            C.NUMCGCCLI, 
                                                            TRIM(G.DESGRPEMPCLI) || ' - ' || G.CODGRPEMPCLI DESGRPEMPCLI
                                                      FROM MRT.T0100043 C 
                                                      LEFT JOIN MRT.T0144040 R ON C.CODCLI = R.CODCLI AND R.DATHRADST IS NULL
                                                      LEFT JOIN MRT.T0143966 G ON G.CODGRPEMPCLI = R.CODGRPEMPCLI 
                                                     WHERE C.DATDSTCLI IS NULL                                       
                                                       AND C.CODCLI = :CODCLI
                                                       AND C.NOMCLI LIKE '%' || UPPER(:NOMCLI) || '%' 
                                                       AND C.NUMCGCCLI = :CNPJ                                                    
                                                ");

        if (_gruporede == 1)
        {
            strBld.AppendLine(" AND G.CODGRPEMPCLI IS NOT NULL ");
        }

        strBld.AppendLine("ORDER BY C.CODCLI");

        return strBld.ToString();

    }


    public string inserirCadastroGrupoEconomicoXClientes()
    {

        StringBuilder strBld = new StringBuilder(@"
                                                     INSERT INTO MRT.RLCCLIGRPEMPTGV (CODGRPEMPCLI, CODCLI)
                                                                              VALUES (:CODGRPEMPCLI, :CODCLI)
        ");

        return strBld.ToString();
    }


    public string buscaClientesExistentes(List<long> _ListClientes)
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT CL.CODCLI, C.NOMCLI, C.NUMCGCCLI, CAD.CODGRPEMPCLI, CAD.NOMGRPEMPCLI, ' ' DESGRPEMPCLI  
                                        FROM MRT.RLCCLIGRPEMPTGV CL
                                        INNER JOIN MRT.T0100043 C ON(CL.CODCLI = C.CODCLI)
                                        INNER JOIN MRT.CADGRPEMPTGV CAD ON(CAD.CODGRPEMPCLI = CL.CODGRPEMPCLI)
                                        WHERE CL.CODCLI IN (#LISTCLIENTES)
                                          AND CAD.CODGRPEMPCLI = :CODGRPEMPCLI 
                                        ".Replace("#LISTCLIENTES", ToList(_ListClientes)));
        return strBld.ToString();
    }

    public string obterCadastroGrupoEconomicoXClientesExistentes()
    {
        return @"SELECT CL.CODCLI AS CODCLICHECK
                        ,CL.CODCLI
                        , C.NOMCLI
                        , C.NUMCGCCLI
                        , CAD.CODGRPEMPCLI
                        , CAD.NOMGRPEMPCLI
                        , MIN(TRIM(G.DESGRPEMPCLI)) DESGRPEMPCLI   
                        , CASE WHEN CC.CODSITCLI = 1 THEN 'EM IMPLANTAÇÃO'
                                WHEN CC.CODSITCLI = 2 THEN 'ATIVO'
                                WHEN CC.CODSITCLI = 3 THEN 'SUSPENSO'
                                WHEN CC.CODSITCLI = 4 THEN 'CANCELADO' END STATUS      
                         , (SELECT WM_CONCAT(S.DESSVCTGV) FROM MRT.CADCLISVCTGV CS INNER JOIN MRT.CADSVCTGV S ON S.CODSVCTGV = CS.CODSVCTGV WHERE CS.CODCLI = CL.CODCLI AND INDSTAATV = 2 ) AS SERVICOS
                        , CID.NOMCID
                        , CID.CODESTUNI      
                    FROM MRT.RLCCLIGRPEMPTGV CL
                   INNER JOIN MRT.T0100043 C ON(CL.CODCLI = C.CODCLI)
                   INNER JOIN MRT.CADGRPEMPTGV CAD ON(CAD.CODGRPEMPCLI = CL.CODGRPEMPCLI)
                    LEFT JOIN MRT.T0144040 R ON C.CODCLI = R.CODCLI AND R.DATHRADST IS NULL
                    LEFT JOIN MRT.T0143966 G ON G.CODGRPEMPCLI = R.CODGRPEMPCLI
                    LEFT JOIN MRT.CADCLITGV CC ON CC.CODCLI = CL.CODCLI 
                   INNER JOIN MRT.T0138938 END ON C.CodCli = END.Codcli AND END.TIPENDCLI = (SELECT MAX(E2.TIPENDCLI)
                                                                                                       FROM MRT.T0138938 E2
                                                                                                       WHERE E2.CODCLI = END.CODCLI
                                                                                                       AND E2.TIPENDCLI IN (0,1,2))
                   INNER JOIN MRT.T0100027 BAI ON END.CodBai    = BAI.Codbai
                   INNER JOIN MRT.T0100035 CID ON BAI.CodCid    = CID.Codcid 
                   WHERE CAD.CODGRPEMPCLI = :CODGRPEMPCLI
                   GROUP BY CL.CODCLI
                            , C.NOMCLI
                            , C.NUMCGCCLI
                            , CAD.CODGRPEMPCLI
                            , CAD.NOMGRPEMPCLI 
                            , CC.CODSITCLI 
                            , CID.NOMCID
                            , CID.CODESTUNI ";

    }

    public string buscaClientesExistentesSession(List<long> _ListClientes)
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT CL.CODCLI AS CODCLICHECK, CL.CODCLI, CL.NOMCLI, CL.NUMCGCCLI, 0 AS CODGRPEMPCLI, '' AS NOMGRPEMPCLI, '' AS DESGRPEMPCLI
                                            , CASE WHEN CC.CODSITCLI = 1 THEN 'EM IMPLANTAÇÃO'
                                                    WHEN CC.CODSITCLI = 2 THEN 'ATIVO'
                                                    WHEN CC.CODSITCLI = 3 THEN 'SUSPENSO'
                                                    WHEN CC.CODSITCLI = 4 THEN 'CANCELADO' END STATUS      
                                             , (SELECT WM_CONCAT(S.DESSVCTGV) FROM MRT.CADCLISVCTGV CS INNER JOIN MRT.CADSVCTGV S ON S.CODSVCTGV = CS.CODSVCTGV WHERE CS.CODCLI = CL.CODCLI AND INDSTAATV = 2 ) AS SERVICOS
                                             , CID.NOMCID
                                             , CID.CODESTUNI      
                                        FROM MRT.T0100043 CL
                                        LEFT JOIN MRT.CADCLITGV CC ON CC.CODCLI = CL.CODCLI 
                                       INNER JOIN MRT.T0138938 END ON CL.CodCli = END.Codcli AND END.TIPENDCLI = (SELECT MAX(E2.TIPENDCLI)
                                                                                                       FROM MRT.T0138938 E2
                                                                                                       WHERE E2.CODCLI = END.CODCLI
                                                                                                       AND E2.TIPENDCLI IN (0,1,2))
                                       INNER JOIN MRT.T0100027 BAI ON END.CodBai    = BAI.Codbai
                                       INNER JOIN MRT.T0100035 CID ON BAI.CodCid    = CID.Codcid   
                                       WHERE CL.CODCLI IN (#LISTCLIENTES)
                                        ".Replace("#LISTCLIENTES", ToList(_ListClientes)));


        return strBld.ToString();
    }


    public string obterCadastroGrupoEconomicoXClientesSessao()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT CL.CODCLI, C.NOMCLI, C.NUMCGCCLI, CAD.CODGRPEMPCLI, CAD.NOMGRPEMPCLI   
                                        FROM MRT.RLCCLIGRPEMPTGV CL
                                       INNER JOIN MRT.T0100043 C ON(CL.CODCLI = C.CODCLI)
                                       INNER JOIN MRT.CADGRPEMPTGV CAD ON(CAD.CODGRPEMPCLI = CL.CODGRPEMPCLI)
                                       WHERE CAD.CODGRPEMPCLI = :CODGRPEMPCLI ");
        return strBld.ToString();
    }


    public string apagarCadastroGrupoEconomicoXClientes(List<long> _ListClientes)
    {

        StringBuilder strBld;

        strBld = new StringBuilder(@" DELETE FROM MRT.RLCCLIGRPEMPTGV CL
                                       WHERE CL.CODCLI IN (#LISTCLIENTES) ".Replace("#LISTCLIENTES", ToList(_ListClientes)));

        return strBld.ToString();
    }

    public string obterMaximoGrupoEconomico()
    {
        return @" SELECT COALESCE(MAX(CODGRPEMPCLI),0) AS CODGRPEMPCLI 
                               FROM MRT.CADGRPEMPTGV";
    }

    public string ativarCadastroGrupoEconomico(List<long> LISTCODGRPEMPCLI)
    {

        return @"
                 UPDATE MRT.CADGRPEMPTGV SET                 
                 CODFNCDST = NULL,
                 DATDST = NULL
                 WHERE CODGRPEMPCLI IN(#LISTCODGRPEMPCLI)
                ".Replace("#LISTCODGRPEMPCLI", Utilitario.ToListStr(LISTCODGRPEMPCLI));
    }

    public string desativarCadastroGrupoEconomico(List<long> LISTCODGRPEMPCLI)
    {
        return @"
                 UPDATE MRT.CADGRPEMPTGV SET                                 
                 CODFNCDST = :CODFNC,
                 DATDST = SYSDATE
                 WHERE CODGRPEMPCLI IN(#LISTCODGRPEMPCLI)
                ".Replace("#LISTCODGRPEMPCLI", Utilitario.ToListStr(LISTCODGRPEMPCLI));
    }


    public string BuscaGrupoEconomico()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT CODGRPEMPCLI
                                             ,NOMGRPEMPCLI
                                             ,INDFATGRP
                                             ,CODCLIGRPFAT
                                        FROM MRT.CADGRPEMPTGV
                                       WHERE CODGRPEMPCLI = :CODGRPEMPCLI ");
        return strBld.ToString();
    }

    public string BuscaGrupoEconomicoAtivo()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT CODGRPEMPCLI
                                             ,NOMGRPEMPCLI
                                        FROM MRT.CADGRPEMPTGV
                                       WHERE CODGRPEMPCLI = :CODGRPEMPCLI 
                                             AND DATDST IS NULL");
        return strBld.ToString();
    }

    public string ExisteGrupoCli()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT COUNT(CODGRPEMPCLI) AS EXISTE
                                        FROM MRT.RLCCLIGRPEMPTGV
                                       WHERE CODCLI = :CODCLI ");
        return strBld.ToString();
    }



    public string VerificaSeEDebito()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@"SELECT CODGRPEMPCLI, CC.INDDBTAUTPGT
                                       FROM MRT.RLCCLIGRPEMPTGV RL
                                      INNER JOIN MRT.CADCLITGV CC ON RL.CODCLI = CC.CODCLI
                                      WHERE CODGRPEMPCLI = :CODGRPEMPCLI
                                      GROUP BY CODGRPEMPCLI, CC.INDDBTAUTPGT
                                   ");
        return strBld.ToString();

    }



    public string BuscaRelacaoGrupoCliente()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@"SELECT GP.CODGRPEMPCLI, RLGP.CODCLI
                                       FROM MRT.CADGRPEMPTGV GP 
                                      INNER JOIN MRT.RLCCLIGRPEMPTGV RLGP ON (GP.CODGRPEMPCLI = RLGP.CODGRPEMPCLI)
                                      WHERE GP.CODGRPEMPCLI = :CODGRPEMPCLI
                                   ");
        return strBld.ToString();

    }

    public string ExisteLojaCentralizadora()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT COUNT(CODGRPEMPCLI) AS EXISTE FROM MRT.CADGRPEMPTGV WHERE CODGRPEMPCLI <> :CODGRPEMPCLI AND CODCLIGRPFAT = :CODCLIGRPFAT ");
        return strBld.ToString();
    }

    public string ExisteCentralizadoraGrupoCli()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT COUNT(CODGRPEMPCLI) AS EXISTE
                                        FROM MRT.RLCCLIGRPEMPTGV
                                       WHERE CODCLI = :CODCLI 
                                         AND CODGRPEMPCLI = :CODGRPEMPCLI ");
        return strBld.ToString();
    }

    public string obterCadastroGrupoFaturamentoServico()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@"  SELECT CLS.CODCLI
                                              ,C.NOMCLI 
                                              ,C.NUMCGCCLI
                                              ,CLS.CODSVCTGV
                                              ,S.DESSVCTGV
                                              ,CASE WHEN CLS.INDSTAATV = 1 THEN 'EM IMPLANTAÇÃO'
                                                    WHEN CLS.INDSTAATV = 2 THEN 'ATIVO'
                                                    WHEN CLS.INDSTAATV = 3 THEN 'SUSPENSO'
                                                    WHEN CLS.INDSTAATV = 4 THEN 'CANCELADO'  END STATUS                
                                         FROM MRT.CADCLISVCTGV CLS
                                        INNER JOIN MRT.T0100043 C ON(CLS.CODCLI = C.CODCLI)
                                        INNER JOIN MRT.CADSVCTGV S ON(CLS.CODSVCTGV = S.CODSVCTGV)
                                        WHERE CLS.CODGRPEMPFAT = :CODGRPEMPCLI
                                        ORDER BY C.NOMCLI
                                   ");

        return strBld.ToString();
    }


    public string AtualizarContaCorrente()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@"  UPDATE MRT.CADGRPEMPTGV SET INDDBTAUTPGT = :INDDBTAUTPGT,
                                                                    CODBCO = :CODBCO,
                                                                    CODAGEBCO = :CODAGEBCO,
                                                                    NUMDIGVRFAGE = :NUMDIGVRFAGE,
                                                                    NUMCNTCRRCTT = :NUMCNTCRRCTT,
                                                                    NUMDIGVRFCNTCRR = :NUMDIGVRFCNTCRR
                                                      WHERE CODGRPEMPCLI = :CODGRPEMPCLI
                                                 ");

        return strBld.ToString();
    }


    public string ToList(List<long> LISTCODSVCTGV)
    {
        string retr = string.Empty;
        bool firstTime = true;

        foreach (long Item in LISTCODSVCTGV)
        {
            if (firstTime)
            {
                firstTime = false;

                retr = (Item.ToString());
            }
            else
            {
                retr = (string.Format("{0}, {1}", retr, Item));
            }

        }

        return retr.Trim();
    }
}