﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class CadastroServicoDALSQL
{
    public string obterCadastroServico(int flgAtivo, int codSvcTgv)
    {
        StringBuilder strBld = new StringBuilder(@"
                 SELECT SVC.CODSVCTGV, 
                 SVC.CODSVCTGV CODSVCTGVCHECK,
                 SVC.DESSVCTGV, 
                 SVC.VLRSVC ,                       
                 SVC.NUMPTOSVC,    
                 SVC.TIPSVC,
                 CASE WHEN SVC.TIPSVC = 1 THEN 'MENSAL' WHEN SVC.TIPSVC = 2 THEN 'EVENTUAL' END DESTIPSVC,
                 CASE WHEN SVC.CODFNCDST IS NOT NULL THEN 'DESATIVADO' ELSE 'ATIVADO' END STATUS,                 
                 TRIM(FNC_CAD.NOMFNC) FNCCAD,
                 TO_CHAR(SVC.DATCAD,'DD/MM/YYYY') DATFNCCAD,
                 TRIM(FNC_ALT.NOMFNC) FNCALT,
                 TO_CHAR(SVC.DATALT,'DD/MM/YYYY') DATFNCALT,
                 CASE SVC.DATDST WHEN SVC.DATDST THEN TRIM(FNC_DST.NOMFNC) ELSE NULL END FNCDST,
                 CASE SVC.DATDST WHEN SVC.DATDST THEN TO_CHAR(SVC.DATDST,'DD/MM/YYYY') ELSE NULL END DATFNCDST,
                 TRIM(SUBSTR(CDOANXCTTCLI, 1, INSTR(CDOANXCTTCLI, ';')-1)) AS CAMINHOARQUIVO,
                 ' ' AS CDOANXCTTCLI
                 ,NVL((SELECT WM_CONCAT(DOC.CODDOCARZ) FROM MRT.CADDOCARZCLITGV DOC LEFT JOIN MRT.RLCSVCDOCTGV RLC ON RLC.CODDOCARZ = DOC.CODDOCARZ WHERE RLC.CODSVCTGV = SVC.CODSVCTGV), ' ') AS CODBANDEIRAS
                 FROM MRT.CADSVCTGV SVC 
                 LEFT JOIN MRT.T0100361 FNC_CAD ON FNC_CAD.CODFNC = SVC.CODFNCCAD
                 LEFT JOIN MRT.T0100361 FNC_ALT ON FNC_ALT.CODFNC = SVC.CODFNCALT
                 LEFT JOIN MRT.T0100361 FNC_DST ON FNC_DST.CODFNC = SVC.CODFNCDST     
                 WHERE 1 = 1
                 AND SVC.CODSVCTGV = :CODSVCTGV
                 AND SVC.TIPSVC = :TIPSVC
                ");

        if (flgAtivo == 1)
        {    
                strBld.AppendLine(" AND DATDST IS NULL ");
        }
        if (flgAtivo == 2)
        {            
                strBld.AppendLine(" AND DATDST IS NOT NULL ");
        }

        strBld.AppendLine("ORDER BY STATUS, CODSVCTGV");

        return strBld.ToString();
    }    

    public string inserirCadastroServico()
    {
        return @"
                 INSERT INTO MRT.CADSVCTGV (CODSVCTGV, DESSVCTGV, TIPSVC, VLRSVC, NUMPTOSVC, CODFNCCAD, DATCAD, CODFNCALT, DATALT)
                 VALUES ((SELECT COALESCE(MAX(CODSVCTGV),0)+1 FROM MRT.CADSVCTGV),
                 UPPER(:DESSVCTGV), :TIPSVC, TO_NUMBER(:VLRSVC), TO_NUMBER(:NUMPTOSVC), :CODFNCCAD, 
                 SYSDATE, :CODFNCALT, SYSDATE)
                ";
    }

    public string alterarCadastroServico()
    {
        return @"
                 UPDATE MRT.CADSVCTGV SET
                 DESSVCTGV = UPPER(:DESSVCTGV),
                 TIPSVC = :TIPSVC,
                 VLRSVC = TO_NUMBER(:VLRSVC),
                 NUMPTOSVC = TO_NUMBER(:NUMPTOSVC),
                 CODFNCALT = :CODFNCALT,                 
                 CDOANXCTTCLI = :CDOANXCTTCLI,
                 DATALT = SYSDATE
                 WHERE CODSVCTGV = :CODSVCTGV
                ";                  
    }

    public string ativarCadastroServico(List<long> LISTCODSVC)
    {   
        
        return @"
                 UPDATE MRT.CADSVCTGV SET                 
                 CODFNCDST = NULL,
                 DATDST = NULL
                 WHERE CODSVCTGV IN(#LISTCODSVCTGV)
                ".Replace("#LISTCODSVCTGV", Utilitario.ToListStr(LISTCODSVC));            
    }

    public string desativarCadastroServico(List<long> LISTCODSVC)
    {     
        return @"
                 UPDATE MRT.CADSVCTGV SET                                 
                 CODFNCDST = :CODFNCDST,
                 DATDST = SYSDATE
                 WHERE CODSVCTGV IN(#LISTCODSVCTGV)
                ".Replace("#LISTCODSVCTGV", Utilitario.ToListStr(LISTCODSVC));
    }

    public string VisualizarTemplate()
    {
        StringBuilder strBld = new StringBuilder(@" SELECT CODSVCTGV, NVL(CDOANXCTTCLI, ' ') AS CDOANXCTTCLI FROM MRT.CADSVCTGV WHERE CODSVCTGV = :CODSVCTGV ");

        return strBld.ToString();
    }

    public string MontarBandeira()
    {
        StringBuilder strBld = new StringBuilder(@" SELECT CODDOCARZ, DESDOCARZ FROM MRT.CADDOCARZCLITGV ");

        return strBld.ToString();
    }

    public string deleteRelacaoServicoDocumento()
    {
        return @" DELETE FROM MRT.RLCSVCDOCTGV WHERE CODSVCTGV = :CODSVCTGV ";
    }


    public string inserirRelacaoServicoDocumento()
    {
        return @" INSERT INTO MRT.RLCSVCDOCTGV (CODSVCTGV, CODDOCARZ) VALUES (:CODSVCTGV, :CODDOCARZ) ";
    }

    public string SelectMaxCodigoServico()
    {
        StringBuilder strBld = new StringBuilder(@" SELECT MAX(CODSVCTGV) AS CODSVCTGV FROM MRT.CADSVCTGV ");

        return strBld.ToString();
    }




}