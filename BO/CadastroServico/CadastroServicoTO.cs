﻿using System.Collections.Generic;

namespace CadastroServicoTO
{
    public class obterCadastroServico
    {
        public int CODSVCTGV { get; set; }

        public int CODSVCTGVCHECK { get; set; }

        public string DESSVCTGV { get; set; }

        public int TIPSVC { get; set; }

        public string DESTIPSVC { get; set; }

        public decimal VLRSVC { get; set; }

        public decimal NUMPTOSVC { get; set; }

        public string FNCCAD { get; set; }

        public string DATFNCCAD { get; set; }

        public string FNCALT { get; set; }

        public string DATFNCALT { get; set; }

        public string FNCDST { get; set; }

        public string DATFNCDST { get; set; }
        
        public string STATUS { get; set; }

        public string CAMINHOARQUIVO { get; set;}

        public string CDOANXCTTCLI { get; set; }

        public string CODBANDEIRAS { get; set; }
    }

    public class obterCadastroServicoTemplate
    {
        public int CODSVCTGV { get; set; }

        public string CDOANXCTTCLI { get; set; }

    }

    public class obterCadastroBandeiras
    {
        public int CODDOCARZ { get; set; }

        public string DESDOCARZ { get; set; }

    }

}