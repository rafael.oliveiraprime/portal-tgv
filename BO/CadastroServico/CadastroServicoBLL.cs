﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Transactions;


public class CadastroServicoBLL
{
    public List<CadastroServicoTO.obterCadastroServico> obterCadastroServico(CadastroServicoApiModel.CadastroServicoApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroServicoDAL();
            return DAL.obterCadastroServico(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CADASTRO SERVIÇO.");
            Utilitario.InsereLog(ex, "obterCadastroServico", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoBLL", "ERRO AO FAZER A CONSULTA DO CADASTRO SERVIÇO.");
            throw;
        }
    }

    public bool inserirCadastroServico(CadastroServicoApiModel.CadastroServicoApiModel objInserir)
    {
        bool _inseriu = false;
        Int64 _codgio = 0;

        try
        {
            var DAL = new CadastroServicoDAL();

            _inseriu = DAL.inserirCadastroServico(objInserir);

            if (_inseriu)
            {
                _codgio = DAL.SelectMaxCodigoServico();
                if (objInserir.CODDOCS.Trim() != "")
                {
                    foreach(string _coddocs in objInserir.CODDOCS.Split(','))
                    {
                        DAL.inserirRelacaoServicoDocumento(_codgio, Int64.Parse(_coddocs)); 
                    }
                }  
            }

            return true;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DO CADASTRO SERVICO.");
            Utilitario.InsereLog(ex, "inserirCadastroServico", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoBLL", "ERRO AO FAZER A INSERCAO DO CADASTRO SERVICO.");
            throw;
        }
    }

    public bool alterarCadastroServico(CadastroServicoApiModel.CadastroServicoApiModel objAlterar)
    {
        bool _inseriu = false;
        Int64 _codgio = 0;


        try
        {
            var DAL = new CadastroServicoDAL();

            using (TransactionScope Scope = new TransactionScope(TransactionScopeOption.Required, (new TimeSpan(0, 5, 0)))) // DE ATÉ 5 MINUTOS.
            {
                _inseriu = DAL.alterarCadastroServico(objAlterar);
                if (_inseriu)
                {
                    _codgio = objAlterar.CODSVCTGV;

                    DAL.deleteRelacaoServicoDocumento(_codgio);
                    if (objAlterar.CODDOCS.Trim() != "0")
                    {
                        foreach (string _coddocs in objAlterar.CODDOCS.Split(','))
                        {
                            DAL.inserirRelacaoServicoDocumento(_codgio, Int64.Parse(_coddocs));
                        }
                    }
                }
                Scope.Complete();  
            }
            return true;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERACAO DO CADASTRO SERVICO.");
            Utilitario.InsereLog(ex, "alterarCadastroServico", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoBLL", "ERRO AO FAZER A ALTERACAO DO CADASTRO SERVICO.");
            throw;
        }
    }

    public bool ativarCadastroServico(CadastroServicoApiModel.CadastroServicoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroServicoDAL();
            return DAL.ativarCadastroServico(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATIVACAO DO CADASTRO SERVICO.");
            Utilitario.InsereLog(ex, "ativarCadastroServico", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoBLL", "ERRO AO FAZER A ATIVACAO DO CADASTRO SERVICO.");
            throw;
        }
    }

    public bool desativarCadastroServico(CadastroServicoApiModel.CadastroServicoApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroServicoDAL();
            return DAL.desativarCadastroServico(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ATIVACAO DO CADASTRO SERVICO.");
            Utilitario.InsereLog(ex, "desativarCadastroServico", "CadastroServicoApiModel.CadastroServicoApiModel", "CadastroServicoBLL", "ERRO AO FAZER A DESATIVACAO DO CADASTRO SERVICO.");
            throw;
        }
    }

    public string VisualizarTemplate(CadastroServicoApiModel.CadastroServicoApiModel objPesquisar)
    {

        CadastroServicoTO.obterCadastroServicoTemplate _item = new CadastroServicoTO.obterCadastroServicoTemplate();
        string _arquivo = "";

        try
        {
            var DAL = new CadastroServicoDAL();
            _item = DAL.VisualizarTemplate(objPesquisar);

            if (_item != null)
            {
                if (_item.CDOANXCTTCLI != null)
                    _arquivo = _item.CDOANXCTTCLI;
            }

            return _arquivo;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO VISUALIZAR O TEMPLATE DE CADASTRO SERVICO.");
            Utilitario.InsereLog(ex, "VisualizarTemplate", "CadastroServicoApiModel.CadastroServicoApiModel objPesquisarl", "CadastroServicoBLL", "ERRO AO VISUALIZAR O TEMPLATE DE CADASTRO SERVICO.");
            throw;
        }
    }

    
    public List<CadastroServicoTO.obterCadastroBandeiras> MontarBandeira()
    {
        try
        {
            var DAL = new CadastroServicoDAL();
            return DAL.MontarBandeira();
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DAS BANDEIRAS.");
            Utilitario.InsereLog(ex, "MontarBandeira", "", "CadastroServicoBLL", "ERRO AO FAZER A CONSULTA DAS BANDEIRAS.");
            throw;
        }
    }


}