﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CadastroServicoDAL : DAL
{
    public List<CadastroServicoTO.obterCadastroServico> obterCadastroServico(CadastroServicoApiModel.CadastroServicoApiModel objPesquisar)
    {
        var DALSQL = new CadastroServicoDALSQL();
        string cmdSql = DALSQL.obterCadastroServico(objPesquisar.PRDATI, objPesquisar.CODSVCTGV);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (objPesquisar.CODSVCTGV == 0){
            objPesquisar.CODSVCTGV = -1;  
        }
        dbCommand.AddWithValue("CODSVCTGV", objPesquisar.CODSVCTGV);
        dbCommand.AddWithValue("TIPSVC", objPesquisar.TIPSVC);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroServicoTO.obterCadastroServico>();
    }   

    public bool inserirCadastroServico(CadastroServicoApiModel.CadastroServicoApiModel objInserir)
    {
        var DALSQL = new CadastroServicoDALSQL();
        string cmdSql = DALSQL.inserirCadastroServico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objInserir.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        objInserir.CODFNCCAD = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //
        dbCommand.AddWithValue("CODSVCTGV", objInserir.CODSVCTGV);
        dbCommand.AddWithValue("DESSVCTGV", objInserir.DESSVCTGV);
        dbCommand.AddWithValue("TIPSVC", objInserir.TIPSVC);
        dbCommand.AddWithValue("VLRSVC", decimal.Parse(objInserir.VLRSVC));
        dbCommand.AddWithValue("NUMPTOSVC", decimal.Parse(objInserir.NUMPTOSVC));
        dbCommand.AddWithValue("CODFNCCAD", objInserir.CODFNCCAD);  
        dbCommand.AddWithValue("CODFNCALT", objInserir.CODFNCALT);        
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool alterarCadastroServico(CadastroServicoApiModel.CadastroServicoApiModel objAlterar)
    {
        var DALSQL = new CadastroServicoDALSQL();
        string cmdSql = DALSQL.alterarCadastroServico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCALT = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;        
        //              
        dbCommand.AddWithValue("CODSVCTGV", objAlterar.CODSVCTGV);
        dbCommand.AddWithValue("DESSVCTGV", objAlterar.DESSVCTGV);
        dbCommand.AddWithValue("TIPSVC", objAlterar.TIPSVC);
        dbCommand.AddWithValue("VLRSVC", decimal.Parse(objAlterar.VLRSVC));
        dbCommand.AddWithValue("NUMPTOSVC", decimal.Parse(objAlterar.NUMPTOSVC));       
        dbCommand.AddWithValue("CODFNCALT", objAlterar.CODFNCALT);
        dbCommand.AddWithValue("CDOANXCTTCLI", objAlterar.CDOANXCTTCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool ativarCadastroServico(CadastroServicoApiModel.CadastroServicoApiModel objAlterar)
    {
        var DALSQL = new CadastroServicoDALSQL();
        string cmdSql = DALSQL.ativarCadastroServico(objAlterar.LISTCODSVC);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);       
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool desativarCadastroServico(CadastroServicoApiModel.CadastroServicoApiModel objAlterar)
    {
        var DALSQL = new CadastroServicoDALSQL();
        string cmdSql = DALSQL.desativarCadastroServico(objAlterar.LISTCODSVC);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCDST = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //                     
        dbCommand.AddWithValue("CODFNCDST", objAlterar.CODFNCDST);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public CadastroServicoTO.obterCadastroServicoTemplate VisualizarTemplate(CadastroServicoApiModel.CadastroServicoApiModel objPesquisar)
    {
        var DALSQL = new CadastroServicoDALSQL();
        string cmdSql = DALSQL.VisualizarTemplate();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        if (objPesquisar.CODSVCTGV == 0)
        {
            objPesquisar.CODSVCTGV = -1;
        }
        dbCommand.AddWithValue("CODSVCTGV", objPesquisar.CODSVCTGV);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroServicoTO.obterCadastroServicoTemplate>().FirstOrDefault();
    }


    public List<CadastroServicoTO.obterCadastroBandeiras> MontarBandeira()
    {
        var DALSQL = new CadastroServicoDALSQL();
        string cmdSql = DALSQL.MontarBandeira();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroServicoTO.obterCadastroBandeiras>();
    }


    public bool inserirRelacaoServicoDocumento(Int64 CODSVCTGV,  Int64 CODDOCARZ)
    {
        var DALSQL = new CadastroServicoDALSQL();
        string cmdSql = DALSQL.inserirRelacaoServicoDocumento();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODSVCTGV", CODSVCTGV);
        dbCommand.AddWithValue("CODDOCARZ", CODDOCARZ);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public bool deleteRelacaoServicoDocumento(Int64 CODSVCTGV)
    {
        var DALSQL = new CadastroServicoDALSQL();
        string cmdSql = DALSQL.deleteRelacaoServicoDocumento();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODSVCTGV", CODSVCTGV);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public Int64 SelectMaxCodigoServico()
    {
        var DALSQL = new CadastroServicoDALSQL();
        string cmdSql = DALSQL.SelectMaxCodigoServico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return Convert.ToInt64(MRT001.ExecuteScalar(dbCommand));
    }




}