﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class CrmDALSQL
{
    public string obterCrm(int clitgv, int codStatus)
    {
    StringBuilder strBld;
        strBld = new StringBuilder(@"
                             SELECT CLI.CODCLI, CLI.NOMCLI
                , CASE CLI.NUMCGCCLI
                       WHEN CLI.NUMCGCCLI THEN(SUBSTR(CLI.NUMCGCCLI, 1, 2) || '.' || SUBSTR(CLI.NUMCGCCLI, 3, 3) || '.' || SUBSTR(CLI.NUMCGCCLI, 6, 3) || '/' || SUBSTR(CLI.NUMCGCCLI, 9, 4) || '-' || SUBSTR(CLI.NUMCGCCLI, 13, 2))
                       ELSE NULL
                  END NUMCGCCLI
                , CASE WHEN TGV.CODSITCLI IN (1,2,3) THEN 'SIM'
                       ELSE 'NÃO'
                  END CLIENTETGV
                , CASE WHEN TGV.CODSITCLI = 1 THEN 'EM IMPLANTAÇÃO'
                       WHEN TGV.CODSITCLI = 2 THEN 'ATIVO'
                       WHEN TGV.CODSITCLI = 3 THEN 'SUSPENSO'
                       WHEN TGV.CODSITCLI IS NULL OR TGV.CODSITCLI = 4 THEN MIN(CASE
                                                            WHEN OPT.INDSTAOPTVND = 1 THEN 'NEGOCIAÇÃO'
                                                            WHEN OPT.INDSTAOPTVND = 5 THEN 'AGUARDANDO APROVAÇÃO DE DESCONTO'
                                                            WHEN OPT.INDSTAOPTVND = 6 THEN 'DESCONTO APROVADO'
                                                            WHEN OPT.INDSTAOPTVND = 7 THEN 'DESCONTO REJEITADO'
                                                            WHEN OPT.INDSTAOPTVND = 2 THEN 'CONTRATO ENVIADO'
                                                            WHEN OPT.INDSTAOPTVND = 3 THEN 'VENDA GANHA'
                                                            WHEN OPT.INDSTAOPTVND = 4 THEN 'VENDA PERDIDA'
                                                            WHEN OPT.INDSTAOPTVND = 8 THEN 'AGUARDANDO DOCUMENTAÇÃO'
                                                            WHEN(OPT.INDSTAOPTVND IS NULL AND TGV.CODSITCLI IS NULL) THEN 'N/D'
                                                            WHEN TGV.CODSITCLI = 4 THEN 'CANCELADO'
                                                            ELSE ''
                                                            END)


                  END STATUS
                 , TGV.CODSITCLI
                 , GRP.NOMGRPEMPCLI, RLCGRP.CODGRPEMPCLI, GRP.CODCLIGRPFAT, CLIGRP.NOMCLI AS NOMCLIGRPFAT
                 , DECODE(GRP.INDFATGRP, 1, 'SIM', 2, 'NÃO') INDFATGRP
                 , CASE CLIGRP.NUMCGCCLI
                        WHEN CLIGRP.NUMCGCCLI THEN(SUBSTR(CLIGRP.NUMCGCCLI, 1, 2) || '.' || SUBSTR(CLIGRP.NUMCGCCLI, 3, 3) || '.' || SUBSTR(CLIGRP.NUMCGCCLI, 6, 3) || '/' || SUBSTR(CLIGRP.NUMCGCCLI, 9, 4) || '-' || SUBSTR(CLIGRP.NUMCGCCLI, 13, 2))
                        ELSE NULL
                   END NUMCGCCLIGRPFAT
                 , TGV.INDDBTAUTPGT
                 , TGV.CODBCO
                 , TGV.CODAGEBCO
                 , TGV.NUMDIGVRFAGE
                 , TGV.NUMCNTCRRCTT
                 , TGV.NUMDIGVRFCNTCRR
                 , SITMRT.DESMTVSITCLI AS STATUSMRT
                        FROM MRT.T0100043 CLI
                        LEFT JOIN MRT.CADCLITGV TGV ON TGV.CODCLI = CLI.CODCLI
                        LEFT JOIN MRT.CADOPTVNDCLITGV OPT ON CLI.CODCLI = OPT.CODCLI
                        LEFT JOIN MRT.RLCCLIGRPEMPTGV RLCGRP ON RLCGRP.CODCLI = CLI.CODCLI
                        LEFT JOIN MRT.CADGRPEMPTGV GRP ON RLCGRP.CODGRPEMPCLI = GRP.CODGRPEMPCLI AND GRP.DATDST IS NULL
                        LEFT JOIN MRT.T0100043 CLIGRP ON CLIGRP.CODCLI = GRP.CODCLIGRPFAT
                        LEFT JOIN MRT.T0101260 SITMRT ON CLI.CODSITCLI = SITMRT.CODCLFBSECLI
                        WHERE 1 = 1
                          AND CLI.TIPCGCCPFCLI = 2--SOMENTE CNPJ

                          AND CLI.NUMCGCCLI = :NUMCGCCLI

                          AND CLI.CODCLI = :CODCLI

                          AND CLI.NOMCLI LIKE '%' || UPPER(:NOMCLI) || '%'
                            -- FILTRO GRUPO EMPRESARIAL-- VAI PESQUISAR SÓ POR CODIGO

                         AND GRP.CODGRPEMPCLI = :CODGRPEMPCLI
       ");
        //FILTRO STATUS DO CLIENTE
        if (codStatus == 4)//CANCELADO
        {
            strBld.AppendLine(" AND TGV.CODSITCLI = :CODSTATUS AND OPT.INDSTAOPTVND IS NULL ");
        }
        else if (codStatus > 0 && codStatus < 4 )//STATUS DA CONTA
        {
            strBld.AppendLine("AND TGV.CODSITCLI = :CODSTATUS");
        }
        else if (codStatus > 4 && codStatus < 12)// STATUS DO SERVIÇO
        {
            strBld.AppendLine("AND OPT.INDSTAOPTVND = :CODSTATUS");
        }
        else if (codStatus == 12)//STATUS NULL (OPORTUNIDADE)
        {
            strBld.AppendLine(" AND TGV.CODSITCLI IS NULL AND OPT.INDSTAOPTVND IS NULL");
        }

        if (clitgv == 1)
        {
            strBld.AppendLine(" AND TGV.CODSITCLI IS NOT NULL -- SE ELE MARCAR O CHECK BOX DE CLIENTE TGV ");
        }
        else if (clitgv == 2)
        {
            strBld.AppendLine(" AND TGV.CODSITCLI IS NULL -- SE ELE MARCAR O NÃO DE CLIENTE TGV ");

        }

        strBld.AppendLine("  GROUP BY CLI.CODCLI, CLI.NOMCLI, CLI.NUMCGCCLI ");
        strBld.AppendLine("  , TGV.CODSITCLI ");
        strBld.AppendLine("  , GRP.NOMGRPEMPCLI, RLCGRP.CODGRPEMPCLI, GRP.CODCLIGRPFAT, CLIGRP.NOMCLI");
        strBld.AppendLine("  , GRP.INDFATGRP");
        strBld.AppendLine("  , CLIGRP.NUMCGCCLI");
        strBld.AppendLine("  , TGV.INDDBTAUTPGT");
        strBld.AppendLine("  , TGV.CODBCO");
        strBld.AppendLine("  , TGV.CODAGEBCO");
        strBld.AppendLine("  , TGV.NUMDIGVRFAGE");
        strBld.AppendLine("  , TGV.NUMCNTCRRCTT");
        strBld.AppendLine("  , TGV.NUMDIGVRFCNTCRR");        
        strBld.AppendLine("  , SITMRT.DESMTVSITCLI");

        // HAVING PASSA SOMENTE PARA FILTRO DE STATUS DE SERVICO...
        if (codStatus > 4 && codStatus < 12 )
        {
            strBld.AppendLine("HAVING (MIN(OPT.INDSTAOPTVND) = :CODSTATUS AND (TGV.CODSITCLI IS NULL OR TGV.CODSITCLI = 4))");
        }

        return strBld.ToString();
    }

    public string obterCrmNegociacao()
    {
        StringBuilder strBld;
        strBld = new StringBuilder(@"
                              SELECT CLI.CODCLI, CLI.NOMCLI
                , CASE CLI.NUMCGCCLI 
                       WHEN CLI.NUMCGCCLI THEN (SUBSTR(CLI.NUMCGCCLI, 1, 2) || '.' || SUBSTR(CLI.NUMCGCCLI, 3, 3) || '.' || SUBSTR(CLI.NUMCGCCLI, 6, 3) || '/' || SUBSTR(CLI.NUMCGCCLI, 9,4) || '-' || SUBSTR(CLI.NUMCGCCLI, 13,2) ) 
                       ELSE NULL 
                  END NUMCGCCLI
                , CASE WHEN TGV.CODSITCLI IS NOT NULL THEN 'SIM'
                       WHEN OPT.INDSTAOPTVND IS NOT NULL THEN 'EM NEGOCIAÇÃO'
                       ELSE 'NÃO'
                  END CLIENTETGV
                , CASE WHEN TGV.CODSITCLI = 1 THEN 'IMPLANTAÇÃO'
                       WHEN TGV.CODSITCLI = 2 THEN 'ATIVO'
                       WHEN TGV.CODSITCLI = 3 THEN 'SUSPENSO'
                       WHEN TGV.CODSITCLI = 4 THEN 'CANCELADO'
                       WHEN TGV.CODSITCLI IS NULL THEN DECODE(OPT.INDSTAOPTVND,NULL,'', 4, 'NEG. - VENDA PERDIDA', 2, 'CONTRATO NEG. ENVIADO', 3, 'ATIVO', 'NEGOCIAÇÃO') 
                  END STATUS      
                 , TGV.CODSITCLI
                 , OPT.INDSTAOPTVND
                 , GRP.NOMGRPEMPCLI, RLCGRP.CODGRPEMPCLI, GRP.CODCLIGRPFAT, CLIGRP.NOMCLI AS NOMCLIGRPFAT
                 , DECODE(GRP.INDFATGRP, 1, 'SIM', 2, 'NÃO') INDFATGRP
                 , CASE CLIGRP.NUMCGCCLI 
                        WHEN CLIGRP.NUMCGCCLI THEN (SUBSTR(CLIGRP.NUMCGCCLI, 1, 2) || '.' || SUBSTR(CLIGRP.NUMCGCCLI, 3, 3) || '.' || SUBSTR(CLIGRP.NUMCGCCLI, 6, 3) || '/' || SUBSTR(CLIGRP.NUMCGCCLI, 9,4) || '-' || SUBSTR(CLIGRP.NUMCGCCLI, 13,2) ) 
                        ELSE NULL 
                   END NUMCGCCLIGRPFAT
                 , TGV.INDDBTAUTPGT
                 , TGV.CODBCO
                 , TGV.CODAGEBCO
                 , TGV.NUMDIGVRFAGE
                 , TGV.NUMCNTCRRCTT
                 , TGV.NUMDIGVRFCNTCRR
            FROM MRT.T0100043 CLI      
            LEFT JOIN MRT.CADCLITGV TGV ON TGV.CODCLI = CLI.CODCLI
            LEFT JOIN MRT.CADOPTVNDCLITGV OPT ON CLI.CODCLI = OPT.CODCLI
            LEFT JOIN MRT.RLCCLIGRPEMPTGV RLCGRP ON RLCGRP.CODCLI = OPT.CODCLI
            LEFT JOIN MRT.CADGRPEMPTGV GRP ON RLCGRP.CODGRPEMPCLI = GRP.CODGRPEMPCLI
            LEFT JOIN MRT.T0100043 CLIGRP ON CLIGRP.CODCLI = GRP.CODCLIGRPFAT
            WHERE 1 = 1   
               AND CLI.CODCLI = :CODCLI");

        strBld.AppendLine("  GROUP BY CLI.CODCLI, CLI.NOMCLI, CLI.NUMCGCCLI ");
        strBld.AppendLine("  , TGV.CODSITCLI ");
        strBld.AppendLine("  , GRP.NOMGRPEMPCLI, RLCGRP.CODGRPEMPCLI, GRP.CODCLIGRPFAT, CLIGRP.NOMCLI");
        strBld.AppendLine("  , GRP.INDFATGRP");
        strBld.AppendLine("  , CLIGRP.NUMCGCCLI");
        strBld.AppendLine("  , TGV.INDDBTAUTPGT");
        strBld.AppendLine("  , TGV.CODBCO");
        strBld.AppendLine("  , TGV.CODAGEBCO");
        strBld.AppendLine("  , TGV.NUMDIGVRFAGE");
        strBld.AppendLine("  , TGV.NUMCNTCRRCTT");
        strBld.AppendLine("  , TGV.NUMDIGVRFCNTCRR");
        strBld.AppendLine("  , OPT.INDSTAOPTVND");

        return strBld.ToString();

    }

    public string obterEstruturaMartins()
    {
        return @"
                 WITH TABESTRUTURA AS (
                 SELECT '1REP' TIPO FROM DUAL 
                 UNION SELECT '2GM' TIPO FROM DUAL
                 UNION SELECT '3GV' TIPO FROM DUAL)
                 SELECT T.TIPO ORDEM
                 , 'MARTINS - FV' EMPRESA
                 , CASE WHEN T.TIPO = '1REP' THEN 'REPRESENTANTE' 
                 WHEN T.TIPO = '2GM'  THEN 'GERENTE MERCADO' 
                 WHEN T.TIPO = '3GV' THEN 'GERENTE VENDAS' 
                 ELSE NULL 
                 END FUNCAO 
                 , CASE WHEN T.TIPO = '1REP' THEN RCA.CODREP
                 WHEN T.TIPO = '2GM'  THEN GM.CODSUP 
                 WHEN T.TIPO = '3GV' THEN GV.CODGER
                 ELSE NULL 
                 END MATRICULA 
                 , CASE WHEN T.TIPO = '1REP' THEN UPPER(TRIM(RCA.NOMREP))
                 WHEN T.TIPO = '2GM'  THEN UPPER(GM.NOMSUP)
                 WHEN T.TIPO = '3GV' THEN UPPER(GV.NOMGER)
                 ELSE NULL 
                 END NOME
                 , CASE WHEN T.TIPO = '1REP' THEN '-' 
                 WHEN T.TIPO = '2GM'  THEN UPPER(GMS.DESUSRRDEMRT) || '@MARTINS.com.br' 
                 WHEN T.TIPO = '3GV' THEN UPPER(GVS.DESUSRRDEMRT) || '@MARTINS.com.br'
                 ELSE NULL 
                 END EMAIL
                 , CASE WHEN T.TIPO = '1REP' THEN UPPER(RCA.NUMTLFREP)
                 WHEN T.TIPO = '2GM'  THEN UPPER(REPGM.NUMSGNTLFCELREP)
                 WHEN T.TIPO = '3GV' THEN UPPER(GV.NUMTLFCEL)
                 ELSE NULL 
                 END TELEFONE
                 , CASE WHEN T.TIPO = '1REP' THEN UPPER(GM.NOMSUP)  
                 WHEN T.TIPO = '2GM'  THEN UPPER(GV.NOMGER)
                 WHEN T.TIPO = '3GV' THEN '-'
                 ELSE NULL 
                 END SUPERIOR      
                 FROM TABESTRUTURA T
                 INNER JOIN mrt.t0100043 cli ON 1 = 1
                 INNER JOIN mrt.t0133774 tet ON cli.codcli = tet.codcli 
                 INNER JOIN mrt.t0133715 vnd ON tet.codtetvnd = vnd.codtetvnd 
                 INNER JOIN mrt.t0100116 rca ON vnd.codrep = rca.codrep and rca.CODUNDNGC = 1 
                 INNER JOIN mrt.t0100124 gm ON rca.codsup = gm.codsup 
                 INNER JOIN mrt.t0100051 gv ON gm.codger = gv.codger 
                 INNER JOIN MRT.CADUSRRDE GMS ON GMS.CODFNC = GM.CODFNCSUP
                 INNER JOIN MRT.CADUSRRDE GVS ON GVS.CODFNC = GV.CODFNCGER
                 INNER JOIN mrt.t0100116 REPGM ON REPGM.CODREP = GM.CODSUP
                 WHERE cli.CODCLI = :CODCLI
                 AND rca.datdstrep IS NULL 
                 AND gv.datdstger IS NULL 
                 AND vnd.datdsttetvnd IS NULL 
                 AND gm.datdstsup IS NULL                 
                 UNION
                 SELECT '4SMART' ORDEM  
                 , 'SMART' EMPRESA
                 , 'SUPERVISOR' FUNCAO
                 , F.CODFNC MATRICULA
                 , UPPER(F.NOMFNC) NOME
                 , UPPER(R.DESUSRRDEMRT) || '@MARTINS.COM.BR' EMAIL
                 , UPPER(S.NUMRMLTLF) TELEFONE
                 , UPPER(S.NOMFNC) SUPERIOR
                 FROM MRT.RLCCLIAREATDSUPSMA A
                 INNER JOIN MRT.T0100361 F ON F.CODFNC = A.CODFNCSUPRPN
                 INNER JOIN MRT.CADUSRRDE R ON R.CODFNC = F.CODFNC
                 INNER JOIN MRT.T0100361 S ON S.CODFNC = R.CODFNCRSP
                 WHERE A.CODCLI = :CODCLI
                 AND A.CODSITCLI IN (0,16)
                 ORDER BY 1
                ";
    }
   
    public string obterEstruturaTribanco()
    {
        return @"
                SELECT  
                  TRIM(TR.NUMORD) ORDEM
                  ,'TRIBANCO' EMPRESA
                  ,CASE TR.NUMORD 
                    WHEN 1 THEN 'ASSISTENTE COMERCIAL'
                    WHEN 2 THEN 'GERENTE COMERCIAL'
                    WHEN 3 THEN 'GERENTE REGIONAL'
                  END FUNCAO
                  ,TRIM(TR.CODFNCRPN) MATRICULA
                  ,UPPER(TRIM(FNC1.NOMFNC)) NOME
                  ,UPPER(TRIM(EMAIL.DESUSRRDEMRT)) || '@MARTINS.COM.BR' EMAIL

                  ,CASE LENGTH(TRIM(TR.LSTTLFCTO)) 
                             WHEN 11 THEN  ( '(' || UPPER(SUBSTR(TR.LSTTLFCTO, 1, 2)) || ')' || UPPER(SUBSTR(TR.LSTTLFCTO, 3, 5)) || '-' ||  UPPER(SUBSTR(TR.LSTTLFCTO, 8, 4)) ) 
                             WHEN 10 THEN  ( '(' || UPPER(SUBSTR(TR.LSTTLFCTO, 1, 2)) || ') ' || UPPER(SUBSTR(TR.LSTTLFCTO, 3, 4)) || '-' || UPPER(SUBSTR(TR.LSTTLFCTO, 7, 4) )) 
                        ELSE UPPER(TR.LSTTLFCTO)
                  END TELEFONE

                  ,UPPER(TRIM(FNC2.NOMFNC)) SUPERIOR
                    FROM MRT.CADETTCMCTGV TR 
                    INNER JOIN MRT.CADUSRRDE RDE 
                        ON RDE.CODFNC = TR.CODFNCRPN 
                        
                    FULL JOIN MRT.T0100361 FNC1 
                      ON FNC1.CODFNC = TR.CODFNCRPN
                      
                   FULL JOIN MRT.T0100361 FNC2 
                      ON FNC2.CODFNC = TR.CODFNCSUPRPN
                      
                    INNER JOIN MRT.CADUSRRDE EMAIL 
                      ON EMAIL.CODFNC = TR.CODFNCRPN
                    WHERE 1 = 1
                          AND TR.CODCLI = :CODCLI 
                    ORDER BY TR.NUMORD
                ";
    }

    public string obterDadosBasicos()
    {
        return @"
                 SELECT C.CODCLI, C.NOMCLI, C.NUMCGCCLI 
         , TRIM(LGR.DesTipLgr) || ' ' || TRIM(END.DesLgr) || ', ' || END.NumEndLgr || ' - ' || END.DESCPLEND ENDERECO
                 , END.CODCEP
                 , BAI.NOMBAIEXD NOMBAI
                 , CID.NOMCIDEXD NOMCID 
                 , TRIM(CID.NOMCIDEXD) || ' - ' || TRIM(CID.CodEstUni) AS NOMCIDEST 
                 , C.NUMTLFCLI NUMTEL 
                 , C.NUMTLFCELCLI NUMTELCEL 
                 , C.NUMINSESTCLI
                 --, email da loja                 
                 , UND.DESUNDESRNGC UNIDADENEGOCIO -- INCLUIR NA TELA
                 , CNL.DESCNL CANAL -- INCLUIR NA TELA
                 , SGM.DESEQINGC SEGMENTOMERCADO -- INCLUIR NA TELA
                 , ATI.NOMATI
                 , C.QDECXACLI
                 , C.TAMAREFISVNDCLI
                 , C.TAMAREFISDPSCLI
                 FROM MRT.T0100043 C       
                 INNER JOIN MRT.T0138938 END ON C.CodCli = END.Codcli
                 INNER JOIN MRT.T0100027 BAI ON END.CodBai    = BAI.Codbai
                 INNER JOIN MRT.T0100035 CID ON BAI.CodCid    = CID.Codcid
                 LEFT  JOIN MRT.T0103905 CPL On END.Codcplbai = CPL.CodCplBai
                 LEFT  JOIN MRT.T0138946 LGR On END.TipLgr    = LGR.TipLgr
                 INNER JOIN MRT.T0114850 CNL ON CNL.CODCNL = C.CODCNL
                 INNER JOIN MRT.T0114893 SGM ON SGM.CODEQINGC = CNL.CODEQINGC
                 INNER JOIN MRT.T0131275 UND ON UND.CODUNDESRNGC = SGM.CODUNDESRNGC
                 INNER JOIN MRT.T0100019 ATI ON ATI.CODATI = C.CODATI
                 WHERE END.CodCli  = :CODCLI
                 AND END.TipEndCli = :TIPENDCLI
                ";
    }

    public string obterGrupoEconomico()
    {
        return @"
                  WITH TABEMP AS (
                 SELECT R.CODGRPEMPCLI
                 FROM MRT.T0100043 C 
                 INNER JOIN MRT.RLCCLIGRPEMPTGV R ON C.CODCLI = R.CODCLI
                 WHERE C.CODCLI = :CODCLI -- DA TELA
                 )
                 SELECT G.CODGRPEMPCLI, G.INDFATGRP
                     , C.CODCLI, C.NOMCLI 
                     , CASE C.NUMCGCCLI WHEN C.NUMCGCCLI THEN (SUBSTR(C.NUMCGCCLI, 1, 2) || '.' || SUBSTR(C.NUMCGCCLI, 3, 3) || '.' || SUBSTR(C.NUMCGCCLI, 6, 3) || '/' || SUBSTR(C.NUMCGCCLI, 9,4) || '-' || SUBSTR(C.NUMCGCCLI, 13,2) ) ELSE NULL END NUMCGCCLI
                     , CID.NOMCID
                     , CID.CODESTUNI
                     , O.CODOPTVND
                     , DECODE(O.INDSTAOPTVND, 1, 'NEGOCIAÇÃO', 2, 'CONTRATO ENVIADO', 3, 'VENDA GANHA', 4, 'VENDA PERDIDA','SEM INDICAÇÃO') STATUS
                 FROM TABEMP T
                INNER JOIN MRT.CADGRPEMPTGV G ON G.CODGRPEMPCLI = T.CODGRPEMPCLI
                INNER JOIN MRT.RLCCLIGRPEMPTGV R ON G.CODGRPEMPCLI = R.CODGRPEMPCLI
                INNER JOIN MRT.T0100043 C ON C.CODCLI = R.CODCLI
                INNER JOIN MRT.T0138938 END ON C.CodCli = END.Codcli
                INNER JOIN MRT.T0100027 BAI ON END.CodBai    = BAI.Codbai
                INNER JOIN MRT.T0100035 CID ON BAI.CodCid    = CID.Codcid
                 LEFT JOIN MRT.CADOPTVNDCLITGV O ON O.CODCLI = C.CODCLI
                ";
    }

    public string obterContatosTelevendas()
    {
        return @"
                    SELECT NUMFAX
                        ,NUMTLF
                        ,NUMTLFCELCTOCLI
                    FROM MRT.T0139020
                    WHERE CODCLI= :CODCLI
                    AND (NUMFAX <> ' '  OR NUMTLF <> ' '  OR NUMTLFCELCTOCLI <> ' ')
                ";
    }

    public string obterCestaProdutoCliente()
    {

            return @"
                          WITH TABPERIODO AS ( 
                              SELECT TO_CHAR(SYSDATE, 'YYYYMM') AS ANOMESREF FROM DUAL UNION
                              SELECT TO_CHAR(ADD_MONTHS(SYSDATE, - 1),   'YYYYMM') FROM DUAL UNION
                              SELECT TO_CHAR(ADD_MONTHS(SYSDATE, - 2), 'YYYYMM') FROM DUAL UNION 
                              SELECT TO_CHAR(ADD_MONTHS(SYSDATE, - 3), 'YYYYMM') FROM DUAL UNION 
                              SELECT TO_CHAR(ADD_MONTHS(SYSDATE, - 4), 'YYYYMM') FROM DUAL UNION 
                              SELECT TO_CHAR(ADD_MONTHS(SYSDATE, - 5), 'YYYYMM') FROM DUAL UNION 
                              SELECT TO_CHAR(ADD_MONTHS(SYSDATE, - 6), 'YYYYMM') FROM DUAL UNION 
                              SELECT TO_CHAR(ADD_MONTHS(SYSDATE, - 7), 'YYYYMM') FROM DUAL UNION 
                              SELECT TO_CHAR(ADD_MONTHS(SYSDATE, - 8), 'YYYYMM') FROM DUAL UNION 
                              SELECT TO_CHAR(ADD_MONTHS(SYSDATE, - 9), 'YYYYMM') FROM DUAL UNION 
                              SELECT TO_CHAR(ADD_MONTHS(SYSDATE, - 10), 'YYYYMM') FROM DUAL UNION 
                              SELECT TO_CHAR(ADD_MONTHS(SYSDATE, - 11), 'YYYYMM') FROM DUAL UNION 
                              SELECT TO_CHAR(ADD_MONTHS(SYSDATE, - 12), 'YYYYMM') FROM DUAL
                            )
                 SELECT A.CODPRDCES, LTRIM(RTRIM(initcap(A.DESPRDCES))) DESPRDCES,LTRIM(RTRIM(A.TIPDDOATR)) TIPDDOATR, LTRIM(RTRIM(A.NUMSEQ)) NUMSEQ
                       , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(SYSDATE, 'YYYYMM'),
                                CASE WHEN M.CODPRDCES = 1 THEN to_char(M.VLRPRDCES, '999G999G999','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 2 THEN to_char(M.VLRPRDCES, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 14 THEN initcap(D.DESMTVSITCLI) ELSE initcap(M.VLRPRDCES) 
                                END,
                            NULL)
                        ) MOVMESUM
                       , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 1), 'YYYYMM'),
                                CASE WHEN M.CODPRDCES = 1 THEN to_char(M.VLRPRDCES, '999G999G999','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 2 THEN to_char(M.VLRPRDCES, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 14 THEN initcap(D.DESMTVSITCLI) 
                                    ELSE initcap(M.VLRPRDCES) 
                                END,
                            NULL)
                        ) MOVMESDOIS
                       , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 2), 'YYYYMM'),
                                CASE WHEN M.CODPRDCES = 1 THEN to_char(M.VLRPRDCES, '999G999G999','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 2 THEN to_char(M.VLRPRDCES, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 14 THEN initcap(D.DESMTVSITCLI) 
                                    ELSE initcap(M.VLRPRDCES) 
                                END,
                            NULL)
                        ) MOVMESTRES
                       , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 3), 'YYYYMM'),
                                CASE WHEN M.CODPRDCES = 1 THEN to_char(M.VLRPRDCES, '999G999G999','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 2 THEN to_char(M.VLRPRDCES, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 14 THEN initcap(D.DESMTVSITCLI) 
                                    ELSE initcap(M.VLRPRDCES) 
                                END,
                            NULL)
                        ) MOVMESQUATRO
                       , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 4), 'YYYYMM'),
                                CASE WHEN M.CODPRDCES = 1 THEN to_char(M.VLRPRDCES, '999G999G999','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 2 THEN to_char(M.VLRPRDCES, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 14 THEN initcap(D.DESMTVSITCLI) 
                                    ELSE initcap(M.VLRPRDCES) 
                                END,
                            NULL)
                        ) MOVMESCINCO
                       , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 5), 'YYYYMM'),
                                CASE WHEN M.CODPRDCES = 1 THEN to_char(M.VLRPRDCES, '999G999G999','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 2 THEN to_char(M.VLRPRDCES, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 14 THEN initcap(D.DESMTVSITCLI) 
                                    ELSE initcap(M.VLRPRDCES) 
                                END,
                            NULL)
                        ) MOVMESSEIS
                       , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 6), 'YYYYMM'),
                                CASE WHEN M.CODPRDCES = 1 THEN to_char(M.VLRPRDCES, '999G999G999','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 2 THEN to_char(M.VLRPRDCES, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 14 THEN initcap(D.DESMTVSITCLI) 
                                    ELSE initcap(M.VLRPRDCES) 
                                END,
                            NULL)
                        ) MOVMESSETE
                       , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 7), 'YYYYMM'),
                                CASE WHEN M.CODPRDCES = 1 THEN to_char(M.VLRPRDCES, '999G999G999','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 2 THEN to_char(M.VLRPRDCES, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 14 THEN initcap(D.DESMTVSITCLI) 
                                    ELSE initcap(M.VLRPRDCES) 
                                END,
                            NULL)
                        ) MOVMESOITO
                       , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 8), 'YYYYMM'),
                                CASE WHEN M.CODPRDCES = 1 THEN to_char(M.VLRPRDCES, '999G999G999','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 2 THEN to_char(M.VLRPRDCES, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 14 THEN initcap(D.DESMTVSITCLI) 
                                    ELSE initcap(M.VLRPRDCES) 
                                END,
                            NULL)
                        ) MOVMESNOVE
                       , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 9), 'YYYYMM'),
                                CASE WHEN M.CODPRDCES = 1 THEN to_char(M.VLRPRDCES, '999G999G999','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 2 THEN to_char(M.VLRPRDCES, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 14 THEN initcap(D.DESMTVSITCLI) 
                                    ELSE initcap(M.VLRPRDCES) 
                                END,
                            NULL)
                        ) MOVMESDEZ
                       , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 10), 'YYYYMM'),
                                CASE WHEN M.CODPRDCES = 1 THEN to_char(M.VLRPRDCES, '999G999G999','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 2 THEN to_char(M.VLRPRDCES, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 14 THEN initcap(D.DESMTVSITCLI) 
                                    ELSE initcap(M.VLRPRDCES) 
                                END,
                            NULL)
                        ) MOVMESONZE
                       , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 11), 'YYYYMM'),
                                CASE WHEN M.CODPRDCES = 1 THEN to_char(M.VLRPRDCES, '999G999G999','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 2 THEN to_char(M.VLRPRDCES, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 14 THEN initcap(D.DESMTVSITCLI) 
                                    ELSE initcap(M.VLRPRDCES) 
                                END,
                            NULL)
                        ) MOVMESDOZE
                       , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 12), 'YYYYMM'),
                                CASE WHEN M.CODPRDCES = 1 THEN to_char(M.VLRPRDCES, '999G999G999','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 2 THEN to_char(M.VLRPRDCES, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                     WHEN M.CODPRDCES = 14 THEN initcap(D.DESMTVSITCLI) 
                                    ELSE initcap(M.VLRPRDCES) 
                                END,
                            NULL)
                        ) MOVMESTREZE

                 FROM TABPERIODO P 
                 LEFT JOIN MRT.CADCESPRDTGV A ON A.DATDST IS NULL
                 LEFT JOIN MRT.MOVCESPRDTGV M ON M.ANOMESREF = P.ANOMESREF AND M.CODPRDCES = A.CODPRDCES AND M.CODCLI = :CODCLI
                 LEFT JOIN MRT.T0101260 D ON TO_CHAR(RTRIM(D.CODMTVSITCLI)) = TO_CHAR(RTRIM(M.VLRPRDCES))
                 WHERE A.CODPRDCES NOT IN (5,6)
                 GROUP BY A.CODPRDCES, A.DESPRDCES, A.TIPDDOATR, A.NUMSEQ
                 
                 UNION ALL
                 
                      SELECT CAD.CODPRDCES, LTRIM(RTRIM(initcap(CAD.DESPRDCES))) DESPRDCES, LTRIM(RTRIM(CAD.TIPDDOATR)) TIPDDOATR, LTRIM(RTRIM(CAD.NUMSEQ)) NUMSEQ
                       , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(SYSDATE, 'YYYYMM'),
                                CASE CAD.CODPRDCES
                                    WHEN 5 THEN to_char(C.NUMFRQCMPCLI, '999G999G999') 
                                    WHEN 6 THEN to_char(C.VLRCMPCLI, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.')
                                END,
                            NULL)
                        ) MOVMESUM
                       , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 1), 'YYYYMM'),
                                CASE CAD.CODPRDCES WHEN 5 THEN to_char(C.NUMFRQCMPCLI, '999G999G999') 
                                    WHEN 6 THEN to_char(C.VLRCMPCLI, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                END,
                            NULL)
                        ) MOVMESDOIS
                       , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 2), 'YYYYMM'),
                                CASE CAD.CODPRDCES WHEN 5 THEN to_char(C.NUMFRQCMPCLI, '999G999G999') 
                                    WHEN 6 THEN to_char(C.VLRCMPCLI, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                END, 
                            NULL)
                        ) MOVMESTRES
                       , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 3), 'YYYYMM'), 
                                CASE CAD.CODPRDCES WHEN 5 THEN to_char(C.NUMFRQCMPCLI, '999G999G999') 
                                    WHEN 6 THEN to_char(C.VLRCMPCLI, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                END,
                            NULL)
                        ) MOVMESQUATRO
                       , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 4), 'YYYYMM'),
                                CASE CAD.CODPRDCES WHEN 5 THEN to_char(C.NUMFRQCMPCLI, '999G999G999') 
                                    WHEN 6 THEN to_char(C.VLRCMPCLI, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                END,
                            NULL)
                        ) MOVMESCINCO
                       , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 5), 'YYYYMM'),
                                CASE CAD.CODPRDCES WHEN 5 THEN to_char(C.NUMFRQCMPCLI, '999G999G999') 
                                    WHEN 6 THEN to_char(C.VLRCMPCLI, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                END,
                            NULL)
                        ) MOVMESSEIS
                        , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 6), 'YYYYMM'),
                                CASE CAD.CODPRDCES WHEN 5 THEN to_char(C.NUMFRQCMPCLI, '999G999G999') 
                                    WHEN 6 THEN to_char(C.VLRCMPCLI, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                END,
                            NULL)
                        ) MOVMESSETE
                        , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 7), 'YYYYMM'),
                                CASE CAD.CODPRDCES WHEN 5 THEN to_char(C.NUMFRQCMPCLI, '999G999G999') 
                                    WHEN 6 THEN to_char(C.VLRCMPCLI, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                END,
                            NULL)
                        ) MOVMESOITO
                        , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 8), 'YYYYMM'),
                                CASE CAD.CODPRDCES WHEN 5 THEN to_char(C.NUMFRQCMPCLI, '999G999G999') 
                                    WHEN 6 THEN to_char(C.VLRCMPCLI, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                END,
                            NULL)
                        ) MOVMESNOVE
                        , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 9), 'YYYYMM'),
                                CASE CAD.CODPRDCES WHEN 5 THEN to_char(C.NUMFRQCMPCLI, '999G999G999') 
                                    WHEN 6 THEN to_char(C.VLRCMPCLI, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                END,
                            NULL)
                        ) MOVMESDEZ
                        , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 10), 'YYYYMM'),
                                CASE CAD.CODPRDCES WHEN 5 THEN to_char(C.NUMFRQCMPCLI, '999G999G999') 
                                    WHEN 6 THEN to_char(C.VLRCMPCLI, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                END,
                            NULL)
                        ) MOVMESONZE
                        , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 11), 'YYYYMM'),
                                CASE CAD.CODPRDCES WHEN 5 THEN to_char(C.NUMFRQCMPCLI, '999G999G999') 
                                    WHEN 6 THEN to_char(C.VLRCMPCLI, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                END,
                            NULL)
                        ) MOVMESDOZE
                        , MAX(
                            DECODE(P.ANOMESREF, TO_CHAR(ADD_MONTHS(SYSDATE, - 12), 'YYYYMM'),
                                CASE CAD.CODPRDCES WHEN 5 THEN to_char(C.NUMFRQCMPCLI, '999G999G999') 
                                    WHEN 6 THEN to_char(C.VLRCMPCLI, '999G999G999D90','NLS_NUMERIC_CHARACTERS = '',.') 
                                END,
                            NULL)
                        ) MOVMESTREZE
                       FROM TABPERIODO P 
                       LEFT JOIN MRT.CADCESPRDTGV CAD ON CAD.DATDST IS NULL
                       LEFT JOIN MRT.T0100175 C ON C.CODCLI = :CODCLI AND C.ANOMESREF = TO_NUMBER(P.ANOMESREF)
                       WHERE CAD.CODPRDCES IN (5,6)

                GROUP BY CAD.CODPRDCES, CAD.DESPRDCES, CAD.TIPDDOATR, CAD.NUMSEQ
                
                ORDER BY 1"
            ;
      
        //return @"
        //    WITH TABCESTA AS (
        //    SELECT CES.CODPRDCES
        //    ,  CES.DESPRDCES
        //    ,  (SELECT MOVSUB.VLRPRDCES FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI1 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(SYSDATE, 'YYYYMM')) AND MOVSUB.CODPRDCES = CES.CODPRDCES) MOVMESUM
        //    ,  (SELECT CES.TIPDDOATR FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI1 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(SYSDATE, 'YYYYMM')) AND MOVSUB.CODPRDCES = CES.CODPRDCES) TIPMESUM
        //    ,  (SELECT MOVSUB.VLRPRDCES FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI2 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 1), 'YYYYMM')) AND MOVSUB.CODPRDCES = CES.CODPRDCES) MOVMESDOIS
        //    ,  (SELECT CES.TIPDDOATR FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI1 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 1), 'YYYYMM'))  AND MOVSUB.CODPRDCES = CES.CODPRDCES) TIPMESDOIS
        //    ,  (SELECT MOVSUB.VLRPRDCES FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI3 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 2), 'YYYYMM')) AND MOVSUB.CODPRDCES = CES.CODPRDCES) MOVMESTRES
        //    ,  (SELECT CES.TIPDDOATR FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI1 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 2), 'YYYYMM'))  AND MOVSUB.CODPRDCES = CES.CODPRDCES) TIPMESTRES
        //    ,  (SELECT MOVSUB.VLRPRDCES FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI4 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 3), 'YYYYMM')) AND MOVSUB.CODPRDCES = CES.CODPRDCES) MOVMESQUATRO
        //    ,  (SELECT CES.TIPDDOATR FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI1 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 3), 'YYYYMM'))  AND MOVSUB.CODPRDCES = CES.CODPRDCES) TIPMESQUATRO
        //    ,  (SELECT MOVSUB.VLRPRDCES FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI5 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 4), 'YYYYMM')) AND MOVSUB.CODPRDCES = CES.CODPRDCES) MOVMESCINCO
        //    ,  (SELECT CES.TIPDDOATR FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI1 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 4), 'YYYYMM'))  AND MOVSUB.CODPRDCES = CES.CODPRDCES) TIPMESCINCO
        //    ,  (SELECT MOVSUB.VLRPRDCES FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI6 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 5), 'YYYYMM')) AND MOVSUB.CODPRDCES = CES.CODPRDCES) MOVMESSEIS
        //    ,  (SELECT CES.TIPDDOATR FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI1 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 5), 'YYYYMM'))  AND MOVSUB.CODPRDCES = CES.CODPRDCES) TIPMESSEIS
        //    ,  (SELECT MOVSUB.VLRPRDCES FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI7 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 6), 'YYYYMM')) AND MOVSUB.CODPRDCES = CES.CODPRDCES) MOVMESSETE
        //    ,  (SELECT CES.TIPDDOATR FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI1 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 6), 'YYYYMM'))  AND MOVSUB.CODPRDCES = CES.CODPRDCES) TIPMESSETE
        //    ,  (SELECT MOVSUB.VLRPRDCES FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI8 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 7), 'YYYYMM')) AND MOVSUB.CODPRDCES = CES.CODPRDCES) MOVMESOITO
        //    ,  (SELECT CES.TIPDDOATR FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI1 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 7), 'YYYYMM'))  AND MOVSUB.CODPRDCES = CES.CODPRDCES) TIPMESOITO
        //    ,  (SELECT MOVSUB.VLRPRDCES FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI9 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 8), 'YYYYMM')) AND MOVSUB.CODPRDCES = CES.CODPRDCES) MOVMESNOVE
        //    ,  (SELECT CES.TIPDDOATR FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI1 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 8), 'YYYYMM'))  AND MOVSUB.CODPRDCES = CES.CODPRDCES) TIPMESNOVE
        //    ,  (SELECT MOVSUB.VLRPRDCES FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI10 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 9), 'YYYYMM')) AND MOVSUB.CODPRDCES = CES.CODPRDCES) MOVMESDEZ
        //    ,  (SELECT CES.TIPDDOATR FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI1 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 9), 'YYYYMM'))  AND MOVSUB.CODPRDCES = CES.CODPRDCES) TIPMESDEZ
        //    ,  (SELECT MOVSUB.VLRPRDCES FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI11 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 10), 'YYYYMM')) AND MOVSUB.CODPRDCES = CES.CODPRDCES) MOVMESONZE
        //    ,  (SELECT CES.TIPDDOATR FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI1 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 10), 'YYYYMM'))  AND MOVSUB.CODPRDCES = CES.CODPRDCES) TIPMESONZE
        //    ,  (SELECT MOVSUB.VLRPRDCES FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI12 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 11), 'YYYYMM')) AND MOVSUB.CODPRDCES = CES.CODPRDCES) MOVMESDOZE
        //    ,  (SELECT CES.TIPDDOATR FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI1 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 11), 'YYYYMM'))  AND MOVSUB.CODPRDCES = CES.CODPRDCES) TIPMESDOZE
        //    ,  (SELECT MOVSUB.VLRPRDCES FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI13 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 12), 'YYYYMM')) AND MOVSUB.CODPRDCES = CES.CODPRDCES) MOVMESTREZE
        //    ,  (SELECT CES.TIPDDOATR FROM MRT.MOVCESPRDTGV MOVSUB WHERE MOVSUB.CODCLI = :CODCLI1 AND MOVSUB.ANOMESREF= TO_NUMBER(TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 12), 'YYYYMM'))  AND MOVSUB.CODPRDCES = CES.CODPRDCES) TIPMESTREZE
        //    FROM MRT.CADCESPRDTGV CES
        //    LEFT JOIN MRT.MOVCESPRDTGV MOV ON MOV.CODPRDCES = CES.CODPRDCES
        //    WHERE MOV.CODCLI = :CODCLI13)
        //         SELECT CODPRDCES
        //         ,  DESPRDCES
        //         ,  COALESCE(MOVMESUM,'-') MOVMESUM
        //         ,  TO_CHAR(TRUNC(SYSDATE), 'MM/YYYY') DATUM
        //         ,  TIPMESUM
        //         ,  COALESCE(MOVMESDOIS,'-') MOVMESDOIS
        //         ,  TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 1), 'MM/YYYY') DATDOIS
        //         ,  TIPMESDOIS
        //         ,  COALESCE(MOVMESTRES,'-') MOVMESTRES
        //         ,  TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 2), 'MM/YYYY') DATTRES
        //         ,  TIPMESTRES
        //         ,  COALESCE(MOVMESQUATRO,'-') MOVMESQUATRO
        //         ,  TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 3), 'MM/YYYY') DATQUATRO
        //         ,  TIPMESQUATRO
        //         ,  COALESCE(MOVMESCINCO,'-') MOVMESCINCO
        //         ,  TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 4), 'MM/YYYY') DATCINCO
        //         ,  TIPMESCINCO
        //         ,  COALESCE(MOVMESSEIS,'-') MOVMESSEIS
        //         ,  TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 5), 'MM/YYYY') DATSEIS
        //         ,  TIPMESSEIS
        //         ,  COALESCE(MOVMESSETE,'-') MOVMESSETE
        //         ,  TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 6), 'MM/YYYY') DATSETE
        //         ,  TIPMESSETE
        //         ,  COALESCE(MOVMESOITO,'-') MOVMESOITO
        //         ,  TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 7), 'MM/YYYY') DATOITO
        //         ,  TIPMESOITO
        //         ,  COALESCE(MOVMESNOVE,'-') MOVMESNOVE
        //         ,  TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 8), 'MM/YYYY') DATNOVE
        //         ,  TIPMESNOVE
        //         ,  COALESCE(MOVMESDEZ,'-') MOVMESDEZ
        //         ,  TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 9), 'MM/YYYY') DATDEZ
        //         ,  TIPMESDEZ
        //         ,  COALESCE(MOVMESONZE,'-') MOVMESONZE
        //         ,  TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 10), 'MM/YYYY') DATONZE
        //         ,  TIPMESONZE
        //         ,  COALESCE(MOVMESDOZE,'-') MOVMESDOZE
        //         ,  TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 11), 'MM/YYYY') DATDOZE
        //         ,  TIPMESDOZE
        //         ,  COALESCE(MOVMESTREZE,'-') MOVMESTREZE
        //         ,  TO_CHAR(ADD_MONTHS(TRUNC(SYSDATE), - 12), 'MM/YYYY') DATTREZE
        //         ,  TIPMESTREZE
        //         FROM TABCESTA
        //         GROUP BY CODPRDCES, DESPRDCES, MOVMESUM, TIPMESUM, MOVMESDOIS, TIPMESDOIS, MOVMESTRES, TIPMESTRES, MOVMESQUATRO, 
        //         TIPMESQUATRO, MOVMESCINCO, TIPMESCINCO, MOVMESSEIS, TIPMESSEIS, MOVMESSETE, TIPMESSETE, MOVMESOITO, TIPMESOITO,
        //         MOVMESNOVE, TIPMESNOVE, MOVMESDEZ, TIPMESDEZ, MOVMESONZE, TIPMESONZE, MOVMESDOZE, TIPMESDOZE, MOVMESTREZE, TIPMESTREZE
        //         ORDER BY CODPRDCES
        //        ";
    }
    public string obterServicosAtvEnc()
    {
        StringBuilder strBld = new StringBuilder(@"        SELECT GCS.CODCLI,
                                                                GCS.CODSVCTGV,
                                                                INITCAP(SR.DESSVCTGV) DESSVCTGV,
                                                                CASE WHEN GCS.INDSTAATV = 1 THEN 'EM IMPLANTAÇÃO'
                                                                    WHEN GCS.INDSTAATV = 2 THEN 'ATIVO'
                                                                    WHEN GCS.INDSTAATV = 3 THEN 'SUSPENSO'
                                                                    WHEN GCS.INDSTAATV = 4 THEN 'CANCELADO'  END STATUS,            
                                                                TO_CHAR(GCS.DATATVSVC,'DD/MM/YYYY') AS DATATVSVC,
                                                                TO_CHAR(GCS.DATINISUSSVC,'DD/MM/YYYY') AS DATINISUSSVC,       
                                                                TO_CHAR(GCS.DATFIMSUSSVC,'DD/MM/YYYY') AS DATFIMSUSSVC,
                                                                TO_CHAR(GCS.DATCNC,'DD/MM/YYYY') AS DATDESSVC,
                                                                (GCS.DATCNC - GCS.DATATVSVC) AS QNTDIAS,
                                                               INITCAP(NVL(CNC.DESMTVCNC, ' ')) AS MOTIVACANC
                                                            FROM MRT.CADCLISVCTGV GCS
                                                            INNER JOIN MRT.CADSVCTGV SR ON (GCS.CODSVCTGV = SR.CODSVCTGV)
                                                            LEFT JOIN MRT.CADMTVCNCSVCTGV CNC ON (CNC.CODMTVCNC = GCS.CODMTVCNC)
                                                            LEFT JOIN MRT.CADGRPEMPTGV CADGRUP ON(GCS.CODGRPEMPFAT = CADGRUP.CODGRPEMPCLI)
                                                            WHERE 1 = 1
                                                            AND GCS.CODCLI = :CODCLI");
        return strBld.ToString();
    }


    public string obterHistoricoSdm()
    {
        StringBuilder strBld = new StringBuilder(@"    SELECT RGT.CODTCKATD
                                                            ,TO_CHAR(RGT.DATSLCATD,'DD/MM/YYYY') AS DATSLCATD
                                                            ,TO_CHAR(SLC.DATFNLATD,'DD/MM/YYYY') AS DATFNLATD
                                                            ,INITCAP(UPPER(SVC.DESSVCCHM)) AS DESSVCCHM
                                                            ,INITCAP(UPPER(STA.DESSTASLCATD)) AS DESSTASLCATD
                                                        FROM MRT.CADRGTSLCATDSDM RGT
                                                            LEFT JOIN MRT.CADSLCSVCSDM SLC ON RGT.CODTCKATD = SLC.CODTCKATD
                                                            LEFT JOIN MRT.CADSVCSDM SVC ON SVC.CODSVCCHM = SLC.CODSVCCHM 
                                                            LEFT JOIN MRT.CADSTASLCATDSDM STA ON RGT.CODSTASLCATD = STA.CODSTASLCATD
                                                        WHERE RGT.CODEDESLTSVC  IN (2,3)
                                                            AND RGT.DESTIPSLCATD = 'Serviço'
                                                            AND CAST(RGT.DESUSRSLT AS INTEGER) = :CODCLI
                                                            ORDER BY SLC.DATFNLATD DESC");
        return strBld.ToString();
    }
}