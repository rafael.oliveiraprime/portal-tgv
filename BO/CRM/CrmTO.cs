﻿using System;
using System.Collections.Generic;

namespace CrmTO
{
    public class obterCrm
    {
        public System.Int64 CODCLI { get; set; }

        public System.Int64 CODCLIGRPFAT { get; set; }

        public string NOMCLIGRPFAT { get; set; }

        public string INDFATGRP { get; set; }

        public string CLIENTETGV { get; set; }

        public string NOMCLI { get; set; }

        public string NUMCGCCLI { get; set; }

        public string NUMCGCCLIGRPFAT { get; set; }

        public string NOMGRPEMPCLI { get; set; }

        public string INDDBTAUTPGT { get; set; }

        public Int64 CODBCO { get; set; }

        public Int64 CODAGEBCO { get; set; }

        public string NUMDIGVRFAGE { get; set; }

        public string NUMCNTCRRCTT { get; set; }

        public string NUMDIGVRFCNTCRR { get; set; }

        public System.Int64 CODGRPEMPCLI { get; set; }

        public int CODSITCLI { get; set; }

        public string STATUS { get; set; }

        public String STATUSMRT { get; set; }

    }

    public class obterCrmNegociacao
    {
        public System.Int64 CODCLI { get; set; }

        public System.Int64 CODCLIGRPFAT { get; set; }

        public string NOMCLIGRPFAT { get; set; }

        public string INDFATGRP { get; set; }

        public string CLIENTETGV { get; set; }

        public string NOMCLI { get; set; }

        public string NUMCGCCLI { get; set; }

        public string NUMCGCCLIGRPFAT { get; set; }

        public string NOMGRPEMPCLI { get; set; }

        public string INDDBTAUTPGT { get; set; }

        public Int64 CODBCO { get; set; }

        public Int64 CODAGEBCO { get; set; }

        public string NUMDIGVRFAGE { get; set; }

        public string NUMCNTCRRCTT { get; set; }

        public string NUMDIGVRFCNTCRR { get; set; }

        public System.Int64 CODGRPEMPCLI { get; set; }

        public string STATUS { get; set; }       

        public int CODSITCLI { get; set; }
    }

    public class obterEstruturaMartins
    {
        public string ORDEM { get; set; }

        public string EMPRESA { get; set; }

        public string FUNCAO { get; set; }

        public string MATRICULA { get; set; }

        public string NOME { get; set; }

        public string EMAIL { get; set; }

        public string TELEFONE { get; set; }

        public string SUPERIOR { get; set; }
    }

    public class obterEstruturaTribanco
    {
        public string ORDEM { get; set; }

        public string EMPRESA { get; set; }

        public string FUNCAO { get; set; }

        public string MATRICULA { get; set; }

        public string NOME { get; set; }

        public string EMAIL { get; set; }

        public string TELEFONE { get; set; }

        public string SUPERIOR { get; set; }
    }

    public class obterDadosBasicos
    {
        public int CODCLI { get; set; }

        public string NOMCLI { get; set; }

        public string NUMCGCCLI { get; set; }

        public string ENDERECO { get; set; }

        public string CODCEP { get; set; }

        public string NOMBAI { get; set; }

        public string NOMCID { get; set; }

        public string NOMCIDEST { get; set; }

        public string NUMTEL { get; set; }

        public string NUMTELCEL { get; set; }

        public string NUMINSESTCLI { get; set; }

        public string UNIDADENEGOCIO { get; set; }

        public string CANAL { get; set; }

        public string NOMATI { get; set; }

        public string SEGMENTOMERCADO { get; set; }

        public int QDECXACLI { get; set; }

        public string TAMAREFISVNDCLI { get; set; }

        public string TAMAREFISDPSCLI { get; set; }

    }

    public class obterGrupoEconomico
    {
        public int CODGRPEMPCLI { get; set; }

        public string INDFATGRP { get; set; }

        public string CODCLI { get; set; }

        public string NOMCLI { get; set; }

        public string NUMCGCCLI { get; set; }

        public string NOMCID { get; set; }

        public string CODESTUNI { get; set; }

        public string CODOPTVND { get; set; }

        public string STATUS { get; set; }
    }

    public class obterGridsEstaticas
    {
        public int CODGRPEMPCLI { get; set; }

        public string INDFATGRP { get; set; }

        public string CODCLI { get; set; }

        public string NOMCLI { get; set; }

        public string NUMCGCCLI { get; set; }

        public string NOMCID { get; set; }

        public string CODESTUNI { get; set; }

        public string CODOPTVND { get; set; }

        public string STATUS { get; set; }

        public List<obterGrupoEconomico> LSTGRPECO { get; set; }

        public List<obterEstruturaMartins> LSTESTMRT { get; set; }

        public List<obterEstruturaTribanco> LSTESTTRI { get; set; }

        public List<obterContatosTelevendas> LSTCNTTLV { get; set; }

        public List<obterHistoricoSdm> LSTHISSDM { get; set; }
    }

    public class obterCestaProdutoCliente
    {
        public int CODPRDCES { get; set; }

        public string DESPRDCES { get; set; }

        public string MOVMESUM { get; set; }
        public string MOVMESDOIS { get; set; }
        public string MOVMESTRES { get; set; }
        public string MOVMESQUATRO { get; set; }
        public string MOVMESCINCO { get; set; }
        public string MOVMESSEIS { get; set; }
        public string MOVMESSETE { get; set; }
        public string MOVMESOITO { get; set; }
        public string MOVMESNOVE { get; set; }
        public string MOVMESDEZ { get; set; }
        public string MOVMESONZE { get; set; }
        public string MOVMESDOZE { get; set; }
        public string MOVMESTREZE { get; set; }


        public int TIPDDOATR { get; set; }
        public int NUMSEQ { get; set; }
        //public int TIPMESUM { get; set; }
        //public int TIPMESDOIS { get; set; }
        //public int TIPMESTRES { get; set; }
        //public int TIPMESQUATRO { get; set; }
        //public int TIPMESCINCO { get; set; }
        //public int TIPMESSEIS { get; set; }
        //public int TIPMESSETE { get; set; }
        //public int TIPMESOITO { get; set; }
        //public int TIPMESNOVE { get; set; }
        //public int TIPMESDEZ { get; set; }
        //public int TIPMESONZE { get; set; }
        //public int TIPMESDOZE { get; set; }
        //public int TIPMESTREZE { get; set; }
    }

    public class obterUltimos12Meses
    {
        public string DATUM { get; set; }
        public string DATDOIS { get; set; }
        public string DATTRES { get; set; }
        public string DATQUATRO { get; set; }
        public string DATCINCO { get; set; }
        public string DATSEIS { get; set; }
        public string DATSETE { get; set; }
        public string DATOITO { get; set; }
        public string DATNOVE { get; set; }
        public string DATDEZ { get; set; }
        public string DATONZE { get; set; }
        public string DATDOZE { get; set; }
        public string DATTREZE { get; set; }
    }


    public class Token
    {
        public string ExpiresIn { get; set; }
        public string AccessToken { get; set; }
    }

    public class DadosRecursoCliente
    {
        public string Autorizadora { get; set; }
        public string Categoria { get; set; }
        public Int64 Cliente { get; set; }
        public string Entidade { get; set; }
        public string IC { get; set; }
        public string Recursos { get; set; }
        public string Tipo { get; set; }
    }

    public class obterContatosTelevendas
    {
        public string NUMFAX { get; set; }
        public string NUMTLF { get; set; }
        public string NUMTLFCELCTOCLI { get; set; }
    }

    public class obterServicosAtvEnc
    {
        public string CODCLI { get; set; }
        public string CODSVCTGV { get; set; }
        public string DESSVCTGV { get; set; }
        public string STATUS { get; set; }
        public string DATATVSVC { get; set; }
        public string DATINISUSSVC { get; set; }
        public string DATFIMSUSSVC { get; set; }
        public string DATDESSVC { get; set; }
        public string QNTDIAS { get; set; }
        public string MOTIVACANC { get; set; }
    }

    public class obterHistoricoSdm
    {
        public int CODTCKATD { get; set; }
        public string DATSLCATD { get; set; }
        public string DATFNLATD { get; set; }
        public string DESSVCCHM { get; set; }
        public string DESSTASLCATD { get; set; }
    }

}