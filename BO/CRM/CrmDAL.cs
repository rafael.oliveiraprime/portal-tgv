﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

public class CrmDAL : DAL
{
    public List<CrmTO.obterCrm> obterCrm(CrmApiModel.obterCrmApiModel objPesquisar)
    {
        var DALSQL = new CrmDALSQL();
        string cmdSql = DALSQL.obterCrm(objPesquisar.CLITGV, objPesquisar.CODSTATUS);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("NUMCGCCLI", objPesquisar.NUMCGCCLI);
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("NOMCLI", objPesquisar.NOMCLI);
        dbCommand.AddWithValue("CODGRPEMPCLI", objPesquisar.CODGRPEMPCLI);

      
        if (objPesquisar.CODSTATUS > 4)
        switch (objPesquisar.CODSTATUS)
        {
            case 5://VENDA GANHA
                    objPesquisar.CODSTATUS = 3;
                    break;
            case 6://VENDA PERDIDA
                    objPesquisar.CODSTATUS = 4;
                    break;
            case 7://NEGOCIAÇÃO
                    objPesquisar.CODSTATUS = 1;
                    break;
            case 8://CONTRATO ENVIADO
                    objPesquisar.CODSTATUS = 2;
                break;
            case 9://AGUARDANDO APROVAÇÃO
                    objPesquisar.CODSTATUS = 5;
                break;
            case 10://DESCONTO APROVADO
                    objPesquisar.CODSTATUS = 6;
                break;
            case 11://DESCONTO REJEITADO
                    objPesquisar.CODSTATUS = 7;
                break;
            case 12://OPORTUNIDADE
                    objPesquisar.CODSTATUS = default(int);
                break;
            default:
                Console.WriteLine("Default case");
                break;
        }

        dbCommand.AddWithValue("CODSTATUS", objPesquisar.CODSTATUS);

            //
            dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CrmTO.obterCrm>();
    }

    public CrmTO.obterCrmNegociacao obterCrmNegociacao(CrmApiModel.obterCrmNegociacao objPesquisar)
    {
        var DALSQL = new CrmDALSQL();
        string cmdSql = DALSQL.obterCrmNegociacao();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        var retorno = MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CrmTO.obterCrmNegociacao>();
        if (retorno.Count > 0)
        {
            return retorno.FirstOrDefault();
        }
        else
        {
            return new CrmTO.obterCrmNegociacao();
        }
        
    }

    public List<CrmTO.obterEstruturaMartins> obterEstruturaMartins(CrmApiModel.obterEstruturaMartins objPesquisar)
    {
        var DALSQL = new CrmDALSQL();
        string cmdSql = DALSQL.obterEstruturaMartins();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //      
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CrmTO.obterEstruturaMartins>();
    }

    public List<CrmTO.obterEstruturaTribanco> obterEstruturaTribanco(CrmApiModel.obterEstruturaTribanco objPesquisar)
    {
        var DALSQL = new CrmDALSQL();
        string cmdSql = DALSQL.obterEstruturaTribanco();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //      
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CrmTO.obterEstruturaTribanco>();
    }


    public CrmTO.obterDadosBasicos obterDadosBasicos(CrmApiModel.obterDadosBasicos objPesquisar)
    {
        var DALSQL = new CrmDALSQL();
        string cmdSql = DALSQL.obterDadosBasicos();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("TIPENDCLI", objPesquisar.TIPENDCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        var retorno = MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CrmTO.obterDadosBasicos>();
        if (retorno.Count == 1)
        {
            return retorno.FirstOrDefault();
        }
        else
        {
            return new CrmTO.obterDadosBasicos();
        }
    }

    public List<CrmTO.obterGrupoEconomico> obterGrupoEconomico(CrmApiModel.obterGrupoEconomico objPesquisar)
    {
        var DALSQL = new CrmDALSQL();
        string cmdSql = DALSQL.obterGrupoEconomico();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //      
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CrmTO.obterGrupoEconomico>();
    }
    
    public List<CrmTO.obterContatosTelevendas> obterContatosTelevendas(CrmApiModel.obterContatosTelevendas objPesquisar)
    {
        var DALSQL = new CrmDALSQL();
        string cmdSql = DALSQL.obterContatosTelevendas();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //      
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CrmTO.obterContatosTelevendas>();
    }

    public List<CrmTO.obterCestaProdutoCliente> obterCestaProdutoCliente(CrmApiModel.obterCestaProdutoCliente objPesquisar)
    {
        var DALSQL = new CrmDALSQL();
        string cmdSql = DALSQL.obterCestaProdutoCliente();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //      
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        //dbCommand.AddWithValue("CODCLI1", objPesquisar.CODCLI);
        //dbCommand.AddWithValue("CODCLI2", objPesquisar.CODCLI);
        //dbCommand.AddWithValue("CODCLI3", objPesquisar.CODCLI);
        //dbCommand.AddWithValue("CODCLI4", objPesquisar.CODCLI);
        //dbCommand.AddWithValue("CODCLI5", objPesquisar.CODCLI);
        //dbCommand.AddWithValue("CODCLI6", objPesquisar.CODCLI);
        //dbCommand.AddWithValue("CODCLI7", objPesquisar.CODCLI);
        //dbCommand.AddWithValue("CODCLI8", objPesquisar.CODCLI);
        //dbCommand.AddWithValue("CODCLI9", objPesquisar.CODCLI);
        //dbCommand.AddWithValue("CODCLI10", objPesquisar.CODCLI);
        //dbCommand.AddWithValue("CODCLI11", objPesquisar.CODCLI);
        //dbCommand.AddWithValue("CODCLI12", objPesquisar.CODCLI);
        //dbCommand.AddWithValue("CODCLI13", objPesquisar.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal();
      
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CrmTO.obterCestaProdutoCliente>();
    }

    public List<CrmTO.obterServicosAtvEnc> obterServicosAtvEnc(CrmApiModel.obterContatosTelevendas objPesquisar)
    {
        var DALSQL = new CrmDALSQL();
        string cmdSql = DALSQL.obterServicosAtvEnc();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //      
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CrmTO.obterServicosAtvEnc>();
    }

    public List<CrmTO.obterHistoricoSdm> obterHistoricoSdm(CrmApiModel.obterHistoricoSdm objPesquisar)
    {
        var DALSQL = new CrmDALSQL();
        string cmdSql = DALSQL.obterHistoricoSdm();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //      
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CrmTO.obterHistoricoSdm>();
    }
}