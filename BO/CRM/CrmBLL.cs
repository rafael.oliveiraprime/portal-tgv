﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

using RestSharp;
using RestSharp.Authenticators;

using Newtonsoft.Json;
using System.Configuration;
using System.Net;
using System.Collections;

using System.Xml;


public class CrmBLL
{
    public List<CrmTO.obterCrm> obterCrm(CrmApiModel.obterCrmApiModel objPesquisar)
    {
        try
        {
            var DAL = new CrmDAL();

            if (objPesquisar.NUMCGCCLI != null)
            {
                if (objPesquisar.NUMCGCCLI != "-1")
                    objPesquisar.NUMCGCCLI = Regex.Replace(objPesquisar.NUMCGCCLI, "[/.-]", String.Empty);
                else
                    objPesquisar.NUMCGCCLI = null;
            }

            return DAL.obterCrm(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CRM.");
            Utilitario.InsereLog(ex, "obterCrm", "CrmApiModel.obterCrmApiModel", "CrmBLL", "ERRO AO FAZER A CONSULTA Do DO CRM.");
            throw;
        }
    }

    public CrmTO.obterCrmNegociacao obterCrmNegociacao(CrmApiModel.obterCrmNegociacao objPesquisar)
    {
        try
        {
            var DAL = new CrmDAL();
            return DAL.obterCrmNegociacao(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO CRM NEGOCIACAO.");
            Utilitario.InsereLog(ex, "obterCrmNegociacao", "CrmApiModel.obterCrmNegociacao", "CrmBLL", "ERRO AO FAZER A CONSULTA DO CRM NEGOCIACAO.");
            throw;
        }
    }

    public List<CrmTO.obterEstruturaMartins> obterEstruturaMartins(CrmApiModel.obterEstruturaMartins objPesquisar)
    {
        try
        {
            var DAL = new CrmDAL();
            return DAL.obterEstruturaMartins(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DA ESTRUTURA BASICA.");
            Utilitario.InsereLog(ex, "obterEstruturaMartins", "CrmApiModel.obterEstruturaMartins", "CrmBLL", "ERRO AO FAZER A CONSULTA DA ESTRUTURA MARTINS.");
            throw;
        }
    }

    public CrmTO.obterDadosBasicos obterDadosBasicos(CrmApiModel.obterDadosBasicos objPesquisar)
    {
        try
        {
            var DAL = new CrmDAL();
            return DAL.obterDadosBasicos(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DOS DADOS BASICOS CLIENTE.");
            Utilitario.InsereLog(ex, "obterDadosBasicos", "CrmApiModel.obterDadosBasicos", "CrmBLL", "ERRO AO FAZER A CONSULTA DOS DADOS BASICOS CLIENTE.");
            throw;
        }
    }

    public List<CrmTO.obterGrupoEconomico> obterGrupoEconomico(CrmApiModel.obterGrupoEconomico objPesquisar)
    {
        try
        {
            var DAL = new CrmDAL();
            return DAL.obterGrupoEconomico(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO GRUPO ECONOMICO.");
            Utilitario.InsereLog(ex, "obterGrupoEconomico", "CrmApiModel.obterGrupoEconomico", "CrmBLL", "ERRO AO FAZER A CONSULTA DO GRUPO ECONOMICO.");
            throw;
        }
    }

    public List<CrmTO.obterCestaProdutoCliente> obterCestaProdutoCliente(CrmApiModel.obterCestaProdutoCliente objPesquisar)
    {
        try
        {
            var DAL = new CrmDAL();
            return DAL.obterCestaProdutoCliente(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DA CESTA PRODUTO DO CLIENTE.");
            Utilitario.InsereLog(ex, "obterCestaProdutoCliente", "CrmApiModel.obterGrupoEconomico", "CrmBLL", "ERRO AO FAZER A CONSULTA DA CESTA PRODUTO DO CLIENTE.");
            throw;
        }
    }

    /// <summary>
    /// Consulta de todas as grids estaticas com somente um ajax.
    /// </summary>
    public CrmTO.obterGridsEstaticas obterGridsEstaticas(CrmApiModel.obterGridsEstaticas objPesquisar)
    {

        //Lista que sera retornada, essa lista tem listas de todas as grids necessárias
        CrmTO.obterGridsEstaticas _listaDados = new CrmTO.obterGridsEstaticas();


        //Declaração daS Api'sModel que que trazem os filtros, nesse caso como é chamada somente a api model direto, depois é necessario repassar
        //as informações necessárias
        CrmApiModel.obterGrupoEconomico objPesquisarGrpEco = new CrmApiModel.obterGrupoEconomico();
        CrmApiModel.obterEstruturaMartins objPesquisarEstruturaMartins = new CrmApiModel.obterEstruturaMartins();
        CrmApiModel.obterEstruturaTribanco objPesquisarEstruturaTribanco = new CrmApiModel.obterEstruturaTribanco();
        CrmApiModel.obterHistoricoSdm objPesquisarHistoricoSdm = new CrmApiModel.obterHistoricoSdm();


        //Repasse das informações da ApiModelGeral para as que enviam a BLL de cada consulta.
        objPesquisarGrpEco.CODCLI = objPesquisar.CODCLI;
        objPesquisarEstruturaMartins.CODCLI = objPesquisar.CODCLI;
        objPesquisarEstruturaTribanco.CODCLI = objPesquisar.CODCLI;
        objPesquisarHistoricoSdm.CODCLI = objPesquisar.CODCLI;
        
        try
        {
            var DAL = new CrmDAL();
            
            //Execução das consultas
            _listaDados.LSTGRPECO = DAL.obterGrupoEconomico(objPesquisarGrpEco);
            _listaDados.LSTESTMRT = DAL.obterEstruturaMartins(objPesquisarEstruturaMartins);
            _listaDados.LSTESTTRI = DAL.obterEstruturaTribanco(objPesquisarEstruturaTribanco);
            _listaDados.LSTHISSDM = DAL.obterHistoricoSdm(objPesquisarHistoricoSdm);

            //Retorno de todas as listas consultadas para serem tratadas no FRONT.
            return _listaDados;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DAS GRIDS ESTATICAS.");
            Utilitario.InsereLog(ex, "obterGridsEstaticas", "CrmApiModel.obterGridsEstaticas", "CrmBLL", "ERRO AO FAZER A CONSULTA DAS GRIDS ESTATICAS.");
            throw;
        }
    }


    public List<CrmTO.DadosRecursoCliente> ObterRecursosCliente(CrmApiModel.obterCestaProdutoCliente objPesquisar)
    {

        List<CrmTO.DadosRecursoCliente> result = new List<CrmTO.DadosRecursoCliente>();
        string client_id = "";
        string client_secret = "";

        try
        {


            string path = ConfigurationManager.AppSettings["pathCredenciais"];
            ConfigurationFileMap fileMap = new ConfigurationFileMap(path); //Path to your config file
            Configuration configuration = ConfigurationManager.OpenMappedMachineConfiguration(fileMap);
            ConfigurationSection _value = configuration.Sections["appSettings"];

            XmlDocument _credentials = new XmlDocument();
            _credentials.LoadXml(_value.SectionInformation.GetRawXml());

            foreach (XmlNode node in _credentials.SelectNodes("/appSettings/add"))
            {
                if (node.Attributes[0].Value.ToString() == "client_id")
                    client_id = node.Attributes[1].Value.ToString();

                if (node.Attributes[0].Value.ToString() == "client_secret")
                    client_secret = node.Attributes[1].Value.ToString();
            }



            CrmTO.Token _token = Arquitetura.Classes.Util.GetToken(client_id, client_secret);

            //string url = "https://framework2014homologa.martins.com.br/SERVICEDESK.RECURSOCLIENTEAPI/RecursoCliente?codCliente=180";    

            string url = ConfigurationManager.AppSettings["urlApi"] + objPesquisar.CODCLI;

            RestClient client = new RestClient(url);
            RestRequest request = new RestRequest(Method.GET);


            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("client_id", client_id);
            request.AddHeader("access_token", _token.AccessToken);
            request.AddHeader("Content-Type", "application/json");

            IRestResponse response = client.Execute(request);

            if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                string json = response.Content;
                //var erro = JsonConvert.DeserializeObject<BadRequestResponse>(json);
                //throw new ExceptionApi(erro.Message);
            }
            else if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                string json = response.Content;
                //var erro = JsonConvert.DeserializeObject<InternalServerErrorResponse>(json);
                //throw new Exception(erro.ExceptionMessage);
            }

            if (response.StatusCode == HttpStatusCode.OK)
            {
                string json = response.Content;
                result = JsonConvert.DeserializeObject<List<CrmTO.DadosRecursoCliente>>(json) as List<CrmTO.DadosRecursoCliente>;
            }
            else if (response.StatusCode == HttpStatusCode.NotFound)
            {
                //retorno.Resultado = new DetalhesTicket();
            }


            return result;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DOS RECURSOS DOS CLIENTES.");
            Utilitario.InsereLog(ex, "ObterRecursosCliente", "CrmApiModel.obterCestaProdutoCliente objPesquisar", "CrmBLL", "ERRO AO FAZER A CONSULTA DOS RECURSOS DOS CLIENTES.");
            throw;
        }
    }

    public static string GetTemplateHtml(string path)
    {
        var logicPath = System.Web.HttpContext.Current.Server.MapPath(path);
        return System.IO.File.ReadAllText(logicPath);
    }

    public List<CrmTO.obterContatosTelevendas> obterContatosTelevendas(CrmApiModel.obterContatosTelevendas objPesquisar)
    {


        try
        {
            List<CrmTO.obterContatosTelevendas> _listRetornoDal = new List<CrmTO.obterContatosTelevendas>();
            List<CrmTO.obterContatosTelevendas> _listRetorno = new List<CrmTO.obterContatosTelevendas>();
            CrmTO.obterContatosTelevendas _itemRetorno = new CrmTO.obterContatosTelevendas();

            string TELEFONES = "";


            var DAL = new CrmDAL();
            _listRetornoDal = DAL.obterContatosTelevendas(objPesquisar);


            foreach(CrmTO.obterContatosTelevendas _item in _listRetornoDal)
            {
                _itemRetorno.NUMFAX = _item.NUMFAX;
                _itemRetorno.NUMTLF = _item.NUMTLF;
                _itemRetorno.NUMTLFCELCTOCLI = _item.NUMTLFCELCTOCLI;

                if (!TELEFONES.Contains(_item.NUMFAX) && !TELEFONES.Contains(_item.NUMTLF) && !TELEFONES.Contains(_item.NUMTLFCELCTOCLI))
                {
                    TELEFONES += _item.NUMFAX + ";" + _item.NUMTLF + ";" + _item.NUMTLFCELCTOCLI + ";";
                    _listRetorno.Add(_itemRetorno);
                }

                _itemRetorno = new CrmTO.obterContatosTelevendas();

            }


            return _listRetorno;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO GRUPO ECONOMICO.");
            Utilitario.InsereLog(ex, "obterContatosTelevendas", "CrmApiModel.obterContatosTelevendas", "CrmBLL", "ERRO AO FAZER A CONSULTA DO GRUPO ECONOMICO.");
            throw;
        }
    }

    public List<CrmTO.obterServicosAtvEnc> obterServicosAtvEnc(CrmApiModel.obterContatosTelevendas objPesquisar)
    {
        try
        {
            var DAL = new CrmDAL();
            return DAL.obterServicosAtvEnc(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO GRUPO ECONOMICO.");
            Utilitario.InsereLog(ex, "obterServicosAtvEnc", "CrmApiModel.obterServicosAtvEnc", "CrmBLL", "ERRO AO FAZER A CONSULTA DOS SERVIÇOS.");
            throw;
        }
    }


}