﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class RelVencimentoPagamentoDAL : DAL
{
    public List<RelVencimentoPagamentoTO.ObterVencimentoPagamento> ObterVencimentoPagamento(RelVencimentoPagamentoApiModel.ObterVencimentoPagamento objPesquisar)
    {
        var DALSQL = new RelVencimentoPagamentoDALSQL();
        string cmdSql = DALSQL.ObterVencimentoPagamento(objPesquisar.LISTCODSTATIT);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //    
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("NOMCLI", objPesquisar.NOMCLI);
        dbCommand.AddWithValue("NUMCGCCLI", objPesquisar.CNPJ);
        dbCommand.AddWithValue("CODSITCLI", objPesquisar.CODSITCLI);
        dbCommand.AddWithValue("DATVNCTIT", objPesquisar.DATVNCTIT);
        dbCommand.AddWithValue("DATPGTTITINI", objPesquisar.DATPGTTITINI);
        dbCommand.AddWithValue("DATPGTTITFIM", objPesquisar.DATPGTTITFIM);        
        dbCommand.AddWithValue("DATIMPRESSINI", objPesquisar.DATIMPRESSINI);
        dbCommand.AddWithValue("DATIMPRESSFIM", objPesquisar.DATIMPRESSFIM);

        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<RelVencimentoPagamentoTO.ObterVencimentoPagamento>();
    }
}