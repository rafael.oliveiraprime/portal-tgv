﻿using System.Linq;
using System.Collections.Generic;

public class RelVencimentoPagamentoDALSQL
{
    public string ObterVencimentoPagamento(List<long> LISTCODSTATIT)
    {
        return @"   SELECT  CAD.CODCLI AS CODIGO
                           ,CLI.NOMCLI AS RAZAO
                           ,CLI.NUMCGCCLI AS CNPJ
                           ,CASE
                              WHEN COT.CODSITCLI = 1 THEN 'EM IMPLANTAÇÃO'
                              WHEN COT.CODSITCLI = 2 THEN 'ATIVO'
                              WHEN COT.CODSITCLI = 3 THEN 'SUSPENSO'
                              WHEN COT.CODSITCLI = 4 THEN 'CANCELADO'
                            END AS STATUS
                           ,TO_CHAR(CAD.DATVNCTIT,'DD/MM/YYYY') AS VENCIMENTO
                           ,TO_CHAR(CAD.DATPGTTIT,'DD/MM/YYYY') AS PAGAMENTO
                           ,CASE WHEN CAD.CODSTATIT = 1 THEN 'ABERTO'
                                 WHEN CAD.CODSTATIT = 2 THEN 'PAGO'  
                                 WHEN CAD.CODSTATIT = 3 THEN 'ATRASO'
                                 WHEN CAD.CODSTATIT = 5 THEN 'PREJUÍZO'
                                 WHEN CAD.CODSTATIT = 4 THEN 'CANCELADO' END STATUS_TIT
                           ,CAD.VLRPCLTIT
                           ,CAD.VLRSLDTIT
                           ,CAD.NUMTITCOBFAT
                           ,TO_CHAR(MOV.DATIPRNOTFSCSVC,'DD/MM/YYYY') AS DATIPRNOTFSCSVC
                      FROM MRT.MOVTITTGV CAD
                     INNER JOIN MRT.T0100043 CLI ON CAD.CODCLI = CLI.CODCLI
                     INNER JOIN MRT.CADCLITGV COT ON CAD.CODCLI = COT.CODCLI
                     INNER JOIN MRT.MOVMESFATCLITGV MOV ON(CAD.ANOMESREF = MOV.ANOMESREF AND CAD.CODCLI = MOV.CODCLI AND CAD.INDFATSVC = MOV.INDFATSVC AND CAD.NUMLOTPRENOTFSCSVC = MOV.NUMLOTPRENOTFSCSVC)
                     WHERE 1 = 1
                       AND CAD.CODCLI = :CODCLI
                       AND CLI.NOMCLI LIKE '%' || UPPER(:NOMCLI) || '%'
                       AND CLI.NUMCGCCLI = :NUMCGCCLI
                       AND COT.CODSITCLI = :CODSITCLI
                       AND TRUNC(CAD.DATPGTTIT) BETWEEN TRUNC(TO_DATE(:DATPGTTITINI,'DD/MM/YYYY'))  AND TRUNC(TO_DATE(:DATPGTTITFIM,'DD/MM/YYYY'))
                       AND TRUNC(MOV.DATIPRNOTFSCSVC) BETWEEN TRUNC(TO_DATE(:DATIMPRESSINI,'DD/MM/YYYY'))  AND TRUNC(TO_DATE(:DATIMPRESSFIM,'DD/MM/YYYY'))
                       AND CAD.CODSTATIT IN(#LISTCODSTATIT)
                    ".Replace("#LISTCODSTATIT", Utilitario.ToListStr(LISTCODSTATIT));
        ;



    }

}