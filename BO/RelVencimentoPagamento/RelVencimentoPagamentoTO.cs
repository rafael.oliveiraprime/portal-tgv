﻿
namespace RelVencimentoPagamentoTO
{
    public class ObterVencimentoPagamento
    {
        public int CODIGO { get; set; }
        public string RAZAO { get; set; }
        public string CNPJ { get; set; }
        public string STATUS { get; set; }
        public string PAGAMENTO { get; set; }
        public string VENCIMENTO { get; set; }
        public string STATUS_TIT { get; set; }
        public decimal VLRPCLTIT { get; set; }
        public decimal VLRSLDTIT { get; set; }
        public decimal NUMTITCOBFAT { get; set; }
        public string DATIPRNOTFSCSVC { get; set; }

    }
}