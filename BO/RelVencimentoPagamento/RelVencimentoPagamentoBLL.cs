﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;


public class RelVencimentoPagamentoBLL
{
    public List<RelVencimentoPagamentoTO.ObterVencimentoPagamento> ObterVencimentoPagamento(RelVencimentoPagamentoApiModel.ObterVencimentoPagamento objPesquisar)
    {
        try
        {
            var DAL = new RelVencimentoPagamentoDAL();

            if (objPesquisar.CNPJ != null)
            {
                objPesquisar.CNPJ = Regex.Replace(objPesquisar.CNPJ, "[/.-]", String.Empty);
            }

            return DAL.ObterVencimentoPagamento(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DO RELATORIO DE VENCIMENTO E PAGAMENTO.");
            Utilitario.InsereLog(ex, "ObterVencimentoPagamento", "RelVencimentoPagamentoApiModel.ObterVencimentoPagamento", "RelVencimentoPagamentoBLL", "ERRO AO FAZER A CONSULTA DO RELATORIO DE VENCIMENTO E PAGAMENTO.");
            throw;
        }
    }
}