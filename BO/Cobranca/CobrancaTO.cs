﻿using System.Collections.Generic;
using System;

namespace CobrancaTO
{
    public class obterCliTitulos
    {
        public Int64 CODCLI { get; set; }
        public string NOMCLI { get; set; }
        public string NUMCGCCLI { get; set; }
        public string NOMRES { get; set; }
        public string DATAGDCTO { get; set; }
        public decimal VLRSLDTIT { get; set; }
        public string STATUS { get; set; }
        public string FORMAPG { get; set; }
    }

    public class ultimoLoteEnviado
    {
        public Int64 ANOMESREF { get; set; }
        public Int64 NUMLOTPRENOTFSCSVC { get; set; }
    }


    public class obterNotasTitulos
    {
        public Int64 CODCLI { get; set; }
        public int NUMNOTFSCSVC { get; set; }
        public int NUMTITCOBFAT { get; set; }
        public string DATVNCTIT { get; set; }
        public decimal VLRPCLTIT { get; set; }
        public int NUMPCLTITNOTFSCSVC { get; set; }
        public decimal VALORPAGO { get; set; }
        public string DATPGTTIT { get; set; }
        public string STATUS { get; set; }
        public string FORMAPAG { get; set; }

    }


}