﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Transactions;

public class CobrancaBLL
{
    public string buscartitulos()
    {
        CobrancaTO.ultimoLoteEnviado _item = new CobrancaTO.ultimoLoteEnviado();
        CobrancaApiModel.CobrancaApiModel _itemtitulos = new CobrancaApiModel.CobrancaApiModel();


        try
        {
            var DAL = new CobrancaDAL();
            _item = DAL.buscaUltimoLoteEnviado();

            TimeSpan tsTimeout = TimeSpan.FromMinutes(5);

            if (_item != null)
            {

                using (TransactionScope Scope = new TransactionScope(TransactionScopeOption.Required, tsTimeout))
                {

                    var readertitulos = DAL.obterTitulosFatEnviada(_item);
                    while (readertitulos.Read())
                    {
                        _itemtitulos.ANOMESREF = readertitulos.GetInt32(0);
                        _itemtitulos.INDFATSVC = readertitulos.GetInt32(1);
                        _itemtitulos.CODCLI = readertitulos.GetInt32(2);
                        _itemtitulos.NUMTITCOBFAT = readertitulos.GetInt32(3);
                        _itemtitulos.CODFILEMP = readertitulos.GetInt32(4);
                        _itemtitulos.DATVNCTIT = readertitulos.GetDateTime(5).ToString();
                        _itemtitulos.NUMPCLTITNOTFSCSVC = readertitulos.GetInt32(6);
                        _itemtitulos.INDDBTAUTPGT = readertitulos.GetInt32(7);
                        _itemtitulos.VLRPCLTIT = readertitulos.GetDecimal(8);
                        _itemtitulos.CODSTATIT = readertitulos.GetInt32(9);
                        _itemtitulos.NUMNOTFSCSVC = readertitulos.GetInt32(10);
                        _itemtitulos.DATIPRNOTFSCSVC = readertitulos.GetDateTime(11).ToString();

                        DAL.inserirtitulos(_itemtitulos);
                        //DAL.updatenumtitmovcad(_itemtitulos);

                        _item = new CobrancaTO.ultimoLoteEnviado();
                    }

                    Scope.Complete();
                }
                
            }

            return "0";
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DA GESTÃO DE CONTAS.");
            Utilitario.InsereLog(ex, "obterGestaoContas", "CadastroContatoApiModel.CadastroContatoApiModel", "GestaoContasBLL", "ERRO AO FAZER A CONSULTA DA GESTÃO DE CONTAS.");
            throw;
        }
    }

    public List<CobrancaTO.obterCliTitulos> obterCliTitulos(CobrancaApiModel.CobrancaApiModel _ObjPesquisar)
    {
        try
        {
            var DAL = new CobrancaDAL();
            return DAL.obterCliTitulos(_ObjPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO OBTER CLIENTES TITULOS");
            Utilitario.InsereLog(ex, "obterCliTitulos", " ", "GestaoContasBLL", "ERRO AO OBTER CLIENTES TITULOS");
            throw;
        }
    }


    
    public List<CobrancaTO.obterNotasTitulos> ObterNotasFiscais(CobrancaApiModel.CobrancaApiModel _ObjPesquisar)
    {
        try
        {
            var DAL = new CobrancaDAL();
            return DAL.ObterNotasFiscais(_ObjPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO OBTER NOTAS FISCAIS DO CLIENTE");
            Utilitario.InsereLog(ex, "ObterNotasFiscais", " ", "GestaoContasBLL", "ERRO AO OBTER NOTAS FISCAIS DO CLIENTE");
            throw;
        }
    }






}