﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrmApiModel;
using System.Data;

public class CobrancaDAL : DAL
{
    
    public CobrancaTO.ultimoLoteEnviado buscaUltimoLoteEnviado()
    {
        try
        {
            var DALSQL = new CobrancaDALSQL();
            string cmdSql = DALSQL.buscaUltimoLoteEnviado();

            var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
            //
            dbCommand.TrataDbCommandUniversal(true, true, true, true);
            //
            return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CobrancaTO.ultimoLoteEnviado>().FirstOrDefault();
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO ANO MÊS ULTIMA FATURA.");
            Utilitario.InsereLog(ex, "buscartitulos", "", "CobrancaDAL", "ERRO ANO MÊS ULTIMA FATURA.");
            throw;
        }
    }

    public int VerificaBusca(CobrancaTO.ultimoLoteEnviado _item)
    {
        var DALSQL = new CobrancaDALSQL();
        string cmdSql = DALSQL.VerificaBusca();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        dbCommand.AddWithValue("ANOMESREF", _item.ANOMESREF);
        //
        return Convert.ToInt32(MRT001.ExecuteScalar(dbCommand));
    }

    public IDataReader obterTitulosFatEnviada(CobrancaTO.ultimoLoteEnviado _item)
    {
        var DALSQL = new CobrancaDALSQL();
        string cmdSql = DALSQL.obterTitulosFatEnviada();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //              
        dbCommand.AddWithValue("ANOMESREF", _item.ANOMESREF);
        dbCommand.AddWithValue("NUMLOTPRENOTFSCSVC", _item.NUMLOTPRENOTFSCSVC);
        //
        return MRT001.ExecuteReader(dbCommand);
    }

    public bool inserirtitulos(CobrancaApiModel.CobrancaApiModel _objInserir)
    {
        var DALSQL = new CobrancaDALSQL();
        string cmdSql = DALSQL.inserirtitulos();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", _objInserir.ANOMESREF);
        dbCommand.AddWithValue("INDFATSVC", _objInserir.INDFATSVC);
        dbCommand.AddWithValue("CODCLI", _objInserir.CODCLI);
        dbCommand.AddWithValue("NUMTITCOBFAT", _objInserir.NUMTITCOBFAT);
        dbCommand.AddWithValue("CODFILEMP", _objInserir.CODFILEMP);
        dbCommand.AddWithValue("DATVNCTIT", DateTime.Parse(_objInserir.DATVNCTIT));
        dbCommand.AddWithValue("NUMPCLTITNOTFSCSVC", _objInserir.NUMPCLTITNOTFSCSVC);
        dbCommand.AddWithValue("INDDBTAUTPGT", _objInserir.INDDBTAUTPGT);
        dbCommand.AddWithValue("VLRPCLTIT", _objInserir.VLRPCLTIT);
        dbCommand.AddWithValue("CODSTATIT", _objInserir.CODSTATIT);        
        dbCommand.AddWithValue("VLRSLDTIT", _objInserir.VLRPCLTIT);
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }

    public bool updatenumtitmovcad(CobrancaApiModel.CobrancaApiModel _objInserir)
    {
        var DALSQL = new CobrancaDALSQL();
        string cmdSql = DALSQL.updateMovcad();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("ANOMESREF", _objInserir.ANOMESREF);
        dbCommand.AddWithValue("CODCLI", _objInserir.CODCLI);
        dbCommand.AddWithValue("DATIPRNOTFSCSVC",DateTime.Parse(_objInserir.DATIPRNOTFSCSVC));
        dbCommand.AddWithValue("NUMPCLTITNOTFSCSVC", _objInserir.NUMPCLTITNOTFSCSVC);
        
        //
        dbCommand.TrataDbCommandUniversal();
        //
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public List<CobrancaTO.obterCliTitulos> obterCliTitulos(CobrancaApiModel.CobrancaApiModel _ObjPesquisar)
    {
        var DALSQL = new CobrancaDALSQL();
        string cmdSql = DALSQL.obterCliTitulos();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", _ObjPesquisar.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true,true,true,true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CobrancaTO.obterCliTitulos>();
    }

    public List<CobrancaTO.obterNotasTitulos> ObterNotasFiscais(CobrancaApiModel.CobrancaApiModel _ObjPesquisar)
    {
        var DALSQL = new CobrancaDALSQL();
        string cmdSql = DALSQL.ObterNotasFiscais();
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue("CODCLI", _ObjPesquisar.CODCLI);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CobrancaTO.obterNotasTitulos>();
    }
}