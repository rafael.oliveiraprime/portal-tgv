﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class CobrancaDALSQL
{
    public string buscaUltimoLoteEnviado()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT ANOMESREF, NUMLOTPRENOTFSCSVC
                                        FROM MRT.MOVMESFATRSMTGV
                                       WHERE ANOMESREF = (SELECT MAX(ANOMESREF)
                                                            FROM MRT.MOVMESFATRSMTGV
                                                           WHERE DATENVFAT IS NOT NULL)  

                                   ");
        return strBld.ToString();
    }

    public string VerificaBusca()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT COUNT(ANOMESREF)
                                        FROM MRT.MOVTITTGV
                                       WHERE ANOMESREF = :ANOMESREF
                                   ");
        return strBld.ToString();
    }


    public string obterTitulosFatEnviada()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT MOVCAP.ANOMESREF
                                            ,MOV.INDFATSVC
                                            ,MOV.CODCLI
                                            ,TIT.NUMTITFATCLI
                                            ,TIT.CODFILEMP
                                            ,TIT.DATVNCTIT
                                            ,TIT.NUMPCLTITNOTFSCSVC
                                            ,MOV.INDDBTAUTPGT
                                            ,TIT.VLRPCLTIT
                                            ,1 AS CODSTATIT
                                            ,TIT.NUMNOTFSCSVC 
                                            ,TIT.DATIPRNOTFSCSVC
                                        FROM MRT.MOVMESFATRSMTGV MOVCAP
                                        INNER JOIN MRT.MOVMESFATCLITGV MOV ON(MOVCAP.ANOMESREF = MOV.ANOMESREF )
                                        INNER JOIN MRT.CADPRENOTFSCSVC CADPRE ON(CADPRE.NUMLOTPRENOTFSCSVC = MOVCAP.NUMLOTPRENOTFSCSVC AND MOV.CODCLI = CADPRE.CODDSNNOTFSC)
                                        INNER JOIN MRT.T0145594 TIT ON(CADPRE.NUMNOTFSCSVC = TIT.NUMNOTFSCSVC AND CADPRE.CODEMP = TIT.CODEMP AND CADPRE.CODFILEMP = TIT.CODFILEMP)
                                        WHERE MOVCAP.ANOMESREF = :ANOMESREF
                                        AND MOVCAP.NUMLOTPRENOTFSCSVC = :NUMLOTPRENOTFSCSVC
                                   ");
        return strBld.ToString();
    }

    public string inserirtitulos()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" INSERT INTO MRT.MOVTITTGV(ANOMESREF, INDFATSVC, CODCLI, NUMTITCOBFAT, CODFILEMP, DATVNCTIT, NUMPCLTITNOTFSCSVC, INDDBTAUTPGT, VLRPCLTIT, CODSTATIT, VLRSLDTIT)
                                                         VALUES(:ANOMESREF, :INDFATSVC, :CODCLI, :NUMTITCOBFAT, :CODFILEMP, :DATVNCTIT, :NUMPCLTITNOTFSCSVC, :INDDBTAUTPGT, :VLRPCLTIT, :CODSTATIT, :VLRSLDTIT)
                                   ");

        return strBld.ToString();
    }


    public string updateMovcad()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" UPDATE MRT.MOVMESFATCLITGV SET NUMNOTFSCSVC = :NUMNOTFSCSVC,
                                                                     DATIPRNOTFSCSVC = :DATIPRNOTFSCSVC
                                       WHERE ANOMESREF = :ANOMESREF
                                         AND    CODCLI = :CODCLI ");

        return strBld.ToString();
    }


    public string obterCliTitulos()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT GC.CODCLI
                                             ,CLI.NUMCGCCLI
                                             ,CLI.NOMCLI
                                             ,CASE WHEN GC.INDDBTAUTPGT = 1 THEN 'DEBITO AUTOMATICO'
                                                   WHEN GC.INDDBTAUTPGT = 0 THEN 'BOLETO' END FORMAPG                   
                                             ,CASE WHEN GC.CODSITCLI = 1 THEN 'EM IMPLANTAÇÃO'
                                                   WHEN GC.CODSITCLI = 2 THEN 'ATIVO'
                                                   WHEN GC.CODSITCLI = 3 THEN 'SUSPENSO'
                                                   WHEN GC.CODSITCLI = 4 THEN 'CANCELADO'  END STATUS
                                             ,SUM(TIT.VLRSLDTIT) AS VLRSLDTIT        
                                             ,NVL(FNC.NOMFNC, ' ') AS NOMRES
                                             ,(SELECT COALESCE(TO_CHAR(MAX(ANT.DATAGDCTO), 'DD/MM/YYYY HH24:MM'),' ') FROM MRT.CADOBSCLITGV ANT WHERE GC.CODCLI = ANT.CODCLI) AS DATAGDCTO
                                        FROM MRT.CADCLITGV GC 
                                       INNER JOIN MRT.T0100043 CLI ON (GC.CODCLI = CLI.CODCLI) 
                                       INNER JOIN MRT.MOVTITTGV TIT ON (CLI.CODCLI = TIT.CODCLI)  
                                        LEFT JOIN MRT.T0100361 FNC ON (FNC.CODFNC = TIT.CODRPN)  
                                       WHERE 1 = 1  
                                         AND TIT.CODCLI = :CODCLI
                                       GROUP BY GC.CODCLI
                                               ,CLI.NUMCGCCLI
                                               ,CLI.NOMCLI
                                               ,GC.INDDBTAUTPGT
                                               ,GC.CODSITCLI
                                               ,FNC.NOMFNC
                                  ");

        return strBld.ToString();
    }



    public string ObterNotasFiscais()
    {
        StringBuilder strBld;

        strBld = new StringBuilder(@" SELECT GC.CODCLI
                                             ,MOV.NUMNOTFSCSVC 
                                             ,TIT.NUMTITCOBFAT                                             
                                             ,TO_CHAR(TIT.DATVNCTIT,'DD/MM/YYYY') AS DATVNCTIT
                                             ,TIT.VLRPCLTIT
                                             ,TIT.NUMPCLTITNOTFSCSVC
                                             ,CASE WHEN (TIT.VLRPCLTIT - VLRSLDTIT) = 0 THEN TIT.VLRPCLTIT
                                                    ELSE (TIT.VLRPCLTIT - VLRSLDTIT) END AS VALORPAGO
                                             ,TO_CHAR(TIT.DATPGTTIT,'DD/MM/YYYY') AS DATPGTTIT
                                             ,CASE WHEN TIT.CODSTATIT = 1 THEN 'ABERTO'
                                                   WHEN TIT.CODSTATIT = 2 THEN 'PAGO'  
                                                   WHEN TIT.CODSTATIT = 3 THEN 'PREJUÍZO'
                                                   WHEN TIT.CODSTATIT = 4 THEN 'CANCELADO' END STATUS                   
                                             ,CASE WHEN TIT.INDDBTAUTPGT = 0 THEN 'BOLETO'
                                                   WHEN TIT.INDDBTAUTPGT = 1 THEN 'DEBITO' END FORMAPAG       
                                        FROM MRT.CADCLITGV GC
                                       INNER JOIN MRT.MOVTITTGV TIT ON(GC.CODCLI = TIT.CODCLI)
                                       INNER JOIN MRT.MOVMESFATCLITGV MOV ON(TIT.ANOMESREF = MOV.ANOMESREF AND TIT.CODCLI = MOV.CODCLI)  
                                       WHERE TIT.CODCLI = :CODCLI   
                                       ORDER BY TIT.DATVNCTIT DESC
                                  ");
        return strBld.ToString();
    }



}