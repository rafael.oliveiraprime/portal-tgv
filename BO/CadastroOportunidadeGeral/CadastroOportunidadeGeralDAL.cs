﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

public class CadastroOportunidadeGeralDAL : DAL
{
    public List<CadastroOportunidadeGeralTO.obterCadastroOportunidadeGeral> obterCadastroOportunidadeGeral(CadastroOportunidadeGeralApiModel.CadastroOportunidadeGeralApiModel objPesquisar)
    {
        bool _escolheustatus = false;

        var DALSQL = new CadastroOportunidadeGeralDALSQL();


        if (objPesquisar.CODCLI > 0)
            _escolheustatus = true;


        //
        if (objPesquisar.CODOPTVND == 0)
        {
            objPesquisar.CODOPTVND = -1;
        }
        //
        if (objPesquisar.NUMCGCCLI == "-1")
        {
            objPesquisar.NUMCGCCLI = null;
        }
        string NUMCGCCLI;

        if (objPesquisar.NUMCGCCLI != null)
        {
            if (objPesquisar.NUMCGCCLI != "")
            {
                NUMCGCCLI = Regex.Replace(objPesquisar.NUMCGCCLI, "[/.-]", String.Empty);
                _escolheustatus = true;
            }
            else
                NUMCGCCLI = null;

        }
        else
        {
            NUMCGCCLI = null;
        }
        //
        if (objPesquisar.NUMCGCCLI == "-1")
        {
            objPesquisar.NUMCGCCLI = null;
        }
        string NUMCGCCLIFIL;

        if (objPesquisar.NUMCGCCLIFIL != null)
        {
            if (objPesquisar.NUMCGCCLIFIL != "")
            {
                NUMCGCCLIFIL = Regex.Replace(objPesquisar.NUMCGCCLIFIL, "[/.-]", String.Empty);
                _escolheustatus = true;
            }
            else
                NUMCGCCLIFIL = null;

        }
        else
        {
            NUMCGCCLIFIL = null;
        }


        if (objPesquisar.CODCNIVNDTGV > 0)
        {
            objPesquisar.CODCNIVNDTGVFIL = objPesquisar.CODCNIVNDTGV;
            _escolheustatus = true;
        }


        if (objPesquisar.CODCLIFIL > 0)
            _escolheustatus = true;

        if (objPesquisar.NOMCLI != null)
        {
            if (objPesquisar.NOMCLI != "")
                _escolheustatus = true;
        }

        if (objPesquisar.DATAGDCTOFIL != null)
        {
            if (objPesquisar.DATAGDCTOFIL != "")
                _escolheustatus = true;
        }

        if (objPesquisar.CODCNLORICLIFIL > 0)
            _escolheustatus = true;


        if (objPesquisar.NUMSEQPESINDFIL > 0)
            _escolheustatus = true;


        if (objPesquisar.CODCNIVNDTGVFIL > 0)
            _escolheustatus = false;


        if (objPesquisar.INDSTAOPTVND > 0)
            _escolheustatus = true;


        string cmdSql = DALSQL.obterCadastroOportunidadeGeral(objPesquisar.TIPOBUSCA, _escolheustatus);       
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);

        //
        dbCommand.AddWithValue("CODOPTVND", objPesquisar.CODOPTVND);
        dbCommand.AddWithValue("CODCLI", objPesquisar.CODCLI);
        dbCommand.AddWithValue("NUMCGCCLI", NUMCGCCLI);

        if (objPesquisar.TIPOBUSCA == 0)
        {
            if ((objPesquisar.INDSTAOPTVND == -1) || (objPesquisar.INDSTAOPTVND == 0))
                objPesquisar.INDSTAOPTVND = 1;
        }
        else
        {
            //if (objPesquisar.INDSTAOPTVND > 0)
                dbCommand.AddWithValue("INDSTAOPTVND", objPesquisar.INDSTAOPTVND);

        }

        Int64 _codfunc = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        if (_codfunc == 90900225)
            objPesquisar.NUMSEQPESINDFIL = 32654;


        dbCommand.AddWithValue("INDSTAOPTVNDFIL", objPesquisar.INDSTAOPTVNDFIL);
        dbCommand.AddWithValue("CODCLIFIL", objPesquisar.CODCLIFIL);
        dbCommand.AddWithValue("NOMCLIFIL", objPesquisar.NOMCLI);
        dbCommand.AddWithValue("NUMCGCCLIFIL", NUMCGCCLIFIL);
        dbCommand.AddWithValue("DATAGDCTOFIL", objPesquisar.DATAGDCTOFIL);
        dbCommand.AddWithValue("CODCNLORICLIFIL", objPesquisar.CODCNLORICLIFIL);
        dbCommand.AddWithValue("NUMSEQPESINDFIL", objPesquisar.NUMSEQPESINDFIL);
        dbCommand.AddWithValue("CODCNIVNDTGVFIL", objPesquisar.CODCNIVNDTGVFIL);
        //
        dbCommand.TrataDbCommandUniversal(true, true, true, true, true);
        //
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<CadastroOportunidadeGeralTO.obterCadastroOportunidadeGeral>();
    }

    public bool alterarResponsavelOportunidade(CadastroOportunidadeGeralApiModel.CadastroOportunidadeGeralApiModel objAlterar)
    {
        var DALSQL = new CadastroOportunidadeGeralDALSQL();
        string cmdSql = DALSQL.alterarResponsavelOportunidade(objAlterar.LISTCODOPTVND);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //            
        objAlterar.CODFNCALTRPN = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
        //              
        dbCommand.AddWithValue("CODCNIVNDTGV", objAlterar.CODCNIVNDTGV);
        dbCommand.AddWithValue("CODFNCALTRPN", objAlterar.CODFNCALTRPN);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return Convert.ToBoolean(MRT001.ExecuteNonQuery(dbCommand));
    }


    public List<GestaoCobrancaTO.TotaisEquipe> obterTotaisEquipes(CadastroOportunidadeGeralApiModel.CadastroOportunidadeGeralApiModel objPesquisar)
    {
        var DALSQL = new CadastroOportunidadeGeralDALSQL();
        string cmdSql = DALSQL.obterTotaisEquipes(objPesquisar.STATUS);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<GestaoCobrancaTO.TotaisEquipe>();
    }

    public List<GestaoCobrancaTO.TotaisEquipe> obterTotaisEquipesModal(CadastroOportunidadeGeralApiModel.CadastroOportunidadeGeralApiModel objPesquisar)
    {
        var DALSQL = new CadastroOportunidadeGeralDALSQL();
        string cmdSql = DALSQL.obterTotaisEquipesModal(objPesquisar.STATUS);
        //
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        //
        dbCommand.AddWithValue(":CODPFLACS", objPesquisar.CODPFLACS);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        return MRT001.ExecuteReader(dbCommand).DataReaderParaClasse<GestaoCobrancaTO.TotaisEquipe>();
    }

    public bool buscaCnpjCorreto(string CODCLI, string CNPJ)
    {
        var DALSQL = new CadastroOportunidadeGeralDALSQL();
        string cmdSql = DALSQL.buscaCnpjCorreto();
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        var NUMCGC = "";
        //
        dbCommand.AddWithValue(":CODCLI", CODCLI);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        var reader = MRT001.ExecuteReader(dbCommand);
        while (reader.Read())
        {
             NUMCGC = reader[1].ToString();           
        }
        if (NUMCGC == CNPJ) return true;
        else return false;
    }

    public int validaTipoNegoc(string DESTIPNGC)
    {
        var DALSQL = new CadastroOportunidadeGeralDALSQL();
        string cmdSql = DALSQL.validaTipoNegoc();
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        int CODCNLORICLI = -1;
        //
        dbCommand.AddWithValue(":DESTIPNGC", DESTIPNGC);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        var reader = MRT001.ExecuteReader(dbCommand);
        while (reader.Read())
        {
            CODCNLORICLI = Convert.ToInt32(reader[0]);
        }
        return CODCNLORICLI;


    }

    public int buscaCodPesInd(string NOMPESCTO)
    {
        var DALSQL = new CadastroOportunidadeGeralDALSQL();
        string cmdSql = DALSQL.buscaCodPesInd();
        var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
        int NUMSEQPESIND = -1;
        //
        dbCommand.AddWithValue(":NOMPESCTO", NOMPESCTO);
        //
        dbCommand.TrataDbCommandUniversal();
        //               
        var reader = MRT001.ExecuteReader(dbCommand);
        while (reader.Read())
        {
            NUMSEQPESIND = Convert.ToInt32(reader[0]);
        }
        return NUMSEQPESIND;


    }

    //public int buscaCodConsultor(string NOMPESCTO)
    //{
    //    var DALSQL = new CadastroOportunidadeGeralDALSQL();
    //    string cmdSql = DALSQL.buscaCodPesInd();
    //    var dbCommand = MRT001.GetSqlStringCommand(cmdSql);
    //    int NUMSEQPESIND = -1;
    //    //
    //    dbCommand.AddWithValue(":NOMPESCTO", NOMPESCTO);
    //    //
    //    dbCommand.TrataDbCommandUniversal();
    //    //               
    //    var reader = MRT001.ExecuteReader(dbCommand);
    //    while (reader.Read())
    //    {
    //        NUMSEQPESIND = Convert.ToInt32(reader[0]);
    //    }
    //    return NUMSEQPESIND;


    //}
}