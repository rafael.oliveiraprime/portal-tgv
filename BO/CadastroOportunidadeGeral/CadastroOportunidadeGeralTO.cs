﻿using System;
using System.Collections.Generic;

namespace CadastroOportunidadeGeralTO
{
    public class obterCadastroOportunidadeGeral
    {
        public int CODOPTVND { get; set; }

        public int CODOPTVNDCHECK { get; set; }

        public int CODCLI { get; set; }

        public string NOMCLI { get; set; }

        public string NUMCGCCLI { get; set; }

        public int CODCNIVNDTGV { get; set; }

        public Int64 NUMSEQPESIND { get; set; }

        public int INDSTAOPTVND { get; set; }

        public int CODCNLORICLI { get; set; }

        public string NOMCON { get; set; }

        public string NOMCNLORICLI { get; set; }

        public string NUMTLFCTO { get; set; }

        public string NUMTLFCEL { get; set; }

        public string NUMTLFCLI { get; set; }

        public string NUMFAXCLI { get; set; }        

        public string STATUS { get; set; }

        public string DATAGDCTO { get; set; } 
        
        public string DATFIMNGCOPT { get; set; }

        public string NOMFNCALTRPN { get; set; }

        public string NOMCTORPNCLI { get; set; }

        public string DESENDETNRPNCLI { get; set; }

        public string DATALTRPN { get; set; }        

        public Int64 NUMSEQPESINDPES { get; set; }

        public string TIPFNCRPNPES { get; set; }

        public int MATRICULAPES { get; set; }

        public string NOMPESCTOPES { get; set; }

        public string NUMCPFPES { get; set; }

        public string NUMTLFPES { get; set; }

        public string NUMTLFCELPES { get; set; }

        public string DESENDETNPES { get; set; }

        public string DESCGRPES { get; set; }

        public string NOMEMPPES { get; set; }

        public string DESOBSDSC { get; set; }

        public int INDFATGRP { get; set; }

        public string NOMGRPEMPCLI { get; set; }

        public string NOMSERVICOS { get; set; }

        public int CODGRPEMPCLI { get; set; }

        public String DATCAD { get; set; }

        public string DESMTVSITCLI { get; set; }

        public string VLRLIMCRDCLI { get; set; }

    }
}