﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public class CadastroOportunidadeGeralDALSQL
{
    public string obterCadastroOportunidadeGeral(int TIPOBUSCA,  bool _escolherustatus)
    {
        string _default = "";

        if (TIPOBUSCA == 0)
            _default = "1, 2, 5, 6, 7";
        else
        {
            if (_escolherustatus)
                _default = ":INDSTAOPTVND";
            else
                _default = "1, 2, 5, 6, 7";
        }


        return @"

                WITH TABOPORTUNIDADES AS (
                                             SELECT OPT.CODCLI, CLI.NOMCLI,
                                             CASE WHEN OPT.CODCNLORICLI=1 THEN 'SITE (PORTAL)' 
                                             WHEN OPT.CODCNLORICLI=2 THEN 'TELEFONE' 
                                             WHEN OPT.CODCNLORICLI=3 THEN 'E-MAIL' 
                                             WHEN OPT.CODCNLORICLI=4 THEN 'WHATSAPP' 
                                             WHEN OPT.CODCNLORICLI=5 THEN 'PARCERIA' 
                                             WHEN OPT.CODCNLORICLI=6 THEN 'CAMPANHA' 
                                             WHEN OPT.CODCNLORICLI=7 THEN 'OUTRO' 
                                             WHEN OPT.CODCNLORICLI=8 THEN 'PROSPECT'
                                             WHEN OPT.CODCNLORICLI=9 THEN 'INDICAÇÃO PRÓPRIA' 
                                             WHEN OPT.CODCNLORICLI=10 THEN 'INDICAÇÃO GERENTE TRIBANCO' 
                                             WHEN OPT.CODCNLORICLI=11 THEN 'INDICAÇÃO GERENTE MARTINS' 
                                             WHEN OPT.CODCNLORICLI=12 THEN 'RCA' 
                                             WHEN OPT.CODCNLORICLI=13 THEN 'EX-SMART' 
                                             WHEN OPT.CODCNLORICLI=14 THEN 'INDICAÇÃO TRIBANCO - GERENCIADOR' 
                                             WHEN OPT.CODCNLORICLI=15 THEN 'MIGRAÇÃO SERVIÇO' 
                                             WHEN OPT.CODCNLORICLI=16 THEN 'INDICAÇÃO TGV - GERENCIADOR' 
                                             WHEN OPT.CODCNLORICLI=17 THEN 'INDICAÇÃO PARCERIA' 
                                             WHEN OPT.CODCNLORICLI=18 THEN 'IMPORT' 
                                                END NOMCNLORICLI,
                                             OPT.CODCNLORICLI,
                                             CASE CLI.NUMCGCCLI WHEN CLI.NUMCGCCLI THEN (SUBSTR(CLI.NUMCGCCLI, 1, 2) || '.' || SUBSTR(CLI.NUMCGCCLI, 3, 3) || '.' || SUBSTR(CLI.NUMCGCCLI, 6, 3) || '/' || SUBSTR(CLI.NUMCGCCLI, 9,4) || '-' || SUBSTR(CLI.NUMCGCCLI, 13,2) ) ELSE NULL END NUMCGCCLI, 
                                             OPT.CODOPTVND, OPT.CODOPTVND CODOPTVNDCHECK, OPT.CODCNIVNDTGV,
                                             OPT.NUMSEQPESIND, 
                                             CASE WHEN INDSTAOPTVND = 1 THEN 'EM NEGOCIAÇÃO' 
                                                  WHEN INDSTAOPTVND = 2 THEN 'CONTRATO ENVIADO' 
                                                  WHEN INDSTAOPTVND = 3 THEN 'VENDA GANHA'                      
                                                  WHEN INDSTAOPTVND = 4 THEN 'VENDA PERDIDA' 
                                                  WHEN INDSTAOPTVND = 5 THEN 'AGUARDANDO APROVAÇÃO DE DESCONTO' 
                                                  WHEN INDSTAOPTVND = 6 THEN 'DESCONTO APROVADO' 
                                                  WHEN INDSTAOPTVND = 7 THEN 'DESCONTO REJEITADO'
                                                  WHEN INDSTAOPTVND = 8 THEN 'AGUARDANDO DOCUMENTAÇÃO' END STATUS,
                                             OPT.INDSTAOPTVND,    
                                             SUBSTR(FNC.NOMFNC,1,25) AS NOMCON,
                                             TRIM(CPI.NOMPESCTO) AS NOMPESCTOPES,                  
                                             CASE WHEN CPI.TIPFNCRPN = 'M' THEN 'FUNCIONARIO MARTINS' WHEN CPI.TIPFNCRPN = 'R' THEN 'RCA AUTÔNOMO' WHEN CPI.TIPFNCRPN = 'T' THEN 'GERENTE TRIBANCO' WHEN CPI.TIPFNCRPN = 'O' THEN 'OUTROS' END TIPFNCRPNPES,
                                             CPI.NUMSEQPESIND AS NUMSEQPESINDPES,
                                             CASE WHEN CPI.CODFNC != 0 THEN CPI.CODFNC WHEN CPI.CODREP != 0 THEN CPI.CODREP ELSE NULL END MATRICULAPES,
                                             CASE CPI.NUMCPF WHEN CPI.NUMCPF THEN (SUBSTR(CPI.NUMCPF, 1, 3) || '.' || SUBSTR(CPI.NUMCPF, 3, 3) || '.' || SUBSTR(CPI.NUMCPF, 6, 3) || '-' || SUBSTR(CPI.NUMCPF, 9,2)) ELSE NULL END NUMCPFPES,
                                             TRIM(CPI.DESENDETN) AS DESENDETNPES,
                                             CPI.DESCGR AS DESCGRPES, CPI.NOMEMP AS NOMEMPPES,
                                             CASE WHEN LENGTH(TRIM(OPT.NUMTLFCTO)) =11 THEN ( '(' || SUBSTR(OPT.NUMTLFCTO, 1, 2) || ') ' || SUBSTR(OPT.NUMTLFCTO, 3, 5) || '-' ||  SUBSTR(OPT.NUMTLFCTO, 8, 4)) WHEN LENGTH(TRIM(OPT.NUMTLFCTO)) =10 THEN '(' || SUBSTR(OPT.NUMTLFCTO, 1, 2) || ') ' || SUBSTR(OPT.NUMTLFCTO, 3, 4) || '-' || SUBSTR(OPT.NUMTLFCTO, 7, 4) END NUMTLFCTO,
                                             CASE WHEN LENGTH(TRIM(OPT.NUMTLFCEL)) =11 THEN ( '(' || SUBSTR(OPT.NUMTLFCEL, 1, 2) || ') ' || SUBSTR(OPT.NUMTLFCEL, 3, 5) || '-' ||  SUBSTR(OPT.NUMTLFCEL, 8, 4)) WHEN LENGTH(TRIM(OPT.NUMTLFCEL)) =10 THEN '(' || SUBSTR(OPT.NUMTLFCEL, 1, 2) || ') ' || SUBSTR(OPT.NUMTLFCEL, 3, 4) || '-' || SUBSTR(OPT.NUMTLFCEL, 7, 4) END NUMTLFCEL,
                                             CASE WHEN LENGTH(TRIM(CPI.NUMTLF)) =11 THEN ( '(' || SUBSTR(CPI.NUMTLF, 1, 2) || ') ' || SUBSTR(CPI.NUMTLF, 3, 5) || '-' || SUBSTR(CPI.NUMTLF, 8, 4)) WHEN LENGTH(TRIM(CPI.NUMTLF)) =10 THEN '(' || SUBSTR(CPI.NUMTLF, 1, 2) || ') ' || SUBSTR(CPI.NUMTLF, 3, 4) || '-' || SUBSTR(CPI.NUMTLF, 7, 4)  END NUMTLFPES,
                                             CASE WHEN LENGTH(TRIM(CPI.NUMTLFCEL)) =11 THEN ( '(' || SUBSTR(CPI.NUMTLFCEL, 1, 2) || ') ' || SUBSTR(CPI.NUMTLFCEL, 3, 5) || '-' || SUBSTR(CPI.NUMTLFCEL, 8, 4)) WHEN LENGTH(TRIM(CPI.NUMTLFCEL)) =10 THEN '(' || SUBSTR(CPI.NUMTLFCEL, 1, 2) || ') ' || SUBSTR(CPI.NUMTLFCEL, 3, 4) || '-' || SUBSTR(CPI.NUMTLFCEL, 7, 4) END NUMTLFCELPES,
                                             CASE WHEN LENGTH(REPLACE(TRIM(CLI.NUMTLFCLI),'-','')) =10 THEN ( '(' || SUBSTR(TRIM(CLI.NUMTLFCLI), 1, 2) || ') ' || SUBSTR(TRIM(CLI.NUMTLFCLI), 4, 4) || '-' || SUBSTR(TRIM(CLI.NUMTLFCLI), 9, 4)) WHEN LENGTH(REPLACE(TRIM(CLI.NUMTLFCLI),'-','')) =11 THEN '(' || SUBSTR(TRIM(CLI.NUMTLFCLI), 1, 2) || ') ' || SUBSTR(TRIM(CLI.NUMTLFCLI), 4, 5) || '-' || SUBSTR(TRIM(CLI.NUMTLFCLI), 10, 4) END NUMTLFCLI,
                                             CASE WHEN LENGTH(REPLACE(TRIM(CLI.NUMFAXCLI),'-','')) =10 THEN ( '(' || SUBSTR(TRIM(CLI.NUMFAXCLI), 1, 2) || ') ' || SUBSTR(TRIM(CLI.NUMFAXCLI), 4, 4) || '-' || SUBSTR(TRIM(CLI.NUMFAXCLI), 9, 4)) WHEN LENGTH(REPLACE(TRIM(CLI.NUMFAXCLI),'-','')) =11 THEN '(' || SUBSTR(TRIM(CLI.NUMFAXCLI), 1, 2) || ') ' || SUBSTR(TRIM(CLI.NUMFAXCLI), 4, 5) || '-' || SUBSTR(TRIM(CLI.NUMFAXCLI), 10, 4) END NUMFAXCLI,                
                                             CASE WHEN INDSTAOPTVND = 3 THEN TO_CHAR(OPT.DATFIMNGCOPT,'DD/MM/YYYY')                       
                                                  WHEN INDSTAOPTVND = 4 THEN TO_CHAR(OPT.DATFIMNGCOPT,'DD/MM/YYYY') 
                                                  ELSE ' ' END AS DATFIMNGCOPT,
                                             ' ' AS DATAGDCTO,
                                             MAX(ANT.NUMSEQOBS) AS NUMSEQOBS,
                                             0 PRIMERADESOBS, FNC_ALTRPN.NOMFNC AS NOMFNCALTRPN, TO_CHAR(OPT.DATALTRPN,'DD/MM/YYYY') AS DATALTRPN,
                                             NOMCTORPNCLI, DESENDETNRPNCLI, DESOBSDSC, GP.INDFATGRP, GP.NOMGRPEMPCLI                                  
                                             ,(SELECT WM_CONCAT(S.DESSVCTGV) FROM MRT.RLCOPTVNDSVCTGV CS INNER JOIN MRT.CADSVCTGV S ON S.CODSVCTGV = CS.CODSVCTGV WHERE CS.CODOPTVND = OPT.CODOPTVND) AS NOMSERVICOS                  
                                             ,GP.CODGRPEMPCLI
                                             ,TO_CHAR(OPT.DATCAD,'DD/MM/YYYY') AS DATCAD
                                             ,STA.DESMTVSITCLI
                                             ,LMT.VLRLIMCRDCLI
                                             FROM MRT.CADOPTVNDCLITGV OPT
                                             INNER JOIN MRT.T0100361 FNC ON FNC.CODFNC = OPT.CODCNIVNDTGV
                                             INNER JOIN MRT.CADDDOPESINDTGV CPI ON CPI.NUMSEQPESIND = OPT.NUMSEQPESIND
                                             INNER JOIN MRT.T0100043 CLI ON CLI.CODCLI = OPT.CODCLI
                                             LEFT JOIN MRT.T0100361 FNC_ALTRPN ON FNC_ALTRPN.CODFNC = OPT.CODFNCALTRPN
                                             LEFT JOIN MRT.CADOBSCLITGV ANT ON ANT.CODOPTVND = OPT.CODOPTVND AND ANT.DATAGDCTO IS NOT NULL
                                             LEFT JOIN MRT.RLCCLIGRPEMPTGV RL ON RL.CODCLI = OPT.CODCLI
                                             LEFT JOIN MRT.CADGRPEMPTGV GP ON RL.CODGRPEMPCLI = GP.CODGRPEMPCLI
                                             LEFT JOIN MRT.T0101260 STA ON STA.CODMTVSITCLI = CLI.CODSITCLI
                                             LEFT JOIN MRT.T0104561 LMT ON LMT.CODCLI = OPT.CODCLI
                                             WHERE 1 = 1
                                             AND OPT.INDSTAOPTVND IN (#INDSTAOPTVND)
                                             AND OPT.INDSTAOPTVND = :INDSTAOPTVNDFIL
                                             AND CLI.NUMCGCCLI = :NUMCGCCLI
                                             AND OPT.CODOPTVND = :CODOPTVND
                                             AND OPT.CODCLI = :CODCLI
                                             AND OPT.CODCLI = :CODCLIFIL
                                             AND CLI.NOMCLI LIKE '%' || UPPER(:NOMCLIFIL) || '%'
                                             AND CLI.NUMCGCCLI = :NUMCGCCLIFIL
                                             AND OPT.CODCNLORICLI = :CODCNLORICLIFIL
                                             AND OPT.NUMSEQPESIND = :NUMSEQPESINDFIL
                                             AND TRUNC(ANT.DATAGDCTO) = TO_DATE(:DATAGDCTOFIL,'dd/mm/yyyy') 
                                             AND OPT.CODCNIVNDTGV = :CODCNIVNDTGVFIL
                                             GROUP BY OPT.CODCLI, CLI.NOMCLI, OPT.CODCNLORICLI, CLI.NUMCGCCLI, OPT.CODOPTVND, OPT.CODCNIVNDTGV, OPT.NUMSEQPESIND, OPT.INDSTAOPTVND, FNC.NOMFNC, OPT.DATCAD, CPI.NOMPESCTO, CPI.TIPFNCRPN, CPI.NUMSEQPESIND, CPI.CODFNC, CPI.CODREP, CPI.NUMCPF, CPI.DESENDETN,
                                             CPI.DESCGR, CPI.NOMEMP, OPT.NUMTLFCTO, OPT.NUMTLFCEL, CPI.NUMTLF, CPI.NUMTLFCEL, CLI.NUMTLFCLI, CLI.NUMFAXCLI, OPT.DATFIMNGCOPT, FNC_ALTRPN.NOMFNC, OPT.DATALTRPN, NOMCTORPNCLI, DESENDETNRPNCLI, DESOBSDSC, GP.INDFATGRP, GP.NOMGRPEMPCLI, GP.CODGRPEMPCLI, OPT.DATCAD, STA.DESMTVSITCLI, LMT.VLRLIMCRDCLI              
                                             ORDER BY MAX(ANT.NUMSEQOBS) DESC
                                        )
                                           SELECT OPT.CODCLI 
                                                  ,OPT.NOMCLI
                                                  ,OPT.NOMCNLORICLI
                                                  ,OPT.CODCNLORICLI
                                                  ,OPT.NUMCGCCLI 
                                                  ,OPT.CODOPTVND 
                                                  ,OPT.CODOPTVNDCHECK
                                                  ,OPT.CODCNIVNDTGV
                                                  ,OPT.NUMSEQPESIND 
                                                  ,OPT.STATUS
                                                  ,OPT.INDSTAOPTVND    
                                                  ,OPT.NOMCON
                                                  ,OPT.NOMPESCTOPES                  
                                                  ,OPT.TIPFNCRPNPES
                                                  ,OPT.NUMSEQPESINDPES
                                                  ,OPT.MATRICULAPES
                                                  ,OPT.NUMCPFPES
                                                  ,OPT.DESENDETNPES
                                                  ,OPT.DESCGRPES
                                                  ,OPT.NOMEMPPES
                                                  ,OPT.NUMTLFCTO
                                                  ,OPT.NUMTLFCEL
                                                  ,OPT.NUMTLFPES
                                                  ,OPT.NUMTLFCELPES
                                                  ,OPT.NUMTLFCLI
                                                  ,OPT.NUMFAXCLI                
                                                  ,OPT.DATFIMNGCOPT
                                                  ,ANT.DATAGDCTO
                                                  ,OPT.PRIMERADESOBS
                                                  ,OPT.NOMFNCALTRPN
                                                  ,OPT.DATALTRPN
                                                  ,OPT.NOMCTORPNCLI
                                                  ,OPT.DESENDETNRPNCLI
                                                  ,OPT.DESOBSDSC
                                                  ,OPT.INDFATGRP
                                                  ,OPT.NOMGRPEMPCLI                                  
                                                  ,OPT.NOMSERVICOS
                                                  ,OPT.CODGRPEMPCLI
                                                  ,OPT.DATCAD
                                                  ,OPT.DESMTVSITCLI
                                                  ,OPT.VLRLIMCRDCLI
                                             FROM TABOPORTUNIDADES OPT
                                             LEFT JOIN MRT.CADOBSCLITGV ANT ON (ANT.CODOPTVND = OPT.CODOPTVND AND ANT.NUMSEQOBS = OPT.NUMSEQOBS)
                                            ORDER BY ANT.DATAGDCTO ".Replace("#INDSTAOPTVND", _default);


    }

    public string alterarResponsavelOportunidade(List<long> LISTCODOPTVND)
    {
        return @"
                 UPDATE MRT.CADOPTVNDCLITGV SET
                 CODCNIVNDTGV = :CODCNIVNDTGV,                 
                 DATALTRPN = SYSDATE,
                 CODFNCALTRPN = :CODFNCALTRPN
                 WHERE CODOPTVND IN(#LISTCODOPTVND)
               ".Replace("#LISTCODOPTVND", Utilitario.ToListStr(LISTCODOPTVND));
    }


    public string obterTotaisEquipes(string LStatus)
    {
        return @" WITH TABTOTAISRESPON AS (       
                                              SELECT CODCNIVNDTGV
                                                     ,COUNT(CODOPTVND) AS TOTAL
                                                FROM MRT.CADOPTVNDCLITGV
                                               WHERE (CODCNIVNDTGV <> 0 AND CODCNIVNDTGV IS NOT NULL) 
                                                 AND INDSTAOPTVND IN (#LStatus)  
                                            GROUP BY CODCNIVNDTGV
                                          )
                          SELECT CON.CODFNC
                                 ,SUBSTR(TRIM(FNC.NOMFNC), 0, 25) AS NOMFNC
                                 ,NVL(RES.TOTAL, 0) AS TOTAL
                                 ,(SELECT NVL(MAX(OPT.CODOPTVND), 0) FROM MRT.CADOPTVNDCLITGV OPT WHERE OPT.CODCNIVNDTGV = CON.CODFNC AND OPT.INDSTAOPTVND = 1 AND OPT.CODCNLORICLI IN(9, 10, 11, 14, 16, 17) ) ULTIMOCODIGO
                            FROM MRT.CADFNCSISTGV CON	
                           INNER JOIN MRT.T0100361 FNC ON FNC.CODFNC = CON.CODFNC
                            LEFT JOIN TABTOTAISRESPON RES ON CON.CODFNC = RES.CODCNIVNDTGV
                           WHERE CON.DATDST IS NULL
                             AND CON.CODPFLACS = 1
                           ORDER BY ULTIMOCODIGO".Replace("#LStatus", LStatus);

    }

    public string obterTotaisEquipesModal(string LStatus)
    {
        return @" WITH TABTOTAISRESPON AS (       
                                              SELECT CODCNIVNDTGV
                                                     ,COUNT(CODOPTVND) AS TOTAL
                                                FROM MRT.CADOPTVNDCLITGV
                                               WHERE (CODCNIVNDTGV <> 0 AND CODCNIVNDTGV IS NOT NULL) 
                                               AND INDSTAOPTVND IN (#LStatus)  
                                            GROUP BY CODCNIVNDTGV
                                          )
                          SELECT CON.*
                                 ,SUBSTR(TRIM(FNC.NOMFNC), 0, 25) AS NOMFNC
                                 ,NVL(RES.TOTAL, 0) AS TOTAL
                                 ,(SELECT NVL(MAX(OPT.CODOPTVND), 0) FROM MRT.CADOPTVNDCLITGV OPT WHERE OPT.CODCNIVNDTGV = CON.CODFNC AND OPT.INDSTAOPTVND = 1 AND OPT.CODCNLORICLI IN(9, 10, 11, 14, 16, 17) ) ULTIMOCODIGO
                            FROM MRT.CADFNCSISTGV CON	
                           INNER JOIN MRT.T0100361 FNC ON FNC.CODFNC = CON.CODFNC
                            LEFT JOIN TABTOTAISRESPON RES ON CON.CODFNC = RES.CODCNIVNDTGV
                           WHERE CON.DATDST IS NULL
                             AND CON.CODPFLACS = :CODPFLACS
                           ORDER BY ULTIMOCODIGO".Replace("#LStatus", LStatus);

    }

    public string buscaCnpjCorreto()
    {
        return @"
                 SELECT CODCLI, NUMCGCCLI from MRT.T0100043 WHERE CODCLI = :CODCLI
               ";
    }

    public string validaTipoNegoc()
    {
        return @"
                 SELECT INDSTANGC from MRT.CADTIPNGCTGV WHERE TRIM(UPPER(DESTIPNGC)) = TRIM(UPPER(:DESTIPNGC)) AND DATDST IS NULL
               ";
    }

    public string buscaCodPesInd()
    {
        return @"
                 SELECT NUMSEQPESIND
                  FROM MRT.CADDDOPESINDTGV	
                  WHERE TRIM(UPPER(NOMPESCTO)) = TRIM(UPPER(:NOMPESCTO))
                  AND DATDST IS NULL
               ";
    }


}