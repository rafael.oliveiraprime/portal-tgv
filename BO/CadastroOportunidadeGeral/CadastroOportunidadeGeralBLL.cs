﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CadastroOportunidadeGeralBLL
{
    public List<CadastroOportunidadeGeralTO.obterCadastroOportunidadeGeral> obterCadastroOportunidadeGeral(CadastroOportunidadeGeralApiModel.CadastroOportunidadeGeralApiModel objPesquisar)
    {
        try
        {           
            var DAL = new CadastroOportunidadeGeralDAL();
            return DAL.obterCadastroOportunidadeGeral(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A CONSULTA DA OPORTUNIDADE GERAL.");
            Utilitario.InsereLog(ex, "obterCadastroOportunidadeGeral", "CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel", "CadastroOportunidadeGeralBLL", "ERRO AO FAZER A CONSULTA DA OPORTUNIDADE GERAL.");
            throw;
        }
    }

    public bool alterarResponsavelOportunidade(CadastroOportunidadeGeralApiModel.CadastroOportunidadeGeralApiModel objAlterar)
    {
        try
        {
            var DAL = new CadastroOportunidadeGeralDAL();
            return DAL.alterarResponsavelOportunidade(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A ALTERAÇÃO DO RESPONSAVEL OPORTUNIDADE.");
            Utilitario.InsereLog(ex, "alterarResponsavelOportunidade", "CadastroOportunidadeGeralApiModel.CadastroOportunidadeGeralApiModel", "CadastroOportunidadeGeralBLL", "ERRO AO FAZER A ALTERAÇÃO DO RESPONSAVEL OPORTUNIDADE.");
            throw;
        }
    }


    public List<GestaoCobrancaTO.TotaisEquipe> obterTotaisEquipes(CadastroOportunidadeGeralApiModel.CadastroOportunidadeGeralApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroOportunidadeGeralDAL();

            if (objPesquisar.INDSTAOPTVND <= 0)
                objPesquisar.STATUS = "1, 2, 5, 6, 7";
            else
                objPesquisar.STATUS = objPesquisar.INDSTAOPTVND.ToString();


            return DAL.obterTotaisEquipes(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO OBTER TOTAIS COSULTORES");
            Utilitario.InsereLog(ex, "obterTotaisEquipes", " ", "GestaoCobracaBLL", "ERRO AO OBTER TOTAIS CONSULTORES");
            throw;
        }
    }

    public List<GestaoCobrancaTO.TotaisEquipe> obterTotaisEquipesModal(CadastroOportunidadeGeralApiModel.CadastroOportunidadeGeralApiModel objPesquisar)
    {
        try
        {
            var DAL = new CadastroOportunidadeGeralDAL();

            if (objPesquisar.INDSTAOPTVND <= 0)
                objPesquisar.STATUS = "1, 2, 5, 6, 7";
            else
                objPesquisar.STATUS = objPesquisar.INDSTAOPTVND.ToString();


            return DAL.obterTotaisEquipesModal(objPesquisar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO OBTER TOTAIS COSULTORES");
            Utilitario.InsereLog(ex, "obterTotaisEquipes", " ", "CadastroOportunidadeGeralBLL", "ERRO AO OBTER TOTAIS CONSULTORES");
            throw;
        }
    }

}