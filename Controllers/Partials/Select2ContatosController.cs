﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2Contatos")]
public class Select2ContatosController : ApiController
{
    [HttpPost]
    [Route("ObterCodigosContatos")]
    public List<CombosTO.obterCodigosContatos> obterCodigosContatos(ContatosApiModel.ContatosApiModel objPesquisa)
    {
        return new CombosBLL().obterCodigosContatos(objPesquisa);
    }

    [HttpPost]
    [Route("ObterDescricoesContatos")]
    public List<CombosTO.obterDescricoesContatos> obterDescricoesContatos(ContatosApiModel.ContatosApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesContatos(objPesquisa);
    }
}