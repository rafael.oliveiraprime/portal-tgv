﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2Sistemas")]
public class Select2SistemasController : ApiController
{
    [HttpPost]
    [Route("ObterDescricoesSistemas")]
    public List<CombosTO.obterDescricoesSistemas> obterDescricoesSistemas(SistemasApiModel.SistemasApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesSistemas(objPesquisa);
    }
}