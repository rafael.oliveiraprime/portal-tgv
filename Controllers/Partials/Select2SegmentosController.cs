﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2Segmentos")]
public class Select2SegmentosController : ApiController
{
    [HttpPost]
    [Route("ObterCodigosSegmentos")]
    public List<CombosTO.obterCodigosSegmentos> obterCodigosSegmentos(SegmentosApiModel.SegmentosApiModel objPesquisa)
    {
        return new CombosBLL().obterCodigosSegmentos(objPesquisa);
    }

    [HttpPost]
    [Route("ObterDescricoesSegmentos")]
    public List<CombosTO.obterDescricoesSegmentos> obterDescricoesSegmentos(SegmentosApiModel.SegmentosApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesSegmentos(objPesquisa);
    }
}