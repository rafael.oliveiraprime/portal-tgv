﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2Condicao")]
public class Select2CondicaoController : ApiController
{
    [HttpPost]
    [Route("ObterCondicao")]
    public List<CombosTO.ObterCondicao> ObterCondicao(CondicaoApiModel.CondicaoApiModel objPesquisa)
    {
        return new CombosBLL().ObterCondicao(objPesquisa);
    }
}