﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2Consultores")]
public class Select2FConsultoresController : ApiController
{
    [HttpPost]
    [Route("ObterCodigosConsultores")]
    public List<CombosTO.obterCodigosConsultores> obterCodigosConsultores(ConsultoresApiModel.ConsultoresApiModel objPesquisa)
    {
        return new CombosBLL().obterCodigosConsultores(objPesquisa);
    }

    [HttpPost]
    [Route("ObterDescricoesConsultores")]
    public List<CombosTO.obterDescricoesConsultores> obterDescricoesConsultores(ConsultoresApiModel.ConsultoresApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesConsultores(objPesquisa);
    }

    [HttpPost]
    [Route("ObterCodigosConsultoresAtivosVend")]
    public List<CombosTO.obterCodigosConsultores> obterCodigosConsultoresAtivosVend(ConsultoresApiModel.ConsultoresApiModel objPesquisa)
    {
        return new CombosBLL().obterCodigosConsultoresAtivosVend(objPesquisa);
    }

    [HttpPost]
    [Route("ObterDescricoesConsultoresAtivosVend")]
    public List<CombosTO.obterDescricoesConsultores> obterDescricoesConsultoresAtivosVend(ConsultoresApiModel.ConsultoresApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesConsultoresAtivosVend(objPesquisa);
    }

    [HttpPost]
    [Route("ObterCodigosConsultoresAtivosCob")]
    public List<CombosTO.obterCodigosConsultores> ObterCodigosConsultoresAtivosCob(ConsultoresApiModel.ConsultoresApiModel objPesquisa)
    {
        return new CombosBLL().obterCodigosConsultoresAtivosCob(objPesquisa);
    }

    [HttpPost]
    [Route("ObterDescricoesConsultoresAtivosCob")]
    public List<CombosTO.obterDescricoesConsultores> ObterDescricoesConsultoresAtivosCob(ConsultoresApiModel.ConsultoresApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesConsultoresAtivosCob(objPesquisa);
    }
}