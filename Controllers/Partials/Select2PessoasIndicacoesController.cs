﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2PessoasIndicacoes")]
public class Select2PessoasIndicacoesController : ApiController
{
    [HttpPost]
    [Route("ObterCodigosPessoasIndicacoes")]
    public List<CombosTO.obterCodigosPessoasIndicacoes> obterCodigosPessoasIndicacoes(PessoasIndicacoesApiModel.PessoasIndicacoesApiModel objPesquisa)
    {
        return new CombosBLL().obterCodigosPessoasIndicacoes(objPesquisa);
    }

    [HttpPost]
    [Route("ObterDescricoesPessoasIndicacoes")]
    public List<CombosTO.obterDescricoesPessoasIndicacoes> obterDescricoesPessoasIndicacoes(PessoasIndicacoesApiModel.PessoasIndicacoesApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesPessoasIndicacoes(objPesquisa);
    }
}