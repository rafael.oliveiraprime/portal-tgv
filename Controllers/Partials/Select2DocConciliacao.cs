﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2DocConciliacao")]
public class Select2DocConciliacaoController : ApiController
{
    [HttpPost]
    [Route("ObterDocConciliacao")]
    public List<CombosTO.obterDocConciliacao> obterDocConciliacao(DocConciliacaoApiModel.DocConciliacaoApiModel objPesquisa)
    {
        return new CombosBLL().obterDocConciliacao(objPesquisa);
    }

    [HttpPost]
    [Route("ObterDescricoesServicos")]
    public List<CombosTO.obterDocConciliacao> obterDescricoesDocConciliacao(DocConciliacaoApiModel.DocConciliacaoApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesDocConciliacao(objPesquisa);
    }
}