﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2MotivosContatos")]
public class Select2MotivosContatosController : ApiController
{
    [HttpPost]
    [Route("ObterCodigosMotivosContatos")]
    public List<CombosTO.obterCodigosMotivosContatos> obterCodigosMotivosContatos(MotivosContatosApiModel.MotivosContatosApiModel objPesquisa)
    {
        return new CombosBLL().obterCodigosMotivosContatos(objPesquisa);
    }

    [HttpPost]
    [Route("ObterDescricoesMotivosContatos")]
    public List<CombosTO.obterDescricoesMotivosContatos> obterDescricoesMotivosContatos(MotivosContatosApiModel.MotivosContatosApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesMotivosContatos(objPesquisa);
    }
    [HttpPost]
    [Route("ObterDescricoesMotivosContatosNeg")]
    public List<CombosTO.obterDescricoesMotivosContatos> obterDescricoesMotivosContatosNeg(MotivosContatosApiModel.MotivosContatosApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesMotivosContatosNeg(objPesquisa);
    }

}