﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2Clientes")]
public class Select2ClientesController : ApiController
{
    [HttpPost]
    [Route("ObterCodigosClientes")]
    public List<CombosTO.obterCodigosClientes> obterCodigosClientes(ClientesApiModel.ClientesApiModel objPesquisa)
    {
        return new CombosBLL().obterCodigosClientes(objPesquisa);
    }

    [HttpPost]
    [Route("ObterDescricoesClientes")]
    public List<CombosTO.obterDescricoesClientes> obterDescricoesClientes(ClientesApiModel.ClientesApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesClientes(objPesquisa);
    }
}