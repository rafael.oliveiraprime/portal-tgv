﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2SistemasCodigosAutomacoes")]
public class Select2SistemasCodigosAutomacoesController : ApiController
{
    [HttpPost]
    [Route("ObterSistemasCodigosAutomacoes")]
    public List<CombosTO.obterSistemasCodigosAutomacoes> obterSistemasCodigosAutomacoes(SistemasCodigosAutomacoesApiModel.SistemasCodigosAutomacoesApiModel objPesquisa)
    {
        return new CombosBLL().obterSistemasCodigosAutomacoes(objPesquisa);
    }
}