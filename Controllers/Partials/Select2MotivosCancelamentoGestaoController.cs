﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2MotivosCancelamentoGestao")]
public class Select2MotivosCancelamentoGestaoController : ApiController
{
    [HttpPost]
    [Route("obterDescricoesMotivosCancelamentoGestao")]
    public List<CombosTO.obterDescricoesMotivosCancelamentoGestao> obterDescricoesMotivosCancelamentoGestao(MotivosCancelamentoGestaoApiModel.MotivosCancelamentoGestaoApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesMotivosCancelamentoGestao(objPesquisa);
    }

}