﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2Parcerias")]
public class Select2ParceriasController : ApiController
{
    [HttpPost]
    [Route("ObterCodigosParcerias")]
    public List<CombosTO.obterCodigosParcerias> obterCodigosParcerias(ParceriasApiModel.ParceriasApiModel objPesquisa)
    {
        return new CombosBLL().obterCodigosParcerias(objPesquisa);
    }

    [HttpPost]
    [Route("ObterDescricoesParcerias")]
    public List<CombosTO.obterDescricoesParcerias> obterDescricoesParcerias(ParceriasApiModel.ParceriasApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesParcerias(objPesquisa);
    }
}