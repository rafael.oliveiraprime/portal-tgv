﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2TiposContatos")]
public class Select2TiposContatosController : ApiController
{
    [HttpPost]
    [Route("ObterCodigosTiposContatos")]
    public List<CombosTO.obterCodigosTiposContatos> obterCodigosTiposContatos(TiposContatosApiModel.TiposContatosApiModel objPesquisa)
    {
        return new CombosBLL().obterCodigosTiposContatos(objPesquisa);
    }

    [HttpPost]
    [Route("ObterDescricoesTiposContatos")]
    public List<CombosTO.obterDescricoesTiposContatos> obterDescricoesTiposContatos(TiposContatosApiModel.TiposContatosApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesTiposContatos(objPesquisa);
    }

    [HttpPost]
    [Route("ObterDescricoesTiposContatosTodos")]
    public List<CombosTO.obterDescricoesTiposContatos> obterDescricoesTiposContatosTodos(TiposContatosApiModel.TiposContatosApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesTiposContatos(objPesquisa);
    }
}