﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2Servicos")]
public class Select2ServicosController : ApiController
{
    [HttpPost]
    [Route("ObterCodigosServicos")]
    public List<CombosTO.obterCodigosServicos> obterCodigosServicos(ServicosApiModel.ServicosApiModel objPesquisa)
    {
        return new CombosBLL().obterCodigosServicos(objPesquisa);
    }

    [HttpPost]
    [Route("ObterDescricoesServicos")]
    public List<CombosTO.obterDescricoesServicos> obterDescricoesServicos(ServicosApiModel.ServicosApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesServicos(objPesquisa);
    }
}