﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2CondicoesComerciais")]
public class Select2CondicoesComerciaisController : ApiController
{
    [HttpPost]
    [Route("ObterDescricoesCondicoesComerciais")]
    public List<CombosTO.obterDescricoesCondicoesComerciais> obterDescricoesCondicoesComerciais(CondicoesComerciasApiModel.CondicoesComerciasApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesCondicoesComerciais(objPesquisa);
    }

    [HttpPost]
    [Route("ObterCondicoesComerciaisVerificacao")]
    public List<CombosTO.obterCondicoesComerciaisVerificacao> obterCondicoesComerciaisVerificacao(CondicoesComerciasApiModel.CondicoesComerciasApiModel objPesquisa)
    {
        return new CombosBLL().obterCondicoesComerciaisVerificacao(objPesquisa);
    }
}