﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2GrupoEconomico")]
public class Select2GrupoEconomicoController : ApiController
{
    [HttpPost]
    [Route("ObterCodigosGrupoEconomico")]
    public List<CombosTO.obterCodigosGrupoEconomico> obterCodigosGrupoEconomico(GrupoEconomicoApiModel.GrupoEconomicoApiModel objPesquisa)
    {
        return new CombosBLL().obterCodigosGrupoEconomico(objPesquisa);
    }

    [HttpPost]
    [Route("ObterDescricoesGrupoEconomico")]
    public List<CombosTO.obterDescricoesGrupoEconomico> obterDescricoesGrupoEconomico(GrupoEconomicoApiModel.GrupoEconomicoApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesGrupoEconomico(objPesquisa);
    }

    
    [HttpPost]
    [Route("ObterDescricoesGrupoEconomicoFat")]
    public List<CombosTO.obterDescricoesGrupoEconomico> ObterDescricoesGrupoEconomicoFat(GrupoEconomicoApiModel.GrupoEconomicoApiModel objPesquisa)
    {
        return new CombosBLL().ObterDescricoesGrupoEconomicoFat(objPesquisa);
    }


}