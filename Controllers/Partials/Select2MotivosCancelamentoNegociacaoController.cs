﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2MotivosCancelamentoNegociacao")]
public class Select2MotivosCancelamentoNegociacaoController : ApiController
{
    [HttpPost]
    [Route("obterDescricoesMotivosCancelamentoNegociacao")]
    public List<CombosTO.obterDescricoesMotivosCancelamentoGestao> obterDescricoesMotivosCancelamentoNegociacao(MotivosCancelamentoGestaoApiModel.MotivosCancelamentoGestaoApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesMotivosCancelamentoNegociacao(objPesquisa);
    }

}