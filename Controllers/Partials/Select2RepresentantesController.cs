﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2Representantes")]
public class Select2RepresentantesController : ApiController
{
    [HttpPost]
    [Route("ObterCodigosRepresentantes")]
    public List<CombosTO.obterCodigosRepresentantes> obterCodigosRepresentantes(RepresentantesApiModel.RepresentantesApiModel objPesquisa)
    {
        return new CombosBLL().obterCodigosRepresentantes(objPesquisa);
    }

    [HttpPost]
    [Route("ObterDescricoesRepresentantes")]
    public List<CombosTO.obterDescricoesRepresentantes> obterDescricoesRepresentantes(RepresentantesApiModel.RepresentantesApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesRepresentantes(objPesquisa);
    }
}