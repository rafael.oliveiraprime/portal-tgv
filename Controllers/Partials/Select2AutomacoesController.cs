﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2Automacoes")]
public class Select2AutomacoesController : ApiController
{
    [HttpPost]
    [Route("ObterCodigosAutomacoes")]
    public List<CombosTO.obterCodigosAutomacoes> obterCodigosAutomacoes(AutomacoesApiModel.AutomacoesApiModel objPesquisa)
    {
        return new CombosBLL().obterCodigosAutomacoes(objPesquisa);
    }

    [HttpPost]
    [Route("ObterDescricoesAutomacoes")]
    public List<CombosTO.obterDescricoesAutomacoes> obterDescricoesAutomacoes(AutomacoesApiModel.AutomacoesApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesAutomacoes(objPesquisa);
    }
}