﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2ServicosCesta")]
public class Select2ServicosCestaController : ApiController
{
    [HttpPost]
    [Route("ObterCodigosServicosCesta")]
    public List<CombosTO.obterCodigosServicosCesta> obterCodigosServicosCesta(CestaServicosApiModel.CestaServicosApiModel objPesquisa)
    {
        return new CombosBLL().obterCodigosServicosCesta(objPesquisa);
    }

    [HttpPost]
    [Route("ObterDescricoesServicosCesta")]
    public List<CombosTO.obterDescricoesServicosCesta> obterDescricoesServicosCesta(CestaServicosApiModel.CestaServicosApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesServicosCesta(objPesquisa);
    }
}