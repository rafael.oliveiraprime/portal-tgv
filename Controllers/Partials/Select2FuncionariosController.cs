﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2Funcionarios")]
public class Select2FuncionariosController : ApiController
{
    [HttpPost]
    [Route("ObterCodigosFuncionarios")]
    public List<CombosTO.obterCodigosFuncionarios> obterCodigosFuncionarios(FuncionariosApiModel.FuncionariosApiModel objPesquisa)
    {
        return new CombosBLL().obterCodigosFuncionarios(objPesquisa);
    }

    [HttpPost]
    [Route("ObterDescricoesFuncionarios")]
    public List<CombosTO.obterDescricoesFuncionarios> obterDescricoesFuncionarios(FuncionariosApiModel.FuncionariosApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesFuncionarios(objPesquisa);
    }
}