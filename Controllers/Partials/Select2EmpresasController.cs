﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2Empresas")]
public class Select2EmpresasController : ApiController
{

    [HttpPost]
    [Route("ObterDescricoesEmpresas")]
    public List<CombosTO.ObterDescricoesEmpresas> ObterDescricoesEmpresas(EmpresasApiModel.Empresas objPesquisa)
    {
        return new CombosBLL().ObterDescricoesEmpresas(objPesquisa);
    }

}