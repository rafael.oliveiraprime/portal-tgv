﻿using System.Net;
using System.Web.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

[RoutePrefix("api/Select2CadastroDocServicos")]
public class Select2CadastroDocServicosController : ApiController
{
    [HttpPost]
    [Route("ObterCodigosServicos")]
    public List<CombosTO.obterCadastroDocServicos> obterCodigosServicos(CadastroDocServicosApiModel.CadastroDocServicosApiModel objPesquisa)
    {
        return new CombosBLL().obterCadastroDocServicos(objPesquisa);
    }

    [HttpPost]
    [Route("ObterDescricoesServicos")]
    public List<CombosTO.obterCadastroDocServicos> obterDescricoesServicos(CadastroDocServicosApiModel.CadastroDocServicosApiModel objPesquisa)
    {
        return new CombosBLL().obterDescricoesCadastroDocServicos(objPesquisa);
    }
}