﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using System.Threading.Tasks;

[RoutePrefix("api/CadastroDocConciliacao")]
public class CadastroDocConciliacaoController : ApiController
{
    [HttpPost]
    [Route("ObterCadastroDocConciliacao")]
    public List<CadastroDocConciliacaoTO.obterCadastroDocConciliacao> obterCadastroDocConciliacao(CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel objPesquisar)
    {

        var bll = new CadastroDocConciliacaoBLL();
        return bll.obterCadastroDocConciliacao(objPesquisar);
    }

    [HttpPost]
    [Route("InserirCadastroDocConciliacao")]
    public bool inserirCadastroDocConciliacao(CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel objInserir)
    {
        return new CadastroDocConciliacaoBLL().inserirCadastroDocConciliacao(objInserir);
    }

    [HttpPost]
    [Route("AlterarCadastroDocConciliacao")]
    public bool alterarCadastroDocConciliacao(CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel objAlterar)
    {
        return new CadastroDocConciliacaoBLL().alterarCadastroDocConciliacao(objAlterar);
    }

    [HttpPost]
    [Route("AtivarCadastroDocConciliacao")]
    public bool ativarCadastroDocConciliacao(CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel objAlterar)
    {
        return new CadastroDocConciliacaoBLL().ativarCadastroDocConciliacao(objAlterar);
    }

    [HttpPost]
    [Route("DesativarCadastroDocConciliacao")]
    public bool desativarCadastroDocConciliacao(CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel objAlterar)
    {
        return new CadastroDocConciliacaoBLL().desativarCadastroDocConciliacao(objAlterar);
    }

    [HttpPost]
    [Route("UploadFileInsert")]
    public async Task<string> UploadFileInsert(string DESDOCARZ, string TIPDOC, string INDENV)
    {
        string nomeArquivo = "";
        string base64String = "";
        byte[] binaryData;
        long bytesRead;

        try
        {
            foreach (string file in HttpContext.Current.Request.Files)
            {
                var fileContent = HttpContext.Current.Request.Files[file];
                if (fileContent != null && fileContent.ContentLength > 0)
                {
                    nomeArquivo = fileContent.FileName;

                    binaryData = new Byte[fileContent.InputStream.Length];
                    bytesRead = fileContent.InputStream.Read(binaryData, 0, (int)fileContent.InputStream.Length);
                    fileContent.InputStream.Close();
                    base64String = System.Convert.ToBase64String(binaryData, 0, binaryData.Length);

                }
            }
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A UPLOAD DE ARQUIVO DO CADASTRO SERVICO.");
            Utilitario.InsereLog(ex, "UploadFile", "codigo", "CadastroDocConciliacaoController", "ERRO AO FAZER A UPLOAD DE ARQUIVO DO CADASTRO SERVICO.");
            return "1";
        }

        var bll = new CadastroDocConciliacaoBLL();
        CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel objAlterar = new CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel();
        try
        {

            objAlterar.DESDOCARZ = DESDOCARZ;
            objAlterar.TIPDOC = int.Parse(TIPDOC);
            objAlterar.INDENV = int.Parse(INDENV);
            objAlterar.CDOANXDOCARZ = nomeArquivo + " ; " + base64String;

            bll.inserirCadastroDocConciliacao(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO SALVAR DADOS DO CADASTRO DE SERVIÇO.");
            Utilitario.InsereLog(ex, "UploadFile", "CODSVCTGV, DESSVCTGV, TIPSVC, VLRSVC, NUMPTOSVC, PRDATI", "CadastroDocConciliacaoController", "ERRO AO SALVAR DADOS DO CADASTRO DE SERVIÇO.");
            return "2";
        }
        await Task.Delay(0);

        return "0";
    }

    [HttpPost]
    [Route("UploadFileEditar")]
    public async Task<string> UploadFileEditar(string CODDOCARZ, string DESDOCARZ, string TIPDOC, string INDENV)
    {
        string nomeArquivo = "";
        string base64String = "";
        byte[] binaryData;
        long bytesRead;

        try
        {
            foreach (string file in HttpContext.Current.Request.Files)
            {
                var fileContent = HttpContext.Current.Request.Files[file];
                if (fileContent != null && fileContent.ContentLength > 0)
                {
                    nomeArquivo = fileContent.FileName;

                    binaryData = new Byte[fileContent.InputStream.Length];
                    bytesRead = fileContent.InputStream.Read(binaryData, 0, (int)fileContent.InputStream.Length);
                    fileContent.InputStream.Close();
                    base64String = System.Convert.ToBase64String(binaryData, 0, binaryData.Length);

                }
            }
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A UPLOAD DE ARQUIVO DO CADASTRO SERVICO.");
            Utilitario.InsereLog(ex, "UploadFile", "codigo", "CadastroDocConciliacaoController", "ERRO AO FAZER A UPLOAD DE ARQUIVO DO CADASTRO SERVICO.");
            return "1";
        }

        var bll = new CadastroDocConciliacaoBLL();
        CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel objAlterar = new CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel();
        try
        {

            objAlterar.CODDOCARZ = int.Parse(CODDOCARZ);
            objAlterar.DESDOCARZ = DESDOCARZ;
            objAlterar.TIPDOC = int.Parse(TIPDOC);
            objAlterar.INDENV = int.Parse(INDENV);
            objAlterar.CDOANXDOCARZ = nomeArquivo + " ; " + base64String;

            bll.alterarCadastroDocConciliacao(objAlterar);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO SALVAR DADOS DO CADASTRO DE SERVIÇO.");
            Utilitario.InsereLog(ex, "UploadFile", "CODSVCTGV, DESSVCTGV, TIPSVC, VLRSVC, NUMPTOSVC, PRDATI", "CadastroDocConciliacaoController", "ERRO AO SALVAR DADOS DO CADASTRO DE SERVIÇO.");
            return "2";
        }
        await Task.Delay(0);

        return "0";
    }

    [HttpPost]
    [Route("VisualizarTemplate")]
    public string VisualizarTemplate(CadastroDocConciliacaoApiModel.CadastroDocConciliacaoApiModel objPesquisar)
    {
        return new CadastroDocConciliacaoBLL().VisualizarTemplate(objPesquisar);
    }
}





