﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

[RoutePrefix("api/RelAutomacao")]
public class RelAutomacaoController : ApiController
{
   
    [HttpPost]
    [Route("ObterRelAutomocao")]
    public List<CadastroAutomacaoTO.obterRelAutomacao> ObterRelAutomocao(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objPesquisar)
    {
        return new CadastroAutomacaoBLL().obterRelAutomacao(objPesquisar);
    }



}