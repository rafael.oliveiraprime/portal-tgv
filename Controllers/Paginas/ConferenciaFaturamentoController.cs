﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;


[RoutePrefix("api/ConferenciaFaturamento")]
public class ConferenciaFaturamentoController : ApiController
{

    [HttpPost]
    [Route("GerarLoteFaturamento")]
    public string GerarLoteFaturamento()
    {
        return new FaturamentoBLL().GerarLoteFaturamento();
    }

    [HttpPost]
    [Route("ObterLotesFaturamento")]
    public List<FaturamentoTO.obterLotesFaturamneto> ObterLotesFaturamento(FaturamentoApiModel.obterDemonstrativoFaturamentoApiModel objPesquisar)
    {
        return new FaturamentoBLL().ObterLotesFaturamento(objPesquisar);
    }


    [HttpPost]
    [Route("VerificaCancelarLote")]
    public string VerificaCancelarLote(FaturamentoApiModel.cancelarLoteFaturamentoApiModel objCancelar)
    {
        return new FaturamentoBLL().VerificaCancelarLote();
    }

    [HttpPost]
    [Route("CancelarLote")]
    public string CancelarLote(FaturamentoApiModel.cancelarLoteFaturamentoApiModel objCancelar)
    {
        return new FaturamentoBLL().CancelarLote();
    }
    
    [HttpPost]
    [Route("ValidarLoteProcessado")]
    public bool ValidarLoteProcessado(FaturamentoApiModel.validarLoteFaturamentoApiModel objValidar)
    {
        return new FaturamentoBLL().ValidarLoteProcessado(objValidar);
    }
    
    [HttpPost]
    [Route("VerificaEnviarLote")]
    public string VerificaEnviarLote()
    {
        return new FaturamentoBLL().VerificaEnviarLote();
    }

    [HttpPost]
    [Route("obterEmailFaturamento")]
    public List<FaturamentoTO.obterSomatoriaEmailFaturamento> obterEmailFaturamento(FaturamentoApiModel.obterEmailFaturamento _objbusca)
    {
        return new FaturamentoBLL().obterSomatoriaFaturamentos(_objbusca);
    }

    
    [HttpPost]
    [Route("EviarLoteFaturamento")]
    public string EviarLoteFaturamento(FaturamentoApiModel.enviarLoteparaFaturamento _objEnvLote)
    {
        return new FaturamentoBLL().EviarLoteFaturamento(_objEnvLote);
    }

}