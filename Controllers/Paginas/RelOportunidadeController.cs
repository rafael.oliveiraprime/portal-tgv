﻿using System.Collections.Generic;
using System.Web.Http;

[RoutePrefix("api/RelPontuacao")]
public class RelPontuacaoController : ApiController
{
    [HttpPost]
    [Route("ObterPontuacao")]
    public List<RelPontuacaoTO.obterPontuacao> ObterPontuacao(RelPontuacaoApiModel.obterPontuacaoApiModel objPesquisar)
    {
        return new RelPontuacaoBLL().obterPontuacao(objPesquisar);
    }

    
    [HttpPost]
    [Route("obterTotaisReceita")]
    public List<RelPontuacaoTO.obterTotaisReceita> obterTotaisReceita(RelPontuacaoApiModel.obterPontuacaoApiModel objPesquisar)
    {
        return new RelPontuacaoBLL().obterTotaisReceita(objPesquisar);
    }

}





