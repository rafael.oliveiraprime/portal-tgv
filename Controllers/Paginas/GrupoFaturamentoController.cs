﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;


[RoutePrefix("api/GrupoFaturamento")]
public class GrupoFaturamentoController : ApiController
{
    [HttpPost]
    [Route("obterCadastroGrupoEconomico")]
    public List<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomico> obterCadastroGrupoEconomico(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel objPesquisar)
    {
        return new CadastroGrupoEconomicoBLL().obterCadastroGrupoEconomico(objPesquisar);
    }

    
    [HttpPost]
    [Route("obterCadastroGrupoFaturamentoServico")]
    public List<CadastroGrupoEconomicoTO.obterCadastroGrupoFaturamentoServico> obterCadastroGrupoFaturamentoServico(GrupoFaturamentoApiModel.GrupoFaturamentoApiModel objPesquisar)
    {
        return new CadastroGrupoEconomicoBLL().obterCadastroGrupoFaturamentoServico(objPesquisar);
    }


    [HttpPost]
    [Route("AtivarCadastroGrupoEconomico")]
    public bool ativarCadastroGrupoEconomico(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel objAlterar)
    {
        return new CadastroGrupoEconomicoBLL().ativarCadastroGrupoEconomico(objAlterar);
    }

    [HttpPost]
    [Route("DesativarCadastroGrupoEconomico")]
    public bool desativarCadastroGrupoEconomico(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel objAlterar)
    {
        return new CadastroGrupoEconomicoBLL().desativarCadastroGrupoEconomico(objAlterar);
    }

    [HttpPost]
    [Route("AtualizarContaCorrente")]
    public string AtualizarContaCorrente(GrupoFaturamentoApiModel.GrupoFaturamentoContaCorrenteApiModel objAlterar)
    {
        return new CadastroGrupoEconomicoBLL().AtualizarContaCorrente(objAlterar);
    }


}