﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for CadastroCancelamentoController
/// </summary>
[RoutePrefix("api/CadastroMotivoCanc")]
public class CadastroMotivoCancController : ApiController
{
    [HttpPost]
    [Route("ObterCadastroMotivoCanc")]
    public List<CadastroMotivoCancTO.obterCadastroMotivoCanc> obterCadastroMotivoCanc(CadastroMotivoCancApiModel.CadastroMotivoCancApiModel objPesquisar)
    {
        return new CadastroMotivoCancBLL().obterCadastroMotivoCanc(objPesquisar);
    }

    [HttpPost]
    [Route("InserirCadastroMotivoCanc")]
    public bool inserirCadastroMotivoCanc(CadastroMotivoCancApiModel.CadastroMotivoCancApiModel objInserir)
    {
        return new CadastroMotivoCancBLL().inserirCadastroMotivoCanc(objInserir);
    }

    [HttpPost]
    [Route("AlterarCadastroMotivoCanc")]
    public bool alterarCadastroMotivoCanc(CadastroMotivoCancApiModel.CadastroMotivoCancApiModel objAlterar)
    {
        return new CadastroMotivoCancBLL().alterarCadastroMotivoCanc(objAlterar);
    }

    [HttpPost]
    [Route("AtivarCadastroMotivoCanc")]
    public bool ativarCadastroMotivoCanc(CadastroMotivoCancApiModel.CadastroMotivoCancApiModel objAlterar)
    {
        return new CadastroMotivoCancBLL().ativarCadastroMotivoCanc(objAlterar);
    }

    [HttpPost]
    [Route("DesativarCadastroMotivoCanc")]
    public bool desativarCadastroMotivoCanc(CadastroMotivoCancApiModel.CadastroMotivoCancApiModel objAlterar)
    {
        return new CadastroMotivoCancBLL().desativarCadastroMotivoCanc(objAlterar);
    }
}