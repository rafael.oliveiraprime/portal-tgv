﻿using System.Collections.Generic;
using System.Web.Http;

[RoutePrefix("api/RelCobranca")]
public class RelCobrancaController : ApiController
{
    [HttpPost]
    [Route("ObterCobranca")]
    public List<RelCobrancaTO.obterCobranca> ObterCobranca(RelcobrancaApiModel.obterCobrancaApiModel objPesquisar)
    {
        return new RelcobrancaBLL().ObterCobranca(objPesquisar);
    }

}





