﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for CadastroContatoController
/// </summary>
[RoutePrefix("api/CadastroContato")]
public class CadastroContatoController : ApiController
{
    [HttpPost]
    [Route("ObterCadastroContato")]
    public List<CadastroContatoTO.obterCadastroContato> obterCadastroContato(CadastroContatoApiModel.CadastroContatoApiModel objPesquisar)
    {
        return new CadastroContatoBLL().obterCadastroContato(objPesquisar);
    }

    [HttpPost]
    [Route("InserirCadastroContato")]
    public bool inserirCadastroContato(CadastroContatoApiModel.CadastroContatoApiModel objInserir)
    {
        return new CadastroContatoBLL().inserirCadastroContato(objInserir);
    }

    [HttpPost]
    [Route("AlterarCadastroContato")]
    public bool alterarCadastroContato(CadastroContatoApiModel.CadastroContatoApiModel objAlterar)
    {
        return new CadastroContatoBLL().alterarCadastroContato(objAlterar);
    }

    [HttpPost]  
    [Route("AtivarCadastroContato")]
    public bool ativarCadastroContato(CadastroContatoApiModel.CadastroContatoApiModel objAlterar)
    {
        return new CadastroContatoBLL().ativarCadastroContato(objAlterar);
    }

    [HttpPost]
    [Route("DesativarCadastroContato")]
    public bool desativarCadastroContato(CadastroContatoApiModel.CadastroContatoApiModel objAlterar)
    {
        return new CadastroContatoBLL().desativarCadastroContato(objAlterar);
    }
}