﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Transactions;
using System.Text.RegularExpressions;


[RoutePrefix("api/GestaoNegociacao")]
public class GestaoNegociacaoController : ApiController
{
    [HttpPost]
    [Route("ObterCadastroOportunidadeGeral")]
    public List<CadastroOportunidadeGeralTO.obterCadastroOportunidadeGeral> obterCadastroOportunidadeGeral(CadastroOportunidadeGeralApiModel.CadastroOportunidadeGeralApiModel objPesquisar)
    {
        return new CadastroOportunidadeGeralBLL().obterCadastroOportunidadeGeral(objPesquisar);
    }

    [HttpPost]
    [Route("AlterarResponsavelOportunidade")]
    public bool alterarResponsavelOportunidade(CadastroOportunidadeGeralApiModel.CadastroOportunidadeGeralApiModel objAlterar)
    {
        return new CadastroOportunidadeGeralBLL().alterarResponsavelOportunidade(objAlterar);
    }

    [HttpPost]
    [Route("obterOportunidadeContato")]
    public List<OportunidadeContatoTO.OportunidadeContato> obterOportunidadeContato(OportunidadeContatoTO.OportunidadeContato objPesquisar)
    {
        return new OportunidadeContatoBLL().obterOportunidadeContato(objPesquisar.CODOPTVND);
    }


    [HttpPost]
    [Route("obterTotaisEquipes")]
    public List<GestaoCobrancaTO.TotaisEquipe> obterTotaisEquipes(CadastroOportunidadeGeralApiModel.CadastroOportunidadeGeralApiModel objPesquisar)
    {
        return new CadastroOportunidadeGeralBLL().obterTotaisEquipes(objPesquisar);
    }

    [HttpPost]
    [Route("obterTotaisEquipesModal")]
    public List<GestaoCobrancaTO.TotaisEquipe> obterTotaisEquipesModal(CadastroOportunidadeGeralApiModel.CadastroOportunidadeGeralApiModel objPesquisar)
    {
        return new CadastroOportunidadeGeralBLL().obterTotaisEquipesModal(objPesquisar);
    }

    [HttpPost]
    [Route("buscaCnpjCorreto")]
    public bool buscaCnpjCorreto(string CODCLI, string CNPJ)
    {
        return new CadastroOportunidadeGeralDAL().buscaCnpjCorreto(CODCLI, CNPJ);
    }

    [HttpPost]
    [Route("validaTipoNegoc")]
    public int validaTipoNegoc(string DESTIPNGC)
    {
        return new CadastroOportunidadeGeralDAL().validaTipoNegoc(DESTIPNGC);
    }

    [HttpPost]
    [Route("buscaCodPesInd")]
    public int buscaCodPesInd(string NOMPESCTO)
    {
        return new CadastroOportunidadeGeralDAL().buscaCodPesInd(NOMPESCTO);
    }


    [HttpPost]
    [Route("obterTemplateXlsx")]
    public System.Web.Mvc.FileResult obterTemplateXlsx()
    {

        var caminhoupload = "~/Templates/";
        caminhoupload = System.Web.HttpContext.Current.Server.MapPath(caminhoupload);
        System.Web.Mvc.FileContentResult result = new System.Web.Mvc.FileContentResult(System.IO.File.ReadAllBytes(caminhoupload + "Import_Negociacao.xlsx"), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
        {
            FileDownloadName = "Import_Negociacao.xlsx"
        };

        return result;
    }


    [HttpPost]
    [Route("ImportarNegociacoes")]
    public CadastroOportunidadeApiModel.retornoImport ImportarNegociacoes(CadastroOportunidadeGeralApiModel.ImportaNegociacaoApiModel objInserir)
    {
        try
        {
            int i = 1;
            int count = objInserir.DATA_XLSX.Length;

            List<int> _codcli = new List<int>();

            List<CadastroOportunidadeApiModel.linhaExcelOptImport> _listOk = new List<CadastroOportunidadeApiModel.linhaExcelOptImport>();
            List<CadastroOportunidadeApiModel.linhaExcelOptImport> _listInvalidos = new List<CadastroOportunidadeApiModel.linhaExcelOptImport>();
            CadastroOportunidadeApiModel.retornoImport _retornoImport = new CadastroOportunidadeApiModel.retornoImport();
            CadastroOportunidadeGeralApiModel.ImportaNegociacaoApiModel _Inserir = new CadastroOportunidadeGeralApiModel.ImportaNegociacaoApiModel();

            var DALOPTCONT = new OportunidadeContatoDAL();
            var DAL = new CadastroConsultorDAL();
            var DALOPT = new CadastroOportunidadeDAL();
            var BLLOPTSVC = new CadastroServicoOportunidadeBLL();
            var DALAGND = new CadastroAnotacaoBLL();
            var dalconta = new GestaoContasDAL();

            CadastroConsultorApiModel.CadastroConsultorApiModel _consultor = new CadastroConsultorApiModel.CadastroConsultorApiModel();
            CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel _clienteopt = new CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel();
            CadastroOportunidadeApiModel.CadastroOportunidadeApiModel _insert = new CadastroOportunidadeApiModel.CadastroOportunidadeApiModel();
            ServicosApiModel.ServicosApiModel _servicoObjPesquisa = new ServicosApiModel.ServicosApiModel();
            CombosTO.obterCodigosServicos _servicoModel = new CombosTO.obterCodigosServicos();
            CadastroGrupoEconomicoTO.BuscaGrupoEconomico _grupoeco = new CadastroGrupoEconomicoTO.BuscaGrupoEconomico();
            var dalgrupoEco = new CadastroGrupoEconomicoDAL();
            var dalCombosCodServico = new CombosDAL();
            CadastroGrupoEconomicoTO.VerificaSeEDebito _VerificaDebito = new CadastroGrupoEconomicoTO.VerificaSeEDebito();
            CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel objInserirGrupo = new CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel();
            CadastroGrupoEconomicoTO.ExisteGrupoCli _existe = new CadastroGrupoEconomicoTO.ExisteGrupoCli();
            CadastroAnotacaoApiModel.CadastroAnotacaoApiModel _objInserirAnotacao = new CadastroAnotacaoApiModel.CadastroAnotacaoApiModel();

            //For que percorre todas linhas dos arquivos, para verificar se ele se enquadra em todas verificações. Se não gera um erro e é colocado na grid problema.
            int countErro = 0;
            var indexConsultores = 0;

            if (objInserir.DATA_XLSX[0][0] == "CODIGO" &&
                objInserir.DATA_XLSX[0][1] == "CNPJ" &&
                objInserir.DATA_XLSX[0][2] == "GRUPO ECONÔMICO" &&
                objInserir.DATA_XLSX[0][3] == "RAZÃO SOCIAL" &&
                objInserir.DATA_XLSX[0][4] == "TELEFONE" &&
                objInserir.DATA_XLSX[0][5] == "TELEFONE" &&
                objInserir.DATA_XLSX[0][6] == "EMAIL" &&
                objInserir.DATA_XLSX[0][7] == "SERVICO" &&
                objInserir.DATA_XLSX[0][8] == "TIPO NEGOCIACÃO" &&
                objInserir.DATA_XLSX[0][9] == "INDICADO POR")
            {
                for (i = 1; i <= count - 1; i++)
                {

                    countErro = 0;
                    if (objInserir.DATA_XLSX[i][0] != null && objInserir.DATA_XLSX[i][2] != null && objInserir.DATA_XLSX[i][4] != null && objInserir.DATA_XLSX[i][6] != null && objInserir.DATA_XLSX[i][7] != null && objInserir.DATA_XLSX[i][8] != null && objInserir.DATA_XLSX[i][9] != null)
                    {


                        //Valida Cliente Martins
                        if (DALOPTCONT.obterDadoBasicos(Convert.ToInt64(objInserir.DATA_XLSX[i][0].Trim())).Count <= 0)
                        {
                            CadastroOportunidadeApiModel.linhaExcelOptImport _linhaInvalida = new CadastroOportunidadeApiModel.linhaExcelOptImport();
                            _linhaInvalida.LINHA = i;
                            _linhaInvalida.MotivoErro = "Código Cliente Inválido.";
                            _linhaInvalida.CODCLI = Convert.ToInt64(objInserir.DATA_XLSX[i][0].Trim());
                            _listInvalidos.Add(_linhaInvalida);
                            countErro++;
                        }


                        //Valida CNPJ
                        if (objInserir.DATA_XLSX[i][1] != null)
                        {
                            if (objInserir.DATA_XLSX[i][1].Length != 14 || !buscaCnpjCorreto(objInserir.DATA_XLSX[i][0], objInserir.DATA_XLSX[i][1]))
                            {
                                CadastroOportunidadeApiModel.linhaExcelOptImport _linhaInvalida = new CadastroOportunidadeApiModel.linhaExcelOptImport();
                                _linhaInvalida.LINHA = i;
                                _linhaInvalida.MotivoErro = "CNPJ Inválido.";
                                _linhaInvalida.CODCLI = Convert.ToInt64(objInserir.DATA_XLSX[i][0].Trim());
                                _listInvalidos.Add(_linhaInvalida);
                                countErro++;
                            }
                        }

                        //Valida Grupo Econom. - NAO VAI NO INSERT-- PERGUNTAR
                        var codigoGrpEcon = Convert.ToInt64(objInserir.DATA_XLSX[i][2]);
                        _grupoeco = dalgrupoEco.BuscaGrupoEconomicoAtivo(codigoGrpEcon);
                        if (_grupoeco == null)
                        {
                            CadastroOportunidadeApiModel.linhaExcelOptImport _linhaInvalida = new CadastroOportunidadeApiModel.linhaExcelOptImport();
                            _linhaInvalida.LINHA = i;
                            _linhaInvalida.MotivoErro = "Grupo Econômico Inválido.";
                            _linhaInvalida.CODCLI = Convert.ToInt64(objInserir.DATA_XLSX[i][0].Trim());
                            _listInvalidos.Add(_linhaInvalida);
                            countErro++;
                        }

                        //Valida Tel Nulo
                        if (objInserir.DATA_XLSX[i][4] == null)
                        {
                            CadastroOportunidadeApiModel.linhaExcelOptImport _linhaInvalida = new CadastroOportunidadeApiModel.linhaExcelOptImport();
                            _linhaInvalida.LINHA = i;
                            _linhaInvalida.MotivoErro = "Telefone em Branco.";
                            _linhaInvalida.CODCLI = Convert.ToInt64(objInserir.DATA_XLSX[i][0].Trim());
                            _listInvalidos.Add(_linhaInvalida);
                            countErro++;
                        }

                        //Valida Tel Invalido
                        if (objInserir.DATA_XLSX[i][4].Length < 8)
                        {
                            CadastroOportunidadeApiModel.linhaExcelOptImport _linhaInvalida = new CadastroOportunidadeApiModel.linhaExcelOptImport();
                            _linhaInvalida.LINHA = i;
                            _linhaInvalida.MotivoErro = "Telefone Inválido.";
                            _linhaInvalida.CODCLI = Convert.ToInt64(objInserir.DATA_XLSX[i][0].Trim());
                            _listInvalidos.Add(_linhaInvalida);
                            countErro++;
                        }


                        //Valida Serviço
                        var codigoServico = Convert.ToInt32(objInserir.DATA_XLSX[i][7]);
                        _servicoObjPesquisa.CODSVCTGV = codigoServico;
                        _servicoModel = dalCombosCodServico.obterCodigosServicos(_servicoObjPesquisa).FirstOrDefault();
                        if (_servicoModel == null)
                        {
                            CadastroOportunidadeApiModel.linhaExcelOptImport _linhaInvalida = new CadastroOportunidadeApiModel.linhaExcelOptImport();
                            _linhaInvalida.LINHA = i;
                            _linhaInvalida.MotivoErro = "Serviço Inválido.";
                            _linhaInvalida.CODCLI = Convert.ToInt64(objInserir.DATA_XLSX[i][0].Trim());
                            _listInvalidos.Add(_linhaInvalida);
                            countErro++;
                        }

                        //Valida Tipo Negociação
                        var tipNegociacao = objInserir.DATA_XLSX[i][8];
                        if (validaTipoNegoc(objInserir.DATA_XLSX[i][8]) == -1)
                        {
                            CadastroOportunidadeApiModel.linhaExcelOptImport _linhaInvalida = new CadastroOportunidadeApiModel.linhaExcelOptImport();
                            _linhaInvalida.LINHA = i;
                            _linhaInvalida.MotivoErro = "Tipo Negociação Inválido.";
                            _linhaInvalida.CODCLI = Convert.ToInt64(objInserir.DATA_XLSX[i][0].Trim());
                            _listInvalidos.Add(_linhaInvalida);
                            countErro++;
                        }

                        //Valida Indicado Por
                        var indicadoPor = objInserir.DATA_XLSX[i][9];
                        if (buscaCodPesInd(objInserir.DATA_XLSX[i][9]) == -1)
                        {
                            CadastroOportunidadeApiModel.linhaExcelOptImport _linhaInvalida = new CadastroOportunidadeApiModel.linhaExcelOptImport();
                            _linhaInvalida.LINHA = i;
                            _linhaInvalida.MotivoErro = "Indicado Por Inválido.";
                            _linhaInvalida.CODCLI = Convert.ToInt64(objInserir.DATA_XLSX[i][0].Trim());
                            _listInvalidos.Add(_linhaInvalida);
                            countErro++;
                        }


                    }
                    else
                    {
                        CadastroOportunidadeApiModel.linhaExcelOptImport _linhaInvalida = new CadastroOportunidadeApiModel.linhaExcelOptImport();
                        _linhaInvalida.LINHA = i;
                        //_linhaInvalida.MotivoErro = "Campo Obrigatório Vazio.";
                        if (objInserir.DATA_XLSX[i][0] == null)
                        {
                            _linhaInvalida.MotivoErro = "Código Cliente em Branco.";
                            _linhaInvalida.CODCLI = 000000;
                        }
                        if (objInserir.DATA_XLSX[i][2] == null)
                        {
                            _linhaInvalida.MotivoErro = "Grupo Econômico em Branco.";
                            _linhaInvalida.CODCLI = Convert.ToInt64(objInserir.DATA_XLSX[i][0].Trim());
                        }
                        if (objInserir.DATA_XLSX[i][4] == null)
                        {
                            _linhaInvalida.MotivoErro = "Telefone em Branco.";
                            _linhaInvalida.CODCLI = Convert.ToInt64(objInserir.DATA_XLSX[i][0].Trim());
                        }
                        if (objInserir.DATA_XLSX[i][6] == null)
                        {
                            _linhaInvalida.MotivoErro = "Email em Branco.";
                            _linhaInvalida.CODCLI = Convert.ToInt64(objInserir.DATA_XLSX[i][0].Trim());
                        }
                        if (objInserir.DATA_XLSX[i][7] == null)
                        {
                            _linhaInvalida.MotivoErro = "Código Serviço em Branco.";
                            _linhaInvalida.CODCLI = Convert.ToInt64(objInserir.DATA_XLSX[i][0].Trim());
                        }
                        if (objInserir.DATA_XLSX[i][8] == null)
                        {
                            _linhaInvalida.MotivoErro = "Tipo Negociação em Branco.";
                            _linhaInvalida.CODCLI = Convert.ToInt64(objInserir.DATA_XLSX[i][0].Trim());
                        }
                        if (objInserir.DATA_XLSX[i][9] == null)
                        {
                            _linhaInvalida.MotivoErro = "Indicado Por em Branco.";
                            _linhaInvalida.CODCLI = Convert.ToInt64(objInserir.DATA_XLSX[i][0].Trim());
                        }

                        _listInvalidos.Add(_linhaInvalida);
                        countErro++;
                    }

                    if (countErro == 0)
                    {
                        CadastroOportunidadeTO.obterMaximoOportunidade _OPT = new CadastroOportunidadeTO.obterMaximoOportunidade();
                        _OPT = DALOPT.obterMaximoOportunidade();
                        var _CODIGOOPORTUNIDADE = _OPT.CODOPTVND + 10;
                        if (indexConsultores >= objInserir.CONSULTORES.Count) { indexConsultores = 0; }
                        insertSucessosOpt(indexConsultores, objInserir, i, DALOPT, BLLOPTSVC, DALAGND, _clienteopt, ref _insert, ref _objInserirAnotacao, ref _CODIGOOPORTUNIDADE);
                        _retornoImport.qtdSucesso++;
                        indexConsultores++;
                    }
                }

                _retornoImport.listErro = _listInvalidos;
            }
            else
            {
                _retornoImport.qtdSucesso = -1;
            }

            

            return _retornoImport;

        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO IMPORTAR ARQUIVO DE NOVAS NEGOCIAÇÕES.");
            Utilitario.InsereLog(ex, "ImportarNegociacoes", "CadastroOportunidadeGeralApiModel.ImportaNegociacaoApiModel", "CadastroOportunidadeGeralController", "ERRO AO IMPORTAR ARQUIVO DE NOVAS NEGOCIAÇÕES");
            throw;
        }

    }

    private void insertSucessosOpt(int indexConsultores,CadastroOportunidadeGeralApiModel.ImportaNegociacaoApiModel objInserir, int i, CadastroOportunidadeDAL DALOPT, CadastroServicoOportunidadeBLL BLLOPTSVC, CadastroAnotacaoBLL DALAGND, CadastroServicoOportunidadeApiModel.CadastroServicoOportunidadeApiModel _clienteopt, ref CadastroOportunidadeApiModel.CadastroOportunidadeApiModel _insert, ref CadastroAnotacaoApiModel.CadastroAnotacaoApiModel _objInserirAnotacao, ref int _CODIGOOPORTUNIDADE)
    {
        using (TransactionScope Scope = new TransactionScope(TransactionScopeOption.Required, (new TimeSpan(0, 10, 0)))) // DE ATÉ 10 MINUTOS.
        {
            try
            {
                
                if (indexConsultores >= objInserir.CONSULTORES.Count)
                {
                    indexConsultores = 0;
                }

                _insert.CODCLI = Convert.ToInt64(objInserir.DATA_XLSX[i][0].Trim());
                _insert.CODOPTVND = _CODIGOOPORTUNIDADE;
                _insert.CODCNIVNDTGV = objInserir.CONSULTORES[indexConsultores];//int.Parse(objInserir.DATA_XLSX[i][10].Trim()); //VAI VIR DO MODAL CONSULTORES COD CONSULTOR
                _insert.NUMSEQPESIND = buscaCodPesInd(objInserir.DATA_XLSX[i][9]); //94; // INDICADO POR
                _insert.CODCNLORICLI = validaTipoNegoc(objInserir.DATA_XLSX[i][8]); //_insert.CODCNLORICLI = 18; antigo era setado assim perguntar se é o codigo do tipo da neg.



                if (objInserir.DATA_XLSX[i][4].Length < 10)
                    _insert.NUMTLFCTO = objInserir.DATA_XLSX[i][4].Trim().PadLeft(10, '0');
                else
                    _insert.NUMTLFCTO = objInserir.DATA_XLSX[i][4].Trim();


                if (_insert.NUMTLFCTO != null)
                    _insert.NUMTLFCTO = Regex.Replace(_insert.NUMTLFCTO, "[-)( ]", String.Empty);



                _insert.CODEDE = Convert.ToInt64(objInserir.DATA_XLSX[i][0].Trim());


                if (objInserir.CONSULTORES.Count > 0)
                {
                    _insert.NOMCTO = objInserir.NOMESCONSULTORES[indexConsultores];//VAI VIR DO MODAL CONSULTORES SE O NOME RESPONSAVEL FOR O NOME DO CONSULTOR
                    _insert.NOMCTORPNCLI = objInserir.NOMESCONSULTORES[indexConsultores];
                }

                indexConsultores++;


                if (objInserir.DATA_XLSX[i][5] != null)
                {
                    if (objInserir.DATA_XLSX[i][5].Length < 10)
                    {
                        _insert.NUMTLFCEL = objInserir.DATA_XLSX[i][5].Trim().PadLeft(10, '0');
                        _insert.NUMTLFCEL = Regex.Replace(_insert.NUMTLFCEL, "[-)( ]", String.Empty);
                    }
                    else
                        _insert.NUMTLFCEL = objInserir.DATA_XLSX[i][5].Trim();
                    _insert.NUMTLFCEL = Regex.Replace(_insert.NUMTLFCEL, "[-)( ]", String.Empty);
                }



                if (objInserir.DATA_XLSX[i][6] != null)
                {
                    _insert.DESENDETNRPNCLI = objInserir.DATA_XLSX[i][6].Trim();
                    _insert.DESENDETNCTO = objInserir.DATA_XLSX[i][6].Trim();
                }

                    DALOPT.inserirCadastroOportunidadeImport(_insert);

                _clienteopt.CODSVCTGV = Convert.ToInt32(objInserir.DATA_XLSX[i][7]);
                _clienteopt.CODOPTVND = _CODIGOOPORTUNIDADE;
                _clienteopt.INDSTANGC = validaTipoNegoc(objInserir.DATA_XLSX[i][8]);
                _clienteopt.CODGRPEMPFAT = Convert.ToInt64(objInserir.DATA_XLSX[i][2]);

                BLLOPTSVC.inserirCadastroServicoOportunidadeImport(_clienteopt);

                _objInserirAnotacao.CODCLI = _insert.CODCLI;
                _objInserirAnotacao.TIPOBSCLI = "1";
                _objInserirAnotacao.CODMTVCTO = 0;
                _objInserirAnotacao.DATAGDCTO = DateTime.Now.AddDays(7).ToString();
                _objInserirAnotacao.DESOBS = " ";
                _objInserirAnotacao.CODFNCCAD = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
                _objInserirAnotacao.DATCAD = DateTime.Now.ToShortDateString();
                _objInserirAnotacao.CODOPTVND = _insert.CODOPTVND;
                _objInserirAnotacao.NUMNIVGARNGC = -1;


                DALAGND.inserirCadastroAnotacaoGeral(_objInserirAnotacao);

                DALOPT.inserirContatoPadraoOportunidade(_insert);

                _objInserirAnotacao = new CadastroAnotacaoApiModel.CadastroAnotacaoApiModel();
                _insert = new CadastroOportunidadeApiModel.CadastroOportunidadeApiModel();


                _CODIGOOPORTUNIDADE++;
                //}
                Scope.Complete();
            }
            catch (Exception ex)
            {
                Scope.Dispose();
                throw ex;
            }
        }
    }
}