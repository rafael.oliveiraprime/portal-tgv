﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for CadastroMotivoContatoController
/// </summary>
[RoutePrefix("api/CadastroMotivoContato")]
public class CadastroMotivoContatoController : ApiController
{
    [HttpPost]
    [Route("ObterCadastroMotivoContato")]
    public List<CadastroMotivoContatoTO.obterCadastroMotivoContato> obterCadastroMotivoContato(CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel objPesquisar)
    {
        return new CadastroMotivoContatoBLL().obterCadastroMotivoContato(objPesquisar);
    }

    [HttpPost]
    [Route("InserirCadastroMotivoContato")]
    public bool inserirCadastroMotivoContato(CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel objInserir)
    {
        return new CadastroMotivoContatoBLL().inserirCadastroMotivoContato(objInserir);
    }

    [HttpPost]
    [Route("AlterarCadastroMotivoContato")]
    public bool alterarCadastroMotivoContato(CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel objAlterar)
    {
        return new CadastroMotivoContatoBLL().alterarCadastroMotivoContato(objAlterar);
    }

    [HttpPost]
    [Route("AtivarCadastroMotivoContato")]
    public bool ativarCadastroMotivoContato(CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel objAlterar)
    {
        return new CadastroMotivoContatoBLL().ativarCadastroMotivoContato(objAlterar);
    }

    [HttpPost]
    [Route("DesativarCadastroMotivoContato")]
    public bool desativarCadastroMotivoContato(CadastroMotivoContatoApiModel.CadastroMotivoContatoApiModel objAlterar)
    {
        return new CadastroMotivoContatoBLL().desativarCadastroMotivoContato(objAlterar);
    }
}