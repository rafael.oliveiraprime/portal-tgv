﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for CadastroServicoController
/// </summary>
[RoutePrefix("api/CadastroCondicao")]
public class CadastroCondicaoController : ApiController
{
    [HttpPost]
    [Route("ObterCadastroCondicao")]
    public List<CadastroCondicaoTO.obterCadastroCondicao> obterCadastroCondicao(CadastroCondicaoApiModel.CadastroCondicaoApiModel objPesquisar)
    {
        return new CadastroCondicaoBLL().obterCadastroCondicao(objPesquisar);
    }

    [HttpPost]
    [Route("InserirCadastroCondicao")]
    public bool inserirCadastroCondicao(CadastroCondicaoApiModel.inserirCadastroCondicao objInserir)
    {
        return new CadastroCondicaoBLL().inserirCadastroCondicao(objInserir);
    }

    [HttpPost]
    [Route("AlterarCadastroCondicao")]
    public bool alterarCadastroCondicao(CadastroCondicaoApiModel.alterarCadastroCondicao objAlterar)
    {
        return new CadastroCondicaoBLL().alterarCadastroCondicao(objAlterar);
    }

    [HttpPost]
    [Route("AtivarCadastroCondicao")]
    public bool ativarCadastroCondicao(CadastroCondicaoApiModel.CadastroCondicaoApiModel objAlterar)
    {
        return new CadastroCondicaoBLL().ativarCadastroCondicao(objAlterar);
    }

    [HttpPost]
    [Route("DesativarCadastroCondicao")]
    public bool desativarCadastroCondicao(CadastroCondicaoApiModel.CadastroCondicaoApiModel objAlterar)
    {
        return new CadastroCondicaoBLL().desativarCadastroCondicao(objAlterar);
    }
}