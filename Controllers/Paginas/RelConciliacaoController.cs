﻿using System.Collections.Generic;
using System.Web.Http;

[RoutePrefix("api/RelConciliacao")]
public class RelConciliacaoController : ApiController
{
    [HttpPost]
    [Route("ObterConciliacao")]
    public List<RelConciliacaoTO.obterConciliacao> ObterConciliacao(RelConciliacaoApiModel.obterRelConciliacaoApiModel objPesquisar)
    {
        return new RelConciliacaoBLL().obterConciliacao(objPesquisar);
    }

    
    [HttpPost]
    [Route("obterTotaisReceita")]
    public List<RelPontuacaoTO.obterTotaisReceita> obterTotaisReceita(RelPontuacaoApiModel.obterPontuacaoApiModel objPesquisar)
    {
        return new RelPontuacaoBLL().obterTotaisReceita(objPesquisar);
    }

}





