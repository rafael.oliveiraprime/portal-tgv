﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Data.OleDb;
using System.Net;
using System.IO;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using Excel = Microsoft.Office.Interop.Excel;



/// <summary>
/// Summary description for ConsultaCestaServicoController
/// </summary>
[RoutePrefix("api/ConsultaCestaServico")]
public class ConsultaCestaServicoController : ApiController
{
    [HttpPost]
    [Route("obterConsultaCestaServico")]
    public List<ConsultaCestaServicoTO.obterConsultaCestaServico> obterConsultaCestaServico(ConsultaCestaServicoApiModel.ConsultaCestaServicoApiModel objPesquisar)
    {
        return new ConsultaCestaServicoBLL().obterConsultaCestaServico(objPesquisar);
    }


    [HttpPost]
    [Route("UploadFile")]
    public async Task<string> UploadFile()
    {
        string nomeArquivo = string.Empty;

        ConsultaCestaServicoApiModel.ConsultaCestaServicoApiModel _objInserir = new ConsultaCestaServicoApiModel.ConsultaCestaServicoApiModel();
        var bll = new ConsultaCestaServicoBLL();
        var caminhoupload = ConfigurationManager.AppSettings["caminhoTemplateContratoVenda"];

        try
        {
            foreach (string file in HttpContext.Current.Request.Files)
            {
                var fileContent = HttpContext.Current.Request.Files[file];

                if (fileContent != null && fileContent.ContentLength > 0)
                {
                    nomeArquivo = fileContent.FileName;
                    var stream = fileContent.InputStream;
                    var path = Path.Combine(caminhoupload, nomeArquivo);
                    using (var fileStream = System.IO.File.Create(path))
                    {
                        stream.CopyTo(fileStream);
                    }
                }
            }
            await Task.Delay(0);

            string arquivo = Path.Combine(caminhoupload, nomeArquivo);

            Excel.Worksheet MySheet = null;
            Excel.Application MyApp = new Excel.Application();
            MyApp.Visible = false;
            Excel.Workbook MyBook = MyApp.Workbooks.Open(arquivo);
            MySheet = (Excel.Worksheet)MyBook.Sheets[1];

            var opa = MySheet.Rows.Count;
            var lastRow = MySheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row;

            for (int index = 2; index <= lastRow; index++)
            {
                 System.Array MyValues = (System.Array)MySheet.get_Range("A" + index.ToString(), "D" + index.ToString()).Cells.Value; 
                _objInserir.CODCLI = int.Parse(MyValues.GetValue(1, 1).ToString());
                _objInserir.ANOMESREF = int.Parse(MyValues.GetValue(1, 2).ToString());
                _objInserir.CODPRDCES = int.Parse(MyValues.GetValue(1, 3).ToString());
                _objInserir.VLRMOVSVCPRDSMA = MyValues.GetValue(1, 4).ToString();

                bll.deletarConsultaCestaServico(_objInserir);
                bll.InserirConsultaCestaServico(_objInserir);

                _objInserir = new ConsultaCestaServicoApiModel.ConsultaCestaServicoApiModel();
            }

            MyBook.Close();
            MyApp.Workbooks.Close();

            FileInfo fileexitente = new FileInfo(Path.Combine(caminhoupload, nomeArquivo));
            if (fileexitente.Exists)
                fileexitente.Delete();
        }
        catch (Exception ex)
        {
            Console.WriteLine("Erro ao acessar os dados: " + ex.Message);
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A UPLOAD DE ARQUIVO DO IMPORTAR CESTA DE PRODUTOS.");
            Utilitario.InsereLog(ex, "UploadFile", "ARQUIVO EXCEL", "ConsultaCestaServicoController", "ERRO AO FAZER A UPLOAD DE ARQUIVO DO IMPORTAR CESTA DE PRODUTOS.");
            return "1";
        }
        return "0";

    }

    [HttpPost]
    [Route("InserirConsultaCestaServico")]
    public bool inserirConsultaCestaServico(ConsultaCestaServicoApiModel.ConsultaCestaServicoApiModel objPesquisar)
    {
        return new ConsultaCestaServicoBLL().InserirConsultaCestaServico(objPesquisar);
    }

    [HttpPost]
    [Route("AlterarConsultaCestaServico")]
    public bool alterarConsultaCestaServico(ConsultaCestaServicoApiModel.ConsultaCestaServicoApiModel objAlterar)
    {
        return new ConsultaCestaServicoBLL().alterarConsultaCestaServico(objAlterar);
    }

    [HttpPost]
    [Route("InserirConsultaCestaServico2")]
    public IHttpActionResult inserirConsultaCestaServico2(ConsultaCestaServicoApiModel.ConsultaCestaServicoApiModel objPesquisar)
    {
        new ConsultaCestaServicoBLL().InserirConsultaCestaServico(objPesquisar);
        return null;
    }
}