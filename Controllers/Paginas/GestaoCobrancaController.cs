﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Threading.Tasks;
using System.IO;
using System.Transactions;


[RoutePrefix("api/GestaoCobranca")]
public class GestaoCobrancaController : ApiController
{
    [HttpPost]
    [Route("buscartitulos")]
    public string buscartitulos()
    {
        return new GestaoCobrancaBLL().buscartitulos();
    }

    [Route("obterCliTitulos")]
    public List<GestaoCobrancaTO.obterCliTitulos> obterCliTitulos(GestaoCobrancaApiModel.GestaoCobrancaApiModel _ObjPesquisar)
    {
        return new GestaoCobrancaBLL().obterCliTitulos(_ObjPesquisar);
    }

    
    [Route("ObterNotasFiscais")]
    public List<GestaoCobrancaTO.obterNotasTitulos> ObterNotasFiscais(GestaoCobrancaApiModel.GestaoCobrancaApiModel _ObjPesquisar)
    {
        return new GestaoCobrancaBLL().ObterNotasFiscais(_ObjPesquisar);
    }

    
    [Route("TransferirResponsavel")]
    public string TransferirResponsavel(GestaoCobrancaApiModel.GestaoCobrancaTranfResponsavelApiModel _ObjPesquisar)
    {
        return new GestaoCobrancaBLL().TransferirResponsavel(_ObjPesquisar);
    }

    
    [Route("obterTotaisEquipes")]
    public List<GestaoCobrancaTO.TotaisEquipe> obterTotaisEquipes()
    {
        return new GestaoCobrancaBLL().obterTotaisEquipes();
    }

    
    [Route("Distribuir")]
    public string Distribuir()
    {
        return new GestaoCobrancaBLL().Distribuir();
    }

    [Route("VerificarDistribuicao")]
    public string VerificarDistribuicao()
    {
        return new GestaoCobrancaBLL().VerificarDistribuicao();
    }

    [Route("VisualisarBoleto")]
    public string VisualisarBoleto(GestaoCobrancaApiModel.EnviarBoletoApiModel _ObjVisualizar)
    {
        return new GestaoCobrancaBLL().VisualisarBoleto(_ObjVisualizar);
    }

    
    [Route("obterCobrancaContato")]
    public List<GestaoCobrancaTO.CobrancaContato> obterCobrancaContato(GestaoCobrancaApiModel.GestaoCobrancaApiModel _ObjPesquisar)
    {
        return new GestaoCobrancaBLL().obterCobrancaContato(_ObjPesquisar);
    }

    
    [Route("EnviarBoleto")]
    public string EnviarBoleto(GestaoCobrancaApiModel.EnviarBoletoApiModel _ObjPesquisar)
    {
        return new GestaoCobrancaBLL().EnviarBoleto(_ObjPesquisar);
    }

    [HttpPost]
    [Route("AlterarStatusTitulo")]
    public string AlterarStatusTitulo(GestaoCobrancaApiModel.AlteraStatusTitulosApiModel _ObjPesquisar)
    {
        return new GestaoCobrancaBLL().AlterarStatusTitulo(_ObjPesquisar);
    }

    [HttpPost]
    [Route("UploadFile")]
    public async Task<string> UploadFile()
    {
        string nomeArquivo = "";
        string base64String = "";
        byte[] binaryData;
        long bytesRead;

        var Caminhocontrato = System.Web.HttpContext.Current.Server.MapPath("~\\UploadedFiles");

        Int64 _CODCLI;
        Int64 _TotalBoletosAtaulizados = 0;

        var BLL = new GestaoCobrancaBLL();

        try
        {
            foreach (string file in HttpContext.Current.Request.Files)
            {
                var fileContent = HttpContext.Current.Request.Files[file];
                if (fileContent != null && fileContent.ContentLength > 0)
                {
                    nomeArquivo = fileContent.FileName;

                    binaryData = new Byte[fileContent.InputStream.Length];
                    bytesRead = fileContent.InputStream.Read(binaryData, 0, (int)fileContent.InputStream.Length);
                    fileContent.InputStream.Close();
                    base64String = System.Convert.ToBase64String(binaryData, 0, binaryData.Length);

                    await Task.Delay(0);

                    FileInfo fileexitente = new FileInfo(@Caminhocontrato + "\\" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + nomeArquivo);
                    if (!fileexitente.Exists)
                    {
                        Byte[] bytes = Convert.FromBase64String(base64String);
                        File.WriteAllBytes(@Caminhocontrato + "\\" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + nomeArquivo, bytes);
                    }

                    FileInfo arquivo = new FileInfo(@Caminhocontrato + "\\" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + nomeArquivo);
                    if (arquivo.Exists)
                    {
                        using (StreamReader sr = new StreamReader(@Caminhocontrato + "\\" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + nomeArquivo))
                        {

                            using (TransactionScope Scope = new TransactionScope(TransactionScopeOption.Required, (new TimeSpan(0, 3, 0)))) // DE ATÉ 3 MINUTOS.
                            {
                                try
                                {

                                    string line;
                                    // Read and display lines from the file until the end of 
                                    // the file is reached.
                                    while ((line = sr.ReadLine()) != null)
                                    {
                                        if (line.Trim() != "")
                                        {
                                            if (line.Split(';')[0] != "CODIGO")
                                            {
                                                _CODCLI = Int64.Parse(line.Split(';')[0]);
                                                if (BLL.AtualizaNotaBoleto(_CODCLI))
                                                    _TotalBoletosAtaulizados = _TotalBoletosAtaulizados + 1;

                                            }
                                        }

                                    }
                                    Scope.Complete();
                                }
                                catch(Exception ex)
                                {
                                    Scope.Dispose();  
                                    throw ex;
                                }

                            }
                        }
                    }
                    arquivo.Delete(); 
                }
            }

            return "0;" + _TotalBoletosAtaulizados.ToString();

        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A UPLOAD DE ARQUIVO DO CADASTRO SERVICO.");
            Utilitario.InsereLog(ex, "UploadFile", "codigo", "CadastroServicoController", "ERRO AO FAZER A UPLOAD DE ARQUIVO DO CADASTRO SERVICO.");
            return "1;1";
        }

    }





    }