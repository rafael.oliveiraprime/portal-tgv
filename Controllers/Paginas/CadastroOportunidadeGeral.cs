﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Transactions;
using System.Text.RegularExpressions;


[RoutePrefix("api/CadastroOportunidadeGeral")]
public class CadastroOportunidadeGeralController : ApiController
{
    [HttpPost]
    [Route("ObterCadastroOportunidadeGeral")]
    public List<CadastroOportunidadeGeralTO.obterCadastroOportunidadeGeral> obterCadastroOportunidadeGeral(CadastroOportunidadeGeralApiModel.CadastroOportunidadeGeralApiModel objPesquisar)
    {
        return new CadastroOportunidadeGeralBLL().obterCadastroOportunidadeGeral(objPesquisar);
    }

    [HttpPost]
    [Route("AlterarResponsavelOportunidade")]
    public bool alterarResponsavelOportunidade(CadastroOportunidadeGeralApiModel.CadastroOportunidadeGeralApiModel objAlterar)
    {
        return new CadastroOportunidadeGeralBLL().alterarResponsavelOportunidade(objAlterar);
    }

    [HttpPost]
    [Route("obterOportunidadeContato")]
    public List<OportunidadeContatoTO.OportunidadeContato> obterOportunidadeContato(OportunidadeContatoTO.OportunidadeContato objPesquisar)
    {
        return new OportunidadeContatoBLL().obterOportunidadeContato(objPesquisar.CODOPTVND);
    }


    [HttpPost]
    [Route("ImportarNegociacoes")]
    public string ImportarNegociacoes(CadastroOportunidadeGeralApiModel.ImportaNegociacaoApiModel objInserir)
    {
        try
        {
            int i = 1;
            int count = objInserir.DATA_XLSX.Length;
            bool _bcamposObrigatorios = false;


            string _codigosCliente = "";
            string _codigosConsultor = "";
            string _codigoClienteNegociacao = "";
            string _codigoClienteDuplicado = "";

            List<int> _codcli = new List<int>();

            var DALOPTCONT = new OportunidadeContatoDAL();
            var DAL = new CadastroConsultorDAL();
            var DALOPT = new CadastroOportunidadeDAL();
            var DALAGND = new CadastroAnotacaoBLL();
            var dalconta = new GestaoContasDAL();

            CadastroConsultorApiModel.CadastroConsultorApiModel _consultor = new CadastroConsultorApiModel.CadastroConsultorApiModel();
            CadastroOportunidadeApiModel.CadastroOportunidadeApiModel _clienteopt = new CadastroOportunidadeApiModel.CadastroOportunidadeApiModel();
            CadastroOportunidadeApiModel.CadastroOportunidadeApiModel _insert = new CadastroOportunidadeApiModel.CadastroOportunidadeApiModel();
            CadastroGrupoEconomicoTO.BuscaGrupoEconomico _grupoeco = new CadastroGrupoEconomicoTO.BuscaGrupoEconomico();
            var dalgrupoEco = new CadastroGrupoEconomicoDAL();
            CadastroGrupoEconomicoTO.VerificaSeEDebito _VerificaDebito = new CadastroGrupoEconomicoTO.VerificaSeEDebito();
            CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel objInserirGrupo = new CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel();
            CadastroGrupoEconomicoTO.ExisteGrupoCli _existe = new CadastroGrupoEconomicoTO.ExisteGrupoCli();
            CadastroAnotacaoApiModel.CadastroAnotacaoApiModel _objInserirAnotacao = new CadastroAnotacaoApiModel.CadastroAnotacaoApiModel();




            string retorno = "<a>Importação feita com Sucesso</a>&Succeso!";
            string retornoerro = "";


            if (count <= 1)
                retornoerro = retornoerro + "<a>Não possui registros para importação</a><br><br>";


            //For que percorre todas colunas dos arquivos, para verificar se ele se enquadra em todas verificações. Se não gera um erro e é colocado na grid problema.
            for (i = 1; i <= count - 1; i++)
            {
                if (objInserir.DATA_XLSX[i][0] != null)
                {
                    if (DALOPTCONT.obterDadoBasicos(int.Parse(objInserir.DATA_XLSX[i][0].Trim())).Count <= 0)
                    {
                        _codigosCliente = _codigosCliente + objInserir.DATA_XLSX[i][0].Trim() + ";";
                    }


                    _clienteopt.CODCLI = int.Parse(objInserir.DATA_XLSX[i][0].Trim());
                    if (DALOPT.obterCadastroOportunidadeCliente(_clienteopt).CODCLI != 0)
                    {
                        _codigoClienteNegociacao = _codigoClienteNegociacao + objInserir.DATA_XLSX[i][0].Trim() + ";";
                    }

                    _clienteopt = new CadastroOportunidadeApiModel.CadastroOportunidadeApiModel();


                    if (!(_codcli.Contains(int.Parse(objInserir.DATA_XLSX[i][0].Trim()))))
                        _codcli.Add(int.Parse(objInserir.DATA_XLSX[i][0].Trim()));
                    else
                        _codigoClienteDuplicado = _codigoClienteDuplicado + objInserir.DATA_XLSX[i][0].Trim() + ";";


                }
                else
                    _bcamposObrigatorios = true;


                if (objInserir.DATA_XLSX[i][2] == null)
                    _bcamposObrigatorios = true;


                if (objInserir.DATA_XLSX[i][5] != null) {

                    _consultor.CODFNC = int.Parse(objInserir.DATA_XLSX[i][5].Trim());

                    if (DAL.obterConsultorAtivo(_consultor).Count <= 0)
                    {
                        _codigosConsultor = _codigosConsultor + objInserir.DATA_XLSX[i][5].Trim() + ";";
                    }

                    _consultor = new CadastroConsultorApiModel.CadastroConsultorApiModel();
                }
                else
                    _bcamposObrigatorios = true;
              
            }



            if (_bcamposObrigatorios)
                retornoerro = retornoerro + "<a>Existe(m) campo(s) obrigatório(s) sem preenchimento</a><br><br>";

            if (_codigosCliente.Length > 0)
                retornoerro = retornoerro + "<a>Não Existe(m) cadastro para o(s) cliente(s) : " + _codigosCliente + "</a><br><br>";

            if (_codigoClienteNegociacao.Length > 0)
                retornoerro = retornoerro + "<a>Existe negociação em aberto para o(s) cliente(s) : " + _codigoClienteNegociacao + "</a><br><br>";

            if (_codigosConsultor.Length > 0)
                retornoerro = retornoerro + "<a>Não existe(m) cadastro para o(s) consultor(es) : " + _codigosConsultor + "</a><br><br>";


            if (_codigoClienteDuplicado.Length > 0)
                retornoerro = retornoerro + "<a>Existe(m) cliente(s) duplicado(s) na planilha : " + _codigoClienteDuplicado + "</a><br><br>";


            if (retornoerro.Length > 0)
                retorno = retornoerro + "&Verificar.";
            else
            {
                CadastroOportunidadeTO.obterMaximoOportunidade _OPT = new CadastroOportunidadeTO.obterMaximoOportunidade();
                _OPT = DALOPT.obterMaximoOportunidade();
                var _CODIGOOPORTUNIDADE = _OPT.CODOPTVND + 10;

                    using (TransactionScope Scope = new TransactionScope(TransactionScopeOption.Required, (new TimeSpan(0, 10, 0)))) // DE ATÉ 10 MINUTOS.
                    {
                        try
                        {
                            for (i = 1; i <= count - 1; i++)
                            {
                                _insert.CODCLI = int.Parse(objInserir.DATA_XLSX[i][0].Trim());
                                _insert.CODOPTVND = _CODIGOOPORTUNIDADE;
                                _insert.CODCNIVNDTGV = int.Parse(objInserir.DATA_XLSX[i][5].Trim());
                                _insert.NUMSEQPESIND = 94;
                                _insert.CODCNLORICLI = 18;


                                if (objInserir.DATA_XLSX[i][2].Length < 10)
                                    _insert.NUMTLFCTO = objInserir.DATA_XLSX[i][2].Trim().PadLeft(10, '0');
                                else
                                    _insert.NUMTLFCTO = objInserir.DATA_XLSX[i][2].Trim();


                                if (_insert.NUMTLFCTO != null)
                                    _insert.NUMTLFCTO = Regex.Replace(_insert.NUMTLFCTO, "[-)( ]", String.Empty);



                                _insert.CODEDE = int.Parse(objInserir.DATA_XLSX[i][0].Trim());
                                _insert.NOMCTO = objInserir.DATA_XLSX[i][1].Trim();


                                if (objInserir.DATA_XLSX[i][3] != null)
                                {
                                    if (objInserir.DATA_XLSX[i][3].Length < 10)
                                        _insert.NUMTLFCEL = objInserir.DATA_XLSX[i][3].Trim().PadLeft(10, '0');
                                    else
                                        _insert.NUMTLFCEL = objInserir.DATA_XLSX[i][3].Trim();

                                }

                                if (_insert.NUMTLFCEL != null)
                                    _insert.NUMTLFCEL = Regex.Replace(_insert.NUMTLFCEL, "[-)( ]", String.Empty);


                                _insert.NOMCTORPNCLI = objInserir.DATA_XLSX[i][1].Trim();

                                if (objInserir.DATA_XLSX[i][4] != null) {
                                    _insert.DESENDETNRPNCLI = objInserir.DATA_XLSX[i][4].Trim();
                                    _insert.DESENDETNCTO = objInserir.DATA_XLSX[i][4].Trim();
                                }


                                DALOPT.inserirCadastroOportunidadeImport(_insert);

                                _objInserirAnotacao.CODCLI = _insert.CODCLI;
                                _objInserirAnotacao.TIPOBSCLI = "1";
                                _objInserirAnotacao.CODMTVCTO = 0;
                                _objInserirAnotacao.DATAGDCTO = DateTime.Now.AddDays(7).ToString();
                                _objInserirAnotacao.DESOBS = " ";
                                _objInserirAnotacao.CODFNCCAD = @ControleDeAcesso.ObterConjuntoDePermissoesUsuario().InformacoesUsuario.CODFNC;
                                _objInserirAnotacao.DATCAD = DateTime.Now.ToShortDateString();
                                _objInserirAnotacao.CODOPTVND = _insert.CODOPTVND;
                                _objInserirAnotacao.NUMNIVGARNGC = -1;


                                DALAGND.inserirCadastroAnotacaoGeral(_objInserirAnotacao);

                                DALOPT.inserirContatoPadraoOportunidade(_insert);

                                _objInserirAnotacao = new CadastroAnotacaoApiModel.CadastroAnotacaoApiModel();
                                _insert = new CadastroOportunidadeApiModel.CadastroOportunidadeApiModel();


                                _CODIGOOPORTUNIDADE++;
                            }
                            Scope.Complete();
                        }
                        catch (Exception ex)
                        {
                            Scope.Dispose();
                            throw ex;
                        }
                    }
            }

            return retorno;
        }
        catch (Exception ex)
        {           
            Utilitario.CriaLogErro(ex, "ERRO AO IMPORTAR ARQUIVO DE NOVAS NEGOCIAÇÕES.");
            Utilitario.InsereLog(ex, "ImportarNegociacoes", "CadastroOportunidadeGeralApiModel.ImportaNegociacaoApiModel", "CadastroOportunidadeGeralController", "ERRO AO IMPORTAR ARQUIVO DE NOVAS NEGOCIAÇÕES");
            throw;
        }

    }


}