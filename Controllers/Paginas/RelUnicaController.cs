﻿using System.Collections.Generic;
using System.Web.Http;

[RoutePrefix("api/RelUnica")]
public class RelUnicaController : ApiController
{
   
    [HttpPost]
    [Route("ObterUnica")]
    public List<RelPontuacaoTO.ObterUnica> ObterUnica(RelPontuacaoApiModel.ObterUnicaApiModel objPesquisar)
    {
        return new RelPontuacaoBLL().ObterUnica(objPesquisar);
    }


}





