﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

[RoutePrefix("api/CadastroSistema")]
public class CadastroSistemaController : ApiController
{
    [HttpPost]
    [Route("ObterCadastroSistemaAutomacao")]
    public List<CadastroSistemaTO.obterCadastroSistemaAutomacao> obterCadastroSistemaAutomacao(CadastroSistemaApiModel.CadastroSistemaApiModel objPesquisar)
    {
        return new CadastroSistemaBLL().obterCadastroSistemaAutomacao(objPesquisar);
    }

    [HttpPost]
    [Route("InserirCadastroSistema")]
    public bool inserirCadastroSistema(CadastroSistemaApiModel.CadastroSistemaApiModel objInserir)
    {
        return new CadastroSistemaBLL().inserirCadastroSistema(objInserir);
    }

    [HttpPost]
    [Route("AlterarCadastroSistemaAutomacao")]
    public bool alterarCadastroSistemaAutomacao(CadastroSistemaApiModel.CadastroSistemaApiModel objInserir)
    {
        return new CadastroSistemaBLL().alterarCadastroSistemaAutomacao(objInserir);
    }
}