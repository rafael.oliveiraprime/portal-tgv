﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Data.OleDb;
using System.Net;
using System.IO;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;

using System.Threading;
using System.Net.Http;
using System.Net.Http.Headers;




[RoutePrefix("api/CadastroServico")]
public class CadastroServicoController : ApiController
{
    [HttpPost]
    [Route("ObterCadastroServico")]
    public List<CadastroServicoTO.obterCadastroServico> obterCadastroServico(CadastroServicoApiModel.CadastroServicoApiModel objPesquisar)
    {

        var bll = new CadastroServicoBLL();
        return bll.obterCadastroServico(objPesquisar);
    }   

    [HttpPost]
    [Route("InserirCadastroServico")]
    public bool inserirCadastroServico(CadastroServicoApiModel.CadastroServicoApiModel objInserir)
    {
        return new CadastroServicoBLL().inserirCadastroServico(objInserir);
    }

    [HttpPost]
    [Route("AlterarCadastroServico")]
    public bool alterarCadastroServico(CadastroServicoApiModel.CadastroServicoApiModel objAlterar)
    {
        return new CadastroServicoBLL().alterarCadastroServico(objAlterar);
    }

    [HttpPost]
    [Route("AtivarCadastroServico")]
    public bool ativarCadastroServico(CadastroServicoApiModel.CadastroServicoApiModel objAlterar)
    {
        return new CadastroServicoBLL().ativarCadastroServico(objAlterar);
    }

    [HttpPost]
    [Route("DesativarCadastroServico")]
    public bool desativarCadastroServico(CadastroServicoApiModel.CadastroServicoApiModel objAlterar)
    {
        return new CadastroServicoBLL().desativarCadastroServico(objAlterar);
    }

    [HttpPost]
    [Route("UploadFile")]
    public async Task<string> UploadFile(string CODSVCTGV, string DESSVCTGV, string TIPSVC, string VLRSVC, string NUMPTOSVC, string PRDATI, string CODDOCS)
    {
        string nomeArquivo = "";
        string base64String = "";
        byte[] binaryData;
        long bytesRead;

        try
        {
            foreach (string file in HttpContext.Current.Request.Files)
            {
                var fileContent = HttpContext.Current.Request.Files[file];
                if (fileContent != null && fileContent.ContentLength > 0)
                {
                    nomeArquivo = fileContent.FileName;

                    binaryData = new Byte[fileContent.InputStream.Length];
                    bytesRead = fileContent.InputStream.Read(binaryData, 0, (int)fileContent.InputStream.Length);
                    fileContent.InputStream.Close();
                    base64String = System.Convert.ToBase64String(binaryData, 0, binaryData.Length);

                }
            }
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A UPLOAD DE ARQUIVO DO CADASTRO SERVICO.");
            Utilitario.InsereLog(ex, "UploadFile", "codigo", "CadastroServicoController", "ERRO AO FAZER A UPLOAD DE ARQUIVO DO CADASTRO SERVICO.");
            return "1";
        }

        var bll = new CadastroServicoBLL();
        CadastroServicoApiModel.CadastroServicoApiModel objAlterar = new CadastroServicoApiModel.CadastroServicoApiModel();
        try
        {
            objAlterar.CODSVCTGV = int.Parse(CODSVCTGV);
            objAlterar.DESSVCTGV = DESSVCTGV;
            objAlterar.TIPSVC = int.Parse(TIPSVC);
            objAlterar.VLRSVC = VLRSVC;
            objAlterar.NUMPTOSVC = NUMPTOSVC;
            objAlterar.PRDATI = int.Parse(PRDATI);
            objAlterar.CDOANXCTTCLI = CODSVCTGV.PadLeft(3, '0') + "_" + nomeArquivo + " ; " + base64String;
            objAlterar.CODDOCS = CODDOCS;

            bll.alterarCadastroServico(objAlterar);

        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO SALVAR DADOS DO CADASTRO DE SERVIÇO.");
            Utilitario.InsereLog(ex, "UploadFile", "CODSVCTGV, DESSVCTGV, TIPSVC, VLRSVC, NUMPTOSVC, PRDATI", "CadastroServicoController", "ERRO AO SALVAR DADOS DO CADASTRO DE SERVIÇO.");
            return "2";
        }
        await Task.Delay(0);

        return "0";
    }

    [HttpPost]
    [Route("VisualizarTemplate")]
    public string VisualizarTemplate(CadastroServicoApiModel.CadastroServicoApiModel objPesquisar)
    {
        return new CadastroServicoBLL().VisualizarTemplate(objPesquisar);
    }

    [HttpPost]
    [Route("MontarBandeira")]
    public List<CadastroServicoTO.obterCadastroBandeiras> MontarBandeira()
    {

        var bll = new CadastroServicoBLL();
        return bll.MontarBandeira();
    }

}





