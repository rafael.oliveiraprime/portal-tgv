﻿using System.Collections.Generic;
using System.Web.Http;

[RoutePrefix("api/RelIndicacoes")]
public class RelIndicacoesController : ApiController
{
    [HttpPost]
    [Route("ObterIndicacoes")]
    public List<RelIndicacoesTO.obterIndicacoes> obterIndicacoes(RelIndicacoesApiModel.obterIndicacoesApiModel objPesquisar)
    {
        return new RelIndicacoesBLL().obterIndicacoes(objPesquisar);
    }
}





