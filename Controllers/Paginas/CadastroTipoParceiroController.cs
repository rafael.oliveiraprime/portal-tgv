﻿using System.Collections.Generic;
using System.Web.Http;

/// <summary>
/// Summary description for CadastroTipoContatoController
/// </summary>
[RoutePrefix("api/CadastroTipoParceiro")]
public class CadastroTipoParceiroController : ApiController
{
    [HttpPost]
    [Route("ObterCadastroTipoParceiro")]
    public List<CadastroTipoParceiroTO.obterCadastroTipoParceiro> obterCadastroTipoParceiro(CadastroTipoParceiroApiModel.CadastroTipoParceiroApiModel objPesquisar)
    {
        return new CadastroTipoParceiroBLL().obterCadastroTipoParceiro(objPesquisar);
    }

    [HttpPost]
    [Route("InserirCadastroTipoParceiro")]
    public bool inserirCadastroTipoParceiro(CadastroTipoParceiroApiModel.CadastroTipoParceiroApiModel objInserir)
    {
        return new CadastroTipoParceiroBLL().inserirCadastroTipoParceiro(objInserir);
    }

    [HttpPost]
    [Route("AlterarCadastroTipoParceiro")]
    public bool alterarCadastroTipoParceiro(CadastroTipoParceiroApiModel.CadastroTipoParceiroApiModel objAlterar)
    {
        return new CadastroTipoParceiroBLL().alterarCadastroTipoParceiro(objAlterar);
    }

    [HttpPost]
    [Route("AtivarCadastroTipoParceiro")]
    public bool ativarCadastroTipoParceiro(CadastroTipoParceiroApiModel.CadastroTipoParceiroApiModel objAlterar)
    {
        return new CadastroTipoParceiroBLL().ativarCadastroTipoParceiro(objAlterar);
    }

    [HttpPost]
    [Route("DesativarCadastroTipoParceiro")]
    public bool desativarCadastroTipoParceiro(CadastroTipoParceiroApiModel.CadastroTipoParceiroApiModel objAlterar)
    {
        return new CadastroTipoParceiroBLL().desativarCadastroTipoParceiro(objAlterar);
    }
}