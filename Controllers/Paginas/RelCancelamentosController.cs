﻿using System.Collections.Generic;
using System.Web.Http;

[RoutePrefix("api/RelCancelamentos")]
public class RelCancelamentosController : ApiController
{
    [HttpPost]
    [Route("ObterCancelamentos")]
    public List<RelCancelamentosTO.obterCancelamentos> obterCancelamentos(RelCancelamentosApiModel.obterCancelamentosApiModel objPesquisar)
    {
        return new RelCancelamentosBLL().obterCancelamentos(objPesquisar);
    }
}





