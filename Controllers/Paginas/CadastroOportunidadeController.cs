﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Http;

[RoutePrefix("api/CadastroOportunidade")]
public class CadastroOportunidadeController : ApiController
{
    [HttpPost]
    [Route("ObterCadastroOportunidade")]
    public List<CadastroOportunidadeTO.obterCadastroOportunidade> obterCadastroOportunidade(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objPesquisar)
    {
        return new CadastroOportunidadeBLL().obterCadastroOportunidade(objPesquisar);
    }

    //[HttpPost]
    //[Route("ObterCadastroOportunidadeCliente")]
    //public CadastroOportunidadeTO.obterCadastroOportunidadeCliente obterCadastroOportunidadeCliente(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objPesquisar)
    //{
    //    return new CadastroOportunidadeBLL().obterCadastroOportunidadeCliente(objPesquisar);
    //}

    [HttpPost]
    [Route("InserirCadastroOportunidade")]
    public string inserirCadastroOportunidade(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir)
    {
        string dataHoraGravação = DateTime.Now.ToString();

        var bllConsultor = new CadastroOportunidadeBLL();
        var dalgrupoEco = new CadastroGrupoEconomicoDAL();
        var dalconta = new GestaoContasDAL();
        int grupoServico = 0;

        CadastroGrupoEconomicoTO.BuscaGrupoEconomico _grupoeco = new CadastroGrupoEconomicoTO.BuscaGrupoEconomico();
        CadastroGrupoEconomicoTO.ExisteGrupoCli _existe = new CadastroGrupoEconomicoTO.ExisteGrupoCli();
        CadastroGrupoEconomicoTO.VerificaSeEDebito _VerificaDebito = new CadastroGrupoEconomicoTO.VerificaSeEDebito();
        CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel objInserirGrupo = new CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel();



        using (TransactionScope Scope = new TransactionScope(TransactionScopeOption.Required, (new TimeSpan(0, 3, 0)))) // DE ATÉ 3 MINUTOS.
        {
            objInserir.DATCAD = dataHoraGravação;

            if (new CadastroOportunidadeBLL().obterCadastroOportunidadeCliente(objInserir).CODCLI != 0)
            {
                Scope.Dispose();
                return "Cliente já tem uma Oportunidade em Andamento!";
            }

            if (new CadastroOportunidadeBLL().obterConsultorDistribuicao(objInserir).CODFNC == 0)
            {
                Scope.Dispose();
                return "Não existe nenhum consultor disponível para distribuição, favor verificar!";
            }

            //VerificaExiste se ja existexite 
            if (objInserir.CODGRPEMPCLI > 0)
            {
                _grupoeco = dalgrupoEco.BuscaGrupoEconomico(objInserir.CODGRPEMPCLI);
                if (_grupoeco != null)
                {
                    if (_grupoeco.CODCLIGRPFAT != objInserir.CODCLI)
                    {
                        _existe = dalgrupoEco.ExisteGrupoCli(objInserir.CODCLI);
                        if (_existe.EXISTE == 0)
                        {
                            if (_grupoeco.INDFATGRP == 1)
                            {
                                //procura tipo de conta debito ou boleto
                                _VerificaDebito = dalgrupoEco.VerificaSeEDebito(objInserir.CODGRPEMPCLI);
                                if (_VerificaDebito != null)
                                {
                                    //insere ela no grupo  e muda  na gestão de conta 
                                    dalconta.AlteraTipoDebito(_VerificaDebito.INDDBTAUTPGT, objInserir.CODCLI);
                                }

                                grupoServico = _grupoeco.CODGRPEMPCLI;
                            }

                            objInserirGrupo.CODCLI = objInserir.CODCLI;
                            objInserirGrupo.CODGRPEMPCLI = objInserir.CODGRPEMPCLI;
                            dalgrupoEco.inserirCadastroGrupoEconomicoXClientes(objInserirGrupo);
                        }
                    }
                }

            }

            if (new CadastroOportunidadeBLL().inserirCadastroOportunidade(objInserir) == false)
            {
                Scope.Dispose();
                return "Erro ao inserir a Oportunidade!";
            }

            if (objInserir.LISTCODSVCTGV != null)
            {
                if (new CadastroOportunidadeBLL().inserirCadastroServicoOportunidadeNovo(objInserir, grupoServico) == false)
                {
                    Scope.Dispose();
                    return "Erro ao inserir o Serviço da Oportunidade!";
                }
            }

            var DAL = new CadastroOportunidadeDAL();
            objInserir.CODOPTVND = DAL.obterMaximoOportunidade().CODOPTVND;

            CadastroAnotacaoApiModel.CadastroAnotacaoApiModel _objInserirAnotacao = new CadastroAnotacaoApiModel.CadastroAnotacaoApiModel();

            _objInserirAnotacao.CODCLI = objInserir.CODCLI;
            _objInserirAnotacao.TIPOBSCLI = "1";
            _objInserirAnotacao.CODMTVCTO = 0;
            _objInserirAnotacao.DATAGDCTO = DateTime.Now.AddDays(1).ToString();
            if (objInserir.DESOBS != null)
            {
                _objInserirAnotacao.DESOBS = objInserir.DESOBS;
            }
            else
            {
                _objInserirAnotacao.DESOBS = " ";
            }
            _objInserirAnotacao.CODFNCCAD = objInserir.CODFNCCAD;
            _objInserirAnotacao.DATCAD = objInserir.DATCAD;
            _objInserirAnotacao.CODOPTVND = objInserir.CODOPTVND;
            _objInserirAnotacao.NUMNIVGARNGC = objInserir.NUMNIVGARNGC;

            if (new CadastroAnotacaoBLL().inserirCadastroAnotacaoGeral(_objInserirAnotacao) == false)
            {
                Scope.Dispose();
                return "Erro ao inserir Anotação!";
            }

            if (new CadastroOportunidadeBLL().inserirContatoPadraoOportunidade(objInserir) == false)
            {
                Scope.Dispose();
                return "Erro ao inserir o Contato da Oportunidade!";
            }

            Scope.Complete();
            return "";
        }
    }

    [HttpPost]
    [Route("InserirCadastroServicoOportunidadeNovo")]
    public bool inserirCadastroServicoOportunidadeNovo(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir)
    {
        return new CadastroOportunidadeBLL().inserirCadastroServicoOportunidadeNovo(objInserir, 0);
    }

    [HttpPost]
    [Route("InserirContatoPadraoOportunidade")]
    public bool inserirContatoPadraoOportunidade(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir)
    {
        return new CadastroOportunidadeBLL().inserirContatoPadraoOportunidade(objInserir);
    }

    [HttpPost]
    [Route("AlterarCadastroOportunidade")]
    public string alterarCadastroOportunidade(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        return new CadastroOportunidadeBLL().alterarCadastroOportunidade(objAlterar);
    }

    [HttpPost]
    [Route("ObterClientes")]
    public CadastroOportunidadeTO.obterClientes obterClientes(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objPesquisar)
    {
        return new CadastroOportunidadeBLL().obterClientes(objPesquisar);
    }

    [HttpPost]
    [Route("ObterClientesCnpj")]
    public CadastroOportunidadeTO.obterClientesCnpj obterClientesCnpj(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objPesquisar)
    {
        return new CadastroOportunidadeBLL().obterClientesCnpj(objPesquisar);
    }


    [HttpPost]
    [Route("ObterConsultorDistribuicao")]
    public CadastroOportunidadeTO.obterConsultorDistribuicao obterConsultorDistribuicao(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objPesquisar)
    {
        return new CadastroOportunidadeBLL().obterConsultorDistribuicao(objPesquisar);
    }

    [HttpPost]
    [Route("AlterarConsultorOportunidadeNovo")]
    public bool alterarConsultorOportunidadeNovo(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        return new CadastroOportunidadeBLL().alterarConsultorOportunidadeNovo(objAlterar);
    }

    //[HttpPost]
    //[Route("AlterarConsultorOportunidadeEditar")]
    //public bool alterarConsultorOportunidadeEditar(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    //{
    //    return new CadastroOportunidadeBLL().alterarConsultorOportunidadeEditar(objAlterar);
    //}

    [HttpPost]
    [Route("RenviarEmailServicoOportunidade")]
    public bool RenviarEmailServicoOportunidade(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        return new CadastroOportunidadeBLL().RenviarEmailServicoOportunidade(objAlterar);
    }

    [HttpPost]
    [Route("RenviarEmailServicoOportunidadeDocumento")]
    public bool RenviarEmailServicoOportunidadeDocumento(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {
         return new CadastroOportunidadeBLL().RenviarEmailServicoOportunidadeDocumento(objAlterar);
    }

    [HttpPost]
    [Route("RenviarEmailServicoOportunidadeDocumentoNovo")]
    public bool RenviarEmailServicoOportunidadeDocumentoNovo(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {

        //Ajustar enviar email e inserir no banco
        return new CadastroOportunidadeBLL().RenviarEmailServicoOportunidadeDocumentoNovo(objAlterar);
    }

    [Route("CancelaCadastroOportunidade")]
    public bool CancelaCadastroOportunidade(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        return new CadastroOportunidadeBLL().CancelaCadastroOportunidade(objAlterar);
    }

    [HttpPost]
    [Route("EnviarParaAprovacao")]
    public bool EnviarParaAprovacao(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        return new CadastroOportunidadeBLL().EnviarParaAprovacao(objAlterar);
    }
    
    [HttpPost]
    [Route("InserirContratosBanco")]
    public bool InserirContratosBanco()
    {
        return new CadastroOportunidadeBLL().InserirContratosBanco();
    }

    
    [HttpPost]
    [Route("VisualizarContrato")]
    public string VisualizarContrato(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        return new CadastroOportunidadeBLL().VisualizarContrato(objAlterar);
    }

    
    [Route("ConcluirCadastroOportunidade")]
    public bool ConcluirCadastroOportunidade(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objAlterar)
    {
        return new CadastroOportunidadeBLL().ConcluirCadastroOportunidade(objAlterar);
    }

    
    [HttpPost]
    [Route("AtualizaGrupoServico")]
    public bool AtualizaGrupoServico(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir)
    {
        return new CadastroOportunidadeBLL().AtualizaGrupoServico(objInserir);
    }

    [HttpPost]
    [Route("AlterarOportunidadeIndicadoPor")]
    public bool AlterarOportunidadeIndicadoPor(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir)
    {
        return new CadastroOportunidadeBLL().AlterarOportunidadeIndicadoPor(objInserir);
    }

    [HttpPost]
    [Route("AlterarOportunidadeOrigemIndicacao")]
    public bool AlterarOportunidadeOrigemIndicacao(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir)
    {
        return new CadastroOportunidadeBLL().AlterarOportunidadeOrigemIndicacao(objInserir);
    }

    [HttpPost]
    [Route("ForcarVendaGanhaDocumentosOportunidade")]
    public bool ForcarVendaGanhaDocumentosOportunidade(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objInserir)
    {
        return new CadastroOportunidadeBLL().ForcarVendaGanhaDocumentosOportunidade(objInserir);
    }


}