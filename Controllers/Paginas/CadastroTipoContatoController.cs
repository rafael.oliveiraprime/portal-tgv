﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for CadastroTipoContatoController
/// </summary>
[RoutePrefix("api/CadastroTipoContato")]
public class CadastroTipoContatoController : ApiController
{
    [HttpPost]
    [Route("ObterCadastroTipoContato")]
    public List<CadastroTipoContatoTO.obterCadastroTipoContato> obterCadastroTipoContato(CadastroTipoContatoApiModel.CadastroTipoContatoApiModel objPesquisar)
    {
        return new CadastroTipoContatoBLL().obterCadastroTipoContato(objPesquisar);
    }

    [HttpPost]
    [Route("InserirCadastroTipoContato")]
    public bool inserirCadastroTipoContato(CadastroTipoContatoApiModel.CadastroTipoContatoApiModel objInserir)
    {
        return new CadastroTipoContatoBLL().inserirCadastroTipoContato(objInserir);
    }

    [HttpPost]
    [Route("AlterarCadastroTipoContato")]
    public bool alterarCadastroTipoContato(CadastroTipoContatoApiModel.CadastroTipoContatoApiModel objAlterar)
    {
        return new CadastroTipoContatoBLL().alterarCadastroTipoContato(objAlterar);
    }

    [HttpPost]
    [Route("AtivarCadastroTipoContato")]
    public bool ativarCadastroTipoContato(CadastroTipoContatoApiModel.CadastroTipoContatoApiModel objAlterar)
    {
        return new CadastroTipoContatoBLL().ativarCadastroTipoContato(objAlterar);
    }

    [HttpPost]
    [Route("DesativarCadastroTipoContato")]
    public bool desativarCadastroTipoContato(CadastroTipoContatoApiModel.CadastroTipoContatoApiModel objAlterar)
    {
        return new CadastroTipoContatoBLL().desativarCadastroTipoContato(objAlterar);
    }
}