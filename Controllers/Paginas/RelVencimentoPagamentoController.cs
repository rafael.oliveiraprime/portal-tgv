﻿using System.Collections.Generic;
using System.Web.Http;

[RoutePrefix("api/RelVencimentoPagamento")]
public class RelVencimentoPagamentoController : ApiController
{
    [HttpPost]
    [Route("ObterVencimentoPagamento")]
    public List<RelVencimentoPagamentoTO.ObterVencimentoPagamento> ObterVencimentoPagamento(RelVencimentoPagamentoApiModel.ObterVencimentoPagamento objPesquisar)
    {
        return new RelVencimentoPagamentoBLL().ObterVencimentoPagamento(objPesquisar);
    }
}





