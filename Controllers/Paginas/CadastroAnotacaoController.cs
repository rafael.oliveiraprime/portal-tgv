﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

[RoutePrefix("api/CadastroAnotacao")]
public class CadastroAnotacaoController : ApiController
{
    [HttpPost]
    [Route("ObterCadastroAnotacao")]
    public List<CadastroAnotacaoTO.obterCadastroAnotacao> obterCadastroAnotacao(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objPesquisar)
    {
        return new CadastroAnotacaoBLL().obterCadastroAnotacao(objPesquisar);
    }

    [HttpPost]
    [Route("ObterCadastroAnotacaoOportunidade")]
    public List<CadastroAnotacaoTO.obterCadastroAnotacaoOportunidade> obterCadastroAnotacaoOportunidade(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objPesquisar)
    {
        return new CadastroAnotacaoBLL().obterCadastroAnotacaoOportunidade(objPesquisar);
    }

    [HttpPost]
    [Route("ObterPrimeiraAnotacaoOportunidade")]
    public CadastroAnotacaoTO.obterPrimeiraAnotacaoOportunidade obterPrimeiraAnotacaoOportunidade(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objPesquisar)
    {
        return new CadastroAnotacaoBLL().obterPrimeiraAnotacaoOportunidade(objPesquisar);
    }

    [HttpPost]
    [Route("ObterCadastroAnotacaoCrm")]
    public List<CadastroAnotacaoTO.obterCadastroAnotacaoCrm> obterCadastroAnotacaoCrm(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objPesquisar)
    {
        return new CadastroAnotacaoBLL().obterCadastroAnotacaoCrm(objPesquisar);
    }

    [HttpPost]
    [Route("InserirCadastroAnotacaoNegociacao")]
    public bool inserirCadastroAnotacaoNegociacao(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objInserir)
    {
        return new CadastroAnotacaoBLL().inserirCadastroAnotacaoNegociacao(objInserir);
    }

    [HttpPost]
    [Route("InserirCadastroAnotacaoGeral")]
    public bool inserirCadastroAnotacaoGeral(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objInserir)
    {
        return new CadastroAnotacaoBLL().inserirCadastroAnotacaoGeral(objInserir);
    }

    [HttpPost]
    [Route("InserirCadastroAnotacaoOportunidadeNovo")]
    public bool inserirCadastroAnotacaoOportunidadeNovo(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objInserir)
    {
        return new CadastroAnotacaoBLL().inserirCadastroAnotacaoOportunidadeNovo(objInserir);
    }

    [HttpPost]
    [Route("InserirCadastroAnotacaoOportunidade")]
    public bool inserirCadastroAnotacaoOportunidade(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objInserir)
    {
        return new CadastroAnotacaoBLL().inserirCadastroAnotacaoOportunidade(objInserir);
    }

    [HttpPost]
    [Route("AlterarCadastroAnotacao")]
    public bool alterarCadastroAnotacao(CadastroAnotacaoApiModel.CadastroAnotacaoApiModel objAlterar)
    {
        return new CadastroAnotacaoBLL().alterarCadastroAnotacao(objAlterar);
    }
}