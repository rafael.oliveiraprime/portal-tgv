﻿using System.Web.Http;
using Arquitetura.Components;
using System.Web.SessionState;
using System.Web;
using System;

[RoutePrefix("api/Login")]
public class LoginController : ApiController, IRequiresSessionState
{

    [HttpGet]
    [Route("Deslogar")]
    [DontAuthorizeFilterApi, DontValidateApiAntiForgeryRules]
    public IHttpActionResult Deslogar()
    {
        try
        {
            if (HttpContext.Current.Session != null)
            {
                HttpContext.Current.Session.Clear();
                HttpContext.Current.Session.Abandon();
            }
            return Redirect(new Uri(Url.Content("~/Paginas/Login/Index")));
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO DESLOGAR.!!");
            Utilitario.InsereLog(ex, "Deslogar", "", "LoginController", "ERRO AO DESLOGAR.!!");
            throw;
        }

    }

    [HttpPost]
    [Route("EfetuarLogin")]
    [DontAuthorizeFilterApi]
    public LoginApiModel.RetornoEfetuarLogin EfetuarLogin(LoginApiModel.EfetuarLogin parametros)
    {
        try
        {
            var ret = new LoginApiModel.RetornoEfetuarLogin();
            //
            var grupos = ControleDeAcesso.AutenticaUsuario(parametros.USUARIO, parametros.SENHA);
            if (ReferenceEquals(grupos, null) || grupos.Count > 0)
            {
                var loginObj = ControleDeAcesso.ObterConjuntoDePermissoesUsuario(parametros.USUARIO, grupos);
                if (ReferenceEquals(loginObj, null))
                {
                    ret.MENSAGEM = "Usuário não existente na base de dados.";
                }
                else
                {
                    if (ReferenceEquals(loginObj.InformacoesUsuario, null))
                    {
                        ret.MENSAGEM = "Usuário sem permissão de acesso associada.";
                    }
                    else if (ControleDeAcesso.VerificarGrupoRedeUsuario(grupos) == false)
                    {
                        ret.MENSAGEM = "Usuário sem permissão de acesso associada.";
                    }
                    else
                    {
                        ret.CODIGO = 1;
                    }
                }
                //
            }
            else
            {
                ret.MENSAGEM = "Usuário ou senha incorretos.";
            }
            //
            return ret;
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO LOGAR.!!");
            Utilitario.InsereLog(ex, "Deslogar", "", "LoginController", "ERRO AO LOGAR.!!");
            throw;
        }
    }
}