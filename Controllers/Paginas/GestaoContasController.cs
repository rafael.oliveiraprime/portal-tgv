﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;


[RoutePrefix("api/GestaoContas")]
public class GestaoContasController : ApiController
{
    [HttpPost]
    [Route("obterGestaoContas")]
    public List<GestaoContasTO.obterGestaoContas> obterGestaoContas(GestaoContasApiModel.GestaocontasApiModel objPesquisar)
    {
        return new GestaoContasBLL().obterGestaoContas(objPesquisar);
    }

    [HttpPost]
    [Route("obeterServicosConta")]
    public List<GestaoContasTO.obeterServicosConta> obeterServicosConta(GestaoContasApiModel.GestaocontasServicoApiModel objPesquisar)
    {
        return new GestaoContasBLL().obeterServicosConta(objPesquisar);
    }

    [HttpPost]
    [Route("AcoesServicoGestaoContas")]
    public string AcoesServicoGestaoContas(GestaoContasApiModel.AcoesServicoGestaoContasApiModel objAtualizar)
    {
        return new GestaoContasBLL().AcoesServicoGestaoContas(objAtualizar);
    }


    [HttpPost]
    [Route("ObterServicoDescontoGestaoConta")]
    public List<GestaoContasTO.obterServicoDescontoGestaoConta> ObterServicoDescontoGestaoConta(GestaoContasApiModel.ServicoDescontosGestaodeContasApiModel objPesquisar)
    {
        return new GestaoContasBLL().ObterServicoDescontoGestaoConta(objPesquisar);
    }


    [HttpPost]
    [Route("InserirCadastroCondicaoComercialServicoGestaoContas")]
    public string InserirCadastroCondicaoComercialServicoGestaoContas(GestaoContasApiModel.ServicoDescontosGestaodeContasApiModel objInserir)
    {
        //valida condição comercial repitida ativa

        if (objInserir.CODCNDCMC > 0)
        {
            var DAL = new GestaoContasDAL();
            int _qtdcondicao = DAL.VerificaServicoDescontoGestaoConta(objInserir);
            if (_qtdcondicao > 0)
                return "Não é permitido incluir a mesma condião comercial";
        }



        //Validação do intervalo digitado, esse só ocorrerá caso a condição comercial tenha alguma.
        CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeNovoApiModel ObjPesquisarCondicao = new CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeNovoApiModel();
        ObjPesquisarCondicao.CODCNDCMC = objInserir.CODCNDCMC;


        if (objInserir.DATINIVLD != null)
            objInserir.DATINIVLD = "01/" + objInserir.DATINIVLD.Split('/')[1] + "/" + objInserir.DATINIVLD.Split('/')[0];

        if (objInserir.DATFIMVLD != null)
            objInserir.DATFIMVLD = DateTime.DaysInMonth(int.Parse(objInserir.DATFIMVLD.Split('/')[0]), int.Parse(objInserir.DATFIMVLD.Split('/')[1])).ToString().PadLeft(2, '0') + "/" + objInserir.DATFIMVLD.Split('/')[1] + "/" + objInserir.DATFIMVLD.Split('/')[0];


        List<CadastroCondicaoComercialServicoOportunidadeTO.verificarVigencia> LISTVERIFVIGENCIA = new CadastroCondicaoComercialServicoOportunidadeBLL().verificarVigencia(ObjPesquisarCondicao);

        if (LISTVERIFVIGENCIA[0].DATINIVLD != null || LISTVERIFVIGENCIA[0].DATFIMVLD != null || LISTVERIFVIGENCIA[0].NUMMESINIVLD != 0 || LISTVERIFVIGENCIA[0].NUMMESFIMVLD != 0)
        {
            if (objInserir.DATINIVLD != null && LISTVERIFVIGENCIA[0].NUMMESINIVLD != 0)
            {
                return "Condição Comercial possui uma restrição mensal e não de data, e não pode ser utilizada.";
            }

            if (objInserir.DATINIVLD == null)
            {
                objInserir.DATINIVLD = LISTVERIFVIGENCIA[0].DATINIVLD;
            }
            if (objInserir.DATFIMVLD == null)
            {
                objInserir.DATFIMVLD = LISTVERIFVIGENCIA[0].DATFIMVLD;
            }

            if (objInserir.DATINIVLD != null && objInserir.DATFIMVLD != null && LISTVERIFVIGENCIA[0].DATFIMVLD != null)
            {
                if (Convert.ToDateTime(objInserir.DATINIVLD) < Convert.ToDateTime(LISTVERIFVIGENCIA[0].DATINIVLD) || Convert.ToDateTime(objInserir.DATINIVLD) > Convert.ToDateTime(LISTVERIFVIGENCIA[0].DATFIMVLD)
                       || Convert.ToDateTime(objInserir.DATFIMVLD) > Convert.ToDateTime(LISTVERIFVIGENCIA[0].DATFIMVLD) || Convert.ToDateTime(objInserir.DATFIMVLD) < Convert.ToDateTime(LISTVERIFVIGENCIA[0].DATINIVLD))
                {
                    return "Intervalo de data Informado, está fora do intervalo da Condição Comercial, o intervalo é de " + LISTVERIFVIGENCIA[0].DATINIVLD + " até " + LISTVERIFVIGENCIA[0].DATFIMVLD;
                }
            }
            else if (objInserir.DATINIVLD != null && objInserir.DATFIMVLD == null || objInserir.DATINIVLD != null && objInserir.DATFIMVLD != null && LISTVERIFVIGENCIA[0].DATFIMVLD == null)
            {
                if (Convert.ToDateTime(objInserir.DATINIVLD) < Convert.ToDateTime(LISTVERIFVIGENCIA[0].DATINIVLD))
                {
                    return "Intervalo de data Informado, está fora do intervalo da Condição Comercial, o intervalo é de " + LISTVERIFVIGENCIA[0].DATINIVLD + " até " + LISTVERIFVIGENCIA[0].DATFIMVLD;
                }
            }
        }
        //------------------------------------------------

        if (new GestaoContasBLL().InserirCadastroCondicaoComercialServicoGestaoContas(objInserir) == false)
        {
            return "Ocorreu um erro ao fazer a inserção da Condição Comercial";
        }
        else
        {
            return "";
        }
    }

    [HttpPost]
    [Route("DesativarCadastroCondicaoComercialServicoGestaoContas")]
    public bool DesativarCadastroCondicaoComercialServicoGestaoContas(GestaoContasApiModel.ServicoDescontosGestaodeContasApiModel objInserir)
    {
        return new GestaoContasBLL().DesativarCadastroCondicaoComercialServicoGestaoContas(objInserir);
    }

    [HttpPost]
    [Route("AtualizarContaCorrente")]
    public string AtualizarContaCorrente(GestaoContasApiModel.GestaoContasContaCorrenteApiModel objInserir)
    {
        return new GestaoContasBLL().AtualizarContaCorrente(objInserir);
    }

    [HttpPost]
    [Route("GerarLoteFaturamento")]
    public string GerarLoteFaturamento()
    {
        return new FaturamentoBLL().GerarLoteFaturamento();
    }
    
    [HttpPost]
    [Route("ObterLoteAtual")]
    public string ObterLoteAtual()
    {
        return new FaturamentoBLL().ObterLoteAtual();
    }
    
    [HttpPost]
    [Route("AlteraQuantidade")]
    public string AlteraQuantidade(GestaoContasApiModel.GestaoContasContaAlterarQuantidadeApiModel objInserir)
    {
        return new GestaoContasBLL().AlteraQuantidade(objInserir);
    }

    [HttpPost]
    [Route("EnvioServicoEventual")]
    public string EnvioServicoEventual(GestaoContasApiModel.GestaoContasContaInserirServicoAvulsoApiModel objInserir)
    {
        return new FaturamentoBLL().EnvioServicoEventual(objInserir);
    }

    [HttpPost]
    [Route("buscartitulosAvulso")]
    public string buscartitulosAvulso(GestaoCobrancaApiModel.GestaoCobrancaApiModel _ObjPesquisar)
    {
        return new GestaoCobrancaBLL().buscartitulosAvulso(_ObjPesquisar);
    }
    
    [HttpPost]
    [Route("AtualizaGrupoServico")]
    public bool AtualizaGrupoServico(GestaoContasApiModel.GestaoContasContaAlterarGrupoFatApiModel objAlterar)
    {
        return new GestaoContasBLL().AtualizaGrupoServico(objAlterar);
    }

    [HttpPost]
    [Route("AtualizaAutomacao")]
    public bool AtualizaAutomacao(GestaoContasApiModel.GestaoContasContaAlterarAutomacaoApiModel objAlterar)
    {
        return new GestaoContasBLL().AtualizaAutomacao(objAlterar);
    }




}