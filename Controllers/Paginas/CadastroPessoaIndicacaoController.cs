﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for CadastroPessoaIndicacaoController
/// </summary>
[RoutePrefix("api/CadastroPessoaIndicacao")]
public class CadastroPessoaIndicacaoController : ApiController
{
    [HttpPost]
    [Route("ObterCadastroPessoaIndicacao")]
    public List<CadastroPessoaIndicacaoTO.obterCadastroPessoaIndicacao> obterCadastroPessoaIndicacao(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objPesquisar)
    {
        return new CadastroPessoaIndicacaoBLL().obterCadastroPessoaIndicacao(objPesquisar);
    }

    [HttpPost]
    [Route("InserirCadastroPessoaIndicacao")]
    public int inserirCadastroPessoaIndicacao(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objInserir)
    {
        try
        {
            if (objInserir.NUMCPF == null)
            {
                objInserir.NUMCPF = "";
            }

            if (new CadastroPessoaIndicacaoBLL().VerificaCPF(objInserir.NUMCPF) <= 0 || objInserir.NUMCPF == "")
            {
                if (new CadastroPessoaIndicacaoBLL().inserirCadastroPessoaIndicacao(objInserir))
                    return 0;
                else
                    return 2;
            }
            else
            {
                return 1;
            }
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO INSERIR PESSOA INDICAÇÃO.");
            Utilitario.InsereLog(ex, "inserirCadastroPessoaIndicacao", "CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel", "CadastroPessoaIndicacaoController", "ERRO AO INSERIR PESSOA INDICAÇÃO.");
            throw;
        }
    }

    [HttpPost]
    [Route("AlterarCadastroPessoaIndicacao")]
    public int alterarCadastroPessoaIndicacao(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objAlterar)
    {
        if (objAlterar.NUMCPFVER == null)
        {
            objAlterar.NUMCPFVER = "";
        }

        var bllPessoaIndicacao = new CadastroPessoaIndicacaoBLL();
        var apiPessoaIndicacao = new CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel() { };
        var PessoaIndicacao = bllPessoaIndicacao.verificaExisteMaisDeUmCPF(objAlterar);

        if (PessoaIndicacao.QTDNUMCPF == 0 || objAlterar.NUMCPF == "")
        {
            if (new CadastroPessoaIndicacaoBLL().alterarCadastroPessoaIndicacao(objAlterar))
                return 0;
            else
                return 2;
        }
        else
        {
            return 1;
        }

    }

    [HttpPost]
    [Route("AtivarCadastroPessoaIndicacao")]
    public bool ativarCadastroPessoaIndicacao(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objAlterar)
    {
        return new CadastroPessoaIndicacaoBLL().ativarCadastroPessoaIndicacao(objAlterar);
    }

    [HttpPost]
    [Route("DesativarCadastroPessoaIndicacao")]
    public bool desativarCadastroPessoaIndicacao(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objAlterar)
    {
        return new CadastroPessoaIndicacaoBLL().desativarCadastroPessoaIndicacao(objAlterar);
    }

    [HttpPost]
    [Route("ObterFuncionarios")]
    public CadastroPessoaIndicacaoTO.obterFuncionarios obterFuncionarios(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objPesquisar)
    {
        return new CadastroPessoaIndicacaoBLL().obterFuncionarios(objPesquisar);
    }

    [HttpPost]
    [Route("ObterRepresentantes")]
    public CadastroPessoaIndicacaoTO.obterRepresentantes obterRepresentantes(CadastroPessoaIndicacaoApiModel.CadastroPessoaIndicacaoApiModel objPesquisar)
    {
        return new CadastroPessoaIndicacaoBLL().obterRepresentantes(objPesquisar);
    }

}