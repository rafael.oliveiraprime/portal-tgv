﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

[RoutePrefix("api/CadastroCondicaoComercialServicoOportunidade")]
public class CadastroCondicaoComercialServicoOportunidadeController : ApiController
{
    [HttpPost]
    [Route("ObterCadastroCondicaoComercialServicoOportunidade")]
    public List<CadastroCondicaoComercialServicoOportunidadeTO.obterCadastroCondicaoComercialServicoOportunidade> obterCadastroCondicaoComercialServicoOportunidade(CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel objPesquisar)
    {
        return new CadastroCondicaoComercialServicoOportunidadeBLL().obterCadastroCondicaoComercialServicoOportunidade(objPesquisar);
    }

    [HttpPost]
    [Route("ObterDadosServicoCliente")]
    public List<GestaoContasTO.obterServicosCliente> ObterDadosServicoCliente(CadastroCondicaoComercialServicoOportunidadeApiModel.obterDadosClienteServico objPesquisar)
    {
        return new CadastroCondicaoComercialServicoOportunidadeBLL().ObterDadosServicoCliente(objPesquisar);
        //List<GestaoContasTO.obterServicosCliente> retorno = new List<GestaoContasTO.obterServicosCliente>();
        //retorno.Add(new GestaoContasTO.obterServicosCliente() { DESSVCTGV = "teste1" });
        //retorno.Add(new GestaoContasTO.obterServicosCliente() { DESSVCTGV = "teste2" });
        //retorno.Add(new GestaoContasTO.obterServicosCliente() { DESSVCTGV = "teste3" });
        //return retorno;
    }

    [HttpPost]
    [Route("ObterCadastroCondicaoComercialServicoOportunidadeSelect")]
    public List<CadastroCondicaoComercialServicoOportunidadeTO.obterCadastroCondicaoComercialServicoOportunidadeSelect> obterCadastroCondicaoComercialServicoOportunidadeSelect(CadastroCondicaoComercialServicoOportunidadeApiModel.obterCadastroCondicaoComercialServicoOportunidadeSelect objPesquisar)
    {
        return new CadastroCondicaoComercialServicoOportunidadeDAL().obterCadastroCondicaoComercialServicoOportunidadeSelect(objPesquisar);
    }

    [HttpPost]
    [Route("InserirCadastroCondicaoComercialServicoOportunidade")]
    public bool inserirCadastroCondicaoComercialServicoOportunidade(CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel objInserir)
    {
        try
        {
            return new CadastroCondicaoComercialServicoOportunidadeBLL().inserirCadastroCondicaoComercialServicoOportunidade(objInserir);
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DA CONDICAO COMERCIAL SERVICO OPORTUNIDADE.");
            throw;
        }

    }

    [HttpPost]
    [Route("DeletarCadastroCondicaoComercialServicoOportunidade")]
    public bool deletarCadastroCondicaoComercialServicoOportunidade(CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeApiModel objDeletar)
    {
        return new CadastroCondicaoComercialServicoOportunidadeBLL().deletarCadastroCondicaoComercialServicoOportunidade(objDeletar);
    }

    [HttpPost]
    [Route("InserirCadastroCondicaoComercialServicoOportunidadeNovo")]
    public string InserirCadastroCondicaoComercialServicoOportunidadeNovo(CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeNovoApiModel objInserir)
    {
        try
        {
            //Validação do intervalo digitado, esse só ocorrerá caso a condição comercial tenha alguma.

            CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeNovoApiModel ObjPesquisarCondicao = new CadastroCondicaoComercialServicoOportunidadeApiModel.CadastroCondicaoComercialServicoOportunidadeNovoApiModel();

            ObjPesquisarCondicao.CODCNDCMC = objInserir.CODCNDCMC;

            List<CadastroCondicaoComercialServicoOportunidadeTO.verificarVigencia> LISTVERIFVIGENCIA = new CadastroCondicaoComercialServicoOportunidadeBLL().verificarVigencia(ObjPesquisarCondicao);

            if (LISTVERIFVIGENCIA[0].DATINIVLD != null || LISTVERIFVIGENCIA[0].DATFIMVLD != null || LISTVERIFVIGENCIA[0].NUMMESINIVLD != 0 || LISTVERIFVIGENCIA[0].NUMMESFIMVLD != 0)
            {
                if (objInserir.NUMMESINIVLD != 0 && LISTVERIFVIGENCIA[0].DATINIVLD != null)
                {
                    return "Condição Comercial possui uma restrição de data e não mensal, o intervalo é de " + LISTVERIFVIGENCIA[0].DATINIVLD + " até " + LISTVERIFVIGENCIA[0].DATFIMVLD;
                }

                if (objInserir.DATINIVLD != null && LISTVERIFVIGENCIA[0].NUMMESINIVLD != 0)
                {
                    return "Condição Comercial possui uma restrição mensal e não de data, o intervalo é do " + LISTVERIFVIGENCIA[0].NUMMESINIVLD + "º Mês até " + LISTVERIFVIGENCIA[0].NUMMESFIMVLD + "º Mês";
                }

                if (objInserir.DATINIVLD == null)
                {
                    objInserir.DATINIVLD = LISTVERIFVIGENCIA[0].DATINIVLD;
                }
                if (objInserir.DATFIMVLD == null)
                {
                    objInserir.DATFIMVLD = LISTVERIFVIGENCIA[0].DATFIMVLD;
                }
                if (objInserir.NUMMESINIVLD == 0)
                {
                    objInserir.NUMMESINIVLD = LISTVERIFVIGENCIA[0].NUMMESINIVLD;
                }
                if (objInserir.NUMMESFIMVLD == 0)
                {
                    objInserir.NUMMESFIMVLD = LISTVERIFVIGENCIA[0].NUMMESFIMVLD;
                }

                if (objInserir.DATINIVLD != null && objInserir.DATFIMVLD != null && LISTVERIFVIGENCIA[0].DATFIMVLD != null)
                {
                    if (Convert.ToDateTime(objInserir.DATINIVLD) < Convert.ToDateTime(LISTVERIFVIGENCIA[0].DATINIVLD) || Convert.ToDateTime(objInserir.DATINIVLD) > Convert.ToDateTime(LISTVERIFVIGENCIA[0].DATFIMVLD)
                           || Convert.ToDateTime(objInserir.DATFIMVLD) > Convert.ToDateTime(LISTVERIFVIGENCIA[0].DATFIMVLD) || Convert.ToDateTime(objInserir.DATFIMVLD) < Convert.ToDateTime(LISTVERIFVIGENCIA[0].DATINIVLD))
                    {
                        return "Intervalo de data Informado, está fora do intervalo da Condição Comercial, o intervalo é de " + LISTVERIFVIGENCIA[0].DATINIVLD + " até " + LISTVERIFVIGENCIA[0].DATFIMVLD;
                    }
                }
                else if (objInserir.DATINIVLD != null && objInserir.DATFIMVLD == null || objInserir.DATINIVLD != null && objInserir.DATFIMVLD != null && LISTVERIFVIGENCIA[0].DATFIMVLD == null)
                {
                    if (Convert.ToDateTime(objInserir.DATINIVLD) < Convert.ToDateTime(LISTVERIFVIGENCIA[0].DATINIVLD))
                    {
                        return "Intervalo de data Informado, está fora do intervalo da Condição Comercial, o intervalo é de " + LISTVERIFVIGENCIA[0].DATINIVLD + " até " + LISTVERIFVIGENCIA[0].DATFIMVLD;
                    }
                }

                if (objInserir.NUMMESINIVLD != 0 && objInserir.NUMMESFIMVLD != 0 && LISTVERIFVIGENCIA[0].NUMMESFIMVLD != 0)
                {
                    if (objInserir.NUMMESINIVLD < LISTVERIFVIGENCIA[0].NUMMESINIVLD || objInserir.NUMMESINIVLD > LISTVERIFVIGENCIA[0].NUMMESFIMVLD
                            || objInserir.NUMMESFIMVLD > LISTVERIFVIGENCIA[0].NUMMESFIMVLD || objInserir.NUMMESFIMVLD < LISTVERIFVIGENCIA[0].NUMMESINIVLD)
                    {
                        return "Intervalo de mês Informado, está fora do intervalo da Condição Comercial, o intervalo é de " + LISTVERIFVIGENCIA[0].NUMMESINIVLD + "º Mês até " + LISTVERIFVIGENCIA[0].NUMMESFIMVLD + "º Mês";
                    }
                }
                else if (objInserir.NUMMESINIVLD != 0 && objInserir.NUMMESFIMVLD == 0 || objInserir.NUMMESINIVLD != 0 && objInserir.NUMMESFIMVLD != 0 && LISTVERIFVIGENCIA[0].NUMMESFIMVLD == 0)
                {
                    if (objInserir.NUMMESINIVLD < LISTVERIFVIGENCIA[0].NUMMESINIVLD)
                    {
                        return "Intervalo de mês Informado, está fora do intervalo da Condição Comercial, o intervalo é de " + LISTVERIFVIGENCIA[0].NUMMESINIVLD + "º Mês até " + LISTVERIFVIGENCIA[0].NUMMESFIMVLD + "º Mês";
                    }
                }
            }
            //------------------------------------------------

            if (new CadastroCondicaoComercialServicoOportunidadeBLL().inserirCadastroCondicaoComercialServicoOportunidadeNovo(objInserir) == false)
            {
                return "Ocorreu um erro ao fazer a inserção da Condição Comercial";
            }
            else
            {
                return "";
            }
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A INSERCAO DA CONDICAO COMERCIAL SERVICO OPORTUNIDADE.");
            throw;
        }
    }
}