﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Data.OleDb;
using System.Net;
using System.IO;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using System.Transactions;
using OfficeOpenXml;


[RoutePrefix("api/ImportaCestaProduto")]
public class ImportaCestaProdutoController : ApiController
{
    [HttpPost]
    [Route("ObterImportaCestaProduto")]
    public List<ImportaCestaProdutoTO.obterImportaCestaProduto> obterImportaCestaProduto(ImportaCestaProdutoApiModel.ImportaCestaProdutoApiModel objPesquisar)
    {
        if (objPesquisar.ANOMESREF <= 0 && objPesquisar.CODCLI <= 0 && objPesquisar.CODPRDCES <= 0)
            return new List<ImportaCestaProdutoTO.obterImportaCestaProduto>();

        return new ImportaCestaProdutoBLL().obterImportaCestaProduto(objPesquisar);
    }

    [HttpPost]
    [Route("UploadFile")]
    public async Task<List<string>> UploadFile(string ANOMESREF, string DELETAR)
    {
        string nomeArquivo = string.Empty;

        ImportaCestaProdutoApiModel.ImportaCestaProdutoApiModel objInserir = new ImportaCestaProdutoApiModel.ImportaCestaProdutoApiModel();

        var bll = new ImportaCestaProdutoBLL();
        var caminhoupload = "~/Templates/";
        caminhoupload = System.Web.HttpContext.Current.Server.MapPath(caminhoupload);


        try
        {
            foreach (string file in HttpContext.Current.Request.Files)
            {
                var fileContent = HttpContext.Current.Request.Files[file];

                if (fileContent != null && fileContent.ContentLength > 0)
                {
                    nomeArquivo = fileContent.FileName;
                    var stream = fileContent.InputStream;
                    var path = Path.Combine(caminhoupload, nomeArquivo);
                    using (var fileStream = System.IO.File.Create(path))
                    {
                        stream.CopyTo(fileStream);
                    }
                }
            }
            await Task.Delay(0);

            string arquivo = Path.Combine(caminhoupload, nomeArquivo);
            var package = new ExcelPackage(new FileInfo(arquivo));
            var sheet = package.Workbook.Worksheets[1];


            int i = 2;
            int repetido = 0;
            int linha = 2;
            int count = sheet.Dimension.End.Row;

            //Inicialização das listas
            objInserir.LISTCODCLI = new List<string>();
            objInserir.LISTPROBLEMA = new List<string>();
            objInserir.LISTCODPRDCES = new List<string>();
            objInserir.LISTCODCLI = new List<string>();
            objInserir.LISTVLRPRDCES = new List<string>();
            objInserir.LISTVERIFCODCLI = new List<int>();
            objInserir.PROBLEMA = 0;
            objInserir.ANOMESREF = int.Parse(ANOMESREF);
            objInserir.DELETAR = int.Parse(DELETAR);


            //Inicialização das listas de produto, valor e cliente para otimizar as verificações.
            List<ImportaCestaProdutoTO.verificaExisteProduto> LISTVERIFCODPRDCES = new ImportaCestaProdutoBLL().verificaExisteProduto();

            List<ImportaCestaProdutoTO.verificaTipoValorProduto> LISTVERIFTIPVLRPRDCES = new ImportaCestaProdutoBLL().verificaTipoValorProduto();

            List<ImportaCestaProdutoTO.verificaExisteCliente> LISTVERIFCODCLI = new ImportaCestaProdutoBLL().verificaExisteCliente();

            //For que percorre todas colunas dos arquivos, para verificar se ele se enquadra em todas verificações. Se não gera um erro e é colocado na grid problema.
            for (i = 2; i <= count; i++)
            {
                //Inserção do valor para não dar erro quando o mesmo for null
                int CODPRDCESDATA_XLSX;
                //int cellValue = ;

                if (String.IsNullOrEmpty(sheet.Cells[i, 1].Text))
                {
                    CODPRDCESDATA_XLSX = 0;
                }
                else
                {
                    CODPRDCESDATA_XLSX = Convert.ToInt32(sheet.Cells[i, 1].Text);
                }

                //Verificação se o codigo do produto existe no CADASTRO CESTA PRODUTO.
                if (LISTVERIFCODPRDCES.Any(l => l.CODPRDCES == Convert.ToInt32(sheet.Cells[i, 1].Text)))
                {
                    //Verificação para se caso tenha algum problema, não adicione a lista de inserção, isso para otimização, pois se há um problema a inserção não é feita.
                    if (objInserir.PROBLEMA == 0)
                        objInserir.LISTCODPRDCES.Add(sheet.Cells[i, 1].Text);
                }
                else
                {
                    objInserir.LISTPROBLEMA.Add(" Linha " + linha + " Produto \"" + sheet.Cells[i, 1].Text + "\" não existe no Cadastro Cesta Produtos, esta Inativo ou está para não ser importado.");
                    objInserir.PROBLEMA++;
                }

                //Inserção do valor para não dar erro quando o mesmo for null
                int CODCLIDATA_XLSX;

                if (String.IsNullOrEmpty(sheet.Cells[i, 2].Text))
                {
                    CODCLIDATA_XLSX = 0;
                }
                else
                {
                    CODCLIDATA_XLSX = Convert.ToInt32((sheet.Cells[i, 2].Text.Trim()));

                }

                //Verificação se cliente é um cliente martins.
                if (LISTVERIFCODCLI.Any(l => l.CODCLI == CODCLIDATA_XLSX))
                {
                    if (objInserir.PROBLEMA == 0)
                        objInserir.LISTCODCLI.Add(sheet.Cells[i, 2].Text.Trim());
                }
                else
                {
                    //Confirmação da verificação se é ou não cliente martins, pesquisa feita na MRT.T0100043.

                    int verificaClienteExiste;
                    verificaClienteExiste = new ImportaCestaProdutoBLL().verificaExisteClienteMartins(CODCLIDATA_XLSX);

                    if (verificaClienteExiste == 1)
                    {
                        objInserir.LISTCODCLI.Add(sheet.Cells[i, 2].Text.Trim());
                    }
                    else
                    {
                        objInserir.LISTPROBLEMA.Add(" Linha " + linha + " Cliente \"" + sheet.Cells[i, 2].Text + "\" não existe na base Martins ou está desativado.");
                        objInserir.PROBLEMA++;
                    }
                }

                //Verificação se o dado é VALOR ou TEXTO, e se o campo está no formato correto.
                decimal produto;

                if (LISTVERIFTIPVLRPRDCES[Convert.ToInt32(sheet.Cells[i, 1].Text) - 1].TIPDDOATR == 1)
                {
                    if (decimal.TryParse(sheet.Cells[i, 3].Text, out produto))
                    {
                        if (objInserir.PROBLEMA == 0)
                        {
                            objInserir.LISTVLRPRDCES.Add(decimal.Parse(sheet.Cells[i, 3].Text.Replace(".", ",")).ToString());
                        }
                    }
                    else
                    {
                        objInserir.LISTPROBLEMA.Add(" Linha " + linha + " Valor  \"" + sheet.Cells[i, 3].Text + "\" do " + " Produto \"" + sheet.Cells[i, 1].Text + "\" " + " não está no tipo correto, deveria ser Númerico.");
                        objInserir.PROBLEMA++;
                    }
                }
                else if (LISTVERIFTIPVLRPRDCES[Convert.ToInt32(sheet.Cells[i, 1].Text) - 1].TIPDDOATR == 2)
                {
                    //if (!(decimal.TryParse(objInserir.DATA_XLSX[i][2], out produto)))
                    if (sheet.Cells[i, 3].Text.Trim() == "SIM" || sheet.Cells[i, 3].Text.Trim() == "NAO" 
                        /*
                        || sheet.Cells[i, 3].Text.Trim() == "TRANSFORMACAO"
                        || sheet.Cells[i, 3].Text.Trim() == "OPERACAO" || sheet.Cells[i, 3].Text.Trim() == "DESATIVADO" || sheet.Cells[i, 3].Text.Trim() == "GRANDE"
                        || sheet.Cells[i, 3].Text.Trim() == "MEDIO" || sheet.Cells[i, 3].Text.Trim() == "PEQUENO" || sheet.Cells[i, 3].Text.Trim() == "MUITO PEQUENO"
                        */
                        )
                    {
                        if (objInserir.PROBLEMA == 0)
                        {
                            objInserir.LISTVLRPRDCES.Add(sheet.Cells[i, 3].Text);
                        }
                    }
                    else
                    {
                        //objInserir.LISTPROBLEMA.Add(" Linha " + linha + " Valor  \"" + objInserir.DATA_XLSX[i][2] + "\" do " + " Produto \"" + objInserir.DATA_XLSX[i][0] + "\" " + " não está no tipo correto, deveria ser Texto.");
                        objInserir.LISTPROBLEMA.Add(" Linha " + linha + " Valor  \"" + sheet.Cells[i, 3].Text + "\" do " + " Produto \"" + sheet.Cells[i, 1].Text + "\" " + " não está no tipo correto, deveria estar marcado com SIM, NAO, TRANSFORMACAO, OPERACAO, DESATIVADO, GRANDE, MEDIO, PEQUENO, MUITO PEQUENO.");
                        objInserir.PROBLEMA++;
                    }
                }

                linha++;
            }

            //File.Delete(arquivo);

            if (objInserir.PROBLEMA == 0)
            {
                int countrepetido = objInserir.LISTCODPRDCES.Count;

                for (int h = 0; h < countrepetido; h++)
                {
                    repetido = 0;
                    for (int j = 0; j < countrepetido; j++)
                    {
                        if (objInserir.LISTCODPRDCES[h].ToString() == objInserir.LISTCODPRDCES[j].ToString() && objInserir.LISTCODCLI[h].ToString() == objInserir.LISTCODCLI[j].ToString())
                        {
                            repetido++;
                        }
                    }
                    if (repetido > 1)
                    {
                        objInserir.LISTPROBLEMA.Add("O Cliente " + objInserir.LISTCODCLI[h] + " e Produto " + objInserir.LISTCODPRDCES[h] + ", estão repetidos, Favor Verificar a linha " + (h + 2));
                        objInserir.PROBLEMA++;
                    }
                }

                if (objInserir.PROBLEMA == 0)
                {
                    using (TransactionScope Scope = new TransactionScope(TransactionScopeOption.Required, (new TimeSpan(0, 5, 0)))) // DE ATÉ 3 MINUTOS.
                    {
                        //Verificação se já existe ou não o ANO/MÊS na tabela, se sim e se o cliente CONFIRMOU, é feito a exclusão antes de inserir, se não é feito somente a inserção.
                        if (objInserir.DELETAR == 1)
                        {
                            foreach (var elem in LISTVERIFCODPRDCES)
                            {
                                objInserir.CODPRDCES = elem.CODPRDCES;
                                if (new ImportaCestaProdutoBLL().deletarImportaCestaProduto(objInserir) == false)
                                {
                                    objInserir.LISTPROBLEMA.Add(" Problema ao fazer a deleção do mês no movimento Cesta Produtos.");
                                    Scope.Dispose();
                                    return objInserir.LISTPROBLEMA;
                                }

                            }
                        }

                        if (new ImportaCestaProdutoBLL().inserirImportaCestaProduto(objInserir) == false)
                        {
                            objInserir.LISTPROBLEMA.Add(" Problema ao fazer a inserção do mês no movimento Cesta Produtos.");
                            Scope.Dispose();
                            return objInserir.LISTPROBLEMA;
                        }

                        Scope.Complete();
                        return objInserir.LISTPROBLEMA;
                    }
                }
                else
                {
                    return objInserir.LISTPROBLEMA;
                }
            }
            else
            {
                return objInserir.LISTPROBLEMA;
            }


        }
        catch (Exception ex)
        {
            Console.WriteLine("Erro ao acessar os dados: " + ex.Message);
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A UPLOAD DE ARQUIVO DO IMPORTAR CESTA DE PRODUTOS.");
            Utilitario.InsereLog(ex, "UploadFile", "ARQUIVO EXCEL", "ImportaCestaProdutoController", "ERRO AO FAZER A UPLOAD DE ARQUIVO DO IMPORTAR CESTA DE PRODUTOS.");
            return objInserir.LISTPROBLEMA;
        }
    }

    [HttpPost]
    [Route("VerificaExisteAnoMes")]
    public int VerificaExisteAnoMes(ImportaCestaProdutoApiModel.ImportaCestaProdutoApiModel objPesquisar)
    {
        //Verificação de lote do mes se já foi enviado ou não
        if (new ImportaCestaProdutoBLL().verificaEnvioLote(objPesquisar.ANOMESREF))
        {
            return 2;
        }

        return new ImportaCestaProdutoBLL().verificaExisteAnoMes(objPesquisar);
    }

    [HttpPost]
    [Route("Teste")]
    public bool Teste()
    {
        try
        {
            return true;
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    [HttpPost]
    [Route("ImportarCestaProduto")]
    public List<string> importarCestaProduto(ImportaCestaProdutoApiModel.ImportaCestaProdutoApiModel objInserir)
    {
        try
        {
            int i = 1;
            int repetido = 0;
            int linha = 2;
            int count = objInserir.DATA_XLSX.Length;

            //Inicialização das listas
            objInserir.LISTCODCLI = new List<string>();
            objInserir.LISTPROBLEMA = new List<string>();
            objInserir.LISTCODPRDCES = new List<string>();
            objInserir.LISTCODCLI = new List<string>();
            objInserir.LISTVLRPRDCES = new List<string>();
            objInserir.LISTVERIFCODCLI = new List<int>();
            objInserir.PROBLEMA = 0;



            //Inicialização das listas de produto, valor e cliente para otimizar as verificações.
            List<ImportaCestaProdutoTO.verificaExisteProduto> LISTVERIFCODPRDCES = new ImportaCestaProdutoBLL().verificaExisteProduto();

            List<ImportaCestaProdutoTO.verificaTipoValorProduto> LISTVERIFTIPVLRPRDCES = new ImportaCestaProdutoBLL().verificaTipoValorProduto();

            List<ImportaCestaProdutoTO.verificaExisteCliente> LISTVERIFCODCLI = new ImportaCestaProdutoBLL().verificaExisteCliente();

            //For que percorre todas colunas dos arquivos, para verificar se ele se enquadra em todas verificações. Se não gera um erro e é colocado na grid problema.
            for (i = 1; i <= count - 1; i++)
            {
                //Inserção do valor para não dar erro quando o mesmo for null
                int CODPRDCESDATA_XLSX;

                if (String.IsNullOrEmpty(objInserir.DATA_XLSX[i][0]))
                {
                    CODPRDCESDATA_XLSX = 0;
                }
                else
                {
                    CODPRDCESDATA_XLSX = Convert.ToInt32((objInserir.DATA_XLSX[i][0]).Trim());

                }

                //Verificação se o codigo do produto existe no CADASTRO CESTA PRODUTO.
                if (LISTVERIFCODPRDCES.Any(l => l.CODPRDCES == Convert.ToInt32(objInserir.DATA_XLSX[i][0].Trim())))
                {
                    //Verificação para se caso tenha algum problema, não adicione a lista de inserção, isso para otimização, pois se há um problema a inserção não é feita.
                    if (objInserir.PROBLEMA == 0)
                        objInserir.LISTCODPRDCES.Add(objInserir.DATA_XLSX[i][0].Trim());
                }
                else
                {
                    objInserir.LISTPROBLEMA.Add(" Linha " + linha + " Produto \"" + objInserir.DATA_XLSX[i][0] + "\" não existe no Cadastro Cesta Produtos, esta Inativo ou está para não ser importado.");
                    objInserir.PROBLEMA++;
                }

                //Inserção do valor para não dar erro quando o mesmo for null
                int CODCLIDATA_XLSX;

                if (String.IsNullOrEmpty(objInserir.DATA_XLSX[i][1]))
                {
                    CODCLIDATA_XLSX = 0;
                }
                else
                {
                    CODCLIDATA_XLSX = Convert.ToInt32((objInserir.DATA_XLSX[i][1]).Trim());

                }

                //Verificação se cliente é um cliente martins.
                if (LISTVERIFCODCLI.Any(l => l.CODCLI == CODCLIDATA_XLSX))
                {
                    if (objInserir.PROBLEMA == 0)
                        objInserir.LISTCODCLI.Add(objInserir.DATA_XLSX[i][1].Trim());
                }
                else
                {
                    //Confirmação da verificação se é ou não cliente martins, pesquisa feita na MRT.T0100043.

                    int verificaClienteExiste;
                    verificaClienteExiste = new ImportaCestaProdutoBLL().verificaExisteClienteMartins(CODCLIDATA_XLSX);

                    if (verificaClienteExiste == 1)
                    {
                        objInserir.LISTCODCLI.Add(objInserir.DATA_XLSX[i][1].Trim());
                    }
                    else
                    {
                        objInserir.LISTPROBLEMA.Add(" Linha " + linha + " Cliente \"" + objInserir.DATA_XLSX[i][1] + "\" não existe na base Martins ou está desativado.");
                        objInserir.PROBLEMA++;
                    }
                }

                //Verificação se o dado é VALOR ou TEXTO, e se o campo está no formato correto.
                decimal produto;

                if (LISTVERIFTIPVLRPRDCES[Convert.ToInt32(objInserir.DATA_XLSX[i][0]) - 1].TIPDDOATR == 1)
                {
                    if (decimal.TryParse(objInserir.DATA_XLSX[i][2], out produto))
                    {
                        if (objInserir.PROBLEMA == 0)
                        {
                            objInserir.LISTVLRPRDCES.Add(decimal.Parse(objInserir.DATA_XLSX[i][2].Replace(".", ",")).ToString());
                        }
                    }
                    else
                    {
                        objInserir.LISTPROBLEMA.Add(" Linha " + linha + " Valor  \"" + objInserir.DATA_XLSX[i][2] + "\" do " + " Produto \"" + objInserir.DATA_XLSX[i][0] + "\" " + " não está no tipo correto, deveria ser Númerico.");
                        objInserir.PROBLEMA++;
                    }
                }
                else if (LISTVERIFTIPVLRPRDCES[Convert.ToInt32(objInserir.DATA_XLSX[i][0]) - 1].TIPDDOATR == 2)
                {
                    //if (!(decimal.TryParse(objInserir.DATA_XLSX[i][2], out produto)))
                    if (objInserir.DATA_XLSX[i][2].Trim() == "SIM" || objInserir.DATA_XLSX[i][2].Trim() == "NAO" || objInserir.DATA_XLSX[i][2].Trim() == "TRANSFORMACAO"
                        || objInserir.DATA_XLSX[i][2].Trim() == "OPERACAO" || objInserir.DATA_XLSX[i][2].Trim() == "DESATIVADO" || objInserir.DATA_XLSX[i][2].Trim() == "GRANDE"
                        || objInserir.DATA_XLSX[i][2].Trim() == "MEDIO" || objInserir.DATA_XLSX[i][2].Trim() == "PEQUENO" || objInserir.DATA_XLSX[i][2].Trim() == "MUITO PEQUENO")
                    {
                        if (objInserir.PROBLEMA == 0)
                        {
                            objInserir.LISTVLRPRDCES.Add(objInserir.DATA_XLSX[i][2]);
                        }
                    }
                    else
                    {
                        //objInserir.LISTPROBLEMA.Add(" Linha " + linha + " Valor  \"" + objInserir.DATA_XLSX[i][2] + "\" do " + " Produto \"" + objInserir.DATA_XLSX[i][0] + "\" " + " não está no tipo correto, deveria ser Texto.");
                        objInserir.LISTPROBLEMA.Add(" Linha " + linha + " Valor  \"" + objInserir.DATA_XLSX[i][2] + "\" do " + " Produto \"" + objInserir.DATA_XLSX[i][0] + "\" " + " não está no tipo correto, deveria estar marcado com SIM, NAO, TRANSFORMACAO, OPERACAO, DESATIVADO, GRANDE, MEDIO, PEQUENO, MUITO PEQUENO.");
                        objInserir.PROBLEMA++;
                    }
                }

                linha++;
            }

            if (objInserir.PROBLEMA == 0)
            {
                int countrepetido = objInserir.LISTCODPRDCES.Count;

                for (int h = 0; h < countrepetido; h++)
                {
                    repetido = 0;
                    for (int j = 0; j < countrepetido; j++)
                    {
                        if (objInserir.LISTCODPRDCES[h].ToString() == objInserir.LISTCODPRDCES[j].ToString() && objInserir.LISTCODCLI[h].ToString() == objInserir.LISTCODCLI[j].ToString())
                        {
                            repetido++;
                        }
                    }
                    if (repetido > 1)
                    {
                        objInserir.LISTPROBLEMA.Add("O Cliente " + objInserir.LISTCODCLI[h] + " e Produto " + objInserir.LISTCODPRDCES[h] + ", estão repetidos, Favor Verificar a linha " + (h + 2));
                        objInserir.PROBLEMA++;
                    }
                }

                if (objInserir.PROBLEMA == 0)
                {
                    using (TransactionScope Scope = new TransactionScope(TransactionScopeOption.Required, (new TimeSpan(0, 5, 0)))) // DE ATÉ 3 MINUTOS.
                    {
                        //Verificação se já existe ou não o ANO/MÊS na tabela, se sim e se o cliente CONFIRMOU, é feito a exclusão antes de inserir, se não é feito somente a inserção.
                        if (objInserir.DELETAR == 1)
                        {
                            
                            if (new ImportaCestaProdutoBLL().deletarImportaCestaProduto(objInserir) == false)
                            {
                                objInserir.LISTPROBLEMA.Add(" Problema ao fazer a deleção do mês no movimento Cesta Produtos.");
                                Scope.Dispose();
                                return objInserir.LISTPROBLEMA;
                            }
                                                        
                        }

                        if (new ImportaCestaProdutoBLL().inserirImportaCestaProduto(objInserir) == false)
                        {
                            objInserir.LISTPROBLEMA.Add(" Problema ao fazer a inserção do mês no movimento Cesta Produtos.");
                            Scope.Dispose();
                            return objInserir.LISTPROBLEMA;
                        }

                        Scope.Complete();
                        return objInserir.LISTPROBLEMA;
                    }
                }
                else
                {
                    return objInserir.LISTPROBLEMA;
                }
            }
            else
            {
                return objInserir.LISTPROBLEMA;
            }
        }

        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO IMPORTAR ARQUIVO CESTA PRODUTO.");
            Utilitario.InsereLog(ex, "importarCestaProduto", "ImportaCestaProdutoApiModel.ImportaCestaProdutoApiModel", "ImportaCestaProdutoController", "ERRO AO IMPORTAR ARQUIVO CESTA PRODUTO.");
            throw;
        }
    }
}