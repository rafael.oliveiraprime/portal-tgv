﻿using System.Collections.Generic;
using System.Web.Http;

[RoutePrefix("api/RelRelacaoServicos")]
public class RelRelacaoServicosController : ApiController
{
    [HttpPost]
    [Route("ObterRelRelacaoServicos")]
    public List<RelRelacaoServicosTO.obterRelacaoServicos> ObterRelRelacaoServicos(RelRelacaoServicosApiModel.obterRelRelacaoServicosApiModel objPesquisar)
    {
        return new RelRelacaoServicosBLL().obterRelacaoServicos(objPesquisar);
    }

}





