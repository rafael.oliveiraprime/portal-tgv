﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for CadastroServicoController
/// </summary>
[RoutePrefix("api/CadastroCestaServico")]
public class CadastroCestaServicoController : ApiController
{
    [HttpPost]
    [Route("ObterCadastroCestaServico")]
    public List<CadastroCestaServicoTO.obterCadastroCestaServico> obterCadastroCondicao(CadastroCestaServicoApiModel.CadastroCestaServicoApiModel objPesquisar)
    {
        return new CadastroCestaServicoBLL().ObterCadastroCestaServico(objPesquisar);
    }   

    [HttpPost]
    [Route("InserirCadastroCestaServico")]
    public bool inserirCadastroCestaServico(CadastroCestaServicoApiModel.CadastroCestaServicoApiModel objInserir)
    {      
        return new CadastroCestaServicoBLL().InserirCadastroCestaServico(objInserir);
    }

    [HttpPost]
    [Route("AlterarCadastroCestaServico")]
    public bool alterarCadastroCestaServico(CadastroCestaServicoApiModel.CadastroCestaServicoApiModel objAlterar)
    {      
        return new CadastroCestaServicoBLL().alterarCadastroCestaServico(objAlterar);
    }

    [HttpPost]
    [Route("AtivarCadastroCestaServico")]
    public bool ativarCadastroCestaServico(CadastroCestaServicoApiModel.CadastroCestaServicoApiModel objAlterar)
    {
        return new CadastroCestaServicoBLL().ativarCadastroCestaServico(objAlterar);
    }

    [HttpPost]
    [Route("DesativarCadastroCestaServico")]
    public bool desativarCadastroCestaServico(CadastroCestaServicoApiModel.CadastroCestaServicoApiModel objAlterar)
    {
        return new CadastroCestaServicoBLL().desativarCadastroCestaServico(objAlterar);
    }
}