﻿using System.Collections.Generic;
using System.Web.Http;

[RoutePrefix("api/RelOportunidade")]
public class RelOportunidadeController : ApiController
{
    [HttpPost]
    [Route("ObterOportunidade")]
    public List<RelOportunidadeTO.obterOportunidade> ObterOportunidade(RelOportunidadeApiModel.obterRelOportunidadeApiModel objPesquisar)
    {
        return new RelOportunidadeBLL().obterOportunidade(objPesquisar);
    }

    
}





