﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

[RoutePrefix("api/CadastroConsultor")]
public class CadastroConsultorController : ApiController
{
    [HttpPost]
    [Route("ObterCadastroConsultor")]
    public List<CadastroConsultorTO.obterCadastroConsultor> obterCadastroConsultor(CadastroConsultorApiModel.CadastroConsultorApiModel objPesquisar)
    {
        return new CadastroConsultorBLL().obterCadastroConsultor(objPesquisar);
    }

    [HttpPost]
    [Route("ObterConsultorVendasAtivo")]
    public List<CadastroConsultorTO.obterConsultorVendasAtivo> obterConsultorVendasAtivo(CadastroConsultorApiModel.CadastroConsultorApiModel objPesquisar)
    {
        return new CadastroConsultorBLL().obterConsultorVendasAtivo(objPesquisar);
    }

    [HttpPost]
    [Route("InserirCadastroConsultor")]
    public int inserirCadastroConsultor(CadastroConsultorApiModel.CadastroConsultorApiModel objInserir)
    {
        try
        {          
            if (new CadastroConsultorBLL().verificaFuncionario(objInserir.CODFNC) <= 0)
            {
                if (new CadastroConsultorBLL().inserirCadastroConsultor(objInserir))
                    return 0;
                else
                    return 2;
            }
            else
            {
                return 1;
            }
        }
        catch (Exception ex)
        {
            Utilitario.CriaLogErro(ex, "ERRO AO INSERIR CADASTRO CONSULTOR.");
            Utilitario.InsereLog(ex, "inserirCadastroConsultor", "CadastroConsultorApiModel.CadastroConsultorApiModel", "CadastroConsultorController", "ERRO AO INSERIR CADASTRO CONSULTOR.");
            throw;
        }
    }

    [HttpPost]
    [Route("AlterarCadastroConsultor")]
    public bool alterarCadastroConsultor(CadastroConsultorApiModel.CadastroConsultorApiModel objAlterar)
    {
        return new CadastroConsultorBLL().alterarCadastroConsultor(objAlterar);
    }

    [HttpPost]
    [Route("AtivarCadastroConsultor")]
    public bool ativarCadastroConsultor(CadastroConsultorApiModel.CadastroConsultorApiModel objAlterar)
    {
        return new CadastroConsultorBLL().ativarCadastroConsultor(objAlterar);
    }

    [HttpPost]
    [Route("DesativarCadastroConsultor")]
    public string desativarCadastroConsultor(CadastroConsultorApiModel.CadastroConsultorApiModel objAlterar)
    {
        return new CadastroConsultorBLL().desativarCadastroConsultor(objAlterar);
    }
}