﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;


[RoutePrefix("api/Cobranca")]
public class CobrancaController : ApiController
{
    [HttpPost]
    [Route("buscartitulos")]
    public string buscartitulos()
    {
        return new  GestaoCobrancaBLL().buscartitulos();
    }

    [Route("obterCliTitulos")]
    public List<GestaoCobrancaTO.obterCliTitulos> obterCliTitulos(GestaoCobrancaApiModel.GestaoCobrancaApiModel _ObjPesquisar)
    {
        return new GestaoCobrancaBLL().obterCliTitulos(_ObjPesquisar);
    }

    
    [Route("ObterNotasFiscais")]
    public List<GestaoCobrancaTO.obterNotasTitulos> ObterNotasFiscais(GestaoCobrancaApiModel.GestaoCobrancaApiModel _ObjPesquisar)
    {
        return new GestaoCobrancaBLL().ObterNotasFiscais(_ObjPesquisar);
    }


    [Route("obterServicosAvulsosParcelas")]
    public List<GestaoCobrancaTO.obterServicosAvulsosParcelas> obterServicosAvulsosParcelas(GestaoCobrancaApiModel.GestaoCobrancaApiModel _ObjPesquisar)
    {
        var DAL = new GestaoCobrancaDAL();
        return DAL.obterServicosAvulsosParcelas(_ObjPesquisar);
    }





}