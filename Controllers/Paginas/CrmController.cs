﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

[RoutePrefix("api/Crm")]
public class CrmController : ApiController
{
    [HttpPost]
    [Route("ObterCrm")]
    public List<CrmTO.obterCrm> obterCrm(CrmApiModel.obterCrmApiModel objPesquisar)
    {
        return new CrmBLL().obterCrm(objPesquisar);
    }

    [HttpPost]
    [Route("ObterCrmNegociacao")]
    public CrmTO.obterCrmNegociacao obterCrmNegociacao(CrmApiModel.obterCrmNegociacao objPesquisar)
    {
        return new CrmBLL().obterCrmNegociacao(objPesquisar);
    }

    [HttpPost]
    [Route("ObterDadosBasicos")]
    public CrmTO.obterDadosBasicos obterDadosBasicos(CrmApiModel.obterDadosBasicos objPesquisar)
    {
        return new CrmBLL().obterDadosBasicos(objPesquisar);
    }

    [HttpPost]
    [Route("ObterGridsEstaticas")]
    public CrmTO.obterGridsEstaticas obterGridsEstaticas(CrmApiModel.obterGridsEstaticas objPesquisar)
    {
        return new CrmBLL().obterGridsEstaticas(objPesquisar);
    }

    [HttpPost]
    [Route("ObterEstruturaMartins")]
    public List<CrmTO.obterEstruturaMartins> obterEstruturaMartins(CrmApiModel.obterEstruturaMartins objPesquisar)
    {
        return new CrmBLL().obterEstruturaMartins(objPesquisar);
    }


    [HttpPost]
    [Route("ObterCestaProdutoCliente")]
    public List<CrmTO.obterCestaProdutoCliente> obterCestaProdutoCliente(CrmApiModel.obterCestaProdutoCliente objPesquisar)
    {      
        return new CrmBLL().obterCestaProdutoCliente(objPesquisar);
    }

    [HttpPost]
    [Route("ObterUltimos12Meses")]
    public CrmTO.obterUltimos12Meses obterUltimos12Meses()
    {

        CrmTO.obterUltimos12Meses _cestaProduto = new CrmTO.obterUltimos12Meses();

        string ano1 = Arquitetura.Classes.Util.DateTimeYear(0);
        string ano2 = Arquitetura.Classes.Util.DateTimeYear(-1);
        string ano3 = Arquitetura.Classes.Util.DateTimeYear(-2);
        string ano4 = Arquitetura.Classes.Util.DateTimeYear(-3);
        string ano5 = Arquitetura.Classes.Util.DateTimeYear(-4);
        string ano6 = Arquitetura.Classes.Util.DateTimeYear(-5);
        string ano7 = Arquitetura.Classes.Util.DateTimeYear(-6);
        string ano8 = Arquitetura.Classes.Util.DateTimeYear(-7);
        string ano9 = Arquitetura.Classes.Util.DateTimeYear(-8);
        string ano10 = Arquitetura.Classes.Util.DateTimeYear(-9);
        string ano11 = Arquitetura.Classes.Util.DateTimeYear(-10);
        string ano12 = Arquitetura.Classes.Util.DateTimeYear(-11);
        string ano13 = Arquitetura.Classes.Util.DateTimeYear(-12);

        string mesExtenso1 = Arquitetura.Classes.Util.DateTimeMonths(0);
        string mesExtenso2 = Arquitetura.Classes.Util.DateTimeMonths(-1);
        string mesExtenso3 = Arquitetura.Classes.Util.DateTimeMonths(-2);
        string mesExtenso4 = Arquitetura.Classes.Util.DateTimeMonths(-3);
        string mesExtenso5 = Arquitetura.Classes.Util.DateTimeMonths(-4);
        string mesExtenso6 = Arquitetura.Classes.Util.DateTimeMonths(-5);
        string mesExtenso7 = Arquitetura.Classes.Util.DateTimeMonths(-6);
        string mesExtenso8 = Arquitetura.Classes.Util.DateTimeMonths(-7);
        string mesExtenso9 = Arquitetura.Classes.Util.DateTimeMonths(-8);
        string mesExtenso10 = Arquitetura.Classes.Util.DateTimeMonths(-9);
        string mesExtenso11 = Arquitetura.Classes.Util.DateTimeMonths(-10);
        string mesExtenso12 = Arquitetura.Classes.Util.DateTimeMonths(-11);
        string mesExtenso13 = Arquitetura.Classes.Util.DateTimeMonths(-12);

        _cestaProduto.DATUM = mesExtenso1 + "/" + ano1;
        _cestaProduto.DATDOIS = mesExtenso2 + "/" + ano2;
        _cestaProduto.DATTRES = mesExtenso3 + "/" + ano3;
        _cestaProduto.DATQUATRO = mesExtenso4 + "/" + ano4;
        _cestaProduto.DATCINCO = mesExtenso5 + "/" + ano5;
        _cestaProduto.DATSEIS = mesExtenso6 + "/" + ano6;
        _cestaProduto.DATSETE = mesExtenso7 + "/" + ano7;
        _cestaProduto.DATOITO = mesExtenso8 + "/" + ano8;
        _cestaProduto.DATNOVE = mesExtenso9 + "/" + ano9;
        _cestaProduto.DATDEZ = mesExtenso10 + "/" + ano10;
        _cestaProduto.DATONZE = mesExtenso11 + "/" + ano11;
        _cestaProduto.DATDOZE = mesExtenso12 + "/" + ano12;
        _cestaProduto.DATTREZE = mesExtenso13 + "/" + ano13;

        //string ano1 = DateTime.Now.AddMonths(0).ToString("yy");
        //string ano2 = DateTime.Now.AddMonths(-1).ToString("yy");
        //string ano3 = DateTime.Now.AddMonths(-2).ToString("yy");
        //string ano4 = DateTime.Now.AddMonths(-3).ToString("yy");
        //string ano5 = DateTime.Now.AddMonths(-4).ToString("yy");
        //string ano6 = DateTime.Now.AddMonths(-5).ToString("yy");
        //string ano7 = DateTime.Now.AddMonths(-6).ToString("yy");
        //string ano8 = DateTime.Now.AddMonths(-7).ToString("yy");
        //string ano9 = DateTime.Now.AddMonths(-8).ToString("yy");
        //string ano10 = DateTime.Now.AddMonths(-9).ToString("yy");
        //string ano11 = DateTime.Now.AddMonths(-10).ToString("yy");
        //string ano12 = DateTime.Now.AddMonths(-11).ToString("yy");       
        //string ano13 = DateTime.Now.AddMonths(-12).ToString("yy");       

        //string mesExtenso1 = System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(Convert.ToInt32(DateTime.Now.AddMonths(0).ToString("MM"))).Substring(0,3);
        //string mesExtenso2 = System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(Convert.ToInt32(DateTime.Now.AddMonths(-1).ToString("MM"))).Substring(0,3);
        //string mesExtenso3 = System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(Convert.ToInt32(DateTime.Now.AddMonths(-2).ToString("MM"))).Substring(0,3);
        //string mesExtenso4 = System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(Convert.ToInt32(DateTime.Now.AddMonths(-3).ToString("MM"))).Substring(0,3);
        //string mesExtenso5 = System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(Convert.ToInt32(DateTime.Now.AddMonths(-4).ToString("MM"))).Substring(0,3);
        //string mesExtenso6 = System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(Convert.ToInt32(DateTime.Now.AddMonths(-5).ToString("MM"))).Substring(0,3);
        //string mesExtenso7 = System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(Convert.ToInt32(DateTime.Now.AddMonths(-6).ToString("MM"))).Substring(0,3);
        //string mesExtenso8 = System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(Convert.ToInt32(DateTime.Now.AddMonths(-7).ToString("MM"))).Substring(0,3);
        //string mesExtenso9 = System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(Convert.ToInt32(DateTime.Now.AddMonths(-8).ToString("MM"))).Substring(0,3);
        //string mesExtenso10 = System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(Convert.ToInt32(DateTime.Now.AddMonths(-9).ToString("MM"))).Substring(0,3);
        //string mesExtenso11 = System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(Convert.ToInt32(DateTime.Now.AddMonths(-10).ToString("MM"))).Substring(0,3);
        //string mesExtenso12 = System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(Convert.ToInt32(DateTime.Now.AddMonths(-11).ToString("MM"))).Substring(0,3);
        //string mesExtenso13 = System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(Convert.ToInt32(DateTime.Now.AddMonths(-12).ToString("MM"))).Substring(0,3);



        return _cestaProduto;
    }

    
    [HttpPost]
    [Route("ObterRecursosCliente")]
    public List<CrmTO.DadosRecursoCliente> ObterRecursosCliente(CrmApiModel.obterCestaProdutoCliente objPesquisar)
    {
        return new CrmBLL().ObterRecursosCliente(objPesquisar);
    }

    
    [HttpPost]
    [Route("ObterContatosTelevendas")]
    public List<CrmTO.obterContatosTelevendas> ObterContatosTelevendas(CrmApiModel.obterContatosTelevendas objPesquisar)
    {
        return new CrmBLL().obterContatosTelevendas(objPesquisar);
    }

    [HttpPost]
    [Route("ObterServicosAtvEnc")]
    public List<CrmTO.obterServicosAtvEnc> ObterServicosAtvEnc(CrmApiModel.obterContatosTelevendas objPesquisar)
    {
        return new CrmBLL().obterServicosAtvEnc(objPesquisar);
    }
}