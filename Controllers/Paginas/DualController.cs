﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for CadastroTipoContatoController
/// </summary>
[RoutePrefix("api/Dual")]
public class DualController : ApiController
{
    [HttpPost]
    [Route("ObterDual")]
    public List<DualTO.obterDual> obterCadastroTipoContato()
    {
        return new DualBLL().obterDual();
    }

    [HttpPost]
    [Route("ObterDualServico")]
    public List<DualTO.obterDualServico> obterDualServico()
    {
        return new DualBLL().obterDualServico();
    }

    [HttpPost]
    [Route("ObterDualDesconto")]
    public List<DualTO.obterDualDesconto> obterDualDesconto()
    {
        return new DualBLL().obterDualDesconto();
    }

    [HttpPost]
    [Route("ObterDuaBranco")]
    public List<DualTO.obterDuaBranco> obterDuaBranco()
    {
        return new DualBLL().obterDuaBranco();
    }

    [HttpPost]
    [Route("ObterDualAnotacao")]
    public List<DualTO.obterDualAnotacao> obterDualAnotacao()
    {
        return new DualBLL().obterDualAnotacao();
    }

    [HttpPost]
    [Route("ObterNotasFiscais")]
    public List<DualTO.obterNotasFiscais> obterNotasFiscais()
    {
        return new DualBLL().obterNotasFiscais();
    }
}