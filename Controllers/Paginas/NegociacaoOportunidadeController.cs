﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

[RoutePrefix("api/NegociacaoOportunidade")]
public class NegociacaoOportunidadeController : ApiController
{
    [HttpPost]
    [Route("ObterNegociacaoOportunidade")]
    public List<NegociacaoOportunidadeTO.obterNegociacaoOportunidade> obterNegociacaoOportunidade(NegociacaoOportunidadeApiModel.NegociacaoOportunidadeApiModel objPesquisar)
    {
        return new NegociacaoOportunidadeBLL().obterNegociacaoOportunidade(objPesquisar);
    }

    [HttpPost]
    [Route("ObterEstruturaBasica")]
    public List<NegociacaoOportunidadeTO.obterEstruturaBasica> obterEstruturaBasica(NegociacaoOportunidadeApiModel.NegociacaoOportunidadeApiModel objPesquisar)
    {
        return new NegociacaoOportunidadeBLL().obterEstruturaBasica(objPesquisar);
    }  

    [HttpPost]
    [Route("ObterDadosBasicos")]
    public NegociacaoOportunidadeTO.obterDadosBasicos obterDadosBasicos(NegociacaoOportunidadeApiModel.NegociacaoOportunidadeApiModel objPesquisar)
    {
        return new NegociacaoOportunidadeBLL().obterDadosBasicos(objPesquisar);
    }

    [HttpPost]
    [Route("ObterContatoClienteNegociacao")]
    public List<NegociacaoOportunidadeTO.obterContatoClienteNegociacao> obterContatoClienteNegociacao(NegociacaoOportunidadeApiModel.NegociacaoOportunidadeApiModel objPesquisar)
    {
        return new NegociacaoOportunidadeBLL().obterContatoClienteNegociacao(objPesquisar);
    }

    [HttpPost]
    [Route("ObterServicosClientes")]
    public List<NegociacaoOportunidadeTO.obterServicosClientes> obterServicosClientes(NegociacaoOportunidadeApiModel.NegociacaoOportunidadeApiModel objPesquisar)
    {
        return new NegociacaoOportunidadeBLL().obterServicosClientes(objPesquisar);
    }

    [HttpPost]
    [Route("obterValoresCondicaoComercial")]
    public NegociacaoOportunidadeTO.obterValoresCondicaoComercial ObterCondicoesComerciais(NegociacaoOportunidadeApiModel.NegociacaoOportunidadeApiModel objPesquisar)
    {
        return new NegociacaoOportunidadeBLL().obterValoresCondicaoComercial(objPesquisar);
    }


    [HttpPost]
    [Route("CancelarOportunidade")]
    public bool CancelarOportunidade(CadastroOportunidadeApiModel.CadastroOportunidadeApiModel objPesquisar)
    {
        //objAlterar.INDSTAOPTVND = 4

        return new NegociacaoOportunidadeBLL().CancelarOportunidade(objPesquisar);
    }




}