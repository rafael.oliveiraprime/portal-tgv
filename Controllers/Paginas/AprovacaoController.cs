﻿using System.Web.Http;
using Arquitetura.Components;
using System.Web.SessionState;
using System.Web;
using System;

[RoutePrefix("api/Aprovacao")]
public class AprovacaoController : ApiController, IRequiresSessionState
{

    [HttpPost]
    [Route("Aprovacao")]
    [DontAuthorizeFilterApi]
    public int Aprovacao(AprovacaoApiModel.CadastroOportunidadeApiModel parametros)
    {
        return new AprovacaoBLL().alterarCadastroOportunidade(parametros); ;
    }

    [Route("VerificaStatus")]
    [DontAuthorizeFilterApi]
    public int VerificaStatus(AprovacaoApiModel.CadastroOportunidadeApiModel parametros)
    {
        return new AprovacaoBLL().VerificaStatus(parametros); ;
    }


    [HttpPost]
    [Route("Aprovar")]
    public bool Aprovar(AprovacaoApiModel.CadastroOportunidadeApiModel parametros)
    {
        return new AprovacaoBLL().Aprovar(parametros); ;
    }

    [HttpPost]
    [Route("Reprovar")]
    public bool Reprovar(AprovacaoApiModel.CadastroOportunidadeApiModel parametros)
    {
        return new AprovacaoBLL().Reprovar(parametros); ;
    }


}