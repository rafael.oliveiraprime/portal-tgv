﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;


[RoutePrefix("api/CadastroGrupoEconomico")]
public class CadastroGrupoEconomicoController : ApiController
{
    [HttpPost]
    [Route("obterCadastroGrupoEconomico")]
    public List<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomico> obterCadastroGrupoEconomico(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel objPesquisar)
    {
        return new CadastroGrupoEconomicoBLL().obterCadastroGrupoEconomico(objPesquisar);
    }

    [HttpPost]
    [Route("InserirCadastroGrupoEconomico")]
    public bool inserirCadastroGrupoEconomico(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel objInserir)
    {
        return new CadastroGrupoEconomicoBLL().inserirCadastroGrupoEconomico(objInserir);
    }

    [HttpPost]
    [Route("AlterarCadastroGrupoEconomico")]
    public string alterarCadastroGrupoEconomico(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel objAlterar)
    {
        return new CadastroGrupoEconomicoBLL().alterarCadastroGrupoEconomico(objAlterar);
    }
    
    [HttpPost]
    [Route("obterCadastroGrupoEconomicoMartins")]
    public List<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoMartins> obterCadastroGrupoEconomicoMartins(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoMartinsApiModel objPesquisar)
    {
        return new CadastroGrupoEconomicoBLL().obterCadastroGrupoEconomicoMartins(objPesquisar);
    }


    [HttpPost]
    [Route("InserirCadastroGrupoEconomicoXClientes")]
    public string inserirCadastroGrupoEconomicoXClientes(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel objInserir)
    {
        return new CadastroGrupoEconomicoBLL().inserirCadastroGrupoEconomicoXClientes(objInserir);
    }


    [HttpPost]
    [Route("obterCadastroGrupoEconomicoXClientesExistentes")]
    public  List<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes> obterCadastroGrupoEconomicoXClientesExistentes(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel objInserir)
    {
        return new CadastroGrupoEconomicoBLL().obterCadastroGrupoEconomicoXClientesExistentes(objInserir);
    }

    
    [HttpPost]
    [Route("ApagarCadastroGrupoEconomicoXClientes")]
    public bool apagarCadastroGrupoEconomicoXClientes(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel objInserir)
    {
        return new CadastroGrupoEconomicoBLL().ApagarCadastroGrupoEconomicoXClientes(objInserir);
    }


    [HttpPost]
    [Route("InserirGrupoCliente")]
    public string inserirGrupoCliente(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoXClientesApiModel objInserir)
    {
        return new CadastroGrupoEconomicoBLL().inserirGrupoCliente(objInserir);
    }

    [HttpPost]
    [Route("FecharSessao")]
    public bool FecharSessao(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel objInserir)
    {
        GrupoEconomicoSession.ClearSession();
        return true;
    }


    [HttpPost]
    [Route("AtivarCadastroGrupoEconomico")]
    public bool ativarCadastroGrupoEconomico(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel objAlterar)
    {
        return new CadastroGrupoEconomicoBLL().ativarCadastroGrupoEconomico(objAlterar);
    }

    [HttpPost]
    [Route("DesativarCadastroGrupoEconomico")]
    public bool desativarCadastroGrupoEconomico(CadastroGrupoEconomicoApiModel.CadastroGrupoEconomicoApiModel objAlterar)
    {
        return new CadastroGrupoEconomicoBLL().desativarCadastroGrupoEconomico(objAlterar);
    }
}