﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

[RoutePrefix("api/CadastroAutomacao")]
public class CadastroAutomacaoController : ApiController
{
    [HttpPost]
    [Route("ObterCadastroAutomacao")]
    public List<CadastroAutomacaoTO.obterCadastroAutomacao> obterCadastroAutomacao(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objPesquisar)
    {
        return new CadastroAutomacaoBLL().obterCadastroAutomacao(objPesquisar);
    }

    //[HttpPost]
    //[Route("AtualizarSistema")]
    //public CadastroAutomacaoTO.obterCadastroAutomacao atualizarSistema(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objPesquisar)
    //{
    //    return new CadastroAutomacaoBLL().atualizarSistema(objPesquisar);
    //}

    [HttpPost]
    [Route("InserirCadastroAutomacao")]
    public int inserirCadastroAutomacao(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objInserir)
    {
        if (new CadastroAutomacaoBLL().VerificaCNPJ(objInserir.NUMCGCEMP) <= 0)  {

            if (new CadastroAutomacaoBLL().inserirCadastroAutomacao(objInserir))
                return 0;
            else 
                return 2;
        }
        else{
            return 1;
        }
    }

    [HttpPost]
    [Route("AlterarCadastroAutomacao")]
    public bool alterarCadastroAutomacao(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objAlterar)
    {
        return new CadastroAutomacaoBLL().alterarCadastroAutomacao(objAlterar);
    }

    [HttpPost]
    [Route("obterAutomacaoSistema")]
    public CadastroAutomacaoTO.obterAutomacaoSistema obterAutomacaoSistema(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objPesquisar)
    {
        return new CadastroAutomacaoBLL().obterAutomacaoSistema(objPesquisar);
    }

    [HttpPost]
    [Route("AtivarCadastroAutomacao")]
    public bool ativarCadastroAutomacao(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objAlterar)
    {
        return new CadastroAutomacaoBLL().ativarCadastroAutomacao(objAlterar);
    }

    [HttpPost]
    [Route("DesativarCadastroAutomacao")]
    public bool desativarCadastroCestaServico(CadastroAutomacaoApiModel.CadastroAutomacaoApiModel objAlterar)
    {
        return new CadastroAutomacaoBLL().desativarCadastroAutomacao(objAlterar);
    }



}