﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Data.OleDb;
using System.Net;
using System.IO;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using System.Transactions;
using OfficeOpenXml;
using Arquitetura.ControllerCommon.Ftps;
using Vendas.TGV.Portal.BO.Util;

[RoutePrefix("api/ImportarEstrutura")]
public class ImportarEstruturaController : ApiController
{
    [HttpPost]
    [Route("obterProcessamento")]
    public List<ImportarEstruturaTO.obterProcessamento> obterProcessamento()
    {
        return new ImportarEstruturaBLL().obterProcessamento();
    }


    [HttpPost]
    [Route("obterEstrutura")]
    public List<ImportarEstruturaTO.obterEstrutura> obterEstrutura(ImportaEstruturaApiModel.ImportaEstruturaApiModel objPesquisar)
    {
        if (objPesquisar.CODCLI == -1 && objPesquisar.CODFILEMP == -1 && objPesquisar.NOMCLI == "" && objPesquisar.NUMCGCCLI == "")
            return new List<ImportarEstruturaTO.obterEstrutura>();
        return new ImportarEstruturaBLL().obterEstrutura(objPesquisar);
    }


    [HttpPost]
    [Route("UploadFile")]
    public async Task<string> UploadFile()
    {
        string nomeArquivo = string.Empty;

        ImportaCestaProdutoApiModel.ImportaCestaProdutoApiModel objInserir = new ImportaCestaProdutoApiModel.ImportaCestaProdutoApiModel();

        var bll = new ImportaCestaProdutoBLL();
        var caminhoupload = "~/Templates/";  //ConfigurationManager.AppSettings["caminhoTemplateContratoVenda"];
        caminhoupload = System.Web.HttpContext.Current.Server.MapPath(caminhoupload);

        
        /*
        Ftps ftp = new Ftps();
            string retr = ftp.SimpleFTPDownload(Constants.HostnameImages, Constants.UsernameImagesFotos, Constants.PasswordImagesFotos, fileName, hostPath: new List<string>() { string.Format("Cli{0}", imagem.CODCLI) });

         * */
        try
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                foreach (string file in HttpContext.Current.Request.Files)
                {
                    var fileContent = HttpContext.Current.Request.Files[file];

                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        nomeArquivo = fileContent.FileName;
                        var stream = fileContent.InputStream;
                        var path = Path.Combine(caminhoupload, nomeArquivo);
                        using (var fileStream = System.IO.File.Create(path))
                        {
                            stream.CopyTo(fileStream);
                        }
                    }
                }
                await Task.Delay(0);
                string arquivo = Path.Combine(caminhoupload, nomeArquivo);

                Ftps sftp = new Ftps();

                List<string> caminhoServidor = new List<string>();
                string caminhoFtps = ConfigurationManager.AppSettings["ProcessamentoAqruivoFtps"];

                caminhoFtps.Split('/').ToList().ForEach(val => caminhoServidor.Add(val));



                string sftpRetorno = sftp.SimpleFTPUpload(Constants.HostnameImages, Constants.UsernameImages, Constants.PasswordImages, caminhoupload, nomeArquivo, caminhoServidor);

                if (sftpRetorno.Equals(string.Empty))
                {
                    scope.Complete();
                    return "0";
                }
                else
                {
                    scope.Dispose();
                    throw new Exception(sftpRetorno);
                }
            }
            //System.IO.Directory.CreateDirectory(_CaminhoProcessaArquivo);

            //System.IO.File.Copy(arquivo, _CaminhoProcessaArquivo + nomeArquivo, true);

        }
        catch (Exception ex)
        {
            Console.WriteLine("Erro ao acessar os dados: " + ex.Message);
            Utilitario.CriaLogErro(ex, "ERRO AO FAZER A UPLOAD DE ARQUIVO DO IMPORTAR CESTA DE PRODUTOS.");
            Utilitario.InsereLog(ex, "UploadFile", "ARQUIVO EXCEL", "ImportaCestaProdutoController", "ERRO AO FAZER A UPLOAD DE ARQUIVO DO IMPORTAR CESTA DE PRODUTOS.");
            return "1";
        }
    }





}