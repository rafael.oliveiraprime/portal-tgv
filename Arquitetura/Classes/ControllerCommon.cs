﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Configuration;


namespace Arquitetura.ControllerCommon
{
    namespace ReportViewer
    {
        public enum tpeExport
        {
            Excel2003,
            Excel,
            Word2003,
            Word,
            PDF,
            Image
        }
        public class ReportViewerReturn
        {
            public byte[] array { get; set; }
            public string mimeType { get; set; }
            public string fileNameExtension { get; set; }
        }
    }

    namespace EnviarEmail
    {
        public class Constants
        {
            #region Enumeradores
            public enum RetornoLogin
            {
                Sucesso = 0,
                UsrSenhaIncorreto = 1,
                Falha = 2,
                SuppliedCredentialInvalid = 49
            }

            public enum RetornoCadastro
            {
                Sucesso = 0,
                Falha = 1,
                CnpjInexistente = 2,
                CpfInexistente = 3,
                CpfCadastrado = 4,
                EmailCadastrado = 5
            }

            public enum TipoAcaoEmail
            {
                TokenExpirado = 0,
                Falha = 1,
                CadastroSucesso = 2,
                RedefinirSenha = 3,
                SenhaRecadastrada = 4,
            }

            public enum TipoToken
            {
                CadastroCliente = 1,
                TrocaSenha = 2
            }
            #endregion

            #region AppSettings
            public static string TIPEDE
            {
                get { return ConfigurationManager.AppSettings["TIPEDE"]; }
            }

            public static string UrlSistema
            {
                get { return ConfigurationManager.AppSettings["UrlSistema"]; }
            }

            public static string ModoDesenvolvimento
            {
                get { return ConfigurationManager.AppSettings["MODODESENVOLVIMENTO"]; }
            }

            public static string CAMINHO_LDAP
            {
                get { return ConfigurationManager.AppSettings["CAMINHO_LDAP"]; }
            }

            public static string PORTA_LDAP
            {
                get { return ConfigurationManager.AppSettings["PORTA_LDAP"]; }
            }

            public static string DISTINGUISHEDNAME_LDAP
            {
                get { return ConfigurationManager.AppSettings["DISTINGUISHEDNAME_LDAP"]; }
            }

            public static string PortaServidorEmail
            {
                get { return ConfigurationManager.AppSettings["PortaServidorEmail"]; }
            }

            public static string ServidorEmail
            {
                get { return ConfigurationManager.AppSettings["ServidorEmail"]; }
            }

            public static string SenhaEmailAplicacao
            {
                get { return ConfigurationManager.AppSettings["SenhaEmailAplicacao"]; }
            }

            public static string EmailAplicacao
            {
                get { return ConfigurationManager.AppSettings["EmailAplicacao"]; }
            }

            public static string NomeEmailAplicacao
            {
                get { return ConfigurationManager.AppSettings["NomeEmailAplicacao"]; }
            }

            public static string EmailsDesenvolvimentoEnviar
            {
                get { return ConfigurationManager.AppSettings["EmailsDesenvolvimentoEnviar"]; }
            }

            public static string LinkBoleto
            {
                get { return ConfigurationManager.AppSettings["LinkBoleto"]; }
            }


            public static string EmailsCopiaContratos
            {
                get { return ConfigurationManager.AppSettings["EmailsCopiaContratos"]; }
            }

            #endregion

            #region Constantes
            public const string USUARIO_LOGADO = "USUARIO_LOGADO";

            public const string COLOR_PGTO_DEBITO = "table-data-pgto-debt";
            public const string COLOR_PGTO_CREDITO = "table-data-pgto-cred";
            public const string COLOR_PGTO_ABERTO = "table-data-pgto-aberto";
            public const string COLOR_PGTO_ATRASADO = "table-data-pgto-atrasado";

            public const string COLOR_STS_PAGO = "table-data-bordered-pago";
            public const string COLOR_STS_ATRASADO = "table-data-bordered-atrasado";
            public const string COLOR_STS_ABERTO = "table-data-bordered-aberto";
            public const string COLOR_STS_ZERADO = "table-data-bordered-zerado";

            public const string COLOR_LN_PAGO = "table-data-colorrow-pago";
            public const string COLOR_LN_ATRASADO = "table-data-colorrow-atrasado";
            public const string COLOR_LN_ABERTO = "table-data-colorrow-aberto";

            public const string PGTOBGCOLOR_ABERTO = "#FFD200";
            public const string PGTOBGCOLOR_ATRASO = "#FF3E32";
            public const string PGTOBGCOLOR_CREDITO = "#7999D0";
            public const string PGTOBGCOLOR_DEBITO = "#FF9500";

            public const string LNBGCOLOR_PAGO = "#5B5B5F";
            public const string LNBGCOLOR_ATRASADO = "#FF3E32";
            public const string LNBGCOLOR_ABERTO = "#FFD200";

            public const string TABLE_EMAIL_FATURA = @"<tr style=""color: {6}; background-color: #F0F1F1; font-weight: bold;"">
                                                       <td style=""border: 2px solid #FFFFFF; text-align: center; padding: 10px;"">{0}</td>
                                                       <td style=""border: 2px solid #FFFFFF; text-align: center; padding: 10px;"">{1}</td>
                                                       <td style=""border: 2px solid #FFFFFF; text-align: center; padding: 10px;""><span style=""width: 14px; height: 14px; display: block; float: left; margin: 3px 3px 0 0; background-color: {7}""></span>{2}</td>
                                                       <td style=""border: 2px solid #FFFFFF; text-align: center; padding: 10px;""><b>{3}</b></td>
                                                       <td style=""border: 2px solid #FFFFFF; text-align: center; padding: 10px;"">{4}</td>
                                                       <td style=""border: 2px solid #FFFFFF; text-align: center; padding: 10px;""><b>{5}</b></td>
                                                   </tr>";
            #endregion
        }
    }

    namespace Ftps
    {

        public class Constants
        {
            #region AppSetings
            public static string FileLog
            {
                get { return ConfigurationManager.AppSettings["FileLog"]; }
            }

            public static string PortaServidorEmail
            {
                get { return ConfigurationManager.AppSettings["PortaServidorEmail"]; }
            }

            public static string ServidorEmail
            {
                get { return ConfigurationManager.AppSettings["ServidorEmail"]; }
            }

            public static string SenhaEmailAplicacao
            {
                get { return ConfigurationManager.AppSettings["SenhaEmailAplicacao"]; }
            }

            public static string EmailAplicacao
            {
                get { return ConfigurationManager.AppSettings["EmailAplicacao"]; }
            }

            public static string NomeEmailAplicacao
            {
                get { return ConfigurationManager.AppSettings["NomeEmailAplicacao"]; }
            }

            public static string EmailsDesenvolvimentoEnviar
            {
                get { return ConfigurationManager.AppSettings["EmailsDesenvolvimentoEnviar"]; }
            }

            public static string ModoDesenvolvimento
            {
                get { return ConfigurationManager.AppSettings["MODODESENVOLVIMENTO"]; }
            }

            public static string EmailsCopiaContratos
            {
                get { return ConfigurationManager.AppSettings["EmailsCopia"]; }
            }


            public static string Emails
            {
                get { return ConfigurationManager.AppSettings["Emails"]; }
            }

            public static string AnexoEmail
            {
                get { return ConfigurationManager.AppSettings["CaminhoAnexoArquivoEmail"]; }
            }

            public static string ProcessamentoAqruivo
            {
                get { return ConfigurationManager.AppSettings["CaminhoProcessaArquivo"]; }
            }
            public static string ProcessamentoAqruivoFtps
            {
                get { return ConfigurationManager.AppSettings["ProcessamentoAqruivoFtps"]; }
            }
            #endregion

            public static string PathUnlock
            {
                get { return ConfigurationManager.AppSettings["PathUnlock"]; }
            }

            public static string ChilkatUnlockKey
            {
                get { return ConfigurationManager.AppSettings["ChilkatUnlockKey"]; }
            }

            public static bool FtpPassive
            {
                get { return Convert.ToBoolean(ConfigurationManager.AppSettings["FtpPassive"]); }
            }

            public static string HostnameImages
            {
                get { return ConfigurationManager.AppSettings["HostnameImages"]; }
            }

            public static string UsernameImages
            {
                get { return ConfigurationManager.AppSettings["UsernameImages"]; }
            }

            public static string PasswordImages
            {
                get { return ConfigurationManager.AppSettings["PasswordImages"]; }
            }

            public static string PathImagesOriginal
            {
                get { return ConfigurationManager.AppSettings["PathImagesOriginal"]; }
            }

            public static string HostPathOriginal
            {
                get { return ConfigurationManager.AppSettings["HostPathOriginal"]; }
            }

            public static string UsernameImagesFotos
            {
                get
                {
                    if (HttpContext.Current.Request.Url.Host.Contains(UrlImagesFotos))
                        return ConfigurationManager.AppSettings["UsernameImagesFotosPrd"];
                    else
                        return ConfigurationManager.AppSettings["UsernameImagesFotos"];
                }
            }

            public static string PasswordImagesFotos
            {
                get
                {
                    if (HttpContext.Current.Request.Url.Host.Contains(UrlImagesFotos))
                        return ConfigurationManager.AppSettings["PasswordImagesFotosPrd"];
                    else
                        return ConfigurationManager.AppSettings["PasswordImagesFotos"];
                }
            }

            public static string UrlImagesFotos
            {
                get { return ConfigurationManager.AppSettings["UrlImagesFotos"]; }
            }

            public static string PathImagesFotos
            {
                get { return ConfigurationManager.AppSettings["PathImagesFotos"]; }
            }

        }
    }


}
