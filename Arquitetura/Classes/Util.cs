﻿
using Arquitetura.ControllerCommon.ReportViewer;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;

using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using Arquitetura.ControllerCommon.EnviarEmail;

using RestSharp;
using RestSharp.Authenticators;
using CrmTO;
using System.Text;
using System.Globalization;

namespace Arquitetura.Classes
{
    public class Util
    {

        public static readonly string timeStampSystemUp = DateTime.Now.ToString("yyyyMMddHHmmssffff");

        private static Thread _memoryManagementThread;

        public static bool memoryManagementThread
        {
            get
            {
                if (_memoryManagementThread == null)
                {
                    _memoryManagementThread = new Thread(async () =>
                    {
                        while (true)
                        {
                            await Task.Delay(1000 * 60);
                            try
                            {
                                GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced, true);
                                GC.WaitForFullGCComplete();
                                GC.WaitForFullGCApproach();
                                GC.WaitForPendingFinalizers();
                            }
                            catch
                            {
                                continue;
                            }
                        }
                    });
                    _memoryManagementThread.Start();
                }
                return true;
            }
        }

        public static string ExportWithReportViewer(string jsonRaw, tpeExport tpe)
        {
            var bytes = Arquitetura.Controllers.ReportViewerController.ExportByJsonParams(jsonRaw, tpe);
            string dtNow = DateTime.Now.ToString("ddMMyyyyhhmmssffffff");
            string fleName = (dtNow + Convert.ToString(".")) + bytes.fileNameExtension;
            string outUrl = (dtNow + Convert.ToString("/")) + bytes.fileNameExtension;
            if (!Directory.Exists(HostingEnvironment.MapPath("~/Temp")))
            {
                Directory.CreateDirectory(HostingEnvironment.MapPath("~/Temp"));
            }

            File.WriteAllBytes(Path.Combine(HostingEnvironment.MapPath("~/Temp"), fleName), bytes.array);
            return VirtualPathUtility.ToAbsolute(Convert.ToString("~/api/Temp/DownloadTemp/") + outUrl);
        }

        public enum TipoEmail
        {
            HTML = 1,
            Texto = 2
        }

        public static bool EnviaEmailPrime(string enderecoDestino, List<String> emailsCopia, string assunto, string mensagem, List<AnexoEmailTO.AnexoEmail> listAnexos, TipoEmail tipoEmail)
        {
            MailMessage email;
            MailAddress origem;
            SmtpClient smtpCliente;
            MemoryStream fluxoMemoria;
            Attachment anexo;

            try
            {
                string PortaServidorEmail = Constants.PortaServidorEmail;
                string ServidorEmail = Constants.ServidorEmail;
                string ModoDesenvolvimento = Constants.ModoDesenvolvimento;
                string SenhaEmailSistema = Constants.SenhaEmailAplicacao;
                string EmailSistema = Constants.EmailAplicacao;
                string NomeEmailSistema = Constants.NomeEmailAplicacao;
                string EmailsDesenvolvimentoEnviar = Constants.EmailsDesenvolvimentoEnviar;
                string EmailsCopiaContratos = Constants.EmailsCopiaContratos;


                if (!string.IsNullOrWhiteSpace(PortaServidorEmail))
                    smtpCliente = new SmtpClient(ServidorEmail, Convert.ToInt32(PortaServidorEmail));
                else
                    smtpCliente = new SmtpClient(ServidorEmail);


                smtpCliente.UseDefaultCredentials = false;

                if (!ModoDesenvolvimento.Equals("0"))
                {
                    if (SenhaEmailSistema.Trim().Length > 0)
                    {
                        smtpCliente.Credentials = new System.Net.NetworkCredential(EmailSistema, SenhaEmailSistema);
                        smtpCliente.EnableSsl = true;
                        smtpCliente.DeliveryMethod = SmtpDeliveryMethod.Network;
                    }
                }


                origem = new MailAddress(EmailSistema, NomeEmailSistema);
                email = new MailMessage();
                email.From = origem;
                if (ModoDesenvolvimento.Equals("0"))
                {
                    if (enderecoDestino == null && emailsCopia != null && emailsCopia.Count > 0)
                    {
                        foreach (string itm in emailsCopia)
                            email.To.Add(itm);
                    }
                    else
                    {
                        email.To.Add(enderecoDestino);
                        var splt = EmailsCopiaContratos.Split(',');
                        foreach (string itm in splt)
                            email.Bcc.Add(itm);


                        //if (emailsCopia != null)
                        //    foreach (string itm in emailsCopia)
                        //        email.CC.Add(itm);
                    }
                }
                else
                {
                    email.To.Add(enderecoDestino);
                    var splt = EmailsCopiaContratos.Split(',');
                    foreach (string itm in splt)
                        email.Bcc.Add(itm);


                    //if (splt.Where(x => x.ToUpper().Equals(enderecoDestino.ToUpper())).Any())
                    //    email.To.Add(enderecoDestino);
                    //else
                }
                email.IsBodyHtml = (tipoEmail == TipoEmail.HTML);
                email.Subject = assunto.Replace("\n", " ");
                email.Body = mensagem;
                email.AlternateViews.Add(ObterLogo(mensagem));


                if (listAnexos != null && listAnexos.Count() > 0)
                {
                    listAnexos.ForEach(val =>
                    {
                        fluxoMemoria = new MemoryStream(val.FileByte);
                        anexo = new Attachment(fluxoMemoria, val.FileName);
                        email.Attachments.Add(anexo);
                    });
                }
                if (!ModoDesenvolvimento.Equals("0"))
                    ServicePointManager.ServerCertificateValidationCallback += delegate (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                    {
                        return true;
                    };

                try
                {
                    smtpCliente.Send(email);
                }
                catch (Exception)
                {
                    if (ModoDesenvolvimento.Equals("0"))
                        throw;
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                mensagem = null;
                smtpCliente = null;
                fluxoMemoria = null;
                anexo = null;
            }
        }

        public static string GetTemplateHtml(string path)
        {
            var logicPath = System.Web.HttpContext.Current.Server.MapPath(path);
            return System.IO.File.ReadAllText(logicPath);
        }

        public static AlternateView ObterLogo(String Mensagem)
        {
            LinkedResource Recurso = new LinkedResource(HostingEnvironment.MapPath("~/assets/img/img_logo_tgv.png"));
            Recurso.ContentId = "img_logo_tgv";
            string CorpoHTML = Mensagem;
            AlternateView Visao = AlternateView.CreateAlternateViewFromString(CorpoHTML, null, System.Net.Mime.MediaTypeNames.Text.Html);
            Visao.LinkedResources.Add(Recurso);
            return Visao;
        }

        public static bool ValidarEmail(string email)
        {
            return new Regex(@"^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$").IsMatch(email);
        }

        public static string RemoverCaracteresEspeciais(string input)
        {
            string exp = "[^a-zA-Z0-9_]";
            String strReter = Regex.Replace(input, exp, "");
            return strReter;
        }


        public static bool EnviaEmailMartins(string enderecoDestino, List<String> emailsCopia, string assunto, string mensagem, List<AnexoEmailTO.AnexoEmail> listAnexos, TipoEmail tipoEmail)
        {
            MailMessage email;
            MailAddress origem;
            SmtpClient smtpCliente;
            MemoryStream fluxoMemoria;
            Attachment anexo;

            try
            {
                string ModoDesenvolvimento = Constants.ModoDesenvolvimento;
                string ServidorEmail = Constants.ServidorEmail;
                string EmailSistema = Constants.EmailAplicacao;
                string NomeEmailSistema = Constants.NomeEmailAplicacao;
                string EmailsDesenvolvimentoEnviar = Constants.EmailsDesenvolvimentoEnviar;
                string EmailsCopiaContratos = Constants.EmailsCopiaContratos;

                smtpCliente = new SmtpClient(ServidorEmail);

                origem = new MailAddress(EmailSistema, NomeEmailSistema);
                email = new MailMessage();
                email.From = origem;
                if (ModoDesenvolvimento.Equals("0"))
                {
                    if (enderecoDestino == null && emailsCopia != null && emailsCopia.Count > 0)
                    {
                        foreach (string itm in emailsCopia)
                            email.To.Add(enderecoDestino);
                    }
                    else
                    {
                        email.To.Add(enderecoDestino);
                        var splitcopias = EmailsCopiaContratos.Split(',');
                        foreach (string itm in splitcopias)
                            email.Bcc.Add(itm);


                        //var splt = EmailsDesenvolvimentoEnviar.Split(',');
                        //foreach (string itm in splt)
                        //    email.CC.Add(itm);

                        //if (emailsCopia != null)
                        //{
                        //    foreach (string itm in emailsCopia)
                        //        email.CC.Add(itm);
                        //}
                    }
                }
                else
                {
                    email.To.Add(enderecoDestino);
                    var splitcopias = EmailsCopiaContratos.Split(',');
                    foreach (string itm in splitcopias)
                        email.Bcc.Add(itm);

                    var splt = EmailsDesenvolvimentoEnviar.Split(',');
                    //if (splt.Where(x => x.ToUpper().Equals(enderecoDestino.ToUpper())).Any())
                        //email.To.Add(enderecoDestino);
                    //else
                    //{
                        //email.To.Add(enderecoDestino);
                        //foreach (string itm in splt)
                        //    email.CC.Add(itm);
                    //}      
                }
                email.IsBodyHtml = (tipoEmail == TipoEmail.HTML);
                email.Subject = assunto.Replace("\n", " ");
                email.Body = mensagem;
                //email.AlternateViews.Add(ObterLogo(mensagem));
                if (listAnexos != null && listAnexos.Count() > 0)
                {
                    listAnexos.ForEach(val =>
                    {
                        fluxoMemoria = new MemoryStream(val.FileByte);
                        anexo = new Attachment(fluxoMemoria, val.FileName);
                        email.Attachments.Add(anexo);
                    });
                }
                if (!ModoDesenvolvimento.Equals("0"))
                    ServicePointManager.ServerCertificateValidationCallback += delegate (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                    {
                        return true;
                    };
                try
                {
                    smtpCliente.Send(email);
                }
                catch (Exception ex)
                {
                        throw ex;
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                mensagem = null;
                smtpCliente = null;
                fluxoMemoria = null;
                anexo = null;
            }
        }

        public static string DateTimeYear(int meses)
        {
           string ano = DateTime.Now.AddMonths(meses).ToString("yy");

            return ano;
        }

        public static string DateTimeMonths(int meses)
        {
        string mesExtenso = System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(Convert.ToInt32(DateTime.Now.AddMonths(meses).ToString("MM"))).Substring(0,3);

            return mesExtenso;
        }


        public static Token GetToken(string client_id, string client_secret)
        {
            string url = "";

            try
            {

                string urlApi = ConfigurationManager.AppSettings["urlApiToken"];

                url = urlApi;
                RestClient client = new RestClient(url);
                client.Authenticator = new HttpBasicAuthenticator(client_id, client_secret);

                RestRequest request = new RestRequest();
                request.Method = Method.POST;
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddParameter("grant_type", "client_credentials");

                var response = client.Execute<Token>(request);

                if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created)
                {
                    //HttpContext.Current.Session["TOKEN"] = response.Data.AccessToken;
                }
                else
                {
                    //throw new ExceptionApi(response.ErrorMessage);
                }

                return response.Data;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static string ReplaceSpecialCharacters(string text)
        {
            StringBuilder sbReturn = new StringBuilder();
            var arrayText = text.Normalize(NormalizationForm.FormD).ToCharArray();
            foreach (char letter in arrayText)
            {
                if (CharUnicodeInfo.GetUnicodeCategory(letter) != UnicodeCategory.NonSpacingMark)
                    sbReturn.Append(letter);
            }
            return Regex.Replace(sbReturn.ToString(), "[^0-9a-zA-Z ]+", string.Empty);
        }


    }
}