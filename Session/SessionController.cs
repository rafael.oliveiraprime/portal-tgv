﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class SessionController<T>
    where T : class, new()
{
    public static string SessionName
    {
        get
        {
            string sessionID = "";
            if (System.Web.HttpContext.Current != null)
                sessionID = System.Web.HttpContext.Current.Session.SessionID;
            return sessionID + "_" + typeof(T).FullName;
        }
    }

    protected static T CurrentSession
    {
        get
        {
            T obj = System.Web.HttpContext.Current.Session[SessionName] as T;
            if (obj == null)
            {
                obj = new T();
                System.Web.HttpContext.Current.Session.Add(SessionName, obj);
            }
            return obj;
        }
    }

    public static void ClearSession()
    {
        System.Web.HttpContext.Current.Session.Remove(SessionName);
    }

    public static void ClearAllSessions()
    {
        string sessionID = "";

        if (System.Web.HttpContext.Current != null)
        {
            sessionID = System.Web.HttpContext.Current.Session.SessionID;

            foreach (System.Collections.DictionaryEntry session in System.Web.HttpContext.Current.Session)
                if (session.Key.ToString().StartsWith(sessionID + "_"))
                    System.Web.HttpContext.Current.Session.Remove(session.Key.ToString());
        }
    }
}