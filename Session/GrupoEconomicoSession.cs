﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


/// <summary>
/// Summary description for GrupoEconomicoSession
/// </summary>
public class GrupoEconomicoSession : SessionController<GrupoEconomicoSession>
{
    private List<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes> _grupo;

    public static List<CadastroGrupoEconomicoTO.obterCadastroGrupoEconomicoXClientesExistentes> Grupo
    {
        get
        {
            return CurrentSession._grupo;
        }
        set
        {
            CurrentSession._grupo = value;
        }
    }
}